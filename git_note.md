## 参考：
[1] progit
    http://www.git-scm.com/book/
    http://www.git-scm.com/book/zh 中文版
    https://github.com/progit/progit

[2] https://help.github.com/articles/fork-a-repo

[3] http://scottchacon.com/2011/08/31/github-flow.html

[4] http://gitguru.com/2009/02/03/rebase-v-merge-in-git/

[5] http://stackoverflow.com/questions/457927/git-workflow-and-rebase-vs-merge-questions

[6] http://notes.envato.com/developers/rebasing-merge-commits-in-git/

[7] [原]通过GitHub Pages建立个人站点（详细步骤）
  http://www.cnblogs.com/purediy/archive/2013/03/07/2948892.html
  
[8] Checking out pull requests locally
  https://help.github.com/articles/checking-out-pull-requests-locally/
  
[9] restoring lost commits 
  http://gitready.com/advanced/2009/01/17/restoring-lost-commits.html
  
[10] git diff,git apply和patch小问题三则
  http://openwares.net/linux/git_diff_git_apply_patch.html
  
[11] How do I remove local (untracked) files from my current Git branch?
  http://stackoverflow.com/questions/61212/how-do-i-remove-local-untracked-files-from-my-current-git-branch
  
[12] git-svn: Use prefix by default?
  http://git.661346.n2.nabble.com/git-svn-Use-prefix-by-default-td7594288.html#a7597159

[13] A collection of .gitignore templates
  https://github.com/github/gitignore

[14]
  https://stackoverflow.com/questions/17404316/the-following-untracked-working-tree-files-would-be-overwritten-by-merge-but-i

[15] git之https或http方式设置记住用户名和密码的方法
  http://www.cnblogs.com/wish123/p/3937851.html

[16] Git for windows 中文乱码解决方案
  https://segmentfault.com/a/1190000000578037
  
[17] How do I make Git ignore file mode (chmod) changes?
  https://stackoverflow.com/questions/1580596/how-do-i-make-git-ignore-file-mode-chmod-changes
  
[18] git 显示中文文件名
  https://www.jianshu.com/p/297ff9b730cf

[19] git导出代码的方法~archive
  https://www.jianshu.com/p/98fa58073554
  
[20] git init
  https://www.runoob.com/git/git-init.html

## 初始化
[20]
```
$ mkdir runoob
$ cd runoob/
$ git init
```
现在你可以看到在你的项目中生成了 .git 这个子目录，这就是你的 Git 仓库了，所有有关你的此项目的所有内容和元数据都存放在这里。
执行 `git init` 的目录不需要是空的，但不能有 `.git` 子目录。

## gui
在工作目录下执行git gui可启动自带的gui程序。

## 换行符
git 可以在签入和签出代码时自动转换。通常，git的内部存储采用\n换行，签入时换行自动转换为\n；签出时可以配置为不转换或转换为\r\n。
也可以将 git 配置为不做换行符自动转换。
有两种方法可以影响git的换行行为：本机配置和仓库配置。
(1) 本机配置
这影响本机上的 git 工具，从而影响本机上的git仓库。
git config --global core.autocrlf false|true|input

false: 不做换行符自动转换
true： 签入时换行自动转换为\n，签出时转换为\r\n
input： 签入时换行自动转换为\n，签出时转不转换

(2) 仓库配置
这影响一个仓库。优先级要高于 core.autocrlf。
在仓库中加入一个文件 .gitattributes，可以在里面设置文件/目录的属性，包括换行设置。

例如这样一行：
* -text
意思是将所有的文件按原样对待（而不是text模式），这样git就不会做自动换行符转换。

参考：http://stackoverflow.com/questions/9933004/best-way-to-disable-git-end-of-line-normalization-crlf-to-lf-across-all-clones

git help attributes 可以找到更多配置方式：

               *.txt           text
               *.vcproj        eol=crlf
               *.sh            eol=lf
               *.jpg           -text


## clone
* 浅克隆
在一些情况下，使用 clone --depth=1 创建一个浅克隆比较划算些。这种克隆初始化的更快，但有一些局限性：不能再被克隆；不能作为push的源（只能用patch提交你的修改）。
浅克隆可以在必要时转化为普通库（ git fetch --unshallow ），并可以实现”增量下载“，详见fetch。

* 指定分支 clone
git clone -b <branch> <remote_repo>

Example:

git clone -b my-branch git@github.com:user/myproject.git

* clone 单个分支
--single-branch，例如
git clone -b 2.4 --single-branch https://github.com/Itseez/opencv.git opencv-2.4

* 关联全部分支
默认情况下，只有主分支（通常是master）被建立关联（track），尽管所有的分支都被下载了。
git clone --mirror url
可以为所有的远程分支建立关联。

## remote
* 列出远程源
git remote -v

* 更改远程源的 url
|git remote set-url origin git@github.com:user/repo.git

* 增加远程源
|git remote add upstream https://github.com/octocat/Spoon-Knife.git

upstream 是远程源的名字。


* update
git remote update
Fetch updates for a named set of remotes in the repository as defined by remotes.<group>.

## push
基本形式 git push [remote-name] [branch-name]

|git push
implies:
|git push origin
|git push origin branchX #default branch is usually master

以上仅限于本地分支与远程分支已经挂钩的情况（可以用 branch --set-upstream-to 设置），如果没有，则使用完整形式 
git push [remote-name] [local-branch]:[remote-branch]

* 删除/同步到远程分支
如果 [local-branch] 留空（':'保留），则效果是删除远程分支，可以理解为将空分支推送到远程仓库。
例如
git push origin :remote-branch
等效的命令是
git push origin --delete remote-branch

删除远程库中本地已不存在的分支：
git push --prune origin

让远程库成为本地库的镜像：
git push --mirror
 (local heads, remotes and tags are mirrored on remote)

使用以上两个方法需要先把本地分支删除。

参考：
http://stackoverflow.com/questions/2003505/how-do-i-delete-a-git-branch-both-locally-and-remote

## fetch
git fetch

git fetch <remote> <rbranch>:<lbranch> 
下载指定的远程分支rbranch，关联到本地分支lbranch。不指定分支时，git fetch 只会下载一个主分支。:<lbranch> 可以省略，这样创建一个同名的本地分支(经实验，并不会创建本地分支)。

-p, --prune
           After fetching, remove any remote-tracking branches which no longer exist on the remote. 可以用于删除已经在远程被删除的本地分支。
           
git fetch --all 下载全部远程分支

* 转化浅克隆为普通库，指定深度，增量下载/断点续传
参考
http://stackoverflow.com/questions/3954852/how-to-complete-a-git-clone-for-a-big-project-on-an-unstable-connection

git fetch --unshallow 
可以转化浅克隆为普通库

git fetch --depth=N 
指定下载历史的深度。
先做浅克隆（git clone --depth=1），然后通过逐渐地增大fetch深度，可以实现增量下载，这在网络不稳定的情况下能改善可靠性。

不过，git还没有实现svn那样基于单个文件的断点续传。

在使用 submoudule 的时候，也可以浅克隆
http://stackoverflow.com/questions/2144406/git-shallow-submodules
git submodule add --depth 1 -- repository path
git submodule update --init --recursive --depth 1
git submodule update --depth 1 -- [<path>...]

不过，如果 submoudule 指向的不是最新的提交，甚至不是主分支中的提交，就会发生错误。这时可以记录下 submoudule 需要的提交的hash，
在github上查找其相关信息（所在分支、倒数第几个提交），然后在 submoudule 的目录里执行：
git fetch --depth N <remote> <rbranch>
N 要达到足够包括所需的提交。然后再在上级仓库目录执行
git submodule update

## add
添加到待提交区，或加入尚未被版本控制的文件。
git add path

* 交互式添加
git add -p
git add -i

## commit
* 自动添加 -a --add
git commit -a -m "aaa"
自动添加所有修改或删除的文件，但不会自动添加尚未被版本控制的文件。

* 修正上次的提交
有时候我们提交完了才发现漏掉了几个文件没有加，或者提交信息写错了。想要撤消刚才的提交操作，可以使用--amend 选项重新提交。
git commit --amend
此命令将使用当前的暂存区域快照提交。如果刚才提交完没有作任何改动，直接运行此命令的话，相当于有机会重新编辑提交说明，而所提交的文件快照和之前的一样。
如果刚才提交时忘了暂存某些修改，可以先补上暂存操作，然后再运行--amend 提交。

## 指定一个提交
在git的命令中，可以用提交的hash或其前几个字符来指代它。例如：
git checkout f82bf36

对于最近的提交，也可以用“倒数法“来指代（注意是波浪线，不是减号）：
HEAD~n：从HEAD起的倒数第 n+1 个提交，HEAD本身是 HEAD~0。
branchName~n：某个分支的倒数 n+1 个提交。

例子： git checkout HEAD~3

对于前驱提交，也可以用 ^来指代，如 HEAD^。
如果有多个前驱提交，可在^后面跟上数字，例如 HEAD^2 表示第二个前驱提交。

## branch
* 查看分支
git branch 查看当前分支
git branch -vv 列出所有本地和远程分支及其关联关系
git checkout 查看当前分支关联的远程分支
git branch -a -v  列出所有分支。带*的是当前检出的分支。远程分支列在remotes/项目下。
git branch -r  列出远程分支。

* 更改默认远程库/关联的远程分支
http://stackoverflow.com/questions/520650/make-an-existing-git-branch-track-a-remote-branch

Given a branch foo and a remote upstream:

As of Git 1.8.0:

git branch -u upstream/foo

Or, if local branch foo is not the current branch:

git branch -u upstream/foo foo

Or, if you like to type longer commands, these are equivalent to the above two:

git branch --set-upstream-to=upstream/foo

git branch --set-upstream-to=upstream/foo foo


这个办法可能不灵，可以尝试删除相应的本地分支，再重新用 fetch 关联：
git fetch <remote_name> <remote_branch>:<local_branch>

注：这个方法也不适用于 git-svn 库，会报告 
fatal: Cannot setup tracking information; starting point 'upstream/foo' is not a branch.
见“checkout”。

* 新建分支
git branch name
允许”/“出现在分支名中。例如
git branch topic/wip

git checkout -b 也可创建分支，并可以指定关联远程分支，详见“checkout”。

* 删除本地分支
git branch -d name //必须是已经合并的分支
git branch -D name //删除任何分支

删除远程分支的方法见 push 命令。


## log 列出提交历史
例子
git log
git log origin/experimental

## diff
git diff origin/HEAD
git diff origin/experimental^ other/some_branch~5

## checkout
### 检出/切换分支
git checkout branch_name
对于远程分支，branch_name 采取 remoteName/branchName 的形式。不过，这样检出远程分支后，没有对应的本地分支（要关联本地分支，用-b，见下面）。

### 检出特定的提交
git checkout <hash_of_commit>

详见“指定一个提交”。

### 新建分支，关联远程分支
git checkout -b new_branch
基于当前的分支创建一个新分支，并检出它。

如果给出的分支名不存在，且正好出现在某个远程库中，则还会自动创建关联（这并不可靠，git 2.5.1 不会关联）。

一个稍显怪异的行为是：如果 new_branch 的名字是 remoteName/branchName 的形式，则会创建一个名叫 remoteName/branchName 的本地分支，应避免这样做。

手工创建关联也是可以的，如果有多个同名远程分支，则必须指定关联了：
git checkout -b <branch> --track <remote>/<branch>
或省略 --track：
git checkout -b <branch> <remote>/<branch>

或者省略本地分支名
git checkout --track <remote>/<branch>

注：这个方法也不适用于 git-svn 库[12]，会报告 
fatal: Cannot setup tracking information; starting point '<remote>/<branch>' is not a branch.
解决办法是不用 --track，而是
git checkout -b <local_branch> <remote>/<branch>
当然，<remote>/<branch> 要先用 git svn fetch 下载到本地。

### 检出部分文件/放弃本地修改
没有像svn revert那样的直接的命令，可以使用
git checkout path_to_file_or_dir
git checkout -- path_to_file_or_dir
覆盖本地修改。
注意前一种写法与 git checkout branch_name 有二义性，所以最好用第二种写法。

## merge
git merge branchName
将某个分支合并到当前分支。
远程分支以 remotes/remoteName/branchName 命名。

## rebase
### rebase 本地分支
操作前：
     c4(C)
    /
c1-c2(A)-c3(B)

在分支 C 上操作：
rebase B
则结果变成：

            c4(C)
           /
c1-c2(A)-c3(B)

### 反向 rebase
如果想逆转上面的 rebase，可以在分支 C 上操作：

rebase -i --onto A SHA1-C^

SHA1-C^ 是要移动的分支 C 起点的 id，上面的例子中就是c3

### rebase 可能会“忽略”一些提交

如果当前分支（C）中的一些提交的内容与基础分支（B）中的一些提交的内容完全相同（不考虑提交日志），则这样的提交可能会被忽略。也就是说，rebase之后，这样的提交就消失了。
虽然这种行为基本上不会丢失信息（除了提交日志），但还是可能给用户带来惊吓，所以在这里指出来。消失的提交仍然可以用 git reflog 命令找回。

## 移动和删除文件
git rm file
git mv file newfile
实际上git使用智能的算法检测文件重命名，git mv仅仅等效于git rm 和 git add。

## stash
暂存未提交的修改：saves your local modifications away and reverts the working directory to match the HEAD commit。
git stash topic_XY
适于修改到中间时，临时要去做另外一项修改。

git stash list 列出已经暂存的修改

git stash apply topic_XY 恢复已经暂存的修改。这时可能发生冲突，需要手工解决。

git stash drop topic_XY 删除暂存的修改。

git stash pop name 与 apply 相似，但会在结束后删除暂存。

如果 stash pop 遇到冲突，git 会把冲突写入相应的文件中，用户可以自行编辑冲突。编辑完冲突，用 git reset 告诉 git 冲突已经解决，然后用 git stash drop 丢弃当前的 stash 。

## pull
实际上是 fetch 和 merge 的组合（除非用 --rebase 参数）。
如果只有一个远程仓库，可简单执行
git pull

如果想要从其它非默认远程仓库获取更新，先确保切换到默认的本地分支，然后执行：
git pull remoteName remoteBranchName

* 下载全部分支
基本的clone 和 pull只会下载默认分支（例如master），要下载全部，可以
git pull --all // 似乎不行

## squash: 将待合并的多个提交压缩为一个提交进行合并
git merge --squash 可以将待合并的多个提交压缩为一个提交进行合并，它也保证会产生线性的历史。
例如：
```
A-B <- main *
 \
 C-D <- dev
```
我们现在位于 main 分支，想要把 dev 分支合并过来，并且把 C、D 两次提交压缩为一次提交 E，也就是达到这样的效果：
```
A-B-E <- main *
 \
 C-D <- dev
```
我们可以这样做：
```
git checkout main
git merge --squash dev
```
输出：
```
自动合并 xxx（文件）
压缩提交 -- 未更新 HEAD
自动合并进展顺利，按要求在提交前停止
```
这时 git 会把 C + D 的内容合并起来添加到 main 的提交缓存中，我们可以修改提交日志后提交，就得到了 E。

## reset
* 针对index，取消待提交（add）的文件
git reset HEAD path_to_file_or_dir
不带参数的形式，git reset，取消所有index的文件。

* 针对提交，soft/hard
git reset [--hard] COMMITHASH
没有 --hard 时，index和工作副本不会被修改，否则也会被修改。
省略 COMMITHASH 时，默认是 HEAD，实际效果是丢弃工作副本和index的未提交修改。

参考 http://www.eqqon.com/index.php/Collaborative_Github_Workflow

Soft Reset

A soft reset rewinds the HEAD back in history without discarding changes. Every change of a commit that has been reset softly can be committed again in order to rewrite the history differently. Identify the COMMITHASH right before things went wrong and type

    git reset COMMITHASH

Hard Reset

A hard reset rewinds the HEAD back in history and discards any changes of the resetted commits. Identify the COMMITHASH right before things went wrong and type

    git reset --hard COMMITHASH

So as you see, there are lots of possibilities to fix errors in local unpublished changes. Cool, isn't it? 

## 找回丢失的HEAD
要撤销上次的 soft/hard reset，可以用特殊的 ORIG_HEAD 名字，这是上次 reset 之前的 HEAD。
git reset ORIG_HEAD
或者给出提交的hash：
git reset 1b6d

git reflog 可以列出历史上对HEAD的一些变更。

## revert
撤销已提交的更改。注意这与svn revert完全不同。
git revert <commits>
这会创建一个新的提交来反转指定的提交；如果跟上-n参数，则只修改工作副本。

EXAMPLES
       git revert HEAD~3
           Revert the changes specified by the fourth last commit in HEAD and create a new commit with the reverted changes.

       git revert -n master~5..master~2
           Revert the changes done by commits from the fifth last commit in master (included) to the third last commit in master (included), but do not create any
           commit with the reverted changes. The revert only modifies the working tree and the index.

## 删除最近的提交
revert 实际上创建了新的提交。如果想真正删除最近的提交，可以切换到该分支，然后
|git reset --hard HEAD~1
1表示后退一个提交
或者指定提交的id
|git reset --hard <sha1-commit-id>
该提交之后的提交会被丢弃。

注意 reset --hard 的副作用是删除所有未提交的改动，因此需要先做好备份，例如stash。

如果被删除的提交已经上传到服务器，那么下次push也会从服务器上删除该提交。如果该提交已经被别人合并，则会引起问题。

## submodule

以下是关于 git submodule 命令用法的 Markdown 格式内容：
Git Submodule 命令用法
git submodule 是 Git 提供的一个功能，用于将一个独立的 Git 仓库嵌入到另一个 Git 仓库中。这在处理依赖关系（如第三方库）或管理多个相关项目时非常有用。以下是 git submodule 的常用命令及其用法。
1. 添加子模块（Submodule）
将一个外部仓库作为子模块添加到当前项目中。
```
git submodule add <repository> [path]
```
<repository>：子模块的 Git 仓库地址。
[path]：可选参数，指定子模块在当前项目中的路径。如果不指定，默认使用仓库的名称。
示例：
```
git submodule add https://github.com/example/repo.git path/to/submodule
```
这会将 https://github.com/example/repo.git 添加到当前项目的 path/to/submodule 目录中，并记录子模块的 URL 和当前提交的版本。
2. 初始化和更新子模块
在克隆包含子模块的仓库时，需要初始化并更新子模块。

```
git submodule init
git submodule update
```
git submodule init：初始化子模块配置，从 .gitmodules 文件中读取子模块信息。
git submodule update：根据 .gitmodules 文件和父项目的记录，克隆并更新子模块到指定的版本。
快捷方式：
```
git submodule update --init --recursive
git submodule update --init --recursive --depth 1
```
--init：初始化子模块。
--recursive：递归初始化和更新嵌套的子模块。
1. 更新子模块到最新版本
将子模块更新到最新的提交版本。
```
git submodule update --remote
```
--remote：从远程仓库拉取最新的提交，并更新子模块。
注意： 更新后需要提交父项目，以记录新的子模块版本。
1. 同步子模块的 URL
如果子模块的远程仓库地址发生变化，需要同步 .gitmodules 文件中的 URL。
```
git submodule sync
```
`git submodule sync --recursive`：递归同步嵌套子模块的 URL。
1. 查看子模块的状态
查看子模块的当前状态，包括是否有未提交的更改或是否需要更新。
```
git submodule status
```
如果子模块有未提交的更改，会显示为 +。
如果子模块有未推送的更改，会显示为 -。
1. 提交子模块的更改
在父项目中记录子模块的版本更改。
进入子模块目录：
```
cd path/to/submodule
```
在子模块中进行更改并提交：
```
git add .
git commit -m "Update submodule"
git push
```
返回父项目目录：
```
cd ..
```
在父项目中更新子模块的版本：
```
git add path/to/submodule
git commit -m "Update submodule version"
git push
```
1. 删除子模块
如果需要从项目中移除子模块，可以使用以下步骤：
删除子模块的目录：
```
rm -rf path/to/submodule
```
从 .gitmodules 文件中移除子模块的配置：
```
git config -f .gitmodules --remove-section submodule.path/to/submodule
```
从 .git/config 文件中移除子模块的配置：
```
git config --remove-section submodule.path/to/submodule
```
提交更改：
```
git add .gitmodules
git commit -m "Remove submodule"
git push
```
1. 其他常用选项
--depth：在添加子模块时，指定克隆的深度（浅克隆），节省时间和空间。
```
git submodule add --depth 1 <repository> [path]
```
--branch：指定子模块的分支或标签。
```
git submodule add -b <branch> <repository> [path]
```
--force：强制更新子模块，即使目标目录已存在。
```
git submodule update --force
```

## SSH
https://help.github.com/articles/generating-ssh-keys

## workflow
* rebase based
http://stackoverflow.com/questions/457927/git-workflow-and-rebase-vs-merge-questions

The reason why a rebase is then better than a merge is that:

    you rewrite your local commit history with the one of the master (and then reapply your work, resolving any conflict then)
    the final merge will certainly be a "fast forward" one, because it will have all the commit history of the master, plus only your changes to reapply.

I confirm that the correct workflow in that case (evolutions on common set of files) is rebase first, then merge.

However, that means that, if you push your local branch (for backup reason), that branch should not be pulled (or at least used) by anyone else (since the commit history will be rewritten by the successive rebase).

command:
git pull --rebase

* Collaboration on Github
http://www.eqqon.com/index.php/Collaborative_Github_Workflow

## submodule
http://blog.jacius.info/git-submodule-cheat-sheet/
http://www.git-scm.com/book/zh/Git-工具-子模块
http://www.git-scm.com/book/en/Git-Tools-Submodules

如果你的项目非常大，包含很多不相关的文件，而且正在不断改变，Git可能比其他系统 更不管用，因为独立的文件是不被跟踪的。Git跟踪整个项目的变更，这通常才是有益的。
一个方案是将你的项目拆成小块，每个都由相关文件组成。如果你仍然希望在同一个资源库里保存所有内容的话，可以使用 git submodule 。

## git svn
* 参考
如何在svn系统中使用git
  http://www.robinlu.com/blog/archives/194

* clone
git svn clone -s <svn-repository-url>
-s（--stdlayout）的意思是SVN库采用标准布局，即trunk,tags,branches。如果不是这样，可以用 --trunk, -T 或者 --branches, 指定这些路径。--branches 和 --trunk 后面跟上相对路径（这意味着branches、trunk 必须是 svn-repository-url 的子目录）。

clone 相当于 init 加上 fetch，上述参数也适用于 init。例如：
git svn init --trunk=MBEditor.js --branches=MBEditor.js_branches svn://192.168.0.204/UCBook3.0/src/EduEditor
git svn fetch

如果不采用trunk,tags,branches布局，也可以不带任何参数。

完成以后，git branch -a 显示类似这样：
* master
  remotes/git-svn
  
git remote -v 则无显示

* rebase
git svn rebase
用于从svn服务器下载更新，类似 svn update。
但与 svn 不同，rebase要求没有未提交的更改。如果有，可以用 git stash 来处理。

如果在git svn rebase时发生代码冲突，需要先手动解决冲突，然后用git add将修改加入索引，然后继续rebase

git svn rebase --continue

注意，git svn rebase 会重写本地 git 历史（与git rebase一样）。因此追踪svn的分支不能作为一般的git协作分支，否则会与其他人的本地分支冲突。
解决方案见下面。

* commit
git commit
本地提交，与git没什么不同。

* dcommit
git svn dcommit [branch_or_commit]
提交到svn服务器。不带参数时，提交当前git分支。参数可以是git分支名字或提交的id。每个本地提交对应一个svn提交。
这个命令还会在提交后执行rebase或reset，除非给出 --no-rebase。


* svn log
git svn log
显示svn提交日志。

* branch
如果 git svn clone 或 init 的时候指定了分支目录，则可以用 git svn branch <分支名> 的命令在 svn 服务器上创建分支。
此外，如果本地 git 库落后于 svn 服务器，则创建分支的点是服务器上的最新提交，而不是本地的最新提交。

注意这并不会立即在本地 git 库中创建跟踪分支，仍然需要用 git checkout -b 新建，见下面。

对于 svn 上的主分支以外的分支，可先用 git svn fetch 下载，用 git branch -a -v 查看，然后用
git checkout -b <local_branch> <remote>/<branch>
创建和关联本地分支。<remote> 默认是 "origin"。注意这里不能用 --track 选项[12]，否则会出现错误：
fatal: Cannot setup tracking information; starting point '<remote>/<branch>' is not a branch.

* git svn 环境下的本地分支管理策略
git svn 要求追踪svn的本地分支具有线性的历史，即每个提交最多只有一个祖先。但是，git中常见的非平凡分支合并（即非fast-forward的合并）会造成一个有多个祖先的提交（即合并提交）。这种多祖先的提交在dcommit的时以及之后会造成很多问题，所以应该避免。

解决办法包括：
1. 合并后即删除临时分支。
追踪svn的本地分支仍然可以合并其他临时分支，但是一旦合并，就删除临时分支，这样在dcommit后，对追踪分支的历史重写不会影响临时分支。

参考：
http://stackoverflow.com/questions/190431/is-git-svn-dcommit-after-merging-in-git-dangerous
Simple solution: Remove 'work' branch after merging

Short answer: You can use git however you like (see below for a simple workflow), including merge. Just make sure follow each 'git merge work' with 'git branch -d work' to delete the temporary work branch.

Background explanation: The merge/dcommit problem is that whenever you 'git svn dcommit' a branch, the merge history of that branch is 'flattened': git forgets about all merge operations that went into this branch: Just the file contents is preserved, but the fact that this content (partially) came from a specific other branch is lost. 

http://stackoverflow.com/questions/2945842/using-git-svn-or-similar-just-to-help-out-with-an-svn-merge
Another excellent source is the Pro Git book, section 'Switching Active Branches' basically says that the merge does work, but dcommit will only store the content of the merge, but the history will be compromised (which breaks subsequent merges), so you should drop the work branch after merge. 

2. git merge --squash
参考 squash 一节。
```
git checkout -b dcommit_helper_for_svnbranch  svnbranch
git merge --squash huge_merge_work_with_messy_nonlinear_history
git commit 'nice merge summary' # single parent, straight from the fresh svnbranch
git dcommit
```
3. subgit
http://www.subgit.com/documentation.html

* git svn 环境下的远程分支管理策略
如果一个代码仓库同时有远程的svn和git中心仓库，并以svn为准，一部分开发者用git svn，一部分纯用git，如何管理？
假定追踪svn的git分支都叫做master。

1. 纯git使用者不能直接提交到本地或远程master分支。只能从本地基于master建立临时分支，推送，然后等待git svn使用者代为提交到svn中。

2. git svn 使用者可以直接提交到本地master分支，但是在提交到svn库之前（即dcommit之前），不能把本地master分支推送到远程git库。
  这是因为在dcommit之前本地master分支并未冻结。
  同纯git使用者一样，可以（而且应该）使用临时分支，也可以把临时分支推送到远程git中供其他人批阅。
  将本地临时分支合并到本地master分支后，应删除本地临时分支，避免将其推送到远程git仓库。

* 将git仓库导入svn仓库
http://stackoverflow.com/questions/661018/pushing-an-existing-git-repository-to-svn

## 解决冲突/conflict
merge, rebase, svn rebase, stash pop 等操作都可能造成冲突。

* 对于 merge 冲突
可以做两件事：
（1）撤消导致冲突的合并：git merge --abort
（2）解决冲突：修改冲突的文件（冲突部分在工作副本中标记出来了），然后 git add 冲突的文件，git commit。

解决冲突时，可以使用以下工具：
git mergetool
git diff
git log --merge -p <path>
git show :1:filename shows the common ancestor, git show :2:filename shows the HEAD version, and git show :3:filename shows the MERGE_HEAD version

解决冲突时，可以单纯使用冲突一方的修改：
git checkout --ours filename.c
git checkout --theirs filename.c
git add filename.c
git commit -m "using theirs"

* 对于 ours|theirs 的解释
ours 是指 HEAD 指针指向的分支（即当前分支），theirs 是指另外的分支。
注意，对于 svn rebase，由于它本质上是 rebase，所以操作时会先切换到跟踪远程svn的分支上，这样 ours 是指远程分支，theirs 是指本地分支。

参考：
http://stackoverflow.com/questions/161813/fix-merge-conflicts-in-git
http://stackoverflow.com/questions/2959443/why-is-the-meaning-of-ours-and-theirs-reversed-with-git-svn

## 清理错误提交的文件
https://rtyley.github.io/bfg-repo-cleaner/
BFG Repo-Cleaner
Removes large or troublesome blobs like git-filter-branch does, but faster. And written in Scala

## 修改以前的提交
### 分支/cherry pick 法
假定要修改 master 分支中的某个以前的提交。
1. 在 master 的 head 上新建一个分支 b1
2. 将 master 的 head 重置到要修改的提交上
3. 用 amend 模式修改 master 的 head
4. 从 b1 中 cherry pick 一个提交
5. 如果要修改4 中cherry pick的提交，重复3-4
6. 删除 b1

### rebase -i
git rebase -i @~3 # 或者 HEAD~3

-i 是 --interactive，3是从head往回数第三个提交。接下来git会用vi打开一个临时文件，里面列出了从head往回数的三个提交（旧的排在前面）。
每一行前面是命令，保持原样就是 pick，要修改提交日志就是 r，要修改文件就是 e，要合并到前面就是s（squash）。修改和保存这个文件后git会再用vi打开提交日志
或文件。

## diff/patch
git apply <patch_file_path>
git diff > <patch_file_path>

## 找回删除了的或被丢弃的提交
git reflog
可以看到最近删除了的或者在 rebase 中被丢弃的提交，记下id。
用 git checkout <id> 可以将它检出，然后新建分支将其保存。
或者 git merge <id> 可以将其直接合并。

## 清除
git clean -d -x -f

This removes untracked files, including directories (-d) and files ignored by git (-x).

## 强制 merge 覆盖文件

错误：“The following untracked working tree files would be overwritten by merge”
是因为当前分支未跟踪的文件在源分支中被跟踪了。解决[14]：

git fetch --all
git reset --hard origin/{{your branch name}}

其它方法是（1）手工删除这些文件（2）用 git clean -d -x -f 清除这些文件。

## github
### 查找特定的提交（commit）
如果已经知道特定的提交的hash，可以这样查看其信息：
在仓库主页面上点击commits，点击任意一个提交，然后把浏览器地址栏上url最后部分的提交hash替换成你想要的，前进。

### gh-pages
project pages将通过子路径的形式提供服务username.github.com/projectname；

    $ git clone https://github.com/USERNAME/PROJECT.git PROJECT

    $ git checkout --orphan gh-pages

    $ git rm -rf .

    $ git add .

    $ git commit -a -m "First pages commit"

    $ git push origin gh-pages

### 检出 pull request
参考[8]：
git fetch origin pull/<pull_request_id>/head:<local_branch_name>

## .gitignore
[13]

## 记住密码
git config --global credential.helper store

## 显示名
git config --global user.email "duanyao@ustc.edu"
git config --global user.name "Duan Yao"

## 中文显示问题

windows 上的字符编码 encoding 设置（影响git gui）：
```
git config --global gui.encoding utf-8
```

命令行（如git log）的设置见[16]。

控制台中避免中文路径转义（中文显示为形如`\351\233\206\357\`）：

```
git config --global core.quotepath false
```

## windows 上的文件权限设置
因为 windows 文件系统不支持 unix 文件权限，所以应该让 git 忽略权限改变：
```
git config --global core.filemode false # 全局
git config core.filemode false # 本仓库
```

要取消设置：

```
git config --unset core.filemode
```

## 导出仓库为归档，archive
[19]
```
git archive -o ../cvat-1.7.0.tgz github-cvat-ai/release-1.7.0
git archive -o ../cvat-2.16.1.tgz v2.16.1
# 打包master下的mydir mydir2目录
git archive --format tar.gz --output "./output.tar.gz" master mydir mydir2
```
