[1.1] https://en.wikipedia.org/wiki/ISO_8601
[2.1] https://github.com/datejs/Datejs
[2.2] https://tc39.github.io/ecma402/
[2.3] https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/DateTimeFormat/prototype
https://stackoverflow.com/questions/1091372/getting-the-clients-timezone-offset-in-javascript

[3.1] Change date format in a Java string
  https://stackoverflow.com/questions/4772425/change-date-format-in-a-java-string

## 不同的时间格式

### ISO 8601
[1.1,2.1]
格式是：
```
YYYY-MM-DDThh:mm:ss.iii{+|-}hh:mm`
YYYY-MM-DDThh:mm:ss.iii{+|-}hhmm`
YYYY-MM-DDThh:mm:ss.iiiZ`
```
例子：
```
1997-07-16T00:00:00.123+01:00
1997-07-16T00:00:00.123+0100
1997-07-16T00:00Z
1997-07-16T00:00-08:00
1997-07-16
```

此格式中，`{+|-}hh:mm` 和 `{+|-}hhmm` 是时区偏移量，例如中国是 `+08:00` 表示领先 UTC 时间8小时；时区偏移量的分钟部分不能省略，`+` 不能省略填补的 0 也不能省略，也不能增加秒的部分；Z表示 `+00:00` 时区。

`Thh:mm:ss.iii` 这部分中，秒以下可以省略，小时和分钟不能省略；`.iii` 是秒的小数部分，位数不限，可以省略；`T` 后面整体可以省略。

月日部分的补零不能省略，必须是两位数。

### GMT 格式
GMT 格式不是正式名称。格式是

```
YYYY-MM-DD hh:mm:ss.iii GMT{+|-}hh:mm
YYYY-MM-DD hh:mm:ss.iii GMT{+|-}hhmm
YYYY-MM-DD hh:mm:ss.iii GMT{+|-}hh
YYYY-MM-DD GMT{+|-}hhmm
YYYY-MM-DDGMT{+|-}hhmm
```
例子：

```
1997-07-16 00:00:00.1 GMT+01:00
1997-07-16 00:00:00.1 GMT+0100
1997-07-16 00:00:00.1 GMT+1
1997-07-16 00:00 GMT+1
1997-07-16 GMT+1
1997-07-16GMT+1
1997-7-16GMT+1
1997-7-16
1997-07-16 GMT
```

此格式中，`GMT+` 是时区偏移量；时区可以省略分钟部分；只有小时部分时，补零也可省略；单独的 `GMT` 或省略时区，表示 `GMT+0`。

日期、时分秒、时区三个部分用空格分开；如果省略时分秒，则日期和时区之间可以没有空格，但时分秒和时区之间必须有空格。

月日的补零可以省略。

## 不同语言、库的时间操作

### javascript
#### ISO 8601 格式
生成：

#### GMT 格式
[2.3]

```
> var dd = new Date('2019-02-21 GMT+8');
> var df=new Intl.DateTimeFormat('zh-CN', {timeZone:'Asia/Shanghai', month:"2-digit", day:"2-digit", year:"numeric", hour:"2-digit", minute:"2-digit", timeZoneName:"short"})
> df.format(dd)
'2019-02-21 02:00 GMT+8'
> var df=new Intl.DateTimeFormat('zh-CN', {timeZone:'Asia/Shanghai', month:"2-digit", day:"2-digit", year:"numeric", timeZoneName:"short"})
> df.format(dd)
'2019-02-21 GMT+8'
```

#### 日期运算
加减天数：

```
> var dd =new Date('2019-02-21 GMT')
undefined
> dd.getDate()
21
> dd.setDate(dd.getDate() + 10)
1551571200000
> dd
2019-03-03T00:00:00.000Z
```
setDate 的参数大于一个月的天数也没关系，会自动调整。

### Java
[3.1]
```
> var DateFormat = Java.type('java.text.DateFormat');
> var df = DateFormat.getDateInstance();
> var date = df.parse('2019-02-21GMT+8');
> date
Thu Feb 21 00:00:00 CST 2019
> df.format(date)
2019-2-21
```

### SQL
MySQL 中的时间似乎不包括时区信息，时间总是在机器时区中解释。日期字面量中写时区部分不算错误，但是会被忽略，例如 `where time > '2019-01-08 GMT+1'`。

