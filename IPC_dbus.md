IPC

## 总论
## # 参考资料
Interprocess Communications
http://msdn.microsoft.com/en-us/library/aa365574%28VS.85%29.aspx
The following IPC mechanisms are supported by Windows:

    Clipboard
    COM
    Data Copy
    DDE
    File Mapping
    Mailslots
    Pipes
    RPC
    Windows Sockets

    


## dbus

### 参考资料
D-Bus Specification
http://dbus.freedesktop.org/doc/dbus-specification.html

dbus实例讲解（一）：初次见面
https://blog.csdn.net/gjsisi/article/details/7671267

Windows下，dbus的hello world
http://blog.csdn.net/chenyufei1013/article/details/6573411

基于DBus的进程间通信(IPC)
http://sy198704.is-programmer.com/posts/33060.html

UcWORK统一桌面系统
http://www.qqtech.com/ucwork.htm

dbus-monitor
https://linux.die.net/man/1/dbus-monitor

### 概述
http://en.wikipedia.org/wiki/D-Bus

D-Bus has three architectural layers:[4]

    *libdbus - a library that allows two applications to connect to each other and exchange messages; in 2013 the systemd project rewrote libdbus in an effort to simplify the code, but it turned out to significantly increase the performance of D-Bus as well. In preliminary benchmarks, BMW found that the systemd D-Bus library increased performance by 360%.[5]
    
    *dbus-daemon - a message-bus daemon executable, built on libdbus, that multiple applications can connect to. The daemon can route messages from one application to zero or more applications, thereby implementing the publish/subscribe paradigm.
    
    *wrapper libraries based on particular application frameworks


### 调试工具
dbus-monitor 监视消息传递。
d-feet 显示注册的接口。
dbus-send、gdbus 发送消息。

```
dbus-send --system --print-reply --dest=org.freedesktop.DBus  /org/freedesktop/DBus org.freedesktop.DBus.ListNames

dbus-send --session --print-reply --dest=org.freedesktop.Notifications /org/freedesktop/Notifications org.freedesktop.Notifications.GetCapabilities 

gdbus call --session --dest=com.deepin.dde.Notification --object-path /com/deepin/dde/Notification  --method com.deepin.dde.Notification.GetCapabilities

gdbus call --session --dest=com.deepin.dde.Notification --object-path /com/deepin/dde/Notification  --method com.deepin.dde.Notification.ClearRecords

gdbus call --system --dest org.freedesktop.testamundo --object-path /org/freedesktop/testamundo --method org.freedesktop.DBus.Introspectable. Introspect

gdbus call --session --dest org.freedesktop.Notifications --object-path /org/freedesktop/Notifications --method org.freedesktop.DBus.Introspectable.Introspect

gdbus call --session --dest com.deepin.dde.freedesktop.Notification --object-path /org/freedesktop/Notifications --method org.freedesktop.DBus.Introspectable.Introspect

gdbus call --session --dest com.deepin.dde.Notification --object-path /com/deepin/dde/Notification --method org.freedesktop.DBus.Introspectable.Introspect
```

### dbus 激活机制

dbus 根据目录 /usr/share/dbus-1/services/ 下的 .service 文件激活还没有启动的进程。例如：

```
/usr/share/dbus-1/services/com.deepin.dde.freedesktop.Notification.service
/usr/share/dbus-1/services/org.kde.plasma.Notifications.service
```

## Dynamic Data Exchange (DDE)
## # 参考资料
Dynamic Data Exchange
http://en.wikipedia.org/wiki/Dynamic_Data_Exchange

About Dynamic Data Exchange
http://msdn.microsoft.com/en-us/library/ms648774.aspx

## # 概述
http://en.wikipedia.org/wiki/Dynamic_Data_Exchange
Dynamic Data Exchange was first introduced in 1987 with the release of Windows 2.0 as a method of interprocess communication so that one program can communicate with or control another program, somewhat like Sun's RPC (Remote Procedure Call).

其机制是基于 windows 消息的，一个对话的两端分别是两个应用程序中的各一个窗口（可以是隐藏窗口）。
发起对话的应用是客户端，另一端是服务端。
数据格式采用剪贴板的格式。

## OLE for Process Control
## # 参考资料
OLE for Process Control
http://en.wikipedia.org/wiki/OLE_for_process_control

# # 概述
OLE for Process Control (过程控制？)
http://en.wikipedia.org/wiki/OLE_for_process_control
The OPC Specification was based on the OLE, COM, and DCOM technologies developed by Microsoft for the Microsoft Windows operating system family. The specification defined a standard set of objects, interfaces and methods for use in process control and manufacturing automation applications to facilitate interoperability. The most common OPC specification is OPC Data Access, which is used to read and write real-time data. 

## WCF
