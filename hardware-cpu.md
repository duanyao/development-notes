## 参考资料

[2.1] Cycles per instruction
  https://en.wikipedia.org/wiki/Cycles_per_instruction
  
[5.1] Instruction tables
  https://www.agner.org/optimize/instruction_tables.pdf
  
[6.1] SIMD Instructions Considered Harmful
  https://www.sigarch.org/simd-instructions-considered-harmful/

[6.3] Introduction to Intel® Advanced Vector Extensions 
 https://software.intel.com/content/www/us/en/develop/articles/introduction-to-intel-advanced-vector-extensions.html
 
[6.4] Intel® Intrinsics Guide is a reference tool for Intel intrinsics
 https://software.intel.com/sites/landingpage/IntrinsicsGuide/
 
[6.5] Floating-Point Operations Per Second (FLOPS) 
 https://en.wikichip.org/wiki/flops

[6.6] What is FLOP/s and is it a good measure of performance?
 https://stackoverflow.com/questions/329174/what-is-flop-s-and-is-it-a-good-measure-of-performance

[6.7] FLOPS
 https://en.wikipedia.org/wiki/FLOPS
 
[6.8] BogoMIPS
 https://www.linux-mips.org/wiki/BogoMIPS
 
[6.9] BogoMIPS Calculator
 https://djwong.org/programs/bogomips/

[6.10] BogoMips mini-Howto
 https://tldp.org/HOWTO/BogoMips/

[7.1] 同步多线程（SMT）
 https://baike.baidu.com/item/%E5%90%8C%E6%AD%A5%E5%A4%9A%E7%BA%BF%E7%A8%8B/142126

## CPU 信息和指令集

在linux可查看 /proc/cpuinfo 文件。其中 CPU 支持的指令集，可以看 flags 字段，如 sse2 avx 等。

不同x86 CPU 的指令集、延迟、吞吐量等信息可以查看[5.1]。

## DTS
集成数字温度传感器（Digital Thermal Sensor，简称DTS）
在 AMI BIOS 的 advanced->cpu thermal configuration->DTS 中可以开启或关闭。

## CPI，IPC，流水线、亚、超标量，指令级并行

Cycles per instruction 也可称作 clock cycles per instruction, clocks per instruction ，即“每条指令所用的时钟周期数”。
其倒数为 Instructions per cycle，IPC，每钟周期的指令数。

典型的指令执行过程分为5个左右的阶段：

Instruction fetch cycle (IF).
Instruction decode/Register fetch cycle (ID).
Execution/Effective address cycle (EX).
Memory access (MEM).
Write-back cycle (WB).

如果每个阶段都需要一个时钟周期，且下一条指令指令只能在上一条指令完全结束后才能开始，则平均每条指令使用5个时钟周期，CPI是5。CPI大于1的CPU属于“亚标量（subscalar）CPU“。
如果采用5级流水线，每个时钟周期都可以获取一条指令，让连续的5条指令分别位于5个不同的阶段，则平均每条指令使用1个时钟周期，CPI是1。CPI等于1的CPU属于“标量（scalar）CPU“。
如果进一步采用多个执行单元，则平均每条指令使用小于1个时钟周期，CPI小于1。CPI小于1的CPU属于“超标量（scalar）CPU“。

对于多核心 CPU，计算 CPI 时只考虑一个核心。

对于同时多线程/超线程（Simultaneous Multithreading, SMT） CPU[7.1]，如何计算 CPI 则不太明确。理论上SMT并不会增加理论CPI，但可以增加实际CPI。

不同的指令需要的执行阶段数可以不相，所以同一CPU执行不同指令的CPI可以不同。

CPI 事实上有理论和实际的区别。如果指令需要等待访问内存，则在等待期间流水线可能堵塞（stall），造成实际 CPI 上升。分支预测错误也可导致实际 CPI 上升。不考虑这些不利因素时的 CPI 为理论 CPI。

CPI 除了可以表征 CPU，也可以表征程序。

## BogoMIPS
BogoMIPS 是一种测量 CPU 的 IPS（instruction per second）的标准。BogoMIPS 的测量仅在一个简单循环中使用加减指令，所以不反映 CPU 的真实性能，而是基本与其时钟频率成正比。[6.8]

[6.9] 提供了 BogoMIPS 的测试代码。

## 向量指令：SIMD、vecter

SIMD (Single Instruction Multiple Data).
RISC-V 没有采用类似 x86、ARM 等的短向量 SIMD 指令，而是任意长度向量指令 [6.1]。

关于 x86 和 ARM CPU 的理论 SIMD 浮点性能，可以查看[6.5]。例如 Intel Skylake 架构（第6代 core），每个核心有 2个 256-bit fused-multiply add (FMA) 执行单元（EU），
每个单元每个周期执行 16 FLOPs 单精度，或 8 FLOPs 双精度，因此总计每个周期 32 FLOPs(单精度) / 16 FLOPs(双精度) [6.5,6.7]。
FMA 意味着一条指令最多2个计算操作，所以一个 EU 一个周期算作2次计算；不过，很多人将一条 FMA 指令看作两条指令，例如 linux perf 程序统计的指令数。
之后支持 AVX 512 的 CPU 的 EU 位宽会扩大到 512bit，所以 FLOPs 会翻倍。

例如，对于 2.5GHz 的 Skylake 双核PC用 CPU，总的理论单精度浮点性能为 2.5 * 32 * 2 = 160 GFLOPs 。

2.5GHz 的 Xeon Platinum 服务器用 CPU（Skylake-SP 架构），每个核心有 2 个 AVX-512 FMA 执行单元，每个核心的理论单精度浮点性能为 2.5 * 64 = 160 GFLOPs 。
Xeon Platinum 8163 为 24 核，总计 3.84 TFLOPS 。

不同的SIMD指令的CPI可以不同，对于x86，可以查阅[6.4]。

