## 参考资料

【时序数据库】应用场景及使用
https://blog.csdn.net/baidu_39511645/article/details/80265435

时序数据库场景下的Elasticsearch（一）：技术特点简介
https://blog.csdn.net/weixin_40674835/article/details/79085914

异常检测之指数平滑（利用elasticsearch来实现）
https://blog.csdn.net/Gamer_gyt/article/details/78584071

时间序列数据的存储和计算 - 开源时序数据库解析
https://blog.csdn.net/yunqiinsight/article/details/80133762

时序数据库深入浅出之存储篇——本质LSMtree，同时 metric（比如温度）+tags 分片
https://www.cnblogs.com/bonelee/p/6928980.html

Writing a Time Series Database from Scratch
https://fabxc.org/tsdb/?spm=a2c4e.11153959.blogcont354894.15.7c623dc6yMN3Ox

时序时空数据库 TSDB
https://help.aliyun.com/product/54825.html

时序数据库的选择？
https://www.zhihu.com/question/50194483

Apache IoTDB
  https://iotdb.apache.org/#/
  https://blog.csdn.net/qiaojialin/article/details/89507353
  https://github.com/apache/incubator-iotdb

TDengine与InfluxDB对比测试
  https://www.taosdata.com/blog/2019/07/19/tdengine%e4%b8%8einfluxdb%e5%af%b9%e6%af%94%e6%b5%8b%e8%af%95/

时序列数据库武斗大会之什么是TSDB
  http://liubin.org/blog/2016/02/18/tsdb-intro/

时序列数据库武斗大会之TSDB名录 Part 1
  http://liubin.org/blog/2016/02/25/tsdb-list-part-1/

时序列数据库武斗大会之OpenTSDB篇
  http://liubin.org/blog/2016/03/05/tsdb-opentsdb/

时序列数据库武斗大会之KairosDB篇
  http://liubin.org/blog/2016/03/12/tsdb-kairosdb/

TDengine 一个开源的专为物联网、车联网、工业互联网、IT运维等设计和优化的大数据平台
  https://www.taosdata.com/cn/

Beringei A high performance, in memory time series storage engine
  https://github.com/facebookarchive/beringei
