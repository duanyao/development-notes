## 参考资料
[1.7] CMake
  https://github.com/Kitware/CMake/tree/v3.16.4

[2.1] [cmake]如何设置Debug和Release编译模式
  http://aigo.iteye.com/blog/2295102

[2.6] How can I create a .deb package with my compiled OpenCV build?
  https://unix.stackexchange.com/questions/46122/how-can-i-create-a-deb-package-with-my-compiled-opencv-build

[2.7 How to build a .deb file for CMake from source?
  https://stackoverflow.com/questions/32012811/how-to-build-a-deb-file-for-cmake-from-source
  
[3.1] Why is CMake designed so that it removes runtime path when installing
  https://stackoverflow.com/questions/32469953/why-is-cmake-designed-so-that-it-removes-runtime-path-when-installing
  
## centos
centos 7 中，cmake 包只有 2.x 版本，cmake3 包才是 3.x 版本。而且，cmake 3.x 的命令是 cmake3 。
要安装 cmake 3.x ，还需要先安装 EPEL 。

```
yum install epel-release
yum install cmake3
```

## 从源码编译安装

cmake 的官方二进制版本在 github 上，下载困难。和编译 CMake 源码[1.7]：

```
sudo apt install libssl-dev  # openssl 开发包
./bootstrap
make
sudo make install
```

或者用 checkinstall 方式：

```
sudo apt install checkinstall # 如果还没有安装 checkinstall

sudo make -n install > make-install.sh
sudo checkinstall sh make-install.sh

# 将 name 和 provides 都改为为 cmake-checkinstall 。

# 删除 checkinstall 备份文件：
sudo rm backup-*.tgz
```

用 sudo 运行 make -n install 和 checkinstall 是有必要的，否则最后 deb 中的文件所有者会不正确。

以后还可以手动安装deb：
```
sudo dpkg -i ./cmake-checkinstall_3.16.4-1_amd64.deb
```

## cmake-gui

可以命令行运行，与 cmake 命令参数一样。

## cmake 参数

cmake 配置时采用的参数存储在当前目录下的 CMakeCache.txt 文件下。

CMakeVars.txt?

## 让 make 显示具体执行的命令

```
cmake .
make VERBOSE=1

或者

cmake -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON .
make
```

## debug/release 编译模式

cmake 的惯例是用变量 `CMAKE_BUILD_TYPE` 控制 Debug/Release 编译模式：
```
cmake -DCMAKE_BUILD_TYPE=Debug
cmake -DCMAKE_BUILD_TYPE=Release 
```

此外还有 RelWithDebInfo, MinSizeRel 等编译模式。

在 CMakeLists.txt 中，则可以用 set() 写死：
```
set(CMAKE_BUILD_TYPE "Release")
```
但这会造成无法用 cmake/cmake-gui 灵活选择编译模式，所以是不推荐的。

在 CMakeLists.txt 中，可以用 `CMAKE_CXX_FLAGS_DEBUG` 和 `CMAKE_CXX_FLAGS_RELEASE` 等变量指定不通编译模式特定的编译器命令选项。

```
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS} -std=c++11 -O2 -g -ggdb")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} -std=c++11 -O2")
```

但这不是必须的，而且最好不要这样做，因为 cmake 会自动添加相关参数。

## 传递额外的编译器参数

命令行方式：
`cmake -D CMAKE_CXX_FLAGS=-Ixxx/include`

或者 gui 方式，修改 `CMAKE_CXX_FLAGS / CMAKE_C_FLAGS` 变量的值。

额外的链接参数通过以下变量表示：
    ```
    CMAKE_EXE_LINKER_FLAGS
    CMAKE_SHARED_LINKER_FLAGS
    CMAKE_MODULE_LINKER_FLAGS # 不知道 module 是啥
    STATIC_LIBRARY_FLAGS
    ```

但通过命令行传递
`cmake -D CMAKE_CXX_FLAGS=-Lxxx/lib` 似乎会出错，不知道是什么原因，但用 gui 没问题。

## 设置 RPATH 或者 RUNPATH

cmake 默认在编译阶段设置 RPATH/RUNPATH ，使得刚编译出来的程序总是能找到链接的动态库，即使它位于非标准位置。
但在执行 make install 时，cmake 默认删除 RPATH/RUNPATH 。

为了在安装时保留 RPATH/RUNPATH ，可以在 cmake 项目文件中设置

```
SET(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
```
cmake 根据以下参数决定使用 RPATH 还是 RUNPATH ：

```
SET(CMAKE_EXE_LINKER_FLAGS "-Wl,--disable-new-dtags") # RPATH
SET(CMAKE_EXE_LINKER_FLAGS "-Wl,--enable-new-dtags")  # RUNPATH
```

RUNPATH 对间接引用的共享库不起作用，因此如果间接引用的共享库在非标准位置，就得一一列出在 target_link_libraries() 和 link_directories() 中。

也可以用

```
set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
```
直接指定 RPATH/RUNPATH 。

## 安装

### 指定安装位置

`cmake -D CMAKE_INSTALL_PREFIX=/usr/local`

### 生成安装包

以 deb 为例，有3种方式：

* make 完成后，`cpack -G DEB` [2.7] 。但这种方法似乎会忽略 `CMAKE_INSTALL_PREFIX` 指定的安装位置。

* `cmake -D CPACK_BINARY_DEB=ON ...`，然后用 `make package` 命令生成 deb 包[2.6]。不是所有的 cmake 脚本里都有这样的设置。

* make 完成后，`checkinstall make install` 。这会尊重 `CMAKE_INSTALL_PREFIX` 指定的安装位置。

## 输出消息

```
message([<mode>] "message to display" ...)

(none)         = Important information
STATUS         = Incidental information
WARNING        = CMake Warning, continue processing
AUTHOR_WARNING = CMake Warning (dev), continue processing
SEND_ERROR     = CMake Error, continue processing,
                              but skip generation
FATAL_ERROR    = CMake Error, stop processing and generation
DEPRECATION    = CMake Deprecation Error or Warning if variable
                 CMAKE_ERROR_DEPRECATED or CMAKE_WARN_DEPRECATED
                 is enabled, respectively, else no message.
```

## 传递编译器宏、包含目录、其他参数

add_definitions(-DFOO -DBAR ...)


Use add_compile_definitions() to add preprocessor definitions.
Use include_directories() to add include directories.
Use add_compile_options() to add other options.
