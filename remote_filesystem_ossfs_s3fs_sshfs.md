## 参考资料
[1.1] s3fs
https://github.com/s3fs-fuse/s3fs-fuse/

[1.2] Must wait for video to copy before streaming in AMS
https://github.com/s3fs-fuse/s3fs-fuse/issues/95

[1.3] Non Amazon S3
https://github.com/s3fs-fuse/s3fs-fuse/wiki/Non-Amazon-S3

[1.4] s3fs FAQ
https://github.com/s3fs-fuse/s3fs-fuse/wiki/FAQ

[1.5] Question: Are files mirrored locally?
https://github.com/s3fs-fuse/s3fs-fuse/issues/1938

[1.6] File is downloading in 24MB chunks even with -o multipart_size=5
https://github.com/s3fs-fuse/s3fs-fuse/issues/1980

[2.1] Goofys is a high-performance, POSIX-ish Amazon S3 file system written in Go
https://github.com/kahing/goofys

[2.2] Alibaba Cloud OSS mount issues for some regions
https://github.com/kahing/goofys/issues/743

[2.3] 使用goofys挂载阿里云OSS，替代ossfs
https://github.com/kahing/goofys/wiki/%E4%BD%BF%E7%94%A8goofys%E6%8C%82%E8%BD%BD%E9%98%BF%E9%87%8C%E4%BA%91OSS%EF%BC%8C%E6%9B%BF%E4%BB%A3ossfs

[3.1] yas3fs
https://github.com/danilop/yas3fs

[3.2] Cache parts of files instead of full files
https://github.com/danilop/yas3fs/issues/109

[3.3] How can the cache be disabled on yas3fs
https://github.com/danilop/yas3fs/issues/96

[4.1] aliyun OSSFS
https://github.com/aliyun/ossfs

[4.2] aliyun 请问缓存机制是如何,本地已经存在的文件,再访问的时候看日志还是有网络请求
https://github.com/aliyun/ossfs/issues/52

[4.3] aliyun ossfs FAQ
https://github.com/aliyun/ossfs/wiki/FAQ

[5.1] OSSFS is a PyFilesystem interface to AliCloud OSS cloud storage.
https://github.com/yjcyxky/fs.ossfs

[6.1] PyFilesystem Filesystem Abstraction for Python.
https://www.pyfilesystem.org/

[7.1] s3fs Python filesystem interface for S3.
https://github.com/fsspec/s3fs

[8.1] minio
https://github.com/minio/minio

[8.2] goofys 和 minio: 在 s3 存储上使用 POSIX 标准
https://zhuanlan.zhihu.com/p/458431042

[9.1] Apache Ozone
https://ozone.apache.org/

[10.1] juicefs
https://juicefs.com/

[11.1] The Hadoop FileSystem API Definition
https://hadoop.apache.org/docs/stable/hadoop-project-dist/hadoop-common/filesystem/index.html

[11.2] Better Data Lake with Apache Hadoop® on Object Storage
https://blog.westerndigital.com/apache-hadoop-object-storage-data-lake/

[12.1] FUSE filesystem over Google Drive
https://github.com/astrada/google-drive-ocamlfuse

# 服务器端

## Apache Ozone

Apache Ozone 是分布式存储服务软件，提供兼容s3接口，以及兼容Hadoop的文件系统[9.1]。

## minio

minio 是个开源的兼容s3接口的存储服务软件[8.1-8.2]。

## Hadoop
Hadoop 提供 Hadoop FileSystem API [11.1]。
Hadoop 通过 S3A 可提供兼容s3接口 [11.1]。

## JuiceFS
[10.1]
JuiceFS 是在类s3存储服务之上搭建的 POSIX 兼容的远程文件系统。由于它将大文件分块（64M）存储，因此使用 JuiceFS 存储的大文件不应再以 s3 的 API 访问。
JuiceFS 也提供其客户端。

# 客户端

## s3fs

### 安装
sudo apt install s3fs

### 服务端兼容性
支持 s3、阿里云oss等。

```
s3fs BUCKET PATH -o url=https://oss-ap-northeast-1.aliyuncs.com -o nomixupload
```

### 缓存特性
* 硬盘缓存似乎不可关闭。但使用 `-o ensure_diskfree NNNN` `可以保证 s3fs 在硬盘空间低于指定值时不再增加缓存用量。
* 读文件时，将首先下载和缓存整个文件，或者通过multipart机制缓存一部分。缓存大小为 multipart_size * parallel_count。默认 multipart_size 为 10MB，最少 5MB；parallel_count 默认 5，所以延迟较大[1.6]。
* 缓存容量是不受限制的，建议定期清理[1.4]。
* `-o use_cache <path> `可以控制缓存目录。默认缓存目录在 `~/.s3fs` 下面。
* `max_stat_cache_size` 可以控制元数据缓存的条数，默认 10000，每条占用约4K内存。stat_cache_expire 可以控制元数据缓存的过期时间，单位是秒。

## Goofys

### 安装
下载 goofys 可执行文件( https://github.com/kahing/goofys/releases/latest/download/goofys )到可执行路径下，加上可执行权限即可：`chmod +x goofys`。

### 服务端兼容性
支持 aws s3、阿里云oss等，只要大体兼容 s3 的 API。
注意加上 --subdomain 选项，否则可能挂载失败，报告 permission denied [2.2]。

### 挂载
将 access key id 和 access key secret 写入配置文件 `~/.aws/credentials` ，例如：

```
[default]
aws_access_key_id = L..Xp
aws_secret_access_key = G...5l

[google]
aws_access_key_id = J..pn
aws_secret_access_key = G...5l

[aliyun]
aws_access_key_id = F..pu
aws_secret_access_key = G...5l
```

goofys 可以同时使用多组 access key（可以属于同一服务器或不同服务器），这被称为 profile 上面的 default、google、aliyun 都是 profile。其中 default 为默认的，挂载时不需要用 `--profile` 指明。

如果要以 root 身份挂载（如通过 /etc/fstab），则需要用 `/root/.aws/credentials`。

命令：
```
goofys --subdomain --profile aliyun --endpoint http://oss-us-east-1.aliyuncs.com --region oss-us-east-1 --uid=1000 --gid=1000 --file-mode=0666 --dir-mode=0777 --use-content-type -o allow_other oss-mybucket /data/wwwroot/example.com/test-folder
```

fstab：
```
goofys#bucket   /mnt/mountpoint      fuse     _netdev,allow_other,--profile=aliyun,--subdomain,--file-mode=0666,--dir-mode=0777,--uid=1000,--gid=1000,--use-content-type,--endpoint=http://oss-cn-beijing.aliyuncs.com    0       0
```
`--endpoint` 指定存储服务的基础url。
`--subdomain` 表示将 bucket 写作子域名，目前是必需的。
`--region` 是 `--endpoint` 的地区部分，例如 oss-us-east-1，goofys 一般可以自己推断出来，所以不是必需的。
`--use-content-type` Content-Type according to file extension and /etc/mime.types (default: off)
`--profile <profile>` 用来在 `~/.aws/credentials` 中选择 profile 。

### 缓存特性

默认无文件缓存，但文件内容可以被操作系统缓存，下次访问会变快。
有元数据缓存（不清楚是goofys还是fuse实现的），下次访问会变快。元数据缓存的寿命可以设置 `--stat-cache-ttl 120s`，默认 1m0s。
也可以开启基于文件的缓存，例如 `--cache "--free:10%:$HOME/cache"`，这要求安装了 catfs 。

在对象存储中新建文件后，需要过一段时间，挂载它的本地目录中才可以看见它；从对象存储中新建文件后，如果此文件近期被访问过，则它可以继续被访问一段时间。

kate 打开 goofys 上的文件时，切换 tab 会很慢，1秒以上，不知原因。

### 资源占用

每个挂载点对应一个 goofys 进程，其初始内存占用较小，约10M，随着访问挂载的文件系统，内存占用会增大，最多可达 220MB。

### 排查错误
syslog 或者 journald 里会有 goofys 的日志。

```
journalctl --since today | grep goofys
```

或者让 goofys 在前台运行（ -f ）：

```
goofys -f --subdomain --profile default --endpoint https://oss-cn-beijing.aliyuncs.com --uid=1000 --gid=1000 --file-mode=0666 --dir-mode=0777 --use-content-type  private-ai-parents ~/media/oss/private
```

## Yet Another S3-backed File System: yas3fs

### 缓存特性

* 可以指定最大缓存容量，用 LRU 策略淘汰缓存。
* 即使值读文件的一部分，也默认缓存整个文件[3.2]。
* 缓存无法完全关闭[3.3]。

## aliyun ossfs
[4.1]
aliyun ossfs 有元数据缓存，位于内存中，可以加速 ls 之类的操作。用 `-omax_stat_cache_size=xxx` 可以控制元数据缓存的条数，默认是1000,每条占用约4K内存。

可以使用-o kernel_cache参数让ossfs能够利用文件系统的page cache，如 果你有多台机器挂载到同一个bucket，并且要求强一致性，请不要使用此 选项。不清楚开启此选项是否可以避免缓存整个文件。

似乎没有类似s3fs的 `-o use_cache <path> ` 选项可以控制缓存目录。

## FUSE filesystem over Google Drive
[12.1]

### 安装
在 https://launchpad.net/~alessandro-strada/+archive/ubuntu/ppa/+packages 找合适的 deb 包。

```
wget https://launchpad.net/~alessandro-strada/+archive/ubuntu/ppa/+files/google-drive-ocamlfuse_0.7.27-0ubuntu1~ubuntu18.04.1_amd64.deb
sudo apt install ./google-drive-ocamlfuse_0.7.27-0ubuntu1~ubuntu18.04.1_amd64.deb
```

首次使用时，要先让 google-drive-ocamlfuse 登录到 google 帐号。在带浏览器的机器上在 GUI 终端里运行
```
google-drive-ocamlfuse
```
它会调用默认浏览器登录 google 帐号，授权给 "gdfuse"，登录成功后，终端里显示”Access token retrieved correctly“。登录状态默认可保持30天，存储在 `~/.gdfuse/` 目录下。
然后，可以挂载到目录：
```
google-drive-ocamlfuse [mountpoint]
google-drive-ocamlfuse [-label <label>] [mountpoint]
```
如果想同时使用多个 google 帐号，可以用不同的 label 来区分；否则，可省略它，默认的 label 为 ”default“，对用的配置目录为 `~/.gdfuse/default/` 。

卸载：
```
fusermount -u mountpoint
```

查错：
```
google-drive-ocamlfuse -debug mountpoint
```
以及网络日志：
```
~/.gdfuse/default/curl.log
```

清除缓存：
```
google-drive-ocamlfuse -cc
```

要在服务器端使用（无GUI），可以先在一台带GUI的机器上对 google-drive-ocamlfuse 完成登录和授权，然后将 ~/.gdfuse 目录复制到服务器上的某个用户目录下，这样服务器就也被授权登录 google drive 了。

```
rsync -rtvxP --exclude 'cache' ~/.gdfuse rd@36.138.101.83:/home/rd/
```

在服务器上启用 http 代理，以访问被屏蔽的 google drive：
```
ssh-keygen -t ed25519 -f ~/.ssh/id_ed25519 -C "rd@aiws-2"
cat /home/rd/.ssh/id_ed25519.pub
sudo apt install autossh

tmux new -s duanyao-proxy
autossh -CNg -L 23128:127.0.0.1:3128 root@8.211.168.122 -p 5432
```

安装：
```
wget -4 https://launchpad.net/~alessandro-strada/+archive/ubuntu/ppa/+files/google-drive-ocamlfuse_0.7.27-0ubuntu1~ubuntu18.04.1_amd64.deb
sudo apt install ./google-drive-ocamlfuse_0.7.27-0ubuntu1~ubuntu18.04.1_amd64.deb
```
挂载：
```
mkdir -p ~/media/gdrive
export http_proxy=http://127.0.0.1:23128;export https_proxy=http://127.0.0.1:23128;export NO_PROXY=localhost,127.0.0.1,10.0.0.0/8,172.16.0.0/12,192.168.0.0/16
google-drive-ocamlfuse ~/media/gdrive
find ~/media/gdrive
```

```
mkdir -p /media/data2/ai-dataset/MuCo
rsync -avxP /home/rd/media/gdrive/.shared/data/ /media/data2/ai-dataset/MuCo/
```
