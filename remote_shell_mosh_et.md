## 参考资料
[1.1] Mosh (mobile shell)
https://mosh.org/

[1.2] mosh - 一款替代SSH的UDP远程终端软件 
https://zhuanlan.zhihu.com/p/28414523

[1.3] mosh-with-jump: 支持跳板机的mosh客户端
https://github.com/aduong/mosh-with-jump

[1.4] mosh-via-tunnel：支持跳板机的mosh客户端
https://github.com/carltonf/mosh-via-tunnel

[2.1] Eternal Terminal
https://github.com/MisterTea/EternalTerminal

[2.2] eternal terminal
https://eternalterminal.dev/

## mosh

### 安装
```
apt install mosh # Debian 
dnf install mosh # Fedora
```
### 配置和使用
mosh 可以在广域网上使用，可以穿过 NAT。但应注意将服务器端的防火墙配置为开放 UDP 端口 60000-61000。

### mosh-with-jump
前提：

跳板机：不需要安装 mosh ，需要安装 socat ，需要额外开放部分 UDP 端口，例如 60000-61000。
目标机：需要安装 mosh ，需要对跳板机额外开放部分 UDP 端口，例如 60000-61000。
客户机：需要安装 mosh ，无需额外开放端口。

mosh-with-jump 的脚本需要做一些修改，用 getent ahosts 替换 dig 来获取跳板机的 IP 地址，因为 dig 会忽略 `/etc/hosts` 中的条目。
```
proxy_host=$(ssh -G "$proxy" | perl -anE '/^hostname (.+)/ && print "$1"')
proxy_ip=$(getent ahosts "$proxy_host")
read -a proxy_ip <<< "$proxy_ip"
proxy_ip=${proxy_ip[0]}
```

登录：
```
mosh-with-jump -J ydjmp aiws-2
```

如果出现问题，可以使用下面的 UDP echo server 脚本测试 UDP 端口是否开放。

https://github.com/rastinder/tcp_udp_port_test
ping.py
```
import socket, threading
port=60005
s_tcp, s_udp = socket.socket(socket.AF_INET, socket.SOCK_STREAM), socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s_tcp.bind(('', port)), s_udp.bind(('', port)), s_tcp.listen(5)
print(f'Server running on port {port}...')
def tcp_handler():
 while True: conn, addr = s_tcp.accept(); print(f'TCP from {addr}'); data = conn.recv(1024); print(f'TCP recv: {data.decode()}'); conn.send(data); conn.close(); print(f'TCP echo sent to {addr}')
def udp_handler():
 while True: data, addr = s_udp.recvfrom(1024); print(f'UDP from {addr}: {data.decode()}'); s_udp.sendto(data, addr); print(f'UDP echo sent to {addr}')
threading.Thread(target=tcp_handler).start(), threading.Thread(target=udp_handler).start()
```

client.py
```
import socket

server_ip = 'label-1.ai-parents.cn'  # Replace with your server's IP
port = 60005

# TCP echo test
# tcp_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# tcp_client.connect((server_ip, port))
# tcp_client.sendall(b'Hello TCP')
# tcp_data = tcp_client.recv(1024)
# print("TCP Response:", tcp_data.decode())
# tcp_client.close()

# UDP echo test
udp_client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
udp_client.sendto(b'Hello UDP', (server_ip, port))
udp_data, _ = udp_client.recvfrom(1024)
print('UDP Response:', udp_data.decode())
udp_client.close()
```

## Eternal Terminal
### 安装
centos 8 安装：
```
sudo dnf install epel-release
sudo dnf install et
```

debian 安装：
```
git clone --recurse-submodules --depth 1 https://github.com/MisterTea/EternalTerminal.git EternalTerminal.git
cd EternalTerminal.git
mkdir build
cd build
cmake ../
make
sudo make install
make package # 生成了 build/EternalTerminal-6.2.9-Linux.deb
```

### 服务器端配置
```
systemctl status et
sudo systemctl enable --now et
```