[1.1] Usage Guide
  https://matplotlib.org/stable/users/explain/quick_start.html

[1.2] Annotating Plots
  https://matplotlib.org/stable/gallery/text_labels_and_annotations/annotation_demo.html
  
[1.3] Customizing Matplotlib with style sheets and rcParams
  https://matplotlib.org/stable/users/explain/customizing.html
  
[1.11] Animations using Matplotlib
  https://matplotlib.org/stable/users/explain/animations/index.html
  
[2.1] Matplotlib笔记
  https://www.jianshu.com/nb/28081317

[2.2] 
  Mastering Data Visualization with Matplotlib: A Comprehensive Guide
  https://medium.com/@abuzarzulfiqar/mastering-data-visualization-with-matplotlib-a-comprehensive-guide-290dbd48ccbf
  
[3.1] Matplotlib可视化及应用
  https://www.jianshu.com/p/db2e28201835

[4.1] Embedding in Qt
https://matplotlib.org/stable/gallery/user_interfaces/embedding_in_qt_sgskip.html

[4.2] Plotting with Matplotlib
https://www.pythonguis.com/tutorials/plotting-matplotlib/

[4.3] Embedding WebAgg
https://matplotlib.org/stable/gallery/user_interfaces/embedding_webagg_sgskip.html

[5.1] The mplot3d Toolkit
  https://matplotlib.org/tutorials/toolkits/mplot3d.html

[5.2] Matplotlib：mpl_toolkits.mplot3d工具包
  https://www.jianshu.com/p/b563920fa7a8
  
[6.1] Linux使用Matplotlib画图中文字体问题
  https://coder-liuu.github.io/posts/linux/linux-matplotlib/

[6.2] 一文彻底解决 matplotlib 中的字体管理
  https://blog.csdn.net/sinat_32570141/article/details/109848383
  
[7.1] axes 创建笛卡尔坐标区
  https://ww2.mathworks.cn/help/matlab/ref/axes.html?s_tid=doc_ta

[7.2] subplot 在各个分块位置创建坐标区
  https://ww2.mathworks.cn/help/matlab/ref/subplot.html?s_tid=doc_ta
  
[7.3] figure 创建图窗窗口
  https://ww2.mathworks.cn/help/matlab/ref/figure.html?s_tid=doc_ta
  
[7.4] python matplotlib中axes与axis的区别是什么?
  https://www.zhihu.com/question/51745620

## 安装

TkAgg

## 简单的例子  
例1：
```
import matplotlib as mpl
import matplotlib.pyplot as plt

plt.ion()  # 启用交互模式。这样不用调用 Figure.show() 就会自动显示出来，其它的修改也会自动刷新。
fig = plt.figure() # 创建并显示了一个空白图示。fig 的类型是 matplotlib.figure.Figure 。
# fig.show()  # 显示 Figure 窗口。plt.ion() 后不需要调用。
ax = fig.add_subplot(111, aspect='equal') # 添加一个子图，子图包括等长的x、y轴，轴上有刻度，默认（0,1范围），但还没有数据点。类型为 matplotlib.axes._subplots.AxesSubplot 。
lines1 = ax.plot([1, 2, 3, 4], [1, 4, 2, 3]) # 添加一组x、y数据点。x、y轴刻度范围会自动相应调整。样式默认为折线图。返回类型为 matplotlib.lines.Line2D 。
lines2 = ax.plot([1, 2, 3, 4], [1, 2, 5, 1]) # 添加第二组x、y数据点。会自动用不同的颜色显示。
plt.title('my lines') # 设置图表标题（非窗口标题）
```
例2[1.1]：
```
import matplotlib.pyplot as plt
import numpy as np

import matplotlib as mpl

fig, ax = plt.subplots()  # Create a figure containing a single axes.
ax.plot([1, 2, 3, 4], [1, 4, 2, 3])  # Plot some data on the axes.
plt.show()
```

## 结构和术语
[1.1, 7.1～7.3]
matplotlib 模仿了 matlab 绘图库的 API，一些术语和翻译可以参考 matlab 的文档[7.1～7.3]。

```
Figure (图窗/图示， plt.figure(), plt.figure.Figure )
  Axes (坐标区/子图， fig.subplots()， 可以有多个)
    Title (标题，ax.set_title()/get_title()，一个)
    x/y Axis （x/y 轴, ax.axis，一套）
    xlabel, ylabel (ax.set_xlabel()，一套)
    Legend（图例， ax.legend，用来指示不同的颜色、线型分别表示什么，一套）
    Line (ax.plot() 线图，直线或曲线，可以有多个)
    Markers (ax.scatter() 散点图，可以有多个)
    Artist （图形元素，所有可见元素， Figure, Axes, and Axis objects). This includes Text objects, Line2D objects, collections objects, Patch）
```
Figure (图窗/图示）是一个矩形显示区域，用来容纳子图。类似其它 GUI 库中的窗口或面板。多个 Figure 之间是不会重叠的。
一个 Figure 内可以有多个 Axes (坐标区/子图），多个子图之间可以部分或完全重叠，也可以均分它们所属的图窗。
一个子图有一套x/y坐标轴、一个标题、一个或多个线图、散点图，以及其它图形元素（如位图和图形）。

Axes（坐标区）和 subplot（子图）的概念和术语经常被混用。在实现层面，只有 Axes 类型。subplot 是 Axes 的特例，即多个子图采用网格布局，实现不重叠且互相对齐的布置。
例如，创建一个拥有2x2个子图的图窗：
```
fig, axs = plt.subplots(2, 2)
axs
array([[<Axes: >, <Axes: >],
       [<Axes: >, <Axes: >]], dtype=object)
type(axs)
<class 'numpy.ndarray'>
```

## 代码风格

mpl 的绘图 API 有两种风格：显式和隐式，或称面向对象式和面向过程式。

面向对象式：先创建 Figure 和 Axes 对象，然后调用 Axes 的方法绘图。
```
x = np.linspace(0, 2, 100)  # Sample data.

# Note that even in the OO-style, we use `.pyplot.figure` to create the Figure.
# plt.subplots() 基本等效于 plt.Figure.subplots() （静态方法）
fig, ax = plt.subplots(figsize=(5, 2.7), layout='constrained')
ax.plot(x, x, label='linear')  # Plot some data on the axes.
ax.plot(x, x**2, label='quadratic')  # Plot more data on the axes...
ax.plot(x, x**3, label='cubic')  # ... and some more.
ax.set_xlabel('x label')  # Add an x-label to the axes.
ax.set_ylabel('y label')  # Add a y-label to the axes.
ax.set_title("Simple Plot")  # Add a title to the axes.
ax.legend()  # Add a legend.
fig.show()
```

另一种创建 Figure 和 Axes 的流程：
```
fig = plt.figure(figsize=(5, 2.7), layout='constrained')
ax = fig.subplots()
```

面向过程式：调用 matplotlib 包下的函数绘图。
```
x = np.linspace(0, 2, 100)  # Sample data.

# 创建一个全局 Figure，返回此 Figure 对象。之后可以通过 plt.gcf() 再次获取此 Figure 对象。
plt.figure(figsize=(5, 2.7), layout='constrained')
plt.plot(x, x, label='linear')  # Plot some data on the (implicit) axes.
plt.plot(x, x**2, label='quadratic')  # etc.
plt.plot(x, x**3, label='cubic')
plt.xlabel('x label')
plt.ylabel('y label')
plt.title("Simple Plot")
plt.legend()
plt.show()
```
面向过程式会稍微简洁一些，适用于快速作图。面向对象式可以处理更复杂的图，以及可复用的绘图代码。
不过，在 python 控制台中，面向过程式会有一些问题，例如图窗只能显示一次。

面向过程式方法创建的 Figure 和 Axes 对象会保存在全局变量中，可以通过 `plt.gcf()` 和 `plt.gca()` 访问，如果不存在会自动创建。

## 通过 ssh 远程使用 Matplotlib GUI
如果远程和本地都使用 Linux ，可以给 ssh 加上 -X 参数，即可在本地显示 Matplotlib GUI，实现查看远程数据。
```
ssh -X user@host

python
...
plt.show()
```
注意，如果是在没有使用 ssh -X 参数来的情况下启动的 tmux 会话，则即使后来用 ssh -X 连接，也无法启动 GUI。

## 阻止 Matplotlib 窗口消失

Matplotlib 窗口不会阻止程序退出，其大多数方法也不是阻塞的，所以 Matplotlib 窗口可能会闪烁一下就消失。可以在程序最后加上获取标准输入的代码，阻止程序退出，Matplotlib 窗口消失：

```
line = sys.stdin.readline()
```

## 图窗

```
import matplotlib.pyplot as plt
import numpy as np

import matplotlib as mpl

fig = plt.figure(figsize=(4, 2), dpi=100.0, facecolor='lightskyblue',
                 layout='constrained')
# 添加/设置图窗标题，在图窗第一行居中。默认没有标题。注意这不影响窗口标题（初始为 Figure 1）。
fig.suptitle('A nice Matplotlib Figure')
# 添加一个子图 Axes，占据整个 fig
ax = fig.add_subplot()
# 添加/设置子图标题。在图窗标题之下。
ax.set_title('Axes', loc='left', fontstyle='oblique', fontsize='medium')
# 显示图窗。在 python 交互控制台下调用此方法后，显示窗口，并返回控制台。用鼠标关闭窗口后可以再次调用此方法显示图窗。
fig.show()

# 显示图窗。效果与 fig.show() 类似，但是用鼠标关闭窗口后再次调用此方法，并不能再次显示。
plt.show()

fig.get_axes() # 返回子图列表，可能是空列表。
```
图窗的大小参数 figsize 是 (宽, 高) ，单位是英寸。`fig.get_figwidth()`， `fig.get_figheight()`， `fig.get_size_inches():[w, h] / fig.set_size_inches(w, h)` 可以查询/设置图窗的大小，单位是英寸。
`fig.get_dpi()` 是获得 DPI，乘以 figsize 可换算到像素尺寸。此 dpi 可以在创建 Figure 时被用户设置，并非绑定屏幕dpi。文档中说默认dpi是100，但在125%缩放比例的计算机上测得200。

facecolor 是背景色，edgecolor 是线条颜色。详细的样式设置可参考“样式表”。

## 后端/图形环境
### 简介
后端是指 mpl 显示图形、处理用户交互的部分。
mpl 的后端有：MacOSX, QtAgg, GTK4Agg, Gtk3Agg, TkAgg, WxAgg, Agg。其中 Agg 是非交互后端，只能输出图像。

可以用 API、配置文件、环境变量查看或设置当前后端。
```
import matplotlib
# linux GUI 上一般是 'QtAgg'，不带 GUI 的则模式是 'Agg'。
matplotlib.rcParams["backend"] # 查看当前后端
plt.rcParams["backend"] # 与上面等效
matplotlib.get_backend() # 与上面等效
```

```
export MPLBACKEND=qtagg
python simple_plot.py
```

```
import matplotlib
matplotlib.use('qtagg')
```

### Jupyter Notebook
```
# 默认状态，使用 inline 后端
import matplotlib
matplotlib.rcParams["backend"]
# 'module://matplotlib_inline.backend_inline' , 

# 使用 inline 后端，直接内嵌 Figure 内部的图形，如 Axes
%matplotlib inline

# 带有 Figure 的窗口装饰，后端 'nbAgg'
%matplotlib notebook
# 同上，但未必可用
%matplotlib widget

# ipympl 后端，需要单独安装 ipympl 包
%matplotlib ipympl
```

### 系统 GUI 后端
python 控制台和在终端执行的 python 脚本、ipython 控制台都属于此类。在此环境下 mpl 可用的后端有 Qt, Tk, Wx。
```
plt.rcParams["backend"] # linux 上一般是 'QtAgg'，也可能不区分大小写。

# 指定后端实现
matplotlib.use('QtAgg')

# 显示出来
plt.show()
```

如果是在无 GUI 环境中，可以使用 'Agg' 后端，将绘图保存为图像。

### 非交互后端/保存图像
Figure.savefig() 可以保存图窗内容为图像文件，格式包括 png, jpeg, PDF, EPS, and SVG 。
```
fig.savefig('MyFigure.png', dpi=200)
```
目前没找到保存图像到内存缓冲区的方法。

### 字体
matplotlib 的默认安装的 Figure 往往无法显示中文（中文显示为方框），特别是 Linux 环境。

在 Linux 环境下，在 python 控制台或 python 脚本中，在显示 matplotlib 图窗前，可以这样设置中文字体：
```
import matplotlib
matplotlib.rcParams['font.sans-serif'].insert(0, "WenQuanYi Micro Hei")
matplotlib.rcParams['font.serif'].insert(0, "AR PL UMing CN")
```
注意应该把中文字体放在`rcParams['font.sans-serif']`最前面，否则中文仍然显示为方框。
这样大部分中文应该就可以显示了。"WenQuanYi Micro Hei" 和 "AR PL UMing CN" 都是操作系统中的中文字体。如果缺少它们，需要安装它们，或者改成其他中文字体。

如果上述操作无法解决问题，可以阅读下面的资料，

先查看mpl字体配置：
```
import matplotlib

import re
fontRcParams = { k: v for k, v in matplotlib.rcParams.items() if re.match('font', k) }
print(fontRcParams)

print(matplotlib.matplotlib_fname())

 '/home/duanyao/opt/venv/v311/lib/python3.11/site-packages/matplotlib/mpl-data/matplotlibrc'

print(matplotlib.get_cachedir())

'/home/duanyao/.cache/matplotlib'
```

matplotlib.rcParams 中的字体设置：
```
{'font.cursive': ['Apple Chancery', 'Textile', 'Zapf Chancery', 'Sand', 'Script MT', 'Felipa', 'Comic Neue', 'Comic Sans MS', 'cursive'], 
'font.family': ['sans-serif'], 
'font.fantasy': ['Chicago', 'Charcoal', 'Impact', 'Western', 'Humor Sans', 'xkcd', 'fantasy'], 
'font.monospace': ['DejaVu Sans Mono', 'Bitstream Vera Sans Mono', 'Computer Modern Typewriter', 'Andale Mono', 'Nimbus Mono L', 'Courier New', 'Courier', 'Fixed', 'Terminal', 'monospace'], 
'font.sans-serif': ['DejaVu Sans', 'Bitstream Vera Sans', 'Computer Modern Sans Serif', 'Lucida Grande', 'Verdana', 'Geneva', 'Lucid', 'Arial', 'Helvetica', 'Avant Garde', 'sans-serif'], 
'font.serif': ['DejaVu Serif', 'Bitstream Vera Serif', 'Computer Modern Roman', 'New Century Schoolbook', 'Century Schoolbook L', 'Utopia', 'ITC Bookman', 'Bookman', 'Nimbus Roman No9 L', 'Times New Roman', 'Times', 'Palatino', 'Charter', 'serif'], 
'font.size': 10.0, 'font.stretch': 'normal', 'font.style': 'normal', 'font.variant': 'normal', 'font.weight': 'normal'}
```

matplotlibrc 记录了 matplotlib 默认的字体族配置，如：
```
#font.serif:      DejaVu Serif, Bitstream Vera Serif, Computer Modern Roman, New Century Schoolbook, Century Schoolbook L, Utopia, ITC Bookman, Bookman, Nimbus Roman No9 L, Times New Roman, Times, Palatino, Charter, serif
#font.sans-serif: DejaVu Sans, Bitstream Vera Sans, Computer Modern Sans Serif, Lucida Grande, Verdana, Geneva, Lucid, Arial, Helvetica, Avant Garde, sans-serif
#font.cursive:    Apple Chancery, Textile, Zapf Chancery, Sand, Script MT, Felipa, Comic Neue, Comic Sans MS, cursive
#font.fantasy:    Chicago, Charcoal, Impact, Western, Humor Sans, xkcd, fantasy
#font.monospace:  DejaVu Sans Mono, Bitstream Vera Sans Mono, Computer Modern Typewriter, Andale Mono, Nimbus Mono L, Courier New, Courier, Fixed, Terminal, monospace
```

matplotlib 缓存目录下的 `fontlist*.json (如 fontlist-v330.json)` 记录了可用的操作系统字体，如果不存在，matplotlib 启动时会创建它。但这里面的中文字体一般不在 matplotlibrc 的配置中，所以导致无法显示中文。
```
duanyao@duanyao-lt-d:~/project$ grep -A3 -B3 -i -e "hei" -e "ming" -e "WenQuanYi" /home/duanyao/.cache/matplotlib/fontlist*.json
      "__class__": "FontEntry"
    },
    {
      "fname": "/usr/share/fonts/truetype/wqy/wqy-microhei.ttc",
      "name": "WenQuanYi Micro Hei",
      "style": "normal",
      "variant": "normal",
      "weight": 400,
--
      "__class__": "FontEntry"
    },
    {
      "fname": "/usr/share/fonts/truetype/wqy/wqy-zenhei.ttc",
      "name": "WenQuanYi Zen Hei",
      "style": "normal",
      "variant": "normal",
      "weight": 500,
--
      "__class__": "FontEntry"
    },
    {
      "fname": "/usr/share/fonts/truetype/arphic/uming.ttc",
      "name": "AR PL UMing CN",
      "style": "normal",
      "variant": "normal",
      "weight": 300,
```

使用 fc-list 命令列出系统里的中文字体（参考 fontconfig.md）：
```
fc-list :lang=zh-cn
```
`:lang=zh-cn` 表示过滤出支持简体中文的字体。如果不限于简体，可改为 `:lang=zh`。冒号（:）与 `lang=zh-cn` 之间不要有空格，否则不起作用。

结果大致如下：

```
/usr/share/fonts/opentype/noto/NotoSansCJK-Bold.ttc: Noto Sans CJK JP,Noto Sans CJK JP Bold:style=Bold,Regular
/usr/share/fonts/opentype/noto/NotoSerifCJK-Bold.ttc: Noto Serif CJK SC:style=Bold
/usr/share/fonts/opentype/noto/NotoSerifCJK-Bold.ttc: Noto Serif CJK TC:style=Bold
/usr/share/fonts/opentype/noto/NotoSerifCJK-Bold.ttc: Noto Serif CJK JP:style=Bold
/usr/share/fonts/opentype/noto/NotoSerifCJK-Bold.ttc: Noto Serif CJK KR:style=Bold
/usr/share/fonts/opentype/noto/NotoSansCJK-Bold.ttc: Noto Sans Mono CJK KR,Noto Sans Mono CJK KR Bold:style=Bold,Regular
/usr/share/fonts/truetype/wqy/wqy-microhei.ttc: 文泉驿微米黑,文泉驛微米黑,WenQuanYi Micro Hei:style=Regular
/usr/share/fonts/opentype/noto/NotoSansCJK-Bold.ttc: Noto Sans Mono CJK JP,Noto Sans Mono CJK JP Bold:style=Bold,Regular
/usr/share/fonts/opentype/noto/NotoSansCJK-Regular.ttc: Noto Sans CJK JP,Noto Sans CJK JP Regular:style=Regular
/usr/share/fonts/truetype/wqy/wqy-zenhei.ttc: 文泉驿正黑,文泉驛正黑,WenQuanYi Zen Hei:style=Regular
/usr/share/fonts/truetype/wqy/wqy-zenhei.ttc: 文泉驿点阵正黑,文泉驛點陣正黑,WenQuanYi Zen Hei Sharp:style=Regular
```

## 子图（Axes and subplots）

```
import matplotlib.pyplot as plt
import numpy as np

fig, axs = plt.subplots(ncols=2, nrows=2, figsize=(3.5, 2.5),
                        layout="constrained")
# for each Axes, add an artist, in this case a nice label in the middle...
for row in range(2):
    for col in range(2):
        axs[row, col].annotate(f'axs[{row}, {col}]', (0.5, 0.5),
                            transform=axs[row, col].transAxes,
                            ha='center', va='center', fontsize=18,
                            color='darkgrey')
fig.suptitle('plt.subplots()')
```
使用 add_subplot() 添加子图。
```
fig = plt.figure()
# 前 3 个数字参数是 (nrows, ncols, index)，index 从 1 开始。1,1,1 就是单个子图，占据整个 figure。
fig.add_subplot(1,1,1, title='a')
# 1,1,1 可简写为 111；最大可写成 339。由于与 title='a' 占据的区域重叠，将会在其上方重叠显示。
fig.add_subplot(111, title='b')
# 移除 title='b' 的子图。
fig.axes[1].remove()
# 添加一个跨单元格子图。子图的区域为包含 (1, 5) 单元格的最小矩形。
fig.add_subplot(3, 3, (1, 5), title='c')
```
3, 3
```
1 2 3
4 5 6
7 8 9
```
## 样式和样式表

matplotlib.rcParams 存储了默认设置和默认样式表。

matplotlib.rcdefaults


plt.style.use('ggplot')
print(plt.style.available)


## 绘制位图

```
import matplotlib as mpl
import matplotlib.pyplot as plt
import cv2

plt.ion()
fig = plt.figure() # 创建并显示了一个空白图示。fig 的类型是 matplotlib.figure.Figure 。
ax = fig.add_subplot(111, aspect='equal')

im = cv2.imread('xxx.jpg')
iv = ax.imshow(im) # matplotlib.image.AxesImage

im2 = cv2.imread('xxx.jpg')
iv.set_data(im2) # 更新图像内容
```

图像格式可有多种：
* numpy 。 (h,w,c;uint8) 格式，颜色为 bgr 或 rgb。cv2.imread() 和 skimage.io.imread() 输出的都是这样的格式。
* PIL

默认会按图像的像素宽度和高度产生x和y坐标轴和刻度，y轴默认向下。

## 绘制图形

```
import matplotlib.patches as patches

x=100
y=200
w=80
h=110
color=[0.9, 0.1, 0.1] # 颜色，r,g,b, 浮点数 [0,1]。
rect = patches.Rectangle((x,y), w, h, fill=False, lw=3, ec=color) # ec = edgecolor
ax.add_patch(patches.Rectangle((x,y), w, h, fill=False, lw=3, ec=color))
```
如果x轴向右、y轴向下，则矩形的原点位于左上。

其它图形包括： Ellipse ，Polygon ， Arrow  等。

Rectangle(xy, width, height, angle=0.0, **kwargs) # x,y 为左上或右下点
Ellipse(xy, width, height, angle=0, **kwargs) # x,y 为中心点

## 文本

```
x=100
y=200
color=[0.9, 0.1, 0.1] # 颜色，r,g,b, 浮点数 [0,1]。
t = ax.text(x, y, "text", c = color, size = 12.0) # matplotlib.text.Text, c = color, size = fontsize
```

坐标原点是文本框的左下角（当y轴向下时）。

注意，默认情况下 matplotlib 的 text 是不能显示非 ASCII 字符的[6.1]。

windows 上指定字体名即可：

```
import matplotlib.pyplot as plt
plt.rcParams['font.sans-serif']=['SimHei'] 
```

但 linux 上还不能正常显示。

## 注解（Annotation）

注解通常是文本和箭头的组合，指向数据点或曲线[1.2]。

```
ax.annotate('figure points',
            xy=(80, 80), xycoords='figure points')
```

## 直方图

```
import matplotlib.pyplot as plt
import numpy as np

x = np.random.normal(170, 10, 250)

plt.hist(x)
plt.show()
```

## 状态管理，刷新，重画
`fig.canvas.draw_idle()` 可以要求刷新/重画图窗，将在下次GUI事件循环空闲时执行。
`fig.canvas.flush_events()` 可以让图窗处理交互事件。
例如，在 python 控制台中：
```
#...创建图示
# 显示图窗
fig.show()
#...修改图示
fig.stale # True, 需要重画
# 刷新/重画图窗
fig.canvas.draw_idle()
# 同时要求处理事件，可选。
fig.canvas.flush_events()
# 再次显示图窗
fig.show()
fig.stale # False, 不需要重画
```
在交互模式下，可以自动重画。python 控制台默认没有启用交互模式。

```
plt.isinteractive()
plt.ion() # Enable interactive mode.
plt.ioff() # Disable interactive mode.
```

临时切换交互模式

```
# if interactive mode is on
# then figures will be shown on creation
plt.ion()
# This figure will be shown immediately
fig = plt.figure()

with plt.ioff():
    # interactive mode will be off
    # figures will not automatically be shown
    fig2 = plt.figure()
    # ...
```


```
fig.canvas.flush_events()
plt.draw() # 刷新显示的内容。在 plt.ion() 启用时，一般不需要调用 plt.draw() 。
ax.cla() # clear 清除内容
```

## 动画/高速绘图
[1.11]
```
import matplotlib.pyplot as plt
import numpy as np

import matplotlib.animation as animation

fig, ax = plt.subplots()
t = np.linspace(0, 3, 40)
g = -9.81
v0 = 12
z = g * t**2 / 2 + v0 * t

v02 = 5
z2 = g * t**2 / 2 + v02 * t

scat = ax.scatter(t[0], z[0], c="b", s=5, label=f'v0 = {v0} m/s')
line2 = ax.plot(t[0], z2[0], label=f'v0 = {v02} m/s')[0]
ax.set(xlim=[0, 3], ylim=[-4, 10], xlabel='Time [s]', ylabel='Z [m]')
ax.legend()


def update(frame):
    # for each frame, update the data stored on each artist.
    x = t[:frame]
    y = z[:frame]
    # update the scatter plot:
    data = np.stack([x, y]).T
    scat.set_offsets(data)
    # update the line plot:
    line2.set_xdata(t[:frame])
    line2.set_ydata(z2[:frame])
    return (scat, line2)


ani = animation.FuncAnimation(fig=fig, func=update, frames=40, interval=30)
plt.show()
```


## 交互事件处理

### 键盘事件
home
pageup
pagedown
end
right
left

## mplot3d

mpl_toolkits.mplot3d是Matplotlib里面专门用来画三维图的工具包，官方指南请点击[5.1]

例子[5.2]
```
import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d as p3d

plt.ion()  # optional, Interactive On. plt.figure() show a window to draw the figure

fig = plt.figure()  # matplotlib.figure.Figure
ax = p3d.Axes3D(fig)

z = np.linspace(0, 15, 1000)
x = np.sin(z)
y = np.cos(z)
ax.plot(x, y, z, 'green')  # return mpl_toolkits.mplot3d.art3d.Line3D[1]
```

## 图形界面嵌入
### 嵌入 Qt 程序
[4.1,4.2] 

```
import sys
import matplotlib
matplotlib.use('Qt5Agg')

from PyQt5 import QtCore, QtWidgets

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure


class MplCanvas(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        super(MplCanvas, self).__init__(fig)


class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        # Create the maptlotlib FigureCanvas object,
        # which defines a single set of axes as self.axes.
        sc = MplCanvas(self, width=5, height=4, dpi=100)
        sc.axes.plot([0,1,2,3,4], [10,1,20,3,40])
        self.setCentralWidget(sc)

        self.show()


app = QtWidgets.QApplication(sys.argv)
w = MainWindow()
app.exec_()
```
