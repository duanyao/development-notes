## 持久存储设备

[1] 基于SSD的数据库性能优化
http://blogread.cn/it/article/2660?f=sa

[2] NAND flash和NOR flash的区别详解
http://www.elecfans.com/bandaoti/cunchu/20120925290176.html

[3] SSD的主要缺陷及Wear Leveling技术详解
http://www.blogread.cn/it/article/3221

[4] SSD磨损数据的分析报告
http://blogread.cn/it/article/3865?f=sa

[5] SSD 寿命的检查和健康判断
http://blogread.cn/it/article/5101?f=sa

[6] 数据的存储介质-固态存储SSD
http://blogread.cn/it/article/6678?f=sa

[7] SSD的随机写一定很慢吗？
http://blogread.cn/it/article/3976?f=sa

[8] OP（Over-provisioning）预留空间
http://www.pceva.com.cn/topic/crucialssd/index-6_5.html

[9] Trim（有翻译为：修剪，但非正式翻译）
http://www.pceva.com.cn/topic/crucialssd/index-6_6.html

[10] Linux 中对 SSD 的优化 Discard，类 TRIM 的功能
http://blogread.cn/it/article/2800?f=sr

[11] 想了解一下突然掉电对SSD的影响
http://bbs.pceva.com.cn/thread-21204-1-1.html

[12] SSD缓存掉电保护：3种方案的利与弊
http://storage.chinabyte.com/410/13166910.shtml

[13] 浴室谈SSD系列特别篇：固态硬盘为何会“变砖”
http://bbs.pceva.com.cn/thread-43988-1-1.html

[14] Wear leveling
https://en.wikipedia.org/wiki/Wear_leveling

[15] SSD Secure Erase
https://www.thomas-krenn.com/en/wiki/SSD_Secure_Erase

[16] 使用hdparm命令发送ATA“Secure Erase”指令恢复SSD硬盘的写性能
http://blog.chinaunix.net/uid-15145533-id-2775789.html

[17] Trim (computing)
https://en.wikipedia.org/wiki/Trim_(computing)

[18] How to enable TRIM?
https://askubuntu.com/questions/18903/how-to-enable-trim/

[19] 杂说闪存番外：我们的数据在SSD上安全吗？
https://zhuanlan.zhihu.com/p/27380955

[20] 杂说闪存番外：手机为什么越用越卡和闪存写放大
https://zhuanlan.zhihu.com/p/27294558

[21] SSD: how to optimize your Solid State Drive for Linux Mint 18.1, Ubuntu 16.04 and Debian
https://sites.google.com/site/easylinuxtipsproject/ssd

[22] 杂说闪存四：闪存硬盘接口大比拼
https://zhuanlan.zhihu.com/p/27068849

[23] Trimming NTFS and ext4 Formatted SSD
https://superuser.com/questions/1060804/trimming-ntfs-and-ext4-formatted-ssd

[24] 笔记本mSATA接口和mini PCI-E接口之间的区别解析图
http://www.jb51.net/hardware/yingpan/141207.html

[25] NGFF是什么？或在今年大放异彩新产品
http://tech.hexun.com/2013-01-11/150038256.html

[26] M.2、U.2谁更好？主流硬盘接口大扫盲
http://news.mydrivers.com/1/466/466321.htm

[27] U.2和M.2接口有什么区别？U.2接口固态硬盘优缺点分析
http://www.pc841.com/article/20160504-65728.html

[28] eTLC和eMLC哪个好 固态硬盘eTLC与eMLC的区别
http://www.pc841.com/article/20160420-65197.html

[29] 使用smartmontools工具查看SSD的"秘密"信息
https://www.chiphell.com/thread-1140952-1-1.html

[30] How to Tell If Your SSD is Failing
https://gigabytekingdom.com/how-to-tell-if-your-ssd-is-failing/

[31] 三星PM9A1 00B00公版固件升级保姆级教程及疑难杂症（SLC Cache降速门）
https://zhuanlan.zhihu.com/p/446386826

## SSD 概述
参考[1]。

NOR和NAND

   NOR和NAND都是闪存技术的一种，NOR是Intel公司开发的，它有点类似于内存，允许通过地址直接访问任何一个内存单元，缺点是：密度低(容量小)，写入和擦除的速度很慢。NAND是东芝公司开发的，它密度高(容量大)，写入和擦除的速度都很快，但是必须通过特定的IO接口经过地址转换之后才可以访问，有些类似于磁盘。

   我们现在广泛使用的U盘，SD卡，SSD都属于NAND类型，厂商将flash memory封装成为不同的接口，比如Intel的SSD就是采用了SATA的接口，访问与普通SATA磁盘一样，还有一些企业级的闪存卡，比如FusionIO，则封装为PCIe接口。

   SLC和MLC

   SLC是单极单元，MLC是多级单元，两者的差异在于每单元存储的数据量(密度)，SLC每单元只存储一位，只包含0和1两个电压符，MLC每单元可以存储两位，包含四个电压符(00,01,10,11)。显然，MLC的存储容量比SLC大，但是SLC更简单可靠，SLC读取和写入的速度都比MLC更快，而且SLC比MLC更耐用，MLC每单元可擦除1w次，而SLC可擦除10w次，所以，企业级的闪存产品一般都选用SLC，这也是为什么企业级产品比家用产品贵很多的原因。

   SSD的技术特点

   SSD与传统磁盘相比，第一是没有机械装置，第二是由磁介质改为了电介质。在SSD内部有一个FTL(Flash Transalation Layer)，它相当于磁盘中的控制器，主要功能就是作地址映射，将flash memory的物理地址映射为磁盘的LBA逻辑地址，并提供给OS作透明访问。

   SSD没有传统磁盘的寻道时间和延迟时间，所以SSD可以提供非常高的随机读取能力，这是它的最大优势，SLC类型的SSD通常可以提供超过35000的IOPS，传统15k的SAS磁盘，最多也只能达到160个IOPS，这对于传统磁盘来说几乎就是个天文数字。SSD连续读的能力相比普通磁盘优势并不明显，因为连续读对于传统磁盘来说，并不需要寻道时间，15k的SAS磁盘，连续读的吞吐能力可以达到130MB，而SLC类型的SSD可以达到170-200MB，我们看到在吞吐量方面，SSD虽然比传统磁盘高一些，但优势虽然并不明显。

   SSD的写操作比较特殊，SSD的最小写入单元为4KB，称为页(page)，当写入空白位置时可以按照4KB的单位写入，但是如果需要改写某个单元时，则需要一个额外的擦除(erase)动作，擦除的单位一般是128个page(512KB)，每个擦除单元称为块(block)。如果向一个空白的page写入信息时，可以直接写入而无需擦除，但是如果需要改写某个存储单元(page)的数据，必须首先将整个block读入缓存，然后修改数据，并擦除整个block的数据，最后将整个block写入，很显然，SSD改写数据的代价很高，SSD的这个特性，我们称之为erase-before-write。

   经过测试，SLC SSD的随即写性能可以达到3000个左右的IOPS，连续写的吞吐量可以达到170-200MB，这个数据还是比传统磁盘高出不少。但是，随着SSD的不断写入，当越来越多的数据需要被改写时，写的性能就会逐步下降。经过我们的测试，SLC在这个方面要明显好于MLC，在长时间写入后，MLC随机写IO下降得非常厉害，而SLC表现则比较稳定。为了解决这个问题，各个厂商都有很多策略来防止写性能下降的问题。


   写入放大

   因为SSD的erase-before-write的特性，所以就出现了一个写入放大的概念，比如你想改写4K的数据，必须首先将整个擦除块(512KB)中的数据读出到缓存中，改写后，将整个块一起写入，这时你实际写入了512KB的数据，写入放大系数是128。写入放大最好的情况是1，就是不存在放大的情况。

   Wear leveling算法可以有效缓解写入放大的问题，但是不合理的算法依然会导致写入放大，比如用户需要写入4k数据时，发现free block pool中没有空白的block，这时就必须在data block pool中选择一个包含无效数据的block，先读入缓存中，改写后，将整个块一起写入，采用wear leveling算法依然会存在写入放大的问题。

   通过为SSD预留更多空间，可以显著缓解写入放大导致的性能问题。根据我们的测试结果，MLC SSD在长时间的随机写入后，性能下降很明显(随机写IOPS甚至降低到300)。如果为wear leveling预留更多空间，就可以显著改善MLC SSD在长时间写操作之后的性能下降问题，而且保留的空间越多，性能提升就越明显。相比较而言，SLC SSD的性能要稳定很多(IOPS在长时间随机写后，随机写可以稳定在3000 IOPS)，我想应该是SLC SSD的容量通常比较小(32G和64G)，而用于wear leveling的空间又比较大的原因。

## SSD 的结构和组成
参考[6]。

主要由如下几个关键的组件组成：

   1.       协议转换层(可选)，主要是把ata/sata/scsi/ide这样的磁盘读写协议转换为针对NAND FLASH 的读写访问请求。这个开销可控。

   2.       FLASH芯片控制层，是整个ssd最重要的部分，也是本章的主角儿~，直接决定了ssd的一切。有一些实现中，需要SDRAM来做缓存支持，并有自己独立的CPU。

   3.       NAND FLASH 芯片，存数据的地方

FLASH 控制器主要在承担的职责有哪些：

   1.       协调多块NAND芯片，使用并行技术，提升整块磁盘的读写吞吐量和IOPS

   2.       处理写入放大问题

   3.       磨损平衡Wear Leveling(WL)

   在大部分的实现里面，控制器是一个专门的芯片，封装了自己的算法和存储芯片。

   不过在最近，有一些公司走出了不同的道路，尝试使用用户的CPU来进行计算和数据存储。

   这两种方式各有优劣。

   如果你使用了专门的控制器，那么好处是不会占用宝贵的中央处理器资源。而坏处则主要是升级有一定难度，并且计算资源会受到板载硬件的限制。

   而如果你与用户共享中央处理器，那么好处是升级容易，用户甚至可以直接通过修改厂商提供的硬件驱动的方式增加更符合自己实际业务场景的写入策略，但坏处是会占用中央处理器资源。

   目前来看，两种方式各有优势，后续也会相互借鉴进而更加完善，目前还没有哪一方能够完全占据市场，在可见的未来应该也不会。

## 磨损平衡 wear leveling
参考[1，14]。

   因为SSD存在“写磨损”的问题，当某个单元长时间被反复擦写时(比如Oracle redo)，不仅会造成写入的性能问题，而且会大大缩短SSD的使用寿命，所以必须设计一个均衡负载的算法来保证SSD的每个单元能够被均衡的使用，这就是wear leveling，称为损耗均衡算法。

   Wear leveling也是SSD内部的FTL实现的，它通过数据迁移来达到均衡损耗的目的。Wear leveling依赖于SSD中的一部分保留空间，基本原理是在SSD中设置了两个block pool，一个是free block pool(空闲池)，一个是数据池(data block pool)，当需要改写某个page时(如果写入原有位置，必须先擦除整个block，然后才能写入数据)，并不写入原有位置(不需要擦除的动作)，而是从空闲池中取出新的block，将现有的数据和需要改写的数据合并为新的block，一起写入新的空白block，原有的block被标识为invalid状态(等待被擦除回收)，新的block则进入数据池。后台任务会定时从data block中取出无效数据的block，擦除后回收到空闲池中。这样做的好处在于，一是不会反复擦写同一个block，二是写入的速度会比较快(省略了擦除的动作)。

   Wear leveling分为两种：动态损耗均衡和静态损耗均衡，两者的原理一致，区别在于动态算法只会处理动态数据，比如数据改写时才会触发数据迁移的动作，对静态数据不起作用，而静态算法可以均衡静态数据，当后台任务发现损耗很低的静态数据块时，将其迁移到其他数据库块上，将这些块放入空闲池中使用。从均衡的效果来看，静态算法要好于动态算法，因为几乎所有的block都可以被均衡的使用，SSD的寿命会大大延长，但是静态算法的缺点是当数据迁移时，可能会导致写性能下降。

  Wear leveling 可以实现在存储设备的控制器中，也可以实现在文件系统中。CF卡、SD卡和SSD设备（MMC/eMMC?）都内置 Wear leveling功能。JFFS2, YAFFS, UDF 都是支持 Wear leveling 的文件系统。

## SSD/Flash 的写入寿命及其监视

参考[3-5, 19, 30]。

SSD 的写入寿命可以用两种方法表示：写入次数，或者最大写入量。最大写入量 = 写入次数 * 设备标称容量。目前大部分的SSD都是由MLC颗粒组成，一般的MLC只有3000次擦写寿命。
SSD 的实际寿命一般比标称值要高一些。到寿命前，SSD 几乎不会出现坏块；快到寿命时，坏块随着写入基本上线性增加；坏块增加到一定数量，则会全盘突然失效[19]。
所以刚开始出现坏块时，就应该立即转移数据并更换 SSD。

SSD 也支持类似机械硬盘的 SMART 功能，可以看到擦除次数、坏块数、剩余寿命等数据。

1. Media Wearout Indicator

    定义：表示SSD上NAND的擦写次数的程度，初始值为100，随着擦写次数的增加，开始线性递减，递减速度按照擦写次数从0到最大的比例。一旦这个值降低到1，就不再降了，同时表示SSD上面已经有NAND的擦写次数到达了最大次数。这个时候建议需要备份数据，以及更换SSD。

    解释：直接反映了SSD的磨损程度，100为初始值，0为需要更换，有点类似游戏中的血点。

    结果：磨损1点

2. Re-allocated Sector Count

    定义：出厂后产生的坏块个数，如果有坏块，从1开始增加，每4个坏块增加1

    解释：坏块的数量间接反映了SSD盘的健康状态。

    结果：基本上都为0

3. Host Writes Count

    主机系统对SSD的累计写入量，每写入65536个扇区raw value增加1

    解释：SSD的累计写入量，写入量越大，SSD磨损情况越严重。每个扇区大小为512bytes，65536个扇区为32MB

    结果：单块盘40T

### 硬盘 SMART 数据工具
smartmontools 工具
```
sudo apt install smartmontools
sudo smartctl /dev/sda -x -d sat # SATA 盘
sudo smartctl /dev/nvme0n1 -x -d nvme # NVME 盘
sudo smartctl /dev/nvme0n1 -x # 自动识别盘类型
```

输出类似：
```
smartctl 7.1 2019-12-30 r5022 [x86_64-linux-5.15.0-105-generic] (local build)
Copyright (C) 2002-19, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Model Number:                       SAMSUNG MZVL21T0HCLR-00B00
Serial Number:                      S676NX0R905814
Firmware Version:                   GXA7401Q
PCI Vendor/Subsystem ID:            0x144d
IEEE OUI Identifier:                0x002538
Total NVM Capacity:                 1,024,209,543,168 [1.02 TB]
Unallocated NVM Capacity:           0
Controller ID:                      6
Number of Namespaces:               1
Namespace 1 Size/Capacity:          1,024,209,543,168 [1.02 TB]
Namespace 1 Utilization:            902,459,949,056 [902 GB]
Namespace 1 Formatted LBA Size:     512
Namespace 1 IEEE EUI-64:            002538 b911b4abbd
Local Time is:                      Wed Apr 24 11:51:05 2024 CST
Firmware Updates (0x16):            3 Slots, no Reset required
Optional Admin Commands (0x0017):   Security Format Frmw_DL Self_Test
Optional NVM Commands (0x0057):     Comp Wr_Unc DS_Mngmt Sav/Sel_Feat Timestmp
Maximum Data Transfer Size:         128 Pages
Warning  Comp. Temp. Threshold:     81 Celsius
Critical Comp. Temp. Threshold:     85 Celsius

Supported Power States
St Op     Max   Active     Idle   RL RT WL WT  Ent_Lat  Ex_Lat
 0 +     8.37W       -        -    0  0  0  0        0       0
 1 +     8.37W       -        -    1  1  1  1        0     200
 2 +     8.37W       -        -    2  2  2  2        0     200
 3 -   0.0500W       -        -    3  3  3  3     2000    1200
 4 -   0.0050W       -        -    4  4  4  4      500    9500

Supported LBA Sizes (NSID 0x1)
Id Fmt  Data  Metadt  Rel_Perf
 0 +     512       0         0

=== START OF SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

SMART/Health Information (NVMe Log 0x02)
Critical Warning:                   0x00
Temperature:                        47 Celsius
Available Spare:                    98%
Available Spare Threshold:          10%
Percentage Used:                    7%
Data Units Read:                    42,437,467 [21.7 TB]
Data Units Written:                 72,544,614 [37.1 TB]
Host Read Commands:                 1,319,110,039
Host Write Commands:                670,860,423
Controller Busy Time:               16,311
Power Cycles:                       277
Power On Hours:                     5,236
Unsafe Shutdowns:                   203
Media and Data Integrity Errors:    516
Error Information Log Entries:      516
Warning  Comp. Temperature Time:    0
Critical Comp. Temperature Time:    0
Temperature Sensor 1:               47 Celsius
Temperature Sensor 2:               53 Celsius

Error Information (NVMe Log 0x01, max 64 entries)
No Errors Logged
```
nvme 工具

```
sudo nvme error-log /dev/nvme0n1
```
输出类似
```
Error Log Entries for device:nvme0n1 entries:64
.................
 Entry[ 0]   
.................
error_count  : 0
sqid         : 0
cmdid        : 0
status_field : 0(SUCCESS: The command completed successfully)
parm_err_loc : 0
lba          : 0
nsid         : 0
vs           : 0
cs           : 0
.................
 Entry[ 1]   
.................
error_count  : 0
sqid         : 0
cmdid        : 0
status_field : 0(SUCCESS: The command completed successfully)
parm_err_loc : 0
lba          : 0
nsid         : 0
vs           : 0
cs           : 0
.................
```

## 数据持久性（热稳定性）
环境温度越高，Flash 存储器就越有可能丢失数据。
一份Intel的研究报告说，在30度情况下，数据经过52周即有可能出现数据丢失。如果我们把温度提高到55度，2周数据就有可能丢失！多么可怕的数字。
当然这是最低标准，而且要求很破旧的SSD都要遵守的原则。实际情况会好的多，但是也为我们敲响了警钟：SSD放着不动，数据可能会丢失的！[19]

## SSD 的性能
参考[7]。

根据Wikipedia和《Design Tradeoffs for SSD Performance》论文的数据，NAND Flash的基本性能指标大致如下：

     4K Page read latency: 25us

     4K Page write latency: 200us

     256K Block erase latency: 1.5ms

    相对于SSD产品升级换代带来的性能差异，基础NAND Flash的性能指标保持比较稳定。

    根据这一基础硬件的性能指标作些简单计算。

     256K Block read latency: 1.6ms，单NAND颗粒读带宽：160MB/s

     256K Block write latency: 13ms，单NAND颗粒写带宽：20MB/s

    回收一个空的Block：1.5ms

    回收一个近满(50个有效页面)的Block：(25us + 200us) * 50 + 1.5ms = 13ms 

## SSD 的预留空间（Over Provisioning）
参考[8,20]。

SSD上的OP指的是用户不可操作的容量，大小为实际容量减去用户可用容量，OP区域一般被用于优化操作如：WL，GC和坏块映射等。

OP一般分三层（见下图）。第一层容量固定为SSD标称容量的7.37%，这是因为标称容量采用千进制为单位，而NAND颗粒容量单位为1024进制，两者正好相差约7.37%。这部分空间被默认用于作OP。第二层OP是否存在及容量大小取决于厂商设置。第三层OP是用户在日常使用中可以分配的，像Fusion-IO公司还给其用户提供工具让他们自行调节大小以满足不同的耐用度及性能要求，一般用户也可以自己在初次分区的时候，不把所有的SSD容量都分满来达到相同目的，此处有一点要注意，若非首次分区就留出空间，必须要先进行全盘擦除（参考“安全擦除”），否则没有效果。
       
虽然OP会减少SSD的可用容量，但有减少WA、提高寿命、性能的正面作用。
预留空间越大，写放大效应越小，写入性能也越好。高端企业级SSD的op空间巨大，Intel的SSD DC3700的op空间达到32%，这让它有了巨大的腾挪空间，它的写入曲线异常平滑[19]。

一般来说，建议在不支持Trim或对SSD的性能有较高要求的时候，如数据库等类的应用，增加一定比例的第三层OP容量（此空间需要先做HDD ERASE，保证没有被使用过），可以很好的起到保持速度，提高寿命的作用。（如必须用SSD组建RAID时，强烈建议留部分OP容量以弥补没有Trim产生的影响。）

## Trim
参考[9,10,17,18,21]。

Trim，其实是一个ATA指令，并无正式中文名称，操作系统发送此指令给SSD主控，以通知它哪些数据占用的地址是‘无效’的。
       在讲解Trim的重要性前，先说一点文件系统的相关知识。当我们在操作系统中删除一个文件时，系统并没有真正删掉这个文件的数据，它只是把这些数据占用的地址标记为‘空’，即可以覆盖使用。但这只是在文件系统层面的操作，硬盘本身并不知道那些地址的数据已经‘无效’，除非系统通知它要在这些地址写入新的数据。在HDD上本无任何问题，因为HDD允许覆盖写入，但到SSD上问题就来了，我们都已知道闪存不允许覆盖，只能先擦除再写入，要得到‘空闲’的闪存空间来进行写入，SSD就必须进行GC操作。在没有Trim的情况下，SSD无法事先知道那些被‘删除’的数据页已经是‘无效’的，必须到系统要求在相同的地方写入数据时才知道那些数据可以被擦除，这样就无法在最适当的时机做出最好的优化，既影响GC的效率(间接影响性能），又影响SSD的寿命。
       大家要牢记，Trim只是一个指令，它让操作系统通知SSD主控某个页的数据已经‘无效’后，任务就已完成，并没有更多的操作。Trim指令发送后，实际工作的是GC机制。Trim可减少WA的原因在于主控无需复制已被操作系统定义为‘无效’的数据（Trim不存在的话，主控就不知道这些数据是无效的）到‘空闲’块内，这代表要复制的‘有效’数据减少了，GC的效率自然也就提高了，SSD性能下降的问题也就减弱了。其实Trim的意义在于它能大量减少“有效”页数据的数量，大大提升GC的效率。特别是消费级的SSD由于一般OP空间较少，因此相对于有大量OP空间的企业级SSD来说，Trim显得尤其重要。
       Trim的作用可参考下图。
       
       Trim指令只有在操作系统进行删除、格式化等操作时候才会发送。Trim指令目前还不支持发送给RAID阵列中的SSD。（操作系统下进行的软件RAID可以支持）。
       Trim的支持需要3个要素：
       1.SSD主控制器和当前搭配的固件需要支持Trim命令接收。
       2.当前操作系统需要支持Trim指令发送。（Win7/2008R2及后续版本）
       3.当前使用的磁盘驱动程序必须支持Trim命令传输。
       只有同时满足以上3个条件，才能做到系统Trim命令发送，驱动传输Trim命令，SSD固件接收到传输来的Trim命令。
       Trim目前不支持以下几种情况:（可能还有更多）
       1. Trim目前不支持RAID阵列中的SSD。（操作系统下进行软件RAID除外）
       2. Trim目前不支持磁盘镜像文件内操作。（VM等虚拟机软件使用的虚拟磁盘文件）
       3. Trim目前不支持加密的文件系统。（以防止暴露加密文件系统信息）

在 ext4 等文件系统中，mount 选项 discard 用于启用 TRIM 功能（详见“文件系统优化”）。discard 选项导致每次删除文件时发送 TRIM 命令，这对IO性能有一定的负面影响。

linux 提供了 fstrim 工具，可以 trim 挂载中的整个文件系统[18,21]。用法：
fstrim -v <mount_point>
将会输出实际 trim 的块数。

不过，USB盘（USB读卡器？）似乎不支持 TRIM。

## SSD 的接口
[22]
mSATA，SATA Express，M.2，U.2, UFS, NVMe

* M.2 / NGFF
  大部分采用单块 PCB 卡的形式，尺寸有长短不一的几种：22X30mm、22X42mm、22X60mm、22X80mm 。也称为 2280 尺寸 [25]。
  通道为 PCIE x2/x4 或 SATA[26,27]。

* mSATA
  mSATA 采用 m-PCIE 的物理接口，但针脚的定义不同，采用 SATA 的通道[24]，早期有m-PCIE接口的固态硬盘，走的是PATA的信号，速度较慢。
  大部分采用单块 PCB 卡的形式，尺寸 50.8×29.8 mm [25]，但也有长度减半的。
  
* U.2
  采用 SATA 的物理接口，但采用 PCIE x4 的通道[26,27]。


* UFS（Universal Flash Storage
  目标是取代eMMC。在2008年，JEDEC就开始为并行的eMMC寻找串行方案。到了2012年，UFS 1.0诞生了，知道今天已经是2.1版了。
  UFS相较eMMC最大的不同是并行信号改为了更加先进的串行信号，从而可以迅速提高频率，同时半双工改为全双工。

* NVMe（NVM Express）
  目标是替换掉SATA接口。早在2009年Intel就开始着手寻找SATA的替代方案，在2011年有了1.0。最新的版本是1.2.1。
  NVMe实际上是一种通讯协议，在通讯协议里是应用层，它使用PCIe协议作为数据和链路层。
  NVMe是为SSD而生。在此之前SSD都用SATA接口。有人会好奇SATA也是串行接口，为啥还要创造个新的接口。SATA接口采用AHCI规范，其已经成为制约SSD速度的瓶颈。AHCI只有1个命令队列，队列深度32；而NVMe可以有65535个队列，每个队列都可以深达65536个命令。NVMe也充分使用了MSI的2048个中断向量优势，延迟大大减小。

市场上也存在一些转接卡，例如 M.2 转 SATA，M.2 转 PCI-E 等，不过 M.2 转 mSATA 的没有见到。


## 文件系统优化
在 linux 上，可以针对 SSD 做一些优化：
* ext4 的挂载选项采用 noatime 或者 relatime 选项来减少写入。写入次数 atime > relatime > noatime。
  另外还可以加上 nodiratime。
* ext4 的挂载选项采用 discard（无参数）。这对即时的IO性能有一定的负面影响，但能保证长期性能稳定。linux 的 vfat 和 ntfs-3g 也支持 discard 选项[23]。
* 周期性的运行 fstrim。这个与 discard 不必同时使用。fstrim 的耗时与需要 trim 的块数正相关。fstrim 应该至少支持那些支持 discard 的文件系统。
* IO 调度器采用 deadline 或者 noop （可通过 /sys/block/sda/queue/scheduler 查看或修改）。一般默认的是 cfq。

## 全盘擦除/安全擦除（secure erase）
SSD 的安全擦除（secure erase）或叫做全盘擦除是指将全部存储单元擦除(erase），回到可用状态。
[16]


## 固件更新
以三星PM9A1为例[31]。
linux 上，`sudo nvme list` 命令（nvme-cli包）可以显示当前固件版本。
