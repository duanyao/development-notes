## 参考资料
[2.1] https://stackoverflow.com/questions/61783369/install-openjdkopenjfx-8-on-ubuntu-20

## 安装
java 10 以前，javafx 是 jdk 的一部分。但是在 debian 等系统上，openjdk10- 是没有包含 javafx 的。可以用 sdkman 来安装包含 javafx 的版本：

```
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"

sdk list java
sdk install java 8.0.272.fx-zulu  # 版本号中的 fx 表示包含了 javafx 。
```

