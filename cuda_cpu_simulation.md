## 参考资料

GPGPU-Sim
http://www.gpgpu-sim.org/

Compile cuda code for CPU
https://stackoverflow.com/questions/21946701/compile-cuda-code-for-cpu

gpgpu-sim
https://github.com/gpgpu-sim/

gpgpu-sim/pytorch-gpgpu-sim 
https://github.com/gpgpu-sim/pytorch-gpgpu-sim

[1.1] gpgpu-sim /gpgpu-sim_distribution
  https://github.com/gpgpu-sim/gpgpu-sim_distribution

GPU Ocelot
http://gpuocelot.gatech.edu/faq/

## 安装

前提：cuda 需要被安装。假设 cuda 安装到了 /usr/local/cuda 。

cd ~/t-project
git clone https://github.com/gpgpu-sim/gpgpu-sim_distribution.git gpgpu-sim_distribution.git
cd gpgpu-sim_distribution.git
export CUDA_INSTALL_PATH=/usr/local/cuda
source setup_environment
make -j4

cd ~/t-project
git clone https://github.com/gpgpu-sim/pytorch-gpgpu-sim.git pytorch-gpgpu-sim.git
cd pytorch-gpgpu-sim.git
git -c submodule."third_party/nervanagpu".update=none submodule update --init

export CUDNN_INCLUDE_DIR=/usr/include/x86_64-linux-gnu/
export CUDNN_LIBRARY=/usr/lib/x86_64-linux-gnu/
export TORCH_CUDA_ARCH_LIST="6.1+PTX"

