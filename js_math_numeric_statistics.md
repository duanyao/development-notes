## 参考资料

[1.0] 通用

[1.1] a standard library for JavaScript and Node.js, with an emphasis on numerical and scientific computing applications. 
  https://github.com/stdlib-js/stdlib

[1.2] Math.js is an extensive math library for JavaScript and Node.js
  https://github.com/josdejong/mathjs

[1.2.1] Math.js Function reference 
  https://mathjs.org/docs/reference/functions.html

[2.0] 统计学
[2.1] jstat native javascript implementations of statistical functions
  https://github.com/jstat/jstat

[2.2] jStat: A Lightweight Statistical JavaScript Library
  https://www.infoq.com/news/2011/08/jStat/

[2.3] Statistical methods in readable JavaScript
  https://simplestatistics.org/

[2.4] a library for performing some basic math and statistics calculations
  https://github.com/pseudosavant/psMathStats

[3.0] 数值计算/科学计算/矩阵计算/AI计算

[3.1] scijs 科学计算库目录
  https://github.com/scijs
  http://scijs.net/packages/

[3.2] multidimensional arrays for JavaScript.
  https://github.com/scijs/ndarray

[3.3] node-lapack
  https://github.com/NaturalNode/node-lapack

[3.4] TensorFlow.js Core WebGL-accelerated ML // linear algebra // automatic differentiation for JavaScript.
  https://github.com/tensorflow/tfjs-core

[3.5] nicolaspanel/numjs Like NumPy, in JavaScript 
  https://github.com/nicolaspanel/numjs

[3.6] JavaScript Numpy
  https://www.npmjs.com/package/jsnumpy

[4.0] 回归、拟合
[4.1] regression-js 线性、多项式回归
  https://github.com/Tom-Alexander/regression-js

[5.0] 符号计算

## 通用

### stdlib
[1.1]
stdlib (/ˈstændərd lɪb/ "standard lib") is a standard library for JavaScript and Node.js, with an emphasis on numerical and scientific computing applications. The library provides a collection of robust, high performance libraries for mathematics, statistics, data processing, streams, and more and includes many of the utilities you would expect from a standard library.

## 统计学
jstat[2.1-2.2] 是 R 程序的部分移植。

simple-statistics.js 是另外一个。

### jstat
```
var jStat=require('jStat')

var b=jStat.create(3, 2, (r, c)=> r+c)

var a=jStat(0,1,5)
```

函数
.sum(toScalar=false) // 按列求和。注意如果行数不超过1行，就自动变成标量结果，这有时可能不是你想要的。
.sumsqrd(toScalar=false) // 按列求平方和
.sumsqerr(toScalar=false) // 按列求方误差（square error）之和。严格来说这不是方误差,因为是将平均值假定为真值的.
注意，如果是二维的，toScalar=true 是先按列求方误差之和，形成的向量再求方误差之和，所以 jStat([[1,2],[3,4]]).sumsqerr( true ) === 0

.sumrow(toScalar=false) // 按列求和

.product(toScalar=false) // 按列求积

.min(toScalar=false) // 按列最小
.max(toScalar=false) // 按列最小

.mean(toScalar=false) // 按列平均

.meansqerr(toScalar=false) // 按列求均方误差（mean squared error）。
注意，如果是二维的，toScalar=true 是先按列求均方误差，形成的向量再求均方误差，所以 jStat([[1,2],[3,4]]).meansqerr( true ) === 0

geomean() // 几何平均

median() // 中位数

cumsum() // 部分和

cumprod() // partial products

diff() // 与前一元素之差

.rank() // 排名 jStat.rank([1, 2, 2, 3]) === [1, 2.5, 2.5, 4]; jStat.rank([1, 2, 2, 2, 3]) === [ 1, 3, 3, 3, 5 ]

.mode() // 众数

.range() // 最大和最小之差

.variance(sampleVariance = false) // 方差(与 meansqerr 有区别吗?), sampleVariance 的区别是除以 N-1 而不是 N

.deviation() // 偏差,与平均值之差

.stdev(toScalar=false) // （按列）标准差。如果 toScalar=true，将各列的标准差再求标准差。

.histogram( dataArray[, numBins] ) // 直方图  jStat.histogram([100, 101, 102, 230, 304, 305, 400], 3) === [3, 1, 3];

vec1.dot(vec2) // 内积, 或者 jStat.dot( arr1, arr2 )

jStat.norm(a) // 模

jStat.sum([1,2,3]) // 求和，只适用于一维数组。高维数组得到奇怪的字符串结果，不知是不是bug。高维数组求和应该使用实例方法，如 jStat([[1,2],[3, 4]]).sum() 。

jStat.mean([1,2,3]) // 求平均，只适用于一维数组。

jStat.stdev([1,2,3]) // 求标准差，只适用于一维数组。

jStat.max([1,2,3]) // === 3
jStat.max( [[1,2],[3,4]] ) // 用于矩阵时似乎有 bug，请使用实例版本。

jStat.subtract(a, b) // 差，适用于标量、向量或混合
jStat.add(a, b) // 和，适用于标量、向量或混合
jStat.multiply(a, b) // a 是向量，b是标量。如果b也是向量则行为不明确。
jStat.divide(a, b) // a 是向量，b是标量。如果b也是向量则行为不明确。

jStat.norm(jStat.subtract([2,2,2], [1,0,1])) // 求点的距离

vec1.angle(vec2) // 相对角度, 单位弧度. 可以是二维或三维. 或者 angle( arr1, arr2 )

jStat.zscore( value, mean, sd ) // z-score of value given the mean mean and the sd standard deviation of the test.
// 1-delta 得1分

jStat.tscore( value, mean, sd, n )

jStat.normal.cdf(value, mean, std) // normal：正态分布，cdf：累积分布函数(Cumulative Distribution Function)，即小于 value 的值出现的概率。mean 和 std 是平均值和标准差。

jStat.normal.inv( p, mean, std ) // cdf 的反函数。输入小于某值的值出现的概率p，返回这个值。

jStat.normal.pdf(value, mean, std) // normal：正态分布，pdf：概率密度函数，即在取值点 value 附近的可能性。pdf 的全域积分等于1。

jStat.normal.sample(mean, std) // 返回一个符合指定的正态分布的随机数，mean 和 std 是平均值和标准差。

## 向量计算
### Math.js
math.subtract(a, b) // 适用于标量和向量、矩阵
math.distance([2,2,2], [1,0,1]) === 2.449 // 点的距离
math.norm([1,0,1]) // 向量的模
math.dot([1,0,1], [1,2,0]) // 内积

向量夹角没有直接给出，间接计算：cos=a·b/(|a|*|b|)

### jStat
jStat.subtract(a, b) // 差，适用于标量、向量或混合
vec1.dot(vec2) // 内积, 或者 jStat.dot( arr1, arr2 )
jStat.norm(jStat.subtract([2,2,2], [1,0,1])) // 求点的距离
vec1.angle(vec2) // 相对角度, 单位弧度. 可以是二维或三维. 或者 angle( arr1, arr2 )

## 矩阵计算
### jStat
jStat.transpose( [[1,2],[3,4]] ) // == [[1,3],[2,4]] 转置。或者 jStat( [[1,2],[3,4]] ).transpose() === jStat([[1,3],[2,4]])

## 数值计算/科学计算/矩阵计算/AI计算

stdlib 适用于数值计算[1.1].

scijs 科学计算库目录[3.1].

ndarray 多维数组库[3.2], 类似 python 的 numpy 库.

numjs [3.5] 是另一个类似 numpy 的库.

node-lapack A node.js wrapper for the high-performance LAPACK linear algebra library [3.3].

TensorFlow.js Core [3.4] 目前只适用于浏览器环境, 依赖 WebGL.

## 符号计算
Math.js 支持这符号计算[1.2].