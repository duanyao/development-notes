## 参考资料

[5.1] building with qmake on Linux - how to prevent qmake from linking to QtCore and QtGui
https://stackoverflow.com/questions/2296514/building-with-qmake-on-linux-how-to-prevent-qmake-from-linking-to-qtcore-and-q

## 安装 qt 开发包

debian: libqt4-dev

centos7: qt-devel  # qt4

## qmake 版本和安装

不同的大版本 qt 会配套不同的 qmake 版本，debian 包名是 qt4-qmake 和 qt5-qmake 。
执行 qmake 命令时，根据环境变量 QT_SELECT=qt4|qt5 来选择具体的版本。

命令名则是 /usr/bin/qmake-qt4 和 /usr/lib/qt5/bin/qmake

## qmake 基本使用
```
export QT_SELECT=qt5 #影响 qmake 命令
mkdir build
cd build
qmake ..
make -j4
```

## qmake 编译调试版

修改 XXX.pro ，开头增加一行：

```
CONFIG += debug
```
这样增加了调试符号。

## 修改直接依赖的库
[5.1]

默认情况下，qmake 自动添加对 libQtCore, libQtGui 的依赖，包括 -lQtCore -lQtGui 链接参数。
有时候一个程序实际上不依赖 QtGui，可以这样在 .pro 文件中减少、增加依赖的Qt 库：

```
QT += network
QT -= gui
```
network 表示 libQtNetwork ，gui 表示 libQtGui 。

## qt plugins
qt plugins 加载失败时，可能不会输出有用的错误信息，这时应该设置

```
export QT_DEBUG_PLUGINS=1
```

然后重新运行程序，例如：

```
sqlitestudio

Found metadata in lib /home/duanyao/opt/SQLiteStudio/platforms/libqxcb.so, metadata=
{
    "IID": "org.qt-project.Qt.QPA.QPlatformIntegrationFactoryInterface.5.3",
    "MetaData": {
        "Keys": [
            "xcb"
        ]
    },
    "archreq": 0,
    "className": "QXcbIntegrationPlugin",
    "debug": false,
    "version": 331520
}


QLibraryPrivate::loadPlugin failed on "/home/duanyao/opt/SQLiteStudio/platforms/libqxcb.so" : "Cannot load library /home/duanyao/opt/SQLiteStudio/platforms/libqxcb.so: (libxcb-util.so.1: 无法打开共享对象文件: 没有那个文件或目录)"
qt.qpa.plugin: Could not load the Qt platform plugin "xcb" in "" even though it was found.
This application failed to start because no Qt platform plugin could be initialized. Reinstalling the application may fix this problem.

Available platform plugins are: xcb.
```

然后用 `apt-file search libxcb-util.so.1` 查找缺少的包。

## 界面设计器
GUI 界面设计器: Qt 设计师； Qt designer； /usr/lib/qt5/bin/designer, qttools5-dev-tools 。

命令行.ui文件编译器：
* uic, /usr/lib/qt5/bin/uic, qtbase5-dev-tools, 可以编译出 c++ 和 python 代码。
* pyuic5, pip pyqt5-tools, 可以编译出 python 代码。
