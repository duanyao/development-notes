[2.1] http://valgrind.org/docs/manual/manual-core.html#manual-core.whatdoes

[2.2] https://stackoverflow.com/questions/16847385/valgrind-to-detect-memory-leak-in-child-process

[2.3] http://valgrind.10908.n7.nabble.com/Valgrind-can-attach-to-a-running-process-td44092.html

[2.4]
https://stackoverflow.com/questions/30499809/valgrind-memory-leak-reachable

[3.1] Massif: a heap profiler
https://www.valgrind.org/docs/manual/ms-manual.html

[3.2]  KDE / massif-visualizer 
https://github.com/KDE/massif-visualizer

[4.1] valgrind DHAT: a dynamic heap analysis tool
https://www.valgrind.org/docs/manual/dh-manual.html

[10.1] https://github.com/mono/mono/issues/7114

## valgrind memcheck
[2.1]
valgrind --tool=memcheck ls -l

跟踪子进程，内存泄露检查[2.2]：
valgrind --tool=memcheck --leak-check-full --trace-children=yes spawn-fcgi -s tempSocket ./myApp param1 param2 param3

内存泄露检查，日志文件。
valgrind --tool=memcheck --log-file=/home/xxx/valgrind_report_%p.log --leak-check=full --show-leak-kinds=all --show-reachable=no ./myApp param1 param2 param3

–log-file
指定报告输出文件。在 linux 上似乎不支持相对路径，总是在前面加上 '//'。%p 展开为进程id。
–track-origins=yes
是否显示未定义的变量，在堆、栈中被定义没有被initialised的变量都被定义成origins。默认是关闭这个option的。
–show-leak-kinds=all
这里可以支持的选项有[definite|possible]，一般只需要去关注definite（绝逼），possible是可能会存在。
–leak-check=full
当服务器退出时是否收集输出内存泄漏，选项有[no|summary|full]这个地方我们将其设置成全输出，默认将会使用summary方式。
--show-reachable=yes|no
是否将退出时仍然可达的动态内存视为泄漏[2.4]。

valgrind 必须在程序启动时执行，而不是像调试器、strace那样可以中途附加到进程。

## valgrind Massif
Massif is a heap profiler. It measures how much heap memory your program uses. 

valgrind --tool=massif prog

输出文件 massif.out.12345

Massif 默认测量 malloc 等分配函数，不测量 mmap, mremap, brk 等低层分配函数。要测量后者，使用
-pages-as-heap=yes

### massif-visualizer

massif-visualizer 是图形化显示 Massif 输出的工具。
包名 massif-visualizer 或者 org.kde.massif-visualizer 。

## valgrind DHAT
It tracks the allocated blocks, and inspects every memory access to find which block, if any, it is to. It presents, on an allocation point basis, information about these blocks such as sizes, lifetimes, numbers of reads and writes, and read and write patterns.

## 调试技巧

You have a double free or buffer overrun in your program? These can be somewhat narrowed down by removing the frees and/or increasing the sizes passed to malloc/calloc.

## 其他工具

Dr. Memory

purify

bounds checker

AddressSanitizer + LeakSanitizer
