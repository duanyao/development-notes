[1] npm太慢， 淘宝npm镜像使用方法
  https://blog.csdn.net/quuqu/article/details/64121812

## npm镜像

1.临时使用

npm --registry https://registry.npmmirror.com install express

其它镜像站：
https://registry.npmjs.org/

    1

2.持久使用

npm config set registry https://registry.npmmirror.com

    1

    配置后可通过下面方式来验证是否成功
    npm config get registry
    或
    npm info express

