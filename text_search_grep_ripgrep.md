## 参考资料
[3.1] https://stackoverflow.com/questions/2908757/count-number-of-occurrences-of-a-pattern-in-a-file-even-on-same-line

[4.1] https://github.com/BurntSushi/ripgrep/releases/tag/0.9.0

## ripgrep, rg
debian 包名： ripgrep
用法：
```
NAME
       rg - recursively search current directory for lines matching a pattern

SYNOPSIS
       rg [OPTIONS] PATTERN [PATH...]

       rg [OPTIONS] -e PATTERN... [PATH...]

       rg [OPTIONS] -f PATTERNFILE... [PATH...]

       rg [OPTIONS] --files [PATH...]

       rg [OPTIONS] --type-list

       command | rg [OPTIONS] PATTERN

       rg [OPTIONS] --help

       rg [OPTIONS] --version

OPTIONS
       -A, --after-context NUM
           Show NUM lines after each match.
       -B, --before-context NUM
           Show NUM lines before each match.
       -s, --case-sensitive
           Search case sensitively.
       -C, --context NUM
           Show NUM lines before and after each match.
       -l, --files-with-matches
           Only print the paths with at least one match.
       -H, --with-filename
           Display the file path for matches. This is the default when more than one file is searched. 
       --sort SORTBY
           This flag enables sorting of results in ascending order. The possible values for this flag are:

               path        Sort by file path.
               modified    Sort by the last modified time on a file.
               accessed    Sort by the last accessed time on a file.
               created     Sort by the creation time on a file.
               none        Do not sort results.
```

### 转义字符
以下字符需要转义：`()`

## 给出字符串的出现次数

用 grep -o 和 wc -l [3.1]

```
$ echo afoobarfoobar | grep -o foo
foo
foo

$ echo afoobarfoobar | grep -o foo | wc -l
2
```

用 rg(ripgrep)：[3.1]

```
> echo afoobarfoobar | rg --count foo
1
> echo afoobarfoobar | rg --count-matches foo
2
```

ripgrep 的安装： sudo apt install ripgrep

注意，只有 0.9 以上版本支持 --count-matches 选项[4.1]，而 linux 发行版自带的版本可能比较老。

rg -oc 与 rg --count-matches 等效。
