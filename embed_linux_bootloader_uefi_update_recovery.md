[1]  vivi、uboot、eboot的区别
http://blog.chinaunix.net/uid-24927314-id-2546446.html

[2] NAND flash和NOR flash的区别详解
http://www.elecfans.com/bandaoti/cunchu/20120925290176.html

[3] Is it possible to boot Linux on a x86 machine with uboot?
https://superuser.com/questions/723112/is-it-possible-to-boot-linux-on-a-x86-machine-with-uboot

[4] BIOS、BootLoader、uboot对比
http://blog.csdn.net/thinkandchange/article/details/23132643

[5] Booting
https://en.wikipedia.org/wiki/Booting#Personal_computers_.28PC.29

[6] U-Boot -- the Universal Boot Loader 
http://www.denx.de/wiki/U-Boot/WebHome

[7] U-Boot on x86
http://www.denx.de/wiki/U-Boot/X86

[8] A Handy U-Boot Trick
https://www.linuxjournal.com/content/handy-u-boot-trick

[9] Comparison of boot loaders
https://en.wikipedia.org/wiki/Comparison_of_boot_loaders

[10] Unified Extensible Firmware Interface (简体中文)
https://wiki.archlinux.org/index.php/Unified_Extensible_Firmware_Interface_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)

[11] Arch boot process
https://wiki.archlinux.org/index.php/Arch_boot_process

[12.1] UEFI+GPT引导基础篇（一）：什么是GPT，什么是UEFI？
http://www.iruanmi.com/what-is-gpt-and-what-is-uefi/

[12.3] The EFI System Partition and the Default Boot Behavior
https://blog.uncooperative.org/blog/2014/02/06/the-efi-system-partition/

[12.3] Extensible Firmware Interface
https://www.csee.umbc.edu/portal/help/architecture/idfefi.pdf

[12.4] UEFI和BIOS探秘
https://www.zhihu.com/column/UEFIBlog

[14] U-Boot Environment Variables
http://www.denx.de/wiki/view/DULG/UBootEnvVariables
[15] U-Boot env from Linux WITHOUT MTD
https://unix.stackexchange.com/questions/322043/u-boot-env-from-linux-without-mtd

[16] GNU GRUB Manual 2.00
https://www.gnu.org/software/grub/manual/html_node/index.html

[17] coreboot
https://en.wikipedia.org/wiki/Coreboot

[18] Enabling OTA updates for the AGL Reference Platform
https://schd.ws/hosted_files/aglmembermeeting2016/f3/Enabling%20OTA%20updates%20for%20the%20AGL%20Reference%20Platform.pdf

[19] Linux-Based Firmware, how to implement a good way to update?
https://stackoverflow.com/questions/5167226/linux-based-firmware-how-to-implement-a-good-way-to-update?lq=1

[20] [agl-discussions] [RFC] Device-side support for software update in AGL
https://lists.linuxfoundation.org/pipermail/automotive-discussions/2016-May/002061.html

[21] Software Update on Embedded Systems
https://events.linuxfoundation.org/sites/events/files/slides/SoftwareUpdateForEmbedded.pdf

[22] SWUpdate - Software Update for Embedded Systems
https://github.com/sbabic/swupdate
https://sbabic.github.io/swupdate/

[23] Making a recovery partition in embedded Linux
https://unix.stackexchange.com/questions/73597/making-a-recovery-partition-in-embedded-linux

[24] A Comparison of Linux Software Update Technologies
https://events.linuxfoundation.org/sites/events/files/slides/Comparison%20of%20Linux%20Software%20Update%20Technologies_0.pdf

[25] How do you update your embedded Linux devices?
https://events.linuxfoundation.org/sites/events/files/slides/linuxcon-japan-2016-softwre-updates-sangorrin.pdf

[26] swupd-client
https://github.com/clearlinux/swupd-client

[27] Mender: over-the-air updater for embedded Linux devices
https://github.com/mendersoftware/mender

[28] ostree libOSTree is a library and suite of command line tools that combines a "git-like" model for committing and downloading bootable filesystem trees, along with a layer for deploying them and managing the bootloader configuration.
https://github.com/ostreedev/ostree

[29] Snapcraft.io Package any app for every Linux desktop, server, cloud or device, and deliver updates directly.
https://snapcraft.io/

[30] QtOTA 基于 ostree
https://doc.qt.io/QtOTA/

[31] swupd Guide
https://git.yoctoproject.org/cgit/cgit.cgi/meta-swupd/tree/docs/Guide.md

[32] SWUpdate Project’s road-map
https://sbabic.github.io/swupdate/roadmap.html

[41] OpenEmbedded, the build framework for embedded Linux.
http://www.openembedded.org/wiki/Main_Page

[42] OpenEmbedded in the Real World
https://events.linuxfoundation.org/sites/events/files/slides/oe_in_the_real_world_smurray_elc2016.pdf

[43] Yocto Project uses a build host based on the OpenEmbedded (OE) project, which uses the BitBake tool, to construct complete Linux images.
https://www.yoctoproject.org/

[46] Ubuntu Core
https://www.ubuntu.com/core

[50] Clear Linux
https://clearlinux.org/

[53] Tiny Core Linux
http://tinycorelinux.net/

[54] A look at Tiny Core Linux
http://tinycorelinux.net/corebook.pdf

[55] dCore, a minimal live Linux system based on Micro Core (Tiny Core Linux) that uses scripts to download select packages directly from vast Debian or Ubuntu repositories and convert them into useable SCEs (self-contained extensions)
http://wiki.tinycorelinux.net/dcore:welcome

[56] Alpine Linux is a security-oriented, lightweight Linux distribution based on musl libc and busybox.
https://alpinelinux.org/

[57] Alpine on ARM
https://wiki.alpinelinux.org/wiki/Alpine_on_ARM

[58] alpine please provide x86/x86_64 image with uboot
https://bugs.alpinelinux.org/issues/5758

[59] Create UEFI boot USB
https://wiki.alpinelinux.org/wiki/Create_UEFI_boot_USB

[61] OpenWrt is a highly extensible GNU/Linux distribution for embedded devices (typically wireless routers). 
https://wiki.openwrt.org/

[62] Openwrt on x86 compatible systems
https://wiki.openwrt.org/inbox/doc/openwrt_x86

[64] Resin.io brings the benefits of Linux containers to the IoT. 
https://resin.io/

[66] Ostro OS is tailored for Intel IoT smart devices using Yocto Project tools. 
https://ostroproject.org/
[67] EFI support in Ostro? OS
https://ostroproject.org/documentation/architecture/efi-boot.html

[81] watchdog in Linux
http://blog.sina.com.cn/s/blog_4dff871201012yzh.html

[82] 使用 watchdog 构建高可用性的 Linux 系统及应用
https://www.ibm.com/developerworks/cn/linux/l-cn-watchdog/index.html

[101] Boot sector
https://en.wikipedia.org/wiki/Boot_sector

[102] GUID Partition Table
https://en.wikipedia.org/wiki/GUID_Partition_Table

[103] Boot windows from grub rescue command prompt
https://unix.stackexchange.com/questions/71555/boot-windows-from-grub-rescue-command-prompt

[104] Does the UEFI partition either “MUST” or “SHOULD” be first for some reason? If so why?
https://askubuntu.com/questions/618244/does-the-uefi-partition-either-must-or-should-be-first-for-some-reason-if-s

[105] Fix UEFI Boot: Fix for Windows 7, 8, 8.1, 10
https://neosmart.net/wiki/fix-uefi-boot/

[111] Managing EFI Boot Loaders for Linux: Using gummiboot
http://www.rodsbooks.com/efi-bootloaders/gummiboot.html

[121] Container Linux quick start
https://coreos.com/os/docs/latest/quickstart.html
https://coreos.com/os/docs/latest/booting-on-vagrant.html

## boot loader 总论
bootloader主要的必须的作用只有一个:就是把操作系统映像文件拷贝到RAM中去，然后跳转到它的入口处去执行。而操作系统文件的来源，可以是flash,sd card,PC(可以通过网络，USB，甚至串口传输）等等，所谓的EBOOT,UBOOT，其实就是表明了系统文件是通过Ethernet或者USB从PC传输过去的 [1]。

bootloader并不是必须的，如果我们的硬件有足够大的norflash，并且实现了XIP技术，那么WinCE 操作系统可以直接在norflash里面运行起来，不需要将它复制到RAM中去，所以bootloader就失去了作用。但是考虑到成本因素，现在的硬件一般都不会配置这么大的 nor flash，image文件都存储在nand flash里面，所以都会用到bootloader [2]。

nor flash 和 nand flash 是两种类型的 flash 存储器。除了读写速度上的差异，最重要的差别是 NOR 支持芯片内执行（XIP, eXecute In Place），而 NAND 必须将文件复制到内存中才能执行，像磁盘一样。目前大量使用的SD卡、固态硬盘都采用 NAND，而 NOR 主要用于运行启动代码[2]。

### 分区
持久存储设备可以分为分区的（大部分硬盘）和不分区的（软盘）。有的设备即可以分区也可以不分区（USB盘）。
不论是否分区，都可以用来安装操作系统并安装引导管理器。
X86 机器的分区方式有 MBR 和 GPT 两种。

### BIOS 和 UEFI
BIOS 和 UEFI 都是 X86 设备的引导前（pre-boot）的执行代码。UEFI 是 BIOS 的后继，但有些场合把 UEFI 也叫做一种 "BIOS"。
UEFI 的相关资料见[12.4]。

BIOS 和 UEFI 与 boot loader 和操作系统有一定的依赖关系，支持 UEFI 的 boot loader 和操作系统才能在使用 UEFI 的设备上运行。
仅支持 BIOS 的 boot loader 有 LILO，仅支持 UEFI 的有 systemd-boot，都支持的有 grub2。

在采用 BIOS 的设备上，boot loader 只能位于 MBR 中，这也要求磁盘分区表采用 MBR 格式。
在采用 UEFI 的设备上，boot loader 可以位于 MBR 中，也可以位于 GPT 分区中；分区格式可以是 MBR 格式也可以是 GPT 格式。
不过，Windows 要求采用 UEFI 的设备必须同时使用 GPT 分区。大部分新的 PC 可以在 BIOS 和 UEFI 模式之间切换。[12.1]

Intel NUC 设备采用 UEFI。

UEFI 自身可以访问文件系统（只限于FAT 和 ISO9660 等少数）以及执行其中的文件（称为 UEFI 应用），因此可以直接引导操作系统内核（不需要通过boot loader）。
不过，这种做法不如通过 boot loader 来的灵活。

UEFI 使用 "EFI 变量（variables）" 来决定自身的行为。变量可以被UEFI自身或操作系统修改，并且是持久的（存放在主板的存储器里）。
其中最重要的是 "BootOrder" 和 Boot#### 变量，决定了 UEFI 执行的 UEFI 应用。如果不能通过 BootOrder 找到 UEFI 应用，或者需要尝试从移动存储设备上引导，
会去搜索 ESP（EFI System Partition）。ESP 并没有什么特殊性，唯一的要求仅仅是采用 FAT 文件系统。
UEFI 应用应该放在 ESP 下的 /EFI/<VENDOR NAME>/ 目录，boot loader 可以放在这里。例如，grub 及其配置文件可以放在 /EFI/grub/ 下面。
默认的，或者说回退的 UEFI 应用是 \EFI\BOOT\BOOTX64.EFI (32 位系统上是 BOOTIA32.EFI)。[12.2]

如果有多个可引导的存储器或多个boot loader，UEFI 会提供菜单让用户选择。
如果在一个存储器上装多个系统，需要注意不要格式化 ESP，也不要去动其它操作系统的 boot loader。

UEFI 还有个 secure boot 功能，通常需要禁用这个功能才能安装其它操作系统。

linux 系统中，修改 EFI 变量的程序是 efibootmgr 。 

### 修复windows/linux的 UEFI 引导
[105]
windows 中，可以用 windows 安装盘或者pe盘启动系统，进入命令行模式，

```
bootrec /fixmbr                 #非必要
bootrec /fixboot
bootrec /ScanOs                 #非必要
bootrec /rebuildBcd
bcdboot C:\windows
bcdboot C:\windows /s V: /f UEFI   #非必要
```
此外，windows 安装盘的图形界面还有“修复启动”的功能，可以试试。

有 windows 安装盘 iso，要制作启动U盘，可以使用 rufus（只有windows版）。以前可以直接将 iso 内容复制到 FAT32 的 U 盘，现在（2021.11）不行了，因为 win10 iso 中有大于 4GB 的文件，因此 U 盘必须分2个区以上，一个 ESP 分区，一个 NTFS 分区存放windows安装盘。

### MBR 和 VBR [101]

对于MBR分区的设备，第一个扇区（512字节）是 MBR。MBR中除了可执行代码（占440字节），还包括分区表。
Volume Boot Record (VBR) (或者 volume boot sector, partition boot record, partition boot sector) 是位于分区头部的引导记录。
VBR也可以位于不分区设备的第一个扇区。

VBR 的格式与 MBR 相同。MBR 和 VBR 都是 X86 代码，是 X86 CPU 的机器上特有的。光盘有特定的引导区结构，不属于MBR/VBR格式。

启动时，BIOS 加载和执行设备的第一个扇区（对分区的是MBR，对不分区的是VBR）。MBR 代码确定一个活动分区，加载和执行它的 VBR。
VBR 的作用是加载和执行其所在分区上的一个程序，通常是 boot loader （即第二阶段 boot loader）。

boot loader 可以加载操作系统，也可以加载和执行一个 VBR，后者称为 chain load。

* MBR 的修复：在 windows 上，使用恢复模式，在命令行执行 bootrec /fixmbr。在 linux 上，sudo apt-get install lilo
sudo lilo -M /dev/sda mbr 。[103]

### GPT 和 ESP 分区
GPT 分区的设备的第一个扇区是保留的，且与 MBR 兼容，称为 "protective MBR"。所以可以从 BIOS 引导 GPT 分区上的操作系统[102]。

ESP 分区可以采用 FAT16 或 FAT32 格式。应注意，FAT32 分区的最小尺寸是 32MiB（mkfs.vfat 可以创建小于这个尺寸的分区，
linux也能挂载，但fsck.vfat 会给出警告，EFI也不一定能识别）。

ESP 分区的序号和位置没有限制，并不需要是第一个分区[104]。

ESP 分区的分区类型应设置为 ‘0xEF00’，GUID 应为 21686148-6449-6e6f-744e656564454649。这可以用 gdisk 来设置。
这个设置并不总是必要的，只是辅助 EFI 固件来查找 ESP 分区。如果在 EFI 固件中设定了 BootOrder，则分区类型并不重要；有的 EFI 固件也能搜索不具有 ESP 分区类型的分区。

### UEFI Shell
开机时按 F2，在 UEFI 的启动设置中选择 Boot Manager -> UEFI Shell 即可进入。不是所有的主板都有此入口。

一开始会列出所有的分区，每一行开头是分区名，类似 BLK0, BLK1 等。
用不带参数的 map 或 mount 命令可以再次列出所有的分区。输入 "BLK1:" 这样的命令可以进入分区的目录，但文件操作（dir）只对挂载了的
分区有效。UEFI 一般只能挂载 VFAT （FAT16/FAT32都可以）分区。

进入一个分区后，运行不带参数的 vol 命令，已挂载的分区会显示相关信息，无法挂载的会显示：
Error: the file 'BLKN:\' could not be opened.

常用命令：
  help -b  # 帮助。-b 表示分页显示，其他命令也大多支持这个选项。
  help -b <命令名> # 显示具体命令的帮助
  devices
  devtree # 显示设备树。能识别的分区显示为 FAT filesystem 字样，不能识别的显示
  dh

### U-Boot
U-Boot 使用 U-Boot 环境变量（Environment Variables）来决定自身的行为。这与 EFI 变量、unix shell 变量很相似。

U-Boot 环境变量是持久存储的，运行时加载到内存。它可以被用户空间的程序修改。
存储的方法主要是 MTD 设备，但也可以配置成从普通文件系统（如FAT）的文件中加载变量。

U-Boot 主要是在非 X86 系统中使用。在 X86 系统上，推荐的做法是用 coreboot + U-Boot，不采用 EFI。
也可以用 EFI + U-Boot，U-Boot 作为 EFI 应用，但这个做法似乎还不够成熟。

X86系统中的应用主要包括：
Chrome OS 设备，采用 coreboot + U-Boot + GPT 的方案，支持双分区更新。
Intel Edison，采用 U-Boot，详情不明，Edison 似乎没有 EFI。

### coreboot
coreboot 与传统 BIOS 和 UEFI 是替代关系。

不过 coreboot 的功能比较简单，主要是初始化硬件，所以需要其他模块来引导系统，称为“载荷”。载荷分这么几类：
* 实现传统 BIOS，如 SeaBIOS
* 实现 UEFI，如 TianoCore
* boot loader，如 grub 和 u-boot

不过，coreboot 支持的主板有限。

### Grub
[16]，并参考 grub.txt。

Grub 也有环境变量（Environment Variables）的概念，它是持久的，Grub bootloader 自身（load_env/save_env）或用户空间程序（grub-editenv）都可以修改它。
它存储在普通文件里（通常是 /boot/grub/grubenv），但限制是不能在特殊的分区上，例如LVM，RAID，ZFS，USB等。

Grub 有个“回退（fallback）”机制，可以在一个启动条目失败的情况下去启动另一个。“失败”的定义是（1）grub不能启动内核（2）内核发生panic。

Grub 支持 MBR 和 UEFI。

### gummiboot
一个 EFI Boot Loader。

### USB disk + UEFI
参考 [59]。基本上，将 U盘像普通硬盘一样做 GPT 分区，并在 ESP 中安装 UEFI 应用即可。 

## 嵌入式 linux 解决方案
### Yocto Project

### Ubuntu Core

### Clear Linux
[50]

### Ostro OS
[66]
Ostro OS 没有 yum/apt 之类的包管理器，采用 OpenEmbeded/yocto 来制作镜像，用 swupd 来升级。

安装：
wget -O - http://172.16.9.102:8000/ostro-image-swupd-intel-corei7-64.dsk.xz | xzcat | dd of=/dev/sdb bs=512k

压缩文件116MB，初步安装后虚拟磁盘 407MB，初次启动后增加到415MB。
默认镜像采用 UEFI 引导，GPT 分区，无 swap。

X86 上只支持 EFI 启动，并且 bootloader, bootloader 设置，Linux 内核，initramfs 都打包到一个文件里了（boot/EFI/BOOT/bootx64.efi）。

### Tiny Core Linux
属于传统的 linux 发行版模式，通过包管理器（tce）来维护和升级系统。

### Alpine Linux
属于传统的 linux 发行版模式，通过包管理器（apk）来维护和升级系统。

### OpenWrt

## 系统更新解决方案
### SWUpdate
[22]
nice and extensible software update framework
支持  Yocto / OpenEmbedded。

问题：依赖u-boot。虽然u-boot支持x86，但如何在x86 pc 上安装还待查。一种思路是用 EFI 启动 u-boot。
alpine 可能在 3.6 (2017.5) 中支持 x86 + u-boot （不知是否仅限于 Intel Edison）。
SWUpdate 支持其他 bootloader 如 grub 的方法还在考虑中[32]。

此外，主要的更新方式是整个分区的镜像，文件级更新不是很有效。

### Swupd (clearlinux / Ostro OS)
[26,31]
revisioned software update mechanism.
问题：clearlinux / Ostro OS 体积较大。

Swupd 目前依赖于 systemd (`update-triggers.target`)。

在 clearlinux 中，可以通过 rpm 包的安装来生成 Swupd 更新包和系统镜像。工具称为 mixer。
clearlinux 不是完全无状态的。刚安装好时，/etc 是空的，管理员可以在其中增加配置文件来覆盖默认的配置。

### Ostree 
git like software update framework
可以与 OpenEmbedded 搭配。

### Mender 
dual partition pattern oriented framework
包括客户端和服务器端部分。
可以搭配 Yocto 使用。
go 语言实现。
问题：
 * 依赖 u-boot
 * 支持分区镜像的更新，没有文件级更新？

### Fwup
configurable image based firmware update tool

### Sysup
update rootfs from the initramdisk

### Resin
docker/nodejs based framework
只处理 container 的更新，不能更新 host。也提供了基础 host 系统，可以跑在
Raspberry Pi, Edison, BeagleBone 等硬件上。

### Dynamic update tools: kpatch kgraft

### snap + Ubuntu Core

### QtOTA
基于 ostree，支持 uboot 和 grub。

## watchdog
[81, 82]

## 工具
### create a bootable USB stick on Windows
Rufus USB installer

## 计划
* 等待 alpine 的 x86 + u-boot，然后 SWupate
* 自行编译 x86 + u-boot (chroot alpine)，然后 SWupate
* 自行编译 Ostro，swupd
* 在 alpine 上编译 snap
