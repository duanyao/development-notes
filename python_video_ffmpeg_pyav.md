[1.1] pyav
  https://pyav.org/docs/stable/index.html

[1.5] Logging
  https://pyav.org/docs/stable/api/utils.html?highlight=log%20level#av.logging.set_level

[1.7.1] How to use Filter?
  https://github.com/PyAV-Org/PyAV/issues/742
[1.7.2]
  https://github.com/PyAV-Org/PyAV/issues/239

[2.1] opencv-python video

## pyav

### 安装

安装依赖：
```
sudo apt-get install -y \
    libavformat-dev libavcodec-dev libavdevice-dev \
    libavutil-dev libswscale-dev libswresample-dev libavfilter-dev
```

安装本体：
```
pip install av
```

### 代码样例

读文件：
```
import av

file_name = '/media/DATA/emulated-oss/private/record-video/-1192494137530247441/2021-11-18GMT+8/1637223817419.mp4'
container = av.open(file_name)

container.streams.video[0].thread_type = 'AUTO'

frame_n = 1
for frame in container.decode(video=0):  # container.decode() 返回 generator
    #frame 的类型是 av.VideoFrame
    print('frame props: time=' + str(frame.time) + ', frame_n=' + str(frame_n) + ', w=' + str(frame.width) + ', f=' + str(frame.format) + ', idx=' + str(frame.index));
    frame_n += 1
    #frame.to_rgb() # 仍是 av.VideoFrame 对象，rgb24 格式
    #frame.to_image() # PIL.Image RGB
    #frame.to_image().save('frame-%04d.jpg' % frame.index)
    #frame.to_ndarray() numpy array ARGB swap bytes if needed
```

frame.index 从 0 开始计算。

默认状态下，av decode 使用单线程（文档说是使用 SLICE threading），设置 `.thread_type = 'AUTO'` 则使用多线程，基本可利用全部 CPU 算力。
av decode 的性能比 ffmpeg（命令行）要慢一些（~10-20%）。

按 packet 解码：

```
import time

import av
import av.datasets

print("Decoding with auto threading...")

container = av.open(av.datasets.curated('pexels/time-lapse-video-of-night-sky-857195.mp4'))

# !!! This is the only difference.
container.streams.video[0].thread_type = 'AUTO'

start_time = time.time()
for packet in container.demux(video=0):
    print(packet)
    for frame in packet.decode():
        print(frame)

auto_time = time.time() - start_time
container.close()


print("Decoded with default threading in {:.2f}s.".format(default_time))
print("Decoded with auto threading in {:.2f}s.".format(auto_time))
```

### 像素格式转换、缩放

frame 为 av.VideoFrame 。从视频解码出来，一般是 yuvj420p 等像素格式。
```
frame.to_rgb() # 转为 av.VideoFrame rgb 格式
frame.to_image() # PIL.Image RGB
frame.to_ndarray() # 转为 numpy ndarray 格式。生成的像素格式和shape取决于原来的像素格式。如果原来是 yuvj420p ，则生成 (1.5*h, w), uint8；如果原来是 rgb24，则生成（h,w,3), uint8 。
frame.to_rgb().to_ndarray() # 转为 rgb24 再转为 ndarray（h,w,3), uint8
frame.to_ndarray(format='bgr24', width=768, height=432) # 缩放，转为 bgr24 再转为 ndarray（h,w,3), uint8
frame.to_image(width=768, height=432)
```

### 日志

[1.5]

pyav 将 ffmpeg 的 log 转化为 python 的 log。

默认状态下 pyav 输出较多的日志。可以用 python 内置的 logging 设置日志级别：

```
import logging
logging.getLogger('libav').setLevel(logging.ERROR)
```

也可以通过编程的方式获得 ffmpeg 的 log：

```
from av.logging import Capture
with Capture() as logs:
  # Do something.
  for log in logs:
    print(log.message)
```

### 使用过滤器 libavfilter
[1.7.1]
```
import av
from av.filter import Graph

def main():
    input_container = av.open('example.mp4', mode='r')
    input_video_stream = input_container.streams.video[0]

    output_container = av.open("out.mp4", "w")
    output_video_stream = output_container.add_stream('libx264', rate=input_video_stream.base_rate)

    graph = Graph()

    in_src = graph.add_buffer(template=input_container.streams.video[0])
    pad = graph.add("pad", "600:400:0:40:violet")
    in_src.link_to(pad)
    sink = graph.add('buffersink')
    pad.link_to(sink)
    graph.configure()

    for frame in input_container.decode(video=0):
        graph.push(frame)
        filtered_frame = graph.pull()
        output_container.mux(output_video_stream.encode(filtered_frame))

    for packet in output_video_stream.encode():
        output_container.mux(packet)

    input_container.close()
    output_container.close()


if __name__ == '__main__':
    main()
```

[1.7.2]
```
    # https://ffmpeg.org/ffmpeg-filters.html#Examples-43
    fchain.append(graph.add("crop", "200:200:12:34"))
    fchain[-2].link_to(fchain[-1])
```
## 与 ffmpeg 命令行工具的性能对比

文件 /media/DATA/emulated-oss/private/record-video/-1192494137530247441/2021-11-18GMT+8/1637223817419.mp4

pyav (thread_type = 'AUTO'，输出 yuvj420p)：

```
\time -v python test_pyav_read.py

        Command being timed: "python test_pyav_read.py"
        User time (seconds): 45.48
        System time (seconds): 0.69
        Percent of CPU this job got: 180%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 0:25.52
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 127212
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 0
        Minor (reclaiming a frame) page faults: 27801
        Voluntary context switches: 12377
        Involuntary context switches: 86493
        Swaps: 0
        File system inputs: 0
        File system outputs: 0
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 0
```

```
\time -v ffmpeg -y -vsync passthrough -i /media/DATA/emulated-oss/private/record-video/-1192494137530247441/2021-11-18GMT+8/1637223817419.mp4 -f rawvideo /dev/null

        User time (seconds): 45.60
        System time (seconds): 0.68
        Percent of CPU this job got: 231%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 0:19.99
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 114456
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 0
        Minor (reclaiming a frame) page faults: 27104
        Voluntary context switches: 10346
        Involuntary context switches: 77401
        Swaps: 0
        File system inputs: 0
        File system outputs: 0
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 0
```

## opencv-python video

[2.1]

```
file_name = '/media/DATA/emulated-oss/private/record-video/-1192494137530247441/2021-11-18GMT+8/1637223817419.mp4'

cap = cv2.VideoCapture(file_name)

while(cap.isOpened()):
    ret, frame = cap.read()
    if not ret:
        print('error read frame')
        break
    print('frame props: time=' + str(cap.get(cv2.CAP_PROP_POS_MSEC)) + ',frame_n=' + str(cap.get(cv2.CAP_PROP_POS_FRAMES)) +
          ',width=' + str(cap.get(cv2.CAP_PROP_FRAME_WIDTH)) +
          ',height=' + str(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
          )
    # frame_c = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    #frame_c = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    frame_c = frame

    #cv2.imshow('frame',frame_c)
    #if cv2.waitKey(40) & 0xFF == ord('q'):
        #break

cap.release()
cv2.destroyAllWindows()
```

默认状态下，VideoCapture 使用全部 CPU 核心。VideoCapture 的性能比 ffmpeg（命令行）要慢一些（~30%）。
