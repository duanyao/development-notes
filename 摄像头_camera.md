## 参考资料
[3.1] 寻相VX-HT01人脸抓拍机 https://aim.baidu.com/product/69cbe474-c7ae-4018-b16e-1ced623a40c1

[4.1] 使用rtsp协议访问海康IP摄像头 https://www.jianshu.com/p/75aac9258637

## 海康IP摄像头
海康威视IP摄像头rtsp协议地址如下：

rtsp://[username]:[passwd]@[ip]:[port]/[codec]/[channel]/[subtype]/av_stream

说明：
username：用户名，例如admin
passwd：密码，例如12345
ip：设备的ip地址，例如192.0.0.64
port：端口号默认554，若为默认可以不写
codec：有h264、h264、MPEG-4、mpeg4这几种
channel：通道号，起始为1
subtype：码流类型，主码流为main,子码流为sub

例如，请求海康IP摄像机通道1的码流，url如下：
主码流：
rtsp://admin:12345@192.0.0.64:554/h264/ch1/main/av_stream
rtsp://admin:12345@192.0.0.64:554/MPEG-4/ch1/main/av_stream

子码流：
rtsp://admin:12345@192.0.0.64/mpeg4/ch1/sub/av_stream
rtsp://admin:12345@192.0.0.64/h264/ch1/sub/av_stream

codec 部分一般没有实际作用，不论写什么都可以正常工作，视频流采用的实际编码格式需要在设备上配置。

访问主码流时，忽略掉 /[codec]/[channel]/[subtype]/av_stream 也是可以的，例如 

rtsp://admin:12345@192.0.0.64:554

访问子码流时，可以只保留 /[channel]/[subtype] ，例如

rtsp://admin:12345@192.0.0.64/ch1/sub

## 内置计算机视觉的摄像头
寻相VX-HT01人脸抓拍机[4.1]

1200 元，焦距是6~22mm可调，对焦距离2~8米。

## 光学综合
手机摄像头的等效焦距
https://blog.csdn.net/pilifeng1/article/details/83106226

1英寸以下的感光元件中，英寸的单位并不是1英寸=25.4mm，而是1英寸=16mm。
不少厂家在标注1英寸以下感光元件时会圆整到常用值，而未参与成像的部分感光元件面积也被计算在内，因此厂商所提供的感光元件尺寸要略大于理论计算得出的数值。事实上，1/2.3英寸感光元件的尺寸通常被定为6.2×4.6mm，对角线长约为7.7mm。

名义(英寸) 宽(mm) 高(mm)  对角线(mm)  焦距系数
1/1.8  7.2  5.3  8.94 4.84
1/2.5  5.76 4.29 7.18 6.02
1/2.7  5.3  4.0  6.64 6.52

什么是等效35mm焦距？
https://blog.csdn.net/brilliantyoho/article/details/17752031

镜头的焦距与视场角简介
https://blog.csdn.net/dulingwen/article/details/89915388

焦距与视角的关系
https://wenku.baidu.com/view/8d7e4ece42323968011ca300a6c30c225801f0d9.html

什么是镜头畸变？如何矫正镜头畸变，合理利用镜头畸变？
https://zhuanlan.zhihu.com/p/115666171

## 摄像头实例

### nubia z17 mini

https://www.gophotonics.com/products/cmos-image-sensors/sony-corporation/21-209-imx258
Optical Format
1/3.06" = 8.30mm

Pixel Size
1.12 µm x 1.12 µm

Active Array
4208(H) x 3120(V)
4.713 x 3.494 mm

Optical Diagonal
5.867 mm

焦距换算系数 43.27/5.867 = 7.375

物理焦距：3.46mm

等效焦距：3.46*7.375=25.52mm

### 海康威视 
DS-2CD3T46F(D)WDV2-I3/I5/I8(S)

400万 1/2.7” CMOS


## 全景摄像（超广角镜头）

### 综合

全景摄像机的原理与进展
https://wenku.baidu.com/view/12526c45f08583d049649b6648d7c1c708a10be8.html

超广角镜头有什么用处？
https://www.zhihu.com/question/22983370/answer/543242121

### 手机外置超广角镜头

“小天”外置手机二合一超广角微距镜头 使用评测 
https://post.smzdm.com/p/467749/

镜头广角倍数为0.45倍距（也就是焦距缩小到原来焦距的0.45倍）

### 鱼眼镜头

500万鱼眼广角摄像头360度全景POE供电有线网络摄像机兼海康大华 全景监控POE 无 4k 1.6mm
https://item.jd.com/10026720184509.html

DS-2CD2955FWD-I(W)(S) 500万 CMOS ICR鱼眼全景日夜型网络摄像机
https://www.hikvision.com/cn/prgs_1245_i15713.html

收纳天地——鱼眼镜头Canon EF 8-15mm f/4L USM
https://zhuanlan.zhihu.com/p/47600149

### 多镜头拼接/多目全景相机

海康800万星光级全景拼接网络摄像机 DS-2CD6924F-I(S)(/NFC)  https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-18693313851.25.55332a11sMtENX&id=612903741293

海康威视2400万环型鹰眼摄像机 全景拼接DS-2CD6A64F-I  https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-18693313851.55.55332a11sMtENX&id=624159948367

海康3200万星光全景拼接摄像头DS-2CD6984F-IHS/NFC(2.8mm) 
https://item.taobao.com/item.htm?spm=a230r.1.14.24.699c71d3SiGpfF&id=636710076280&ns=1&abbucket=19#detail
https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-18693313851.58.55332a11sMtENX&id=626392262508



### 图像拼接（软件）

基于海康威视的全景拼接系统
https://blog.csdn.net/qq_35614920/article/details/85405268
https://github.com/doubiiot/Panoramic-Mosaic
