## 参考资料
[2.1] How to install OpenCV 4 on Ubuntu
  https://www.pyimagesearch.com/2018/08/15/how-to-install-opencv-4-on-ubuntu/

[2.2] opencv+opencv_contrib 4.1 source
  https://github.com/opencv/opencv/archive/4.1.0.zip
  https://github.com/opencv/opencv_contrib/archive/4.1.0.zip
  
[2.3] opencv Releases
  https://opencv.org/releases/
  
[2.4] Install OpenCV-Python in Ubuntu 
  https://docs.opencv.org/4.2.0/d2/de6/tutorial_py_setup_in_ubuntu.html

[2.5] Opencv 3: cudarithm.hpp not installed on linux when building without cuda
  https://stackoverflow.com/questions/31908448/opencv-3-cudarithm-hpp-not-installed-on-linux-when-building-without-cuda
  
[2.6] How can I create a .deb package with my compiled OpenCV build?
  https://unix.stackexchange.com/questions/46122/how-can-i-create-a-deb-package-with-my-compiled-opencv-build

## debian/deepin

### 2.4 二进制包
libopencv-videostab2.4v5
  libopencv-gpu2.4v5{a} libopencv-legacy2.4v5{a} libopencv-ml2.4v5{a} libopencv-photo2.4v5{a} libopencv-videostab2.4v5

libopencv-superres2.4v5

libopencv-stitching2.4v5

libopencv-contrib2.4v5

libopencv-ocl2.4v5

### 开发包
只有 libopencv-dev 含有 OpenCVConfig.cmake 文件，其它子包是没有的。
安装 libopencv-dev 将会安装所有的 opencv 开发包。

deepin 的 libopencv-dev 的版本有 2.4 和 3.2 ，并不能同时安装。虽然运行库可以同时安装。

apt-file search OpenCVConfig.cmake
libopencv-dev: /usr/share/OpenCV/OpenCVConfig.cmake

apt install libopencv-dev

## 从源码安装
参考[2.1]。

安装前置依赖：
```
sudo apt-get install build-essential cmake unzip pkg-config

# 以下是可选择解码器
sudo apt-get install libjpeg-dev libpng-dev
sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev 
sudo apt-get install libv4l-dev
sudo apt-get install libxvidcore-dev libx264-dev

sudo apt-get install libgtk-3-dev

sudo apt-get install libatlas-base-dev gfortran

# 二选一即可。如果都装，都会被检测到。
sudo apt-get install python3-dev
sudo apt-get install python-dev

# 可选。cmake 会检测 numpy 头文件。如果只想使用 venv 中的 numpy ，则无需安装。
sudo apt-get install python3-numpy
sudo apt-get install python-numpy
```

下载两个源码包：opencv-4.1.0.zip 和 opencv_contrib-4.1.0.zip [2.2]，解压。

```
cd opencv-4.1.0
mkdir build && cd build

# 如果要修改 cmake 选项然后增量编译，则删除以下文件再运行 cmake 命令：
rm Makefile CMakeCache.txt CMakeVars.txt

# 自动检测 python2 和 python3 的安装位置
cmake -D CMAKE_BUILD_TYPE=RELEASE \
	-D CMAKE_INSTALL_PREFIX=/usr/local/opencv4 \
	-D BUILD_TESTS=OFF \
	-D BUILD_PERF_TESTS=OFF \
	-D INSTALL_PYTHON_EXAMPLES=OFF \
	-D INSTALL_C_EXAMPLES=OFF \
	-D OPENCV_ENABLE_NONFREE=ON \
	-D WITH_CUDA=ON \
	-D OPENCV_EXTRA_MODULES_PATH=~/t-project/opencv_contrib-4.1.0/modules \
	-D BUILD_EXAMPLES=OFF \
	-D PYTHON3_PACKAGES_PATH:PATH=lib/python \
	-D PYTHON2_PACKAGES_PATH:PATH=lib/python \
	..

# 自动检测 python2 的安装位置，指定 python3 的安装位置（在venv中）
cmake -D CMAKE_BUILD_TYPE=RELEASE \
	-D CMAKE_INSTALL_PREFIX=/usr/local/opencv4 \
	-D BUILD_TESTS=OFF \
	-D BUILD_PERF_TESTS=OFF \
	-D INSTALL_PYTHON_EXAMPLES=OFF \
	-D INSTALL_C_EXAMPLES=OFF \
	-D OPENCV_ENABLE_NONFREE=ON \
	-D WITH_CUDA=ON \
	-D OPENCV_EXTRA_MODULES_PATH=~/t-project/opencv_contrib-4.1.0/modules \
	-D BUILD_EXAMPLES=OFF \
	-D PYTHON3_EXECUTABLE=~/opt/venv/v37/bin/python3.7 \
	-D PYTHON3_INCLUDE_DIR=/usr/local/include/python3.7m \
	-D PYTHON3_LIBRARY=/usr/local/lib/libpython3.7m.so \
	-D PYTHON3_PACKAGES_PATH:PATH=lib/python \
	-D PYTHON2_PACKAGES_PATH:PATH=lib/python \
	..
```

opencv 并不真的支持多版本共存。如果 prefix 是标准位置（/usr 或者 /usr/local）, 则安装后会与以前安装的 opencv 冲突。因为 opencv 总是会安装不带版本号的 so，例如 `/usr/local/lib/libopencv_video.so` 。
因此，要多版本共存，则 `CMAKE_INSTALL_PREFIX` 不能是标准位置。

如果在 venv 环境里安装了 numpy，且让 PYTHON3_EXECUTABLE 指向 venv 的 python 程序，则 cmake 应该自动检测到 venv 中的 numpy，并显示：

```
numpy:  ~/opt/venv/v37/lib/python3.7/site-packages/numpy/core/include (ver 1.18.1)
```
如果检测 numpy 不正确，也可以在 cmake 命令中直接指定：
```
-D PYTHON3_NUMPY_INCLUDE_DIRS=~/opt/venv/v37/lib/python3.7/site-packages/numpy/core/include/
```

`PYTHON3_PACKAGES_PATH:PATH` 和 `PYTHON2_PACKAGES_PATH:PATH` 指定 python 库的安装位置（相对于 CMAKE_INSTALL_PREFIX 目录）。注意，`:PATH` 部分不能省略，否则就必须使用绝对路径。
如果不指定，则默认值是 `lib/pythonX.Y/dist-packages`，根据检测到的 python 版本而不同。
这个位置不是很理想，实际上没有必要让路径包含 python 版本号，因为 opencv python 的文件已经包含了版本号。

要启用 CUDA 支持，需要自行在 cmake 设置 `WITH_CUDA=ON`。否则，即使系统上安装了 cuda tookit，编译时也不会自动使用。启用 CUDA 会显著增加安装尺寸。
在没有 nv 显卡或者没有安装 nv 驱动的机器上仍然是可以编译有 CUDA 支持的 opencv 的，只要安装了 cuda tookit 即可。

可以用 `-D BUILD_TESTS=OFF -D BUILD_PERF_TESTS=OFF` 指示不要编译测试，从而加快编译速度。默认是ON。

接下来，编译：

```
make -j4
```

安装
```
sudo make install
sudo ldconfig     # 仅当安装于标准位置时需要
```

或者用 checkinstall 方式：

```
sudo apt install checkinstall # 如果还没有安装 checkinstall

sudo make -n install > make-install.sh
sudo checkinstall sh make-install.sh

# 将 name 和 provides 都改为为 opencv4-checkinstall，version 改为 4.1.0 。

# 删除 checkinstall 备份文件：
sudo rm backup-*.tgz
```

用 sudo 运行 make -n install 和 checkinstall 是有必要的，否则最后 deb 中的文件所有者会不正确。

以后还可以手动安装deb：
```
sudo dpkg -i ./opencv4-checkinstall_4.1.0-1_amd64.deb
```

由于安装在了非标准位置，其动态库无法被正常加载，所以要增加一个配置文件：`/etc/ld.so.conf.d/z0-opencv4-checkinstall.conf`，内容是

```
/usr/local/opencv4/lib/
```

然后刷新 so 缓存：`sudo ldconfig -v`，然后运行 `/usr/local/opencv4/bin/opencv_version` ，验证动态库是否正常加载。

python 库分别位于（用 `dpkg -L opencv4-checkinstall` 查看）：

```
/usr/local/opencv4/lib/python/cv2/python-2.7/cv2.so
/usr/local/opencv4/lib/python/cv2/python-3.7/cv2.cpython-37m-x86_64-linux-gnu.so
```

checkinstall 方式看来无法实现同时安装适用于多个 python 小版本的库。

注意，这样的 python 库是在非标准位置，无法直接加载，需要自行把 `/usr/local/opencv4/lib/python` 添加到 python 的 `sys.path` 列表中，或者 PYTHONPATH 环境变量中。


另一种生成 deb 的方式是打开 `-D CPACK_BINARY_DEB=ON`，然后用 `make package` 命令生成 deb 包[2.6]。

### 错误处理

`checkinstall sh make-install.sh` 可能出现以下错误：

```
CMake Error at cmake_install.cmake:92 (file):
  file INSTALL cannot set permissions on
  "/usr/local/opencv4/lib/cmake/opencv4/OpenCVConfig-version.cmake": No such
  file or directory.
```
原因不明。可以将 build 目录下的内容全部删除，然后全部重新编译。

## 从其它 C++ 项目引用 opencv

如果使用 cmake，则这样写

```
cmake_minimum_required(VERSION 2.8)
project(demo)

find_package(OpenCV 4.1 REQUIRED) # 或者省略版本号 find_package(OpenCV)，或者指定目录 find_package(OpenCV REQUIRED PATHS "/usr/local/opencv4")

include_directories(${OpenCV_INCLUDE_DIRS})
add_executable(main main.cpp)
target_link_libraries(main ${OpenCV_LIBS})
```
如果 OpenCV 安装位置非标准，则用 `-D OpenCV_DIR` 指定其安装位置：

```
cmake /path/to/your/sources -D OpenCV_DIR=/usr/local/opencv4
```

不过，`/usr/local/opencv4` 这个位置是可以被 cmake 的 `find_package(OpenCV)` 指令自动找到的。
