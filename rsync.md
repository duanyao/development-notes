## 参考资料
[1] https://serverfault.com/questions/43014/copying-a-large-directory-tree-locally-cp-or-rsync

[2] 《rsync同步的艺术》–linux命令五分钟系列之四十二 http://roclinux.cn/?p=2643

[3] rsync命令 http://man.linuxde.net/rsync

## 复制本地文件
[1]
rsync -aAXHxvhW --no-compress --progress /src/ /dst/
rsync -rtlAXHxvhW --no-compress --progress /src/ /dst/
rsync -rtlAXHxvhW --update --no-compress --progress /src/ /dst/

-a is for archive, which preserves ownership, permissions etc. 也意味着保留软链接原来的样子，而不是把目标文件复制过去。注意，并不意味着保留 ACL、扩展属性、硬链接。
-a --no-owner 或 -a --no-o 归档模式，但不保留owner。
-r 递归
-t 保留修改时间
-l 保留符号链接
-L --copy-links 复制符号链接指向的本体。如果 -l 和 -L 都没有指定，则跳过符号链接，并输出 "skipping non-regular file" 的警告。
-A 保留 ACL
-X 保留扩展属性
-H 保留硬链接（视情况添加）
-x 不跨越文件系统边界
--del 从目标上删除多余的文件（边传输边删除）。默认不删除。
--delete-after 传输完成后从目标上删除多余的文件。
-v is for verbose, so I can see what's happening (optional)
-h is for human-readable, so the transfer rate and file sizes are easier to read (optional)
-W is for copying whole files only, without delta-xfer algorithm which should reduce CPU load
--no-compress as there's no lack of bandwidth between local devices
--progress so I can see the progress of large files (optional)
--update 仅当源文件修改时间较新时复制。

简化版：
rsync -aAXHxvh /src/ /dst/

如果有错误，可以去掉 -v 参数再试一次。

## 是否创建与源目录同名的目录

rsync -avh /src /dst/

结果是将 src 复制到 /dst/ 下，产生 /dst/src 的结构。而

rsync -avh /src/ /dst/

是将 /src/* 复制到 /dst/* 。

## 复制远程文件
```
rsync -avz --no-o --progress -e ssh /local/dir/ user@remote-host:/remote/dir/
rsync -avz --progress --usermap=0-99:nobody,wayne:admin,*:normal --groupmap=usr:1,1:usr /local/dir/ user@remote-host:/remote/dir/
rsync -avz --progress --chown=USER:GROUP /local/dir/ user@remote-host:/remote/dir/
```
这是从本地复制文件到远程；颠倒源和目标即可从远程复制到本地。

`-e ssh` 指定采用 ssh 服务来执行命令。这样只要远程主机开了 ssh 服务，本地有 rsync 客户端即可工作。
如果要指定ssh的端口为1234，改为 `-e 'ssh -p 1234'`。

远程文件/目录的形式与 scp 相同，即 `<remote-username>@<remote-address>:<remote-path>` 的形式。

-a --no-owner 或 -a --no-o 归档模式，但不保留owner。
--usermap 和 --chown 用来指定远程文件的所有者。如果不指定，则属于 ssh 登录用户，或者属于与本地用户相同的id（-a 或 -o选项）。

## 目录结尾的 '/'

目录结尾是否写 '/' 对结果是有影响的。

* 如果希望目标目录与源目录的内容完全相同，就要让源目录以'/'结尾；这时目标目录的结尾没有影响。
* 如果希望将源目录放到目标目录下面，作为子目录，就要让源目录不以'/'结尾；这时目标目录的结尾也没有影响。

加上 --del 参数时，如果源目录不以'/'结尾，则目标目录下与原目录同名的目录中的文件才可能被删除。

## 文件比较的方式

默认采用修改时间+文件大小来判断。
如果加上 --update 参数，仅当源文件修改时间较新时复制。

## 从给定的文件列表中同步
```
rsync -av --files-from=./rsync_list.txt src/ dest/
```

rsync_list.txt 中每一行包括一个文件或目录，采用相对路径，相对 src/ 和 dest/ 目录。

## 排除特定文件或目录
```
rsync -a --exclude 'dir1' src_directory/ dst_directory/
rsync -a --exclude 'dir1/*' src_directory/ dst_directory/
rsync -a --exclude={'file1.txt','dir1/*','dir2'} src_directory/ dst_directory/
rsync -a --exclude '*.jpg*' src_directory/ dst_directory/
```

