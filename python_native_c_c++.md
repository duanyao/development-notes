## ctypes
```
import sys
from ctypes import CDLL

if sys.platform =="linux":
    dll = CDLL(r'{}/lib/libarcsoft_face_engine.so'.format(root_dir))
    dllc = CDLL("libc.so.6")
elif platform =="windows_x64":
    dll = CDLL(r'{}/lib/libarcsoft_face_engine.dll'.format(root_dir))
    dllc = cdll.msvcrt

ASFDetectFaces = dll.ASFDetectFaces
ASFDetectFaces.restype = c_int32
ASFDetectFaces.argtypes = (c_void_p, c_int32, c_int32, c_int32, POINTER(c_ubyte), POINTER(ASF_MultiFaceInfo))
```


## pybind11

## swig
