# IO benchmark tools

## 参考资料

[1.10] demo bench failed #156  https://github.com/filebench/filebench/issues/156
[1.11] filebench: Waiting for pid 34082 thread https://github.com/filebench/filebench/issues/112

[2.1] linux: how to simulate hard disk latency? I want to increase iowait value without using a cpu power
https://serverfault.com/questions/523509/linux-how-to-simulate-hard-disk-latency-i-want-to-increase-iowait-value-withou

[2.2] Device-Mapper's "delay" target delays reads and/or writes
https://www.kernel.org/doc/html/latest/admin-guide/device-mapper/delay.html
https://www.kernel.org/doc/Documentation/device-mapper/delay.txt

[3.1] blktrace - generate traces of the i/o traffic on block devices
https://linux.die.net/man/8/blktrace

[4.1] dm-flakey
https://www.kernel.org/doc/html/latest/admin-guide/device-mapper/dm-flakey.html

## dd

```
cd ~/disk_test
dd if=/dev/zero bs=1G count=120 of=d1.raw
dd if=/dev/zero bs=1G count=20 >>d1.raw
```
dd 的 bs（block size）可以任意大小，单位后缀可以是K、M、G等，无后缀则是byte。
不写 of 参数，而是重定向到文件也可以，用 `>>` 可以附加到已有文件。

## filebench

### 安装
#### 源码安装

### 系统配置

关闭 `randomize_va_space` ：
```
sysctl kernel.randomize_va_space
2
sudo sysctl kernel.randomize_va_space=0
```
`randomize_va_space` 设置为 2 或 1 会造成 filebench 无限期运行（卡在 Starting N XX instances），不会输出结果[1.10-1.11]。

### 运行 demo

创建测试脚本 `test1.f`：
```
define fileset name="testF",entries=10000,filesize=16k,prealloc,path="/tmp"
define process name="readerP",instances=2 {
    thread name="readerT",instances=3 {
        flowop openfile name="openOP",filesetname="testF"
        flowop readwholefile name="readOP",filesetname="testF"
        flowop closefile name="closeOP"
    }
}
run 60
```

执行，输出：
```
filebench -f test1.f

Filebench Version 1.5-alpha3
WARNING: Could not open /proc/sys/kernel/shmmax file!
It means that you probably ran Filebench not as a root. Filebench will not increase shared
region limits in this case, which can lead to the failures on certain workloads.
0.000: Allocated 177MB of shared memory
0.003: Fileset has no directory width around line 3
0.004: Populating and pre-allocating filesets
0.010: testF populated: 10000 files, avg. dir. width = 0, avg. dir. depth = -0.0, 0 leafdirs, 156.250MB total size
0.010: Removing testF tree (if exists)
0.667: Pre-allocating directories in testF tree
0.816: Pre-allocating files in testF tree
1.061: Waiting for pre-allocation to finish (in case of a parallel pre-allocation)
1.061: Population and pre-allocation of filesets completed
1.061: Starting 2 readerP instances
2.063: Running...
62.067: Run took 60 seconds...
62.067: Per-Operation Breakdown
closeOP              35908563ops   598436ops/s   0.0mb/s    0.001ms/op [0.001ms - 0.739ms]
readOP               35908564ops   598436ops/s 9350.6mb/s    0.003ms/op [0.001ms - 0.772ms]
openOP               35908569ops   598436ops/s   0.0mb/s    0.004ms/op [0.002ms - 8.551ms]
62.067: IO Summary: 107725696 ops 1795307.712 ops/s 598436/0 rd/wr 9350.6mb/s 0.003ms/op
62.067: Shutting down processes
```

事后应删除 `/tmp/testF`。

执行，输出：
```
filebench -f ~/opt/share/filebench/workloads/webserver.f

Filebench Version 1.5-alpha3
WARNING: Could not open /proc/sys/kernel/shmmax file!
It means that you probably ran Filebench not as a root. Filebench will not increase shared
region limits in this case, which can lead to the failures on certain workloads.
0.000: Allocated 177MB of shared memory
0.005: Web-server Version 3.1 personality successfully loaded
0.005: Populating and pre-allocating filesets
0.005: logfiles populated: 1 files, avg. dir. width = 20, avg. dir. depth = 0.0, 0 leafdirs, 0.002MB total size
0.005: Removing logfiles tree (if exists)
0.006: Pre-allocating directories in logfiles tree
0.006: Pre-allocating files in logfiles tree
0.007: bigfileset populated: 1000 files, avg. dir. width = 20, avg. dir. depth = 2.3, 0 leafdirs, 14.760MB total size
0.007: Removing bigfileset tree (if exists)
0.007: Pre-allocating directories in bigfileset tree
0.008: Pre-allocating files in bigfileset tree
0.032: Waiting for pre-allocation to finish (in case of a parallel pre-allocation)
0.032: Population and pre-allocation of filesets completed
0.032: Starting 1 filereader instances
1.042: Running...
61.078: Run took 60 seconds...
61.121: Per-Operation Breakdown
appendlog            2364556ops    39390ops/s 307.7mb/s    2.016ms/op [0.001ms - 64.439ms]
closefile10          2364458ops    39388ops/s   0.0mb/s    0.001ms/op [0.000ms - 7.864ms]
readfile10           2364458ops    39388ops/s 580.8mb/s    0.007ms/op [0.001ms - 9.059ms]
openfile10           2364459ops    39388ops/s   0.0mb/s    0.014ms/op [0.002ms - 89.670ms]
closefile9           2364460ops    39388ops/s   0.0mb/s    0.001ms/op [0.000ms - 6.612ms]
readfile9            2364460ops    39388ops/s 581.6mb/s    0.007ms/op [0.001ms - 7.788ms]
openfile9            2364465ops    39388ops/s   0.0mb/s    0.014ms/op [0.002ms - 88.119ms]
closefile8           2364465ops    39388ops/s   0.0mb/s    0.001ms/op [0.000ms - 7.915ms]
readfile8            2364465ops    39388ops/s 581.6mb/s    0.007ms/op [0.001ms - 7.025ms]
openfile8            2364467ops    39388ops/s   0.0mb/s    0.014ms/op [0.002ms - 86.414ms]
closefile7           2364467ops    39388ops/s   0.0mb/s    0.001ms/op [0.000ms - 6.268ms]
readfile7            2364467ops    39388ops/s 581.3mb/s    0.007ms/op [0.001ms - 12.420ms]
openfile7            2364468ops    39388ops/s   0.0mb/s    0.014ms/op [0.002ms - 49.882ms]
closefile6           2364469ops    39388ops/s   0.0mb/s    0.001ms/op [0.000ms - 6.983ms]
readfile6            2364469ops    39388ops/s 580.8mb/s    0.007ms/op [0.001ms - 9.446ms]
openfile6            2364471ops    39388ops/s   0.0mb/s    0.015ms/op [0.002ms - 94.949ms]
closefile5           2364471ops    39388ops/s   0.0mb/s    0.001ms/op [0.000ms - 8.376ms]
readfile5            2364471ops    39388ops/s 581.9mb/s    0.007ms/op [0.001ms - 10.880ms]
openfile5            2364471ops    39388ops/s   0.0mb/s    0.014ms/op [0.002ms - 85.913ms]
closefile4           2364472ops    39388ops/s   0.0mb/s    0.001ms/op [0.000ms - 6.993ms]
readfile4            2364473ops    39388ops/s 581.6mb/s    0.007ms/op [0.001ms - 9.377ms]
openfile4            2364474ops    39388ops/s   0.0mb/s    0.015ms/op [0.002ms - 94.068ms]
closefile3           2364474ops    39388ops/s   0.0mb/s    0.001ms/op [0.000ms - 7.922ms]
readfile3            2364474ops    39388ops/s 581.0mb/s    0.007ms/op [0.001ms - 7.655ms]
openfile3            2364474ops    39388ops/s   0.0mb/s    0.015ms/op [0.002ms - 48.796ms]
closefile2           2364478ops    39388ops/s   0.0mb/s    0.001ms/op [0.000ms - 7.081ms]
readfile2            2364480ops    39388ops/s 581.3mb/s    0.007ms/op [0.001ms - 10.554ms]
openfile2            2364480ops    39388ops/s   0.0mb/s    0.015ms/op [0.002ms - 83.972ms]
closefile1           2364481ops    39388ops/s   0.0mb/s    0.001ms/op [0.000ms - 8.099ms]
readfile1            2364481ops    39388ops/s 581.6mb/s    0.007ms/op [0.001ms - 7.600ms]
openfile1            2364483ops    39388ops/s   0.0mb/s    0.016ms/op [0.002ms - 88.557ms]
61.121: IO Summary: 73298661 ops 1221031.779 ops/s 393881/39390 rd/wr 6121.3mb/s 0.072ms/op
61.121: Shutting down processes
```

事后应删除临时目录/文件：
```
/tmp/bigfileset
/tmp/logfiles
```
## fio

### 例子

顺序读600G（同步）：

```
fio -filename=disk_test/d1.raw -direct=1 -iodepth 1 -thread -rw=read -ioengine=psync -bs=16k -size=20G -numjobs=30 -runtime=1000 -group_reporting -name=test-read
```
-size 是指每个 job 的 io 量，-numjobs 是 job 数量，因此总 io 量是两者的乘积，即 600GiB 。
每个 job 会用一个线程跑，因此总共 30 个线程。-name 是本次测试的名字，可以随便起。

输出
```
...
Run status group 0 (all jobs):
   READ: bw=874MiB/s (916MB/s), 874MiB/s-874MiB/s (916MB/s-916MB/s), io=600GiB (644GB), run=702961-702961msec

Disk stats (read/write):
  nvme0n1: ios=39318186/867, merge=0/348, ticks=20983126/446, in_queue=20983675, util=100.00%
```

顺序读140G，异步：

```
fio -filename=disk_test/d1.raw -direct=1 -iodepth 64 -thread -rw=read -ioengine=libaio -bs=16k -size=20G -numjobs=7 -runtime=1000 -group_reporting -name=test-read

fio -filename=disk_test/d1.raw -direct=1 -iodepth 64 -thread -rw=read -ioengine=libaio -bs=1M -size=60G -numjobs=10 -runtime=1000 -group_reporting -name=test-read
```

随机读140G/600G，异步：
```
fio -filename=disk_test/d1.raw -direct=1 -iodepth 64 -thread -rw=randread -ioengine=libaio -bs=16k -size=20G -numjobs=7 -runtime=1000 -group_reporting -name=test-read

fio -filename=disk_test/d1.raw -direct=1 -iodepth 64 -thread -rw=randread -ioengine=libaio -bs=16k -size=60G -numjobs=10 -runtime=1000 -group_reporting -name=test-read
```

顺序写140G，异步：

```
fio --filename=disk_test/d1.raw -iodepth=64 -ioengine=libaio -direct=1 -rw=write -bs=1m -size=20g -numjobs=7 -runtime=1000 -group_reporting -name=test-write
```

随机写140G/280G，异步：

```
fio --filename=disk_test/d1.raw -iodepth=64 -ioengine=libaio -direct=1 -rw=randwrite -bs=1m -size=20g -numjobs=7 -runtime=1000 -group_reporting -name=test-write

fio --filename=disk_test/d1.raw -iodepth=64 -ioengine=libaio -direct=1 -rw=randwrite -bs=1m -size=40g -numjobs=7 -runtime=1000 -group_reporting -name=test-write
```

小批量写入：

```
fio --filename=disk_test/d1.raw -iodepth=64 -ioengine=libaio -direct=1 -rw=write -bs=1m -size=1g -numjobs=5 -runtime=1000 -group_reporting -name=test-write

fio --filename=disk_test/d1.raw -iodepth=64 -ioengine=libaio -direct=1 -rw=randwrite -bs=1m -size=1g -numjobs=5 -runtime=1000 -group_reporting -name=test-write
```

## iozone

## 模拟缓慢的存储设备/文件系统

### dm-delay
[2.1-2.2]
dm-delay 模拟磁盘延迟。

```
# 创建 20MB的文件
fallocate -l 20M /tmp/slow-disk-20M.img
sudo losetup --show --find /tmp/slow-disk-20M.img

# --find,-f 列出一个空闲 loop 设备 --show print device name after setup
sudo losetup --show --find /tmp/slow-disk-20M.img

/dev/loop0

# 显示设备大小，按 sector （默认 512B）计算
sudo blockdev --getsize /dev/loop0

40960

# `0 40960`：起始和结束 sector；`delay`：设备类型；`/dev/loop0 0 200`：设备 /dev/loop0，起始sector是0，延时200ms。
echo "0 40960 delay /dev/loop0 0 200" | sudo dmsetup create slow-disk-20M

ls -l /dev/mapper/slow-disk-20M

lrwxrwxrwx 1 root root 7  9月23日 13:25 /dev/mapper/slow-disk-20M -> ../dm-0

sudo chmod +r /dev/dm-0

sudo dd if=/dev/mapper/slow-disk-20M of=/dev/null count=1000 bs=1K

输入了 4000+0 块记录
输出了 4000+0 块记录
4096000 字节 (4.1 MB, 3.9 MiB) 已复制，3.69321 s，1.1 MB/s

sudo dd if=/dev/mapper/slow-disk-20M of=/dev/null count=50 bs=1K iflag=direct
输入了 50+0 块记录
输出了 50+0 块记录
51200 字节 (51 kB, 50 KiB) 已复制，10.2785 s，5.0 kB/s

sudo dd if=/dev/mapper/slow-disk-20M of=/dev/null count=4 bs=1M iflag=direct
输入了 4+0 块记录
输出了 4+0 块记录
4194304 字节 (4.2 MB, 4.0 MiB) 已复制，0.816807 s，5.1 MB/s
```

默认参数下，dd 仍会利用操作系统的缓存（预取），所以读取延迟比预期的低，速率比预期的快。加上 `iflag=direct` 让 dd 以 O_DIRECT 参数打开文件，可以绕过缓存，反映出真实的延迟。

从上面的结果可以看出，dm-delay 产生的的延迟是针对每次 read() 调用的，因此 dd 的块大小越大，读取速率就越大。

卸载 dm-delay 设备：

```
dmsetup remove slow-disk-20M
losetup -d /dev/loop0
# confirmed with
dmsetup ls
losetup -l
```

### dm-flakey
[4.1] 模拟周期性磁盘错误。错误类型包括 error_reads、drop_writes、error_writes、corrupt_bio_byte 、random_read_corrupt、random_write_corrupt 等。

## blktrace
[3.1]
跟踪和记录块设备的访问模式，并可以重放，用于测试。
