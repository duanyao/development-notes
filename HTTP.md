## 参考资料
[1] Hypertext Transfer Protocol -- HTTP/1.1 
  https://www.w3.org/Protocols/rfc2616/rfc2616.html
[2] Header Field Definitions
  https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
  
[3] HTTP 协议中 Vary 的一些研究
  https://imququ.com/post/vary-header-in-http.html
  
[4] Should HTTP 304 Not Modified-responses contain cache-control headers?
  https://stackoverflow.com/questions/1587667/should-http-304-not-modified-responses-contain-cache-control-headers
  https://tools.ietf.org/html/rfc7232#section-4.1

[5] Location vs. Content-Location
  https://www.subbu.org/blog/2008/10/location-vs-content-location

[6] Protocol Parameters
  https://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html

[7] What's the rationale behind the HTTP Date header?
  https://stackoverflow.com/questions/1610254/whats-the-rationale-behind-the-http-date-header

[8] 13 Caching in HTTP
  https://www.w3.org/Protocols/rfc2616/rfc2616-sec13.html

[9] Why does Firebug show a “206 Partial Content” response on a video loading request?
  https://stackoverflow.com/questions/9755168/why-does-firebug-show-a-206-partial-content-response-on-a-video-loading-reques
  
[10] Hypertext Transfer Protocol (HTTP/1.1): Caching
  https://tools.ietf.org/html/rfc7234

[11] X-Cache and X-Cache-Lookup headers explained
  https://anothersysadmin.wordpress.com/2008/04/22/x-cache-and-x-cache-lookup-headers-explained/

[12] Is HTTP 1.1 Full duplex?
  https://stackoverflow.com/questions/23419469/is-http-1-1-full-duplex

[13] Implications of Full-Duplex HTTP draft-zhu-http-fullduplex-08.txt
  https://tools.ietf.org/html/draft-zhu-http-fullduplex-08

[14] Full-Duplex Channel over HTTP
  https://www.innovation.ch/java/HTTPClient/fullduplex.html

[15] What's the difference between Cache-Control: max-age=0 and no-cache?
  https://stackoverflow.com/questions/1046966/whats-the-difference-between-cache-control-max-age-0-and-no-cache

[16] 扼杀 304，Cache-Control: immutable
  https://www.cnblogs.com/ziyunfei/p/5642796.html

[17] Implement Cache-control: immutable
  https://bugzilla.mozilla.org/show_bug.cgi?id=1267474
  https://bugs.chromium.org/p/chromium/issues/detail?id=611416
  https://bugs.webkit.org/show_bug.cgi?id=167497
  
[18] Using Immutable Caching To Speed Up The Web
  https://hacks.mozilla.org/2017/01/using-immutable-caching-to-speed-up-the-web/#comment-20502
  
[19] Understanding the HTTP Vary Header and Caching Proxies
  http://mark.koli.ch/understanding-the-http-vary-header-and-caching-proxies-squid-etc

[21] HTTP 协议中的 Transfer-Encoding
https://imququ.com/post/transfer-encoding-header-in-http.html

[22] Transfer-Encoding: gzip vs. Content-Encoding: gzip
https://stackoverflow.com/questions/11641923/transfer-encoding-gzip-vs-content-encoding-gzip

[23] RFE: Improved support for HTTP/1.1 transfer codings (transfer-encoding)
https://bugzilla.mozilla.org/show_bug.cgi?id=68517

[24] Incorrect ETag on gzip:ed content
https://bz.apache.org/bugzilla/show_bug.cgi?id=39727

[25] Support full-duplex HTTP streaming
https://github.com/whatwg/fetch/issues/229

[31] 浅谈浏览器http的缓存机制
  http://web.jobbole.com/85509/
  
[32] HTTP Cache (Firefox)
  https://developer.mozilla.org/en-US/docs/Mozilla/HTTP_cache
  
[33] Caching best practices & max-age gotchas
  https://jakearchibald.com/2016/caching-best-practices/
  
[41] Caching Tutorial for Web Authors and Webmasters
  http://mesh.tistory.com/entry/HTTP-Caching-Tutorial
  http://www.docin.com/p-676023364.html
  
[42] HTTP协议：不可小觑的Content-Length
  https://my.oschina.net/xishuixixia/blog/93185
  
[51] HTTP cookie
https://en.wikipedia.org/wiki/HTTP_cookie

[52] HTTP State Management Mechanism
https://www.ietf.org/rfc/rfc2109.txt

[53] ssl-cookies
https://blog.hboeck.de/uploads/ssl-cookies.pdf

[61] Squid configuration directive cache_peer
http://www.squid-cache.org/Doc/config/cache_peer/

[7.1] HTTP 身份验证
https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Authentication

[7.2] WWW-Authenticate
https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Headers/WWW-Authenticate

[8.1] http client should emit error on content length mismatch #6300
https://github.com/nodejs/node-v0.x-archive/issues/6300

[9.1] HTTP response status codes
https://developer.mozilla.org/en-US/docs/Web/HTTP/Status#server_error_responses

[10.1] HTTP/Headers/Server-Timing
https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Server-Timing

[10.2] Web/API/PerformanceServerTiming
https://developer.mozilla.org/en-US/docs/Web/API/PerformanceServerTiming

## 例子
GET /index.html HTTP/1.1
Host: www.example.com

HTTP/1.1 200 OK
Date: Mon, 23 May 2005 22:38:34 GMT
Content-Type: text/html; charset=UTF-8
Content-Length: 138
Last-Modified: Wed, 08 Jan 2003 23:11:55 GMT
Server: Apache/1.3.3.7 (Unix) (Red-Hat/Linux)
ETag: "3f80f-1b6-3e1cb03b"
Accept-Ranges: bytes
Connection: close

<html>...

## 状态码
[9.1]
Informational responses (100 – 199)
Successful responses (200 – 299)
Redirection messages (300 – 399)
Client error responses (400 – 499)
Server error responses (500 – 599)

## 半双工还是全双工？
对于HTTP 1.1，逻辑上说是半双工的[13]：
* 成对的请求-响应在时间上是不交叠的，即服务器要接收完整个请求后，才可以开始发送响应。
* 在一个TCP连接上，客户端可以不等上一个请求的响应完全返回，就接着发送下一个请求；服务器应该按照请求的顺序发送响应。前提是这些请求的方法是“安全的”，例如GET。

但根据[13]，HTTP1.1 规范对这一点并不很明确，并建议使用 "X-Accept-Streaming: full-duplex;timeout=30" 之类的头来协商启用全双工特性。

HTTP 2.0 和 fetch API 标准显式支持全双工，允许在客户端发送完全部请求体之前就开始发送响应，但浏览器开发还在进行中（2018.6）[25]。

经过测试，node.js http模块作为服务器和客户端在 http 1.1 协议下是允许全双工的。

## 持久连接
HTTP/1.0 的持久连接机制是后来才引入的，通过 Connection: keep-alive 这个头部来实现，服务端和客户端都可以使用它告诉对方在发送完数据之后不需要断开 TCP 连接，以备后用。HTTP/1.1 则规定所有连接都必须是持久的，除非显式地在头部加上 Connection: close。所以实际上，HTTP/1.1 中 Connection 这个头部字段已经没有 keep-alive 这个取值了，但由于历史原因，很多 Web Server 和浏览器，还是保留着给 HTTP/1.1 长连接发送 Connection: keep-alive 的习惯 [21]。

## Content-Length 和 Transfer-Encoding

在使用持久连接的情况下，表示“消息传输完毕”的方法有两个：发送正确的 content-length 头；使用 transfer-Encoding : chunked 编码。
前者适用于发送前已知大小的消息，如静态文件；后者则适用于发送前不易确定或不知道大小的消息，如 Server Sent Event，动态生成的页面等。

使用这两种方式都可以让客户端检测到连接过早关闭的错误。如果发生这种错误，客户端应该报错，代理服务器不应该缓存这种响应[42]。

在头部加入 Transfer-Encoding: chunked 之后，就代表这个报文采用了分块编码。这时，报文中的实体需要改为用一系列分块来传输。
每个分块包含十六进制的长度值和数据（没有"0x"前缀），长度值独占一行，长度不包括它结尾的 CRLF（\r\n），也不包括分块数据结尾的 CRLF。
最后一个分块长度值必须为 0，对应的分块数据没有内容，表示实体结束。按照这个格式改造下之前的代码[21]：

JS
require('net').createServer(function(sock) {
    sock.on('data', function(data) {
        sock.write('HTTP/1.1 200 OK\r\n');
        sock.write('Transfer-Encoding: chunked\r\n');
        sock.write('\r\n');

        sock.write('b\r\n');
        sock.write('01234567890\r\n');

        sock.write('5\r\n');
        sock.write('12345\r\n');

        sock.write('0\r\n');
        sock.write('\r\n');
    });
}).listen(9090, '127.0.0.1');

上面这个例子中，我在响应头中表明接下来的实体会采用分块编码，然后输出了 11 字节的分块，接着又输出了 5 字节的分块，最后用一个 0 长度的分块表明数据已经传完了。用浏览器访问这个服务，可以得到正确结果。可以看到，通过这种简单的分块策略，很好的解决了前面提出的问题。

Transfer-Encoding 不影响 content-length 和 etag，即 content-length 和 etag 应根据编码前的数据产生。

注：node.js 的 http 模块自动编解码 Transfer-Encoding，即客户代码发送和接收的都是未编码的数据。

Transfer-Encoding 的值除了 chunked 以外还可以有 gzip, deflate 等。实际上一个消息可以用多个编码，例如 
Transfer-Encoding: gzip, chunked
意思是先用 gzip，再用 chunked 编码。chunked 必须在最外层。

响应也可以既不使用 Content-Length 也不使用 chunked 编码，这时就要靠传输完毕后关闭TCP连接来指示响应体的结束了。
但有请求体的请求（POST/PUT）不能这样做，原因很简单，如果请求体发送完毕就关闭连接，就无法接收到响应了。
所以，有请求体的请求既不使用 Content-Length 也不使用 chunked 编码是个错误，node.js http 遇到这种情况会立即断开连接。
有些老旧的 HTTP 服务器和客户端的实现要求请求体必须提供 Content-Length，而不支持 chunked 编码。

## 长度不匹配：Content-Length / Transfer-Encoding 与响应体实际长度不匹配，提前结束的响应体。
客户端实际接收到的数据长度与 Content-Length 和 Transfer-Encoding 指定的长度可能会出现不匹配。有以下几种情况：
* 服务器端提前结束了发送响应体，一般是提前关闭相应的 TCP 连接。这又可能有几种原因：
  * 客户端的 TCP ACK 包丢失或延迟太久，服务器认为 TCP 连接已经失效，于是关闭。包丢失或延迟则可能由于网络拥塞。
  * 服务器负载太高，决定提前结束一些连接。
  * 服务器崩溃导致的连接关闭。
* 中间网络节点的流量控制，切断 TCP 连接。
* 服务器端有 bug，Content-Length 和 Transfer-Encoding 指定的长度是错的。

HTTP 协议规定，长度不匹配的情况下客户端应该报错如何处理。客户端的主流实现是报错，但也有不视为错误的，例如 node.js 的 http(s) 客户端，使用时要小心[8.1]。

## Content-encoding
Content-encoding 可以用于指定消息正文的压缩方式，如 gzip 或 deflate （其实际格式是zlib）。与 Transfer-Encoding 不同，Content-encoding 影响 content-length。
即后者应该是编码后的消息正文的长度。

Content-encoding 不影响 content-type。

可以同时使用 Content-encoding 和 Transfer-Encoding，编码时应先做 Content-encoding 后做 Transfer-Encoding，解码时则相反。

请求头 accept-encoding 指出的编码方式应用于 Content-encoding，而不是 Transfer-Encoding。

Content-encoding 也可以用于请求；如果服务器不支持该编码，应返回状态 415  (Unsupported Media Type)。

## Transfer-Encoding 和 Content-encoding，如何选择

理论上说，要压缩消息最好使用 TE，因为它是消息的属性，不是载荷的属性，TE的压缩不影响 content-length 和 etag。
而 Content-encoding 理论上是载荷的属性，影响 content-length 和 etag，进而影响缓存。

但是实际上浏览器似乎都不支持除了 chunked 以外的 TE，而且 Accept-Encoding 也只针对 CE，无法影响 TE，因此在实践中，CE 部分扮演了 TE 的角色：
如果要压缩消息，就应该使用 CE 指出压缩格式，TE 中则最多用一下 chunked（因为有时压缩后的长度无法预先知道，无法设置 content-length）。
而 etag 应该反映 CE 编码前的载荷（尽管这是有争议的[24]）。

## Date 头
### 概述
格式为 `Date: <day-name>, <day> <month> <year> <hour>:<minute>:<second> GMT`，例如 `Date: Wed, 21 Oct 2015 07:28:00 GMT`。
请求和响应都可以包含此头，意思是产生该消息的时间。代理服务器在存储和转发时不应该修改这个值。

### 响应头 Date
原则上 Date 头在任何响应中都应该有，意思是产生该消息的时间，大致上就是服务器开始发送该消息的时间。
Date 被客户端用来计算消息的年龄（age），详见“年龄”一节。

### 请求头 Date
请求头 Date是可选的，大部分客户端默认不发送 Date 头。服务器一般都可以解析请求头 Date。

## 缓存 cache
### 基本概念
这里不讨论“反向代理”。

从服务器到客户端，整个数据传播路径上的节点分为3类：
1. 服务器
2. 缓存
  缓存又分两种：本地缓存（如浏览器的缓存）和网络缓存（即代理服务器）。数据传播路径上可能有多个缓存节点。
3. 客户端
  注意这里将客户端和本地缓存作为两个独立的节点来考虑。

HTTP 协议中有几个关于缓存条目的重要概念：新鲜度、验证机制、可缓存性。

* 年龄 age
  age 的确切含义是：缓存条目在缓存中驻留的时间（驻留时间， resident_time ），加上http响应在网络中传输消耗的时间（响应延迟，response_delay）[7, 10]。
  
  计算方法：
  current_age = corrected_initial_age + resident_time
  resident_time = now - response_time
  corrected_initial_age = max(apparent_age, corrected_age_value)
  corrected_age_value = age_value + response_delay
  response_delay = response_time - request_time
  apparent_age = max(0, response_time - date_value)
  
  解释：
    age_value 是上级缓存提供的 'age' 头，表明其估算的 age 值，如果不存在，则定为 0。
    date_value 是原始服务器的 'date' 头，按原始服务器的时钟计算。
    request_time 和 response_time 是本级缓存对上级发起请求和收完响应的时间，按本级缓存的时钟计算。
    now 是当前时间，按本级缓存的时钟计算。
    
  全部展开后：
    current_age = max(max(0, response_time - date_value), age_value + response_time - request_time) + (now - response_time)
    
  几个特殊情况下的简化：
    * 如果原始服务器和本级缓存的时钟是同步的，则 response_time - date_value 和 age_value + response_time - request_time 应该是相等的。
      current_age = response_time - date_value + (now - response_time) = now - date_value，或者
      current_age = age_value + response_time - request_time + (now - response_time) = age_value + (now - request_time)
    * 如果本级缓存的时钟显著偏快，则 
      current_age = response_time - date_value + (now - response_time) = now - date_value
    * 如果本级缓存的时钟显著偏慢，则
      current_age = age_value + response_time - request_time + (now - response_time) = age_value + (now - request_time)

* 新鲜度
  缓存条目的新鲜度是两个状态：新鲜/过时。新鲜的条目可以直接发送给客户端，过时的则需要先去服务器验证数据是否改变（以下简称验证/validate），再发送给客户端。
  新鲜度由 Expires: <UTC time> 和 Cache-Control: max-age=<duration> 响应头控制。
  Expires 指定一个绝对时间点（UTC格式），在这之前是新鲜的，反之则是过时的。
    很明显，只有缓存和远端服务器的时钟是同步的，Expires才能正常工作。
  max-age 指定一个时间间隔，单位是秒，例如 max-age=5。缓存条目的 age 小于 max-age 之前是新鲜的，反之则是过时的。
    max-age 不受时钟同步问题的影响。max-age=0 将导致每次请求都去服务器验证。

  如果响应头没有指出新鲜度，既没有 max-age 也没有 expires，缓存可能采取一些启发式算法来决定新鲜度。例如，根据响应头 last-modified 和 date 推测 max-age：
    max-age = (date - last-modified) * 20%
  
  注意，规范允许 expires 和 max-age 在某些配置下被缓存忽略或放宽，从而发送过时的数据给客户端。

* 验证机制
  验证机制是用来确定服务器上的数据相对缓存的数据是否修改过。如果相同，服务器可以返回 304 响应表示“未修改”，可以使用缓存的版本；否则按普通的做法返回 2XX 等响应。  
  验证机制有两种：
  * 基于时间戳。由响应头 last-modified: <UTC time> 和请求头 if-modified-since: <UTC time> 来控制。
  * 基于校验值（ETag）。由响应头 ETag: <etag value> 和 if-none-match: <etag value> 来控制。
  这两种机制可以同时使用。
  
  304 响应可以（甚至应该）携带所有必要的新鲜度和验证机制相关的头，缓存会相应更新[4]。如果缺少某些头，缓存可能使用以前的，但并不能保证。

* 验证指示
  除了新鲜度，还有一些响应头可以直接干预验证的时机和频率：
  * 响应头 Cache-Control: no-cache
    注意意思不是不允许缓存，而是是要求缓存总是先验证，再发送数据给客户端，效果类似 max-age=0
    no-cache 还可以有值，例如 no-cache="Set-Cookie"，意思是如果有Set-Cookie响应头，则要先验证。
    与新鲜度不同，这个指令不能被缓存忽略。
  * 响应头 Cache-Control: must-revalidate
    意思也是要求缓存总是先验证，再发送数据给客户端。与新鲜度不同，这个指令不能被缓存忽略。
  
* 可缓存性
  即确定一个响应能否被缓存。
  * GET 以外的方法的响应一般都不会被缓存
  * 没有验证机制的响应不会被缓存
  * Cache-Control: no-store 响应头会阻止缓存
  * Cache-Control: private 响应头会阻止共享的缓存（即代理服务器），但不会阻止私有缓存（如浏览器缓存）。Cache-Control: public 则允许一切形式的缓存。
  * 可变响应，即会根据 user-agent, Accept-Language, Accept-Encoding（压缩方式）, Accept-Charset, Cookie 等请求头的不同而不同的响应。可变响应会影响可缓存性。
    可变响应用 Vary: <header_name1>,  <header_name2> 标记，表示它会根据哪些请求头而变化[19]。
    缓存如果可以存储一份响应的多个版本，并记录与 Vary 指示的字段的对应关系，则可以缓存可变响应，否则就不应缓存。
    不过，由于几乎所有的浏览器都支持 gzip 和 deflate 压缩的响应，所以如果 vary 是 Accept-Encoding 且 content-encoding 是 gzip 或 deflate，则可以放心缓存。
    
    可变响应可能会使用 Content-Location 来给出一个别名url，例如
      GET /myResource
      Accept: application/xml
    可能得到
      Content-Type: application/xml
      Content-Location: http://example.org/myResource?format=xml
    这时缓存可以将 Content-Location 指定的 url 和原始 url （加上Content-Type）与缓存项关联。

### 请求头中的缓存控制指令
客户端也可以在请求头中包括部分 Cache-Control 指令，它对访问路径上的缓存会产生影响[15]。
* Cache-Control: max-age=0
指示缓存在返回副本前必须去源头验证。

如果在浏览器中用 fetch/XHR 访问时包含 Cache-Control: max-age=0 头，则浏览器发送的真实请求中会包含 Cache-Control: max-age=0，并加上 if-modified-since 和 if-none-match 头（如果浏览器缓存了），源服务器可以返回304响应。在浏览器中按“刷新”按钮的效果与此相同。

* Cache-Control: no-cache
指示缓存从源头重新获取副本再返回。注意这与响应头中的 no-cache 的语义不太一致。

如果在浏览器中用 fetch/XHR 访问时包含 Cache-Control: no-cache 头，则浏览器发送的真实请求中会包含 Cache-Control: no-cache，并略去 if-modified-since 和 if-none-match 头，从而确保路径上的缓存和源头返回新的副本。在浏览器中强制刷新（shift+“刷新”）的效果与此相同。

请求头中的 Cache-Control: must-revalidate 等没有效果。

* Cache-Control: only-if-cached
指示缓存返回缓存的副本，缓存不考虑是否新鲜。如果没有缓存的副本，则出错，而不会去请求源头。出错的具体方式是返回 504 Gateway Timeout 响应[2]。
squid 的报错方式是返回 504 Gateway Timeout 响应，并加上 X-Squid-Error: ERR_ONLY_IF_CACHED_MISS 0 和 X-Cache: MISS from... 头。

如果多台缓存节点有协作关系，则允许从有协作关系的节点获取数据[2]。squid 的 sibling 节点属于此类，可以通过 allow-miss 选项来控制 only-if-cached 的使用[61]。

### 相互依赖的文件与缓存
html 与其引用的 css, js 等文件通常存在版本依赖。如果其中部分是旧版本，部分是新版本，则会出问题。
HTTP 协议本身无法表达文件之间的依赖关系，所以合理的做法是：被依赖的文件修改时，总是采用一个新的url（文件名里含版本号，或查询串含版本号），并且设置足够长的 max-age，如一年 [33]。

采用 cache-control: max-age=0 或 no-cache 是不合适的，因为一方面降低了加载速度，另一方面某些有 bug 的代理服务器会无视它们。

主文档 （html 文件）如果经常修改，但 url 不能变，则可以采用 max-age=0 或 no-cache。

### cache-control: immutable
如果文件修改时，总是采用一个新的url，这等效于文件是不可变的，因此验证机制其实是不必要的，304响应也纯粹是浪费资源。
为了优化这种情况，引入了一个新的 cache-control: immutable 指令 [16]，目前 Firefox 49+ 和 Safari 实现了[17]。
支持这个头的浏览器将不会发出条件请求，而是总是使用缓存的版本（除非强制刷新或有 no-cache 指令存在）。

对于已经采用了超长 max-age 的网站来说，immutable 指令主要是优化了非强制刷新的性能。
前面提到，非强制刷新时，即使资源没有过期，浏览器也会去验证（加上max-age=0指令）。为什么不能简单地去掉浏览器的这种行为，从而提高非强制刷新的性能？
这是因为“资源没有过期”并不意味着“资源是最新的版本”，而用户对非强制刷新的期待是“得到最新的版本”。immutable 表明资源“永远只有一个版本”，因此解决了这个问题[18]。

不过，Chrome 54+ 似乎确实改变了非强制刷新时的行为，作为 immutable 指令的替代：不验证未过期的资源[17]。

### 304 响应
304 响应应该包括哪些头？

RFC7232 updates RFC2616 to say[4]:

    The server generating a 304 response MUST generate any of the following header fields that would have been sent in a 200 (OK) response to the same request: Cache-Control, Content-Location, Date, ETag, Expires, and Vary.


### XMLHttpRequest Caching Tests
This is a set of functional tests for determining how client-side HTTP caches operate.
http://www.mnot.net/javascript/xmlhttprequest/cache.html

### x-cache 响应头
[11]。

### 部分响应的缓存
HTTP 规范允许缓存部分响应(206)，并且允许多个部分响应合并[8,9]。
部分响应不应改变 Etag 和 Last-Modified 头。

主要浏览器，如 Firefox 和 Chrome，似乎都支持缓存部分响应。Firefox 的实现说明可参考[32]。

代理服务器可以缓存也可以不缓存部分响应，也可以在客户端请求部分响应时，去源头取得整个响应，从而优化后续的部分请求。squid 可以配置这种行为，见
range_offset_limit 和 quick_abort_min。


## 部分请求
请求使用 Range 头，响应使用 Content-Range 头，来指出区段的范围。部分响应的状态码是 206（partial content）。

  GET /abc
  Range: bytes 21010-47021

  HTTP/1.1 206 Partial content
  Date: Wed, 15 Nov 1995 06:25:24 GMT
  Last-Modified: Wed, 15 Nov 1995 04:58:08 GMT
  Content-Range: bytes 21010-47021/47022
  Content-Length: 26012
  Content-Type: image/gif
  
  Examples of byte-ranges-specifier values (assuming an entity-body of length 10000):

      - The first 500 bytes (byte offsets 0-499, inclusive):  bytes=0-
        499

      - The second 500 bytes (byte offsets 500-999, inclusive):
        bytes=500-999

      - The final 500 bytes (byte offsets 9500-9999, inclusive):
        bytes=-500

      - Or bytes=9500-

      - The first and last bytes only (bytes 0 and 9999):  bytes=0-0,-1

      - Several legal but not canonical specifications of the second 500
        bytes (byte offsets 500-999, inclusive):
         bytes=500-600,601-999
         bytes=500-700,601-999

服务器和代理服务器并非必须支持部分请求，而是可以对部分请求返回状态 200 和完整的响应。

## HTTP 身份认证

### 概述
HTTP 身份认证有多种实现方式：

* HTTP 认证[7.1]。是 HTTP 协议定义的用户名+密码的认证方式，通过特定 HTTP 头（WWW-Authenticate/Authorization）传递认证信息。几乎所有的web服务器和浏览器都支持，无需编程。可进一步细分为 basic（基本）和 digest（摘要）等多种方式。此外，HTTP 认证还包括代理服务器认证（Proxy-Authenticate/Proxy-Authorization 头）。
* TLS 双向认证。是 TLS 协议定义的基于公钥体制的认证方式。客户端持有自己的证书，证书是被服务器端的认可的根证书签发的，客户端访问服务器时要提供自己的公钥证书来证明自己的身份。几乎所有的web服务器和浏览器都支持，无需编程。
* 自定义的认证方式。可以使用用户名+密码、短信、单点登录等方式认证，通常使用 cookie 机制来保持登录状态。自定义认证通常需要编程来实现。

### HTTP basic 认证

WWW-Authenticate and Authorization headers

client:
Get /index.htm
Host:www.a.com

Server:
HTTP 401 Login Required 
WWW-Authenticate: Basic realm="realm A"

client:
Get /index.htm
Authorization: GTu#^j98
Server:
HTTP 200 OK
...

之后浏览器会对每个请求自动发送Authorization头，如果服务器需要（甚至服务器并没有明示）[7.1]。
Authorization 头携带了用户名和密码，即“用户名:密码“的base64形式，因此属于明文传输密码[7.2]。应当使用 https 来保护。

在服务器端，HTTP basic 认证通常使用文本文件来存储用户名和密码。例如 apache 和 nginx 通用的户名和密码文件格式为：
```
aladdin:$apr1$ZjTqBB3f$IF9gdYAGlMrs2fuINjHsz.
user2:$apr1$O04r.y2H$/vEkesPhVInBByJUkXitA/
```
每一行冒号前为用户名，冒号后为base64编码的密码。

nginx 中的 HTTP basic 认证配置方式为：
```
location /status {
    auth_basic           "Access to the staging site";
    auth_basic_user_file /etc/apache2/.htpasswd;
}
```
auth_basic_user_file 应指向一个上述户名和密码文件。

apache 中的 HTTP basic 认证配置方式为，在需要认证访问的目录下建立 `.htaccess` 文件，内容是：

```
AuthType Basic
AuthName "Access to the staging site"
AuthUserFile /path/to/.htpasswd
Require valid-user
```
AuthUserFile 应指向一个上述户名和密码文件。

### 子资源的认证问题

如果页面引用的子资源与页面不同源，且子资源需要 HTTP 认证，则浏览器可能会阻止弹出认证对话框，甚至不发送 Authorization 头，即使用户已经输入过密码。
从 Firefox 59 起，浏览器在从不同源的加载图片资源到当前的文档时，将不会再触发 HTTP 认证对话框，也不发送 Authorization 头。修改配置（about:config）`network.auth.subresource-img-cross-origin-http-auth-allow` 可能有用。 

## Fat URLs
URL中嵌入了用户标识或会话标识。

## Cookie
### Cookie 的例子
http://www.httpwatch.com/httpgallery/authentication/ 

---
client:
Get /index.htm
Host:www.a.com
Server:
HTTP 200 OK
Set-Cookie: id=1234;Domain=www.a.com
client:
Get /index.htm
Cookie: id=1234
---
除了domain，还可以指定path来进一步限制cookie的作用域：
Set-cookie: pref=compact;Domain=airtravelbargains.com;Path=/autos/

### Cookie 的参数
Cookie 的参数都可以在 Set-Cookie 头中指定，语法是：
Set-Cookie: cookie_name=cookie_value; argn1=argv1; argn2=argv2

* Domain
指定 cookie 适用的域名。设置了域名后，所有匹配这个域名的请求都会带上这个 cookie，其他域名则不会。注意这里的“域”与同源规则中的“域”不同，只包括域名部分，不包括协议和端口，因此不够严格，容易造成安全问题（可参考Web Security_Crypto.txt）。

Set-Cookie 头中，用 Domain=www.a.com 的语法指定域。域名也会匹配子域名。为了兼容老的实现，也可以用 "." 来显式指定匹配子域名，如 .a.com。

Set-Cookie 头不可以跨域设置 cookie，也不能设置只有一节的域名，如 .com。

* Path
用于进一步限制 cookie 的适用范围，也会匹配子路径。

* 

### 会话id，用 cookie 登录
一个典型的web登录过程如下：
用户在登录界面的表单中填写用户名和口令，浏览器提交给服务器；
服务器如果验证通过，则在响应头中设置 cookie，如 "SESSID=a38e3b0e51932347294581c5dbb70d80" 。
以后用户访问网站的任何页面时，都会发送 cookie "SESSID=a38e3b0e51932347294581c5dbb70d80"，服务器可以验证这个值。

为了安全，会话id必须是不可预测的。

## server-timing
Server-Timing 响应头字段用于将服务器端测量出的处理每个请求的中间延迟时间添加到响应中[10.1]。
这个响应头可用于支持浏览器的 PerformanceServerTiming API，这是个较新的功能，2023 年获得了广泛的浏览器支持[10.2]。

例子：
```
// Single metric with value
Server-Timing: cpu;dur=2.4

// Single metric with description and value
Server-Timing: cache;desc="Cache Read";dur=23.2

// Two metrics with value
Server-Timing: db;dur=53, app;dur=47.2
```
多个字段之间用逗号加空格分隔，每个字段的格式是 `name;dur=nnn.mmm;desc="description"`，name 是必须的，可以是随意的变量名，dur 和 desc 是可选的，意思分别是耗时（s）和描述。
