输出机读格式的显卡信息
例子：
```
$ nvidia-smi --query-gpu=index,utilization.gpu,utilization.memory --format=csv
index, utilization.gpu [%], utilization.memory [%]
0, 0 %, 0 %
1, 0 %, 0 %

nvidia-smi --query-gpu=index,utilization.gpu,memory.used --format=csv,noheader
0, 0 %, 8 MiB
1, 0 %, 259 MiB

nvidia-smi --query-gpu=index,utilization.gpu,temperature.gpu,memory.used --format=csv

```
`nvidia-smi --query-gpu` 可以查询哪些字段，可以通过 `nvidia-smi --help-query-gpu` 命令来查看。

"memory.used"
Total memory allocated by active contexts.

"utilization.gpu"
Percent of time over the past sample period during which one or more kernels was executing on the GPU.
The sample period may be between 1 second and 1/6 second depending on the product.

"utilization.memory"
Percent of time over the past sample period during which global (device) memory was being read or written.
The sample period may be between 1 second and 1/6 second depending on the product.

但是 nvidia-smi --query-gpu 似乎不能指定累积时间，默认的累积时间较短，可能造成 utilization.gpu 的值波动较大。
