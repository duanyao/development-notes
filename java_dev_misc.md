## 参考资料

[2.1] https://sdkman.io/
[2.2] https://stackoverflow.com/questions/61783369/install-openjdkopenjfx-8-on-ubuntu-20

## sdkman
```
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
```
sdkman 可以用来安装 jdk 、gradle 等工具，可以多版本共存。sdkman 是安装到 ~/.sdkman 目录下，所以不需要 root 权限。
 
## 安装 jdk

* 通过 sdkman

```
sdk current java
sdk list java
sdk install java 8.0.272.fx-zulu  # 版本号中的 fx 表示包含了 javafx
sdk use java 8.0.272.fx-zulu
sdk default java 8.0.272.fx-zulu
```

采用系统 jdk（假定是 /usr/lib/jvm/java-11-openjdk-amd64）：

```
rm ~/.sdkman/candidates/java/openjdk-11
ln -s /usr/lib/jvm/java-11-openjdk-amd64 ~/.sdkman/candidates/java/openjdk-11
rm ~/.sdkman/candidates/java/current
ln -s openjdk-11 ~/.sdkman/candidates/java/current
```
## 安装 gradle

```
sdk list gradle
sdk install gradle 6.7.1
```
