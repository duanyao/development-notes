## 参考资料

[1] https://github.com/chromium/chromium/blob/master/docs/windows_build_instructions.md

[2] https://stackoverflow.com/questions/50305917/chromium-how-to-make-an-actual-installer-out-of-mini-installer-exe

## 设置 proxy

```
set http_proxy=http://192.168.1.35:3128 && set https_proxy=http://192.168.1.35:3128
set http_proxy=http://192.168.1.6:3128 && set https_proxy=http://192.168.1.6:3128
```

```
netsh winhttp show proxy
netsh winhttp set proxy "192.168.1.35:3128"
netsh winhttp reset proxy
```

修改 C:\software\depot_tools\.cipd_impl.ps1:116

```
  echo "Downloading CIPD client for $Platform from $URL..."
  $proxy = New-Object System.Net.WebProxy("http://192.168.1.35:3128")
  $wc = (New-Object System.Net.WebClient)
  $wc.proxy = $proxy
```

## 获取源码

安装 depot_tools ：

在系统 path 中加入以下两项，且应该在最前面。
```
C:\software\depot_tools
P:\@home\duanyao\t-project\depot_tools.git
P:\@home\duanyao\t-project\chromium\out\win-hevc
```

增加环境变量 DEPOT_TOOLS_WIN_TOOLCHAIN ，设置为 0 。

```
cd P:\@home\duanyao\t-project\chromium\src
gclient revert # 可选，撤销全部未提交修改，并执行 sync。可以修复unix文件模式问题，不需要再使用 git config core.filemode false 。
gclient sync   # 可选，从服务器更新全部代码，并执行 runhooks
gclient runhooks
```

```
IOError: [Errno 13] Permission denied: 'P:\\@home\\duanyao\\t-project\\chromium\\src\\.landmines'
```
无法写入，似乎没有写入权限。

winbtrfs 的权限问题非常难以理解和解决。该用管理员身份运行控制台（cmd.exe）可能会好一些。

============================================
cd C:\project\chromium
fetch --nohooks --no-history chromium
============================================

## 编译

cd P:\@home\duanyao\t-project\chromium\src
gn gen ..\out\win-hevc

```
Exception: dbghelp.dll not found in "C:\Program Files (x86)\Windows Kits\10\Debuggers\x64\dbghelp.dll"
You must installWindows 10 SDK version 10.0.19041.0 including the "Debugging Tools for Windows" feature.
ERROR at //build/toolchain/win/BUILD.gn:49:3: Script returned non-zero exit code.
  exec_script("../../vs_toolchain.py",
```

VS 自带的 Windows 10 SDK 安装后不包括"Debugging Tools for Windows"，所以不行，要从微软网站上单独下载安装 Windows 10 SDK 。地址是：
https://developer.microsoft.com/en-US/windows/downloads/windows-10-sdk/
安装时，"Debugging Tools for Windows" 默认没有选中，需要自行选中。下载完毕后，至少安装 X64 Debuggers And Tools-x64_en-us.msi 文件。

修改
~/t-project/chromium/out/release-hevc/args.gn

```
enable_nacl=false
symbol_level=0
blink_symbol_level=0
is_debug = false
#is_official_build = true # enable the official build level of optimization
dcheck_always_on = true
chrome_pgo_phase = 0

ffmpeg_branding = "Chrome"
proprietary_codecs = true
enable_platform_hevc = true
```

gn gen ..\out\win-hevc
autoninja -C ..\out\win-hevc chrome -j4  # 注意，仍然需要开着代理

错误：

```
FAILED: obj/base/third_party/double_conversion/double_conversion/fixed-dtoa.obj
..\..\src\third_party\llvm-build\Release+Asserts\bin\clang-cl.exe /nologo /showIncludes:user "-imsvcC:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Tools\MSVC\14.26.28801\ATLMFC\include"
...
CreateProcess failed: The system cannot find the file specified.
```

文件 src\third_party\llvm-build\Release+Asserts\bin\clang-cl.exe 确实不存在。
原因可能是 src\third_party\llvm-build\Release+Asserts 已经存在 linux 版本，所以 windows 版本就被忽略了，没有下载。

```
dir src\third_party\llvm-build\Release+Asserts\bin\

2020/07/10  15:31    <DIR>          .
2020/07/23  07:09    <DIR>          ..
2020/07/10  15:31        10,366,528 llvm-ar
2020/07/23  07:09    <SYMLINK>      lld-link [lld]
2020/07/10  15:31         3,361,264 llvm-symbolizer
2020/07/10  15:31        44,573,024 lld
2020/07/10  15:31        63,770,632 clang
2020/07/10  15:31         3,944,072 llvm-pdbutil
2020/07/23  07:09    <SYMLINK>      clang++ [clang]
2020/07/23  07:09    <SYMLINK>      clang-cl [clang]
2020/07/10  15:31         2,906,896 llvm-objcopy
2020/07/10  15:31           242,880 llvm-undname
2020/07/23  07:09    <SYMLINK>      ld.lld [lld]
```

将 Release+Asserts 改名为 Release+Asserts.linux ，再运行一遍 
gclient runhooks ，则 Release+Asserts 重新被创建，并且下载了 windows 版：

```
dir src\third_party\llvm-build\Release+Asserts\bin\

2020/07/10  15:46    <DIR>          .
2020/08/06  11:29    <DIR>          ..
2020/07/10  15:46        64,008,704 clang-cl.exe
2020/07/10  15:46        43,429,888 lld-link.exe
2020/07/10  15:46         4,516,864 llvm-pdbutil.exe
2020/07/10  15:46         4,112,384 llvm-symbolizer.exe
2020/07/10  15:46           713,216 llvm-undname.exe
```

错误：
```
FAILED: gen/chrome/elevation_service/elevation_service_idl.h gen/chrome/elevation_service/elevation_service_idl.dlldata.c gen/chrome/elevation_service/elevation_service_idl_i.c gen/chrome/elevation_service/elevation_service_idl_p.c gen/chrome/elevation_service/elevation_service_idl.tlb
C:/software/depot_tools/bootstrap-3_8_0_chromium_8_bin/python/bin/python.exe ../../src/build/toolchain/win/midl.py environment.x64

To rebaseline:
  copy /y c:\users\duanyao\appdata\local\temp\tmp9fec9q\* P:\@home\duanyao\t-project\chromium\src\third_party\win_build_output\midl\chrome\elevation_service\x64
ninja: build stopped: subcommand failed.
Traceback (most recent call last):
  File "C:\software\depot_tools\ninjalog_uploader_wrapper.py", line 125, in <module>
    sys.exit(main())
  File "C:\software\depot_tools\ninjalog_uploader_wrapper.py", line 78, in main
    config = LoadConfig()
  File "C:\software\depot_tools\ninjalog_uploader_wrapper.py", line 33, in LoadConfig
    ninjalog_uploader.IsGoogler('chromium-build-stats.appspot.com'),
  File "C:\software\depot_tools\ninjalog_uploader.py", line 43, in IsGoogler
    _, content = h.request('https://' + server + '/should-upload', 'GET')
  File "C:\Users\duanyao\.vpython-root\e5b9c6\lib\site-packages\httplib2\__init__.py", line 1659, in request
    (response, content) = self._request(conn, authority, uri, request_uri, method, body, headers, redirections, cachekey)
  File "C:\Users\duanyao\.vpython-root\e5b9c6\lib\site-packages\httplib2\__init__.py", line 1399, in _request
    (response, content) = self._conn_request(conn, request_uri, method, body, headers)
  File "C:\Users\duanyao\.vpython-root\e5b9c6\lib\site-packages\httplib2\__init__.py", line 1355, in _conn_request
    response = conn.getresponse()
  File "C:\software\depot_tools\bootstrap-3_8_0_chromium_8_bin\python\bin\Lib\httplib.py", line 1135, in getresponse
    raise ResponseNotReady()
httplib.ResponseNotReady
```
解决：设置 http_proxy 

错误：
```
FAILED: gen/chrome/elevation_service/elevation_service_idl.h gen/chrome/elevation_service/elevation_service_idl.dlldata.c gen/chrome/elevation_service/elevation_service_idl_i.c gen/chrome/elevation_service/elevation_service_idl_p.c gen/chrome/elevation_service/elevation_service_idl.tlb
C:/software/depot_tools/bootstrap-3_8_0_chromium_8_bin/python/bin/python.exe ../../src/build/toolchain/win/midl.py environment.x64 P:/@home/duanyao/t-project/chromium/src/third_party/win_build_output/midl/chrome/elevation_service gen/chrome/elevation_service none elevation_service_idl.tlb elevation_service_idl.h elevation_service_idl.dlldata.c elevation_service_idl_i.c elevation_service_idl_p.c ../../src/third_party/llvm-build/Release+Asserts/bin/clang-cl.exe ../../src/chrome/elevation_service/elevation_service_idl.idl /char signed /env x64 /Oicf
midl.exe output different from files in gen/chrome/elevation_service, see c:\users\duanyao\appdata\local\temp\tmp1tcwvc
--- gen/chrome/elevation_service\elevation_service_idl_p.c

...

To rebaseline:
  copy /y c:\users\duanyao\appdata\local\temp\tmpa_u0vo\* P:\@home\duanyao\t-project\chromium\src\third_party\win_build_output\midl\chrome\elevation_service\x64
```

解决：参考 P:\@home\duanyao\t-project\chromium\src\third_party\win_build_output\README.chromium
执行 

```
copy /y c:\users\duanyao\appdata\local\temp\tmpa_u0vo\* P:\@home\duanyao\t-project\chromium\src\third_party\win_build_output\midl\chrome\elevation_service\x64
```

错误：
```
  File "gen\mojo\public\tools\bindings\cpp_templates.zip\tmpl_b512df959760731b9d160be4449b40e02aea72c2.pyc", line 56, in top-level template code
ValueError: bad marshal data (unknown type code)
```

解决：尝试删除 P:\@home\duanyao\t-project\chromium\out\win-hevc\gen\mojo\public\tools\bindings 目录，继续编译，可能还会在 gen\mojo 出错几次，尝试继续编译几次，错误并未消失。
最终，执行 gclient revert，删除输出目录，从头开始编译，似乎解决了。

错误：

```
Error: ENOENT: no such file or directory, open 'P:\@home\duanyao\t-project\chromium\out\win-hevc\gen\src\third_party\devtools-frontend\src\test\e2e\resources\sources\different-workers.html
    at Object.openSync (fs.js:440:3)
    at Object.writeFileSync (fs.js:1265:35)
    at Object.<anonymous> (P:\@home\duanyao\t-project\chromium\src\third_party\devtools-frontend\src\scripts\build\ninja\copy-files.js:22:6)
    at Module._compile (internal/modules/cjs/loader.js:955:30)
    at Object.Module._extensions..js (internal/modules/cjs/loader.js:991:10)
    at Module.load (internal/modules/cjs/loader.js:811:32)
    at Function.Module._load (internal/modules/cjs/loader.js:723:14)
    at Function.Module.runMain (internal/modules/cjs/loader.js:1043:10)
    at internal/main/run_main_module.js:17:11 {
```

win-hevc\gen\src 这个目录在成功的编译（linux）或失败的编译后都不存在；原始版本在：

P:\@home\duanyao\t-project\chromium\src\third_party\devtools-frontend\src\test\e2e\resources\sources

问题似乎是 copy-files.js 没有先创建目标目录，就开始复制文件；也有可能是目标目录弄错了。按前一种改，`P:\@home\duanyao\t-project\chromium\src\third_party\devtools-frontend\src\scripts\build\ninja\copy-files.js` 增加了两行：
```
  const destDir = path.dirname(destPath);
  if (!fs.existsSync(destDir)) fs.mkdirSync(destDir, { recursive: true });
```

错误：

```
FAILED: protoc.exe protoc.exe.pdb
ninja -t msvc -e environment.x64 -- ..\..\src\third_party\llvm-build\Release+Asserts\bin\lld-link.exe /nologo -libpath:..\..\src\third_party\llvm-build\Release+Asserts\lib\clang\12.0.0\lib\windows "-libpath:C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Tools\MSVC\14.26.28801\ATLMFC\lib\x64" "-libpath:C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Tools\MSVC\14.26.28801\lib\x64" "-libpath:C:\Program Files (x86)\Windows Kits\10\lib\10.0.19041.0\ucrt\x64" "-libpath:C:\Program Files (x86)\Windows Kits\10\lib\10.0.19041.0\um\x64" /OUT:./protoc.exe /PDB:./protoc.exe.pdb @./protoc.exe.rsp
lld-link: error: could not get the member for symbol __declspec(dllimport) private: class std::__1::allocator<char> const & __cdecl std::__1::basic_string<char, struct std::__1::char_traits<char>, class std::__1::allocator<char>>::__alloc(void) const: truncated or malformed archive (terminator characters in archive member "10" not the correct "`\n" values for the archive member header for @   0     644
```


autoninja -C "..\out\win-hevc"  -j4 -v -t clean protoc.exe
autoninja -C "..\out\win-hevc"  -j4 -v protoc.exe

## ffmppeg
增加 ffmpeg 的解码器需要重新编译它——这次编译的目的并非生成可执行代码，而是让 chromium 的构建系统统计一下需要包含哪些 ffmpeg 的文件。

在 windows 上编译，需要 cygwin 和 visual studio。虽然 chromium 的文档说 mingw + VS 也可以，但我没能成功。

以下是编译的步骤。

安装 cygwin 64位，其中应包括 python2, make, nasm, perl 包。python2 不能用 windows 的，因为它会调用 shell 脚本，windows 版 python 不支持。
nasm 不能用 windows 版的，因为它不认识 cygwin 的路径。

启动 64 位 VS 命令行，再从中启动 cygwin 命令行（拖拽 cygwin 桌面图标进来回车即可）。目的是将 VS 的编译器等工具加入 PATH 。

```
mv /usr/bin/link.exe /usr/bin/link-gnu.exe  # (1) cygwin 自带的 link 程序与 VS 的链接器 link 重名了。
cd /cygdrive/c/project/chromium/src/third_party/ffmpeg
cp chromium/scripts/cygwin-wrapper /usr/local/bin  # (2) cygwin-wrapper 作为 ffmpeg 的编译器、链接器的中介
/usr/bin/python chromium/scripts/build_ffmpeg.py win x64 # (3) 此处 python 不能是 windows 版 python，要确保是 cygwin 版的 python 2.7
chromium/scripts/copy_config.sh  # (4) 更新 chromium/config/Chromium/linux/x64/config.h , chromium/config/Chrome/linux/x64/libavcodec/codec_list.c 等文件
chromium/scripts/generate_gn.py  # (5) 更新 ffmpeg_generated.gni 文件。必须从 /cygdrive/c/project/chromium/src/third_party/ffmpeg/ 目录运行，并且在 build_ffmpeg.py linux x64 和 copy_config.sh 之后运行。
```

错误(3)：
```
EXTERN_PREFIX="" AR="cygwin-wrapper lib" NM="cygwin-wrapper dumpbin -symbols" /cygdrive/c/project/chromium/src/third_party/ffmpeg/compat/windows/makedef libavformat/libavformat.ver libavformat/allformats.o libavformat/avio.o libavformat/aviobuf.o libavformat/cutils.o libavformat/dump.o libavformat/file_open.o libavformat/flac_picture.o libavformat/flacdec.o libavformat/format.o libavformat/id3v1.o libavformat/id3v2.o libavformat/isom.o libavformat/matroska.o libavformat/matroskadec.o libavformat/metadata.o libavformat/mov.o libavformat/mov_chan.o libavformat/mov_esds.o libavformat/mp3dec.o libavformat/mux.o libavformat/oggdec.o libavformat/oggparsecelt.o libavformat/oggparsedaala.o libavformat/oggparsedirac.o libavformat/oggparseflac.o libavformat/oggparseogm.o libavformat/oggparseopus.o libavformat/oggparseskeleton.o libavformat/oggparsespeex.o libavformat/oggparsetheora.o libavformat/oggparsevorbis.o libavformat/oggparsevp8.o libavformat/options.o libavformat/os_support.o libavformat/pcm.o libavformat/protocols.o libavformat/qtpalette.o libavformat/rawdec.o libavformat/replaygain.o libavformat/riff.o libavformat/riffdec.o libavformat/rmsipr.o libavformat/sdp.o libavformat/url.o libavformat/utils.o libavformat/vorbiscomment.o libavformat/wavdec.o > libavformat/avformat-58.def
make: *** [/cygdrive/c/project/chromium/src/third_party/ffmpeg/ffbuild/library.mak:102：libavutil/avutil-56.dll] 错误 1
make: *** 正在等待未完成的任务....
Could not create temporary library.
```

原因是 ffmpeg 将 VS 的 lib.exe 当做 ar 来用，但是它们接受的参数是不同的。

解决：修改 src\third_party\ffmpeg\compat\windows\makedef

```
if [ -n "$AR" ]; then
    $AR rcs ${libname} $@ >/dev/null
else
    lib.exe -out:${libname} $@ >/dev/null
fi

改为

if [ -n "$AR" ]; then
    $AR -out:${libname} $@ >/dev/null
else
    lib.exe -out:${libname} $@ >/dev/null
fi
```

错误(3)（不重要）：

```
LD      libavutil/avutil-56.dll
  正在创建库 libavutil/avutil.lib 和对象 libavutil/avutil.exp
LD      libavformat/avformat-58.dll
  正在创建库 libavformat/avformat.lib 和对象 libavformat/avformat.exp
libavcodec.a(libopusdec.o) : error LNK2019: 无法解析的外部符号 opus_strerror，函数 libopus_decode_init 中引用了该符号
libavcodec.a(libopusdec.o) : error LNK2019: 无法解析的外部符号 opus_multistream_decoder_create，函数 libopus_decode_init 中引用了该符号
libavcodec.a(libopusdec.o) : error LNK2019: 无法解析的外部符号 opus_multistream_decode，函数 libopus_decode 中引用了该符号
libavcodec.a(libopusdec.o) : error LNK2019: 无法解析的外部符号 opus_multistream_decode_float，函数 libopus_decode 中引用了该符号
libavcodec.a(libopusdec.o) : error LNK2019: 无法解析的外部符号 opus_multistream_decoder_ctl，函数 libopus_decode_init 中引用了该符号
libavcodec.a(libopusdec.o) : error LNK2019: 无法解析的外部符号 opus_multistream_decoder_destroy，函数 libopus_decode_close 中引用了该符号
libavformat\avformat-58.dll : warning LNK4088: 因 /FORCE 选项生成了映像；映像可能不能运行
```

## 打包
[2]

```
cd C:\project\chromium
mklink /d chrome src\chrome
mklink /d third_party src\third_party
cd C:\project\chromium\src
autoninja -C ..\out\win-hevc -j4 mini_installer
```
这样生成了两个主要文件：

chrome.7z  - chrome 程序本体
mini_installer.exe  - 安装器，包含了 chrome.7z 的内容

不过，mini_installer.exe  没有压缩，文件较大（600MB+），压缩后 240MB 左右。

执行  mini_installer.exe 时，没有界面，大约数十秒后，会安装到 C:\Users\<user name>\AppData\Local\Chromium\Application ，并创建桌面快捷方式。

用户数据则放在 C:\Users\<user name>\AppData\Local\Chromium\User Data\