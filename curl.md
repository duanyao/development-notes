
## http 基本用法
```
curl -i -X POST http://127.0.0.1:8185/faceDetect?name=xxx --data-binary "@xx/face.jpg"

curl -i -X POST http://localhost:9100 /detect_touch --data-binary "@test/data/detect_touch_req_1.json"
```

```
curl -H 'Accepted-Language: en-US' -H 'x-country: US' https://echo.hoppscotch.io
```
## 超时

```
--connect-timeout <seconds>
    Maximum  time  in  seconds  that you allow the connection to the
    server to take. 

-m, --max-time <seconds>
    Maximum  time  in  seconds that you allow the whole operation to
    take.
```

## 显示
```
--no-progress-meter
    Option to switch off the progress meter output without muting or otherwise affecting warning and informational messages like -s, --silent does.
```
