## 参考资料
[1.1] http://maven.apache.org/

[1.5] How do I add a project as a dependency of another project?
  https://stackoverflow.com/questions/15383322/how-do-i-add-a-project-as-a-dependency-of-another-project

[2.1] https://www.yiibai.com/maven/

[2.2] maven 仓库快速镜像
  http://www.cnblogs.com/ae6623/p/4416256.html

[2.3] 【FAQ】新版maven.aliyun.com答疑
  https://yq.aliyun.com/articles/621196?spm=a2c40.aliyun_maven_repo.0.0.dc983054sWNMUh

[2.4] https://stackoverflow.com/questions/2059431/get-source-jars-from-maven-repository

## 安装
* debian
  deepin 当前版本是 3.6 (2020.11)

  ```
  sudo apt install maven
  update-alternatives: 使用 /usr/share/maven/bin/mvn 来在自动模式中提供 /usr/bin/mvn (mvn)
```

* sdkman
参考 java_dev_misc.md

允许安装多个版本。

```
sdk list maven
sdk install maven
```
## 基本操作
Maven项目的结构和内容在一个XML文件中声明，pom.xml 项目对象模型（POM），这是整个Maven系统的基本单元。

 一些基本的操作，编译，构建，单元测试，安装，网站生成和基于Maven部署项目。

```
    使用Maven构建项目
    “mvn package”

    使用Maven清理项目
    “mvn clean”

    使用Maven运行单元测试
    “mvn test”

    将项目打包和安装项目到本地资源库 (默认 ~/.m2/repository 目录)
    “mvn install”
 
    生成基于Maven的项目文档站点
    “mvn site” 

    使用“mvn site-deploy”部署站点（WebDAV例子）
    “mvn site-deploy” 通过WebDAV部署自动生成的文档站点到服务器

    部署基于Maven的war文件到Tomcat
    “mvn tomcat:deploy” 以 WAR 文件部署到 Tomcat

    在 tomcat 中运行项目。使用mvn的内置tomcat，默认端口 8080，可以在 pom.xml 的 plugin 中配置。
    mvn tomcat:run
```

## 项目配置文件 pom.xml
pom.xml 位于项目源码的根目录下。

声明依赖：

```
<dependency>
        <groupId>org.jvnet.localizer</groupId>
        <artifactId>localizer</artifactId>
        <version>1.8</version>
</dependency>
```

当你建立这个 Maven 项目，它将依赖找不到失败并输出错误消息。

声明远程储存库：

告诉 Maven 来获得 Java.net 的依赖，你需要声明远程仓库在 pom.xml 文件这样：
pom.xml

```
<repositories>
	<repository>
	    <id>java.net</id>
	    <url>https://maven.java.net/content/repositories/public/</url>
	</repository>
</repositories>
```

## mvn 工具的配置

```
<maven 安装目录>/
  conf/
    settings.xml

 ~/
  .m2/
    settings.xml  # 配置文件，初始不存在
    repository/   # 本地仓库缓存，下载的库文件放在这里

<项目目录>/
  .mvn
    maven.config
    extensions.xml
```

注意，虽然 eclipse 的 maven 插件默认使用 ~/.m2/settings.xml 作为用户配置，但修改后需要点击“preference->maven->user settings->update settings” 才能生效。

## 搜索包

https://search.maven.org/ 可以按类名搜索。

https://repository.sonatype.org/  可以按类名搜索。

## 镜像
建议参考 https://github.com/ae6623/Zebra/blob/master/maven-repo-settings-ali.xml （放到 ~/.m2/settings.xml）
采用阿里云的镜像来加速 maven 包的下载。地址是 https://maven.aliyun.com/repository/public

eclipse 要注意刷新 ~/.m2/settings.xml 使其生效。

注意其中可能有个不合适的设置，需要根据自己的实际情况修改，或者删除：

```
  <!-- localRepository
   | The path to the local repository maven will use to store artifacts.
   |
   | Default: ${user.home}/.m2/repository
  <localRepository>/path/to/local/repo</localRepository>
  -->
  <localRepository>D:\Repositories\Maven</localRepository>
```

## 下载源码包和javadoc
在项目目录下执行命令[2.3]：
mvn dependency:sources  # 下载源码
mvn dependency:resolve -Dclassifier=javadoc # 下载 Javadocs
mvn dependency:sources -DincludeArtifactIds=guava # 下载指定包的源码
mvn eclipse:eclipse  # 下载源码后，刷新 eclipse 工程，关联源码到 jar。

在 eclipse 中，项目节点右键->maven->download source/javadocs 即可下载源码/文档。


## 多模块项目/子项目
如果一些子项目共同组成一个大项目，那么每个子项目仍然用一个普通 pom.xml 来配置，而父项目可以也用一个 pom.xml 来引用子项目。例如


```
parent
|- pom.xml
|- Project1
|   `- pom.xml
`- Project2
    `- pom.xml
```

在 parent/pom.xml 中使用 `<modules>` 和 `<module>` 元素来引用子项目的目录：

```
<project>
   ...
   <artifactId>myparentproject</artifactId>
   <groupId>...</groupId>
   <version>...</version>

   <packaging>pom</packaging>
   ...
   <modules>
     <module>Project1</module>
     <module>Project2</module>
   </modules>
   ...
</project>
```

注意 `<packaging>pom</packaging>`。


可以在父项目目录运行各种 mvn 命令，这就相当于在每个子项目上分别运行命令，并且会自动按依赖关系运行。
