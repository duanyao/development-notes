## 参考资料
https://www.tutorialspoint.com/java8/java8_nashorn_java_script.htm

## 

例子：
```
var BigDecimal = Java.type('java.math.BigDecimal');

function calculate(amount, percentage) {

   var result = new BigDecimal(amount).multiply(new BigDecimal(percentage)).divide(
      new BigDecimal("100"), 2, BigDecimal.ROUND_HALF_EVEN);
   
   return result.toPlainString();
}
var result = calculate(568000000000000000023,13.9);
print(result);
```
Java.type('javafx.util.Pair')
