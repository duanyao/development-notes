[1.1] MMD（MikumikuDance）的发展历史
  https://www.bilibili.com/read/cv20273?from=articleDetail
  
[1.2] Step-by-Step instructions for MikuMikuDance
  https://learnmmd.com/StartHere/
  
[1.3] 最系统的MMD零基础入门教程宝典
  https://www.bilibili.com/read/cv2659946
  https://www.bilibili.com/video/BV1G4411e7hz/?spm_id_from=333.788.videocard.7
  
[1.4] RedialC MMD 软件教程
  https://space.bilibili.com/11831050/channel/detail?cid=14735
  
[1.5]【MMD教程】入门级引导教程 可以说非常有用了
  https://www.bilibili.com/video/BV1Sb411A7uC/?spm_id_from=333.788.videocard.11
  
[1.6]【MMD教程】这应该是最详细的MMD入门教程了吧
  https://www.bilibili.com/video/BV1dx411m7JB/?spm_id_from=333.788.videocard.3
  
[1.7]【MMD动作教程】初级篇-关键帧概念与动作节奏 曦歪（CY）
  https://www.bilibili.com/video/BV1cs411Q7wY/?spm_id_from=333.788.videocard.4
  
[1.8] 【MMD动作教程】初级篇-骨骼轴向 曦歪（CY）
  https://www.bilibili.com/video/BV1Zs411B7MV/?spm_id_from=333.788.videocard.4
  
[1.9] 【MMD动作教程】动作制作新人向教学：MMD动作制作基本要领讲解 曦歪（CY）
  https://www.bilibili.com/video/BV1Es411S7rs
  
[1.10] MMD动作制作新人入门级教程 曦歪（CY）
  https://space.bilibili.com/8070827/channel/detail?cid=1890
  
[1.11]【K帧系】第一期 - 模型准备
  https://www.bilibili.com/video/BV1Mt411f7ak
  
[1.20]【舞蹈MMD制作教学】10分钟就能学会制作舞蹈MMD视频 破碎面具
  https://www.bilibili.com/video/BV1JW411X7XE

[2.1] PmxEditor
  https://www.deviantart.com/inochi-pm/art/PmxEditor-vr-0254f-English-Version-v2-0-766313588
  
[2.2] MMD-物理的优化和J点的调整
  https://www.bilibili.com/read/cv252746
  
[2.3] MMD-模型与骨骼的故障以及修复方案
  https://www.bilibili.com/read/cv245790
  
[2.4] 从零开始的改模学习【爱蜜莉雅改模演示】
  https://www.bilibili.com/video/BV1D7411k7a2/?spm_id_from=333.788.videocard.11
  
[2.5] 教程】B站最基础最详细的MMD骨骼教程（基础篇）
  https://www.bilibili.com/video/BV1nJ411Q79s/?spm_id_from=333.788.videocard.3
  
[2.6]【教程】B站最基础最详细的MMD骨骼教程（bug篇）
  https://www.bilibili.com/video/BV1fJ41167Wj/?spm_id_from=333.788.videocard.10
  
[4.1] OpenMMD：没有专业摄像设备也能动作捕捉！K帧动作设计苦手的福音~
  https://www.bilibili.com/read/cv2835857
  
[4.2] OpenMMD represents the OpenPose-Based Deep-Learning
  https://github.com/peterljq/OpenMMD
  
[4.3] OpenMMD_V1.0
  82qe https://pan.baidu.com/s/1L-TxEsgXD3zRHTAM8YQd7w#list/path=%2F&parentPath=%2Fsharelink3156223315-1050112483171531

[4.4] Raise NotImplementedError with equal aspect on 3D axes #13474
  https://github.com/matplotlib/matplotlib/pull/13474

[4.5] A simple yet effective baseline for 3d human pose estimation. In ICCV, 2017
  https://github.com/una-dinosauria/3d-pose-baseline
  https://arxiv.org/pdf/1705.03098.pdf
  
[4.6]
  https://github.com/errno-mmd/3d-pose-baseline-vmd

[4.7]
  https://github.com/errno-mmd/FCRN-DepthPrediction-vmd

[4.8]
  https://github.com/miu200521358/VMD-3d-pose-baseline-multi

[5.1]【一】如何快速使用视频生成MMD运动数据
  https://www.bilibili.com/read/cv3400259/

[6.1] 专业引擎unity比mmd还简单好用
  https://www.bilibili.com/video/BV1Db411e74e?p=2

[6.2] OpenPose Unity Plugin
  https://github.com/CMU-Perceptual-Computing-Lab/openpose_unity_plugin

[7.1]【MMD从入坑到入坟|从零开始】超详细教程！C4D|布料解算|Redshift|Octane|MMD桥|Prem
  https://www.bilibili.com/video/BV1SE411V7oL/?spm_id_from=333.788.videocard.4

[9.1] iwaraR视频批量下载（可下载全站）
  https://www.bilibili.com/video/BV1RE411F7VB
  https://www.lanzous.com/iaj3lcf

[9.2] MMD十周年版以及对应Bridge版
  https://www.lanzous.com/b00zb2hwd

  
## openmmd
PyQt5

```
~/t-project/openpose.git$
./build/examples/openpose/openpose.bin --model_pose COCO --video /home/duanyao/human_action/OpenMMD_V1.0/OpenMMD_v1.0/examples/media/motion_sample_1/Real-person.mov --write_json /home/duanyao/human_action/OpenMMD_V1.0/OpenMMD_v1.0/work_1/kp_json --write_video /home/duanyao/human_action/OpenMMD_V1.0/OpenMMD_v1.0/work_1/kp.mp4 --number_people_max 1
```
产物：
work_1/kp_json/ 每一帧一个文件。
work_1/kp.mp4

```
~/human_action/OpenMMD_V1.0/OpenMMD_v1.0/3d-pose-baseline-vmd$
python src/openpose_3dpose_sandbox_vmd.py --camera_frame --residual --batch_norm --dropout 0.5 --max_norm --evaluateActionWise --use_sh --epochs 200 --load 4874200 --gif_fps 30 --verbose 3 --openpose ../work_1/kp_json --person_idx 1
```
结果所在目录形如 `../work_1/kp_json_3d_20200524_061507_idx01` ， 3D 关键点。

这一步可出现错误：

```
  File "/home/duanyao/opt/venv/v37/lib/python3.7/site-packages/matplotlib/axes/_base.py", line 1281, in set_aspect
    'It is not currently possible to manually set the aspect '
NotImplementedError: It is not currently possible to manually set the aspect on 3D axes
```

这是 matplotlib 3.1.0 以上的兼容性问题。可以降级 matplotlib (3.0.3)。`pip install 'matplotlib<3.1.0'`。

~/human_action/OpenMMD_V1.0/OpenMMD_v1.0/FCRN-DepthPrediction-vmd$
python tensorflow/predict_video.py --model_path tensorflow/data/NYU_FCRN.ckpt --video_path ../examples/media/motion_sample_1/Real-person.mov --baseline_path ../work_1/kp_json_3d_20200524_061507_idx01 --interval 10 --verbose 3

产物：
work_1/kp_json_3d_20200524_061507_idx01/
   movie_depth.gif
   depth/


python applications/pos2vmd_multi.py -v 3 -t ../work_1/kp_json_3d_20200524_061507_idx01 -b born/あにまさ式ミクボーン.csv -c 30 -z 2 -x 15 -m 0 -i 0 -d 0 -a 1 -k 1 -e 0

产物：
work_1/kp_json_3d_20200524_061507_idx01/output_20200524_220236_間引きなし.vmd

这是 MMD 的动作文件。

使用：在 MMD 中，先载入人体模型，再载入上述动作文件，然后即可播放。
