## 参考资料

frp 代码库 https://github.com/fatedier/frp

frp 文档 https://gofrp.org/

## 简介

frpc 进程运行在内网服务器上，frps 进程运行在中介服务器（有外网IP）上，客户端直接访问中介服务器，中介服务器转发到内网服务器上。

## 安装
下载 https://github.com/fatedier/frp/releases/download/v0.50.0/frp_0.50.0_linux_amd64.tar.gz
解压，将 frpc 和 frps 复制到任何目录都，例如 ~/bin

## 配置文件

### ssh 的配置

服务器端：

frps.ini 

```
[common]
bind_port = 7000
# auth token
token = 12345678
```

frps -c /root/.config/frp/frps.ini

客户端：

frpc.ini
```
[common]
server_addr = label-1.ai-parents.cn
server_port = 7000
# auth token
token = 12345678

[ssh-gtrd]
type = tcp
local_ip = 127.0.0.1
local_port = 22
remote_port = 7022
```

其中 `[ssh]` 可以随便取名，但不能重复，即使是在不同的客户端之间

```
frpc -c /root/.config/frp/frpc.ini

autossh -p7022 rd@label-1.ai-parents.cn

autossh -p7022 -CNg -L 5151:127.0.0.1:5151 rd@label-1.ai-parents.cn
```
