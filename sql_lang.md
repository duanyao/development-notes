## 参考资料
[5.1] MySQL select random records using ORDER BY RAND()
  http://www.mysqltutorial.org/select-random-records-database-table.aspx
 
[5.2] How to request a random row in SQL?
  https://stackoverflow.com/questions/19412/how-to-request-a-random-row-in-sql

[6.1] SELECT list is not in GROUP BY clause and contains nonaggregated column … incompatible with sql_mode=only_full_group_by
  https://stackoverflow.com/questions/41887460/select-list-is-not-in-group-by-clause-and-contains-nonaggregated-column-inc

[6.2] Disable ONLY_FULL_GROUP_BY
  https://stackoverflow.com/questions/23921117/disable-only-full-group-by
  
[7.1] Delete from join 用法
  https://blog.csdn.net/xia_xing/article/details/47780091

[7.2] SQL Server-聚焦NOT IN VS NOT EXISTS VS LEFT JOIN...IS NULL性能分析（十八）
  https://www.cnblogs.com/CreateMyself/p/6165543.html

[7.3] SQL Server-聚焦IN VS EXISTS VS JOIN性能分析（十九）
  https://www.cnblogs.com/CreateMyself/p/6165982.html
  
[8.1] Delete Duplicates From a Table in SQL Server
  http://www.sqlservertutorial.net/sql-server-basics/delete-duplicates-sql-server/
  
[8.2]  How to Remove Duplicate Records in SQL
  https://www.databasestar.com/sql-remove-duplicates/
  
[8.3] SQL | Remove Duplicates without Distinct
  https://www.geeksforgeeks.org/sql-remove-duplicates-without-distinct/
  
[9.1] 慎用create table as select,一定要注意默认值的问题
  https://blog.csdn.net/haiross/article/details/17002119

## 字面量表
```
select 1 as a, 2 as b
```
生成表
```
a b
---
1 2
```

如果省略列名，则列名取自值。
```
select 1, 2
```
生成表
```
1 2
---
1 2
```

如果要生成多行的字面量表，可以用 union all 连接（可以只在第一行指定列名）：

```
select 1 as a, 2 as b
union all
  select 1, 2
union all
  select 2, 4
```

生成表
```
a	b
-----
1	2
1	2
2	4
```

某些数据库还支持更直接的语法：

SQL-server:
```
SELECT a, b FROM (VALUES (1, 2), (3, 4), (5, 6), (7, 8), (9, 10) ) AS MyTable(a, b)
```
PostgreSQL:
```
select * from (values (1,7), (2,6), (3,13), (4,12), (5,9) ) x(id, count);
```

还可以给其它的表/结果添加字面量列：将字面量与 from...where 部分联用即可，则字面量作为一列重复出现在每一个结果行中。

```
select
  t.*, 0 as c
from
(
  select 1 as a, 2 as b
  union all
    select 1, 2
  union all
    select 2, 4
) as t
```
生成表
```
a	b	c
---------
1	2	0
1	2	0
2	4	0
```

## left join

left join 时，左右两表根据连接条件匹配的行，配对后出现在结果中，这与 inner join 一样；
左表中无法与右表匹配的行，也出现在结果中，这些行中来自右表的字段值为 null 。

因此，left join 的结果行数大于等于左表行数，也大于等于 inner join 行数。

left join 则是左右反过来。

例子：
```
select * from
  (select 1 as a union all select 3 union all select 5) as t1
left join
  (select 1 as b union all select 1 union all select 5) as t2
  on t1.a = t2.b
```

输出

```
1	1
1	1
3	null
5	5
```

## 随机选择

随机选择若干行：

```
SELECT * FROM tbl ORDER BY RAND() LIMIT 3; # mysql

SELECT column FROM table ORDER BY RANDOM() LIMIT 1 # PostgreSQL

SELECT TOP 1 column FROM table ORDER BY NEWID() # Microsoft SQL Server

SELECT column FROM ( SELECT column FROM table ORDER BY dbms_random.value ) WHERE rownum = 1 # Oracle
```

## group
[6.1]

When MySQL's only_full_group_by mode is turned on, it means that strict ANSI SQL rules will apply when using GROUP BY. With regard to your query, this means that if you GROUP BY of the proof_type column, then you can only select two things:

    the proof_type column, or
    aggregates of any other column


By "aggregates" of other columns, I mean using an aggregate function such as MIN(), MAX(), or AVG() with another column. 

The ANSI SQL extends what is allowed to be selected in GROUP BY by also including columns which are functionally dependent on the column(s) being selected. 

Violating strict grouping doesn't necessarily produce incorrect results. I usually do it when the fields are guaranteed to be identical (e.g. a one-to-many join). And even if it's not guaranteed, selecting an arbitrary row will often be no more incorrect then using an aggregate function or adding a grouping level.

查看是否有 only_full_group_by：
select @@sql_mode;

要取消[6.2]：

SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));

## 根据两个表之间的关系（join 或存在）查询、删除
存在匹配：
```
delete table1 from table1 join table2 on table1.a = table2.b where table2.b = 'c'
```

不存在匹配：
[7.1]

```
DELETE A FROM YSHA A LEFT JOIN YSHB B ON A.code=B.code WHERE B.code is NULL

等同于

DELETE FROM YSHA WHERE NOT EXISTS (SELECT 1 FROM YSHB B WHERE YSHA.code=B.code)
```

[7.2, 7.3] 认为 NOT EXISTS 性能大幅度好于 LEFT JOIN is NULL，NOT EXISTS和NOT IN 差不多。

## 查询结果去重

### 用 group by 去重
```
select a, b, c from t where ... group by a, b
select * from t where ... group by a, b
```

这对于 a,b 相同的多行，会只取其中一行，一般是按返回的顺序取第一行。

选取哪一行似乎无法控制，因为
(1)不能把 order by 放在 group by 之前：
```
select a, b, c from t where ... order by c group by a, b
```
(2)子查询中的 order by 不起作用：
```
select a, b, c from (select * from t order by c) as t_ where ... group by a, b
```

当然，也可以不只取其中一行，而是把去重字段以外的字段聚合起来，例如 count(), sum(), max(), min(), group_concat() 等。

其中 group_concat 为 mysql 特有，用于连接字符串字段，例如：

```
select a, b, group_concat(distinct c separator ' ') as c_list from t where ... group by a, b
```
其中 distinct 和 separator ' ' 可选，默认分隔符为逗号。group_concat 也可以作用于数字等类型，先转化为字符串再连接。null 值被丢弃；如果只有 null 值，返回 null。

```
select a, group_concat(b separator '\n') as b_list from ( select 1 as a, 'f' as b union select 1, 'g' ) as t group by a
```
max() 和 min() 可以用于聚合布尔字段，等效于 or 或 and 聚合。

### 用 union 去重

参考“并集”

### 用 distinct 去重

## 查找重复行，删除重复行
[8.1-8.3]

## 利用查询结果创建表并填充数据
```
create table as select ...
```
但这样的表并不会将默认值、约束等信息也迁移过来 [9.1]。

## 根据时间段聚合

* `group by date(time_column), hour(time_column)`
* date_format (mysql)
  ```
  SELECT count( id ) , date_format( your_date_column, '%Y-%m-%d %H' ) as my_date
  FROM  your_table 
  GROUP BY my_date
  order by your_date_column desc;
  ```

## 并集

UNION 不重复 UNION ALL 可重复。

SELECT column_name(s) FROM table_name1
UNION
SELECT column_name(s) FROM table_name2

SELECT column_name(s) FROM table_name1
UNION ALL
SELECT column_name(s) FROM table_name2

## 字符串操作

### 测量长度和null

length(str) 测量字符串长度。length(null) 返回 null ，而不是 0 。

要对 null 字符串返回 0，可以 `coalesce(length(str), 0)` 。

要判断字符串是否非null且非空，可以 `!!coalesce(length(str), 0)` 。

## null 值操作

isnull(value) 对 null 返回 1 ，否则返回 0 。没有 isnotnull()，可以用 !isnull(value) 或者 not isnull(value) 代替。

coalesce 可以对 null 值返回备选值。`coalesce(v1,v2,v3...)` 返回左起第一个不是 null 的值，如果都是 null 则返回 null。
例如 `coalesce(null, 0)` 和 `coalesce(0, 1)` 返回 0，`coalesce(null, null)` 返回 null 。

对 null 的 boolean 操作往往与其它语言（如 JS）不同，所以最好不用。例如 :

```
select not null -- 结果: null
select 1 and null -- 结果: null
select 1 or null  -- 结果: 1
select 0 and null  -- 结果: 0
select 0 or null  -- 结果: null
```
max、min 中的 null 似乎被忽略：
```
select min(t.a), max(t.a) from (select 1 as a union select null) as t; -- 结果: 1 1
```
## 日期、时间操作

```
select datediff('2010-10-08 18:23:13', '2010-09-21 21:40:36') -- 结果：17 (bigint)
select timestampdiff(second,'2007-12-30 12:01:01','2007-12-31 10:02:00'); -- 79259 (bigint)
select adddate('2021-03-15', 7)  -- 结果： '2021-03-22' (char)
select adddate(date('2021-03-15'), -7)  -- 结果： '2021-03-22' (date)
```

## limit, offset

例子：
```
select * from
(
 select 1 as a union select 2 union select 3 union select 4
) as t
order by t.a desc
limit 2 offset 1
```
输出：
```
a
--
3
2
```

`limit n offset m` 是个整体，offset m 可以省略，但必须在 limit n 之后，不能独立使用。

limit 和 offset 应当与 order by 搭配，否则输出部分无法预测。

MySQL 的实现中，limit 和 offset 的参数必须是整数字面量，不能是一般表达式，如 1+2, coalesce(null, 0) 等。不清楚 SQL 标准和其它数据库是如何实现的。
