## 参考资料
[5] Does The HTTP Response Stream Need Error Event Handlers In Node.js? 
  https://www.bennadel.com/blog/2823-does-the-http-response-stream-need-error-event-handlers-in-node-js.htm
[6] stream readable pipe
  http://nodejs.cn/api/stream.html#stream_readable_pipe_destination_options
[7] Error handling with node.js streams
  https://stackoverflow.com/questions/21771220/error-handling-with-node-js-streams
  
[8.1] http client should emit error on content length mismatch #6300
https://github.com/nodejs/node-v0.x-archive/issues/6300

[8.2] truncated S3 read stream #257 
https://github.com/aws/aws-sdk-js/issues/257

[11] Socket setTimeout behaviour not working as expected for HTTP requests
  https://github.com/nodejs/node/issues/5899
  

[31] Node.Js TLS(SSL) HTTPS双向验证
http://blog.csdn.net/marujunyy/article/details/8477854

[32] SSL/TLS在NODEJS中的应用
http://www.cnblogs.com/LevinJ/archive/2012/05/16/2503586.html



## 事件流程和错误处理
本节的内容主要来自对 node.js 5.4.1 和 7.4.0 的测试结果。参考 MyNodeJS\httpErrorHandling。

### request(options: RequestOptions, callback?: (res: IncomingMessage) => void): ClientRequest
* 正常流程：ClientRequest 上发送完数据、调用 end() 后，ClientRequest 产生  finish, close 事件。res 上接收完数据后，产生 end, close 事件。
  注意此处 close 表示底层 socket 的关闭，要等请求和响应都结束后才会发生。

* 如果 options 中有非法参数，可能同步抛出异常，如 url 不合规范。否则，返回 ClientRequest。

* 如果在能够连接服务器前发生网络错误（网络不通、DNS错误、拒绝连接），则在返回的 ClientRequest 上产生 error 和 close 事件，且callback不会调用。

* 如果自己在 callback 调用之前调用 ClientRequest.abort() 则 ClientRequest 上产生 close, error 事件，且callback不会调用。

* 如果在连接服务器后、接收完响头应前，服务器过早关闭TCP连接，则在返回的 ClientRequest 上产生 error 和 close 事件，error.code 可能是 'ECONNRESET'，且callback不会调用。

* 如果在连接服务器后、接收完响头应前、发送完 ClientRequest 前，主动关闭连接（如超时），则在返回的 ClientRequest 上产生 error 和 close 事件，error.code 可能是 'ECONNRESET'，且callback不会调用。

* 如果在接收完响头后，服务器过早关闭连接，则返回的 ClientRequest 上产生 close 事件（注意没有error和finish事件），接着 res 产生 aborted（不一定有）, end, close 事件。
  res.socket 上产生 finish, end, close 事件。如果在接收完响头后，ClientRequest 还未发完，就主动销毁连接（如发生了 timeout），结果相同（加上timeout事件）。
  aborted 事件不一定会发生，可能与 TCP 连接关闭的方式有关，如正常关闭或 Reset 关闭（这点没有测试）。
  对于有 Content-length 头的响应，客户代码可以检测到响应体长度不匹配；没有 Content-length 头、采用 chunked 编码的响应体，客户代码未必能检测到问题[8.1-8.2]。

* 如果在接收完响头后，网络中断（如拔客户端或服务器网线），而 ClientRequest 还未发完，则 ClientRequest 上产生 finish, error 和 close 事件，error.code 可能是 'ECONNRESET'；
注意，error 事件可能延迟发生，在这之前仍可向 ClientRequest 写入数据，但无法到达服务器。
接着，在 callback 的 res 上产生 aborted, end, close 事件。

* 如果在接收完响头后，网络中断，则连接可能会长时间维持，因为 request() 默认不会超时。如果给 ClientRequest setTimeout，并在 res.socket.on('timeout') 或者 ClientRequest.socket.on('timeout') 时销毁连接（.socket.destroy()），则 ClientRequest 会产生 timeout, close 事件，res 会产生 aborted, end, close 事件。

* 如果接收响应体的途中，服务器过早关闭TCP连接，造成只收到部分响应体，则不会报告任何错误。
综上所述，正确的错误处理包括：
0. 给 ClientRequest 设置读写超时，监听 socket 事件，监听 socket 的 timeout 事件，并在超时时销毁连接。
1. 捕获 request() 的同步异常。
2. 监听返回的 ClientRequest 的 error 事件。aborted 事件似乎不会发生，不过也可以监听。finish, close 可以不用管。错误发生后，可停止发送数据。
3. 当 callback 调用后，监听 res 的 aborted 事件。error 事件似乎不会发生，不过也可以监听。
4. 有 Content-length 头的响应，最好测试其与实际响应体长度是否相等，没有 Content-length 头的则没有好的办法。服务器或中间节点可能过早关闭连接，这在高负载环境下是个常见问题，而 node.js 不会检测这种情况（其他语言和库通常会报错），需要客户代码处理[8.1-8.2]。

### http.Server 的 'request' 事件处理器：onRequest(req: IncomingMessage, res: ServerResponse)

* 在连接后、'request' 事件前，如果服务器或客户端中断连接，则在 socket 上产生 close 事件。

* 'request' 事件一般在服务器接收完请求头后立即触发（POST请求可能推迟到接收到body的部分数据后？）。node.js 的 http.Server 实现是支持全双工的，即可以边接收请求体，边发送响应体。

* 正常流程：req 上接收完后产生 end 事件（注意没有 close 事件！）， res 发送完后产生 finish 事件（注意没有 close 事件，因为 ServerResponse 的 close 事件意味着过早中断TCP连接）。

* 如果在'request' 事件后、接收完请求体前，或者在接收完请求体后、发送完响应体前，客户端过早断开连接（如ctl+C，发送RST重置），或者网络中断（如拔客户端或服务器网线），或者服务器主动销毁 socket（例如监听 socket 的 timeout 事件并销毁连接），
则在 req 上产生 aborted 和 close 事件（注意没有 end 事件，与一般 Readable 不同），在 res 上产生 close 事件（意味着过早中断TCP连接，注意没有 error 事件）。

正常情况下，服务器方面的req和res永远不会有 error 事件。但是，罕见的情况下，也确实观察到了 res 上的 error 事件，例如

events.js:160
      throw er; // Unhandled 'error' event
      ^

Error: write after end
    at ServerResponse.write (_http_outgoing.js:450:15)
    at IncomingMessage.ondata (_stream_readable.js:555:20)
    at emitOne (events.js:101:20)
    at IncomingMessage.emit (events.js:188:7)
    at IncomingMessage.Readable.read (_stream_readable.js:381:10)
    at flow (_stream_readable.js:761:34)
    at ServerResponse.<anonymous> (_stream_readable.js:623:7)
    at emitNone (events.js:86:13)
    at ServerResponse.emit (events.js:185:7)
    at Socket.ondrain (_http_common.js:213:44)

触发条件：uploadProxy，移动版百度首页->视频

综上所述，正确的错误处理包括：
0. 给 server.timeout /socket 和 req.socket 设置读写超时或保留默认值，在 server 上监听 connection 事件，在 socket 上监听 timeout 并销毁连接。
  注意仅有 req.socket 的读写超时是不够的，因为在接收完请求头之前，是不会产生 request 事件的。
1. 监听 req 的 aborted 事件，或者 res 的 close 事件。
2. 监听 req 和 res 的 error 事件。

### 超时

#### TCP socket 超时机制
node.js 的 TCP socket 可以有3种超时机制：

* TCP 连接超时。node.js socket 模块没有提供直接的 API 来改变这个超时，而采用操作系统的默认机制。
  当操作系统的 TCP socket 发生连接超时时，node.js socket 会发出 'error' 事件（code是ETIMEDOUT），而不是 'timeout' 事件。
  客户代码可以通过设置 timer 并监听 'connect' 事件，来自行实现小于操作系统默认值的TCP 连接超时。node.js 的 http(s).request() API 就是这样做的。

* 读写超时：超过一定时间没有读写活动，则认为超时了。这可以通过 socket.setTimeout(timeout[, callback]) 来设置。
  注意这仅仅会导致产生 timeout 事件，并不会自动关闭连接，需要客户代码主动关闭。timeout 事件发生后，如果客户代码不关闭连接，实际上仍可继续正常收发数据。

* TCP keep alive 超时：超过一定时间没有读写活动，则操作系统发出探测包，检测对端是否能响应，如果不能则产生错误，中断连接。
  这可以通过 socket.setKeepAlive([enable][, initialDelay]) 来设置。
  但是，node.js 不完全能控制探测的总耗时，而默认值又很大，所以对实时性要求高的程序并不实用。详见 network-tcp.txt。

默认情况下，node.js 的 TCP socket 未设置超时。

#### http 超时机制
http 模块基本上复用 socket 对象的超时机制。对于读写超时，http 模块也不会主动关闭 socket，除了在 socket 上，还会在 ClientRequest、IncomingMessage、ServerResponse 上产生 timeout 事件。
timeout 事件发生后，如果客户代码不关闭连接，实际上仍可继续正常收发数据。

客户端方面，http.request() 和 http.ClientRequest 有3个设置超时的方法(默认都没有设置，采用操作系统默认)：

* request(options: { timeout?: number, ... })
  设置 TCP 连接超时。注意这不影响底层的 TCP 连接超时。如果这个时间先达到了，在返回的 ClientRequest 对象上产生 'timeout' 事件，但会正常进行后续操作；客户代码可以调用 ClientRequest.abort() 来放弃后续操作，这也会产生一个 'abort' 事件（详细行为见下面关于.abort()的介绍）。
  如果底层的 TCP 连接超时先达到，则产生code是ETIMEDOUT'error' 事件，并放弃后续操作。

* ClientRequest.setTimeout(timeout: number, callback?: ()=>void)
  设置读写超时。等效于调用对应的 socket.setTimeout()。这与 request() 中设置的 TCP 连接超时互不影响。
  当超时发生时，ClientRequest 对象以及对应的 socket 对象上产生 'timeout' 事件。同样，读写超时不会自动中断连接，需要客户代码自行关闭，可以调用 ClientRequest.abort()。

* ClientRequest.setSocketKeepAlive([enable][, initialDelay])
  TCP keep alive 超时，等效于调用对应的 socket.setKeepAlive()。


在服务器方面，http.createServer() 创建的 Server 具有 120 秒的默认读写超时，通过 server.timeout 属性可以获取、设置这个值（单位毫秒）。但设置后只对之后创建的连接生效。此外，也可以在 server 的 connection 事件或者 request 事件中设置读写超时。

#### http 服务器的超时 bug
node.js 4.4.4- 和 5.10- 存在此问题。当http服务器的一个连接上只接收数据时，会被错误地视为不活动，造成误触发超时事件[11]。

#### DNS 超时
值得注意的是，DNS 的查询时间并不在 socket/http 超时机制的考虑范围之内。

### 主动中断事务
#### socket.destroy(err?: Error) 和 socket.end()

调用 socket.destroy() 会完全关闭 TCP 连接。如果有 err 参数，会在 socket 上产生 error 事件。
在 TCP 层面，则发送了 FIN 包结束连接，同时也不再接收数据。
如果对端是 node.js，则 socket 上可能会得到 error 事件，code 是 'ECONNRESET'，不过这并不意味着发送了 TCP RST 包。对端也可能不会得到错误，只是正常关闭。

socket.end() 是半关 TCP 连接（发送FIN包，但还接收数据），并且一般不会在 socket 上产生 error 事件。当然，两者都会在 socket 上产生 close 事件。

#### 客户端 http.request()
* http.ClientRequest.abort() 将丢弃还未送到 socket 的数据，并调用 socket.destroy()（如果已经连接）。

如果还未完成 TCP 连接，会在 ClientRequest 上产生 abort, error(socket hung up/ECONNRESET), close 事件。【测试方法：request()返回后立即abort()】
如果TCP已连接，已发送请求，还未收到响应（request()的回调尚未发生），同上。【测试方法：ClientRequest finish 事件中调用 abort()】
如果已经收到响应对象 response，这会在 ClientRequest 上产生 abort, close 事件，在 response 上产生 aborted, end, close 事件。【测试方法：request() 回调中 abort()】

socket 上则产生 close 事件。

http.ClientRequest.destroy() 目前与 abort() 等效，但这个方法似乎没有文档。

* response.destroy() 导致 ClientRequest 上产生 close 事件，在 response 上产生 aborted, end, close 事件。

#### 服务器端
* 如果在接收完请求、发送响应头之前决定中断事务，一般可以用特定的http错误代码予以响应。

* 如果在接收完请求体之前决定中断事务，例如发现 POST/PUT 的请求体过大，就需要中断相关的TCP 连接了。可以直接调用 req.socket.destroy(err)，也可以调用 req.destroy(err)，两者是等效的。
  req.socket.destroy(err) 和 req.destroy(err) 都在 socket 上触发 error 事件（如果有参数），在 req 上产生 aborted 和 close 事件，在 res 上产生 close 事件。
socket.end() 一般也有类似的效果。

* 如果在发送请求体的过程中决定中断事务，同上。


## HTTPS
[31-32]

tls.createServer([options][, secureConnectionListener])

requestCert <boolean> If true the server will request a certificate from clients that connect and attempt to verify that certificate. Defaults to false.
rejectUnauthorized <boolean> If true the server will reject any connection which is not authorized with the list of supplied CAs. This option only has an effect if requestCert is true. Defaults to false

## Cookie（客户端）
node.js 内置的 http client 并不存储和自动发送 cookie。
tough-cookie 库可以用于管理客户端的 cookie，request 库利用了它来存储和自动发送 cookie。

## 浏览器模拟
### zombie
Insanely fast, headless full-stack testing

### phanos
用 PhantomJs browser 来模拟。

## （附录）流（stream）的错误处理
### class Writable 的状态转换和事件
Writable 是抽象类。
客户代码或实现代码都可以调用 .end() 方法来结束，让其进入 ended 状态，并产生 finish 事件。
ended 状态下继续写入会导致 error 事件。
重复调用 .end() 不是错误，后继的调用没有效果。

子类要实现 _write(chunk, encoding, callback) 方法。如果在 callback 中返回 Error，则会导致 Writable 产生 error 事件。
注意这种错误不会自动让其进入 ended 状态，如果在出错时不主动调用 .end()，则流仍然是正常可写的。

Writable 没有实现 close 事件，其子类可能实现。

在具体实现中，有的 Writable 在出错时不产生 finish 事件，而是产生 close 事件，例如 ServerResponse 。

### class Readable 的状态转换和事件
Readable 是抽象类。
子类要实现 _read() 方法，有数据后调用 .push(data) 方法推入缓冲区，.push(null) 表示流结束。

流结束时，无论正常还是异常结束，Readable 产生 end 事件，之后就不会再读到数据了。
但是，一个显著的例外是 http.Server 的入站请求（IncomingMessage），它在连接提前断开的情况下，没有 end 事件，有 aborted 和 close 事件。
不过，http.request() 产生的出站响应（IncomingMessage）在同样的情况下则有 aborted, end, close 事件。

end 以外的事件，如 error, aborted 都要由 Readable 的子类产生，并且没有自动的副作用（如导致流结束）。
因此，客户代码在 Readable 的子类的实例上调用 .emit('error') ，无法保证能正常工作。

如果子类愿意，可以产生 error, aborted 事件但是不结束流，或者推迟结束流，甚至可以产生多个这类事件，虽然这不符合逻辑。

如果通过 data 事件来读取流，则 pause() 会暂停 data 事件，而 resume() 会恢复。
零长度的流不产生 data 事件，且 pause() 对其无效，即不会暂停 end 事件的发送 （https://github.com/nodejs/node/issues/14976）。

### class PassThrough 和 class Transform
继承关系：
Writable 抽象
Readable 抽象
  Duplex 抽象
    Transform 抽象
      PassThrough

### 流管道上的错误处理
流管道：
readable.pipe(writable)
* readable 上发生 end 事件时，它会调用 writable.end() [5]，writable.end()会导致 finish 事件。
* readable 上发生 aborted 事件时，会导致 writable 的 finish 事件。
* readable 上发生 error 事件时，
* readable 上发生 close 事件时，

### Readable#pipe(writable) 的实现细节
* 一个 Readable 上可以有多个下游的 Writable。有数据时，写入每个下游的 Writable；同时发出 data 事件（也就是说pipe和data事件可以并行）。
* 监听自己的 end 事件，发生的时候，调用 writable.end() 。注意这不会导致 unpipe。这个行为可以被关闭。
* 监听 writable 的 close 和 error 事件，发生的时候调用 Writable#unpipe(writable) ；
  此处监听 error 事件不算作客户代码的 error 监听器，不会阻止程序崩溃。
* 调用 pipe() 和 unpipe() 的时候，都会在 writable 上产生相应的事件。
* Readable 自己的 aborted, error 事件对管道没有直接影响。虽然具体的 Readable 实现中，aborted, error 可能同时会结束流，导致 end 事件。

综上，pipe() 主要依赖源的 end 事件和目标的 error 事件。多数情况下这不是问题：Readable 一般总是产生一个 end 事件，不论是否异常；
Writable 在结束后（无论是否异常），继续写入也会自动产生 error 事件。

但也有例外，如 http.Server 的入站请求（IncomingMessage），它在连接提前断开的情况下，没有 end 事件，有 aborted 和 close 事件。
这种情况下，需要额外监听源的 aborted 事件，然后调用 writable.end()。
http.Server 的入站响应（ServerResponse）没有 error 事件，异常时有 close 事件。
