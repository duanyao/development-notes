## 参考资料
[1.1] Flask
https://flask.net.cn/

[1.2] 操作请求数据
https://flask.net.cn/quickstart.html#id14

[1.5] flask中gunicorn的使用
https://www.cnblogs.com/zongfa/p/12614459.html

[2.1] FastAPI is a modern, fast (high-performance), web framework
https://fastapi.tiangolo.com/

[2.3] Uvicorn is an ASGI web server implementation for Python
https://www.uvicorn.org/

[2.4] FastAPI vs Flask - The Complete Guide
https://christophergs.com/python/2021/06/16/python-flask-fastapi/

[2.5] Architecture Flask vs FastAPI
https://stackoverflow.com/questions/62976648/architecture-flask-vs-fastapi

[3.1] Automated graceful reload of gunicorn in production
https://serverfault.com/questions/823546/automated-graceful-reload-of-gunicorn-in-production

[3.2] Signal Handling
https://docs.gunicorn.org/en/stable/signals.html

[3.3] 如何优雅的退出/关闭/重启gunicorn进程
https://cloud.tencent.com/developer/article/1493641

[3.4] Graceful restart of application servers/ gunicorn workers
https://singhvishal0304.medium.com/graceful-restart-of-application-servers-gunicorn-workers-8be195522836

[3.5] an exhaustive list of settings for Gunicorn
https://docs.gunicorn.org/en/stable/settings.html

## flask

### 安装
```
pip install flask
```

### 简单使用

foo_srv.py:

```
from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return 'hello world'

def main():
    app.run(port=9100, debug=True)

if __name__ == '__main__':
    main()
```

### 故障排除
#### werkzeug 版本不兼容
Flask 2.1.3 ～ 2.2.2 没有指定其依赖的 werkzeug 的版本，但实际上只兼容于 werkzeug 2.x，不兼容 werkzeug 3.x 。直接安装 Flask 2.1.3 后自动安装 werkzeug 2.0.x ，会出错：
```
ImportError: cannot import name 'url_quote' from 'werkzeug.urls'
```

### 请求参数

提取url路径参数：

```
@app.route('/user/<username>')
def show_user_profile(username): # str 类型
    # show the user profile for that user
    return 'User %s' % escape(username)

@app.route('/post/<int:post_id>') # 也可以是 float
def show_post(post_id):
    # show the post with the given id, the id is an integer
    return 'Post %d' % post_id

@app.route('/path/<path:subpath>')
def show_subpath(subpath):
    # show the subpath after /path/
    return 'Subpath %s' % escape(subpath)
```

提取url搜索参数：

```
@app.route('/login', methods=['POST', 'GET'])
def login():
    error = None
    if request.method == 'POST':
        if valid_login(request.args.get['username'],
                       request.args.get['password']):
            return log_the_user_in(request.args.get['username'])
        else:
            error = 'Invalid username/password'
    # the code below is executed if the request method
    # was GET or the credentials were invalid
    return render_template('login.html', error=error)
```

提取表单参数：

```
@app.route('/login', methods=['POST', 'GET'])
def login():
    error = None
    if request.method == 'POST':
        if valid_login(request.form['username'],
                       request.form['password']):
            return log_the_user_in(request.form['username'])
        else:
            error = 'Invalid username/password'
    # the code below is executed if the request method
    # was GET or the credentials were invalid
    return render_template('login.html', error=error)
```

### 请求的方法

```
from flask import request

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        return do_the_login()
    else:
        return show_the_login_form()
```

### 请求对象 request

导入 flask.request 对象后（ `from flask import request`），可以在请求处理函数中通过 request 对象访问请求的属性。


```
@app.route('/login', methods=['POST', 'GET'])
def login():
    error = None
    if request.method == 'POST':
        if valid_login(request.form['username'],
                       request.form['password']):
            return log_the_user_in(request.form['username'])
        else:
            error = 'Invalid username/password'
    # the code below is executed if the request method
    # was GET or the credentials were invalid
    return render_template('login.html', error=error)

当 form 属性中不存在这个键时会发生什么？会引发一个 KeyError 。 如果你不像捕捉一个标准错误一样捕捉 KeyError ，那么会显示一个 HTTP 400 Bad Request 错误页面。因此，多数情况下你不必处理这个问题。

要操作 URL （如 ?key=value ）中提交的参数可以使用 args 属性:

searchword = request.args.get('key', '')
```
request.args 的类型是 werkzeug.datastructures.ImmutableMultiDict ，值的类型不会自动转换。

request 的类型为 werkzeug.local.LocalProxy，并非真正的全局对象，没有线程安全问题。

### 访问请求头

request.headers 包含了请求头。这是个类似 dict 的对象（ werkzeug.datastructures.EnvironHeaders ），但允许包含重复的键（头）。
request.headers.get(key) 返回第一个匹配的值，request.headers.getlist(key) 返回所有匹配的值。
request.headers.get(key) 大小写不敏感。但如果迭代 headers，输出的头的每个单词的首字母会确保大写，例如 `Content-Length`。
request.headers 似乎无法处理一些自定义的头，如 `X-Received-Time`，无法通过 get(key) 访问，自定义头需要通过转换为普通 dict 或 list 来访问，例如： `dict(request.headers.items()).get('X-Received-Time')` 。

### 访问请求体
flask. 对象：
```
 get_json(force=False, silent=False, cache=True)

    Parse data as JSON.
    If the mimetype does not indicate JSON (application/json, see is_json()), this returns None.

    If parsing fails, on_json_loading_failed() is called and its return value is used as the return value. The default implementation raises BadRequest
    
 get_data(cache=True, as_text=False, parse_form_data=False)

    This reads the buffered incoming data from the client into one bytestring. By default this is cached but that behavior can be changed by setting cache to False.
    
 property form

    The form parameters. By default an ImmutableMultiDict is returned from this function. This can be changed by setting parameter_storage_class to a different type. This might be necessary if the order of the form data is important.

    Please keep in mind that file uploads will not end up here, but instead in the files attribute.

 property files

    MultiDict object containing all uploaded files. Each key in files is the name from the <input type="file" name="">. Each value in files is a Werkzeug FileStorage object.

    It basically behaves like a standard file object you know from Python, with the difference that it also has a save() function that can store the file on the filesystem.

    Note that files will only contain data if the request method was POST, PUT or PATCH and the <form> that posted to the request had enctype="multipart/form-data". It will be empty otherwise.

    See the MultiDict / FileStorage documentation for more details about the used data structure.
```



### 重定向和错误

使用 redirect() 函数可以重定向。使用 abort() 可以 更早退出请求，并返回错误代码:

```
from flask import abort, redirect, url_for

@app.route('/')
def index():
    return redirect(url_for('login'))

@app.route('/login')
def login():
    abort(401)
    this_is_never_executed()
```

### 响应体

请求处理函数的返回值的类型决定了响应体的内容和mime类型：

* 返回字符串，则直接作为响应体，mime类型为 'text/html' 。
* 返回字典dict，则以 json 方式序列化，mime类型为 'application/json' 。这是通过调用 flask.json.jsonify() 方法产生一个 response 对象。
* 返回元组，则解释为 (response, status) 、 (response, headers) 或者 (response, status, headers)

jsonify() 可以更灵活地处理json序列化：

```
@app.route("/users")
def users_api():
    users = get_all_users()
    return jsonify([user.to_json() for user in users])
```

### 日志

```
app.logger.debug('A value for debugging')
app.logger.warning('A warning occurred (%d apples)', 42)
app.logger.error('An error occurred')
```

当然，直接用 python 的 logging 模块也可以。

### 更快的 json 实现
flask 默认使用 python 标准库的 json 模块来编解码 json 数据，但它并不快。
orjson（ https://github.com/ijl/orjson ）是个更快的 json 实现。
```
from flask import Flask
from flask_orjson import OrjsonProvider # pip install flask-orjson

app = Flask(__name__)
app.json = OrjsonProvider(app)
```

flask 更换 json 实现的功能从 2.2.0 开始支持。

### JSON 配置
flask 默认使用 python 标准库的 json 模块来编码 json 数据，并且默认设置了 `ensure_ascii = True, sort_keys = True` 。
要改变这种行为，对于 2.2 以前的版本可以：
```
app = Flask(__name__)
app.config["JSON_AS_ASCII"] = False
app.config["JSON_SORT_KEYS"] = False
```
2.3 以后以上方式被删除了，设置后不会生效。
2.2 以后的版本可以：
```
from flask.json.provider import DefaultJSONProvider

app = Flask(__name__)
json = DefaultJSONProvider(app)
json.ensure_ascii = False
json.sort_keys = False
app.json = json
```

非默认的 JSONProvider 理论上也应该支持 ensure_ascii 和 sort_keys 属性，但有的并非如此。
OrjsonProvider (flask-orjson 2.0.0) 可以这样设置：

```
from flask_orjson import OrjsonProvider

app = Flask(__name__)
json = OrjsonProvider(app)
json.option = orjson.OPT_SORT_KEYS | orjson.OPT_NAIVE_UTC
app.json = json
```
注意 OrjsonProvider 和 orjson 默认并不排序字典的键。

### flask 版本变更和兼容性要点
* 2.2.0 支持 JSONProvider
* 2.3.0 放弃支持 Python 3.7，要求 Werkzeug>=2.3.0, 删除了配置项 JSON_AS_ASCII, JSON_SORT_KEYS, JSONIFY_MIMETYPE, and JSONIFY_PRETTYPRINT_REGULAR
* 2.3.3 支持 Python 3.12，要求 Werkzeug >= 2.3.7
* 3.0.0 要求 Werkzeug >= 3.0.0

## gunicorn

### 安装
pip install gunicorn

### 简单使用
监听 127.0.0.1:8000，1个worker。
```
gunicorn foo_srv:app
```
监听所有 IPv4 地址，8080 端口
```
gunicorn -w 4 -b 0.0.0.0:8080 foo_srv:app --timeout 60
```

`-w 4` 表示使用4个子进程。
`0.0.0.0:8080` 是绑定的本机地址和端口。
`--timeout 60` 表示gunicorn主进程最多允许worker进程处理一个请求时用时60秒，超过60秒后会认为worker已经失去响应，会重启这个worker，并对客户端返回超时错误。
foo_srv 是定义 web 服务的 python 模块名，此模块可以使用 flask，如前面 flask 的例子所示。如果此模块在当前目录下，则 gunicorn 可直接找到它。app 是该模块定义的一个属性，如前面的
```
app = Flask(__name__)
```

如果 foo_srv 以及其依赖不在默认的模块搜索路径中，可以指定 `--pythonpath` 来添加额外的搜索路径（多个路径用逗号分开）：

```
gunicorn -w 4 -b 0.0.0.0:8080 foo_srv:app --timeout 60 --pythonpath /dir/of/foo_srv/
```

### 并行处理模式
gunicorn 有多种并行处理模式，包括进程、线程、协程、异步IO，或者前者的混合等。
gunicorn 术语中，“worker” 是多义的，可以指工作进程，也可以指并行处理模式中用来处理请求的实体，可以对应进程、线程、协程等之一。
使用举例：
```
gunicorn --worker-class=sync --workers=5 app:app
gunicorn --worker-class=gthread --workers=5 --threads=10 app:app
gunicorn --worker-class=gevent --workers=5 --worker-connections=1000 app:app
```
--workers 参数是指工作进程数，而 --worker-class 参数是指并行处理模式。
`--worker-class=sync --workers=5` 表示使用 sync (同步) 并行处理模式，有5个工作进程，且每个工作进程只有1个线程，因此可以并行处理 5 个请求。sync 并行处理模式是默认的。
`--worker-class=gthread --workers=5 --threads=10` 表示使用 gthread（实际上就是 python Thread）并行处理模式，且每个工作进程有10个线程，有5个工作进程，因此总共有 5 * 10 == 50 个线程，因此可以并行处理 50 个请求。
`--worker-class=gevent --workers=5 --worker-connections=1000` 表示使用 gevent（一种协程的实现）并行处理模式，且每个工作进程有1000个协程（默认值），有5个工作进程，因此可以并行处理 5000 个请求。

### gunicorn gevent 并行处理模式
gevent 是个第三方协程库，用以取代标准库的 threading 时，可以处理更大量的并发。
gevent 采用协作式多任务，因此基本上不需要使用标准库的锁机制（但 threading.Event 有应用场景）。
gevent 可以通过给标准库打运行时补丁（Monkey patching）的方式，替代标准 threading 的功能。gunicorn --worker-class=gevent 使用了这种方式。详见 python_coroutine_gevent.md 。
但应当注意，gevent 与 concurrent.futures.ProcessPoolExecutor 不兼容。

### 配置文件
gunicorn 可以接受一个 python 脚本作为配置文件，此配置文件可以实现所有的命令行参数的功能，且有一些独有的功能。配置文件的例子：

`gunicorn_config.py`：
```
bind = "127.0.0.1:8000"
workers = 2
threads = 2
worker_connections = 4
pythonpath = 'path1,path2'
wsgi_app = "flask_test:app"
#worker_class = "gevent"
worker_class = "gthread"
```
运行：
```
gunicorn -c ./gunicorn_config.py
```
配置文件中的变量名与命令行参数名不是100%对应的，可以查看文档[3.5]。命令行参数可覆盖等效的配置文件参数。

### 钩子函数
gunicorn 配置文件中可以写一些钩子函数，在一些事件发生时会被 gunicorn 调用[3.5]：

```
on_starting(server): before the master process is initialized
when_ready(server): after the server is started
pre_fork(server, worker): before a worker is forked
```

### gunicorn 重加载代码

对 gunicorn 主进程发送 HUP 信号，就可以导致它重新加载应用代码[3.1]。同时，现有worker进程会停止，被新的worker进程替代，但主进程不变[3.2]。

```
kill -HUP $MAINPID
```

主进程可以用 `pstree -ap|grep gunicorn` 查看[3.3]。

如果用systemd，可以在配置文件中加上：

```
ExecReload=/bin/kill -HUP $MAINPID
```
然后用命令：
```
systemctl reload gunicorn
```

注意，不要用符号链接来区分不同版本的应用代码，因为 gunicorn 只在启动时解析应用代码的符号链接一次，kill -HUP 时不会重新解析符号链接，而是使用原有结果。
如果有这种需求，建议用硬重启（重启主进程）。

### gunicorn 版本变更和兼容性要点
* 22.0.0 - 2024-04-17 Python 3.7 ~ 3.12
* 21.0.0 - 2023-07-17 Python ~ 3.11

## FastAPI

FastAPI 与 flask 类似，但使用了 async 机制。

```
from fastapi import FastAPI


app = FastAPI(debug=False)


@app.get("/")
async def root():
    return {"message": "hello"}
```

### FastAPI + uvicorn

```
import uvicorn
from fastapi import FastAPI
from time import sleep
app = FastAPI()

@app.get('/')
async def root():
    print('Sleeping for 10')
    sleep(10)
    print('Awake')
    return {'message': 'hello'}

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)
```
