# ffmpeg 命令行工具综述

## 参考资料
[1.1] ffmpeg -h full
[1.2] ffmpeg 命令行文档
  https://ffmpeg.org/ffmpeg-all.html
[1.3] FFmpeg Bitstream Filters Documentation 
  https://ffmpeg.org/ffmpeg-bitstream-filters.html

[2.1]
https://superuser.com/questions/1056599/ffmpeg-re-encode-a-video-keeping-settings-similar
[2.2] A wish for ffmpeg to be able to reference input properties
https://trac.ffmpeg.org/ticket/5081

[3.1] Seeking (cut)
http://trac.ffmpeg.org/wiki/Seeking
[3.2] 
https://trac.ffmpeg.org/ticket/7306

[4.1]
https://trac.ffmpeg.org/wiki/Concatenate

[4.2]
https://stackoverflow.com/questions/7333232/how-to-concatenate-two-mp4-files-using-ffmpeg

[4.3] 使用ffmpeg合并视频文件的三种方法
http://itindex.net/detail/52379-ffmpeg-合并-视频

[4.4] FFMpeg无损合并视频的多种方法
https://blog.csdn.net/doublefi123/article/details/47276739

[4.5] ffmpeg - video conversion with multiple audio tracks
https://video.stackexchange.com/questions/21734/ffmpeg-video-conversion-with-multiple-audio-tracks

[5.1] What do ALSA devices like “hw:0,0” mean? How do I figure out which to use?
https://superuser.com/questions/53957/what-do-alsa-devices-like-hw0-0-mean-how-do-i-figure-out-which-to-use

[6.1] adding silent audio in ffmpeg
https://stackoverflow.com/questions/12368151/adding-silent-audio-in-ffmpeg

[7.1] moreutils
https://joeyh.name/code/moreutils/

[8.1] AAC ADTS格式分析
https://blog.csdn.net/bsplover/article/details/7426476

[9.1] 
  https://stackoverflow.com/questions/8672809/use-ffmpeg-to-add-text-subtitles
  
[9.2] FFMPEG An Intermediate Guide/subtitle options
  https://en.wikibooks.org/wiki/FFMPEG_An_Intermediate_Guide/subtitle_options

[10.1]
https://www.quora.com/What-is-the-difference-between-an-I-Frame-and-a-Keyframe-in-video-encoding

[10.2] 
https://forum.videohelp.com/threads/342173-how-many-b-frames-is-optimal-for-h264-encoding

[11.1] how generate dts pts from elementary stream
http://ramugedia.com/how-generate-dts-pts-from-elementary-stream

[11.2] DTS - Decode Time Stamp
http://www.bretl.com/mpeghtml/DTS.HTM

[11.3] PTS - Presentation Time Stamp
http://www.bretl.com/mpeghtml/PTS.HTM

[11.4] understanding DTS PTS 
http://www.ffmpeg-archive.org/understanding-DTS-PTS-td940270.html

[12.1] X264 Settings
http://www.chaneru.com/Roku/HLS/X264_Settings.htm

[12.2] Advanced configuration options for libx264
http://help.encoding.com/knowledge-base/article/advanced-configuration-options-for-the-libx264-video-codec/

[12.3] ffmpeg 和 x264的参数对照
http://www.cnblogs.com/soief/archive/2013/12/12/3471465.html

[13.1] fps Convert the video to specified constant frame rate by duplicating or dropping frames as necessary
https://ffmpeg.org/ffmpeg-filters.html#fps-1

[14.1] Adding metadata to a video or audio file with FFmpeg
https://write.corbpie.com/adding-metadata-to-a-video-or-audio-file-with-ffmpeg/

[15.1] ffmpeg drawtext 文本位置及时间戳
https://blog.csdn.net/toopoo/article/details/105603154

[15.2] 《ffmpeg basics》中文版 -- 10.在视频上添加文本
https://blog.csdn.net/qq_34305316/article/details/103937279?depth_1-utm_source=distribute.pc_relevant.none-task-blog-OPENSEARCH-4&utm_source=distribute.pc_relevant.none-task-blog-OPENSEARCH-4

[15.3] Can FFMPEG extract images from a video with timestamps?
https://superuser.com/questions/575854/can-ffmpeg-extract-images-from-a-video-with-timestamps/575935#575935

[15.4] Frame Number Overlay With FFmpeg
https://stackoverflow.com/questions/15364861/frame-number-overlay-with-ffmpeg

[16.1] How to extract all key frames from a video clip?
https://superuser.com/questions/669716/how-to-extract-all-key-frames-from-a-video-clip

## 命令的一般形式

ffmpeg 命令行工具接收一个或多个输入文件（此处“文件”是抽象意义上的），生成一个或多个输出文件。输入文件可以是普通文件、视音频采集设备、网络流媒体、标准输入等，输出可以是普通文件、网络流媒体、标准输出等。
在输入和输出之间可以进行各种转换，包括解码、过滤、编码等。

ffmpeg 命令行的一些特殊形式不符合上述描述，例如可以列出视音频采集设备。

命令的一般形式[1.2]：

  `ffmpeg [global_options] [[infile1 options] -i infile1] [[infile2 options] -i infile2]... [[outfile1 options] outfile1] [[outfile2 options] outfile2]... `

实际的例子：

  `ffmpeg -loglevel debug -y -f gdigrab -framerate 30 -i desktop -i input2.mp3 -c:v libx264 out.mkv`

ffmpeg 命令行的选项以 `-` 开头，例如 `-loglevel`、`-framerate`，后面可能跟一个参数，例如 `debug`，`30`，也可能不带参数，例如 `-y`。
-i 选项后面的参数是输入文件，一般是 url、文件名的形式，也有用名字表示的，如 `-i desktop` 表示计算机屏幕。输入文件可以有多个。
输出文件不需要特定的选项，不能识别为输入或者选项的参数就会被当做输出文件，如 `out.mkv`。输出文件可以有多个。

全局选项（global_options）是位置无关的，但一般写在最开始的位置。
其它很多选项是位置相关的，很多选项可以出现多次（如 `-i` ，可以指定多个输入）。
位置相关选项应用于其后的第一个输入文件或输出文件上面，这样的一组选项内部的顺序则是没有关系的。
上面例子中，`-loglevel debug -y` 是全局选项，`-c:v libx264` 是输出选项，`-f gdigrab -framerate 30` 是第一个输入文件（“desktop”）的输入选项。

过滤器的一般语法：

```
ffmpeg -i INPUT -vf "split [main][tmp]; [tmp] crop=iw:ih/2:0:0, vflip [flip]; [main][flip] overlay=0:H/2" OUTPUT
```
`-vf` 表示 video filter，多个并行的过滤器管线表达式用 “;” 分开，一个过滤器管线的多个过滤器用逗号分开；过滤器的参数语法为 `filter_name=key1=value1:key2=value2...` 或者 `filter_name=value1:value2...`。

## 怎样查命令行参数的资料

执行命令 `ffmpeg -h full` 可以得到详细的帮助信息，包括每个选项的属性（全局/文件相关）和简单解释，但是不包括例子和总体概念。
这个帮助很长，你可以将其写入文件（ `ffmpeg -h full > ffmpeg-help-full.txt` ）方便查阅。
或者用 `ffmpeg -h full | grep <关键字> -B10 -A5` 来搜索。`-B10 -A5` 的意思是显示前10行和后5行。

专项帮助选项（未包含在 `ffmpeg -h full` 中）

```
-formats            show available formats
-muxers             show available muxers
-demuxers           show available demuxers
-devices            show available devices
-codecs             show available codecs
-decoders           show available decoders
-encoders           show available encoders
-bsfs               show available bit stream filters
-protocols          show available protocols
-filters            show available filters
-pix_fmts           show available pixel formats
-layouts            show standard channel layouts
-sample_fmts        show available audio sample formats
-colors             show available color names
-sources device     list sources of the input device
-sinks device       list sinks of the output device
-hwaccels           show available HW acceleration methods
```

某个 muxer、encoder 的专项帮助：

`ffmpeg -h muxer=mpegts`

`ffmpeg -h encoder=libx264`

### ffprobe 的帮助信息
ffprobe -h

## 一些特殊的输入输出文件

* `pipe:` ：用来表示标准输入输出，放在输入文件的位置就是标准输入，否则就是标准输出。
* `/dev/null` 和 `nul` ：用来表示被丢弃的输出，写在输出文件的位置上。`/dev/null` 是 POSIX 的，nul 是 Windows 的。这主要用来在测试中避免输出端的 IO 产生影响。

例子：
```
cat input.avi | ffmpeg -i pipe: -c:v libx264 out.mkv
ffmpeg -i input.avi -c:v libx264 -f mkv pipe: > out.mkv
ffmpeg -i input.avi -c:v libx264 -f mkv nul
```

## 输入输出文件格式、音视频编码格式的指定和猜测

不需要指定（实际上也几乎不能指定）ffmpeg 的输入文件格式（及其音视频编码格式、帧率等），它会自动探测。
自动探测相关的参数有 -probeszie, -analyzeduration, -fpsprobesize 等，可查阅专门章节。

输出文件格式可以指定，也可以让 ffmpeg 猜测，猜测的依据是文件名后缀或者协议。

例如

`ffmpeg -i foo.avi foo.mp4` 

输入的格式和编码格式将通过 foo.avi 文件探测出来；
根据输出文件的后缀 mp4 决定输出的容器格式为 mp4 ，而视频、音频编码器则默认为 h.264 和 aac ，分辨率、帧率保持原样，其它压缩参数采用默认值。

如果输出文件没有扩展名/协议名，就需要至少指定容器格式，这时音视频编码格式仍可默认：

`ffmpeg -i foo.avi -f mpegts pipe:`

`-f <format>` 是位置相关的选项，用在输出位置时，指定容器格式。mpegts 容器（扩展名应为 ts）的默认视频编码是 mpeg2。

如果要进一步指定输出的音视频编码格式，可以分别使用 `-c:a <audio format>` `-c:v <video format>` 。

通常一种音视频编码格式拥有大量的可调参数，如视频的帧率、分辨率、颜色格式、GOP、质量因数；音频的声道布局、采样率、位数等。
如果一些参数未指定，ffmpeg 先尽量使用与输入文件相同的参数，如帧率、分辨率、颜色格式、声道布局、采样率、位数等；另一些参数无法或不方便从输入文件获得，则采用默认值，
例如码率以及控制码率的参数，如 x264 的 `-crf`、GOP 等[2.1]。有提议增加机制让 ffmpeg 转码时可以引用输入文件的编码参数[2.2]。

如果输入不是普通文件或者流媒体，而是音视频采集设备，则一般要用 `-f` 和 `-i` 来分别指定设备类型和设备名称，例如：

`ffmpeg -f gdigrab -framerate 20 -i desktop desk.mp4`

gdigrab 是 windows 系统的桌面采集设备，desktop 指桌面。设备类型和设备名可以分别用 ffmpeg -devices 和 ffmpeg -sources 来列出。

音视频采集详见相关章节。

## 单位
默认时间单位是微秒，默认数据量单位是字节，默认传输率单位是位/秒（bps），默认频率（包括帧率、采样率等）单位是 Hz。
不想使用默认单位时，可以在数值后面加上单位。
数据量单位可以是K（KiB）、M（MiB），传输率单位可以是K（Kbps）、M（Mbps）。

## 日志

ffmpeg 的日志（打印在控制台上的文字）是输出到标准错误的，这是因为标准输出被留作音视频数据的输出（用 `pipe:` 协议）。
所以，如果想把日志导出到文件，应该 `ffmpeg ... 2> ffmpeg.log` 。要用管道分析日志，应当 `ffmpeg ... |& grep ...`，`|&` 是 bash 操作符（win10可以用WSL，msys 貌似不行）。

`-report` 选项可以直接将日志写入当前目录下的文件，名字形如 `program-YYYYMMDD-HHMMSS.log` 。

### 日志级别
ffmpeg 可以输出不同详细程度的日志，从简略到详细分别是：

‘quiet, -8’
‘panic, 0’
‘fatal, 8’
‘error, 16’
‘warning, 24’
‘info, 32’
‘verbose, 40’
‘debug, 48’
‘trace, 56’

举例：
```
ffmpeg -loglevel debug -i foo.avi foo.mp4
ffmpeg -loglevel 48 -i foo.avi foo.mp4
```

不指定 `-loglevel`，则默认值是 `info` 。

ffmpeg 不同模块的日志详细程度差别较大，有的模块不产生什么有用的日志（例如 gdigrab ）。

### 日志时间戳
ffmpeg 日志不标注时间。利用 linux 的 ts 工具可以给每一行日志加上时间戳，便于进行性能相关的分析。例如：

`ffmpeg args... |& ts "%m:%.S"`

`|&` （bash操作符）将 ffmpeg 的标准错误输出到 ts 程序，ts 将每一行加上时间戳输出。"%m:%.S" 是时间格式，%m 是分钟，%.S 
是带小数的秒。

在 Win10 上，可以在 WSL 系统里安装和运行 ts 程序。ts 属于 moreutils 软件包[7.1]。

## 退出码和信号
与大多数程序一样，ffmpeg 正常退出时退出码为 0，出现错误时退出码不等于0。
SIGINT (ctrl+C)，SIGTERM 等可以导致 ffmpeg 退出，linux 上退出码为 255。

## 音频采集/录音（linux）

Linux 上可以采用两种输入方式：ALSA 和 PulseAudio ，分别用 `-f alsa` 和 `-f pulse` 指定。
两者既可以采集话筒声音，也可以采集声卡声音。

例子：
`ffmpeg -f pulse -i default` 或者 `ffmpeg -f alsa -i default`。
输入设备名不知道时，可以用 `default`。不指定输出文件时，将显示输入设备的属性后退出，大概是这样：

```
Guessed Channel Layout for Input Stream #0.0 : stereo
Input #0, alsa, from 'default':
  Duration: N/A, start: 1525760340.502230, bitrate: 1536 kb/s
    Stream #0:0: Audio: pcm_s16le, 48000 Hz, stereo, s16, 1536 kb/s
```
录制声音：
`ffmpeg -y -f alsa -i default system.wav`

linux 上采集设备可以用 `ffmpeg -sources` 列出，结果大概是这样（包含音视频采集设备，此处节选alsa和pulse部分）：

```
Auto-detected sources for alsa:
* default [Playback/recording through the PulseAudio sound server]
  null [Discard all samples (playback) or generate zero samples (capture)]
  sysdefault:CARD=PCH [Default Audio Device]
  front:CARD=PCH,DEV=0 [Front speakers]
  dmix:CARD=PCH,DEV=0 [Direct sample mixing device]
  dsnoop:CARD=PCH,DEV=0 [Direct sample snooping device]
  hw:CARD=PCH,DEV=0 [Direct hardware device without any conversions]
  plughw:CARD=PCH,DEV=0 [Hardware device with all software conversions]
Auto-detected sources for pulse:
  bluez_sink.00_11_C2_08_F6_79.headset_head_unit.monitor [Monitor of G18]
  bluez_source.00_11_C2_08_F6_79.headset_head_unit [G18]
  alsa_output.pci-0000_00_1b.0.analog-stereo.monitor [Monitor of 内置音频 模拟立体声]
* alsa_input.pci-0000_00_1b.0.analog-stereo [内置音频 模拟立体声]

```

方括号里是注释，设备名就是 `default`，`front:CARD=PCH,DEV=0` 这样的了，冒号及其之后的部分有时可以省略，例如 `front`。alsa 设备还可以用 `aplay -L` 命令列出[5.1]。
对于 alsa，`default` 是默认的声音设备，这可能是默认的话筒，也可能是默认的声卡。在联想 V470 笔记本上测试时，`default` 和 `sysdefault` 都是话筒。
带有 speakers 字样的是扬声器，可以用来记录系统的声音。注意，这必须在扬声器工作的情况下才能记录，如果插上耳机就可能记录不到；并且，系统音量也影响记录到的音量。

对于 pulse 设备，带有 sink、output 字样的是扬声器，带有 source、input 字样的是话筒。记录扬声器（alsa_output）时，系统音量不影响记录的音量。

alsa 不能记录蓝牙耳机，pulse 则可以（bluez 开头的设备）；alsa 记录系统声音也有较大的杂音，pulse 则没有这个问题。所以最好使用 pulse 设备。

前面带星号的是当前在工作的设备（不一定准确）。

除了采用采集设备默认的采样率和声道数，还可以用以下位置相关选项指定: 
  -sample_rate(-ar)   默认 48000，单位 Hz，也可以写作 48K
  -channels(-ac)      默认 2

例如：

`ffmpeg -y -channels 1 -ar 44.1k -f pulse -i "alsa_output.pci-0000_00_1b.0.analog-stereo.monitor" pulse.wav`

注意，ffmpeg 网站上提供的 linux 静态编译版没有打开 libpulse codec，无法录制声卡声音，显示“Unknown input format: 'pulse'”。可以试试发行版自带的版本。


## 音频采集（Windows）
Windows 上 `ffmpeg -sources` 并不能列出采集设备（目前没有实现）。

用 `ffmpeg -list_devices true -f dshow -i dummy` 可以列出 direct show 采集设备，例如：

```
[dshow @ 0277ea00] DirectShow video devices (some may be both video and audio devices)
[dshow @ 0277ea00]  "Lenovo EasyCamera"
[dshow @ 0277ea00]     Alternative name "@device_pnp_\\?\usb#vid_5986&pid_0364&mi_00#7&d810bd5&0&0000#{65e8773d-8f56-11d0-a3b9-00a0c9223196}\global"
[dshow @ 0277ea00] DirectShow audio devices
[dshow @ 0277ea00]  "楹﹀厠椋?(Realtek High Definition Audio)"[dshow @ 0277ea00]     Alternative name "@device_cm_{33D9A762-90C8-11D0-BD43-00A0C911CE86}\wave_{3C3F3329-9581-4366-882B-2A06C17E48A4}"
```

“DirectShow video devices”下面缩进列出的是视频设备，“DirectShow audio devices”下面是音频采集
但是，并不容易判断声音采集设备是话筒还是声卡。

设备名是缩进的第一层，例如 `Lenovo EasyCamera` 和 `楹﹀厠椋?(Realtek High Definition Audio)`（应为 `"麦克风 (Realtek High Definition Audio)"`）。
如果有中文会乱码，原因是 ffmpeg 以 UTF-8 输出中文，但 cmd.exe 控制台不支持，可以 2> 重定向到文件再查看。

缩进的第二层是别名“Alternative name”，如 `@device_cm_{33D9A762-90C8-11D0-BD43-00A0C911CE86}\wave_{3C3F3329-9581-4366-882B-2A06C17E48A4}` ，
可以代替设备名使用。注意，Alternative name 的 UUID 可能在系统重启后改变。

不写输出，指定设备名（别名），可以看到设备参数：

`ffmpeg -f dshow -i audio="@device_cm_{33D9A762-90C8-11D0-BD43-00A0C911CE86}\wave_{3C3F3329-9581-4366-882B-2A06C17E48A4}"`

## 视频采集

### 桌面采集(linux)

例子：

`ffmpeg -video_size 1366x768 -framerate 10 -f x11grab -draw_mouse 0 -i :0.0 desk.mp4`

linux 桌面视频采集的设备类型是 `x11grab` 用 `-f` 指定，设备名是 `:0`, `:1` 或者 `:0.0` 的形式，用 `-i` 指定。
`-draw_mouse` 指定是否截取鼠标指针，0 和 1 分别表示不截取/截取。默认值是1。

设备名实际上是显示设备编号，并不是固定的，多个桌面（ctrl + alt + fn切换）可能不同。可以通过 `DISPLAY` 环境变量取得一个进程（不限于GUI程序）所在的显示设备。
设备名不存在默认值，不能写 `default`。

`-video_size` 指定截取区域，如果小于屏幕尺寸则是截取一部分，并不是截取全屏后缩放到这个尺寸（要缩放得使用 scale 过滤器）。默认值是 640x480。
注意，不存在直接指定截取全屏的方法，所以你需要事先知道屏幕分辨率。
但如果指定的区域超过了屏幕大小，则 ffmpeg x11grab 会在出错信息中包含正确的屏幕分辨率，这可以加以利用。例如：

`ffmpeg -video_size 10000x10000 -f x11grab -i :0.0`
输出
`[x11grab @ 0x556a05373e00] Capture area 10000x10000 at position 0.0 outside the screen size 1366x768` ，
通过解析文本可以得到屏幕分辨率。

`-framerate` 指定采集帧率，单位 Hz。默认值是 29.97。Linux 上 x11grab 帧率几乎没什么限制，指定 100 也能跑，CPU 占用 18% 的样子（i5 2.5GHz，1366x768），但一般没必要超过 60。

以上选项都是位置相关的，前面说过，要写在 `-i` 选项之前才有效。

单纯测试桌面采集的命令：

`ffmpeg -y -framerate 60 -video_size 1366x768 -f x11grab -i :0.0 -f rawvideo -c:v copy /dev/null`

单帧截屏：

```
ffmpeg -video_size 1920x1080 -f x11grab -i :0.0 -frames:v 1 `date +%F_%H%M%S`.png
```

### 桌面采集（windows）
例子：

`ffmpeg -f gdigrab -framerate 30 -draw_mouse 0 -i desktop desk.mp4`

桌面视频采集的设备类型是 `gdigrab` 用 `-f` 指定。设备名用 `-i` 指定，`desktop` 截取整个桌面，`title=window_title` 截取具有指定标题的窗口。
`-framerate` 指定帧率，单位 Hz。默认值是 29.97。`gdigrab` 能截取的最大帧率比较有限，最多能稳定在三十多一点，且跑在主线程，是阻塞模式。
`-draw_mouse` 指定是否截取鼠标指针，0 和 1 分别表示不截取/截取。默认值是1。

### 帧率控制
对于桌面采集（不论linux还是windows）的帧率，有两个事实：
（1）实际帧率不一定等于 `-framerate`，在采集过程中也可能不断变化，这主要是因为 CPU 负载高时采集帧率会下降（gdigrab比较明显），且采集和视频编码都会阻塞主线程，因此后者对实际帧率的影响也很大。
（2）ffmpeg 默认通过 "probe" 过程来估算帧率，而不是直接采用 `-framerate` 的值。估算帧率需要相当数量的帧，这会导致下一阶段（如视频编码）的延迟。

所以，桌面采集的帧率控制方式一般是可变帧率而不是固定帧率（除非帧率很低），且最好避免让 ffmpeg 估算帧率。例子：

`ffmpeg -y -loglevel debug -probesize 32 -vsync passthrough -framerate 100 -video_size 1366x768 -f x11grab -i :0.0 -c:v libx264 desk.ts`

`-probesize` 单位是字节，默认值是 5000000，最小允许 32。32 肯定小于一帧的大小，这样就相当于关闭了帧率探测。这时 ffmpeg 会警告：
`[x11grab @ 0x55b4b12f8fc0] Stream #0: not enough frames to estimate rate; consider increasing probesize`
这是没有危害的。

`-vsync passthrough` 的意思是直接采用上游（也就是采集设备）的时间戳。

## 摄像头采集

### 摄像头bug的应对：rtsp+hevc 流的头几帧没有时间戳

有的摄像头（如部分海康威视的型号）存在bug，即 rtsp+hevc 流的头几帧可能没有设置时间戳，导致 -c copy 模式无法录像（转码则无此问题）。例如：

```
ffmpeg -y -rtsp_transport tcp -i rtsp://admin:xhm123456@ddns-35.ai-bama.cn:10061 -c copy c.ts

[mpegts @ 0x5f2f280] Timestamps are unset in a packet for stream 0. This is deprecated and will stop working in the future. Fix your code to set the timestamps properly
[mpegts @ 0x5f2f280] first pts and dts value must be set                                                                                                                  
av_interleaved_write_frame(): Invalid data found when processing input
```
用 ffprobe 检查：
```
ffprobe -show_packets -rtsp_transport tcp -i rtsp://admin:xhm123456@ddns-35.ai-bama.cn:10061 > a.txt
```
可发现确实第一帧没有pts和dts：
```
[PACKET]
codec_type=video
stream_index=0
pts=N/A
pts_time=N/A
dts=N/A
dts_time=N/A
duration=11250
duration_time=0.125000
size=749424
pos=N/A
flags=K_
[/PACKET]
```

绕开此问题的一个方法是，设置一个较短的 -ss 参数，跳过开头的几帧：
```
ffmpeg -y -ss 0.2 -rtsp_transport tcp -i rtsp://admin:xhm123456@ddns-35.ai-bama.cn:10061 -c copy b.ts
```

## 音频的属性及其转换
一个音频流的属性有很多：
* 声道布局（-channels, channel_layout）：单声道(mono, 1)，立体声（stereo, 2），2.1声道，5.1声道等。
  ffmpeg 输出参数 -ac 用于变更、设定输出文件的声道数。例如 `ffmpeg -i in.mp4 -ac 2 out.mp4`。
  输入参数 -channels 可用于指定音频采集设备、PCM 输入的声道数。
  `ffmpeg -layouts` 显示可用的声道布局。

* 采样率：单位 Hz。不是所有的采样率都支持，有的编码格式、容器格式有限制。常见的采样率是 8、16、11.025、22.05、44.1 KHz。
  ffmpeg 输出参数 -ar 用于变更、设定输出文件的采样率。例如 `ffmpeg -i in.mp4 -ar 44100 out.mp4`。
  输入参数 -sample_rate 用于指定音频采集设备、PCM 输入的采样率。

* 采样格式：格式包括（1）整数还是浮点数、是否有符号等。（2）每个声音采样用的位数。
  `-sample_fmt` 变更、设定输出文件的采样格式，如 `ffmpeg -i … -c:a flac -sample_fmt s16 output.flac`。
  `ffmpeg -sample_fmts` 或者 `ffmpeg -formats | grep PCM` 列出可用的格式。常用的格式有 s16, s16p 等，
  'p' 的意思是不同声道的采样点不是交错存储，而是一段一段存储的。例如对双声道，s16 是 left1,right1,left2,right2，s16p 是 left1,left2,right1,right2。
  每种编码格式支持的采样格式是有限的，例如 flac 支持 s16, s32, 但不支持 u8, s16p, s32p, s64 等；aac 仅支持 fltp。
  如果输入的采样格式不在输出编码格式支持的范围内，且没有指定输出采样格式，ffmpeg 可以自动转换。

* 码率：`-b:a` 或 `-ab` 指定音频码率。只有压缩格式才需要。单位bps，或者加上后缀`K,M`等。

* 编码格式：`-c:a` 或 `-codec:a` 或 `acodec` 。


## 任务控制
### 暂停/恢复
* POSIX
  发送 `SIGSTOP` 信号暂停，`SIGCONT` 信号继续。在控制台里可以分别用 `ctrl+z` 和 `fg` 或 `kill -s SIGCONT <PID>`。

* Windows
  控制台里按 pause/break 键暂停，按回车恢复。但这个功能似乎不是 ffmpeg 实现的，而是 Windows 控制台的功能，pause/break 只是阻塞了 ffmpeg 的标准输出，让 ffmpeg 无法前进；但是，如果 ffmpeg 设置在较低的 loglevel 上因而没有标准输出时，则 pause/break 不起作用。

  Process Explorer 工具可以挂起（suspend）一个进程，适用于 ffmpeg。windows 自带的“任务管理器-资源监视器”也可以。

## fflags
`-fflags arg1+arg2+arg3`

  -fflags            <flags>      ED...... (default flush_packets+autobsf)
     flush_packets                E....... reduce the latency by flushing out packets immediately
     ignidx                       .D...... ignore index
     genpts                       .D...... generate pts
     nofillin                     .D...... do not fill in missing values that can be exactly calculated
     noparse                      .D...... disable AVParsers, this needs nofillin too
     igndts                       .D...... ignore dts
     discardcorrupt               .D...... discard corrupted frames
     sortdts                      .D...... try to interleave outputted packets by dts
     keepside                     .D...... don't merge side data
     fastseek                     .D...... fast but inaccurate seeks
     latm                         E....... enable RTP MP4A-LATM payload
     nobuffer                     .D...... reduce the latency introduced by optional buffering
     bitexact                     E....... do not write random/volatile data
     shortest                     E....... stop muxing with the shortest stream
     autobsf                      E....... add needed bsfs automatically (delays header until each stream's first packet is written)

## 时间戳的概念
[11.1-]
dts: decode timestamp 解码时间，用来指示一个帧何时解码。
pts: present timestamp 显示时间，用来指示一个帧何时显示出来。

dts 和 dts 一般储存在容器层面。前者不一定存在，后者通常一定存在。
如果没有 B 帧，原则上只需要 pts 不需要 dts，因为帧的解码顺序和显示顺序总是相同。
在容器文件中，数据一般（总是？）按照 dts 升序存储，但 pts 不一定是升序的，因为视频存在 B 帧，应当将其依赖的后面的帧靠前存储。

有些情况下，确实只有 pts 而没有 dts 。例如 ffmpeg 生成的 mpegts + h.264 流，如果没有 B 帧（-tune zerolatency），则没有 dts；ffprobe 这时将 dts 视为与 pts 相同。

dts 对一些内存有限的嵌入式解码器是有用的，可以决定解码的时机[11.4]。
ffmpeg 有些时候依赖 dts 进行 seeking，因为这不需要真正解码。

一个文件中 pts 一般从0或接近0的正数开始，但dts可能从接近0的负值开始，因为 dts 要小于对应帧的 pts。
还有一种情况，初始时间戳是较小的负值，播放器的处理方式是跳过时间戳为负的包，从0位置开始播放，但不是所有的播放器都支持。mp4 容器通过 edit list 支持这个特性。

由于 dts 的单调性，可以用来检测不可靠的流媒体传输中的时间戳回滚（rollover）、溢出(overflow)、间断等问题。例如，ffplay、vlc 播放 mpegts 流的时候，对于dts的回滚、溢出，会给后续时间戳加上一个偏移量并继续播放。溢出是一种特殊的回滚，幅度是固定的。

因为有的容器时间戳位数较少，溢出是容易发生的。例如 mpegts 中是 33 位无符号，单位是 1/90KHz，所以 26 小时就会溢出。

## 网络传输流媒体的时间戳问题

网络传输媒体流的时候，可能出现的时间戳问题（`*`表示媒体流的时间戳，`/`表示自然时间，'停表' 是指暂停播放器的时钟）：

正常：

```
    */
   */
  */
 */
*/
```

中间丢失：

```
    */
    /
   /
 */
*/
```

解决策略（1）：暂停播放+停表，恢复时调整时间戳。
解决策略（2）：（伪）暂停播放，不停表，恢复时不调整时间戳。
解决策略（3）：暂停播放+重置时间戳和播放器时钟。副作用：播放器可能遇到时间戳错乱。
解决策略（4）：暂停播放+停表，恢复时加快播放。中断时间介于1-2倍网络中断时间，延迟可逐渐恢复。

中间推迟到达：
```
    */
   */
  */
  *
*/
```
解决策略（1）：暂停播放+停表，恢复时加快播放。
解决策略（2）：（伪）暂停播放+不停表，恢复时跳过过时的帧。
解决策略（3）：暂停播放+重置时间戳和播放器时钟。副作用：播放器可能遇到时间戳错乱。

时间戳回滚：

```

   *   / #2 
  *   /
 *   /
*   /    #1
  */     #0
 */
*/
```

出现原因：在1#处，视频流源ffmpeg重启。这不会自然出现，当需要在特定点插入关键帧（h.264 IDR帧）时，可能有必要重启ffmpeg进程。
另一个可能是时间戳发生了溢出，这在 mpegts 中是可能发生的。

解决策略（1）：暂停播放+停表，认为 #1到#2之间的数据属于过去，不进行播放，直到收到#2才恢复播放。Exoplayer 实现了这个策略。
解决策略（2）：检测到时间戳回滚，将#1之后的时间戳向未来平移，恢复时间戳的单调性。认为 #1到#2之间的数据属于过去，不进行播放，直到收到#2才恢复播放。
  ffplay、vlc 实现了这个策略。

## 帧率控制

ffmpeg 转换视频或解码视频的时候，如果不指定输出帧率，则：
* 如果源视频是固定帧率的，则输出视频采用原视频的帧率。
* 如果源视频是可变帧率的，则默认将输出帧率调整到 25fps，类似应用了 `-vf fps=fps=25` 。
* 如果源视频是可变帧率的，且指定了 `-vsync vfr`（vfr==可变帧率），则输出也是可变帧率的，且保持原有的帧率。

如果要尽量让输出视频的帧数、帧率、时间戳与源视频一致，除了指定 `-vf fps=fps=...` 等于源视频帧率，还可以加上 `-vsync passthrough` 或者 `-copyts` 。`-vsync` 是全局选项。

经过测试，使用 libx264 或者 rawvideo 编码，不指定输出帧率，仅指定 `-vsync passthrough` 时，可以保持原视频的帧率。

有的编码器不支持小于 1 的 fps ，例如 vp9 、av1 等；如果要求的输出帧率小于 1，会显示错误 `g_timebase.den out of range` 。

### 参数 -r

`-r N` 强制指定帧率，而不是让 ffmpeg 采用或猜测视频流的帧率。
单位是帧/秒。
它可以是输入或输出参数。

用途：
* 作为输入参数时，一般用于没有帧率/时间戳的输入，例如一系列图片；或者用于帧率/时间戳错误的输入，例如某些网络摄像头的h.264流。
  这个参数不会导致对帧重新采样或重复、丢弃帧，只是调整元数据中的帧率或帧的时间戳。

* 作为输出参数时，指定输出的帧率。如果输出的帧率比输入高，则会插入重复帧来补足。

例子，将输入的 h.264 原始流设定为 8fps，并按 0.5fps 抽帧、转换为 jpg 文件：

`ffmpeg -r 8 -i input.264 -r 0.5 %d.jpg`

作为输入参数也可以用于 ffplay，例如以 8fps 的速度播放h.264 原始流：

`ffplay -r 8 input.264`

### filter fps

`-filter:v fps` 或者 `-vf fps` 指定输出帧率。
例如：
```
ffmpeg -i input.mp4 -vf fps=fps=film:round=near output.mp4
ffmpeg -i input.mp4 -vf fps=fps=25 output.mp4
ffmpeg -i input.mp4 -vf fps=fps=source_fps output.mp4
```
film 表示 24fps ，source_fps 表示沿用源文件的帧率。不过，目前 ffmpeg 似乎不支持 source_fps ，原因不明。

## 抽帧

用类似以下的做法抽帧时 `ffmpeg -i input.mp4 -r 0.5 %d.jpg`，如果用 `帧率*视频时长` 来计算抽帧的帧数，可能会有偏差。这是因为 ffmpeg 计算帧的时间戳经常会有误差，特别在`帧率*视频时长`为整数时，最后一帧的时间戳在边界附近，即可能输出，也可能不输出，总帧数就不确定。在`帧率*视频时长`不为整数时，更准确的总帧数公式为 `抽帧帧数=floor(帧率*视频时长) + 1`。

抽帧的帧路径的指定方式为：

* `%d.jpg`：当前目录下，生成 1.jpg、2.jpg 等。
* `img/%5d.jpg`：img/目录下，生成 00001.jpg、00002.jpg、... 00010.jpg等。
* `-frame_pts true %5d.jpg` ，当前目录下，生成 00000.jpg、00002.jpg、... 00004.jpg等，数字表示 pts ，但经过测试，pts 并不正确。

抽帧时，如果不指定输出帧率（`-r`），有时候输出默认采用 25fps，而非将原视频的帧1:1抽出，例如源视频采用可变帧率；这个行为与其它视频编码器是类似的。
对可变帧率源视频，加上 `-vsync vfr` 或者 `-vsync passthrough` 可能可以保证将原视频的帧1:1抽出；passthrough 对某些视频可能报错 `Invalid pts (0) <= last (0)`。

可以将视频指定时间点（段）的内容抽帧，例如：

```
ffmpeg -ss 00:23:00 -i Mononoke.Hime.mkv -frames:v 1 -q:v 3 out1.jpg
ffmpeg -ss 1200 -t 3 -i input.mp4 -r 2 -q:v 3 out-%d.jpg
ffmpeg -i input.mp4 -frames:v 1 out-%d.jpg
```

可以用两种方法指定范围和输出的数量：开始时间（`-ss 00:23:00`）加上帧数（`-frames:v 1`），或者开始时间加上时长（`-t 3`）以及帧率（`-r 2`，输出参数），可以指定截图的帧率或帧数。
仅指定`-frames:v 1`不指定时间，可以截取视频的第一帧。

如果想仅抽出关键帧，可以写成[16.1]：

```
ffmpeg -y -threads 1 -skip_frame nokey -i input.mp4 -vsync passthrough -vf fps=fps=0.1 %5d.jpg
ffmpeg -y -threads 1 -skip_frame nointra -i input.mp4 -vsync passthrough %5d.jpg
```

nokey 和 nointra 分别跳过非 key frame 和非 i-frame。在 h.265 编码的视频中，两者似乎没有什么差别。`-vsync passthrough` 是有必要的，否则会输出很多重复帧，以保持默认帧率（例如30fps）。
`-threads 1` 可能是有用的，有人表示如果使用多线程，输出文件的顺序可能是乱的[16.1]。抽出关键帧后，还可进一步用过滤器，例如用 `-vf fps=fps=` 改变帧率。

-skip_frame 是输入/输出参数，抽帧时应为输入参数，写在 -i 前面。如果写成输出参数，则不会生效，而是警告：
```
[out#0/image2 @ 0x55f63d286200] Codec AVOption skip_frame (Allow frame skipping) has not been used for any stream. The most likely reason is either wrong type (e.g. a video option with no video streams) or that it is a private option of some encoder which was not actually used for any stream.
```

-skip_frame 但似乎只影响抽帧。如果同时还有其它的非抽帧输出，则不受影响，例如：

```
ffmpeg -vsync passthrough -skip_frame nokey -ss 0 -t 600 -i a.mp4 -c copy -f segment -segment_list playlist.m3u8 -segment_list_flags +live -segment_time 60 segs/%03d.mp4 -vf fps=fps=0.1 frames/%5d.jpg
```

## 时间戳控制

`-fflags +genpts`
`-fflags +igndts`
`-vsync parameter`  全局参数。
`-fps_mode[:stream_specifier] parameter (output,per-stream)` 每个输出或输出流的参数。
  vsync 和 fps_mode 功能类似，都是控制帧率模式，参数范围是： auto|passthrough|vfr|cfr|drop 。
  passthrough 也可以写成 0。
  默认是 auto，根据 muxer 的能力选择 vfr 或 cfr 模式。
  passthrough 和 vfr 保留原视频的时间戳和帧率。其区别在于 vfr 会防止2帧有相同的时间戳（通过丢弃其中一帧）但 passthrough 不做处理。
  -vsync 已经废弃（ffmpeg 6.0）。如果两者同时出现，则 fps_mode 生效。
  fps_mode 是输出参数。
`-copyts` 复制时间戳，全局参数。

## 流的丢弃、复制
`-an` 丢弃声音，`-vn` 丢弃视频。两者都是位置相关的，要作用于输出文件。

`ffmpeg -i input.mp4 -an output.mp4`

如果容器格式仅支持音频或视频，则不支持的流也会被丢弃。例如

`ffmpeg -i input.mp4 output.mp3`

`copy` 作为编码器名时，意味着复制流，不重新编码（但时间戳仍可重编），例如。

`ffmpeg -i input.mp4 -c:v copy -c:a copy output.mkv`

当音频视频都用 copy 时，可简写为 `-c copy`。

## 在一个输出文件里包括多个音轨（流）
ffmpeg 默认在一个输出文件中保留最多一个视频流、一个音频流、一个字幕流，如果有多余的会被丢弃。
如果要超过这个限制，应该用 `-map` 来选择流，例如：

`ffmpeg -i input.mp4 -map 0:v -map 0:a -c:v libx265 -c:a copy output.mp4`

其中 `-map 0:v` 选中输入文件0（即input.mp4）的所有视频流，`-map 0:a` 选中输入文件0的所有音频流。
`-c:v` 和 `-c:a` 是应用于所有的视频流和音频流的。更复杂的例子见“复杂的流选择”一节。

## 将多个文件并联式合成/替换流
将一个视频文件加上音频：

`ffmpeg -i input1.mp4 -i input2.mp3 -shortest out.mp4`

如果 input1.mp4 中没有音频，那么很简单，两者的音视频合成在一起。如果 input1.mp4 中有音频流，则输出中保留它，而忽略 input2.mp3 。这是因为：ffmpeg 默认在输出中保留一个视频流和一个音频流；有多个输入文件时，靠前的优先。因此 input1.mp4 的音频流会被优先选中并保转换到 out.mp4 中。如果要优先使用 input2.mp3 的音频流，应该把它放在前面：

`ffmpeg -i input2.mp3 -i input1.mp4 -shortest out.mp4`

`-shortest` 是输出文件的选项，表示其总时长是各个输入流当中最短的那个。
如果不写`-shortest` ，则总时长是各个输入流当中最长的那个（但是并没有`-longest`这个选项），较短的流播放完毕后会有空缺，较长的流继续播放（参考“含有不等长的、有空白流的文件”）。

## 将多个文件串联式合并（concat）
[4.1, 4.2, 4.3, 4.4]
### concat protocol
[4.1]
`ffmpeg -i "concat:input1.ts|input2.ts|input3.ts" -c copy output.ts`
使用条件：
* 采用允许串连式合并的容器格式，例如 MPEG-2 transport streams (mpegts, .ts)、MPEG-1等。
* 编码参数基本相同，如编码格式、分辨率、帧率。

输出格式基本上不受限制，可以与输入不同，例如：

`ffmpeg -i "concat:input1.ts|input2.ts|input3.ts" -c copy -bsf:a aac_adtstoasc -movflags +faststart output.mp4`

因为 mp4 不被 concat 协议支持，所以可以先转化为 ts 再合并：

`ffmpeg -i input1.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts intermediate1.ts` 。

其中 `-bsf:v h264_mp4toannexb` 是针对 h.264 编码的一个修正，这是 mp4 中的 h.264 无损转 ts 必须的。

concat 协议中，输入文件的路径中有空格、中文并不需要特殊处理，但整个命令行参数用引号括起来还是必要的。

### concat demuxer
[4.1]
`ffmpeg -f concat -safe 0 -i mylist.txt -c copy output`

其中 mylist.txt 是输入文件的列表，格式为：

```
# this is a comment
file '/path/to/file1'
file '/path/to/file2'
file '/path/to/file3'
```

`#` 开头的是注释。`file` 指定文件，路径可以是绝对或相对（相对 mylist.txt）。
`-safe` 是 concat demuxer 的选项，默认是 1（true），只有设置为 0（false）才允许使用绝对路径。

与 concat protocol 相比，concat demuxer 几乎可以用于任意容器格式。但因为使用复制编码器（-c copy），
则也受到类似的限制。

concat demuxer 用于 mp4+h.264 文件以及 -c copy 时，视频结合部可能出现解码错误；mpegts+h.264 则不会。所以即使用 concat demuxer，最好也采用 ts 格式。

### concat filter
[4.1]
```
ffmpeg -i input1.mp4 -i input2.webm -i input3.mov \
-filter_complex "[0:v:0][0:a:0][1:v:0][1:a:0][2:v:0][2:a:0]concat=n=3:v=1:a=1[outv][outa]" \
-map "[outv]" -map "[outa]" output.mkv
```

concat filter 不能与复制编码器（-c copy）搭配，必须要重新编码；如果源文件使用不同的编码或参数，则也需要使用 concat filter 。

参考“复杂的流选择”一节。

## 将一段视频循环输入
### loop 过滤器
loop 过滤器仅适于视频部分，尚不清楚音频部分会如何。
```
# 0~29 帧重复 10 次
ffmpeg -i input1.mp4 -vf loop=loop=10:size=30:start=0 output.mp4
# 一个静态帧重复 100 次，帧率8
ffmpeg -i input1.jpg -vf loop=loop=100:size=1:start=0 -r 8 output.mp4
# 一个静态帧无限循环，并用 vlc 串流到 rtsp://127.0.0.1:8554/
ffmpeg -i input1.jpg -vf loop=loop=-1:size=1,scale=-1:640 -r 8 -f mpegts pipe: | vlc -v -I dummy --sout "#rtp{sdp=rtsp://:8554/}" --sout-keep -
```


## 跳转（seek），截取（切分）文件
[3.1]

要截取一个片段，可用 `-ss` 指定绝对开始时间，`-t` 指定片段时长，或 `-to` 指定绝对结束时间，或 `-vframes`（等效`-frames:v`） 指定帧数：

```
ffmpeg -i source.m4v -ss 593.3 -t 551.64 -c copy part2.m4v
ffmpeg -ss 00:23:00 -i Mononoke.Hime.mkv -frames:v 1 out1.jpg
ffmpeg -i video.mp4 -ss 00:01:00 -to 00:02:00 -c copy cut.mp4
```

`-ss` 既可以作为输入的参数（`-i input` 之前），也可以作为输出参数，两者的实现不同：前者尽量不解码直接跳转，后者则解码直到指定的位置（所以慢得多）；但是，如果指定 `-c copy` 则两者都不解码。
两者对时间戳的处理不同（前者会把跳转到的位置的时间戳重置为0，再交给下一步），除此之外应尽量用前者。

时间格式有两种，一是 HOURS:MM:SS.MILLISECONDS ，如 01:23:45.678；二是浮点数或整数，以秒为单位，如 593.3 。

`-c copy` 可以强迫不转码。但是如果开始时间处在两个 key frame 之间，copy 可能出问题。ffmpeg 的做法是从上一个 key frame 开始复制，并在时间戳上进行补偿，即第一个key frame的时间戳变成负值，播放器从第一个key frame开始解码，但从时间戳 >=0 的帧开始播放。例如 mp4 容器支持 “edit list” 特性，可以告诉播放器不是从第一帧而是后面的某一帧开始播放。但是有的播放器不支持这个特性，截取的文件播放就不正常。

`-t` 可以作为输入的参数，也可以作为输出参数，效果似乎相同。

`-to` 只能作为输出参数。注意，使用 `-to` 时，`-ss` 也应作为输出参数出现，否则 `-to` 的效果与 `-t` 相同，这是因为 `-ss` 作为输入的参数时重置时间戳。

一些 ffmpeg 官方文档没有指出或不准确，测试出来的行为（一般用mp4+h.264+aac测试）：

* `-ss -t` 作为输出参数并与 `-c copy` 联用时，并不会解码，性能上与作为输入参数没什么差别。

* `-ss -t` 作为输出参数并与 `-c copy` 联用时（用mp4测试），指定的时间是 dts 而不是 pts ，可以用 `ffprobe -show_packets` 查看（不要信任 `ffprobe -show_frames` 的 dts，不准确）。
  有时候第一帧的 dts 是负值（pts是0），因此 `-ss` 也得是负值。一段视频的 dts 时长不一定等于 pts 时长，所以时长也不一定是符合一般认知。
  这个行为可能是 bug，已经报告[3.2]。

* `-ss -t` 作为输出参数并与 `-c copy` 联用时，mp4->ts 的转换后的时间戳起点是 1.44s，这并不是错误。

* `-ss -t` 作为输出参数并与 `-c copy` 联用时，`-ss`不会对齐到最近的关键帧，而是丢弃非关键帧帧直到下一个关键帧；如果有音频则音频不会同时丢弃，可能造成开始的一段时间只有音频没有视频。
  `-ss` 的精度大致是 10^-7 秒，即 ffmpeg 的时间单位精度。

* `-ss -t` 作为输入参数并与 `-c copy` 联用时，`-to` 和 `-t` 都不精确，即要求 1秒长度可能得到3秒长度。

* `-ss -t` 作为输入参数并与 `-c copy` 联用时，`-ss` 会被对齐到最近的关键帧。

可能相关的选项：

-reset_timestamps 1
-copyts
-avoid_negative_ts 1
-noaccurate_seek

时间戳等于 `-ss` 的值的帧会包含在输出文件中，而时间戳等于 `-to` 或 `-t` 的帧不包含在输出文件中，也就是一个半开半闭区间。

## 多段切分，如 HLS
hls muxer:

```
ffmpeg -i all.ts -c copy -bsf:a aac_adtstoasc -f hls -hls_time 30 -hls_playlist_type vod -hls_segment_type fmp4 all-mp4.m3u8
ffmpeg -i all.ts -c copy -f hls -hls_time 20 -hls_playlist_type vod -hls_segment_type mpegts all.m3u8
```

用 `-hls_segment_type` 指定分段的格式。mpegts 是较为简单的。fmp4 会生成一个 init.mp4 文件和多个 .m4s 文件。后者是一个 mp4 分段（segment），不能独立播放。

segment muxer，切分为 mp4：
```
ffmpeg -i a.mp4 -c copy -f segment -segment_list playlist.m3u8 -segment_list_flags +live -segment_time 60 segs/%03d.mp4
```
每个分片是以关键帧开始的，可以独立播放；但分片的时间轴是全局的，即一个分片的第一帧的时间一般不是从0开始，而是接着上一个分片的结尾的时间戳。

## fragmented mp4, fmp4, ismv
HLS 中可以使用 mp4 分段，但要求是 fragmented mp4，即元数据附加于一个一个的分段，而不是集中于 mp4 文件的尾部。
ismv 就是 Smooth Streaming，分段的 mp4，用这个命令可以生成：

```
ffmpeg -i input -c copy -f ismv -frag_duration 2000000 output.mp4
```
`-frag_duration` 给定分段的时间长度，注意单位是微秒。
用 `MP4Box -info output.mp4` 可以确认是不是分段 mp4，ffmpeg 还不行。

不过，直接用整个的分段 mp4 文件作为的 HLS 的分段，android、chrome android 并不兼容，但 webkit 核心的浏览器是可以播放的（如 epiphany-browser）。

## 含有不等长的、有空白流的文件
一个文件/容器中的视频、音频流的时长是有可能不等的，例如做并联式合成，但没有用 `-shortest` 选项。

这会有一些后果：
* 整个文件的总时长按最长的那个流算。
* 这个文件参与串联式合并时，按最长的那个流算。这样可能在生成的文件中间出现某个流的空缺，不过播放一般没问题。
* ffmpeg 做转码、转容器时，不会自动给较短的流补上时长。也就是说，如果一个文件中视频流100秒，音频流70秒，那么单独提取出音频后，只有70秒。

## 一个输入文件，多个输出文件
如果我们想要将一个流媒体的视频和音频分离开，分别写入两个文件，就可以用到一个输入文件，多个输出文件的技巧。例如：

`ffmpeg -i rtsp://xxxx.xx -an -c:v copy video.mp4 -vn -c:a copy video.aac`

其中，一个输入是 `-i rtsp://xxxx.xx`，视频输出及其参数是 `-an -c:v copy video.mp4` ，音频输出及其参数是 `-vn -c:a copy video.aac` 。

`-vn` 不一定要加，因为 aac 格式本来就只能包括音频。

## 复杂的流选择
也可以用 `-map` 来选择和丢弃流。

## 生成无声音频流（音轨）
可以用 anullsrc 设备（实际上是个过滤器filter）：

`ffmpeg -f lavfi -i anullsrc=channel_layout=stereo:sample_rate=44100 -i video.mov -shortest -c:v copy -c:a aac output.mov`

其中 lavfi 的意思是 "Libavfilter virtual input device", `-i anullsrc` 必须与 `-f lavfi` 搭配使用。
channel_layout（简写cl）可以是 stereo（立体声，2声道）或 mono（单声道），sample_rate（简写r）是采样率，不指定时，以上值就是默认值。
`-shortest` 意思是采用输入的各个流中最短的那个作为输出文件的总时长（anullsrc无限长）。

## 静态图片转为视频
```
ffmpeg -r 10 -i %d.jpg -r 30 out.mp4
```

将输入的帧命名为 1.jpg, 2.jpg 等即可。-r 指定输入和输出的帧率。
注意，当输入帧数小于3时，ffmpeg 可能有bug，产生的视频的帧数可能比输入的帧少。

单帧静态图片循环视频：
```
ffmpeg  -i 4.jpg -vf loop=loop=100:size=1 -r 8  still_image_4.mp4
```
`loop=100`：循环 100 次，`size=1`：输入的帧数为1。`loop=-1`：无限循环。
`-r 8` 输出的帧率是 8fps。


## 编码格式、流格式、容器格式
* 编码格式：音视频的压缩方式，例如音频的 aac、mp3、pcm，视频的 h.264、vp8等。编码格式一般不包括元数据，如帧率、时间戳等。裸的编码格式可能不适于传输或作为文件存储。
* 流格式：编码格式的最简单封装，可以传输或作为文件存储。与容器格式的区别是：一般只能用于一种编码格式，不能同时容纳音频、视频；缺少一些元数据如时间戳。例如，aac音频编码可以采用 ADTS (Audio Data Transport Stream) 流格式[8.1]，作为文件时扩展名.aac；嵌入 mp4 容器时则采用 ASC（Audio Specific Configuration）流格式。
* 容器格式：用来容纳一个或多个并行的音视频和字幕流。音视频流可能采取裸的编码格式或某种流格式。容纳多个流时，这些流的数据一般被切分为小块（叫做“包”(packet)），交错地按解码/播放顺序存放，方便解码播放，例如：`[第1帧视频][第1-2帧的音频][第2帧视频][第2-3帧的音频]` 。在每个包上，容器格式会记录时间戳，用来指定应该在什么时间点播放。对于视频，通常每个包包含且只包含一帧。对于音频，一个包通常包含固定时长的“采样”（sample）。

### 需要转换的流格式
h.264 和 aac 在不同的容器格式（mpegts、aac、mp4）中要采取不同的流格式，ffmpeg 有时候不能自动进行转换，这就需要指定转换。`-bsf:a` 和 `-bsf:v` 分别是音频、视频流格式转换器（Bitstream Filter）。

aac 编码在 aac 和 mpegts 容器中采取 ADTS 流格式，在 mp4、m4a、flv容器中采取 ASC 格式，因此转换时：

`ffmpeg input.ts -c copy -bsf:a aac_adtstoasc output.mp4`

比较新的(2018) ffmpeg 可以自动应用这个转换，不需要指定。

h.264 编码从 mp4 转到 mpegts 时，要转成 Annex B of the ITU-T H.264 specification：

`ffmpeg input.mp4 -c copy -bsf:v h264_mp4toannexb output.ts`

更多的流格式转换器用法见[1.3]。

### 容器上的时间戳
时间戳一般记录在容器层次。
编码格式中一般没有时间戳，但可能有帧率等元数据，因此裸的编码格式有时候也可以播放，例如 .264 格式（h.264裸流）。

## 字幕

### mp4 的内嵌 mov_text 格式字幕

mp4 容器支持的内嵌字幕轨道只有 mov_text 一种。vlc、mpv 等本地播放器以及苹果设备支持这种字幕，但是浏览器目前并不支持。

将外置 srt 字幕转为内嵌 mov_text 字幕[9.1]：
```
ffmpeg -i infile.mp4 -i infile.srt -c copy -c:s mov_text outfile.mp4
```
用 `-c:s mov_text` 指定输出的字幕编码，c 指 codec，s 指 subtitle 。如果与 -c copy 连用就要放在它后面，这样就部分覆盖了 -c copy。

更复杂的例子参考[9.2]。

### 转换字幕格式

```
ffmpeg -i subtitles.srt subtitles.ass
```

### 烧录字幕到视频中
[9.1]
```
ffmpeg -i mymovie.mp4 -vf ass=subtitles.ass mysubtitledmovie.mp4
```

## 元数据

ffmpeg 可以给媒体容器文件添加元数据：

```
ffmpeg -i 0.ts -c copy -metadata title="title" -metadata comment="comment" 0.mp4
```
mp4 支持的字段有：title author album year track comment copyright description lyrics ，mp3 有 artist 等，详见 [14.1]。

## 分析视频（ffprobe）

### 例子
* 提取视频关键帧信息，json格式

`ffprobe -v quiet -select_streams v -skip_frame nokey -show_frames -print_format json input.mp4`

如果省略 `-select_streams v` 则也输出音频帧；如果省略 `-skip_frame nokey` 则也显示非关键帧。
如果有多个视频流，用 `v:0` 或 `v:1` 进一步限定。

### 参数

-show_frames        show frames info
-show_packets       show packets info
-show_programs      show programs info
-show_streams       show streams info
-show_chapters      show chapters info
-count_frames       count the number of frames per stream
-count_packets      count the number of packets per stream

-select_streams v|a
-skip_frame nokey|nointra|bidir

### -show_frames
可以显示帧的类型（pict_type=I|B|P，key_frame=0|1）。
key_frame=1 是 key frame，pict_type=I 是 i-frame，参考 "i-frame 和 key frame"。

[FRAME]
media_type=video
stream_index=0
key_frame=0
pkt_pts=3271500
pkt_pts_time=36.350000
pkt_dts=3271500
pkt_dts_time=36.350000
best_effort_timestamp=3271500
best_effort_timestamp_time=36.350000
pkt_duration=4500
pkt_duration_time=0.050000
pkt_pos=2499268
pkt_size=1790
width=1280
height=718
pix_fmt=yuv420p
sample_aspect_ratio=N/A
pict_type=P
coded_picture_number=727
display_picture_number=0
interlaced_frame=0
top_field_first=0
repeat_pict=0
[/FRAME]

结合 `-print_format json` 可以输出 json 格式：

```
{
    "frames": [
        {
            "media_type": "video",
            "stream_index": 0,
            "key_frame": 1,
            "pkt_pts": 0,
            "pkt_pts_time": "0.000000",
            "pkt_dts": 0,
            "pkt_dts_time": "0.000000",
            "best_effort_timestamp": 0,
            "best_effort_timestamp_time": "0.000000",
            "pkt_duration": 512,
            "pkt_duration_time": "0.040000",
            "pkt_pos": "48",
            "pkt_size": "47749",
            "width": 1366,
            "height": 768,
            "pix_fmt": "yuv420p",
            "sample_aspect_ratio": "1:1",
            "pict_type": "I",
            "coded_picture_number": 0,
            "display_picture_number": 0,
            "interlaced_frame": 0,
            "top_field_first": 0,
            "repeat_pict": 0
        },...
}
```

-show_frames 的性能不是很好（不清楚是否要解码）。对一个384p的h.264+aac视频，速度大约是900帧/s（只处理关键帧是160帧/s），音频的影响不大。

-show_frames 显示的 pkt_dts、pkt_dts_time 似乎是不可信的。（1）与 -show_packets 显示的数值可能不符（2）使用或不使用 `-skip_frame nokey` 的时候，数值不同。

-show_frames 显示的 pkt_pts、pkt_pts_time 与 -show_packets 的 pts、pts_time 相符。pkt_pos、pkt_size 与 -show_packets 的 size、pos 相符。

### -show_packets
对应 AVPacket 结构，与 -show_frames 的结果是对应的。

flags: "K_" 里面的 'K' 应该是表示 key frame 。注意几乎所有的音频包都有 K flags，因为确实符合定义。


```
{
    "packets": [
        {
            "codec_type": "video",
            "stream_index": 0,
            "pts": 1,
            "pts_time": "0.040000",
            "dts": 0,
            "dts_time": "0.000000",
            "duration": 1,
            "duration_time": "0.040000",
            "size": "10805",
            "pos": "1812584",
            "flags": "K_"
        },
        {
            "codec_type": "video",
            "stream_index": 0,
            "pts": 4,
            "pts_time": "0.160000",
            "dts": 1,
            "dts_time": "0.040000",
            "duration": 1,
            "duration_time": "0.040000",
            "size": "1050",
            "pos": "1823389",
            "flags": "__"
        },
```

```
[PACKET]
codec_type=audio
stream_index=1
pts=13676544
pts_time=310.125714
dts=13676544
dts_time=310.125714
duration=1024
duration_time=0.023220
convergence_duration=N/A
convergence_duration_time=N/A
size=144
pos=19528456
flags=K_
[/PACKET]
```

### 显示容器、流的元数据 -show_streams -show_format

`ffprobe -v quiet -show_streams -show_format -print_format json -i input`

输出：

```
{
    "streams": [
        {
            "index": 0,
            "codec_name": "h264",
            "codec_long_name": "H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10",
            "profile": "High",
            "codec_type": "video",
            "codec_time_base": "1/50",
            "codec_tag_string": "avc1",
            "codec_tag": "0x31637661",
            "width": 1366,
            "height": 768,
            "coded_width": 1376,
            "coded_height": 768,
            "has_b_frames": 2,
            "pix_fmt": "yuv420p",
            "level": 32,
            "chroma_location": "left",
            "refs": 1,
            "is_avc": "true",
            "nal_length_size": "4",
            "r_frame_rate": "25/1",
            "avg_frame_rate": "25/1",
            "time_base": "1/12800",
            "start_pts": 0,
            "start_time": "0.000000",
            "duration_ts": 51712,
            "duration": "4.040000",
            "bit_rate": "176471",
            "bits_per_raw_sample": "8",
            "nb_frames": "101",
            "disposition": {
                "default": 1,
                "dub": 0,
                "original": 0,
                "comment": 0,
                "lyrics": 0,
                "karaoke": 0,
                "forced": 0,
                "hearing_impaired": 0,
                "visual_impaired": 0,
                "clean_effects": 0,
                "attached_pic": 0,
                "timed_thumbnails": 0
            },
            "tags": {
                "language": "und",
                "handler_name": "VideoHandler"
            }
        }
    ],
    "format": {
        "filename": "xxx.mp4",
        "nb_streams": 1,
        "nb_programs": 0,
        "format_name": "mov,mp4,m4a,3gp,3g2,mj2",
        "format_long_name": "QuickTime / MOV",
        "start_time": "0.000000",
        "duration": "4.040000",
        "size": "91166",
        "bit_rate": "180526",
        "probe_score": 100,
        "tags": {
            "major_brand": "isom",
            "minor_version": "512",
            "compatible_brands": "isomiso2avc1mp41",
            "encoder": "Lavf56.25.101"
        }
    }
}
```

`streams[i].codec_type == 'video'` 表示视频流。`streams[i].nb_frames` 是总帧数。`streams[i].duration` 和 `format.duration` 是总时长，单位是秒；两者的区别在于，前者来自流的元数据，后者来自容器的元数据；元数据可能不存在（如 Matroska 和 WebM 没有流元数据），也可能不准确。

可以给 ffprobe 加上 -count_frames 参数，这样会增加一个 `streams[i].nb_read_frames` 属性，应该也是总帧数，但运行很慢，可能是解码了所有的帧。
给 ffprobe 加上 `-show_packets` 参数，或者 `-count_packets` 参数，会增加一个 `streams[i].nb_read_packets` 属性，对于视频来说一般也等于总帧数。这个做法运行较快。

可以加上 `-show_packets` 并解析 packets 属性（数组），可以计量视频帧数，总时长就是最大的 `pts_time + duration_time` 。

### 显示媒体详情
`ffprobe -loglevel trace -i input.ts`
用 ffmpeg 效果也差不多：
`ffmpeg -loglevel trace -i input.ts`

此外，mediainfo 工具有时候更好：

`mediainfo -f input.ts`
`mediainfo --Details=1 input.ts`

## 音视频基本概念
### i-frame 和 key frame
[10.1]
都是视频编码的概念，两者并不完全相同，后者是前者的子集。

key frame: 自身的解码不依赖于本bitstream前面的任何数据（包括帧，也包括状态数据（量化表之类））；这个帧之后的帧的解码不依赖于这个帧之前的任何数据。总之，解码器只需要从这个帧往后的数据就可以解码从这个帧往后的所有帧。

i-frame: 自身的解码不依赖于前面的帧，但可以依赖前面的其它数据；这个帧之后的帧可以依赖这个帧之前的 i-frame。

不同的编码格式中，术语并不都是 i-frame 和 key frame。例如h.264和vp8/9中将 key frame 叫做 IDR frame 和 key frame：

i-frame: I-frame picture (H.262 / MPEG-2 part 2) or intra-only frame (VP9) or consists solely of an I-slice (H.264 / MPEG-4 part 10)
key frame: point of random access(MPEG-2), instantaneous decoding refresh (IDR) picture (H.264), key frame (VP8 and VP9)

注意在 ffmpeg 中，几乎所有的音频包都被认为是 key frame，因为几乎所有的音频包都可以独立解码，符合定义。

## 控制 i-frame、key frame 、B frame 的出现
如果不指定，ffmpeg 采用一些默认做法来插入关键帧，如固定的 GOP、根据场景变化、根据原视频的关键帧位置等。

将一个视频重压，默认情况下关键帧位置会保持不变，但微小的位移是有可能的（mp4(h.264)->mp4(h.264))。


### 在指定位置插入关键帧
-force_key_frames[:stream_specifier] time[,time...] (output,per-stream)
这是在指定位置插入关键帧，是输出参数。注意，与 -c:v copy 联用时被忽略。

### 指定 i-frame/key frame 间隔
`-g N`
指定 GOP，单位是帧。默认值是12。
这里 GOP 是 i-frame 还是 key frame 间隔尚不明确。

常用的 GOP，换算成时间（除以帧率）在数秒量级。

### 指定最大连续 B frame 数量
`-bf N`
-1~16，0表示禁用B帧，-1表示使用编码器默认行为，默认0（这个似乎有问题，默认情况下 libx264 是会生成 B 帧的，相当于-1）。

最大连续 B frame 数量一般在 2-5 之间为宜，特殊场景可能用到 16 [10.2]。

### 参考帧的数量
是指 P/B frame 最多可以参考几个别的帧。这个数值决定了解码器的帧缓存的帧数需求。
一些硬件解码器因为资源受限，会限制允许的参考帧的数量，所以编码时可能也需要指定。

ffmpeg 的参数是 `-refs N` ， 对应的 x264 参数是 `ref=N` [12.3]。

取值范围1-16，x264 默认值是3，ffmpeg 默认值是 1。

## 传递参数给 x264 编码器
用 x264 --fullhelp 或者 x264 -h 可以查看 x264 支持的选项。部分选项的解释可以参考 [12.1]。

`ffmpeg ... -x264opts key1=value1:key2=value2 ` 可以传递参数给 x264 编码器，这里的key不带前面的 `-`。

x264 参数与 ffmpeg 通用参数的对应关系可以参考[12.2, 12.3]，但应注意有一些过时或写错的参数。

## 画面缩放、旋转、裁剪等变换
将画面缩小到 1/2：

```
ffmpeg -i input -vf scale=iw/2:-1 output
```
`-vf` 是指视频过滤器（video filter），`scale` 是过滤器类型（缩放），`iw` 是ffmpeg内置变量，指输入画面的宽度（`ih`则是高度），`iw/2:-1`是指“画面宽度缩小到输入画面的1/2，宽度不指定（-1，按同比例缩放）。`iw/2` 也可写作 `iw*0.5`。

将画面顺时针旋转90度：

```
ffmpeg -i input -vf rotate=PI/2:oh=iw:ow=ih output
```
`rotate` 是过滤器类型（旋转），值是旋转角度，单位是弧度，顺时针为正，PI是ffmpeg内置常数，即3.14...，旋转的中心是画面的中心。
`ow, oh` 是ffmpeg内置变量，指输出画面的宽度和高度。因为是旋转90度所以用 `oh=iw:ow=ih` 交换宽度高度。如果不指定 ow 和 oh，则与 iw 和 ih 相同，这会造成画面较长的方向被截掉一部分，较短的方向加上黑边。
要旋转任意角度并确保画面不被截掉，可以写 
```
ffmpeg -i input -vf rotate='PI/2:ow=hypot(iw,ih):oh=ow' output
```

`hypot()` 是ffmpeg内置函数，意思是平方和的平方根。注意单引号的使用，因为 `-vf` 参数内部使用 `,` 来分割不同的过滤器，所以要避免与 `hypot()` 参数表中的逗号混淆。

先缩小到 1/2，再顺时针旋转90度：
```
ffmpeg -i input -vf "scale=iw/2:-1,rotate=PI/2:oh=iw:ow=ih" output
```

多个串联的过滤器可以用逗号连接。注意随着过滤器的应用，iw、ih的值也会发生变化：它总是指本级过滤器面对的输入画面的值，它可能是上一级过滤器的输出画面的值。

裁剪画面：

```
ffmpeg -i input -vf crop=100:100:12:34 output
```

crop 的4个参数按顺序是 `out_w, out_h, x, y`，即截取的区域的宽度、高度，左上角x坐标和y坐标。如果省略x,y，如 `crop=100:100`，则是居中截取。
注意，如果 crop 的坐标超出输入画面的范围，ffmpeg 会报错退出，而不是自动修改范围。

## 静态图像的输入、输出
ffmpeg 的视频操作基本上都可以作用于静态图像。

例如，对一个 jpg 文件缩放、旋转、截取:
```
ffmpeg -i input.jpg -vf "scale=iw/2:-1,rotate=PI/2:oh=iw:ow=ih,crop=381:454:470:550" -q:v 3 output.jpg
```

`-q:v` 控制输出的画面质量。对于 jpg，数值越小质量越高，文件也越大，3是基本不失真的，0～2 输出的文件大小几乎没有差别；不指定时，默认的 jpg 输出质量比较低，但似乎并非对应于某个固定的 `-q:v`（文件大小在 `-q:v 15` 左右），所以经常有必要指定。

## 在帧上绘制文本
[15.1-15.4]
ffmpeg 可以在输出帧上绘制文本。

此处举例绘制帧的时间戳、帧序号的方法：

```
ffmpeg -i input.mp4 -vf "scale=-1:432, drawtext=fontfile=Arial.ttf: text='Frame\:%{frame_num}, pts\:%{pts\:hms}, localtime\:%{localtime}': x=(w-tw)/2: y=h-(2*lh): fontcolor=black: fontsize=20: box=1: boxcolor=white: boxborderw=5" output.mp4
```
帧序号用变量 %{frame_num} 取得，默认从0开始，用 start_number=1 可以改为从 1 开始。
%{pts\:hms} 为显示时间，秒的部分有小数精确到毫秒。尚不知道只采用秒为单位的写法。
%{localtime} 为本地时间。
text中的':'等需要转义。
fontfile=Arial.ttf 的文件名应为全路径或相对当前目录的路径。这个部分也可以省略，让ffmpeg从系统中选择一个字体。
