# 编译 Windows 版 ffmpeg 的方法

## 参考资料
[1] CompilationGuide/MSVC
https://trac.ffmpeg.org/wiki/CompilationGuide/MSVC

[2] win下编译ffmpeg库,Compile and build ffmpeg library and dll on Windows x64( 正版)
http://www.cnblogs.com/erickingxu/p/3794507.html

[3] yasm
http://yasm.tortall.net/

[4] mingw
https://sourceforge.net/projects/mingw/files/Installer/

[5] How to install pkg config in windows?
http://stackoverflow.com/questions/1710922/how-to-install-pkg-config-in-windows

[6] MinGW + x264 + ffmpeg
http://www.cnblogs.com/chsin/archive/2013/04/15/3022299.html

[7] impossible to build statically linked standalone binaries with msys?
http://mingw.5.n7.nabble.com/impossible-to-build-statically-linked-standalone-binaries-with-msys-td10578.html

[8] ffmpeg no longer works under Windows XP (possible suspect identified)
https://ffmpeg.zeranoe.com/forum/viewtopic.php?t=2477

[9] Making last version of FFmpeg work under XP
https://ffmpeg.zeranoe.com/forum/viewtopic.php?f=13&t=3572

[10] (patch) Cannot build ffmpeg with --enable-libopenh264
https://trac.ffmpeg.org/ticket/5417

[11] Compile for Windows on Linux
http://www.blogcompiler.com/2010/07/11/compile-for-windows-on-linux/

[12] mingw installation on Linux
https://stackoverflow.com/questions/39622501/mingw-installation-on-linux

[13] [FFmpeg-devel] [MSVC toolchain] Patch to allow building FFmpeg with Linux bash on Windows (WSL)
https://ffmpeg.org/pipermail/ffmpeg-devel/2017-December/223104.html

## msys + VS
(不推荐，建议用 WSL + VS)

### 安装 yasm / nasm
将 yasm.exe 和  nasm.exe 安装到 PATH 中。
ffmpeg 使用 yasm。较新的 x264 使用 nasm，从这里获取 http://www.nasm.us/

### 安装 msys
运行 mingw-get-setup.exe
默认安装位置 C:\MinGW

装好后，在 C:\MinGW\msys\1.0\etc 新建 fstab 文件，内容是：
  C:\MinGW   /mingw
  
运行 C:\MinGW\msys\1.0\msys.bat 可以打开 msys 控制台。

### 安装 pkg-config
如果不需要通过 pkg-config 来查找外部库如 x264，则不需要这一步。

http://ftp.gnome.org/pub/gnome/binaries/win32/dependencies/pkg-config_0.26-1_win32.zip
http://ftp.gnome.org/pub/gnome/binaries/win32/glib/2.28/glib_2.28.8-1_win32.zip

分别提取 pkg-config.exe 和 libglib-2.0-0.dll 和 intl.dll 到某个在 PATH 下的目录。

### 安装 VS 2013+

### 启动命令行窗口（设定环境变量）
* 启动VS控制台。在 Windows 控制台 cmd.exe 中执行
  "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\vcvarsall.bat" x86_amd64|x86
或者执行 "开始菜单->Visual Studio->Visual Studio Tools->VS2013 x64 兼容工具命令提示|VS2013 x86 本机工具命令提示"

VS2015 的版本号是 14.0 。

注意 x86 和 x64 分别用于编译 32 和 64 位程序，不可混用。

* 在VS控制台中启动msys控制台。接着，在VS控制台中执行：
    C:\MinGW\msys\1.0\msys.bat
  这样会打开一个新的 msys bash 窗口，可以在这里使用 VS 的命令和 mingw 命令。

* 修正 link.exe
  在上一步，在 bash 窗口中，mingw 的 link 会优先于 VS 的 link，执行
    where link
  得到
```
    C:\MinGW\msys\1.0\bin\link.exe
    c:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\bin\link.exe
    或者
    c:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\bin\amd64\link.exe
```
  即有效的是msys的link.exe，这是不行的。要执行
```
    export PATH="/c/Program Files (x86)/Microsoft Visual Studio 12.0/VC/BIN/":$PATH
    export PATH="/c/Program Files (x86)/Microsoft Visual Studio 14.0/VC/BIN/":$PATH  #VS2015
```
  对64位编译环境则是（注意32/64的选择要与“启动命令行窗口”一致）
```
    export PATH="/c/Program Files (x86)/Microsoft Visual Studio 12.0/VC/BIN/amd64/":$PATH
    export PATH="/c/Program Files (x86)/Microsoft Visual Studio 14.0/VC/BIN/amd64/":$PATH  #VS2015
```
    
* 修正 cl 64bit
  VC++ cl 有 32 位和 64 位两个版本，默认情况下只有前者在路径里，即
  c:\Program Files (x86)\Microsoft Visual Studio XX.0\VC\bin\cl.exe

  后者在

  c:\Program Files (x86)\Microsoft Visual Studio XX.0\VC\bin\amd64\cl.exe

  修正 link.exe 的步骤也会影响 cl。

### 编译
* 注意事项
msys的 make 3.81 的并行编译似乎有bug，会导致死锁。如果发现make -jN进度停滞不前，可以在命令行中停止make，改用串行make。
或者，不指定并行数量，make -j 也可以。

msys还带有一个 mingw32-make (版本3.82)，可以并行编译，可以用它替换 make 3.81。

* x264
[1]
```
cd E:/project/codecbox.js.git/build/x264
CC=cl ./configure --enable-static --extra-cflags="-DNO_PREFIX" --prefix=/E/project/codecbox.js.git/build/dist-native
make
make install
```

根据[1]，NO_PREFIX 对于 VC 编译器可能是必要的，对其他编译器则未必。

./configure 失败则可以看 config.log 日志。

* openh264
```
  cd /e/project/codecbox.js.git/build/openh264
  make ASM=yasm OS=msvc PREFIX=/e/project/codecbox.js.git/build/dist-native install
```

注意 openh264 会去寻找 nasm，所以要加上 ASM=yasm 。
注意目前（2016.4），master 分支无法与 ffmpeg 编译，见 https://github.com/cisco/openh264/issues/2396
所以需要使用 openh264 的 1.5.1 分支。

* SDL 1.2
  只有 ffplay 需要。
```
  cd /e/project/codecbox.js.git/build/SDL-1.2.15
  LDFLAGS="-static" ./configure --prefix=/e/project/codecbox.js.git/build/dist-native
  mingw32-make -j4
```

  SDL 并不支持在 mingw 中使用 VC 编译器，如果设置 CC=cl CXX=cl 则 configure 会失败。不过 SDL 支持直接用 VC 编译。
  
* ffmpeg
  注意 ffmpeg 不能用 mingw32-make 来编译，只能用 msys 自带的 make，否则会有语法错误。但这样不能指定并行数，要用 make -j 代替。

```
  export LIB="E:/project/codecbox.js.git/build/dist-native/lib;$LIB"
  export INCLUDE="E:/project/codecbox.js.git/build/dist-native/include;$INCLUDE"
  export PKG_CONFIG_PATH=/e/project/codecbox.js.git/build/dist-native/lib/pkgconfig

  cd /e/project/codecbox.js.git/build/ffmpeg
  
  ./configure --toolchain=msvc --prefix=/e/project/codecbox.js.git/build/dist-native --enable-ffplay --disable-ffprobe  --enable-gpl --enable-libx264 --enable-libopenh264 --enable-encoder=libx264 --enable-encoder=libopenh264
```
  
  或者
```
  ./configure --toolchain=msvc --prefix=/e/project/codecbox.js.git/build/dist-native --disable-ffprobe
```
  
  保留 libx264，不保留 ffplay 。
```
  ./configure --toolchain=msvc --prefix=/e/project/codecbox.js.git/build/dist-native --disable-ffplay --disable-ffprobe  --enable-gpl --enable-libx264 --enable-encoder=libx264
  
  make
  make install
```
  
  ffmpeg 的 configure 可以自动检测到 yasm 所以不用指定；默认编译 ffmpeg 和 ffprobe 但不编译 ffplay；默认不会启用外部codec，所以要加上 --enable-gpl --enable-libx264 --enable-libopenh264 。

```
Enabled protocols:
async                   http                    rtmpt
cache                   httpproxy               rtmpts
concat                  https                   rtp
crypto                  icecast                 srtp
data                    md5                     subfile
ffrtmphttp              mmsh                    tcp
file                    mmst                    tls_schannel
ftp                     pipe                    udp
gopher                  rtmp                    udplite
hls                     rtmps

Enabled indevs:
dshow                   lavfi                   vfwcap gdigrab
```

* 调试版
在 configure 命令后加上 --enable-debug 参数。
似乎即使不加，也会生成 ffmpeg_g.exe 和 ffmpeg_g.pdb ，可以在 VS 中调试。
如果编译 dll，则每个 dll 也会生成一个 pdb 文件。

* ffplay
  因为依赖 SDL，所以不能用 VC 编译。如果之前用 VC 编译过，要先 make clean。

```
  export LIB="E:/project/codecbox.js.git/build/dist-native/lib;$LIB"
  export INCLUDE="E:/project/codecbox.js.git/build/dist-native/include;$INCLUDE"
  export PKG_CONFIG_PATH=/e/project/codecbox.js.git/build/dist-native/lib/pkgconfig
  
  ./configure --prefix=/e/project/codecbox.js.git/build/dist-native --enable-ffplay --disable-ffprobe --disable-ffmpeg --disable-iconv --disable-protocol=https --disable-protocol=tls_schannel --disable-encoders --extra-ldflags="-static"
  mingw32-make -j4
  make install
```

错误1：
```
libavformat/tls_schannel.c: In function 'tls_shutdown_client':
libavformat/tls_schannel.c:114:49: error: 'SEC_I_CONTEXT_EXPIRED' undeclared (first use in this function)
         if (sspi_ret == SEC_E_OK || sspi_ret == SEC_I_CONTEXT_EXPIRED) {
                                                 ^
libavformat/tls_schannel.c:114:49: note: each undeclared identifier is reported only once for each function it appears in
libavformat/tls_schannel.c: In function 'tls_read':
libavformat/tls_schannel.c:437:25: error: 'SEC_I_CONTEXT_EXPIRED' undeclared (first use in this function)
             sspi_ret == SEC_I_CONTEXT_EXPIRED) {
                         ^
common.mak:60: recipe for target 'libavformat/tls_schannel.o' failed
```

加上 --disable-protocol=https --disable-protocol=tls_schannel 可解决

错误2：

```
C:\MinGW\bin\ar.exe: libav: No such file or directory
library.mak:40: recipe for target 'libavcodec/libavcodec.a' failed
mingw32-make: *** [libavcodec/libavcodec.a] Error 1
```

用 make 可解决。

错误3：
提示缺少libgcc_s_dw2-1.dll 和 libiconv-2.dll 和 libmsys-1.0.dll。

将 ffplay.exe 和 SDL.dll 移动到 C:\MinGW\msys\1.0\bin，则可以从 msys 的 bash 中运行，但直接从 windows 中运行还是不行。
根据[7]，似乎很难让用 mingw gcc 编译的程序摆脱对 msys 的依赖。

## 编译自己的代码，链接到 ffmpeg 的库
可以参考 ffmpeg 编译系统的编译器参数。可以用以下方法查看：

如果已经编译过一次

touch ffmpeg.c

让 make 显示具体的命令行

make SHELL="/bin/bash -x"

结果大致如下：

```
cl -I. -I./ -D_ISOC99_SOURCE -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE -Dstrtod=avpriv_strtod -Dsnprintf=avpriv_snprintf -D_snprintf=avpriv_snprintf -Dvsnprintf=avpriv_vsnprintf -D_WIN32_WINNT=0x0502 -nologo -D_USE_MATH_DEFINES -D_CRT_SECURE_NO_WARNINGS -D_CRT_NONSTDC_NO_WARNINGS -Z7 -W4 -wd4244 -wd4127 -wd4018 -wd4389 -wd4146 -wd4057 -wd4204 -wd4706 -wd4305 -wd4152 -wd4324 -we4013 -wd4100 -wd4214 -wd4307 -wd4273 -wd4554 -wd4701 -O2 -Oy- -Dinline=__inline -c -Foffmpeg.o ffmpeg.c

./compat/windows/mslink -libpath:libavcodec -libpath:libavdevice -libpath:libavfilter -libpath:libavformat -libpath:libavresample -libpath:libavutil -libpath:libpostproc -libpath:libswscale -libpath:libswresample -nologo -LARGEADDRESSAWARE -debug -out:ffmpeg_g.exe cmdutils.o ffmpeg_opt.o ffmpeg_filter.o ffmpeg.o ffmpeg_dxva2.o libavdevice.a libavfilter.a libavformat.a libavcodec.a libswresample.a libswscale.a libavutil.a ws2_32.lib vfw32.lib user32.lib gdi32.lib psapi.lib ole32.lib strmiids.lib uuid.lib oleaut32.lib shlwapi.lib Secur32.lib psapi.lib advapi32.lib shell32.lib ole32.lib user32.lib
```

其中
```
-Oy- ：帧指针省略（关闭）
-Z7 启用旧式调试信息
-c 只编译
```

如果编译 C++ ，可能要用到：
-EHsc 使用异常处理

那些宏定义在编译自己的代码时不一定必要。

例子：

```
cl -nologo -D_ISOC99_SOURCE -EHsc -I E:/project/codecbox.js.git/build/dist-native/include -c -FoCodecBoxDecoder.o CodecBoxDecoder.cpp

cl -nologo -D_ISOC99_SOURCE -EHsc -I E:/project/codecbox.js.git/build/dist-native/include -c -Fotest.o test.cpp

link -libpath:E:/project/codecbox.js.git/build/dist-native/lib -debug CodecBoxDecoder.o test.o libavdevice.a libavfilter.a libavformat.a libavcodec.a libswresample.a libswscale.a libavutil.a ws2_32.lib vfw32.lib user32.lib gdi32.lib psapi.lib ole32.lib strmiids.lib uuid.lib oleaut32.lib shlwapi.lib Secur32.lib psapi.lib advapi32.lib shell32.lib ole32.lib user32.lib -out:test.exe 
```

-D_ISOC99_SOURCE 似乎并不必要。

也可以创建 VS 工程，需要注意的点是：

（1）要选用 release 模式。debug模式有问题。

（2）选用多字节编码而不是unicode

（3）C运行时间库选用“多线程”

（4）链接输入文件应包括

libavdevice.a
libavfilter.a
libavformat.a
libavcodec.a
libswresample.a
libswscale.a
libavutil.a
ws2_32.lib
vfw32.lib
psapi.lib
advapi32.lib
strmiids.lib
shlwapi.lib
Secur32.lib

## Windows XP 的问题
ffmpeg-20150525-git-8ce564e 是最后一个支持 XP 的版本，之后需要一个补丁才可以[8]。
不使用 --enable-libmfx 时，最后支持 XP 的版本是 ffmpeg-20160227-git-5156578

## MXE + WSL
（不推荐，建议用 WSL + VS）

下载 MXE
git clone https://github.com/mxe/mxe.git git mxe.git

文档：docs/index.html

在 WSL 中，安装MXE的依赖：

```
  apt-get install \
    autoconf \
    automake \
    autopoint \
    bash \
    bison \
    bzip2 \
    flex \
    g++ \
    g++-multilib \
    gettext \
    git \
    gperf \
    intltool \
    libc6-dev-i386 \
    libgdk-pixbuf2.0-dev \
    libltdl-dev \
    libssl-dev \
    libtool-bin \
    libxml-parser-perl \
    make \
    openssl \
    p7zip-full \
    patch \
    perl \
    pkg-config \
    python \
    ruby \
    scons \
    sed \
    unzip \
    wget \
    xz-utils
```

下载需要的软件包（在 WSL 中 mxe.git目录）：

make download-x264

修改编译设置：mxe.git/settings.mk ：
  MXE_TARGETS := i686-w64-mingw32.shared
可能的选项：
```
  i686-w64-mingw32.static  32位静态
  i686-w64-mingw32.shared  32位dll
  x86_64-w64-mingw32.static 64位静态
  x86_64-w64-mingw32.shared  64位dll
```
可以同时包括多个选项，用空格分开。
  
编译：
  make x264

如果编译失败，在日志（mxe.git/log）里找原因。可能会有一些莫名其妙的找不到头文件的错误，再次运行有可能通过。
目前发现如果 mxe.git 位于普通 NTFS 文件系统，则编译 gcc 时会因为一些莫名其妙的找不到头文件的错误而失败；在 WSL 的模拟 linux 文件系统中（如 ~ 目录）则能成功。

编译出来的程序安装在 mxe.git/usr 中，编译中的临时目录是 mxe.git/tmp-*


## MXE + Linux
（不推荐，建议用 WSL + VS）

/etc/lsb-release 中如果有冒用 ubuntu 等发行版的情况，最好改回来，否则可能出错。
mxe.git 位于 NTFS 文件系统（ntfs-3g），而临时目录在 ext4 文件系统时，编译没问题。

## WSL + VS

这是编译 Windows 版 ffmpeg 的较好的方法。

### 前置步骤

安装 WSL 。

打开 VS x86 命令窗口，执行 bash（WSL的）。

```
  export PREFIX=/mnt/e/project/codecbox.js.git/build/dist-native
  export PREFIX_W=e:/project/codecbox.js.git/build/dist-native
```
  
注意 PREFIX_W 是 windows 风格的路径，一些 windows 工具（如 cl.exe 编译器）在 WSL 中运行时也只认识这种路径，所以需要这个格式。
分隔符仍然要用 '/' 不是 '\'，否则编译系统的脚本会出问题。
  
cd $PREFIX/../

### x264
参考[1]，有修改

```
  cd $PREFIX/../x264
  CC=cl.exe LD=link.exe ./configure --host=mingw32 --enable-shared --disable-cli --extra-cflags="-DNO_PREFIX" --prefix=$PREFIX
  make -j4
  make install
```

注意要加上 --host=mingw32 来让编译系统知道编译目标是 win32（hack）。
指定VC的编译器和链接器（CC=cl.exe LD=link.exe，如果不够可能还要定义 AR=lib.exe NM=dumpbin.exe），注意 .exe 不能省略，这是 WSL 的特点。
  
### ffmpeg

如果上一步是编译了 x264 的 dll 版，则应该到 $PREFIX/lib 下将 libx264.dll.lib 复制/改名为 libx264.lib ，这是因为不知什么原因 ffmpge configure 使用 libx264.lib 的名字。

修改 configure:3919 ，给 cl、c99wrap、dumpbin、lib 加上 exe 后缀[13]。修改前：

```
        cl_major_ver=$(cl 2>&1 | sed -n 's/.*Version \([[:digit:]]\{1,\}\)\..*/\1/p')
        if [ -z "$cl_major_ver" ] || [ $cl_major_ver -ge 18 ]; then
            cc_default="cl"
            cxx_default="cl"
```

修改 compat\windows\mslink ，给 cl、link 加上 exe 后缀。修改 compat\windows\makedef，给dumpbin、lib 加上 exe 后缀[13]。

```
  export PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig
  cd $PREFIX/../ffmpeg
  
  ./configure --enable-cross-compile --arch=x86 --target-os=win32 --toolchain=msvc --extra-cflags=-I$PREFIX_W/include --extra-ldflags=-libpath:$PREFIX_W/lib  --prefix=$PREFIX --pkg-config=pkg-config --enable-shared --disable-static  --enable-ffplay --disable-ffprobe --disable-doc --enable-gpl --enable-libx264 --disable-encoders --enable-encoder=libx264,aac --disable-protocols --enable-protocol=file,pipe,http,rtmp,concat --disable-filters --enable-filter=anullsrc,scale --disable-muxers --enable-muxer=mp4,mpegts,adts,flv,wav,rawvideo
```
  
注意 --extra-cflags 和 --extra-ldflags 仍然是必要的，仅有 pkg-config 不能保证编译系统检测到 x264 库的存在，
这是因为 cl.exe 等 windows 工具不认识 linux 路径 /mnt/d/... ，需要指定对应的 windows 路径 e:/... 。
经测试，$INCLUDE 和 $LIB 似乎对 cl.exe 无效，所以只剩下 --extra-cflags 和 --extra-ldflags 的办法了。

注意 --extra-ldflags 是 -libpath:... 而不是与 gcc 兼容的 -L... 。

## WSL + mingw
（不推荐，建议用 WSL + VS）

[11,12]

apt install gcc-mingw-w64-i686 nasm yasm # nasm 和 yasm 用于编译 x264 和 ffmpeg 的汇编代码
不过，部分linux系统（ubuntu）的nasm版本可能过低，需要从源码编译安装。MXE 会编译一个最新的 nasm，可以拿来用。

如果是要编译64位 ffmpeg，则安装 gcc-mingw-w64-x86-64 。

### 编译 x264
```
./configure --host=mingw32 --cross-prefix=i686-w64-mingw32- --enable-shared --disable-cli --prefix=/mnt/e/project/codecbox.js.git/build/dist-wsl-mingw
make -j4
make install
```

注意 host 和 cross-prefix 对于在 linux 上交叉编译 mingw 是必要的，在 windows 上编译 mingw 则不必要。

### 编译 ffmpeg
```
  export PREFIX=/mnt/e/project/codecbox.js.git/build/dist-wsl-mingw
  export LIB=$PREFIX/lib:$LIB
  export INCLUDE=$PREFIX/include:$INCLUDE
  export PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig

  ./configure --arch=x86 --target-os=mingw32 --cross-prefix=i686-w64-mingw32- --extra-ldflags=-static-libgcc --pkg-config=pkg-config --prefix=$PREFIX  --enable-shared --disable-static --disable-ffplay --disable-ffprobe --disable-doc --enable-gpl --enable-libx264 --disable-encoders --enable-encoder=libx264,aac --disable-protocols --enable-protocol=file,pipe,http,rtmp,concat --disable-filters --enable-filter=anullsrc,scale --disable-muxers --enable-muxer=mp4,mpegts,adts,flv,wav,rawvideo
```

--pkg-config=pkg-config 是必要的，因为在指定 --cross-prefix 的情况下，默认也会给 pkg-config 命令加上前缀。

--extra-ldflags=-static-libgcc 用来消除对 mingw dll 的依赖（libgcc_s_sjlj-1.dll）（C++ 代码还要加上 -static-libstdc++）。
然而并没有效果，需要复制下面第一个文件到 ffmpeg 所在目录：
/usr/lib/gcc/i686-w64-mingw32/5.3-win32/libgcc_s_sjlj-1.dll
/usr/lib/gcc/i686-w64-mingw32/5.3-posix/libgcc_s_sjlj-1.dll

这可能是因为 -static-libgcc 与 ffmpeg 的 dll 编译模式不兼容，得使用 --enable-static --disable-shared 。

可能的 bsf：
--disable-bsfs  --enable-bsf=h264_mp4toannexb,h264_metadata,aac_adtstoasc

## 增加源文件

以在 avutil 中增加源文件 s.h 和 s.c 为例。
* 在 libavutil 目录下增加源文件 s.h 和 s.c。
* 在 libavutil/Makefile 的 OBJS = ... 后面增加 s.o ，在 HEADERS = ... 增加 s.h 。
* 如果源码中有函数需要从 dll/so 导出，有两个选择：（1）让函数名以 "av" 开头，这是默认导出的。（2）给函数名以其他前缀，并把前缀添加到 libavutil/libavutil.v 文件。
  改动之前是这样的：
```
  LIBAVUTIL_MAJOR {
    global:
        av*;
    local:
        *;
  };
```

  global表示导出的函数的前缀，在"av*"下面增加其他前缀即可。
  在 windows 编译目标上，导出函数表由 compat/windows/makedef 脚本生成  
