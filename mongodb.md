## 参考资料
[4.1] MongoDB Backup Methods
  https://www.mongodb.com/docs/manual/core/backups/

[4.2] How to Back Up and Restore MongoDB
  https://www.mongodb.com/basics/backup-and-restore

[4.3] mongodump
  https://www.mongodb.com/docs/database-tools/mongodump/
  
[4.4] mongodb-database-tools installation
  https://www.mongodb.com/docs/database-tools/installation/installation/
  https://www.mongodb.com/try/download/database-tools
  https://fastdl.mongodb.org/tools/db/mongodb-database-tools-debian10-x86_64-100.6.0.tgz
  
[4.5] Restore a MongoDB database(mongorestore)
  https://coderwall.com/p/3hx06a/restore-a-mongodb-database-mongorestore
  
[4.6] mongorestore
  https://www.mongodb.com/docs/database-tools/mongorestore/

## 客户端工具

从 [4.4] 下载 mongodb-database-tools-debian10-x86_64-100.6.0.tgz ，解压，有 mongodump 和 mongorestore 等工具。

## 备份

备份 mongodb 有3种方式：
1. 备份整个 mongodb 数据目录。这要求此数据库处于关闭状态，否则会备份出不一致的数据。
2. 用 mongodb 的 mongodump 工具导出数据（到目录或压缩包）。这要求此数据库处于运行的状态，并且数据库可以处于网络上。
3. 用 MongoDB Atlas 备份，即利用云存储的文件系统快照机制备份。

下面介绍一下 mongodump 的备份方式：

无论是备份还是恢复，都应该先启动 mongod，打开数据库。如果可能，不要让其它可能修改它的客户端程序连接，以最大限度地避免干扰，尤其是恢复备份时。
可以用此方式单独启动 fiftyone 的 mongodb：

```
mongod --dbpath $HOME/.fiftyone/var/lib/mongo --logpath $HOME/.fiftyone/var/lib/mongo/log/mongo.log --port 0 --nounixsocket
```

启动mongodb后，要确定 mongod 的监听端口：
```
netstat -lnp | grep mongo
tcp        0      0 127.0.0.1:42893         0.0.0.0:*               LISTEN      737155/mongod
```
导出到一个压缩包：
```
cd /media/data2/annotation/fiftyone_bk
mongodump --uri=mongodb://127.0.0.1:42893 --archive=fo_20221020_1.gz --gzip
```
如果 `--uri=xxx` 后面还有其它参数，就不要把`=`用空格代替，否则 mongodump 会报告解析错误。

也可以导出为目录（在当前目录下生成 `dump/` 子目录，里面为 mongodb 的备份文件）：
```
cd /media/data2/annotation/fiftyone_bk
mongodump --uri mongodb://127.0.0.1:42893
```

恢复：
```
cd /media/data2/annotation/fiftyone_bk
mongorestore  --uri=mongodb://127.0.0.1:42893 dump/ --drop
mongorestore  --uri=mongodb://127.0.0.1:42893 --archive=fo_20221020_1.gz --gzip --drop
```
加入 --drop 参数通常是必要的，因为 mongorestore 默认不删除/覆盖同名的旧数据，而--drop 参数要求它删除同名的旧数据[4.5]。
