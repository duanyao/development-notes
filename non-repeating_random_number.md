# non-repeating random number (不重复的随机数发生)

## 参考资料
[1] https://stackoverflow.com/questions/196017/unique-non-repeating-random-numbers-in-o1

[1.1] https://stackoverflow.com/a/16097246


[2.1] FPE https://en.wikipedia.org/wiki/Format-preserving_encryption

[2.2] FFX https://csrc.nist.gov/csrc/media/projects/block-cipher-techniques/documents/bcm/proposed-modes/ffx/ffx-spec.pdf

[2.3] FPE Java Lib https://sourceforge.net/projects/format-preserving-encryption/

## 序列号加密算法
参考[1.1]。

简单来说，就是对称加密一个自增序列号，结果就是不重复的随机数序列。原理在于，不同的明文经过相同的加密算法和秘钥加密后，必然是不同的。

正式定义：

随机数发生器的状态：一个自增序列号 n，一个对称加密秘钥 k，两者都需要保密和持久化；一个对称加密算法 `e(秘钥, 明文): 密文`。
生成随机数的算法：
```
n[i+1] = n[i] + 1; // i = 0,1,2...; 
r[i] = e(k, n[i]); // r[i] 就是生成的第 i 个随机数。
```

### 加密块的大小问题
常用的块加密算法，明文和密文块的大小是算法定死的，如 AES 是 128 位，这样就不能调节生成的随机数的位宽（或者说值域）。
为了解决这个问题，可以应用 format-preserving encryption[2.1], 例如 AES-FFX[2.1, 2.2, 2.3]。流式密码应该并不适用。

### 更换秘钥的问题
如果更换秘钥，则不能保证以后生成的随机数不与以前生成的相冲突。解决方案有：
* 召回随机数。如果可以修改以前生成的随机数（如都在一个数据库里），则可以先将以前生成的随机数解密，用新秘钥加密，再写入数据库。
* 预留版本号空间。假如随机数的位宽是 64，那么可以保留 8 位作为版本号，每次变更秘钥或加密算法时递增，其余 56 位才是生成的随机数。这样，允许变更秘钥或加密算法 256 次，不会冲突。
  当然，如果秘钥泄露不召回随机数，只是新秘钥，则以前的随机数是可以被攻击者还原为自增序列号的。

### 分布式节点问题
如果多个节点都要生成随机数，且互相不能冲突，也不能承受同步开销，那么可以将自增序列号 n 按节点进一步划分命名空间。例如 56 位的序列号中，前16位用作节点编号，后40位在每个节点上自增。

### 不同单位的数据库合并问题
这可以归结为更换秘钥的问题。不同单位选择的秘钥是不同的，但秘钥编号可能冲突，可以重新分配秘钥编号，或者召回。

### 随机数升位问题
随着数据规模的扩大，随机数的位宽可能需要增长。
这可以归结为更换秘钥的问题。
