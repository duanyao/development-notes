## 参考资料

[4.1] Installing a custom Python version into a Docker image.
http://blog.dscpl.com.au/2015/06/installing-custom-python-version-into.html

[4.2] Instrumenting CPython with DTrace and SystemTap
https://docs.python.org/3/howto/instrumentation.html

[5.1] How to Install Python 3.7 on Debian 9
https://linuxize.com/post/how-to-install-python-3-7-on-debian-9/

[5.2] Difference in details between “make install” and “make altinstall”
https://stackoverflow.com/questions/16018463/difference-in-details-between-make-install-and-make-altinstall

[5.3] Compiling Python with Checkinstall
https://stackoverflow.com/questions/40434550/compiling-python-with-checkinstall

[5.4] Python 3.5 - missing libpython3.5m.so (shared library)
https://github.com/travis-ci/travis-ci/issues/4834

[5.5] pbuilder使用
https://www.cnblogs.com/taosim/articles/3223135.html

[5.6] How to create debian package from source
https://coderwall.com/p/urkybq/how-to-create-debian-package-from-source

[5.7] No module named '_bz2' in python3
https://stackoverflow.com/questions/50335503/no-module-named-bz2-in-python3

[5.8] dh_usrlocal: … is not a directory
https://unix.stackexchange.com/questions/409800/dh-usrlocal-is-not-a-directory



## 编译环境需求
```
python版本  debian版本  支持编译  不支持的原因
3.11        10          ?
3.11        11          y
3.13        11          n       需要 autoconf>=2.71，但系统只有 autoconf=2.69
3.13        12          y
```

## 编译选项

### 最简编译
在 python 源码目录下执行：
```
./configure
make -j4
sudo make install
```
### configure 选项解释
参考 [4.1-4.2] 。

`--enable-shared` 创建 python 解释器的动态库 libpythonX.Y.so，python 可执行程序将会动态链接到它。默认不启用，不创建 python 解释器的动态库 libpythonX.Y.so，python 可执行程序是静态链接的。一些第三方程序（如 apache web 服务器）会利用动态库嵌入 python 解释器，所以最好打开。debian 系统上的 python 实际上是动态/静态连接复合的（做了两次构建），python 可执行程序是静态的，但同时也提供 libpythonX.Y.so 动态库[4.1]。静态连接的 python 可执行程序据说有一些性能优势。
`--prefix` 安装位置（.py 等文件），默认是 `/usr/local`。
`--exec-prefix` 安装位置（可执行文件、共享库等），默认同 `--prefix`，但如果指定了 `--prefix` ，也最好同时指定 `--exec-prefix`。
`--libdir` 库文件的安装位置，默认是 `<exec-prefix>/lib`，但在多架构系统上可能最好改为形如 `/usr/lib/x86_64-linux-gnu`。
`--without-static-libpython` 不构建 libpythonX.Y.a 库。默认构建 .a 库。
`--enable-loadable-sqlite-extensions` 启用 sqlite 的模块，默认 no。
`--with-dtrace` 启用 DTrace 或 SystemTap 。linux 上需要安装了 systemtap-sdt-dev 或 systemtap-sdt-devel 包[4.2]。
`--enable-optimizations` 使用 PGO 来优化编译，这会导致编译过程很慢。默认no。
`--with-system-expat[=no]` python 源码包里自带了 expat 的源码（Modules/expat/），编译时可以使用它们，也可以使用操作系统中的 expat 开发包[4.1]。默认 no 。
`--with-system-ffi[=no]` : python 源码包里自带了 ffi 的源码（Modules/_ctypes），编译时可以使用它们，也可以使用操作系统中的 ffi 开发包。默认值依赖于操作系统类型，linux 上似乎一般是yes。

在 debian 12 上编译 python 3.13 并启用 `--with-dtrace` ，会出现错误 `dtrace invalid option -frelease` ，原因不明。

### 查看已有的 python 是怎样构建的
debian 系统中，deb 包有时包含构建时用的 Makefile ，例如 python3.12 deb 包构建时用的 Makefile 在 /usr/lib/python3.12/config-3.12-x86_64-linux-gnu/Makefile ，其中的 configure 选项也记录在里面了：
```
# configure script arguments
CONFIG_ARGS=     '--enable-shared' '--prefix=/usr' '--libdir=/usr/lib/x86_64-linux-gnu' '--enable-ipv6' '--enable-loadable-sqlite-extensions' '--with-dbmliborder=bdb:gdbm' '--with-computed-gotos' '--without-ensurepip' '--with-system-expat' '--with-dtrace' '--with-wheel-pkg-dir=/usr/share/python-wheels/' '--with-ssl-default-suites=openssl' 'MKDIR_P=/bin/mkdir -p' '--with-system-ffi' 'CC=x86_64-linux-gnu-gcc' 'CFLAGS=-g   -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -fcf-protection ' 'LDFLAGS=  -g -fwrapv -O2   ' 'CPPFLAGS=-Wdate-time -D_FORTIFY_SOURCE=2'
```

此外，python 还提供了 API 查看构建选项：
```
python3.12
Python 3.12.4 (main, Jul  9 2024, 09:31:23) [GCC 13.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> from distutils.sysconfig import get_config_var
>>> get_config_var('CONFIG_ARGS')
"'--enable-shared' '--prefix=/usr' '--libdir=/usr/lib/x86_64-linux-gnu' '--enable-ipv6' '--enable-loadable-sqlite-extensions' '--with-dbmliborder=bdb:gdbm' '--with-computed-gotos' '--without-ensurepip' '--with-system-expat' '--with-dtrace' '--with-wheel-pkg-dir=/usr/share/python-wheels/' '--with-ssl-default-suites=openssl' 'MKDIR_P=/bin/mkdir -p' '--with-system-ffi' 'CC=x86_64-linux-gnu-gcc' 'CFLAGS=-g   -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -fcf-protection ' 'LDFLAGS=  -g -fwrapv -O2   ' 'CPPFLAGS=-Wdate-time -D_FORTIFY_SOURCE=2'"
```

### pyc 文件的安装
默认会将标准库的 .py 文件编译为 .pyc 文件并安装。这会提高 python 启动速度，但会占用磁盘空间。没有选项可以禁止编译为 .pyc 文件，可以安装后再删除：
```
find <prefix/lib/pythonX.Y> -type d -name __pycache__ -exec rm {} \;
```

## 从源码安装（debian, make altinstall ）

参考 [5.1] 。

```
wget https://www.python.org/ftp/python/3.7.6/Python-3.7.6.tgz
tar -xf Python-3.7.6.tgz
sudo apt install build-essential zlib1g-dev libbz2-dev liblzma-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev libsqlite3-dev tcl-dev tk-dev systemtap-sdt-dev
cd Python-3.7.6
./configure --enable-shared --enable-loadable-sqlite-extensions --disable-optimizations # --enable-optimizations
make -j4
sudo make altinstall
sudo ldconfig -v

python3.7 --version
pip3.7 --version
```

altinstall 与 install 的区别是：不会创建不带小版本号的符号链接，如 /usr/local/bin 下只会有 python3.7 和 pip3.7 ，不会有 python3, pip3, python, pip [5.2].

默认安装位置是：/usr/local 。

`--enable-shared` 将会创建 python 的动态库 libpython3.7m.so ，很多第三方程序会链接它，所以最好打开。
`--enable-optimizations` 使用 PGO 来优化编译，这会导致编译过程很慢。

不装 libbz2-dev 也可以编译成功，但是内置模块 bz2 将无法成功导入（ `ModuleNotFoundError: No module named '_bz2'` ）[5.7]。类似的，liblzma-dev 是 lzma 模块所需。

不装 tcl-dev tk-dev 也可以编译成功，但内置模块 `_tkinter` （GUI工具）将无法成功导入（ `ModuleNotFoundError: No module named '_tkinter'`)。

安装 libsqlite3-dev 并使用 --enable-loadable-sqlite-extensions 来启用内置模块 sqlite3 。

### checkinstall

如果要创建 deb 包，用以下语句代替 `sudo make altinstall` （checkinstall 工具可以用 apt 安装）：

```
$ sudo make -n altinstall > altinstall_script.sh
$ chmod +x altinstall_script.sh
$ sudo checkinstall ./altinstall_script.sh
```

Name 和 Provides 都改为 python3.7-checkinstall

checkinstall 对于 python 运行得很慢，而且覆盖不完全，因此不推荐。

## 从源码手工生成 debian control 文件

https://www.man7.org/linux/man-pages/man5/deb-src-control.5.html
https://www.man7.org/linux/man-pages/man5/deb-control.5.html

安装编译依赖：
```
sudo apt install fakeroot dh-make build-essential zlib1g-dev libbz2-dev liblzma-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev libsqlite3-dev tcl-dev tk-dev autoconf-archive
```

下载源码：

```
mkdir python
cd python
apt source python3.9
```
如果 /etc/apt/sources.list 中的 deb-src 行处于注释的状态，需要反注释。
如果无此版本的python，可能需要添加更新或更旧的系统版本的源。这种源只需要保留其中的 deb-src 行，注释 deb 行，这样可降低冲突的概率。
例如，ubuntu23.04只有python3.11，而ubuntu20.04有python3.9，要下载3.9，可以新建 /etc/apt/sources.list.d/ubuntu2004-src.list 文件，内容为：
```
# deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal main restricted universe multiverse
deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal main restricted universe multiverse
# deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-updates main restricted universe multiverse
deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-updates main restricted universe multiverse
# deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-backports main restricted universe multiverse
deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-backports main restricted universe multiverse
# deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-security main restricted universe multiverse
deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-security main restricted universe multiverse
```

dedian 系列可以使用 dedian sid 源。

下载了以下文件：
```
python3.9_3.9.2.orig.tar.xz      19149464
python3.9_3.9.2-1.debian.tar.xz    212196
python3.9_3.9.2-1.dsc
```
apt source 自动解压生成：

```
python3.9_3.9.2/
  debian/          # 来自 python3.9_3.9.2-1.debian.tar.xz
```

重命名：

```
mv python3.9_3.9.2.orig.tar.xz python3.9-all-in-one-3.9.2.tar.xz
mv python3.9_3.9.2 python3.9-3.9.2.debian1  # 之后就没什么用了

tar xvJf python3.9-all-in-one-3.9.2.tar.xz # 解压生成 Python-3.9.2 目录
mv Python-3.9.2 python3.9-all-in-one-3.9.2
```
注意目录名 python3.9-all-in-one-3.9.2 中绝对不要写下划线，dh_make 不认，尽管文档中说可以用 -p <package name> 来使用下划线模式。
最后一个减号之后的部分是版本号，之前是包名。

用 dh_make 从源码生成 debian 标准的源码包：

```
cd python3.9-all-in-one-3.9.2
dh_make -f ../python3.9-all-in-one-3.9.0~rc1.tar.xz

Type of package: (single, indep, library, python)
[s/i/l/p]?sY
Maintainer Name     : unknown
Email-Address       : duanyao@duanyao-laptop-c
Date                : Sat, 22 Aug 2020 06:18:27 +0800
Package Name        : python3.9-all-in-one
Version             : 3.9.0~rc1
License             : blank
Package Type        : single
Are the details correct? [Y/n/q]
```

生成了 python3.9-all-in-one_3.9.0~rc1.orig.tar.xz

debian/control 修改为：
```
Source: python3.9-all-in-one
Section: python
Priority: optional
Maintainer: duanyao <duanyao@duanyao-laptop-c>
Build-Depends: debhelper (>= 11), autotools-dev, build-essential, zlib1g-dev, libbz2-dev, liblzma-dev, libncurses5-dev, libgdbm-dev, libnss3-dev, libssl-dev, libreadline-dev, libffi-dev, libsqlite3-dev, tcl-dev, tk-dev
Standards-Version: 4.1.3
Homepage: https://duanyao.my/debian/python3.9-all-in-one
#Vcs-Browser: https://salsa.debian.org/debian/python3.9-all-in-one
#Vcs-Git: https://salsa.debian.org/debian/python3.9-all-in-one.git

Package: python3.9-all-in-one
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: python3.9, python3.9-dev, python3.9-venv, libpython3.9, libpython3.9-dev, libpython3.9-stdlib, libpython3.9-testsuite, python3.9-minimal, libpython3.9-minimal
Provides: python3.9, python3.9-dev, python3.9-venv, libpython3.9, libpython3.9-dev, libpython3.9-stdlib, libpython3.9-testsuite, python3.9-minimal, libpython3.9-minimal
Description: python 3.9 all in one package
 Python is a high-level, interactive, object-oriented language. Its 3.9 version
 includes an extensive class library with lots of goodies for
 network programming, system administration, sounds and graphics.
```
debian/changelog 修改为：
```
python3.9-all-in-one (3.9.0~rc1-1) unstable; urgency=medium

  * Initial release

 -- duanyao <duanyao@duanyao-laptop-c>  Sat, 22 Aug 2020 06:18:27 +0800
```
debian/rules 追加：

```
tmp_install_dir=debian/python3.9-all-in-one
prefix=/usr
#DEB_HOST_MULTIARCH=$(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
#tmp_lib_dir=$(tmp_install_dir)/$(prefix)/lib/$(DEB_HOST_MULTIARCH)
tmp_lib_dir=$(tmp_install_dir)/$(prefix)/lib

override_dh_auto_configure:
	#dh_auto_configure -- --enable-shared --enable-loadable-sqlite-extensions --prefix=$(prefix) --exec_prefix=$(prefix)
	./configure --enable-shared --enable-loadable-sqlite-extensions --prefix=$(prefix) --exec_prefix=$(prefix)

#override_dh_auto_clean:
#	$(MAKE) clean

override_dh_auto_install:
	$(MAKE) altinstall DESTDIR=$(tmp_install_dir) prefix=$(prefix)
	mv $(tmp_lib_dir)/libpython3.so $(tmp_lib_dir)/libpython3.so.3.9
```

注解：
prefix=/usr 是安装位置。改成 /usr/local 会导致编译错误（ dh_usrlocal: is not a directory，见后面），所以不要这样做 。
将 `--enable-shared --enable-loadable-sqlite-extensions` 参数追加到 configure 脚本的参数中。
其它 configure 参数由 dh_auto_configure 脚本自动添加，但如果用 ./configure 取代 dh_auto_configure -- ，就只会有自行指定的参数了。
为了多版本共存，不能使用默认的 make install，而是要用 make altinstall 。这时需要自行指定 DESTDIR 和 prefix 。
`libpython3.so` 这个文件名会导致无法多版本共存，故而待安装完成后将其改为带小版本号的名字：libpython3.so.3.9 。

DEB_HOST_MULTIARCH 的值形如 x86_64-linux-gnu ，但设置 tmp_lib_dir=$(tmp_install_dir)/$(prefix)/lib/$(DEB_HOST_MULTIARCH) 并不成功，编译产物的位置（如）似乎会忽略它，因此设置 tmp_lib_dir=$(tmp_install_dir)/$(prefix)/lib 。可能需要设置 `--libdir=/usr/lib/$(DEB_HOST_MULTIARCH)`

设置编译选项：
```
export DEB_BUILD_OPTIONS="nocheck nobech nopgo"
```
nocheck: 不跑测试
nobech: 不跑性能测试
nopgo: 不做 profile guided optimizition

生成 configure 文件：
```
autoreconf
```
编译：
```
dpkg-buildpackage --no-sign --jobs=4 --no-pre-clean
```
因为第一次编译时，Makefile 还不存在，所以无法 clean，要加上 --no-pre-clean 选项；以后的编译可以不加 --no-pre-clean ，如果想从头来一遍。
如果成功，会生成 python3.9-all-in-one_3.9.0~rc1-1_amd64.deb 文件，可以查看其内容是否正确：

```
dpkg-deb -c ../python3.9-all-in-one_3.9.2-1_amd64.deb  # 列出包含的文件
dpkg-deb -I ../python3.9-all-in-one_3.9.2-1_amd64.deb
```

#### 问题 dh_usrlocal: is not a directory
dpkg-buildpackage --no-sign --jobs=4 输出错误：
```
dh_usrlocal: debian/python3.9-all-in-one/usr/local/bin/2to3-3.9 is not a directory
make: *** [debian/rules:18：binary] 错误 25
```
参考 [5.8]，这是因为 dh 默认不支持安装到 /usr/local ，debian/rules 中 prefix 应改为 /usr 。

#### 问题：Could not find platform dependent libraries <exec_prefix> ；无法加载本地库

执行 python3.9，头两行输出警告：
```
Could not find platform dependent libraries <exec_prefix>
Consider setting $PYTHONHOME to <prefix>[:<exec_prefix>]
```

打印 sys.exec_prefix 和 sys.prefix 变量的结果都是 `/usr`，这理论上应该是正确的。
但是，本地库实际上的位置是 `/usr/lib/x86_64-linux-gnu/python3.9/lib-dynload/` ，而非传统的
`/usr/lib/pythonX.Y/lib-dynload/`（不存在），所以本地库如 math 等都无法加载。

网上未能找到相关答案。搜索 python3.9 源码，找到：
```
grep -Irn "Could not find platform dependent libraries" *
Modules/getpath.c:810:                "Could not find platform dependent libraries <exec_prefix>\n");
```

根据 Modules/getpath.c 的注释，python3.9 查找 sys.exec_prefix 的方法并没有变，因此本地库还是应该放在 `/usr/lib/python3.9/lib-dynload/` 目录下。

查看 python3.9-all-in-one-3.9.0~rc1/config.log 中相关部分：

```
./configure --build=x86_64-linux-gnu --prefix=/usr --includedir=${prefix}/include --mandir=${prefix}/share/man --infodir=${prefix}/share/info --sysconfdir=/etc --localstatedir=/var --disable-silent-rules --libdir=${prefix}/lib/x86_64-linux-gnu --libexecdir=${prefix}/lib/x86_64-linux-gnu --runstatedir=/run --disable-maintainer-mode --disable-dependency-tracking --enable-shared --enable-loadable-sqlite-extensions --prefix=/usr --exec_prefix=/usr

bindir='${exec_prefix}/bin'
build='x86_64-pc-linux-gnu'
build_alias='x86_64-linux-gnu'
build_cpu='x86_64'
build_os='linux-gnu'
build_vendor='pc'
datadir='${datarootdir}'
datarootdir='${prefix}/share'
docdir='${datarootdir}/doc/${PACKAGE_TARNAME}'
dvidir='${docdir}'
exec_prefix='/usr'
host='x86_64-pc-linux-gnu'
host_alias=''
host_cpu='x86_64'
host_os='linux-gnu'
host_vendor='pc'
htmldir='${docdir}'
includedir='${prefix}/include'
infodir='${prefix}/share/info'
libdir='${prefix}/lib/x86_64-linux-gnu'
libexecdir='${prefix}/lib/x86_64-linux-gnu'
localedir='${datarootdir}/locale'
localstatedir='/var'
mandir='${prefix}/share/man'
oldincludedir='/usr/include'
pdfdir='${docdir}'
prefix='/usr'
program_transform_name='s,x,x,'
psdir='${docdir}'
runstatedir='/run'
sbindir='${exec_prefix}/sbin'
sharedstatedir='${prefix}/com'
sysconfdir='/etc'
```

我仅仅指定了 `--enable-shared --enable-loadable-sqlite-extensions --prefix=$(prefix) --exec_prefix=$(prefix)` 其它参数可能是 dh_auto_configure 自己加的。

对比了编译 python 3.7.6 时的 config.log 发现确实有些差异：

```
./configure --enable-shared --enable-loadable-sqlite-extensions

bindir='${exec_prefix}/bin'
build='x86_64-pc-linux-gnu'
build_alias=''
build_cpu='x86_64'
build_os='linux-gnu'
build_vendor='pc'
datadir='${datarootdir}'
datarootdir='${prefix}/share'
docdir='${datarootdir}/doc/${PACKAGE_TARNAME}'
dvidir='${docdir}'
exec_prefix='${prefix}'
host='x86_64-pc-linux-gnu'
host_alias=''
host_cpu='x86_64'
host_os='linux-gnu'
host_vendor='pc'
htmldir='${docdir}'
includedir='${prefix}/include'
infodir='${datarootdir}/info'
libdir='${exec_prefix}/lib'
libexecdir='${exec_prefix}/libexec'
localedir='${datarootdir}/locale'
localstatedir='${prefix}/var'
mandir='${datarootdir}/man'
oldincludedir='/usr/include'
pdfdir='${docdir}'
prefix='/usr/local'
program_transform_name='s,x,x,'
psdir='${docdir}'
sbindir='${exec_prefix}/sbin'
sharedstatedir='${prefix}/com'
sysconfdir='${prefix}/etc'
```

差别在于 libdir 和 libexecdir 多了 x86_64-linux-gnu 部分，造成了问题。
修改 rules 文件，用 ./configure 取代 dh_auto_configure -- ，不再指定 libdir 和 libexecdir，就解决了问题。

#### 问题 error: Please install autoconf-archive package and re-run autoreconf

执行 dpkg-buildpackage --no-sign --jobs=4 --no-pre-clean 可能报错：

```
configure: error: Please install autoconf-archive package and re-run autoreconf
```
发现 autoconf-archive 的确没安装。

```
sudo install autoconf-archive
autoreconf
```
解决了问题。

### 从 debian source 创建 deb (new 2, 部分失败)

```
~/t-project/deb-src$ apt source python3.9

正在读取软件包列表... 完成
提示：python3.9 的打包工作被维护于以下位置的 Git 版本控制系统中：
https://salsa.debian.org/cpython-team/python3.git
请使用：
git clone https://salsa.debian.org/cpython-team/python3.git
获得该软件包的最近更新(可能尚未正式发布)。
需要下载 19.0 MB 的源代码包。
获取:1 https://mirrors.tuna.tsinghua.edu.cn/debian sid/main python3.9 3.9.0~rc1-1 (dsc) [3,317 B]
获取:2 https://mirrors.tuna.tsinghua.edu.cn/debian sid/main python3.9 3.9.0~rc1-1 (tar) [18.8 MB]
获取:3 https://mirrors.tuna.tsinghua.edu.cn/debian sid/main python3.9 3.9.0~rc1-1 (diff) [209 kB]                                              
已下载 19.0 MB，耗时 7秒 (2,700 kB/s)                                                                                                          
dpkg-source: info: extracting python3.9 in python3.9-3.9.0~rc1
dpkg-source: info: unpacking python3.9_3.9.0~rc1.orig.tar.xz
dpkg-source: info: unpacking python3.9_3.9.0~rc1-1.debian.tar.xz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: applying deb-setup.diff
dpkg-source: info: applying deb-locations.diff
dpkg-source: info: applying distutils-install-layout.diff
dpkg-source: info: applying locale-module.diff
dpkg-source: info: applying distutils-link.diff
dpkg-source: info: applying distutils-sysconfig.diff
dpkg-source: info: applying tkinter-import.diff
dpkg-source: info: applying gdbm-import.diff
dpkg-source: info: applying link-opt.diff
dpkg-source: info: applying setup-modules.diff
dpkg-source: info: applying bdist-wininst-notfound.diff
dpkg-source: info: applying profiled-build.diff
dpkg-source: info: applying langpack-gettext.diff
dpkg-source: info: applying disable-sem-check.diff
dpkg-source: info: applying lib-argparse.diff
dpkg-source: info: applying ctypes-arm.diff
dpkg-source: info: applying multiarch.diff
dpkg-source: info: applying lib2to3-no-pickled-grammar.diff
dpkg-source: info: applying ext-no-libpython-link.diff
dpkg-source: info: applying test-no-random-order.diff
dpkg-source: info: applying multiarch-extname.diff
dpkg-source: info: applying tempfile-minimal.diff
dpkg-source: info: applying disable-some-tests.diff
dpkg-source: info: applying ensurepip-wheels.diff
dpkg-source: info: applying ensurepip-disabled.diff
dpkg-source: info: applying mangle-fstack-protector.diff
dpkg-source: info: applying reproducible-buildinfo.diff
dpkg-source: info: applying pydoc-use-pager.diff
dpkg-source: info: applying local-doc-references.diff
dpkg-source: info: applying doc-build-texinfo.diff
dpkg-source: info: applying build-math-object.diff
dpkg-source: info: applying argparse-no-shutil.diff
dpkg-source: info: applying arm-alignment.diff
dpkg-source: info: applying sysconfigdata-name.diff
dpkg-source: info: applying hurd_kfreebsd_thread_native_id.diff
dpkg-source: info: applying sphinx3.diff

~/t-project/deb-src$ cd python3.9-3.9.0~rc1
~/t-project/deb-src/python3.9-3.9.0~rc1$ ls
aclocal.m4          config.sub    debian   Include     LICENSE  Makefile.pre.in  netlify.toml  PC        pyconfig.h.in  setup.py
CODE_OF_CONDUCT.md  configure     Doc      install-sh  m4       Misc             Objects       PCbuild   Python         Tools
config.guess        configure.ac  Grammar  Lib         Mac      Modules          Parser        Programs  README.rst

~/t-project/deb-src$ sudo mk-build-deps --install --remove
```

```
dpkg-buildpackage --no-check-builddeps --no-sign --jobs=4
dpkg-buildpackage --no-check-builddeps --no-sign --jobs=4 --no-pre-clean
```

错误：

```
/bin/sh: 1: sphinx-build: not found
make[1]: *** [Makefile:52: build] Error 127
make[1]: Leaving directory '/home/duanyao/t-project/deb-src/python3.9-3.9.0~rc1/Doc'
make: *** [debian/rules:697：stamps/stamp-doc-html] 错误 2
dpkg-buildpackage: error: fakeroot debian/rules binary subprocess returned exit status 2
```

```
apt-file search sphinx-build # 找到 python3-sphinx
sudo apt install python3-sphinx
```

错误：

```
/usr/bin/make -C Doc html
make[1]: Entering directory '/home/duanyao/t-project/deb-src/python3.9-3.9.0~rc1/Doc'
Makefile:101: warning: overriding recipe for target 'texinfo'
Makefile:92: warning: ignoring old recipe for target 'texinfo'
mkdir -p build
Using existing Misc/NEWS file
PATH=./venv/bin:$PATH sphinx-build -b html -d build/doctrees  -D today='August 12, 2020' -D html_last_updated_fmt='August 12, 2020'  . build/html 
Running Sphinx v1.8.4
making output directory...

Theme error:
no theme named 'python_docs_theme' found (missing theme.conf?)
make[1]: *** [Makefile:52: build] Error 2
make[1]: Leaving directory '/home/duanyao/t-project/deb-src/python3.9-3.9.0~rc1/Doc'
make: *** [debian/rules:697：stamps/stamp-doc-html] 错误 2
dpkg-buildpackage: error: fakeroot debian/rules binary subprocess returned exit status 2
```
网上搜索发现这个错误是因为系统 python3 缺少包 python-docs-theme 。这个包在 apt 中没有，需要用 pip 安装：

```
sudo pip3 install python-docs-theme
```
继续运行 `dpkg-buildpackage --no-check-builddeps --no-sign --jobs=4 --no-pre-clean`，成功。

存在的问题见“从 debian source 创建 deb：pip 和 python3-distutils 问题”。

### 从 debian source 创建 deb (快速，部分失败)

```
cd ~/t-project/deb-src/python3.9-3.9.0~rc1
sudo mk-build-deps --install --remove
rm python3.9-build-deps_3.9.0~rc1-1_amd64.deb

export DEB_BUILD_OPTIONS="nocheck nobech nopgo"
dpkg-buildpackage --no-check-builddeps --no-sign --jobs=4
dpkg-buildpackage --no-check-builddeps --no-sign --jobs=4 --no-pre-clean
```
nocheck: 不跑测试
nobech: 不跑性能测试
nopgo: 不做 profile guided optimizition

双核机器用时大约25min。

存在的问题见“从 debian source 创建 deb：pip 和 python3-distutils 问题”。

### 从 debian source 创建 deb：pip 和 python3-distutils 问题

从 debian source 创建 python3.9 deb 后，运行 python3.9 -m pip 出错：

```
File "/usr/lib/python3/dist-packages/pip/_internal/locations.py", line 10, in <module>
    from distutils import sysconfig as distutils_sysconfig
ImportError: cannot import name 'sysconfig' from 'distutils' (/usr/lib/python3.9/distutils/__init__.py)
```

检查系统 python (3.7) 的 sysconfig 的文件和对应的 deb 包，得到：

python3-distutils: /usr/lib/python3.7/distutils/sysconfig.py

而 python3.9 编译后并没有对应的 python3-distutils deb 包，所以出错了。
python3-distutils 的命名也有点问题，因为没有指定小版本。ubuntu 似乎已经改名为 python3.7-distutils, python3.9-distutils 等。

在源码中查找此文件：

```
~/t-project/deb-src/python3.9-3.9.0~rc1$ find -name sysconfig.py
./Lib/distutils/sysconfig.py
./Lib/sysconfig.py
./debian/libpython3.9-minimal/usr/lib/python3.9/sysconfig.py
./debian/tmp-dbg/usr/lib/python3.9/distutils/sysconfig.py
./debian/tmp-dbg/usr/lib/python3.9/sysconfig.py
./.pc/distutils-install-layout.diff/Lib/distutils/sysconfig.py
./.pc/distutils-sysconfig.diff/Lib/distutils/sysconfig.py
./.pc/multiarch.diff/Lib/sysconfig.py
./.pc/multiarch.diff/Lib/distutils/sysconfig.py
./.pc/mangle-fstack-protector.diff/Lib/distutils/sysconfig.py
./.pc/sysconfigdata-name.diff/Lib/sysconfig.py
./.pc/sysconfigdata-name.diff/Lib/distutils/sysconfig.py
```

查询 debian 网站得知，python3-distutils 的源码包是 python3-stdlib-extensions
( https://packages.debian.org/source/sid/python3-stdlib-extensions )

apt source python3-stdlib-extensions
cd python3-stdlib-extensions-3.8.5

此源码实际上同时支持多个版本的 python ，从 3.6 到 3.9 。默认编译 3.8 和 3.9 。
因为我的机器上没装 3.8，因此要跳过这个版本。编辑 /home/duanyao/t-project/deb-src/python3-stdlib-extensions-3.8.5/debian/rules:17 ，
`PYVERS	= 3.8 3.9` 改为 `PYVERS	= 3.9` 。执行：

```
sudo mk-build-deps --install --remove
rm -f *.deb

dpkg-buildpackage --no-check-builddeps --no-sign --jobs=4
```
错误：
```
/bin/bash: python3.9-dbg：未找到命令
make: *** [debian/rules:61：dbg-stamp-py3.9] 错误 127
```

安装 `sudo apt install python3.9-dbg`，继续运行 `dpkg-buildpackage --no-check-builddeps --no-sign --jobs=4 --no-pre-clean`。

/bin/bash: rdfind：未找到命令
make: *** [debian/rules:77：install] 错误 127

sudo apt install rdfind

### 从 debian source 创建 deb (new git，失败)
```
~/t-project/deb-src$ apt source python3
正在读取软件包列表... 完成
选择 python3-defaults 作为源代码包而非 python3
提示：python3-defaults 的打包工作被维护于以下位置的 Git 版本控制系统中：
https://salsa.debian.org/cpython-team/python3-defaults.git
请使用：
git clone https://salsa.debian.org/cpython-team/python3-defaults.git
获得该软件包的最近更新(可能尚未正式发布)。
需要下载 142 kB 的源代码包。
获取:1 https://mirrors.tuna.tsinghua.edu.cn/debian sid/main python3-defaults 3.8.2-3 (dsc) [2,797 B]
获取:2 https://mirrors.tuna.tsinghua.edu.cn/debian sid/main python3-defaults 3.8.2-3 (tar) [139 kB]
已下载 142 kB，耗时 1秒 (252 kB/s)    
dpkg-source: info: extracting python3-defaults in python3-defaults-3.8.2
dpkg-source: info: unpacking python3-defaults_3.8.2-3.tar.gz
```

```
~/t-project/deb-src$ git clone https://salsa.debian.org/cpython-team/python3-defaults.git python3-defaults.git
正克隆到 'python3-defaults.git'...
remote: Enumerating objects: 2578, done.
remote: Counting objects: 100% (2578/2578), done.
remote: Compressing objects: 100% (1033/1033), done.
remote: Total 2578 (delta 1794), reused 2294 (delta 1516), pack-reused 0
接收对象中: 100% (2578/2578), 2.35 MiB | 7.00 KiB/s, 完成.
处理 delta 中: 100% (1794/1794), 完成.

~/t-project/deb-src$ cd python3-defaults.git/
```

```
~/t-project/deb-src/python3-defaults.git$ sudo mk-build-deps --install --remove
[sudo] duanyao 的密码：
dh_testdir
dh_testroot
dh_prep
dh_testdir
dh_testroot
dh_install
dh_installdocs
dh_installchangelogs
dh_compress
dh_fixperms
dh_installdeb
dh_gencontrol
dh_md5sums
dh_builddeb
dpkg-deb: 正在 '../python3-defaults-build-deps_3.8.5-1_all.deb' 中构建软件包 'python3-defaults-build-deps'。

The package has been created.
Attention, the package has been created in the current directory,
not in ".." as indicated by the message above!
正在选中未选择的软件包 python3-defaults-build-deps。
(正在读取数据库 ... 系统当前共安装有 405025 个文件和目录。)
准备解压 python3-defaults-build-deps_3.8.5-1_all.deb  ...
正在解压 python3-defaults-build-deps (3.8.5-1) ...
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
正在修复依赖关系...Starting pkgProblemResolver with broken count: 1
Starting 2 pkgProblemResolver with broken count: 1
Investigating (0) python3-defaults-build-deps:amd64 < 3.8.5-1 @iU mK Nb Ib >
Broken python3-defaults-build-deps:amd64 依赖 on python3.8:any:any < none @un H > (>= 3.8.2-1~)
  Removing python3-defaults-build-deps:amd64 because I can't find python3.8:any:any
Done
 完成
Starting pkgProblemResolver with broken count: 0
Starting 2 pkgProblemResolver with broken count: 0
Done
下列软件包将被【卸载】：
  python3-defaults-build-deps
升级了 0 个软件包，新安装了 0 个软件包，要卸载 1 个软件包，有 72 个软件包未被升级。
有 1 个软件包没有被完全安装或卸载。
解压缩后将会空出 9,216 B 的空间。
您希望继续执行吗？ [Y/n] y
(正在读取数据库 ... 系统当前共安装有 405028 个文件和目录。)
正在卸载 python3-defaults-build-deps (3.8.5-1) ...
mk-build-deps: Unable to install python3-defaults-build-deps at /usr/bin/mk-build-deps line 416.
mk-build-deps: Unable to install all build-dep packages
```

下载 https://www.python.org/ftp/python/3.8.5/Python-3.8.5.tar.xz （建议用境外代理），解压到 ~/t-project/deb-src/python3-defaults.git 。

```
dpkg-buildpackage --no-check-builddeps --no-sign --jobs=4
dpkg-buildpackage --no-check-builddeps --no-sign --jobs=4 --no-pre-clean
```

### 从 debian source 创建 deb (pbuilder，失败)

创建镜像：
```
sudo pbuilder create --distribution buster --mirror http://mirrors.163.com/debian/
```
修改编译时依赖的 python 版本：

```
$ git diff
diff --git a/debian/control b/debian/control
index 438cc5b..fa0aa4b 100644
--- a/debian/control
+++ b/debian/control
@@ -3,7 +3,7 @@ Section: python
 Priority: optional
 Maintainer: Matthias Klose <doko@debian.org>
 Uploaders: Piotr Ożarowski <piotr@debian.org>
-Build-Depends: debhelper (>= 11), dpkg-dev (>= 1.17.11), python3.8:any (>= 3.8.2-1~),
+Build-Depends: debhelper (>= 11), dpkg-dev (>= 1.17.11),
   lsb-release,
   python3-minimal:any,
   python3-docutils,
diff --git a/debian/rules b/debian/rules
index 1b60d6a..97137e2 100755
--- a/debian/rules
+++ b/debian/rules
@@ -28,7 +28,7 @@ UPSTRVER   := 3.8.5-1~
 STDLIBVER   := 3.8.5-1~
 
 ifeq (,$(filter $(distrelease),lenny etch squeeze wheezy lucid maverick natty oneiric precise quantal raring saucy trusty))
-  bd_i586 = dpkg-dev (>= 1.17.11), python3.8:any (>= 3.8.2-1~),
+  bd_i586 = dpkg-dev (>= 1.17.11),
 else
   bd_i586 = dpkg-dev (>= 1.16.1~),
 endif
```

编译：
```
cd ~/t-project/deb-src/python3-defaults.git
pdebuild
```

$ sudo pbuilder --update

### 从 debian source 创建 deb (new，失败)

```
~/t-project/deb-src$ apt source python3
正在读取软件包列表... 完成
选择 python3-defaults 作为源代码包而非 python3
提示：python3-defaults 的打包工作被维护于以下位置的 Git 版本控制系统中：
https://salsa.debian.org/cpython-team/python3-defaults.git
请使用：
git clone https://salsa.debian.org/cpython-team/python3-defaults.git
获得该软件包的最近更新(可能尚未正式发布)。
需要下载 142 kB 的源代码包。
获取:1 https://mirrors.tuna.tsinghua.edu.cn/debian sid/main python3-defaults 3.8.2-3 (dsc) [2,797 B]
获取:2 https://mirrors.tuna.tsinghua.edu.cn/debian sid/main python3-defaults 3.8.2-3 (tar) [139 kB]
已下载 142 kB，耗时 1秒 (252 kB/s)    
dpkg-source: info: extracting python3-defaults in python3-defaults-3.8.2
dpkg-source: info: unpacking python3-defaults_3.8.2-3.tar.gz

~/t-project/deb-src$ ls -l | grep python
drwxr-xr-x 1 duanyao duanyao      214 4月   7 18:43 python3-defaults-3.8.2
-rw-r--r-- 1 duanyao duanyao     2797 4月   8 04:28 python3-defaults_3.8.2-3.dsc
-rw-r--r-- 1 duanyao duanyao   139030 4月   8 04:28 python3-defaults_3.8.2-3.tar.gz

~/t-project/deb-src$ cd python3-defaults-3.8.2
```

```
~/t-project/deb-src/python3-defaults-3.8.2$ sudo mk-build-deps --install --remove
[sudo] duanyao 的密码：
dh_testdir
dh_testroot
dh_prep
dh_testdir
dh_testroot
dh_install
dh_installdocs
dh_installchangelogs
dh_compress
dh_fixperms
dh_installdeb
dh_gencontrol
dh_md5sums
dh_builddeb
dpkg-deb: 正在 '../python3-defaults-build-deps_3.8.2-3_all.deb' 中构建软件包 'python3-defaults-build-deps'。

The package has been created.
Attention, the package has been created in the current directory,
not in ".." as indicated by the message above!
正在选中未选择的软件包 python3-defaults-build-deps。
(正在读取数据库 ... 系统当前共安装有 405025 个文件和目录。)
准备解压 python3-defaults-build-deps_3.8.2-3_all.deb  ...
正在解压 python3-defaults-build-deps (3.8.2-3) ...
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
正在修复依赖关系...Starting pkgProblemResolver with broken count: 1
Starting 2 pkgProblemResolver with broken count: 1
Investigating (0) python3-defaults-build-deps:amd64 < 3.8.2-3 @iU mK Nb Ib >
Broken python3-defaults-build-deps:amd64 依赖 on python3.8:any:any < none @un H > (>= 3.8.2-1~)
  Removing python3-defaults-build-deps:amd64 because I can't find python3.8:any:any
Done
 完成
Starting pkgProblemResolver with broken count: 0
Starting 2 pkgProblemResolver with broken count: 0
Done
下列软件包将被【卸载】：
  python3-defaults-build-deps
升级了 0 个软件包，新安装了 0 个软件包，要卸载 1 个软件包，有 72 个软件包未被升级。
有 1 个软件包没有被完全安装或卸载。
解压缩后将会空出 9,216 B 的空间。
您希望继续执行吗？ [Y/n] y
(正在读取数据库 ... 系统当前共安装有 405028 个文件和目录。)
正在卸载 python3-defaults-build-deps (3.8.2-3) ...
mk-build-deps: Unable to install python3-defaults-build-deps at /usr/bin/mk-build-deps line 416.
mk-build-deps: Unable to install all build-dep packages
```

```
dpkg-buildpackage --no-check-builddeps --no-sign --jobs=4 --no-pre-clean
```

### 从 debian source 创建 deb（失败）
```
gpg --gen-key
```
生成了 /home/duanyao/.gnupg/*, '/home/duanyao/.gnupg/openpgp-revocs.d/xxxx.rev' 等文件。

gpg -a --output ~/.gnupg/duanyao.gpg --export 'duanyao'

新建了 ~/.gnupg/duanyao.gpg 文件。

gpg --import ~/.gnupg/duanyao.gpg

没有实际作用。

http://deb.debian.org/debian/pool/main/p/python3-defaults/python3-defaults_3.7.3-1.tar.gz

apt-get install build-essential dh-make devscripts fakeroot

```
~/Downloads/python3-defaults$ dpkg-buildpackage -rfakeroot
dpkg-buildpackage: info: source package python3-defaults
dpkg-buildpackage: info: source version 3.7.3-1
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Matthias Klose <doko@debian.org>
dpkg-buildpackage: info: host architecture amd64
 dpkg-source --before-build python3-defaults
dpkg-source: warning: unknown information field 'Cnf-Visible-Pkgname' in input data in package's section of control info file
dpkg-checkbuilddeps: error: Unmet build dependencies: python3.7:any (>= 3.7.3-1~) python3-docutils w3m xsltproc
dpkg-buildpackage: warning: build dependencies/conflicts unsatisfied; aborting
dpkg-buildpackage: warning: (Use -d flag to override.)
```

从 python3-defaults/debian/control 的 Build-Depends: 行删除  `python3.7:any (>= 3.7.3-1~),`

```
~/Downloads/python3-defaults$ sudo apt install python3-docutils w3m xsltproc

~/Downloads/python3-defaults$ dpkg-buildpackage -rfakeroot
```

```
 signfile python3-defaults_3.7.3-1.dsc
gpg: skipped "Matthias Klose <doko@debian.org>": 没有秘匙
gpg: dpkg-sign.mzw4FxS_/python3-defaults_3.7.3-1.dsc: clear-sign failed: 没有秘匙

dpkg-buildpackage: error: failed to sign .dsc file
```

control/control.in 的Maintainer 行改为 `Maintainer: Duan Yao <duanyao@ustc.edu>`
changelog 的最上面一条，`Matthias Klose <doko@debian.org>` 改为 `Duan Yao <duanyao@ustc.edu>`

```
~/Downloads/python3-defaults$ dpkg-buildpackage -rfakeroot
```

以上并不成功。

```
sudo apt install pbuilder
```

/root/.pbuilderrc
```
MIRRORSITE=http://ftp2.cn.debian.org/debian/  # http://http.us.debian.org/debian
APTCACHE=/var/cache/apt/archives
```

```
sudo pbuilder create
```

```
cd ~/t-project/python-deb/python3-defaults_3.7.3-1/python3-defaults
make pdebuild
```

结果在 /var/cache/pbuilder/result

## 使用非系统 libc

configure 的选项：
```
--with-libc=STRING

    Override libc C library to STRING (default is system-dependent).

--with-libm=STRING

    Override libm math library to STRING (default is system-dependent).
```