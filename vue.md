[1] vue + typescript 项目起手式
https://segmentfault.com/a/1190000011744210

[2] vue 集成 typescript Element UI 之懒人集成法
https://segmentfault.com/a/1190000012840792

[3] TypeScript Vue Starter
https://github.com/Microsoft/TypeScript-Vue-Starter

[4] vue-loader was used without the corresponding plugin.
https://blog.csdn.net/qq_37160067/article/details/80141952

[5] This quick start guide will teach you how to get TypeScript and Vue working together. 
https://github.com/DanielRosenwasser/typescript-vue-tutorial

## 最简 vue 程序

```
// index.html
<html>
    <head>
        <script src="vue.js"></script>
    </head>
    <body>
        
        <div id="app">
            {{ message }}
        </div>
        
        <script src="index.js"></script>
    </body>
</html>

// index.js
var app = new Vue({ 
    el: '#app',
    data: {
        message: 'Hello Vue!'
    }
});

// vue.js : vue库本体，从此处下载 https://cdn.jsdelivr.net/npm/vue/dist/vue.js
```

## 安装 vue 集成 typescript Element UI
[2]
安装 vue cli 工具：

  npm install -g @vue/cli
  npm install -g @vue/cli-init

安装 SASS 工具：

  npm i node-sass -g 

初始化一个项目，目录是 vue-t1：
  vue init ducksoupdev/vue-webpack-typescript vue-t1
选项：
? Project name vue-t1
? Project description A Vue.js project
? Author Duan Yao <duanyao@ustc.edu>
? Use hot-reloading? No

然后
  cd vue-t1
  npm install

安装 element-ui（已经带有 typescript 定义文件）。
  npm install element-ui

在 src/main.ts 中，导入 element-ui：

  import * as Element from 'element-ui';

  npm run dev

## TypeScript Vue Starter
[3]
npm install -g webpack-cli

mkdir vue-t2
cd vue-t2
npm init
npm install --save-dev typescript webpack ts-loader css-loader vue vue-loader vue-template-compiler

新建 tsconfig.json 文件[3]。
新建 webpack.config.js 文件[4]。

mkdir src
cd src
mkdir components
cd ..


错误：
ERROR in ./src/components/Hello.vue
Module Error (from ./node_modules/vue-loader/lib/index.js):
vue-loader was used without the corresponding plugin. Make sure to include VueLoaderPlugin in your webpack config.

需要补丁[4]. 但是仍然不能用。

"vue-loader": "^15.4.0",

## typescript-vue-tutorial
[5] 使用特定版本的 vue, typescript 等才可以：

  "dependencies": {
    "vue": "https://github.com/DanielRosenwasser/vue#540a38fb21adb7a7bc394c65e23e6cffb36cd867",
    "vue-router": "https://github.com/DanielRosenwasser/vue-router#01b8593cf69c2a5077df45e37e2b24d95bf35ce3"
  },
  "devDependencies": {
    "css-loader": "^0.28.1",
    "ts-loader": "^2.0.3",
    "typescript": "^2.3.2",
    "vue-loader": "^12.0.3",
    "vue-template-compiler": "2.2.1",
    "webpack": "^2.5.0"
  }


