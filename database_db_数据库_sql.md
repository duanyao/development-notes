## 参考资料
别人推荐的SQL书，留着，慢慢学习
https://www.cnblogs.com/xia392145655/articles/2255207.html

Amazon和Mysql之间的那点事儿（RDS，EC2，高可用和负载均衡，备份）
https://blog.csdn.net/winsonyuan/article/details/52785188

Amazon EC2安装mysql多实例并配置主从复制
https://blog.csdn.net/qq1010885678/article/details/47955717

MYSQL性能优化分享(分库分表)
https://www.jb51.net/article/29771.htm


## 关注点

分区、分库、分表

高可用

负载均衡

主从、复制

备份


## ORM
hibernate
MyBatis
Spring JdbcTemplate
guzz
memcached

## 时序数据库
参考“db_time_时序数据库.md”。
