## 参考资料

[1.1] COCO and Pascal VOC data format for Object detection
https://towardsdatascience.com/coco-data-format-for-object-detection-a4c5eaf518c5

[2.1] The CIFAR-10 and CIFAR-100
  http://www.cs.toronto.edu/~kriz/cifar.html

[2.2] [CIFAR] Learning Multiple Layers of Features from Tiny Images, Alex Krizhevsky, 2009.
  http://www.cs.toronto.edu/~kriz/learning-features-2009-TR.pdf

[3.1] coco dataset
  https://cocodataset.org/

[3.2] coco 标注格式
  https://cocodataset.org/#format-data

[3.3] Microsoft COCO: Common Objects in Context
  https://arxiv.org/abs/1405.0312
  
[3.4] How to work with object detection datasets in COCO format
  https://towardsdatascience.com/how-to-work-with-object-detection-datasets-in-coco-format-9bf4fb5848a4

[4.2] pascal/VOC
  http://host.robots.ox.ac.uk/pascal/VOC/voc2007/

[4.3] voc2007/VOCtrainval
  http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtrainval_06-Nov-2007.tar  450MB

[4.4] https://deepai.org/dataset/pascal-voc
[4.5] https://data.deepai.org/PASCALVOC2007.zip  1.65G

[4.6] http://host.robots.ox.ac.uk/pascal/VOC/voc2012/VOCtrainval_11-May-2012.tar  2GB
[4.7] http://host.robots.ox.ac.uk/pascal/VOC/voc2012/VOCdevkit_18-May-2011.tar  

[4.8] PASCAL VOC2011 Action Examples
http://host.robots.ox.ac.uk/pascal/VOC/voc2012/actionexamples/index.html

[5.1] ImageNet(ISLVRC2012)数据集
https://zhuanlan.zhihu.com/p/370799616

[5.2] ILSVRC2012数据集(分类部分)简要介绍和初步处理
https://blog.csdn.net/u012024357/article/details/90679222

[5.3] Pytorch 图像分类实战 —— ImageNet 数据集
https://xungejiang.com/2019/07/26/pytorch-imagenet/

![5.4] ImageNet使用方法？
https://www.zhihu.com/question/264345314

![6.1] Tiny ImageNet 
https://paperswithcode.com/dataset/tiny-imagenet

![6.2] Tiny ImageNet This is a miniature of ImageNet classification Challenge.
https://www.kaggle.com/competitions/tiny-imagenet/data

[10.1] 5 Million Faces – Top 14 Free Image Datasets for Facial Recognition
https://imerit.net/blog/5-million-faces-top-14-free-image-datasets-for-facial-recognition-all-pbm/

## CIFAR

下载：

```
wget https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz
wget http://www.cs.toronto.edu/~kriz/cifar-100-python.tar.gz
```

The CIFAR-10 dataset consists of 60000 32x32 colour images in 10 classes, with 6000 images per class.
airplane 										
automobile 										
bird 										
cat 										
deer 										
dog 										
frog 										
horse 										
ship 										
truck

The CIFAR-100 dataset
This dataset is just like the CIFAR-10, except it has 100 classes containing 600 images each. There are 500 training images and 100 testing images per class. The 100 classes in the CIFAR-100 are grouped into 20 superclasses. Each image comes with a "fine" label (the class to which it belongs) and a "coarse" label (the superclass to which it belongs).
Here is the list of classes in the CIFAR-100:

Superclass 	Classes
aquatic mammals 	beaver, dolphin, otter, seal, whale
fish 	aquarium fish, flatfish, ray, shark, trout
flowers 	orchids, poppies, roses, sunflowers, tulips
food containers 	bottles, bowls, cans, cups, plates

数据集采用自定义二进制格式。

## COCO

下载：

```
wget http://images.cocodataset.org/zips/val2017.zip
wget http://images.cocodataset.org/annotations/annotations_trainval2017.zip
```
### 概念

* Things: Things are objects with a specific size and shape, that are often composed of parts. things are countable objects such as people, animals, tools. 

* Stuff: Stuff classes are background materials that are defined by homogeneous or repetitive patterns of fine-scale properties, but have no specific or distinctive spatial extent or shape. Stuff classes are amorphous regions of similar texture or material such as grass, sky, road.

* panoptic: 'panoptic' is "including everything visible in one view".

### 标注

330K images (>200K labeled)

* object detection + segmentation
  * 200,000 images
  * 80 object categories
  * 1.5 million object instances
  * 500k object instances segmented

* human body keypoints
  250k people, 200k images

* Recognition in context

* Superpixel stuff segmentation

* caption 图片标签，是一句话，无结构。

### 任务

* Panoptic Segmentation: addresses both stuff and thing classes, unifying the typically distinct semantic and instance segmentation tasks.
  things are countable objects such as people, animals, tools. Stuff classes are amorphous regions of similar texture or material such as grass, sky, road. 'panoptic' is "including everything visible in one view"

* DensePose: dense estimation of human pose in challenging, uncontrolled conditions. simultaneously detecting people, segmenting their bodies and mapping all image pixels that belong to a human body to the 3D surface of the body.

* Stuff Segmentation: semantic segmentation of stuff classes (grass, wall, sky). 

* Object Detection: two object detection tasks: using either bounding box output or object segmentation output (the latter is also known as instance segmentation). detection task addresses thing classes (person, car, elephant),

* Keypoint Detection: localization of person keypoints in challenging, uncontrolled conditions.

### 数据集格式

```
val2017/
  000000000285.jpg
  000000000632.jpg
  ...

annotations_trainval2017/annotations/
  captions_train2017.json
  captions_val2017.json
  instances_train2017.json
  instances_val2017.json
  person_keypoints_train2017.json
  person_keypoints_val2017.json
```

coco 中的各类 id 均为整数，从1开始连续排列。

instances_val2017.json 是对象检测、分割的标注，其格式为：

```
{
    "images":
    [
        "images": [
        {
            "license": 4,
            "file_name": "000000397133.jpg",
            "coco_url": "http://images.cocodataset.org/val2017/000000397133.jpg",
            "height": 427,
            "width": 640,
            "date_captured": "2013-11-14 17:02:52",
            "flickr_url": "http://farm7.staticflickr.com/6116/6255196340_da26cf2c9e_z.jpg",
            "id": 397133
        },
    ],
    "annotations":
    [
        {
            "segmentation":
            [
                [
                    510.66,
                    423.01,
                    511.72,
                    420.03,
                    // ...
                ],
                //...
            ],
            "area": 702.1057499999998,
            "iscrowd": 0,
            "image_id": 289343,
            "bbox": [  // [x,y,width,height] 原点为左上角，绝对坐标/长度
                473.07,
                395.93,
                38.65,
                28.67
            ],
            "category_id": 18,
            "id": 1768
        }
    ],
    "categories": [
        {
            "supercategory": "person",
            "id": 1,
            "name": "person"
        },
        {
            "supercategory": "vehicle",
            "id": 2,
            "name": "bicycle"
        },
        {
            "supercategory": "vehicle",
            "id": 3,
            "name": "car"
        },
        //...
    ],
}
```

categories（标签）总计 91 类（含一个背景类）。

person_keypoints_val2017.json 是人体关键点的标注，其格式为：

```
{
    "images": [
        {
        ...
        },
        ...
    ],
    "annotations": [
        {
            "segmentation": [
                [
                    125.12,
                    539.69,
                    140.94,
                    522.43,
                ],
            ],
            "num_keypoints": 10, // 有标注的关键点的数量，即 v > 0。
            "area": 47803.27955,
            "iscrowd": 0,
            "keypoints": [
                0, // x0
                0, // y0
                0, // v0: 可见性和标注有效性，0 未标注，1 不可见但标注了，2 可见且标注了。
                0, // x1
                ...
            ],
            "image_id": 425226,
            "bbox": [
                73.35,
                206.02,
                300.58,
                372.5
            ],
            "category_id": 1,
            "id": 183126
        },
        ...
    ],
    "categories": [
        {
            "supercategory": "person",
            "id": 1,
            "name": "person",
            "keypoints": [ // 各个关键点的顺序和定义
                "nose", // 1
                "left_eye", // 2
                "right_eye", // 3
                "left_ear", // 4
                "right_ear", // 5
                "left_shoulder", // 6
                "right_shoulder", // 7
                "left_elbow", // 8
                "right_elbow", // 9
                "left_wrist", // 10
                "right_wrist", // 11
                "left_hip", // 12
                "right_hip", // 13
                "left_knee", // 14
                "right_knee", // 15
                "left_ankle", // 16
                "right_ankle" // 17
            ],
            "skeleton": [ // 各个关键点的连接关系。
                [
                    16,
                    14
                ],
                [
                    14,
                    12
                ],
                [
                    17,
                    15
                ],
                [
                    15,
                    13
                ],
                [
                    12,
                    13
                ],
                [
                    6,
                    12
                ],
                [
                    7,
                    13
                ],
                [
                    6,
                    7
                ],
                [
                    6,
                    8
                ],
                [
                    7,
                    9
                ],
                [
                    8,
                    10
                ],
                [
                    9,
                    11
                ],
                [
                    2,
                    3
                ],
                [
                    1,
                    2
                ],
                [
                    1,
                    3
                ],
                [
                    2,
                    4
                ],
                [
                    3,
                    5
                ],
                [
                    4,
                    6
                ],
                [
                    5,
                    7
                ]
            ]
        }
    ]
}
```

### 图片分类/标签标注

captions_val2017.json 

每个图片一个 caption，是一句话，无结构。

```
"annotations": [
        {
            "image_id": 179765,
            "id": 38,
            "caption": "A black Honda motorcycle parked in front of a garage."
        },
]
```

### cvat 导出的 coco 1.0 格式

```
xxx.zip
  annotations/
    instances_default.json
  images/
    frame_000000.PNG
    frame_000001.PNG
    ...
```

instances_default.json 的格式，比起 COCO 数据集，annotation中多了一个 attributes 属性。

```
...
    "annotations": [
        {
            "id": 1,
            "image_id": 1,
            "category_id": 2,
            "segmentation": [],
            "area": 5093.339999999999,
            "bbox": [
                39.34,
                227.42,
                50.25,
                101.36
            ],
            "iscrowd": 0,
            "attributes": {
                "occluded": false
            }
        },
    ]
```

cvat 可以用 "Tag" 模式来做图片分类，导出的数据集中，分类（tag）是用 attributes 表示的。

## PASCAL VOC

20 classes. The train/val data has 11,530 images containing 27,450 ROI annotated objects and 6,929 segmentations. 

People in action classification dataset are additionally annotated with a reference point on the body.

Person: person
Animal: bird, cat, cow, dog, horse, sheep
Vehicle: aeroplane, bicycle, boat, bus, car, motorbike, train
Indoor: bottle, chair, dining table, potted plant, sofa, tv/monitor

10 action classes + "other"

### 任务

* Classification: For each of the twenty classes, predicting presence/absence of an example of that class in the test image.

* Detection: Predicting the bounding box and label of each object from the twenty target classes in the test image. 

* Segmentation: Generating pixel-wise segmentations giving the class of the object visible at each pixel, or "background" otherwise. 

* Action Classification: Predicting the action(s) being performed by a person in a still image. 

## ImageNet
[5.1-5.3]

超过1400万的图像URL被ImageNet手动注释，以指示图片中的对象;在至少一百万张图像中，还提供了边界框。ImageNet包含2万多个类别; 一个典型的类别，如“气球”或“草莓”，每个类包含数百张图像。
在一些论文中，有的人会将这个数据叫成ImageNet 1K 或者ISLVRC2012，两者是一样的。“1 K”代表的是1000个类别。用这个数据测试模型结构是很方便的。有几点原因：1.很多的论文都使用了此数据集，跟其他模型比较时，可以直接引用结果；2. ImageNet的评价指标是固定的，大家都使用top1 、top5等；3. 可以直接看出你修改的模型结构到底有没有提高。[5.1]

ILSVRC2012_img_train.tar压缩包，里面包含120多万的自然图像，大概有150G。含有1000个类别的压缩包，分别对应1000个类别。每个压缩包解压之后都可以得到对应的类别照片。ILSVRC2012_img_val.tar中含有50000张图片，解压之后是直接是图像，并没有按照类别区分开。[5.1]

## WIDER FACE
http://shuoyang1213.me/WIDERFACE/

人脸检测数据集。32,203 images and label 393,703 faces 。


## LFW emotion dataset

https://github.com/KDDI-AI-Center/LFW-emotion-dataset

manually annotate LFW dataset according to three types of facial expressions (positive, negative, neural), which contain five types of facial orientations (up, left, center, right, down).

## Facial Expression Recognition Challenge

https://www.kaggle.com/c/challenges-in-representation-learning-facial-expression-recognition-challenge/data

facial expression in to one of seven categories (0=Angry, 1=Disgust, 2=Fear, 3=Happy, 4=Sad, 5=Surprise, 6=Neutral).

The training set consists of 28,709 examples. The public test set used for the leaderboard consists of 3,589 examples. The final test set, which was used to determine the winner of the competition, consists of another 3,589 examples.

## YouTube Faces Dataset with Facial Keypoints

https://www.kaggle.com/selfishgene/youtube-faces-with-facial-keypoints

## UTKFace Large Scale Face Dataset 

https://susanqq.github.io/UTKFace/

## VGGFace
https://zhuanlan.zhihu.com/p/76538698

## img2dataset 可下载的一些数据集
https://github.com/rom1504/img2dataset

mscoco 600k image/text pairs that can be downloaded in 10min
sbucaptions 860K image/text pairs can be downloaded in 20 mins.
cc3m 3M image/text pairs that can be downloaded in one hour
cc12m 12M image/text pairs that can be downloaded in five hour
laion400m 400M image/text pairs that can be downloaded in 3.5 days
laion5B 5B image/text pairs that can be downloaded in 7 days using 10 nodes
laion-aesthetic Laion aesthetic is a 120M laion5B subset with aesthetic > 7 pwatermark < 0.8 punsafe < 0.5
laion-art Laion aesthetic is a 8M laion5B subset with aesthetic > 8 pwatermark < 0.8 punsafe < 0.5
laion-coco Laion-COCO is a 600M subset of LAION2B-EN, captioned with an ensemble of BLIP L/14 and 2 CLIP versions (L/14 and RN50x64).
laion-high-resolution Laion high resolution is a 170M resolution >= 1024x1024 subset of laion5B
laion-face Laion face is the human face subset of LAION-400M for large-scale face pretraining. It has 50M image-text pairs.
coyo-700m COYO is a large-scale dataset that contains 747M image-text pairs as well as many other meta-attributes to increase the usability to train various models.
commonpool CommonPool is a large-scale dataset collected from CommonCrawl containing 12.8B image-text pairs.
datacomp-1b DataComp-1B is a large-scale dataset with 1.4B image-text pairs filtered from CommonPool.
