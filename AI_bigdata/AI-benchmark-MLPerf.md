Better AI for Everyone
https://mlcommons.org/

MLPerf Inference Benchmark Suite
https://github.com/mlcommons/inference
https://arxiv.org/abs/1911.02549

MLPerf™ Inference Benchmarks for Image Classification and Object Detection Tasks
https://github.com/mlcommons/inference/tree/master/vision/classification_and_detection

MLPerf Training Reference Implementations
https://github.com/mlcommons/training

关于MLPerf的一些调查 2019
https://blog.csdn.net/han2529386161/article/details/102723482
