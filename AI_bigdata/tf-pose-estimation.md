## 参考资料

[1] https://github.com/ildoonet/tf-pose-estimation

[2] Low GPU usage for inference when multithreaded vs multiprocess c++
  https://github.com/tensorflow/tensorflow/issues/26356

## tensorflow
可以安装 tensorflow1.7.1 的 GPU 或 CPU 版。
tensorflow1.15 并不兼容。

## 安装（python2）

在源码仓库运行：
`sudo pip install -e .`
或者 `sudo pip3 install -e .`

其他依赖的pip包：Cython pycocotools

sudo apt install python3-tk  # 仅python3，这个包不在 pip 里面。

错误：

```
ERROR: Cannot uninstall 'psutil'. It is a distutils installed project and thus we cannot accurately determine which files belong to it which would lead to only a partial uninstall.
```

这是因为 psutil 是 apt 安装的，pip 无法升级他。需要先卸载：

```
sudo apt search psutil

python-psutil/panda,now 5.4.2-1 amd64 [已安装]

sudo apt remove python-psutil
```

之后再次安装成功。

测试
```
python run.py --model=mobilenet_thin --resize=432x368 --image=./images/p1.jpg
```

错误：

```
  File "/home/duanyao/project/tf-pose-estimation.git/tf_pose/runner.py", line 5, in <module>
    from functools import lru_cache
ImportError: cannot import name lru_cache
```

```
sudo pip install backports.functools_lru_cache
```
注意，如果用名字 backports-functools_lru_cache 也可以安装，但这不是正规名字，卸载时就认不出来了。

加载 backports.functools_lru_cache 仍然错误：

```
python

>>> from backports.functools_lru_cache import lru_cache
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ImportError: No module named functools_lru_cache
>>> import sys
>>> sys.path
['', '/usr/lib/python2.7', '/usr/lib/python2.7/plat-x86_64-linux-gnu', '/usr/lib/python2.7/lib-tk', '/usr/lib/python2.7/lib-old', '/usr/lib/python2.7/lib-dynload', '/usr/local/lib/python2.7/dist-packages', '/home/duanyao/project/tf-pose-estimation.git', '/usr/lib/python2.7/dist-packages', '/usr/lib/python2.7/dist-packages/gtk-2.0']
>>> 
```

查看

```
ls /usr/local/lib/python2.7/dist-packages/backports/
functools_lru_cache.py  functools_lru_cache.pyc  __init__.py  __init__.pyc  shutil_get_terminal_size  weakref.py  weakref.pyc

cat /usr/local/lib/python2.7/dist-packages/backports/__init__.py

__path__ = __import__('pkgutil').extend_path(__path__, __name__)
```

改用 apt 安装 python-backports.functools-lru-cache ，成功：

```
sudo pip uninstall backports.functools_lru_cache

  Successfully uninstalled backports.functools-lru-cache-1.5

apt search python-backports.functools-lru-cache

  python-backports.functools-lru-cache/panda,panda 1.4-2 all

sudo apt install python-backports.functools-lru-cache

dpkg -L python-backports.functools-lru-cache

  /usr/lib/python2.7/dist-packages/backports/functools_lru_cache.py
  
cat /usr/lib/python2.7/dist-packages/backports/__init__.py

  （空文件）

pytyon

>>> from backports.functools_lru_cache import lru_cache
  （成功）
```

看起来，backports.* 的包只能安装到 /usr/lib/python2.7/ 下面，不能安装到 /usr/local/lib/python2.7/ 下面。其它的包倒是没关系（如cv）。


另一个错误：
```
  File "/home/duanyao/project/tf-pose-estimation.git/tf_pose/estimator.py", line 23, in <module>
    logger.handlers.clear()
AttributeError: 'list' object has no attribute 'clear'
```
修改：
```
#logger.handlers.clear()
logger.handlers[:]
```
前者是python3.3的方法。


```
  File "/home/duanyao/project/tf-pose-estimation.git/tf_pose/eval.py", line 16, in <module>
    from pycocotools.coco import COCO
ImportError: No module named pycocotools.coco


sudo pip install pycocotools
```

pycocotools ：Tools for working with the MSCOCO dataset

```
python run_webcam.py --model=mobilenet_thin --resize=432x368 --camera=0
```

## 安装（python3）

```
sudo pip3 install -e .

```

错误：

```
Collecting matplotlib>=2.2.2 (from tf-pose===0.1.1)
  Using cached https://pypi.tuna.tsinghua.edu.cn/packages/12/d1/7b12cd79c791348cb0c78ce6e7d16bd72992f13c9f1e8e43d2725a6d8adf/matplotlib-3.1.1.tar.gz
    Complete output from command python setup.py egg_info:
    
    Beginning with Matplotlib 3.1, Python 3.6 or above is required.
    
    This may be due to an out of date pip.
    
    Make sure you have pip >= 9.0.1.
    
    
    ----------------------------------------
Command "python setup.py egg_info" failed with error code 1 in /tmp/pip-build-8kqkpmvf/matplotlib/
You are using pip version 8.1.1, however version 19.2.3 is available.
You should consider upgrading via the 'pip install --upgrade pip' command.

```

执行 `pip install --upgrade pip` 并没有效果。这可能是因为在这个系统上，pip 默认是 pip3，但 matplotlib 的安装脚本调用的是 pip2，所以需要也升级 pip2。

```
sudo pip3 install --upgrade cython
sudo pip3 install pycocotools
pip3 install slidingwindow # root 用户，用不用 sudo 有区别？
```

有的环境里需要：
```
sudo apt install swig
cd tf_pose/pafprocess/
swig -python -c++ pafprocess.i && python3 setup.py build_ext --inplace
```


测试
```
python3 run.py --model=mobilenet_thin --resize=432x368 --image=./images/p1.jpg
```




## 效果（GPU）
GTX950

```
python run_webcam.py --model=mobilenet_thin --resize=432x368 --camera=0
```

使用 mobilenet_thin ，摄像头，单人，15fps，GTX950，75% GPU占用，1.5GB显存占用。手臂平举或高举时可能检测错误。

使用 mobilenet_v2_large ，摄像头，单人，14fps，GTX950，70% GPU占用，1.5GB显存占用。手臂平举或高举时检测准确性较高。

\time -v python run_image.py --model=mobilenet_v2_large --resize=848x480 --image=/home/duanyao/project/sample_record/key_point_test/1.jpg --out-image=./1-out.jpg
原图大小 800x450，resize 要求是 16 的倍数。

mobilenet_v2_large 耗时，不算初始化:

GTX950: 0.15-0.18s 
GTX1070Ti: 0.07s
GTX1070: 0.109s

内存占用约1.5GB。

\time -v python run_image.py --model=cmu --resize=848x480 --image=/home/duanyao/project/sample_record/key_point_test/3.jpg --out-image=./3-cmu.jpg

cmu 耗时约 0.67 秒，不算初始化，不算初始化:

GTX950: 0.67s
GTX1070Ti: 0.18s
GTX1070: 0.2365s

内存占用约3.7GB。

正确率：

3张教室图片，缩放到800x450，人数25。
作为对比，百度有3，2，0人未检出。

mobilenet_v2_large：分别有7，5，6人未检出。

### 性能测试 GPU

```
\time -v python run_image.py --image=./1.jpg --out-image=./1-out.jpg --model=mobilenet_v2_large --resize=848x480 --repeat=100
\time -v python run_image.py --image=./1.jpg --out-image=./1-out.jpg --model=mobilenet_v2_large --resize=1024x576 --repeat=100

\time -v python run_image.py --image=./1.jpg --out-image=./1-out.jpg --model=cmu --resize=848x480 --repeat=30
\time -v python run_image.py --image=./1.jpg --out-image=./1-out.jpg --model=cmu --resize=1024x576 --repeat=30
```

resize 对耗时的影响较大，原始图像的尺寸则影响不大（1024x576 是原始图像尺寸）：

字段：
分辨率  每一帧的时间  GPU 占用率

GTX950 + mobilenet_v2_large
848x480   0.1556  0.75
1024x576  0.2160  0.85

GTX950 + cmu
848x480   0.6812  0.9
1024x576  0.8455  0.9


GTX1070Ti + mobilenet_v2_large
848x480   0.06      0.5
1024x576  0.0898    0.6

GTX1070Ti + cmu
848x480   0.1826  0.85
1024x576  0.2160  0.85

GTX1070Ti + cmu
848x480   0.1826  0.85
1024x576  0.2160  0.85

GTX1070 + mobilenet_v2_large
848x480   0.1045    0.5
1024x576  0.1350    0.6

GTX1070 + cmu
848x480   0.2382    0.7
1024x576  0.3205    0.8

940MX + cmu
1024x576  2.3  0.9
320x240   2.3  0.9

940MX + mobilenet_v2_large
1024x576  -  -
320x240   0.65  0.9

### 性能测试 GPU 多线程
注意，多线程访问 TfPoseEstimator.inference() 会有竞态条件（race condition），导致输出混乱、崩溃等问题。多线程多检测器实例可以避免这个问题。

instance-3239106
```
scp -r -P 8131 infer_server_stdio.py root@bob.geeekvr.com:/root/tf-pose-estimation.git/

scp -r -P 8131 body-image-576p tf_pose tf_pose.egg-info *.txt *.py root@bob.geeekvr.com:/root/tf-pose-estimation.git/

watch -n 0.5 nvidia-smi

cat input_list.txt | python infer_server_stdio.py
```
字段：
分辨率  fps  GPU占用率  线程数

GTX950 + mobilenet_v2_large
1024x576  5.4  1

GTX1070Ti + mobilenet_v2_large
1024x576  12.716  0.8  4
1024x576  13.487  0.8  8
1024x576  13.633  0.8  16

GTX1080Ti + mobilenet_v2_large
1024x576  16.631  0.5  8
1024x576  17.117  0.5  12

#### 多线程+多进程
字段：
分辨率  fps  GPU占用率  线程数x进程数

GTX1070Ti + mobilenet_v2_large
1024x576  7.128+7.299 0.8  1x2
1024x576  8.661+8.366 0.95 3x2
1024x576  9.214+8.592 0.95 4x2
1024x576  8.781+8.458 0.95 6x2
1024x576  16.8 0.95 1x4
1024x576  17.2 1.0 1x6

GTX1080Ti + mobilenet_v2_large
1024x576  12.849+12.481 0.9 6x2
1024x576  24.5 0.9 4x2
1024x576  23.7 0.9 3x2
1024x576  24.4 0.9 2x2
1024x576  24.4 0.9 2x3
1024x576  24.6 1.0 1x6
1024x576  24.6 1.0 1x8

RTX 2080 Ti + mobilenet_v2_large
1024x576  26 1.0 1x9

GeForce 940MX + mobilenet_v2_large
1024x576  1.46 0.9 1x2

结论：多线程+多进程可以充分利用GPU，单纯的多进程也可以，但是要多消耗显存。

有人观察到类似的现象，请参考 AI-tensorflow.md

2080 Ti 的实际性能比起 1080Ti 没有理论上那样明显。

#### 多ＧＰＵ机器
有的机器达不到理论的加速比，例如双 1080Ti　的　fps 在 37-45。

#### 多线程多检测器实例
每个线程使用独立的检测器实例。不过看起来显存是共享的，不会随着检测器实例的增加而增加。

字段：
分辨率  fps  GPU占用率

GTX1070Ti + mobilenet_v2_large
1024x576  12.017  0.65  2
1024x576  12.539  0.7   4

结论：多检测器实例并不有助于充分利用GPU，性能与单纯的多线程相当。

## 效果（CPU）
Intel(R) Xeon(R) CPU E3-1231 v3 @ 3.40GHz；8个虚拟核心。

```
python3 run_webcam.py --model=mobilenet_thin --resize=432x368 --camera=0
```
使用 mobilenet_thin ，摄像头，单人，3fps，86% CPU 占用，250MB内存占用。


```
\time -v python3 run_image.py --image=/home/duanyao/project/sample_record/body_point_image/576p/1.jpg --out-image=./1-out.jpg --model=mobilenet_v2_large --resize=848x480 --repeat=4
\time -v python3 run_image.py --image=/home/duanyao/project/sample_record/body_point_image/576p/1.jpg --out-image=./1-out.jpg --model=mobilenet_v2_large --resize=1024x576 --repeat=4
\time -v python3 run_image.py --image=/home/duanyao/project/sample_record/body_point_image/576p/1.jpg --out-image=./1-out.jpg --model=mobilenet_v2_large --resize=432x368  --repeat=4
```

mobilenet_v2_large 耗时约 1.7 秒（AVX2 优化版1.56秒），不算初始化。CPU 占用约 88%，内存占用约0.54GB。

resize 对耗时的影响较大，原始图像的尺寸则影响不大（1024x576 是原始图像尺寸）：
432x368   1.22
848x480   1.42
1024x576  2.02

不过，适当提高分辨率（甚至上采样）有利于提高识别准确率。

注意，resize=432x368 与不写 resize 还是有较大差异的。

## tensorrt

```
2019-08-23 13:07:16.591003: E tensorflow/contrib/tensorrt/log/trt_logger.cc:38] DefaultLogger Parameter check failed at: ../builder/Network.cpp::addInput::364, condition: isValidDims(dims)
2019-08-23 13:07:16.591074: W tensorflow/contrib/tensorrt/convert/convert_graph.cc:412] subgraph conversion error for subgraph_index:0 due to: "Invalid argument: Failed to create Input layer" SKIPPING......( 7 nodes)
2019-08-23 13:07:16.593155: E tensorflow/contrib/tensorrt/log/trt_logger.cc:38] DefaultLogger Parameter check failed at: ../builder/Network.cpp::addInput::364, condition: isValidDims(dims)
2019-08-23 13:07:16.593175: W tensorflow/contrib/tensorrt/convert/convert_graph.cc:412] subgraph conversion error for subgraph_index:1 due to: "Invalid argument: Failed to create Input layer" SKIPPING......( 7 nodes)
2019-08-23 13:07:16.595199: E tensorflow/contrib/tensorrt/log/trt_logger.cc:38] DefaultLogger Parameter check failed at: ../builder/Network.cpp::addInput::364, condition: isValidDims(dims)
2019-08-23 13:07:16.595220: W tensorflow/contrib/tensorrt/convert/convert_graph.cc:412] subgraph conversion error for subgraph_index:2 due to: "Invalid argument: Failed to create Input layer" SKIPPING......( 7 nodes)
...
```

## 视频形式

