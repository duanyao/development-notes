## 参考资料

[1.1] CVAT is free, online, interactive video and image annotation tool for computer vision
https://github.com/opencv/cvat

[1.2] Frequently asked questions
https://openvinotoolkit.github.io/cvat/docs/faq/

[1.7] Computer Vision Annotation Tool: A Universal Approach to Data Annotation
https://software.intel.com/en-us/articles/computer-vision-annotation-tool-a-universal-approach-to-data-annotation

[2.1] Creating an annotation task
https://openvinotoolkit.github.io/cvat/docs/manual/basics/creating_an_annotation_task/

[2.3] Settings
https://openvinotoolkit.github.io/cvat/docs/manual/basics/settings/

[2.4] Track mode with polygons
https://openvinotoolkit.github.io/cvat/docs/manual/advanced/annotation-with-polygons/track-mode-with-polygons/

[3.1] opencv-tools
https://openvinotoolkit.github.io/cvat/docs/manual/advanced/opencv-tools/

[3.2] AI Tools
https://openvinotoolkit.github.io/cvat/docs/manual/advanced/ai-tools/

[3.3] deep learning (DL) models for semi-automatic and automatic annotation
https://openvinotoolkit.github.io/cvat/docs/manual/advanced/models/

[3.4] installation of components needed for semi-automatic and automatic annotation
https://openvinotoolkit.github.io/cvat/docs/administration/advanced/installation_automatic_annotation/

[3.5] Guide to using the automatic annotation of tasks.
https://openvinotoolkit.github.io/cvat/docs/manual/advanced/automatic-annotation/

[3.6] Interactors are used to create a polygon semi-automatically
https://openvinotoolkit.github.io/cvat/docs/manual/advanced/ai-tools/#interactors

[3.7] Automatic annotation 500 ServerError
https://github.com/openvinotoolkit/cvat/issues/2189

[1.3] mask_rcnn_inception_resnet_v2_atrous_coco
https://github.com/opencv/cvat/tree/develop/utils/open_model_zoo/mask_rcnn_inception_resnet_v2_atrous_coco

[1.4] Faster R-CNN with Inception v2
https://github.com/opencv/cvat/tree/develop/utils/open_model_zoo/faster_rcnn_inception_v2_coco

[1.5] Support for openVINO 2020 is missing
https://github.com/opencv/cvat/issues/1179

[1.6] Re-Identification Application
https://github.com/opencv/cvat/tree/master/cvat/apps/reid

[4.1] Instructions on how to backup CVAT data with Docker.
https://openvinotoolkit.github.io/cvat/docs/administration/advanced/backup_guide/

[5.1] Control User Access and Permissions in CVAT with Open Policy Agent
https://www.cvat.ai/post/cvat-open-policy-agent

## 用户管理

cvat 的内置用户组有：

* super user, 即管理员，最高权限的用户。可以设置其他用户的权限，也能看到和操作所有的标注项目。安装时的操作 `python3 ~/manage.py createsuperuser` 就是创建 super user 。
* user , 普通用户。可以创建自己的标注项目，可以更改自己有权限的项目的指派。不能看到和操作其它用户的项目，除非其它用户分派任务给自己。
* annotator：标注者，可以标注数据，不能创建项目。2.16 不可更改项目的指派。
* observer：观察者，只能看，不能修改。
* 不属于任何用户组：可以标注数据，不能创建项目，可以更改自己有权限的项目的指派。这可能是早期版本（1.7）的特性，后来被取消了。

在 cvat web 界面上，任何人都可以创建自己的账号。但刚创建的账号不属于任何用户组，什么也做不了，需要让管理员给账号设置用户组。

cvat 还有一套组织角色（Organization roles）
* Owner：组织创建者。可以邀请其他用户加入此组织。
* Maintainer：可以邀请其他用户加入此组织。可以创建和分配项目。
* Supervisor：可以创建和分配项目。
* Worker：可以标注数据，不能创建项目。

## 权限控制（2.x）

### 概述
cvat 2.x 的用户组有两类：
1. 本地用户组。在 `/admin/auth/group` 可以看到，有以下几个组：
  admin
	business
	user
	worker
  annotator
  observer
  以上是按照权限从高到低排序的。应当注意，cvat 2.x 的权限系统[5.1]只描述了前4个组（见 `cvat/apps/iam/rules/utils.rego`），后2个组只有最低限度的权限。
  user 及以上的用户组可以创建项目（并可以主动访问共享目录），worker 及以上的用户组可以指派项目给其它用户。

2. 组织用户组。在 `/admin/auth/organization` 可以看到，有以下几个组：
  owner
  maintainer
  supervisor
  worker
  以上是按照权限从高到低排序的（见 `cvat/apps/organizations/rules/organizations.rego`）。

如果一个用户加入了组织，则他的权限同时包括本地用户组和组织用户组的权限。一个用户可以同时属于多个组织，在每个组织中可属于不同的组织用户组。
project 和 task 也可以指定属于一个组织，这样这个 project 和 task 只能被组织用户组中的用户访问。

普通用户（即除了 admin 组）只能列出所属组织的其他用户，如果不属于任何组织，就无法列出其它用户。这样，如果一个普通用户不属于任何组织，则实际上无法将其控制的项目指派给其它用户。因此，建议所有用户都加入某个组织。

### 调试权限控制
cvat 2.x 的权限控制策略由多个 .rego 文件组成，例如：
```
cvat/apps/iam/rules/utils.rego
cvat/apps/organizations/rules/organizations.rego
cvat/apps/engine/rules/users.rego
cvat/apps/engine/rules/tasks.rego
```
rego 文件是一种类似 python 的语法，用于描述权限控制策略 [5.1]。目前无法直接调试 rego 文件，cvat也不会输出权限控制的相关日志，因此通常需要通过“修改 rego 文件 -> 测试 -> 修改 rego 文件“的循环来调试。

例如，要调试 `cvat/apps/engine/rules/tasks.rego`，可以从 cvat 源码中复制和修改 tasks.rego 文件，用 bind mount 方式替换 cvat 容器中的文件。
创建 `/media/data2/docker-compose-srv/cvat-debug/cvat-2.16.1/docker-compose.debug-dy.yml`：
```
services:
  cvat_server:
    volumes:
      - /media/data2/docker-compose-srv/cvat-debug/cvat-2.16.1/cvat/apps/engine/rules/tasks.dy.rego:/home/django/cvat/apps/engine/rules/tasks.rego
```
复制 tasks.rego 创建 `/media/data2/docker-compose-srv/cvat-debug/cvat-2.16.1/cvat/apps/engine/rules/tasks.dy.rego`，在尾部增加：
```
allow if {
    input.scope in {
        utils.UPDATE_OWNER, utils.UPDATE_ASSIGNEE, utils.UPDATE_PROJECT,
        utils.DELETE, utils.UPDATE_ORG
    }
    input.auth.organization.id == input.resource.organization.id
    # utils.has_perm(utils.WORKER) # 注释掉检查本地用户组的过程
    organizations.has_perm(organizations.WORKER)
    is_project_staff
}
```
重启 cvat：
```
cd /media/data2/docker-compose-srv/cvat-2.16.1-custom1
export CVAT_HOST=gtrd.local
docker compose -f docker-compose.yml -f components/serverless/docker-compose.serverless.yml -f docker-compose.custom-vol.yml down

docker compose -f docker-compose.yml -f components/serverless/docker-compose.serverless.yml -f docker-compose.custom-vol.yml -f ../cvat-debug/cvat-2.16.1/docker-compose.debug-dy.yml up -d
```
在 cvat GUI 中测试权限控制行为是否符合预期。如果不符合，修改 tasks.dy.rego 文件，重启 cvat_server 容器：
```
docker compose -f docker-compose.yml -f components/serverless/docker-compose.serverless.yml -f docker-compose.custom-vol.yml -f ../cvat-debug/cvat-2.16.1/docker-compose.debug-dy.yml stop cvat_server

docker compose -f docker-compose.yml -f components/serverless/docker-compose.serverless.yml -f docker-compose.custom-vol.yml -f ../cvat-debug/cvat-2.16.1/docker-compose.debug-dy.yml start cvat_server
```

## 项目管理

[2.1]
cvat 的项目管理分三个层级：project - task - job ，统称为“条目”。
project（项目）用来聚合一组同类的 task（任务），这些任务可以共享一组共同的标签。
task 表示一个标注任务，其中应包含一组待标注的数据。task 也可以独立存在。

job 对 task 中的部分或全部数据进行的一次标注就是一个 job 。创建 task 的时候就会自动创建 job 。
一个较大的 task 可以中的数据可以切分成若干份，做成多个 job 分配给不同的人去做，这样的多个 job 中的数据也可以有一定的重叠。
对于视频标注任务，每个Job中的帧数量为 segment size，相邻两个job之间的重叠帧数量就是 overlap size。
Job可以设置状态，主要就是 标注中/验证中/已完成 三个状态。

条目的创建者可以将它们分配给别的用户去完成。创建者和被分配的用户都拥有这个条目，它们也可以将它们再分配给其它用户。
注意，除了创建者，每个条目最多只能再有一个拥有者。

建议的外包项目管理方式：
1. 发包公司为每个外包公司创建一个组织，将外包公司成员加入这个组织。
2. 外包公司的成员的本地组是 worker 或更低。使其不能主动访问共享目录。
3. 发包公司创建project，将其加入此组织，并指派给外包公司指定的对接人；其下的 task 可以暂不指派给人，但 task 也需要加入此组织。
4. 外包公司的对接人进一步将project、task指派给标注人和质检人。

## 创建视频/图像类标注任务

[2.1]
视频/图像类标注任务都会被表示为一列有序的帧。每个这类任务可以加入一个视频文件、一个包含图像文件的压缩包、一个PDF、或多个图像文件。
如果确实需要在一个任务中加入多个视频，可以预先抽帧，或者将视频合并。

视频文件会被自动抽帧，默认是每一帧都抽，但也可以指定抽帧的间隔（Frame step），间隔是指帧的数量而不是帧间的时间。
cvat 并不考虑视频帧之间的时间间隔不完全均匀的问题，标注是针对帧的序号，而不是视频的时间点。
如果抽帧的间隔可能不完全符合预期，可以考虑预先用 ffmpeg 改变帧率或者抽帧。
视频或图像文件可以从本地文件拖放，也可以指定网络文件的url，只要这个url能够被cvat服务器访问。多个图像文件的url用换行分割。

目前 cvat 支持 h264、h265 等视频格式，但不支持 av1 格式。
帧数较多的视频，或者一般浏览器不支持的视频格式（如h.265）上传后会被分段转码后存储，占用空间较大，且更耗时。帧数较少的视频可能仅存储原视频。

## 标签(label)和属性(attributes)

[2.1]
每个被标注的物体或轨迹可以有一个标签(label)，而一个标签可以有多个属性。一个工程或任务可以管理一组标签。

通常，标签用来区分物体的基本类别，如人、汽车。而属性用来进一步描述物体的特点，如人的身份、性别，汽车的轮子数量等。

每个标签要设置名字，可以随时修改，已经标注的物体的标签名也会跟着变。

每个属性要设置名字、数据类型、值域。目前 CVAT 中仅可事后修改值域，而修改名字会出错，所以设计属性时要慎重。

属性的数据类型有数字、单选列表（radio）、布尔（checkbox）、字符串等。数字类型必须设置最小、最大、步长。单选列表的值都是字符串。
目前 CVAT 无法设置属性的默认值。数字的默认值是最小值，单选列表的默认值是第一项。

理论上说，单选列表可以用来在人脸识别类标注中表示人的身份，但在 CVAT 中，一旦人数较多，单选列表会显得太长，难以操作。
所以，建议采用数字或字符串类型来表示身份。

标签和属性的修改有两种方式：GUI和JSON。JSON的格式如下：

```
[
  {
    "name": "头",
    "id": 1,
    "color": "#ff0000",
    "attributes": [
      {
        "id": 3,
        "name": "blur",
        "input_type": "number",
        "mutable": true,
        "values": [
          "0",
          "1",
          "0.5"
        ]
      },
      {
        "id": 4,
        "name": "face_shown",
        "input_type": "number",
        "mutable": true,
        "values": [
          "0",
          "1",
          "1"
        ]
      },
      {
        "id": 6,
        "name": "name",
        "input_type": "radio",
        "mutable": false,
        "values": [
          "甲",
          "乙",
          "丙",
          "丁"
        ]
      },
      {
        "id": 5,
        "name": "person",
        "input_type": "number",
        "mutable": false,
        "values": [
          "0",
          "99",
          "1"
        ]
      }
    ]
  }
]
```

标签在数组中的顺序似乎不重要，但其id的大小似乎是重要的：导出为数据集时，标签的排序是按此处的id升序排列的。

标签一旦确定，很多方面就不能修改，例如其id。如果确实需要修改标签，可以新建一个project，同时新建标签，然后把task移动到新的project中（这个过程中会要求对应新旧标签）。

问题：

* 如何设置属性的默认值？
* 如何修改属性名？

## 物体跟踪类标注

物体跟踪类标注任务是标注一列有序的帧中的物体（如行人、汽车）的移动轨迹。

基础操作步骤：

* 画物体框：在物体出现的第一帧中，点击工具条上的“画矩形（draw a rectangle）-轨迹（track）”，画一个矩形正好套住要标注的物体。与轨迹对应的另一种类型是“形状（shape）”，用于不需要追踪移动轨迹的标注方式。

* 打标签：可选。在右侧“物体（Objects）”面板，修改物体的标签（change current label）和属性（attributes）。也可以在物体框上右键，修改属性。

* 跳帧：在上方工具栏按“>”按钮或键盘“F”键跳到下一帧，或者按 ">>" 或者键盘 "V" 跳过多帧（step）。step 默认是 10 帧。

* 调整：之前画的矩形框会继续显示，但位置一般不会变化。如果物体已经移出了矩形框，或者变大变小了，可以用鼠标调整矩形框，让它继续套住物体。注意，即使物体在移动，也不一定需要每一帧都调整，可以隔几帧调整一次，中间的帧的位置会被通过内插法自动调整。一条轨迹上手动调整过的帧称为关键帧（keyframe），在右侧面板上会显示实心五角星，没有手动调整的帧为非关键帧，显示空心五角星；可以点击五角星按钮切换关键帧/非关键帧模式。

其它操作：

* 被遮挡：如果一个物体被临时遮挡或临时跑出画面外，可以不终止轨迹，而是设置其“被遮挡的(occluded)”状态（右侧面板上的“switch occluded”），矩形框继续可见。这个状态会向后延续，等此物体重新出现，可将被遮挡的状态撤销。

* 场外：如果一个物体跑出画面外或消失，可以设置其“场外（outside）”状态（右侧面板上的“switch outside”），这一帧矩形框将不可见，且轨迹终止在这一帧。

* 合并轨迹：如果发现两个轨迹表示的其实是同一个物体，可以将两个轨迹合并，操作方法是：来到第一个轨迹的最后一帧，先按下左侧面板”合并（merge shapes/tracks）“按钮，再点击第一个轨迹的框，再跳到第二个轨迹的第一帧，再点击第二个轨迹的框，再点击”合并”按钮，就完成了合并轨迹。孤立的“形状（shape）”也可以合并，合并后也就变成了轨迹。与合并轨迹相对的操作是拆分轨迹（split a track）。

问题：

* 画面上最多只显示一个物体的属性，如何显示所有物体的属性？在“设置”（右上角下拉菜单->settings）->workspace->Always show object details。
* 如何修改跳帧的步长（step）？在“设置”（右上角下拉菜单->settings）->player->player step 设置。

## 多边形

* 空洞的处理。使用background类。
https://openvinotoolkit.github.io/cvat/docs/manual/advanced/annotation-with-polygons/creating-mask/

* 分体的处理。使用group？

## 智能 open cv 工具
[3.1-] 

open cv 工具
### 智能剪刀（Intelligent scissors）

自动检测边缘，辅助创建多边形标注框。

### 自动调整对比度（Histogram Equalization）

自动增强图像的对比度。

## 使用 AI 自动标注工具

安装方法以及可安装的工具见“启用 AI 自动标注工具”。

CVAT 的自动标注工具分为几个类型，如
* reid (reidentification）：同一性识别，将不同帧中的同一对象挑出来。方法可以是目标跟踪或对象全身特征比较。主要针对人体。
* tracker: 跟踪移动物体。
* interactor ：即交互式对象分割，例如标注员选中一辆汽车，自动标注工具自动勾勒出汽车的轮廓。
* detector ：即对象检测，可以检测出一组给定类型的对象，如人、汽车、狗等。

### interactor: Deep Extreme Cut (DEXTR) 
内存占用大约是 1.13 GiB。
点击左侧工具栏 “AI Tools”，选择 interactors, DEXTR, 点击 "Interact" 按钮，然后用鼠标在目标的上、右、下、左边界上各打一个点，DEXTR 工具会自动生成一条环绕目标的多边形，调节多边形的顶点数（可选），然后再次点击“AI Tools”确认完成。这样生成的多边形是形状而非轨迹，可以标注若干帧之后将其合并为轨迹。


### openvino-omz-public-yolo-v3-tf: YOLO v3 detector 对象检测

内存占用大约是 1.14 GiB。
在任务列表中点击 actions->automatic annotation，model 选择 YOLO v3 ，然后建立标签映射：左侧为模型的标签，右侧为标注任务的标签，例如 person:人。然后点 annotate 开始自动标注。
YOLO v3 会自动创建矩形框包围目标。这是 shape，不是track。


### openvino-omz-public-faster_rcnn_inception_v2_coco：Faster RCNN detector 对象检测。

内存占用大约是 480 MiB。
操作方法同 YOLO v3 detector ，效果也类似。

### openvino-omz-intel-person-reidentification-retail-0300：人体同一性识别

内存占用大约是 350 MiB。
先使用任意 detector 自动标注人体，例如 Faster RCNN 。person-reidentification 自身无法检测人体。
然后在任务列表中点击 actions->automatic annotation，model 选择 person reidentification 然后点 annotate 开始自动标注。
效果是将不同帧中的同一个人的矩形框合并为一个轨迹。


## 安装

先安装 docker-ce (较新的版本已经自带 docker compose）

```
git clone https://github.com/opencv/cvat
cd cvat
docker compose up -d   # -d 后台运行
```

可以指定要运行的 cvat 的版本：

```
cd /home/cvat
docker compose down
CVAT_VERSION=v2.4.7 docker compose up -d
```
目前，CVAT有1.7（最新为 1.7.0）和2.x（最新为 2.16.1）两大版本，两者的API有些不兼容（影响 fiftyone 等）。
可以在已有数据上切换 CVAT 版本，但有些版本的升级是不兼容的，需要手动升级，见。

```
# 1.7.0
docker exec -it cvat bash -ic 'python3 ~/manage.py createsuperuser'

# 2.16.1
docker exec -it cvat_server bash -ic 'python3 ~/manage.py createsuperuser'
```
设置 admin 账号和密码。完毕后自动退出。

本地访问：

```
http://localhost:8080/
```

如果想要非本机也可访问，应在 docker compose up 之前设置环境变量 CVAT_HOST 为 localhost 以外的值：

```
cd cvat
export CVAT_HOST=192.168.31.79
docker-compose up -d 
```
注意这样设置以后，本机和非本机都只能从 http://192.168.31.79:8080 访问，localhost 不再有效。

在局域网中使用时，建议将 CVAT_HOST 设置为 mDNS 域名，例如 `CVAT_HOST=gtrd.local`。

停止运行
```
docker compose down
```

如果要启用 https，在一台开放了 80 和 443 端口的公网访问的服务器上启动 cvat，使用 let's encrypt 的 https 证书：

```
export CVAT_HOST=label-1.ai-parents.cn           # 指示本服务器所用的域名
export ACME_EMAIL=duanyao@chengzhangguiji.cn     # 代表 let's encrypt 帐号的电邮地址
docker compose -f docker-compose.yml -f docker-compose.https.yml up -d      # 启动容器，增加 https 支持。
```

之后可以通过 https://label-1.ai-parents.cn 访问。

cvat 1.7：如果服务器上还没有部署过 let's encrypt 的 https 证书，可参考 crypto_tls_cert_openssl.md 进行初次证书申请。之后，证书的管理和更新由 cvat 的 traefik 容器负责（详见 docker-compose.https.yml ）。
cvat 2.x：不需要手动做初次证书申请，直接用 docker-compose 启动即可，cvat 自动申请证书。

traefik 容器会定期自动更新 https 证书。如果确需手动更新证书，可登录进 traefik 容器，改名或删除包含证书的 acme.json 文件，然后重启 traefik 容器：

```
docker exec -it traefik sh
mv letsencrypt/acme.json letsencrypt/acme.bk.json  # 在 traefik 容器内执行
exit

export CVAT_HOST=label-1.ai-parents.cn
export ACME_EMAIL=duanyao@chengzhangguiji.cn
docker-compose -f docker-compose.yml -f docker-compose.https.yml restart traefik
```

停止运行（需要确保 ACME_EMAIL 设置了）：
```
export ACME_EMAIL=duanyao@chengzhangguiji.cn
docker-compose -f docker-compose.yml -f docker-compose.https.yml down
```

停止运行的命令应该与启动容器的命令对称，指定相同的 -f 配置文件。否则，有些容器可能没有被关掉；这时可以用 `docker-compose down --remove-orphan` 和 `docker stop <id>` 来停止它们。

如果安装或使用中有什么错误，可以用 `docker logs <id>` 查看日志。`<id>` 是容器名，可以是 "cvat" 等，可以用 docker ps 查看。

可以用 docker stats 查看内存/CPU 占用。没有安装附加功能的情况下，CVAT大约占用 1.4 GiB，主要是 "cvat" 容器。

## 共享目录
cvat 服务可以将宿主机上的目录作为“共享目录”，创建任务时可以选用其中的图像文件，这些文件会被创建的任务引用而非复制，减少存储空间和IO开销。
创建 docker-compose.custom-vol.yml ：
```
services:
  cvat_server:
    volumes:
      - cvat_share_data2:/home/django/share/media/data2:ro
  cvat_worker_import:
    volumes:
      - cvat_share_data2:/home/django/share/media/data2:ro
  cvat_worker_export:
    volumes:
      - cvat_share_data2:/home/django/share/media/data2:ro
  cvat_worker_annotation:
    volumes:
      - cvat_share_data2:/home/django/share/media/data2:ro

volumes:
  cvat_share_data2:
    driver_opts:
      type: none
      device: /media/data2
      o: bind
```

启动服务：
```
docker compose -f docker-compose.yml -f components/serverless/docker-compose.serverless.yml -f docker-compose.custom-vol.yml up -d cvat_db
```

共享目录的挂载点必须在 `/home/django/share` 下，可以有多个，可以有任意深度。

应注意共享目录可能带来的安全性问题：cvat 中有创建任务权限的用户组（user以上）都可以浏览共享目录，并通过创建任务来访问其中的图像。worker 和以下权限的用户组不能创建任务，也无法浏览共享目录。

## 云存储
cvat 可以使用云存储中的图像、视频来创建标注任务，目前支持 AWS S3 和 Google Cloud Storage。
在 


## 升级 cvat 的数据库

cvat 的数据库版本和表结构经过几次升级，各个版本存在不兼容的情况，可能需要手动升级，而不是简单地更改。
cvat 1.7.0 ～ 2.2.0 使用的数据库版本为 postgres:10-alpine ，cvat 2.3.0 ～ 2.16.1（更新的版本待查）使用的数据库版本为 postgres:15-alpine 。
目前明确知道的是，从 2.2.0 升级到 2.3.0 时需要手动升级数据库，具体来说是通过 postgres 的导出和导入功能来实现。

## 启用 AI 自动标注工具

如果之前已经启动了 docker 容器，则先停止。

```
docker-compose -f docker-compose.yml -f components/serverless/docker-compose.serverless.yml up -d
docker-compose -f docker-compose.yml -f components/serverless/docker-compose.serverless.yml up --build # 如果修改过 docker-compose 文件。
docker-compose -f docker-compose.yml -f components/serverless/docker-compose.serverless.yml down
```
这样，会多启动一个 nuclio 容器。
注意 docker-compose down 并不会停止后面 nuctl deploy 创建的容器。可以用 `docker stop $(docker ps -q)` 关闭所有的容器。

公开服务器启动：
```
export CVAT_HOST=label-1.ai-parents.cn
export ACME_EMAIL=duanyao@chengzhangguiji.cn
docker-compose -f docker-compose.yml -f components/serverless/docker-compose.serverless.yml -f docker-compose.https.yml up -d
docker-compose -f docker-compose.yml -f components/serverless/docker-compose.serverless.yml -f docker-compose.https.yml down
```

查看 components/serverless/docker-compose.serverless.yml 中的 nuclio 版本号，例如 `image: quay.io/nuclio/dashboard:1.5.16-amd64` ，则版本号为 1.5.16 。
下载 nuctl 工具：

```
wget https://github.com/nuclio/nuclio/releases/download/1.5.16/nuctl-1.5.16-linux-amd64
sudo chmod +x nuctl-1.5.16-linux-amd64
sudo mv nuctl-1.5.16-linux-amd64 /usr/local/bin/nuctl
```

创建 function (在 CVAT 目录下执行，每次 docker-compose up 启动 docker 容器之后要再次执行)：

```
# create project 只需要执行一次，然后可以执行 deploy 多次。
nuctl create project cvat --platform local

# nuctl deploy 对不同的模型各执行一次

nuctl deploy --project-name cvat --path serverless/openvino/omz/public/faster_rcnn_inception_v2_coco/nuclio --volume `pwd`/serverless/common:/opt/nuclio/common --platform local

nuctl deploy --project-name cvat --path serverless/openvino/omz/intel/person-reidentification-retail-300/nuclio --volume `pwd`/serverless/common:/opt/nuclio/common --platform local

nuctl deploy --project-name cvat --path serverless/pytorch/foolwood/siammask/nuclio --volume `pwd`/serverless/common:/opt/nuclio/common --platform local

nuctl deploy --project-name cvat --path serverless/openvino/omz/public/yolo-v3-tf/nuclio --volume `pwd`/serverless/common:/opt/nuclio/common --platform local

nuctl deploy --project-name cvat --path serverless/openvino/dextr/nuclio --volume `pwd`/serverless/common:/opt/nuclio/common --platform local
```
--platform local 表示对本地 docker 执行。

这个过程中可能会下载若干 docker 镜像和其他文件，详见 serverless/**/function.yaml 和 serverless/**/function-gpu.yaml 。
如果需要翻墙，应设置 http_proxy 和 https_proxy 环境变量。
部署一个 function 可能占用几个 GB 的硬盘，可以用 docker images 命令查看。
部署期间可以通过 http://localhost:8070/ 查看其状态。

安装完毕后，CVAT web 界面主页上的"Models"页面下会列出已经安装的AI模型。
也可以在服务器上用 nuctl 命令检查：

```
nuctl get function --platform local

  NAMESPACE |                          NAME                          | PROJECT | STATE | NODE PORT | REPLICAS  
  nuclio    | openvino-dextr                                         | cvat    | ready |     49154 | 1/1       
  nuclio    | openvino-omz-intel-person-reidentification-retail-0300 | cvat    | ready |     49153 | 1/1
```

如果显示 STATE 栏不是 ready 而是 unhealthy ，可能是 function 没有正确部署，可以重新运行 nuctl deploy 试试。

如果安装或使用中有什么错误，可以用 `docker logs cvat` 或 `docker logs nuclio` 查看日志。

nuclio 容器的启动似乎依赖外网是否通畅（如设置了 http_proxy 则代理也要通畅），如果不通可能会出错，导致 cvat 自动标注不可用。

要一次创建全部支持的 function ，可以执行（不建议，消耗的时间和硬盘空间都比较大）：

```
serverless/deploy_cpu.sh
serverless/deploy_gpu.sh
```

要删除 nuctl deploy 部署的函数，应该用 `nuctl --platform local del function <name>`，name 就是 `nuctl get function --platform local` 列出的 NAME 字段。
虽然也可以用 docker stop <contain_name> 来停止 nuctl deploy 部署的函数的容器，但其效果是临时的，下次重启系统时，这些容器又会自动启动。

可用的模型介绍：

pth-foolwood-siammask: Fast Online Object Tracking and Segmentation 对象跟踪和对象分割。内网下载有问题。
/home/duanyao/project/cvat.git/serverless/pytorch/foolwood/siammask/nuclio/function.yaml

openvino-omz-intel-person-reidentification-retail-0300：人体同一性识别，基于全身特征。也可以用于目标跟踪。内存需要 350 MiB 。
/home/duanyao/project/cvat.git/serverless/openvino/omz/intel/person-reidentification-retail-300/nuclio/function.yaml
https://docs.openvinotoolkit.org/2021.1/omz_models_intel_person_reidentification_retail_0288_description_person_reidentification_retail_0288.html

Deep Extreme Cut (DEXTR) ：交互式对象分割。内存需要 1.13GiB 。
/home/duanyao/project/cvat.git/serverless/openvino/dextr/nuclio/function.yaml
https://cvlsegmentation.github.io/dextr/

pth-saic-vul-fbrs ：f-BRS 交互式对象分割。
/home/duanyao/project/cvat.git/serverless/pytorch/saic-vul/fbrs/nuclio/function.yaml

pth.shiyinzhang.iog ： Interactive Object Segmentation with Inside-Outside Guidance 交互式对象分割。
/home/duanyao/project/cvat.git/serverless/pytorch/shiyinzhang/iog/nuclio/function.yaml

tf-faster-rcnn-inception-v2-coco ：Faster RCNN via Tensorflow 对象检测。
/home/duanyao/project/cvat.git/serverless/tensorflow/faster_rcnn_inception_v2_coco/nuclio/function.yaml
/home/duanyao/project/cvat.git/serverless/tensorflow/faster_rcnn_inception_v2_coco/nuclio/function-gpu.yaml

openvino-mask-rcnn-inception-resnet-v2-atrous-coco：Mask RCNN inception resnet v2 COCO via Intel OpenVINO 对象检测。内存需要 4GB 以上。
/home/duanyao/project/cvat.git/serverless/openvino/omz/public/mask_rcnn_inception_resnet_v2_atrous_coco/nuclio/function.yaml

openvino-omz-public-faster_rcnn_inception_v2_coco：Faster RCNN 对象检测。
/home/duanyao/project/cvat.git/serverless/openvino/omz/public/faster_rcnn_inception_v2_coco/nuclio/function.yaml

openvino-omz-public-yolo-v3-tf: YOLO v3 detector 对象检测。
/home/duanyao/project/cvat.git/serverless/openvino/omz/public/yolo-v3-tf/nuclio/function.yaml

pth.facebookresearch.detectron2.retinanet_r101：RetinaNet R101 from Detectron2 对象检测。
/home/duanyao/project/cvat.git/serverless/pytorch/facebookresearch/detectron2/retinanet/nuclio/function.yaml

## AI 自动标注工具除错

可以命令行测试 function:

```
image=$(cat t1.png | base64 | tr -d '\n')

cat << EOF > /tmp/input.json
{"image": "$image"}
EOF

cat /tmp/input.json | nuctl invoke openvino-omz-public-faster_rcnn_inception_v2_coco -c 'application/json' --platform local
```

输出：
```
21.09.03 16:28:29.879    nuctl.platform.invoker (I) Executing function {"method": "POST", "url": "http://:32778", "headers": {"Content-Type":["application/json"],"X-Nuclio-Log-Level":["info"],"X-Nuclio-Target":["openvino-omz-public-faster_rcnn_inception_v2_coco"]}}
21.09.03 16:28:30.760    nuctl.platform.invoker (I) Got response {"status": "200 OK"}
21.09.03 16:28:30.760                     nuctl (I) >>> Start of function logs
21.09.03 16:28:30.760 er_rcnn_inception_v2_coco (I) Run faster_rcnn_inception_v2_coco model {"time": 1630657709881.5515, "worker_id": "1"}
21.09.03 16:28:30.760                     nuctl (I) <<< End of function logs

> Response headers:
Server = nuclio
Date = Fri, 03 Sep 2021 08:28:30 GMT
Content-Type = application/json
Content-Length = 1574

> Response body:
[
    {
        "confidence": "0.9964563",
        "label": "person",
        "points": [
...
```

可以在监视 nuclio 容器的日志的情况下执行 AI 自动标注，看是否有错误：

```
docker logs nuclio --follow
```

案例分析[3.7]：CVAT 的自动标注无法使用，界面上显示
```
500 Server Error: Internal server error for url: http://nuclio:8070
```
docker logs nuclio 发现了以下错误：

```
21.09.03 08:45:50.191 .api/function_invocations (W) Failed to invoke function {"err": "Failed to send HTTP request", "errVerbose": "\nError - Post \"http://172.17.0.1:49158\": dial tcp 172.17.0.1:49158: i/o timeout\n    /nuclio/pkg/platform/abstract/invoker.go:119\n\nCall stack:\nFailed to send HTTP request\n    /nuclio/pkg/platform/abstract/invoker.go:119\nFailed to send HTTP request", "errCauses": [{"error": "Post \"http://172.17.0.1:49158\": dial tcp 172.17.0.1:49158: i/o timeout"}]}
```
我们看到网卡 docker0 的 IP 地址是 172.18.0.1 ，而非 docker 默认的 172.17.0.1 ，这可能是因为物理网卡 eth0 已经占据了 172.17.0.0/16 网段，docker 改变了自己的网段。
同时，CVAT 不知为何坚持访问不存在的地址 172.17.0.1 ，所以无法工作。

```
# ifconfig
br-380064f8c183: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 172.20.0.1  netmask 255.255.0.0  broadcast 172.20.255.255
        inet6 fe80::42:9fff:feca:f345  prefixlen 64  scopeid 0x20<link>
        ether 02:42:9f:ca:f3:45  txqueuelen 0  (Ethernet)
        RX packets 29  bytes 4729 (4.6 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 62  bytes 245967 (240.2 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

docker0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 172.18.0.1  netmask 255.255.0.0  broadcast 172.18.255.255
        inet6 fe80::42:98ff:fedd:38ef  prefixlen 64  scopeid 0x20<link>
        ether 02:42:98:dd:38:ef  txqueuelen 0  (Ethernet)
        RX packets 29  bytes 4729 (4.6 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 62  bytes 245967 (240.2 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 172.17.191.220  netmask 255.255.240.0  broadcast 172.17.191.255
        inet6 2408:400a:3c:8602:2845:ec66:a26b:2a01  prefixlen 128  scopeid 0x0<global>
        inet6 fe80::216:3eff:fe0e:ab82  prefixlen 64  scopeid 0x20<link>
        ether 00:16:3e:0e:ab:82  txqueuelen 1000  (Ethernet)
        RX packets 72085  bytes 19274377 (18.3 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 79893  bytes 31548598 (30.0 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

解决办法：
(0) 关闭 docker 。
(1) 修改物理网卡的地址 `ifconfig eth0 172.29.0.183 netmask 255.255.0.0` 。
(2) 修改 docker0 的地址为 172.17.0.1 。`ifconfig docker0 172.17.0.1 netmask 255.255.0.0` 。
(3) 重启 docker 。

## 升级 cvat_redis

docker-compose.redis5.yml :

```
  cvat_redis:
    container_name: cvat_redis
    image: redis:5.0.14-alpine
```

docker-compose -f docker-compose.redis5.yml -f components/serverless/docker-compose.serverless.yml up -d

## 更新/升级 CVAT
从 1.7 升级到 2.2
```
mv cvat.git cvat.git.bk

wget https://github.com/opencv/cvat/archive/refs/tags/v1.7.0.zip -O cvat-v1.7.0.zip
unzip cvat-v1.7.0.zip

wget https://github.com/opencv/cvat/archive/refs/tags/v2.2.0.zip -O cvat-v2.2.0.zip
unzip cvat-v2.2.0.zip

mv cvat-2.2.0 cvat.git
```

[1.2]
因为更新可能涉及数据库结构的变化，所以最好先备份数据，再更新。

```
docker-compose pull
```
输出：
```
Pulling cvat_db    ... done
Pulling cvat_redis ... done
Pulling cvat       ... done
Pulling cvat_ui    ... done
Pulling traefik    ... done
```

然后关闭、再启动 docker 容器：

```
docker-compose down
docker-compose up -d
```

更新过程可能比较耗时，可以用 `docker logs cvat` 查看日志。

但这只更新了 cvat 本体，serverless function 还没更新。

```
docker-compose -f docker-compose.yml -f components/serverless/docker-compose.serverless.yml pull
```
## 备份和恢复数据

cvat 的本地存储位置：

```
ls -l /var/lib/docker/volumes/

drwx-----x 3 root root     19 8月  20 13:28 cvat-1_cvat_data
drwx-----x 3 root root     19 8月  20 13:28 cvat-1_cvat_db
drwx-----x 3 root root     19 9月   1 19:54 cvat-1_cvat_events
drwx-----x 3 root root     19 8月  20 13:28 cvat-1_cvat_keys
drwx-----x 3 root root     19 8月  20 14:26 cvat-1_cvat_letsencrypt
drwx-----x 3 root root     19 8月  20 13:28 cvat-1_cvat_logs

drwx-----x 3 root root     19 9月   1 19:54 nuclio-local-storage
drwx-----x 3 root root     19 9月   2 00:57 nuclio-nuclio-openvino-dextr
drwx-----x 3 root root     19 9月   1 20:11 nuclio-nuclio-openvino-omz-intel-person-reidentification-retail-0300
drwx-----x 3 root root     19 9月   3 14:25 nuclio-nuclio-openvino-omz-public-faster_rcnn_inception_v2_coco
```

注意前缀 cvat-1 取自 docker-compose 运行的目录名（cvat 源码目录）。因此，如果把 cvat 源码目录改名，会看不到原来的数据。
各个卷的内容（除去前缀）：
cvat_db 是主数据库，PostgreSQL 格式，挂载到容器 cvat_db 的 /var/lib/postgresql/data 目录。
cvat_data 是主文件存储，包括图片视频等，挂载到容器 cvat 的 /home/django/data 目录。
cvat_logs: used to store logs of CVAT backend processes managed by supevisord. Mounted into cvat container by /home/django/logs

备份时，先停止所有 cvat 相关容器。要用 docker stop $(docker ps -q) 来停止，不要用 docker-compose down ，后者会让下一步找不到容器。

然后

```
mkdir backup
docker run --rm --name temp_backup --volumes-from cvat_db -v $(pwd)/backup:/backup ubuntu tar -cjvf /backup/cvat_db.tar.bz2 /var/lib/postgresql/data
docker run --rm --name temp_backup --volumes-from cvat -v $(pwd)/backup:/backup ubuntu tar -cjvf /backup/cvat_data.tar.bz2 /home/django/data
# [optional]
docker run --rm --name temp_backup --volumes-from cvat_elasticsearch -v $(pwd)/backup:/backup ubuntu tar -cjvf /backup/cvat_events.tar.bz2 /usr/share/elasticsearch/data
```

应当产生以下文件：
cvat_data.tar.bz2  cvat_db.tar.bz2  cvat_events.tar.bz2

恢复数据

```
cd <path_to_backup_folder>
docker run --rm --name temp_backup --volumes-from cvat_db -v $(pwd):/backup ubuntu bash -c "cd /var/lib/postgresql/data && tar -xvf /backup/cvat_db.tar.bz2 --strip 4"
docker run --rm --name temp_backup --volumes-from cvat -v $(pwd):/backup ubuntu bash -c "cd /home/django/data && tar -xvf /backup/cvat_data.tar.bz2 --strip 3"
# [optional]
docker run --rm --name temp_backup --volumes-from cvat_elasticsearch -v $(pwd):/backup ubuntu bash -c "cd /usr/share/elasticsearch/data && tar -xvf /backup/cvat_events.tar.bz2 --strip 4"
```


/home/django/data/data/6/raw/1624496810975.mp4

/home/django/data/data/6/original/0.mp4
/home/django/data/data/6/original/1.mp4
/home/django/data/data/6/original/47.mp4
/home/django/data/data/6/original/48.mp4

/home/django/data/data/6/compressed/0.zip
/home/django/data/data/6/compressed/1.zip
/home/django/data/data/6/compressed/47.zip
/home/django/data/data/6/compressed/48.zip

$ du -d 1 -h /home/django/data/data/6/
437M    /home/django/data/data/6/compressed
240M    /home/django/data/data/6/original
55M     /home/django/data/data/6/raw
730M    /home/django/data/data/6/

$ du -d 1 -h /home/django/data/data/1/
0       /home/django/data/data/1/compressed
0       /home/django/data/data/1/original
2.2M    /home/django/data/data/1/raw
2.2M    /home/django/data/data/1/

## 卸载 cvat
先停止 cvat 的 docker-compose：
```
docker-compose down
```
再删除相关的 docker volume：
```
docker volume ls
DRIVER    VOLUME NAME
local     5eea4f81031e0a20d2b37c89fc5265d4b18792f420a68f5a5d367f5b91f783a8
local     74362b79b6bc7fc8cbb32afa7d7d6c9307b8acb73ef347620d8e606bc785f24a
local     c5442b7ecac8fec9a1247efdd0ebe6cb0f92e74dc955a54aa7c9e3c04af4171b
local     cvatgit_cvat_data
local     cvatgit_cvat_db
local     cvatgit_cvat_events
local     cvatgit_cvat_keys
local     cvatgit_cvat_logs
local     nuclio-local-storage

docker volume rm cvatgit_cvat_data

docker volume rm nuclio-local-storage
```
## 查看cvat数据库

查看 docker-compose.yml 确认数据库设置：

```
services:
  cvat_db:
    container_name: cvat_db
    environment:
      POSTGRES_USER: root
      POSTGRES_DB: cvat
      POSTGRES_HOST_AUTH_METHOD: trust
    volumes:
      - cvat_db:/var/lib/postgresql/data
    networks:
      - cvat
```
通过 docker 确认网络参数，且可以连接：
```
docker network ls

NETWORK ID     NAME          DRIVER    SCOPE
dfe4196e5337   bridge        bridge    local
63f52b94aba4   cvat-1_cvat   bridge    local
ccbda7dfdf86   host          host      local
4e658f65e6c0   none          null      local

docker network inspect cvat-1_cvat

"a781615dac275c399c487b2ce3175cba168eaa5e711f9753652f511a0caaf75a": {
  "Name": "cvat_db",
  "IPv4Address": "172.30.0.3/16",
  "IPv6Address": ""
},

tcping 172.30.0.3 5432

172.30.0.3 port 5432 open
```
本机端口映射（连接本机cvat时可以省略）：
```
autossh -p22 -CNg -L 5432:172.30.0.3:5432 root@label-1.ai-parents.cn
```
用 dbeaver 连接 localhost:5432 ，数据库类型 postgresql，数据库名 cvat（不能省略），用户名 root ，密码空。

### 数据库结构

```
// 项目
engine_project {
  id,
  name,
}

// 任务
engine_task {
  id,
  name,
  project_id, // 引用 engine_project.id
}

// task 的一个分块，与 engine_job 是 1:1 关系
engine_segment {
  id,
  start_frame,
  stop_frame,
  task_id, // 引用 engine_task.id
}

// 工作包（job），与 engine_segment 是 1:1 关系
engine_job {
  id,
  segment_id, // 引用 engine_segment.id
}

// shape 标注的实例
engine_labeledshape {
  id,
  job_id, // 引用 engine_job.id
  frame,
  label_id,
  points,
}

// track 标注的实例
engine_labeledtrack {
  id,
  job_id, // 引用 engine_job.id
  label_id,
}

// tracked shape, track 所属的 shape
engine_trackedshape {
  track_id, // 引用 engine_labeledtrack.id
}

```
## 本地开发

```
docker-compose -f docker-compose.yml -f docker-compose.dev.yml build
docker-compose up -d
```
加上 `-f docker-compose.dev.yml` 后，会在本地构建 docker 镜像，否则，build 实际上什么也不做。

## 命令行工具

```
python /var/test/cvat-develop/utils/cli/cli.py --auth admin:xxxyyy --server-host label-1.ai-parents.cn --server-port 443 --https import /media/label-data-1/work/人体接触数据集1期/bc-2022-07--01/cvat_task_pack_d/bc-2022-07--01.zip
```

## 问题

### 登录失败，http 500 错误，磁盘使用率高
查看日志：`docker logs cvat_server` ：
```
2024-08-05 08:47:51,121 DEBG 'uvicorn-1' stdout output:
INFO:     192.168.8.76:0 - "GET /api/server/health/?format=json&org= HTTP/1.0" 500 Internal Server Error

2024-08-05 08:47:54,794 DEBG 'uvicorn-1' stderr output:
[2024-08-05 08:47:54,793] ERROR health-check: warning: 0c4b7c8dea9e 90.5% disk usage exceeds 90%
Traceback (most recent call last):
  File "/opt/venv/lib/python3.10/site-packages/health_check/backends.py", line 30, in run_check
    self.check_status()
  File "/opt/venv/lib/python3.10/site-packages/health_check/contrib/psutil/backends.py", line 21, in check_status
    raise ServiceWarning(
health_check.exceptions.ServiceWarning: warning: 0c4b7c8dea9e 90.5% disk usage exceeds 90%
```

也可以通过 health_check 功能查看
```
docker exec -t cvat_server python manage.py health_check

Cache backend: default   ... working
Cache backend: media     ... working
DatabaseBackend          ... working
DiskUsage                ... warning: 0c4b7c8dea9e 90.5% disk usage exceeds 90%
MemoryUsage              ... working
OPAHealthCheck           ... working
```

虽然磁盘使用率高（90%）是个警告而非错误，但仍然会导致 cvat 2.x 无法登录。降低磁盘使用率可以解决。磁盘使用率指的是 docker 数据目录（/var/lib/docker/）所在的文件系统。
