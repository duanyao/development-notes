## 参考资料
[1.1] https://github.com/open-mmlab/mmskeleton/blob/master/doc/GETTING_STARTED.md

[1.5] installation issue (unknown file type '.pyx') https://github.com/open-mmlab/mmskeleton/issues/292

[1.6] cudaGetDevice() failed. Status: CUDA driver version is insufficient for CUDA runtime version解决 https://www.machunjie.com/trouble/110.html

[1.7] ImportError: cannot import name 'PILLOW_VERSION' https://blog.csdn.net/ternence_hsu/article/details/103821264

[1.8] [build] Raise in cmake when seeing NVCC{9/9.1} + GCC6 combo https://github.com/pytorch/pytorch/pull/8863

[2.1] 旧版　st-gcn　代码的安装使用 https://github.com/yysijie/st-gcn/blob/master/OLD_README.md

[3.1] kinetics-skeleton 数据和 st-gcn 旧版模型和　openpose coco 模型
https://pan.baidu.com/s/1dwKG2TLvG-R1qeIiE4MjeA#list/path=%2Fsharelink1842042542-74981645138189%2FAAAI18&parentPath=%2Fsharelink1842042542-74981645138189

[3.2] mmskeleton 用到的预训练模型
https://pan.baidu.com/s/1iqOoQmIywuDQckgmehQ8HQ

## 新版 mmskeleton
见 [1.1-1.8] 。

### 下载源码
git clone https://github.com/open-mmlab/mmskeleton.git ~/project/mmskeleton.git

### 安装

#### 前置步骤

安装 nvidia 源的 cuda-toolkit-10-0, 卸载非 nvidia 源的 cuda（如 nvidia-cuda-dev 9.1, nvidia-cuda-toolkit 9.1, libcupti-dev 9.1）。

默认编译器改为 gcc-6 或 gcc-7 。避免 cuda 9.x 和 gcc-6 的组合，因为会有 bug 。

测试过的 python 版本为 3.7 (3.5 确定不兼容)；torch 版本为 1.4.0 。

#### venv 方式安装

创建 `>= 3.7` 的 venv ，如 `~/opt/venv/v37`。然后：

```
~/opt/venv/v37/bin/activate

pip install torch torchvision cython 'pillow<7.0.0'

cd ~/project/mmskeleton.git/

python setup.py develop
```

会出现以下错误：
```
building 'mmskeleton.ops.nms.gpu_nms' extension
error: unknown file type '.pyx' (from 'mmskeleton/ops/nms/gpu_nms.pyx')
```

修正错误并继续：

```
cd mmskeleton/ops/nms/
python setup_linux.py build

# 假定 python 版本是 3.7，如果不是则相应修改
cp build/temp.linux-x86_64-3.7/*.o ~/project/mmskeleton.git/build/temp.linux-x86_64-3.7/mmskeleton/ops/nms/
cp build/lib.linux-x86_64-3.7/*.so ~/project/mmskeleton.git/build/lib.linux-x86_64-3.7/mmskeleton/ops/nms/

cd ~/project/mmskeleton.git/
python setup.py develop

# 会自动下载、编译、安装 mmdetection，依赖 cuda
python setup.py develop --mmdet
```

### conda 方式

不推荐，最好用 venv 方式安装。

[1.1]
```
. ~/opt/miniconda3/bin/activate
conda create -n open-mmlab python=3.7 -y

# 位置 /home/duanyao/opt/miniconda3/envs/open-mmlab

conda activate open-mmlab

# conda install pytorch torchvision -c pytorch
conda install pytorch torchvision cython 'pillow<7.0.0'

cd ~/project/mmskeleton.git/

python setup.py develop
```

错误：
```
building 'mmskeleton.ops.nms.gpu_nms' extension
error: unknown file type '.pyx' (from 'mmskeleton/ops/nms/gpu_nms.pyx')
```

修复[1.5]：
```
conda install cython

cd mmskeleton/ops/nms/
python setup_linux.py build

cp build/temp.linux-x86_64-3.7/*.o ~/project/mmskeleton.git/build/temp.linux-x86_64-3.7/mmskeleton/ops/nms/
cp build/lib.linux-x86_64-3.7/*.so ~/project/mmskeleton.git/build/lib.linux-x86_64-3.7/mmskeleton/ops/nms/

cd ~/project/mmskeleton.git/
python setup.py develop
python setup.py develop --mmdet # 可选
```

错误2：

```
$ python mmskl.py pose_demo
Traceback (most recent call last):
  File "mmskl.py", line 7, in <module>
    import mmskeleton
  File "/home/duanyao/project/mmskeleton.git/mmskeleton/__init__.py", line 2, in <module>
    from . import datasets, processor, models, ops, apis
  File "/home/duanyao/project/mmskeleton.git/mmskeleton/datasets/__init__.py", line 1, in <module>
    from .coco import COCODataset
  File "/home/duanyao/project/mmskeleton.git/mmskeleton/datasets/coco.py", line 19, in <module>
    from ..ops.nms.nms import oks_nms
  File "/home/duanyao/project/mmskeleton.git/mmskeleton/ops/nms/nms.py", line 14, in <module>
    from .gpu_nms import gpu_nms
ImportError: /home/duanyao/project/mmskeleton.git/mmskeleton/ops/nms/gpu_nms.cpython-37m-x86_64-linux-gnu.so: undefined symbol: cudaSetupArgument
```

```
$ conda list | grep cudatoolkit
cudatoolkit               10.1.243             h6bb024c_0    https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main

$ which nvcc
/usr/bin/nvcc

$ nvcc --version
nvcc: NVIDIA (R) Cuda compiler driver
Copyright (c) 2005-2017 NVIDIA Corporation
Built on Fri_Nov__3_21:07:56_CDT_2017
Cuda compilation tools, release 9.1, V9.1.85

$ conda search cudatoolkit
cudatoolkit                      9.0      h13b8566_0  pkgs/main           
cudatoolkit                      9.2               0  anaconda/pkgs/main  

$ conda install cudatoolkit=9.0

$ find mmskeleton -name "*.o" -exec rm {} \;
$ find mmskeleton -name "*.so" -exec rm {} \;
$ rm -rf build

$ pip uninstall mmdet mmskeleton

$ python setup.py develop
$ python setup.py develop --mmdet
```
错误3：
```
$ python mmskl.py pose_demo
Traceback (most recent call last):
  File "mmskl.py", line 7, in <module>
    import mmskeleton
  File "/home/duanyao/project/mmskeleton.git/mmskeleton/__init__.py", line 1, in <module>
    from . import utils
  File "/home/duanyao/project/mmskeleton.git/mmskeleton/utils/__init__.py", line 3, in <module>
    from .checkpoint import load_checkpoint, get_mmskeleton_url, cache_checkpoint
  File "/home/duanyao/project/mmskeleton.git/mmskeleton/utils/checkpoint.py", line 1, in <module>
    from mmcv.runner import load_checkpoint as mmcv_load_checkpoint
  File "/home/duanyao/opt/miniconda3/envs/open-mmlab/lib/python3.7/site-packages/mmcv-0.2.16-py3.7-linux-x86_64.egg/mmcv/runner/__init__.py", line 2, in <module>
    from .checkpoint import (load_checkpoint, load_state_dict, save_checkpoint,
  File "/home/duanyao/opt/miniconda3/envs/open-mmlab/lib/python3.7/site-packages/mmcv-0.2.16-py3.7-linux-x86_64.egg/mmcv/runner/checkpoint.py", line 11, in <module>
    import torchvision
  File "/home/duanyao/opt/miniconda3/envs/open-mmlab/lib/python3.7/site-packages/torchvision/__init__.py", line 2, in <module>
    from torchvision import datasets
  File "/home/duanyao/opt/miniconda3/envs/open-mmlab/lib/python3.7/site-packages/torchvision/datasets/__init__.py", line 9, in <module>
    from .fakedata import FakeData
  File "/home/duanyao/opt/miniconda3/envs/open-mmlab/lib/python3.7/site-packages/torchvision/datasets/fakedata.py", line 3, in <module>
    from .. import transforms
  File "/home/duanyao/opt/miniconda3/envs/open-mmlab/lib/python3.7/site-packages/torchvision/transforms/__init__.py", line 1, in <module>
    from .transforms import *
  File "/home/duanyao/opt/miniconda3/envs/open-mmlab/lib/python3.7/site-packages/torchvision/transforms/transforms.py", line 17, in <module>
    from . import functional as F
  File "/home/duanyao/opt/miniconda3/envs/open-mmlab/lib/python3.7/site-packages/torchvision/transforms/functional.py", line 5, in <module>
    from PIL import Image, ImageOps, ImageEnhance, PILLOW_VERSION
ImportError: cannot import name 'PILLOW_VERSION' from 'PIL' (/home/duanyao/opt/miniconda3/envs/open-mmlab/lib/python3.7/site-packages/PIL/__init__.py)
```
解决：
```
$ pip install 'pillow<7.0.0'
```

错误4：
```
python setup.py develop --mmdet

    /usr/include/c++/6/tuple:502:1: error: body of constexpr function ‘static constexpr bool std::_TC<<anonymous>, _Elements>::_NonNestedTuple() [with _SrcTuple = std::tuple<at::Tensor, at::Tensor, double, long int>&&; bool <anonymous> = true; _Elements = {at::Tensor, at::Tensor, double, long int}]’ not a return-statement
         }
     ^
    error: command '/usr/bin/nvcc' failed with exit status 1
    Running setup.py install for mmdet ... error
```

原因是 cuda9.0/9.1 的 nvcc 有 bug，但这个bug只是跟 gcc 6 组合时体现。所以改用 gcc 4.9 或者 cuda9.2

```
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-4.9 10
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-6 20

sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-4.9 10
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-6 20

sudo update-alternatives --install /usr/bin/cc cc /usr/bin/gcc 30
sudo update-alternatives --set cc /usr/bin/gcc

sudo update-alternatives --install /usr/bin/c++ c++ /usr/bin/g++ 30
sudo update-alternatives --set c++ /usr/bin/g++

sudo update-alternatives --config gcc # 选择 4.9
sudo update-alternatives --config g++ # 选择 4.9
```

错误5：
```
usr/bin/nvcc -I/home/duanyao/opt/miniconda3/envs/open-mmlab/lib/python3.7/site-packages/numpy/core/include -I/usr/include -I/home/duanyao/opt/miniconda3/envs/open-mmlab/include/python3.7m -c nms_kernel.cu -o build/temp.linux-x86_64-3.7/nms_kernel.o -arch=sm_35 --ptxas-options=-v -c --compiler-options '-fPIC'
/usr/lib/gcc/x86_64-linux-gnu/6/include/stddef.h(436): error: identifier "nullptr" is undefined
```

原因是 nvcc 选择 g++/gcc 版本的方式是写死的，在脚本 `/usr/lib/nvidia-cuda-toolkit/bin/g++` 和 `/usr/lib/nvidia-cuda-toolkit/bin/gcc` 里，按照 6、5、4.9、4.8 的顺序查找，有6就不会用4.9。
所以编辑他们，修改为：
```
#if g++-6 --version >/dev/null 2>&1; then  #cuda9.0/9.1 not compat with g++6
#	prog=g++-6
#elif g++-5 --version >/dev/null 2>&1; then
if g++-5 --version >/dev/null 2>&1; then
	prog=g++-5
elif g++-4.9 --version >/dev/null 2>&1; then
	prog=g++-4.9
#...
```

错误6：

```
$ python mmskl.py pose_demo
Load configuration information from ./configs/pose_estimation/pose_demo.yaml

Pose estimation:
Traceback (most recent call last):
  File "mmskl.py", line 121, in <module>
    main()
  File "mmskl.py", line 115, in main
    call_obj(**cfg.processor_cfg)
  File "/home/duanyao/project/mmskeleton.git/mmskeleton/utils/importer.py", line 24, in call_obj
    return import_obj(type)(**kwargs)
  File "/home/duanyao/project/mmskeleton.git/mmskeleton/processor/pose_demo.py", line 78, in inference
    model = init_pose_estimator(detection_cfg, estimation_cfg, device=0)
  File "/home/duanyao/project/mmskeleton.git/mmskeleton/apis/estimation.py", line 18, in init_pose_estimator
    detection_model = mmdet.apis.init_detector(detection_model_file,
  File "/home/duanyao/opt/miniconda3/envs/open-mmlab/lib/python3.7/site-packages/lazy_import-0.2.2-py3.7.egg/lazy_import/__init__.py", line 156, in __getattribute__
    _load_module(self)
  File "/home/duanyao/opt/miniconda3/envs/open-mmlab/lib/python3.7/site-packages/lazy_import-0.2.2-py3.7.egg/lazy_import/__init__.py", line 537, in _load_module
    msg.format(**modclass._lazy_import_error_strings)), None)
  File "<string>", line 3, in raise_from
ImportError: mmskeleton.utils.third_party attempted to use a functionality that requires module mmdet.apis, but it couldn't be loaded. Please install mmdet and retry.


$ python -c "from mmdet.apis import init_detector, inference_detector"

Traceback (most recent call last):
  File "<string>", line 1, in <module>
  File "/home/duanyao/opt/miniconda3/envs/open-mmlab/lib/python3.7/site-packages/mmdet/apis/__init__.py", line 2, in <module>
    from .inference import (inference_detector, init_detector, show_result,
  File "/home/duanyao/opt/miniconda3/envs/open-mmlab/lib/python3.7/site-packages/mmdet/apis/inference.py", line 11, in <module>
    from mmdet.core import get_classes
  File "/home/duanyao/opt/miniconda3/envs/open-mmlab/lib/python3.7/site-packages/mmdet/core/__init__.py", line 6, in <module>
    from .post_processing import *  # noqa: F401, F403
  File "/home/duanyao/opt/miniconda3/envs/open-mmlab/lib/python3.7/site-packages/mmdet/core/post_processing/__init__.py", line 1, in <module>
    from .bbox_nms import multiclass_nms
  File "/home/duanyao/opt/miniconda3/envs/open-mmlab/lib/python3.7/site-packages/mmdet/core/post_processing/bbox_nms.py", line 3, in <module>
    from mmdet.ops.nms import nms_wrapper
  File "/home/duanyao/opt/miniconda3/envs/open-mmlab/lib/python3.7/site-packages/mmdet/ops/__init__.py", line 2, in <module>
    from .dcn import (DeformConv, DeformConvPack, DeformRoIPooling,
  File "/home/duanyao/opt/miniconda3/envs/open-mmlab/lib/python3.7/site-packages/mmdet/ops/dcn/__init__.py", line 1, in <module>
    from .deform_conv import (DeformConv, DeformConvPack, ModulatedDeformConv,
  File "/home/duanyao/opt/miniconda3/envs/open-mmlab/lib/python3.7/site-packages/mmdet/ops/dcn/deform_conv.py", line 9, in <module>
    from . import deform_conv_cuda
ImportError: /home/duanyao/opt/miniconda3/envs/open-mmlab/lib/python3.7/site-packages/mmdet/ops/dcn/deform_conv_cuda.cpython-37m-x86_64-linux-gnu.so: undefined symbol: _ZN3c105ErrorC1ENS_14SourceLocationERKSs
```

删除除了 .git 目录的所有东西，然后
```
git reset --hard
```
然后重新编译安装。错误依旧。

单独安装 mmdetection

```
git clone https://github.com/open-mmlab/mmdetection.git mmdetection.git
cd mmdetection.git
pip install -r requirements/build.txt
pip install "git+https://github.com/cocodataset/cocoapi.git#subdirectory=PythonAPI"
pip install -v -e .  # or "python setup.py develop"
```
错误7：
```
python tools/collect_env.py

Traceback (most recent call last):
  File "tools/collect_env.py", line 12, in <module>
    from mmdet.ops import get_compiler_version, get_compiling_cuda_version
  File "/home/duanyao/project/mmdetection.git/mmdet/ops/__init__.py", line 2, in <module>
    from .dcn import (DeformConv, DeformConvPack, DeformRoIPooling,
  File "/home/duanyao/project/mmdetection.git/mmdet/ops/dcn/__init__.py", line 1, in <module>
    from .deform_conv import (DeformConv, DeformConvPack, ModulatedDeformConv,
  File "/home/duanyao/project/mmdetection.git/mmdet/ops/dcn/deform_conv.py", line 10, in <module>
    from . import deform_conv_cuda
ImportError: /home/duanyao/project/mmdetection.git/mmdet/ops/dcn/deform_conv_cuda.cpython-37m-x86_64-linux-gnu.so: undefined symbol: _ZN3c105ErrorC1ENS_14SourceLocationERKSs
```

解决：
* 系统安装 nvidia 源的 cuda-toolkit-10-0, 卸载 nvidia-cuda-dev 9.1, nvidia-cuda-toolkit 9.1, libcupti-dev 9.1. 
* 默认 编译器改为 gcc-6 .
* conda 安装最新的 pytorch 1.3.1 和 cudatoolkit 10.0.130
* 删除 mmdetection.git 内除了 .git 的文件，git reset --hard, 重新编译 python setup.py develop

正常了：
```
$ python tools/collect_env.py
sys.platform: linux
Python: 3.7.6 | packaged by conda-forge | (default, Jan  7 2020, 22:33:48) [GCC 7.3.0]
CUDA available: True
CUDA_HOME: /usr/local/cuda
NVCC: Cuda compilation tools, release 10.0, V10.0.130
GPU 0: GeForce 940MX
GCC: gcc (Debian 6.3.0-18+deb9u1) 6.3.0 20170516
PyTorch: 1.3.1
PyTorch compiling details: PyTorch built with:
  - GCC 7.3
  - Intel(R) Math Kernel Library Version 2019.0.4 Product Build 20190411 for Intel(R) 64 architecture applications
  - Intel(R) MKL-DNN v0.20.5 (Git Hash 0125f28c61c1f822fd48570b4c1066f96fcb9b2e)
  - OpenMP 201511 (a.k.a. OpenMP 4.5)
  - NNPACK is enabled
  - CUDA Runtime 10.0
  - NVCC architecture flags: -gencode;arch=compute_35,code=sm_35;-gencode;arch=compute_50,code=sm_50;-gencode;arch=compute_60,code=sm_60;-gencode;arch=compute_61,code=sm_61;-gencode;arch=compute_70,code=sm_70;-gencode;arch=compute_75,code=sm_75;-gencode;arch=compute_50,code=compute_50
  - CuDNN 7.6.5
    - Built with CuDNN 7.6.4
  - Magma 2.5.0
  - Build settings: BLAS=MKL, BUILD_NAMEDTENSOR=OFF, BUILD_TYPE=Release, CXX_FLAGS=-fvisibility-inlines-hidden -std=c++11 -fmessage-length=0 -march=nocona -mtune=haswell -ftree-vectorize -fPIC -fstack-protector-strong -fno-plt -O2 -ffunction-sections -pipe -I/opt/conda/conda-bld/pytorch_1574381331675/_h_env_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_place/include -fdebug-prefix-map=/opt/conda/conda-bld/pytorch_1574381331675/work=/usr/local/src/conda/pytorch-1.3.1 -fdebug-prefix-map=/opt/conda/conda-bld/pytorch_1574381331675/_h_env_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_placehold_place=/usr/local/src/conda-prefix -Wno-deprecated -fvisibility-inlines-hidden -fopenmp -DUSE_FBGEMM -DUSE_QNNPACK -DUSE_PYTORCH_QNNPACK -O2 -fPIC -Wno-narrowing -Wall -Wextra -Wno-missing-field-initializers -Wno-type-limits -Wno-array-bounds -Wno-unknown-pragmas -Wno-sign-compare -Wno-unused-parameter -Wno-unused-variable -Wno-unused-function -Wno-unused-result -Wno-strict-overflow -Wno-strict-aliasing -Wno-error=deprecated-declarations -Wno-stringop-overflow -Wno-error=pedantic -Wno-error=redundant-decls -Wno-error=old-style-cast -fdiagnostics-color=always -faligned-new -Wno-unused-but-set-variable -Wno-maybe-uninitialized -fno-math-errno -fno-trapping-math -Wno-stringop-overflow, DISABLE_NUMA=1, PERF_WITH_AVX=1, PERF_WITH_AVX2=1, PERF_WITH_AVX512=1, USE_CUDA=True, USE_EXCEPTION_PTR=1, USE_GFLAGS=OFF, USE_GLOG=OFF, USE_MKL=ON, USE_MKLDNN=ON, USE_MPI=OFF, USE_NCCL=ON, USE_NNPACK=ON, USE_OPENMP=ON, USE_STATIC_DISPATCH=OFF, 

TorchVision: 0.4.2
OpenCV: 4.2.0
MMCV: 0.2.16
MMDetection: 1.0.0+1d7b0a7
MMDetection Compiler: GCC 6.3
MMDetection CUDA Compiler: 10.0
```

```
$ python mmskl.py pose_demo
Load configuration information from ./configs/pose_estimation/pose_demo.yaml

Pose estimation:
Downloading: "https://s3.ap-northeast-2.amazonaws.com/open-mmlab/mmdetection/models/cascade_rcnn_r50_fpn_20e_20181123-db483a09.pth" to /home/duanyao/.cache/torch/checkpoints/cascade_rcnn_r50_fpn_20e_20181123-db483a09.pth
```

```
*.pkl file will be generated by running :
python tools/kinetics_gendata.py --data_path <path to kinetics-skeleton>
```

### 测试/推理

预训练模型从 [3.2] 下载，放在 `~/.cache/torch/checkpoints` ，如下：

```
cascade_rcnn_r50_fpn_20e_20181123-db483a09.pth
htc_dconv_c3-c5_mstrain_400_1400_x101_64x4d_fpn_20e_20190408-0e50669c.pth
pose_hrnet_w32_256x192-76ea353b.pth
st_gcn.kinetics-6fa43f73.pth
st_gcn.ntu-xsub-300b57d4.pth
st_gcn.ntu-xview-9ba67746.pth
```
然后可以运行

```
$ python mmskl.py pose_demo
Load configuration information from ./configs/pose_estimation/pose_demo.yaml

Pose estimation:
[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] 300/300, 0.6 task/s, elapsed: 538s, ETA:     0s

Generate video:
[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] 300/300, 679.7 task/s, elapsed: 0s, ETA:     0s
Video was saved to work_dir/pose_demo/skateboarding.mp4
```

## 旧版 st-gcn

### 安装

先要安装 openpose 及其 python API，详见 AI_openpose.md 。

下载代码：
```
git clone https://github.com/yysijie/st-gcn.git st-gcn.git
cd st-gcn.git
```

模型下载[3.1]到 st-gcn.git/models 目录。

```
pip install -r requirements.txt

cd torchlight; python setup.py install; cd ..
```

### 运行 demo

```
python main.py demo_offline --video resource/media/ta_chi.mp4 --openpose /usr/local/        # 处理完整个视频后再显示结果
python main.py demo --video resource/media/ta_chi.mp4 --openpose /usr/local/                # 边处理视频边显示结果
python main.py demo --video camera_source --fps 1 --openpose /usr/local/                    # 从摄像头采集视频并同时显示结果
```

注意 `--openpose` 的写法：它期待 `/usr/local/python/openpose` 目录包含 openpose 的 python API 。

代码 `processor/demo_realtime.py` 原本缺少 `--fps` 参数，可以这样加上：


```
@@ -184,10 +184,14 @@ class DemoRealtime(IO):
                             type=int)
         parser.add_argument('--height',
                             default=1080,
                             type=int,
                             help='height of frame in the output video.')
+        parser.add_argument('--fps',
+                            default=1.0,
+                            type=float,
+                            help='fps of camera.')
         parser.set_defaults(
```

内存1.8GB，显存 1.9GB 。

### 低帧率测试
cd resource/media
ffmpeg -i skateboarding.mp4 -r 0.5 skateboarding-r0.5.mp4
ffmpeg -i skateboarding-r0.5.mp4 -r 30 skateboarding-r0.5-r30.mp4

python main.py demo_offline --openpose /usr/local/ --video resource/media/skateboarding.mp4 
skateboarding

python main.py demo --openpose /usr/local/ --video resource/media/skateboarding-r0.5-r30.mp4
abseiling

python main.py demo_offline --openpose /usr/local/ --video resource/media/skateboarding-r0.5.mp4
triple jump

python main.py demo_offline --openpose /usr/local/ --video resource/media/skateboarding-r2.mp4 
triple jump

python main.py demo_offline --openpose /usr/local/ --video resource/media/skateboarding-r8.mp4 
parkour

python main.py demo_offline --openpose /usr/local/ --video resource/media/skateboarding-r15.mp4
parkour

python main.py demo_offline --openpose /usr/local/ --video resource/media/skateboarding-r8-r30.mp4
skateboarding

python main.py demo_offline --openpose /usr/local/ --video resource/media/skateboarding-r2-r30.mp4
skiing

python main.py demo_offline --openpose /usr/local/ --video resource/media/skateboarding-r2-4s.mp4 

### 准备数据集
```
python tools/kinetics_gendata.py --data_path /home/duanyao/Downloads/kinetics-skeleton
```
kinetics-skeleton 目录结构为：
  - kinetics_train/
  - kinetics_train_label.json
  - kinetics_val/
  - kinetics_val_label.json


`kinetics_val` 和 `kinetics_train` 下是平铺的数据文件，每个表示一个视频，文件名形如：`_0xo7EkS0tE.json` 。其格式为：

```
{
  data: [
    {
      frame_index: 1,
      skeleton: [
        {
          pose: [0.000, 0.000, 0.894, 0.535,...],   // 36 元素，x1, y1, x2, y2 排列，根据宽高归一化。
          score: [0.000, 0.159, 0.180, 0.000, ...]
        }, 
        { pose: [0.000, 0.000, 0.894, 0.535,...], score: [0.000, 0.159, 0.180, 0.000, ...] },
      ]
    },
    {
      frame_index: 2,
      skeleton: [...]
    }
  ],
  "label": "eating watermelon",
  "label_index": 117
}
```

`kinetics_xxx_label.json` 的格式为：

```
{
    "--07WQ2iBlw": {
        "has_skeleton": true, 
        "label": "javelin throw", 
        "label_index": 166
    }, 
    "--33Lscn6sk": {
        "has_skeleton": true, 
        "label": "flipping pancake", 
        "label_index": 129
    },
}
```

其内容与 `kinetics_xxx/yyy.json` 中有所重复。
label_index 从 0 开始。

生成的文件在 data/Kinetics/kinetics-skeleton/
  - train_data.npy
  - val_data.npy

npy 是 numpy 数组的序列化形式。其形状为 (N, C, T, V, M)，分别是：样本数、通道数（3,即 x, y, score）、帧数（最大300）、关节数（一般18）、人数（最大2）。

### 在数据集上测试

```
python main.py recognition -c config/st_gcn/kinetics-skeleton/test.yaml --test_batch_size 32
```
默认的 test_batch_size 是 64，占用显存超过 2GB，改为 32 后使用显存1.8GB。
测试使用的是 `data/Kinetics/kinetics-skeleton/val_data.npy` ，包含约2.5万个视频，每个视频约10秒，30帧/秒。

测试用时约35min，GeForce 940MX 。等效于119秒/秒，或3571帧/秒。

结果：

```
Model:   net.st_gcn.Model.
Weights: ./models/st_gcn.kinetics.pt.
Evaluation Start:
mean_loss: 3.2329459240440404
Top1: 31.60%
Top5: 53.68%
Done.
```

### 自定义数据集

#### 训练

```
python main.py recognition -c config/st_gcn/kinetics-skeleton/train.yaml
```
300 个样本，其中 270 个用于训练，batch_size 30。
样本帧率 7.5，平均时长 3 秒。
内存占用 1.2GB，显存占用 1861MiB，GPU 占用 97%。
轮数 50，用时 310s 。
总帧数 6075，每一帧用时 0.051s。

```
[05.18.20|07:59:33] Training epoch: 0
[05.18.20|07:59:34] 	Iter 0 Done. | loss: 1.1440 | lr: 0.100000
[05.18.20|07:59:39] 	mean_loss: 3.286774476369222
[05.18.20|07:59:39] Time consumption:
[05.18.20|07:59:39] Done.
[05.18.20|07:59:39] Training epoch: 1
[05.18.20|07:59:44] 	mean_loss: 1.3592397371927898
[05.18.20|07:59:44] Time consumption:
[05.18.20|07:59:44] Done.
[05.18.20|07:59:44] Training epoch: 2
[05.18.20|07:59:50] 	mean_loss: 1.22970712184906
[05.18.20|07:59:50] Time consumption:
[05.18.20|07:59:50] Done.

[05.18.20|08:04:35] Training epoch: 49
[05.18.20|08:04:40] 	mean_loss: 1.099425779448615
[05.18.20|08:04:40] Time consumption:
[05.18.20|08:04:40] Done.
[05.18.20|08:04:40] The model has been saved as ./work_dir/recognition/kinetics_skeleton/ST_GCN/epoch50_model.pt.
[05.18.20|08:04:40] Eval epoch: 49
[05.18.20|08:04:44] 	mean_loss: 1.2117897272109985
[05.18.20|08:04:44] 	Top1: 40.00%
[05.18.20|08:04:44] 	Top5: 100.00%
[05.18.20|08:04:44] Done.
```

#### 测试
```
python main.py recognition -c config/st_gcn/kinetics-skeleton/test_c.yaml

[05.18.20|08:24:58] Model:   net.st_gcn.Model.
[05.18.20|08:24:58] Weights: ./work_dir/recognition/kinetics_skeleton/ST_GCN/epoch50_model.pt.
[05.18.20|08:24:58] Evaluation Start:
[05.18.20|08:25:02] 	mean_loss: 1.2117897272109985
[05.18.20|08:25:02] 	Top1: 40.00%
[05.18.20|08:25:02] 	Top5: 100.00%
[05.18.20|08:25:02] Done.
```

样本数 30，平均时长 3秒。测试用时 4 秒。速度：22 秒/秒。每一帧用时 0.0059s 。

#### 测试视频识别

```
main.py demo_offline -c config/st_gcn/kinetics-skeleton/demo_offline_c.yaml --video /home/duanyao/data-development/ai-dataset/hmdb51/videos-7.5/walk/20060723sfjffbitemebaby_walk_u_nm_np1_fr_med_0.mp4 --fps 7.5 --openpose /usr/local/ --openpose_model_dir /home/duanyao/t-project/openpose.git/models --result_video work_dir/20060723sfjffbitemebaby_walk_u_nm_np1_fr_med_0.stgcn.mp4
```

### api

#### class `naive_pose_tracker`

`update(self, multi_pose, current_frame): void`
  - `multi_pose: numpy.ndarray<float>` openpose 的输出结果，3维，`[帧内人序号][关节序号][x,y,置信度]`
  
  multi_pose 输入之前要进行正规化：x,y 分别除以画面宽度、高度，减0.5；置信度为0则 x,y 设置为0。

`get_skeleton_sequence(): data`

  - data: numpy.ndarray<double> 4 维
    0. 关节的(x,y,置信度), 大小固定3
    1. 轨迹内帧序号，固定 128。如果数据不足，前部填0。如果该轨迹当前已消失，后部填零。最后一项表示当前视频帧。
    2. 关节序号，大小固定 18
    3. 轨迹（人）序号，大小是人数

`trace_info: list<(trace, latest_frame:int)>`
  - 序号: 轨迹（人）序号
  - `trace: numpy.ndarray<float>` 3维，`[骨架序号][关节序号][x,y,置信度]`
    如果一个人每一帧都出现，那么骨架序号等于帧序号。
  - `latest_frame`: 此轨迹（人）出现的最后一视频帧的序号

#### class `processor.demo_realtime.DemoRealtime`

`predict(data): voting_label_name, video_label_name, output, intensity`
  - `data: torch.Tensor<float>` 5维：
    0. 批处理序号，大小固定1
    1. 关节的(x,y,置信度), 归一化（实际大小除以宽或高），大小固定3
    2. 轨迹内帧序号，固定 128。如果数据不足，前部填0。
    3. 关节序号，大小固定 18。
    4. 轨迹（人）序号，大小是人数
  
  - `voting_label_name: string`，当前识别结果，例：'snowkiting'。这是累加所有人、所有帧、所有关节的动作分值，取最可能的一个动作。
  - `video_label_name: string[][]`, 每一帧每个人识别结果，2维，`[帧序号][人序号]`，例：'snowkiting'。这是累加一个人、一个帧、所有关节的动作分值，取最可能的一个动作。
  - `output`: 等同于 `net.st_gcn.Model.extract_feature(data): output`，消去轴0。
  - `intensity: numpy.ndarray<float>`, 3维, feature 的第0轴的均方根。
    0. 帧序号。大小是 `[32,]`
    1. 关节序号。大小是 18。
    2. 轨迹（人）序号。大小是人数。
    
`label_name: string[]`
  常量，大小固定 400，如 'abseiling', 'air drumming', 'answering questions' 。

#### class `net.st_gcn.Model`: `net/st_gcn.py`

`extract_feature(data): output, feature`

  - `data: torch.Tensor<float>` 5维：
    0. 批处理序号，大小固定1
    1. 关节的(x,y,置信度), 大小固定3
    2. 轨迹内帧序号，固定 128。如果数据不足，前部填0。
    3. 关节序号，大小固定 18
    4. 轨迹（人）序号，大小是人数

  - `output: torch.Tensor<float>` 5维：
    0. 批处理序号，大小固定1
    1. 动作类型序号。大小是 400。
    2. 帧序号。大小是 [32,]
    3. 关节序号。大小是 18。
    4. 轨迹（人）序号。大小是人数。

    值：概率或分值，表示属于此动作的可能性。
    表示预测结果。

  - `feature: torch.Tensor<float>` 5维：
    0. 批处理序号，大小固定1
    1. 动作类型序号? 大小是 256。
    2. 帧序号。大小是 [32,]
    3. 关节序号。大小是 18。
    4. 轨迹（人）序号。大小是人数。

    值：？
    表示预测结果的前一层？

#### function stgcn_visualize

```
def stgcn_visualize(pose, # 关键点，由 naive_pose_tracker.get_skeleton_sequence() 输出，固定128帧
                    edge, # list<tuple<i:int,j:int>>, 表示应该连接哪些关节点。i和j是关节序号。i和j可以相等。来自 DemoRealtime.model.graph.edge
                    feature, # DemoRealtime.predict() 输出的 intensity
                    video, # 视频帧序列 list<ndarray<unint8,w,h,c>>
                    label=None, # 动作结论（总体） string
                    label_sequence=None, # string[][]`, 每一帧每个人识别结果，2维，`[帧序号][人序号]`，
                    height=1080,
                    fps=None):
```
