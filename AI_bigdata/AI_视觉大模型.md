Computer Vision in the Wild (CVinW)
https://github.com/Computer-Vision-in-the-Wild/CVinW_Readings

Awesome-Open-Vocabulary
https://github.com/jianzongwu/Awesome-Open-Vocabulary

什么是Open-Vocabulary object detection？
https://www.zhihu.com/question/564426703

MetaCLIP Demystifying CLIP Data 2023
https://arxiv.org/abs/2309.16671
https://github.com/facebookresearch/MetaCLIP

OpenCLIP
https://github.com/mlfoundations/open_clip

No "Zero-Shot" Without Exponential Data: Pretraining Concept Frequency Determines Multimodal Model Performance 2024
https://huggingface.co/papers/2404.04125

GLIP: Grounded Language-Image Pre-training
https://github.com/microsoft/GLIP

RegionCLIP: Region-based Language-Image Pretraining
https://github.com/microsoft/RegionCLIP

Detecting Twenty-thousand Classes using Image-level Supervision
https://github.com/facebookresearch/Detic/

Object Detection in the Wild 
https://eval.ai/web/challenges/challenge-page/1839/overview

Grounded-Segment-Anything
https://github.com/IDEA-Research/Grounded-Segment-Anything

Grounding DINO Open-Set Detection. Detect everything with language
https://github.com/IDEA-Research/GroundingDINO

"DINO: DETR with Improved DeNoising Anchor Boxes for End-to-End Object Detection".
https://github.com/IDEA-Research/DINO

IDEA-Research / Grounded-Segment-Anything
https://github.com/IDEA-Research/Grounded-Segment-Anything

中科大和华为提出MVP：基于CLIP来进行视觉无监督训练
https://zhuanlan.zhihu.com/p/479221008

MVP: Multimodality-guided Visual Pre-training
https://arxiv.org/abs/2203.05175

开放世界目标检测的近期工作及简析！（基于Captioning/CLIP/伪标签/Prompt）2023
https://zhuanlan.zhihu.com/p/594654423

Open-vocabulary Object Detection via Vision and Language Knowledge Distillation 2021
https://arxiv.org/abs/2104.13921

Fine-tuned CLIP Models are Efficient Video Learners 2023
https://arxiv.org/abs/2212.03640
https://github.com/muzairkhattak/ViFi-CLIP

AIM: Adapting Image Models for Efficient Video Action Recognition 2023
https://arxiv.org/abs/2302.03024
https://adapt-image-models.github.io/

Revisiting Classifier: Transferring Vision-Language Models for Video Recognition 2023
https://arxiv.org/abs/2207.01297
https://github.com/whwu95/Text4Vis

Open-VCLIP: Transforming CLIP to an Open-vocabulary Video Model via Interpolated Weight Optimization 2023
https://arxiv.org/abs/2302.00624
https://github.com/wengzejia1/Open-VCLIP

A Guide to Fine-Tuning CLIP Models with Custom Data 2021
https://medium.com/aimonks/a-guide-to-fine-tuning-clip-models-with-custom-data-6c7c0d1416fb

CLIP Few Shot Recognition 2022
https://github.com/b-hahn/CLIP

CLIP Training Code #83 2021
https://github.com/openai/CLIP/issues/83

Robust fine-tuning of zero-shot models (open clip) 2021,2022
https://github.com/mlfoundations/wise-ft

Robust fine-tuning of zero-shot models 2021
https://arxiv.org/abs/2109.01903

Tip-Adapter: Training-free Adaption of CLIP for Few-shot Classification 2022
https://github.com/gaopengcuhk/Tip-Adapter
https://arxiv.org/pdf/2207.09519.pdf

Prompt, Generate, then Cache
https://github.com/OpenGVLab/CaFo

Prompt, Generate, then Cache: Cascade of Foundation Models makes Strong Few-shot Learners
https://arxiv.org/pdf/2303.02151.pdf

Enhancing Few-shot CLIP with Semantic-Aware Fine-Tuning 2023
https://arxiv.org/pdf/2311.04464.pdf

SLIP: Self-supervision meets Language-Image Pre-training 2022
https://github.com/facebookresearch/SLIP
https://arxiv.org/abs/2112.12750

FILIP: Fine-grained Interactive Language-Image Pre-Training 2021
https://arxiv.org/abs/2111.07783

VideoX - Multi-modal Video Content Understanding 2019-2023
2D-TAN, X-CLIP, SeqTrack 合集
https://github.com/microsoft/VideoX

Expanding Language-Image Pretrained Models for General Video Recognition 2022
视频分类
https://github.com/microsoft/VideoX/tree/master/X-CLIP
https://arxiv.org/abs/2208.02816

Multi-Scale 2D Temporal Adjacency Networks for Moment Localization with Natural Language (MS-2D-TAN) 2021
视频时域检测
https://arxiv.org/pdf/2012.02646

img2dataset
https://github.com/rom1504/img2dataset

InstantID: Zero-shot Identity-Preserving Generation in Seconds
https://arxiv.org/abs/2401.07519
https://github.com/InstantID/InstantID
https://mp.weixin.qq.com/s/Etl0HUVRdxwsgcM0ErqHjA
https://instantid.github.io/
https://huggingface.co/spaces/InstantX/InstantID

刚刚，字节版GPTs「扣子 Coze」上线了 [2024.2.1]
https://mp.weixin.qq.com/s/efNjbeK8Zul39nLzQuawCg

