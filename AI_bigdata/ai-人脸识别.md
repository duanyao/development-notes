## 参考资料

[1.0] 深度学习大讲堂
  https://zhuanlan.zhihu.com/dlclass

[1.1] 人脸检测深度学习
  https://zhuanlan.zhihu.com/p/25335957

[1.2] 人脸识别中的活体检测
  https://zhuanlan.zhihu.com/p/25401788

[1.3] 目前人脸识别技术的挑战是什么？
  https://www.zhihu.com/question/39032661

[1.4] 人脸检测与识别年度进展概述
  https://mp.weixin.qq.com/s/UMvleRIAQebAgQBxM4toJw
  https://zhuanlan.zhihu.com/p/27638463

[1.5] 视频行为识别年度进展 
  https://mp.weixin.qq.com/s?__biz=MzI1NTE4NTUwOQ==&mid=2650326555&idx=1&sn=ffb945f27814bb450b8de2d87087227d
  https://zhuanlan.zhihu.com/p/27415472

[1.6] 行人检测、跟踪与检索领域年度进展报告
  https://zhuanlan.zhihu.com/p/26807041

[1.7] 人脸识别哪家强？
  https://www.zhihu.com/question/37060782

[2.8] SeetaFace Engine is an open source C++ face recognition engine, which can run on CPU with no third-party dependence.
  https://github.com/seetaface/SeetaFaceEngine

[2.9] SeetaFace开源人脸识别引擎介绍
  https://zhuanlan.zhihu.com/p/22451474

[2.10] Dlib+OpenCV深度学习人脸识别
  http://blog.csdn.net/jcjx0315/article/details/73449315

[2.11] 人脸比对 SDK 离线调用 Face++的人脸检测、人脸比对、人脸追踪与关键点
  https://www.faceplusplus.com.cn/face-compare-sdk/
  
[3.6] Head Pose Image Database
  http://www-prima.inrialpes.fr/perso/Gourier/Faces/HPDatabase.html
  
[3.7] Estimating Face Orientation from Robust Detection of Salient Facial Features
  http://www-prima.inrialpes.fr/perso/Gourier/Pointing04-Gourier.pdf
  
[4.1] FaceBoxes: A CPU Real-time Face Detector with High Accuracy 
  https://github.com/zeusees/FaceBoxes
  
[4.2] Faceboxes 人脸检测及开源实现
  https://blog.csdn.net/lsy17096535/article/details/78644844
  
[4.3] Tensorflow实现卷积神经网络，用于人脸关键点识别
  https://blog.csdn.net/qq_45086997/article/details/90449369

[5.1] 智云视图
  http://www.zeusee.com/face_page.html
  
[6.0] opencv
[6.1] Haar Feature-based Cascade Classifier for Object Detection facedetect demo
  https://docs.opencv.org/master/d5/d54/group__objdetect.html
  https://github.com/opencv/opencv/blob/master/samples/cpp/dbt_face_detection.cpp
  
[6.2] opencv_contrib Collection of face recognition techniques
  https://github.com/opencv/opencv_contrib/tree/master/modules/face

[6.3] detect facial feature points using Haarcascade classifiers for face, eyes, nose and mouth
  https://github.com/opencv/opencv/blob/master/samples/cpp/facial_features.cpp

[7.0] OpenFace
  https://cmusatyalab.github.io/openface/
[7.1] 如何通过OpenFace实现人脸识别框架
  https://blog.csdn.net/heyc861221/article/details/80130033
  
[8.0] Tiny Face Detector that can find ~800 faces out of ~1000 reportedly present
  https://github.com/peiyunh/tiny
  https://github.com/varunagrawal/tiny-faces-pytorch

[10.0]
[10.3] Wei Lun Chao Face Recognition
  http://disp.ee.ntu.edu.tw/~pujols/Face%20Recognition-survey.pdf

[10.4] 人脸识别技术大总结1——Face Detection & Alignment
  http://www.cnblogs.com/sciencefans/p/4394861.html

[15.1] sensetime sdk-doc
https://sdk-doc.sensetime.com/
  
[16.1] 京东AI开放平台 人脸对比
http://neuhub.jd.com/ai/api/face/compare

[17.1] 万维识别 人脸识别
http://www.wvsense.com/core.html

## 概念和名词
人脸检测（Face detection）：给出图片中人脸的位置和大小，一般是给出一个包围盒。有些实现中将检测和校准/对齐步骤合一，统称人脸检测
人脸校准/人脸对齐（Face alignment）：给出人脸的一系列关键点/特征点的位置。比如鼻子左侧，鼻孔下侧，瞳孔位置，上嘴唇下侧。
人脸特征提取（Face feature extraction）：从人脸照片中提取一组特征数据。人脸识别实际上是在人脸特征数据上进行的。
人脸识别（Face Recognition）：identification 和 verification 的统称。
人脸检索/人脸识别（Face identification）：输入一张人脸照片，返回此人的身份。这通常涉及在一个有很多人的人脸库中进行检索。注意此处“人脸识别”有多重含义。
人脸校验/人脸对比（Face verification/Face compare）：输入一张人脸照片和一个身份，返回两者是否匹配。这通常涉及对比两张人脸照片的相似度。

Face identification 可以基于 Face verification 来实现：对于输入照片，逐一与人脸库中的照片对比，找出相似度最高的那个作为答案返回。

## 关于准确率的各种定义

## 测试/训练用数据集
### 头位测试数据
2790 monocular face images of 15 persons with variations of pan and tilt angles from -90 to +90 degrees. [3.6, 3.7]

测试结果

水平角度测试：Person01，不带眼睛，2018-11-19

实际角度\测试结果      微软       百度        face++
0                   5.0        -0.82      2.8
+15                 -3.6       4.55       14
-15                 17.6       -24        -19
+30                 -12.9      14.95      16
-30                 29.7       -39.31     -27
+45                 -21.9      29.08      29
-45                 20.6       -47.78     -35
+60                 -20.2      39         32
-60                 19.8       -61       -42

## 具体实现
### FaceBoxes
A CPU Real-time Face Detector with High Accuracy 开源
[4.1]

Tensorflow实现卷积神经网络，用于人脸关键点识别 [4.3]：练习教程。

### 百度

* 表情/情绪识别

  相对不倾向于中性表情。有些中性表情会被识别为其它表情。

  smile/laugh 与 happy 并不直接挂钩。可能有不笑但是 happy 较高的情况。


### 微软

* 表情/情绪识别

  倾向于中性表情。有些有表情会被识别为中性。

  smile/laugh 与 happy 并不直接挂钩。可能有不笑但是 happy 较高的情况。

## 基于 opencv 的

一个基于 Haar Feature-based Cascade Classifier 的人脸检测器[6.1]。

opencv_contrib Collection of face recognition techniques[6.2]：

* Eigen Faces
* Fisher Faces
* Local Binary Pattern Histograms

detect facial feature points using Haarcascade classifiers for face, eyes, nose and mouth[6.3]。

## 基于 openface 的
OpenFace is a Python and Torch implementation of face recognition with deep neural networks and is based on the CVPR 2015 paper FaceNet: A Unified Embedding for Face Recognition and Clustering by Florian Schroff, Dmitry Kalenichenko, and James Philbin at Google[7.0].

OpenFace 的工作方式[7.1]：
Input Image -> Detect

输入：原始的可能含有人脸的图像。

输出：人脸位置的bounding box。

这一步一般我们称之为“人脸检测”（Face Detection），在OpenFace中，使用的是dlib、OpenCV现有的人脸检测方法。此方法与深度学习无关，使用的特征是传统计算机视觉中的方法（一般是Hog、Haar等特征）。

Detect -> Transform -> Crop

输入：原始图像 + 人脸位置bounding box

输出：“校准”过的只含有人脸的图像

对于输入的原始图像 + bounding box，这一步要做的事情就是要检测人脸中的关键点，然后根据这些关键点对人脸做对齐校准。所谓关键点，就是下图所示的绿色的点，通常是眼角的位置、鼻子的位置、脸的轮廓点等等。有了这些关键点后，我们就可以把人脸“校准”，或者说是“对齐”。解释就是原先人脸可能比较歪，这里根据关键点，使用仿射变换将人脸统一“摆正”，尽量去消除姿势不同带来的误差。这一步我们一般叫Face Alignment。

在OpenFace中，这一步同样使用的是传统方法，特点是比较快，对应的论文是：
One Millisecond Face Alignment with an Ensemble of Regression Trees

Crop -> Representation

输入：校准后的单张人脸图像

输出：一个向量表示。

这一步就是使用深度卷积网络，将输入的人脸图像，转换成一个向量的表示。在OpenFace中使用的向量是128x1的，也就是一个128维的向量。

