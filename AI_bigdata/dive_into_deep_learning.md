## 参考资料

[1.1] 《动手学深度学习》第二版 https://zh-v2.d2l.ai/

[1.5] http://zh.gluon.ai/chapter_prerequisite/install.html

[2.1] pandas API reference https://pandas.pydata.org/pandas-docs/stable/reference/index.html

[3.1] Jupyter https://jupyter.org/

[4.13.1] 13.1. 图像增广 https://zh-v2.d2l.ai/chapter_computer-vision/image-augmentation.html

[13.13] 13.13. 实战 Kaggle 比赛：图像分类 (CIFAR-10)
https://zh-v2.d2l.ai/chapter_computer-vision/kaggle-cifar10.html

[a1.4.1] 4.1 Fine tuning 模型微调 
https://pytorchbook.cn/chapter4/4.1-fine-tuning/

[a2.1] Transfer Learning
https://cs231n.github.io/transfer-learning/

## 安装

第一版：
```
mxnet==1.5.0      # mxnet-cu100mkl  # 1.5.1 CUDA-10.0 and MKLDNN
jupyter==1.0.0  
matplotlib==2.2.2 #  INSTALLED: 3.1.3
pandas==0.23.4    #  INSTALLED: 1.0.1                      - Powerful data structures for data analysis, time series, and statistics
d2lzh==1.0.0
```

第二版（2021.6.23）：

```
tensorflow-gpu==2.5.0  # tensorflow=2.5.0
tensorflow_probability==0.13.0 # 概率模型
mxnet==1.8.0           # mxnet cpu 版。gpu 版：mxnet-cu102 , cu102: cuda 10.2
torch==1.9.0           # 不区分 cpu/gpu 版
pandas==1.3.0          # 数据处理
d2l==0.16.6            # 封装了本书中常用的函数和类
notebook==6.4.0        # jupyter notebook
```
## 关于 jupyter notebook
jupyter notebook 是在浏览器中运行的交互式 python shell 。
通过 pip 安装 notebook 包后，再执行 jupyter notebook 即可启动它。
jupyter notebook 有一些特殊语句，是在普通 python 环境下无法使用的，如：

```
# 将matplotlib的图表直接嵌入到Notebook之中
%matplotlib inline 
```
set MXNET_GLUON_REPO=https://apache-mxnet.s3.cn-north-1.amazonaws.com.cn/ jupyter notebook

使用 d2l-zh.zip 中的 jupyter notebook 代码：
```
mkdir d2l-zh && cd d2l-zh
curl https://zh-v2.d2l.ai/d2l-zh-2.0.0.zip -o d2l-zh.zip
unzip d2l-zh.zip && rm d2l-zh.zip
cd pytorch # cd tensorflow

jupyter notebook
```

## 1. 前言

### 1.3.2.无监督学习

聚类（clustering）问题
主成分分析（principal component analysis）
因果关系（causality）
生成对抗性网络（generative adversarial networks）：为我们提供一种合成数据的方法，甚至像图像和音频这样复杂的结构化数据。

## 2. 预备知识

### 2.1. 数据操作

### 2.2 数据预处理

pandas 是个数据处理库[2.1]。

```
import pandas as pd
data = pd.read_csv(data_file) # type: <class 'pandas.core.frame.DataFrame'>，等效与二维数组，第一轴为行。
inputs, outputs = data.iloc[:, 0:2], data.iloc[:, 2]  # 取出（复制）0-1 和第 2 列

print(inputs)

   NumRooms Alley
0       NaN  Pave
1       2.0   NaN
2       4.0   NaN
3       NaN   NaN

inputs = inputs.fillna(inputs.mean())  # 对 'NA' 或 'NaN' 单元填充为列平均值。
print(inputs)

   NumRooms Alley
0       3.0  Pave
1       2.0   NaN
2       4.0   NaN
3       3.0   NaN

inputs = pd.get_dummies(inputs, dummy_na=True)  # get_dummies 是pandas实现one hot encode的方式
print(inputs)

   NumRooms  Alley_Pave  Alley_nan
0       3.0           1          0
1       2.0           0          1
2       4.0           0          1
3       3.0           0          1

print(inputs.values)  # 类型是 <class 'numpy.ndarray'> / float64

array([[3., 1., 0.],
       [2., 0., 1.],
       [4., 0., 1.],
       [3., 0., 1.]])
```

### 2.3 线性代数

矩阵操作（tensorflow）：
```
A = tf.reshape(tf.range(20), (5, 4))

<tf.Tensor: shape=(5, 4), dtype=int32, numpy=
array([[ 0,  1,  2,  3],
       [ 4,  5,  6,  7],
       [ 8,  9, 10, 11],
       [12, 13, 14, 15],
       [16, 17, 18, 19]], dtype=int32)>

tf.transpose(A)  # 转置

B = tf.constant([[1, 2, 3], [2, 0, 4], [3, 4, 5]])

B == tf.transpose(B)

<tf.Tensor: shape=(3, 3), dtype=bool, numpy=
array([[ True,  True,  True],
       [ True,  True,  True],
       [ True,  True,  True]])>

```

两个矩阵的按元素乘法称为 哈达玛积（Hadamard product）（数学符号 ⊙）。tensorflow 等框架中的算符均为 `*` 。

张量乘以或加上一个标量不会改变张量的形状，其中张量的每个元素都将与标量相加或相乘。

降维（求和）：

```
A = tf.reshape(tf.range(20), (5, 4))
tf.reduce_sum(x)  # 降低所有轴的维度求和，得到一个标量
tf.reduce_sum(A, axis=0)  # 沿着轴 0 求和，轴 0 的维度消失。
tf.reduce_sum(A, axis=[0, 1]) # 沿着轴0和1求和，轴0和1的维度消失。


tf.reduce_mean(A, axis=0) # 沿着轴 0 求平均，轴 0 的维度消失。
tf.reduce_sum(A, axis=0) / A.shape[0] # 同上，另一种方法。
```

保持维度求和：

```
A = tf.reshape(tf.range(20), (5, 4))
sum_A = tf.reduce_sum(A, axis=0, keepdims=True) # 求和后维度不消失，而是该维度的长度变成1。
A / sum_A
```

累计求和（不改变形状）：

```
tf.cumsum(A, axis=0)

<tf.Tensor: shape=(5, 4), dtype=float32, numpy=
array([[ 0.,  1.,  2.,  3.],
       [ 4.,  6.,  8., 10.],
       [12., 15., 18., 21.],
       [24., 28., 32., 36.],
       [40., 45., 50., 55.]], dtype=float32)>
```

点积：

```
x = tf.range(4, dtype=tf.float32)
y = tf.ones(4, dtype=tf.float32)
tf.tensordot(x, y, axes=1) # axes=1 不可省略。
tf.reduce_sum(x * y) # 同上
torch.dot(x, y)
```

矩阵-向量积，即 Y = A * X ，Y 和 X 为向量，而 A 为矩阵。A 的轴 1 长度应该与 X 的轴 0 长度相同。

```
A = tf.reshape(tf.range(20, dtype=tf.float32), (5, 4))
x = tf.range(4, dtype=tf.float32)
tf.linalg.matvec(A, x)
torch.mv(A, x)
```

矩阵-矩阵乘法

```
A = tf.reshape(tf.range(20, dtype=tf.float32), (5, 4))
B = tf.ones((4, 3), tf.float32)
tf.matmul(A, B)  # 得到 (5,3) 形状的矩阵
torch.mm(A, B)
```

L2范数（模）

```
u = tf.constant([3.0, -4.0])
tf.norm(u)
```
矩阵的元素平方和的平方根是弗罗贝尼乌斯范数（Frobenius norm），tf、torch中的算符与L2范数相同。

L1 范数（绝对值之和）

```
tf.reduce_sum(tf.abs(u))
```

线性代数还有很多，其中很多数学对于机器学习非常有用。例如，矩阵可以分解为因子，这些分解可以显示真实世界数据集中的低维结构。
机器学习的整个子领域都侧重于使用矩阵分解及其向高阶张量的泛化来发现数据集中的结构并解决预测问题。

### 2.4. 微分

积分（integral calculus），微分（differential calculus）

可以将拟合模型的任务分解为两个关键问题：（1）优化（optimization）：用模型拟合观测数据的过程；（2）泛化（generalization）：数学原理和实践者的智慧，能够指导我们生成出有效性超出用于训练的数据集本身的模型。

在深度学习中，我们通常选择对于模型参数可微的损失函数。

plot 函数，可视化曲线。

#### 2.4.4. 链式法则

这是因为在深度学习中，多元函数通常是 复合（composite）的，所以我们可能没法应用上述任何规则来微分这些函数。

### 2.5. 自动求导

深度学习框架通过自动计算导数，即 自动求导 （automatic differentiation），来加快这项工作。实际中，根据我们设计的模型，系统会构建一个 计算图 （computational graph），来跟踪计算是哪些数据通过哪些操作组合起来产生输出。自动求导使系统能够随后反向传播梯度。 这里，反向传播（backpropagate）只是意味着跟踪整个计算图，填充关于每个参数的偏导数。

```
import tensorflow as tf

x = tf.range(4, dtype=tf.float32)
x
x = tf.Variable(x)
# 把所有计算记录在磁带上
with tf.GradientTape() as t:
    y = 2 * tf.tensordot(x, x, axes=1)
y # <tf.Tensor: shape=(), dtype=float32, numpy=28.0>

t # <tensorflow.python.eager.backprop.GradientTape
x_grad = t.gradient(y, x)
x_grad # tensorflow.python.framework.ops.EagerTensor <tf.Tensor: shape=(4,), dtype=float32, numpy=array([ 0.,  4.,  8., 12.], dtype=float32)>
       # x_grad 形状与 x 相同

with tf.GradientTape() as t:
    y = tf.reduce_sum(x)
t.gradient(y, x)  # 被新计算的梯度覆盖

```

```
import torch

x = torch.arange(4.0)
x
x.requires_grad_(True)  # 等价于 `x = torch.arange(4.0, requires_grad=True)`
x.grad  # 默认值是None

y = 2 * torch.dot(x, x)
y # tensor(28., grad_fn=<MulBackward0>)

y.backward()
x.grad # tensor([ 0.,  4.,  8., 12.])

# 在默认情况下，PyTorch会累积梯度，我们需要清除之前的值
x.grad.zero_()
y = x.sum()
y.backward()
x.grad # tensor([1., 1., 1., 1.])
```

#### 2.5.2. 非标量变量的反向传播
#### 2.5.3. 分离计算

#### 2.5.4. Python控制流的梯度计算

使用自动求导的一个好处是，即使构建函数的计算图需要通过 Python控制流（例如，条件、循环或任意函数调用），我们仍然可以计算得到的变量的梯度。

注：有控制流的函数通常是分段解析的，所以在每个分段上仍然可以解析地求导。

### 2.6. 概率

```
import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp
from d2l import tensorflow as d2l
```

```
import torch
from torch.distributions import multinomial
from d2l import torch as d2l
```

### 2.7 查阅文档

```
print(dir(tf.random))
help(tf.ones)
```
在Jupyter记事本中，我们可以使用 ? 在另一个窗口中显示文档。例如，list?将创建与help(list) 几乎相同的内容，并在新的浏览器窗口中显示它。此外，如果我们使用两个问号，如 list??，将显示实现该函数的 Python 代码。

## 3. 线性神经网络

### 3.1. 线性回归

我们要试图预测的目标（在这个例子中是房屋价格）称为 标签（label）或 目标（target）。预测所依据的自变量（面积和房龄）称为 特征（features）或 协变量（covariates）。

损失函数：能够量化目标的实际值与预测值之间的差距。

注：损失函数应具有可加性，即一组样本的损失函数是每个样本的损失函数的和。这也意味着损失函数关于参数的梯度有可加性。这样，小批量梯度下降法可以与全量梯度下降法等效。

梯度下降最简单的用法是计算损失函数（数据集中所有样本的损失均值）关于模型参数的导数（在这里也可以称为梯度）。但实际中的执行可能会非常慢：因为在每一次更新参数之前，我们必须遍历整个数据集。因此，我们通常会在每次需要计算更新的时候随机抽取一小批样本，这种变体叫做小批量随机梯度下降（minibatch stochastic gradient descent）。

总结一下，算法的步骤如下：（1）初始化模型参数的值，如随机初始化；（2）从数据集中随机抽取小批量样本且在负梯度的方向上更新参数，并不断迭代这一步骤。

|B| 表示每个小批量中的样本数，这也称为批量大小（batch size）。η 表示 学习率（learning rate）。批量大小和学习率的值通常是手动预先指定，而不是通过模型训练得到的。
这些可以调整但不在训练过程中更新的参数称为 超参数（hyperparameter）。 调参（hyperparameter tuning） 是选择超参数的过程。
超参数通常是我们根据训练迭代结果来调整的，而训练迭代结果是在独立的验证数据集（validation dataset）上评估得到的。

注：通常的小批量随机梯度下降法在计算完一个小批量的梯度后，就更新一次参数，这就与全量梯度下降法不完全等效了，使得学习效果与小批量的大小相关（通常越大越好）。
增大小批量的大小则受到内存容量的限制，不能无限增大。这时，可以利用损失函数（及其关于参数的导数）的可加性，把多个小批量计算出的梯度相加，再更新一次参数，等于是增大了小批量的大小。

给定特征估计目标的过程通常称为预测（prediction）或推断（inference）。在统计学中，推断更多地表示基于数据集估计参数。当深度学习从业者与统计学家交谈时，术语的误用经常导致一些误解。

矢量化加速。

正态分布与平方损失。因此，在高斯噪声的假设下，最小化均方误差等价于对线性模型的最大似然估计。

神经网络图。
我们可以将线性回归模型视为仅由单个人工神经元组成的神经网络，或称为单层神经网络。

### 3.2. 线性回归的从零开始实现

```
import random
import tensorflow as tf
from d2l import tensorflow as d2l
```

我们生成一个包含1000个样本的数据集，每个样本包含从标准正态分布中采样的2个特征。我们的合成数据集是一个矩阵 X∈R1000×2。

我们使用线性模型参数w=[2,−3.4]⊤、b=4.2和噪声项ϵ
y=Xw+b+ϵ.

生成数据集及其标签：
(3.2.1)
```
# 形状：return: X:(num_examples, len(w)), y: (num_examples, 1)

def synthetic_data(w, b, num_examples):  #@save
    """生成 y = Xw + b + 噪声。"""
    X = tf.zeros((num_examples, w.shape[0]))
    X += tf.random.normal(shape=X.shape)
    y = tf.matmul(X, tf.reshape(w, (-1, 1))) + b # 形状：y: (num_examples, 1)
    y += tf.random.normal(shape=y.shape, stddev=0.01)
    y = tf.reshape(y, (-1, 1))
    return X, y

true_w = tf.constant([2, -3.4])
true_b = 4.2
features, labels = synthetic_data(true_w, true_b, 1000)

print('features:', features[0], '\nlabel:', labels[0])
```

小批量读取数据集：

```
def data_iter(batch_size, features, labels):
    num_examples = len(features)
    indices = list(range(num_examples))
    # 这些样本是随机读取的，没有特定的顺序
    random.shuffle(indices)
    for i in range(0, num_examples, batch_size):
        j = tf.constant(indices[i:min(i + batch_size, num_examples)])
        yield tf.gather(features, j), tf.gather(labels, j)
        

# 测试读取
batch_size = 10
# 形状：X: (batch_size, len(w)==2), y: (batch_size, 1)
for X, y in data_iter(batch_size, features, labels):
    print(X, '\n', y)
    break
```

训练：

```
# 初始化模型参数
w = tf.Variable(tf.random.normal(shape=(2, 1), mean=0, stddev=0.01),
                trainable=True)
b = tf.Variable(tf.zeros(1), trainable=True)

# 定义模型（网络）。 形状：w: (2, 1), b: (1,1), X: (batch_size, 2), return: (batch_size, 1)
def linreg(X, w, b):  #@save
    """线性回归模型。"""
    return tf.matmul(X, w) + b

#定义损失函数。注意这是一个批量样本的损失总和。形状：y: (batch_size, 1), y_hat: (batch_size, 1)
def squared_loss(y_hat, y):  #@save
    """均方损失。"""
    return (y_hat - tf.reshape(y, y_hat.shape))**2 / 2

#定义优化算法
def sgd(params, grads, lr, batch_size):  #@save
    """小批量随机梯度下降。"""
    for param, grad in zip(params, grads):
        param.assign_sub(lr * grad / batch_size) # 用批量大小（batch_size）来归一化步长

#训练

lr = 0.03
num_epochs = 3
net = linreg
loss = squared_loss

for epoch in range(num_epochs):
    for X, y in data_iter(batch_size, features, labels):
        with tf.GradientTape() as g:
            l = loss(net(X, w, b), y)  # `X`和`y`的小批量损失。形状：l: 
        # 计算l关于[`w`, `b`]的梯度
        dw, db = g.gradient(l, [w, b])
        # 使用参数的梯度更新参数
        sgd([w, b], [dw, db], lr, batch_size)
    train_l = loss(net(features, w, b), labels)
    print(f'epoch {epoch + 1}, loss {float(tf.reduce_mean(train_l)):f}')
```

因为我们计算的损失是一个批量样本的总和，所以我们用批量大小（batch_size）来归一化步长。


### 3.3. 线性回归的简洁实现



### 3.5. 图像分类数据集（Fashion-MNIST）

数据集下载到 ~/.mxnet/datasets/fashion-mnist/

```
-rw-r--r-- 1 duanyao duanyao  4398264 5月  10 00:38 t10k-images-idx3-ubyte.gz
-rw-r--r-- 1 duanyao duanyao     5097 5月  10 00:38 t10k-labels-idx1-ubyte.gz
-rw-r--r-- 1 duanyao duanyao 26434613 5月  10 00:38 train-images-idx3-ubyte.gz
-rw-r--r-- 1 duanyao duanyao    29497 5月  10 00:38 train-labels-idx1-ubyte.gz
```

Fashion-MNIST中一共包括了10个类别，分别为t-shirt（T恤）、trouser（裤子）、pullover（套衫）、dress（连衣裙）、coat（外套）、sandal（凉鞋）、shirt（衬衫）、sneaker（运动鞋）、bag（包）和ankle boot（短靴）。

训练集中和测试集中的每个类别的图像数分别为6,000和1,000。因为有10个类别，所以训练集和测试集的样本数分别为60000和10000。

模型的输入向量的长度是28×28=784：该向量的每个元素对应图像中每个像素。由于图像有10个类别，单层神经网络输出层的输出个数为10，因此softmax回归的权重和偏差参数分别为784×10和1×10的矩阵。

batch_size = 256，训练集235，测试集40，类别10，训练5轮，CPU模式，用时约23秒，4.6s/轮。

## 5. 卷积神经网络

## 13. 计算机视觉

### 13.1. 图像增广

在“池化层”一节里我们解释了池化层能降低卷积层对目标位置的敏感度。
除此之外，我们还可以通过对图像随机裁剪来让物体以不同的比例出现在图像的不同位置，这同样能够降低模型对目标位置的敏感性。

另一类增广方法是变化颜色。我们可以从4个方面改变图像的颜色：亮度、对比度、饱和度和色调。
注：非均匀亮度修改？侧光修改？

数据增强的方式是这样的：比如在一个epoch之内，我是把所有的图片都过一遍，对于每张图片我都是进行一个transform的操作，比如transform内部有0.5的概率进行左右flip，那么这张图片左右flip的概率就是0.5，可能这一个epoch不flip，下一个epoch就会flip

例子：使用 CIFAR-10 。这是因为Fashion-MNIST数据集中对象的位置和大小已被规范化，而CIFAR-10数据集中对象的颜色和大小差异更明显。

### 13.2. 微调

补充材料：[a1.4.1, a2.1]

本节我们介绍迁移学习中的一种常用技术：微调（fine tuning）。


在源数据集（如ImageNet数据集）上预训练一个神经网络模型，即源模型。

创建一个新的神经网络模型，即目标模型。它复制了源模型上除了输出层外的所有模型设计及其参数。我们假设这些模型参数包含了源数据集上学习到的知识，且这些知识同样适用于目标数据集。我们还假设源模型的输出层与源数据集的标签紧密相关，因此在目标模型中不予采用。

为目标模型添加一个输出大小为目标数据集类别个数的输出层，并随机初始化该层的模型参数。

在目标数据集（如椅子数据集）上训练目标模型。我们将从头训练输出层，而其余层的参数都是基于源模型的参数微调得到的。

当目标数据集远小于源数据集时，微调有助于提升模型的泛化能力。

13.2.1. 热狗识别

接下来我们来实践一个具体的例子：热狗识别。我们将基于一个小数据集对在ImageNet数据集上训练好的ResNet模型进行微调。该小数据集含有数千张包含热狗和不包含热狗的图像。我们将使用微调得到的模型来识别一张图像中是否包含热狗。

由于features中的模型参数是在ImageNet数据集上预训练得到的，已经足够好，因此一般只需使用较小的学习率来微调这些参数。而成员变量output中的模型参数采用了随机初始化，一般需要更大的学习率从头训练。

可以看到，微调的模型因为参数初始值更好，往往在相同迭代周期下取得更高的精度。

数据集下载地址：

http://d2l-data.s3-accelerate.amazonaws.com/hotdog.zip

获取和审阅数据集图片：
```
#%matplotlib inline
import os
import sys
import torch
import torchvision
from torch import nn
from d2l import torch as d2l

import matplotlib.pyplot as plt

plt.ion()

#@save
d2l.DATA_HUB['hotdog'] = (d2l.DATA_URL + 'hotdog.zip',
                         'fba480ffa8aa7e0febbb511d181409f899b9baa5')

# 下载到了 ../data/hotdog.zip, 解压到了 ../data/hotdog/
#data_dir = d2l.download_extract('hotdog') 
data_dir = '../data/hotdog' # 如果下载失败，可手动下载 http://d2l-data.s3-accelerate.amazonaws.com/hotdog.zip 到 ../../data/ ，并解压到 ../data/hotdog/

train_imgs = torchvision.datasets.ImageFolder(os.path.join(data_dir, 'train'))
test_imgs = torchvision.datasets.ImageFolder(os.path.join(data_dir, 'test'))

train_imgs.classes # ['hotdog', 'not-hotdog']
train_imgs.class_to_idx # {'hotdog': 0, 'not-hotdog': 1}
train_imgs[1500] # (<PIL.Image.Image image mode=RGB size=414x628 at 0x7FD6528DC9D0>, 1)
train_imgs[0] # (<PIL.Image.Image image mode=RGB size=122x144 at 0x7FD6528DCCD0>, 0)
train_imgs.imgs[0] # ('../data/hotdog/train/hotdog/0.png', 0)

hotdogs = [train_imgs[i][0] for i in range(8)]
not_hotdogs = [train_imgs[-i - 1][0] for i in range(8)]
# 使用 Matplotlib 显示图像
d2l.show_images(hotdogs + not_hotdogs, 2, 8, scale=1.4);
# 阻止程序退出。Matplotlib 窗口不会阻止程序退出。
line = sys.stdin.readline()
```
训练：
```
#%matplotlib inline
import os
import torch
import torchvision
from torch import nn
from d2l import torch as d2l

#@save
#d2l.DATA_HUB['hotdog'] = (d2l.DATA_URL + 'hotdog.zip',
                         #'fba480ffa8aa7e0febbb511d181409f899b9baa5')

#data_dir = d2l.download_extract('hotdog') # 下载到了 ../data/hotdog.zip, 解压到了 ../data/hotdog/
data_dir = '../data/hotdog' # 如果下载失败，可手动下载 http://d2l-data.s3-accelerate.amazonaws.com/hotdog.zip 到 ../data/ ，并解压到 ../data/hotdog/


# 使用三个RGB通道的均值（参数1）和标准偏差（参数2），以标准化每个通道
normalize = torchvision.transforms.Normalize([0.485, 0.456, 0.406],
                                             [0.229, 0.224, 0.225])

train_augs = torchvision.transforms.Compose([
    torchvision.transforms.RandomResizedCrop(224),
    torchvision.transforms.RandomHorizontalFlip(),
    torchvision.transforms.ToTensor(), normalize])

test_augs = torchvision.transforms.Compose([
    torchvision.transforms.Resize(256),
    torchvision.transforms.CenterCrop(224),
    torchvision.transforms.ToTensor(), normalize])

# 从 site-packages/d2l/torch.py 复制，修改了日志输出
def train_ch13(net, train_iter, test_iter, loss, trainer, num_epochs,
               devices=d2l.try_all_gpus()):
    """Train a model with mutiple GPUs (defined in Chapter 13)."""
    timer, num_batches = d2l.Timer(), len(train_iter)
    animator = d2l.Animator(xlabel='epoch', xlim=[1, num_epochs], ylim=[0, 1],
                            legend=['train loss', 'train acc', 'test acc'])
    net = nn.DataParallel(net, device_ids=devices).to(devices[0])
    for epoch in range(num_epochs):
        # Sum of training loss, sum of training accuracy, no. of examples,
        # no. of predictions
        metric = d2l.Accumulator(4)
        for i, (features, labels) in enumerate(train_iter):
            timer.start()
            l, acc = d2l.train_batch_ch13(net, features, labels, loss, trainer,
                                      devices)
            metric.add(l, acc, labels.shape[0], labels.numel())
            timer.stop()
            if (i + 1) % (num_batches // 5) == 0 or i == num_batches - 1:
                animator.add(
                    epoch + (i + 1) / num_batches,
                    (metric[0] / metric[2], metric[1] / metric[3], None))
                print(f'epoch {epoch}, iter {i}, '
                    f'loss {metric[0] / metric[2]:.3f}, train acc '
                    f'{metric[1] / metric[3]:.3f}')
        test_acc = d2l.evaluate_accuracy_gpu(net, test_iter)
        animator.add(epoch + 1, (None, None, test_acc))
        print(f'epoch {epoch}, '
            f'loss {metric[0] / metric[2]:.3f}, train acc '
            f'{metric[1] / metric[3]:.3f}, test acc {test_acc:.3f}')
        print(f'{metric[2] * num_epochs / timer.sum():.1f} examples/sec on '
            f'{str(devices)}')

# 如果 `param_group=True`，输出层中的模型参数将使用十倍的学习率
def train_fine_tuning(net, learning_rate, batch_size=128, num_epochs=5,
                      param_group=True):
    train_iter = torch.utils.data.DataLoader(
        torchvision.datasets.ImageFolder(os.path.join(data_dir, 'train'),
                                         transform=train_augs),
        batch_size=batch_size, shuffle=True)
    test_iter = torch.utils.data.DataLoader(
        torchvision.datasets.ImageFolder(os.path.join(data_dir, 'test'),
                                         transform=test_augs),
        batch_size=batch_size)
    devices = d2l.try_all_gpus()
    loss = nn.CrossEntropyLoss(reduction="none") # 参数的解释见后面
    if param_group:
        params_1x = [
            param for name, param in net.named_parameters() if name not in ["fc.weight", "fc.bias"]
        ]
        
        trainer = torch.optim.SGD(
            [
                { 'params': params_1x },
                { 'params': net.fc.parameters(), 'lr': learning_rate * 10 }
            ],
            lr=learning_rate,
            weight_decay=0.001
        )
    else:
        trainer = torch.optim.SGD(net.parameters(), lr=learning_rate,
                                  weight_decay=0.001)

    train_ch13(net, train_iter, test_iter, loss, trainer, num_epochs,  devices)

def main_fine_tune():
    # 导致下载 "https://download.pytorch.org/models/resnet18-f37072fd.pth" to /home/duanyao/.cache/torch/hub/checkpoints/resnet18-f37072fd.pth
    finetune_net = torchvision.models.resnet18(pretrained=True)
    # 将预训练模型的输出层（全连接层）替换为一个新建的 Linear 层，输出分类数改为2
    finetune_net.fc = nn.Linear(finetune_net.fc.in_features, 2) # 2：输出的分类数为 2
    # 随机初始化
    nn.init.xavier_uniform_(finetune_net.fc.weight);

    print(finetune_net.fc) # Linear(in_features=512, out_features=2, bias=True)
    train_fine_tuning(finetune_net, 5e-5, batch_size = 32)

def main_scratch():
    scratch_net = torchvision.models.resnet18()
    scratch_net.fc = nn.Linear(scratch_net.fc.in_features, 2)
    train_fine_tuning(scratch_net, 5e-4, param_group=False)

main_fine_tune()
```

注意，batch_size = 32 需要 1.7GiB显存。

解释：`CrossEntropyLoss(reduction="none")` [13.13] 评论区：

我看了torch源码，reduction总共有三个选项，“mean”,“sum”,“none”。
当我们输入一个batch的样本后，每个样本都有一个损失值，就得到一个batch_size大小的损失值向量。
这三个选项就是对这个损失值向量做的操作。
"mean"就是对这个向量求平均，得到整个batch的平均损失
“sum”就是求和损失向量
“none”:什么也不做，直接返回整个损失向量。
在代码中，执行backward()之前是l.sum().backward() ，其实相当于reduction=“sum”，然后执行l.backward()。
（btw,reduction 默认值为mean，选取不同的reduction需要注意learning_rate的调整，使用sum的话loss变成mean的batch_size倍，梯度也变成batch_size倍，注意lr过大导致模型不收敛的问题）

有待研究的点：
* xavier_uniform_ 的具体含义？
* torch.optim.SGD 的配置和查看。
* CrossEntropyLoss 的配置和查看，weight_decay。
* 观察和测试 DataLoader / ImageFolder 的行为。 
* 让 animator 在脚本中正常工作。
* 采用 224 以外的分辨率？
* 数据增广时，采用



13.3. 目标检测和边界框

13.9. 语义分割和数据集

本节将探讨语义分割（semantic segmentation）问题，它关注如何将图像分割成属于不同语义类别的区域。值得一提的是，这些语义区域的标注和预测都是像素级的。


13.9.1. 图像分割和实例分割

计算机视觉领域还有2个与语义分割相似的重要问题，即图像分割（image segmentation）和实例分割（instance segmentation）。我们在这里将它们与语义分割简单区分一下。

图像分割将图像分割成若干组成区域。这类问题的方法通常利用图像中像素之间的相关性。它在训练时不需要有关图像像素的标签信息，在预测时也无法保证分割出的区域具有我们希望得到的语义。

实例分割又叫同时检测并分割（simultaneous detection and segmentation）。它研究如何识别图像中各个目标实例的像素级区域。与语义分割有所不同，实例分割不仅需要区分语义，还要区分不同的目标实例。如果图像中有两只狗，实例分割需要区分像素属于这两只狗中的哪一只。

9.9.2. Pascal VOC2012语义分割数据集

压缩包大小是2 GB左右

