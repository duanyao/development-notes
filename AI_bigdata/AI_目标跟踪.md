## 参考资料

[1.1] multi-object-tracking 
  https://github.com/topics/multi-object-tracking

[2.1] How to evaluate tracking with the HOTA metrics
  https://jonathonluiten.medium.com/how-to-evaluate-tracking-with-the-hota-metrics-754036d183e1

[2.2] HOTA: A Higher Order Metric for Evaluating Multi-Object Tracking
  https://arxiv.org/abs/2009.07736

[2.3] TrackEval
  https://github.com/JonathonLuiten/TrackEval

[3.1] MOT Challenge: The Multiple Object Tracking Benchmark!
  https://motchallenge.net/

[3.2] 行人重识别(ReID) ——数据集描述 Market-1501
  https://blog.csdn.net/ctwy291314/article/details/83544088
  https://zhuanlan.zhihu.com/p/340616182
  https://pan.baidu.com/s/1ntIi2Op?_at_=1644236753274
  
[3.3] https://votchallenge.net/

[3.4] VOT2017
  https://data.votchallenge.net/vot2017/presentations/vot2017_presentation.pdf

[3.5] VOT2017 结果抢先看
  https://zhuanlan.zhihu.com/p/30550465

[4.1] SORT
  https://github.com/abewley/sort
  https://gitee.com/benjiaxu/sort

[4.2] 目标跟踪初探（DeepSORT）
  https://zhuanlan.zhihu.com/p/90835266

[4.3] 多目标跟踪：SORT和Deep SORT
  https://zhuanlan.zhihu.com/p/59148865

[4.4] nwojke /deep_sort 
  https://github.com/nwojke/deep_sort

[4.5] strongerfly /Yolov5_DeepSort_Pytorch 
  https://github.com/strongerfly/Yolov5_DeepSort_Pytorch
  https://github.com/mikel-brostrom/Yolov5_DeepSort_Pytorch

[5.1] FairMOT
  https://github.com/ifzhang/FairMOT
  https://arxiv.org/abs/2004.01888

[6.1] Siamese RPN 超实时性单目标跟踪网络——Siamese RPN(CVPR2018 spotlight论文)
https://blog.csdn.net/leviopku/article/details/81068487  

[6.2] Object tracking:Siamese-RPN
https://zhuanlan.zhihu.com/p/68074958

[7.1]【运动模型】SiamMOT: Siamese Multi-Object Tracking
https://zhuanlan.zhihu.com/p/422603651

[8.1] ByteTrack: Multi-Object Tracking by Associating Every Detection Box
https://zhuanlan.zhihu.com/p/421264325

[9.1] Joint Detection and Embedding (JDE)论文阅读
https://zhuanlan.zhihu.com/p/413525281

[10.1] CMU Object Detection & Tracking for Surveillance Video Activity Detection
https://github.com/JunweiLiang/Object_Detection_Tracking

[11.1] oc sort
https://github.com/PaddlePaddle/PaddleDetection/tree/release/2.6/configs/mot/ocsort

## 数据集和竞赛项目

### MOT Challenge
[3.1]

### VOT
主要涉及单目标跟踪。并不意味着画面中只有一个目标，而是只要求跟踪一个指定的目标。
[3.3-3.5]

## 评测方法

### HOTA
多目标跟踪指标。
实现工具见 TrackEval（ TrackEval.md ）。

## Siamese RPN
单目标跟踪算法。[6.1] 

## SiamMOT: Siamese Multi-Object Tracking
多目标跟踪算法。[7.1] 

## ByteTrack: Multi-Object Tracking by Associating Every Detection Box
多目标跟踪算法,2021。[8.1] 

## Joint Detection and Embedding (JDE)
多目标跟踪算法。[9.1] 

之前的模型诸如DeepSORT，把embedding和detection分开，比如用YOLO来做目标检测，用另一个网络Resnet来提取目标的特征向量，这种方法被称为Seperate Detection and Embedding（SDE）。SDE 框架的目标检测与嵌入学习相互独立，时间开销为两者之和，且随着跟踪目标的变多， 计算时间也相应增加。因此该论文提出了JDE，将目标检测和嵌入学习融合在同一个网络中，很好的避免了重复计 算，JDE 框架精度相同且有着速度上的显著提升，能够实现实时的多目标跟踪。[9.1]

## CMU Object Detection & Tracking for Surveillance Video Activity Detection
多目标跟踪程序 [10.1]。

## SORT
多目标跟踪算法。[4.1]

输入格式为 MOT 格式：

```
# <frame_id>, <track_id>, <bb_left>, <bb_top>, <bb_width>, <bb_height>, <conf>, <x>, <y>, <z>
1,1,199,813,140,268,1,1,0.83643
2,1,201,812,140,268,1,1,0.84015
3,1,203,812,140,268,1,1,0.84015
```
其中，<track_id>, <conf>, <x>, <y>, <z> 不会被利用。
frame_id 从 1 开始，升序连续排列。

输出同样是 MOT 格式，其中 <conf> 恒等于 1，<track_id> 从 1 开始升序排列。如果中间帧跟丢目标，则相应的行会被丢弃。


一个轨迹的 frame_id 可以有空缺，默认情况下会被 SORT 跟踪器视为这些帧中目标暂时消失。SORT 会在目标消失 max_age（参数）帧后终止轨迹。max_age 默认是1，意味着立即终止。如果目标检测不够稳定，可以增加 max_age 。

除了第一帧，SORT 跟踪器默认不会根据检测框立即构建轨迹，而是等待 min_hits 帧（默认是3），要立即构建轨迹，可改为 1。SORT 这样做的意义笔者尚不理解。

SORT 将新的一帧的每个目标框与当前的每个轨迹的预测框计算IOU。如果对于一个轨迹的预测框，没有任何新目标框与其 IOU 大于参数 iou_threshold（默认0.3），则认为不能匹配，这个轨迹在此暂时中断。
如果有多个新目标框与任意轨迹的预测框的 IOU 大于 iou_threshold ，则利用线性分配算法（linear_assignment）将新目标框分配到轨迹上。

可见，要让 SORT 跟踪不中断，一般应确保相邻帧中的目标框不脱离接触，即IOU大于0。虽然 SORT 的预测框会考虑到目标的速度，但毕竟很可能预测不准。因此 SORT 不能直接跟踪小尺寸的高速目标

## OC_SORT (Observation-Centric SORT: Rethinking SORT for Robust Multi-Object Tracking)
[11.1]
