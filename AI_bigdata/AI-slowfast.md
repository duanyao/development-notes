## 参考资料
[1.1] SlowFast Networks for Video Recognition
  https://arxiv.org/abs/1812.03982

[1.2] SlowFast
  https://github.com/facebookresearch/SlowFast

[1.3] ICCV 2019 Tutorial on Visual Recognition for Images, Video, and 3D
  https://alexander-kirillov.github.io/tutorials/visual-recognition-iccv19/

[1.4] Recognition in Video (slides) - Christoph Feichtenhofer
  https://www.dropbox.com/s/syy8842llbxjwin/ICCV19_SlowFast_tutorial.pdf?dl=0

[2.2] Error while installing av 7.0.0 using pip
  https://github.com/mikeboers/PyAV/issues/607

[2.3]  detectron2/INSTALL.md
  https://github.com/facebookresearch/detectron2/blob/master/INSTALL.md
  
[2.4]  SlowFast/slowfast/datasets/DATASET.md 
  https://github.com/facebookresearch/SlowFast/blob/master/slowfast/datasets/DATASET.md
  
[3.1] Perform Prediction on Videos/Camera
  https://github.com/anhminh3105/SlowFast/blob/prediction_demo/GETTING_STARTED.md
  
[3.2] Perform Prediction on Videos/Camera
  https://github.com/anhminh3105/SlowFast/tree/master
  
[3.4] Perform Prediction on Videos/Camera
  https://github.com/facebookresearch/SlowFast/issues/34#issuecomment-597117717
  
[3.5] Perform Prediction on Videos/Camera
  https://github.com/duanyao/SlowFast

[4.1] Detectron2 Model Zoo and Baselines
  https://github.com/facebookresearch/detectron2/blob/master/MODEL_ZOO.md

[5.1] Questions about gpu memory of slowfast network and I3D
  https://github.com/facebookresearch/SlowFast/issues/78

## 资料和文档
论文见 [1.1] 。源码 [1.2]。介绍 slowfast 的ppt [1.3, 1.4] 。

## 安装

安装依赖的 python 包（venv 中）：

```
source ~/opt/venv/v37/bin/activate
pip install cython torch>=1.3 torchvision numpy opencv-python pycocotools simplejson
pip install fvcore
pip install pandas
```

安装 pyav（ffmpeg 的 python 接口）：

```
#主机系统中安装 ffmpeg 的开发包（pip av 包的依赖）：
sudo apt install libavdevice-dev libavfilter-dev libavformat-dev libavcodec-dev libavutil-dev libswscale-dev libswresample-dev
apt policy libavcodec-dev

#venv 中安装。这一步会编译C代码。
pip install 'av<7'
#pip install av
```

注：pyav 的当前版本是 7.x ~ 8.x，它依赖 ffmpeg 4.2.x。如果你的 debian 系统的 ffmpeg 版本是 3.x，则应该安装 pyav 6.x 。

安装 detectron2：[2.3]

```
git clone https://github.com/facebookresearch/detectron2.git detectron2.git
cd detectron2.git
pip install -e .  #在 venv 中
```

也可以安装预编译版本（cu100 对应 cuda 10.0，如果版本不同请相应修改，更高版本有 cu110、cu113等）：
```
pip install detectron2 -f https://dl.fbaipublicfiles.com/detectron2/wheels/cu100/index.html
```

安装本体：

```
python setup.py build develop
```

### 演示程序

官方代码里目前缺少演示程序。可以下载 [3.1-3.4] 的 demo_app 分支或者 [3.5] 的 demo_duanyao 分支，它包括针对摄像头或者单个视频文件的行为检测（AVA）和识别（kinects）程序。

## 下载模型

从 [4.1] 下载 slowfast 的两个模型：`ava/SLOWFAST_32x2_R101_50_50.pkl` 和 `kinetics400/SLOWFAST_8x8_R50.pkl` 。前者用于行为检测，后者用于行为识别。
下载后，链接或复制到工程目录下：

```
ln -s /home/duanyao/data-development/ai-model/slowfast-model/ava/SLOWFAST_32x2_R101_50_50.pkl .
ln -s /home/duanyao/data-development/ai-model/slowfast-model/kinetics400/SLOWFAST_8x8_R50.pkl .
```

要做行为检测，还需要一个 detectron2 的模型 `https://dl.fbaipublicfiles.com/detectron2/COCO-Detection/faster_rcnn_R_50_FPN_3x/137849458/model_final_280758.pkl` 。
下载后复制或链接为 `~/.torch/iopath_cache/detectron2/COCO-Detection/faster_rcnn_R_50_FPN_3x/137849458/model_final_280758.pkl` （2021.12）。过去（2020.4）是 `~/.torch/fvcore_cache/detectron2/COCO-Detection/faster_rcnn_R_50_FPN_3x/137849458/model_final_280758.pkl` 。

## 减少显存需求

在 `model.eval()` （文件 `tools/demo_net.py`, `tools/test_net.py`, `tools/train_net.py`）之前增加[5.1]：
```
for p in model.parameters():
    p.requires_grad = False
```

另外可以修改 train.bath_size [5.1]，但也许只对训练有用。

## 运行演示程序

需要使用 [3.1-3.4] 的 demo_app 分支或者 [3.5] 的 demo_duanyao 分支。

### 行为检测（SLOWFAST）

确保 `SLOWFAST_32x2_R101_50_50.pkl` 模型在项目目录下。

修改 `demo/AVA/SLOWFAST_32x2_R101_50_50.yaml` 文件。`DATA_SOURCE_PATH: "xxxx"` 是视频文件路径，`DATA_SOURCE: 0` 则表示从摄像头采集；两者只能保留一个。

修改 `tools/demo_net.py` 中 

```
video_writer = cv2.VideoWriter('output.mp4',
        fourcc, 32.0,
        (1024, 576)
        )
```
的 (1024, 576) 为输入视频的宽和高。

如果你的 python 环境同时安装了 opencv-python 4 和 pyqt5，则要设置环境变量。请根据实际情况调整版本号。如果是 venv ，则 `VIRTUAL_ENV` 会被自动设置。

```
export QT_PLUGIN_PATH=$VIRTUAL_ENV/lib/python3.9/site-packages/cv2/qt/plugins
```

运行行为检测：
```
python ./tools/run_net.py --cfg demo/AVA/SLOWFAST_32x2_R101_50_50.yaml
```
需要显存 1.34GB

时间构成：
  - 视频解码：~1ms，共64帧
  - 人体检测：1.37s （与分辨率、人数关系不大）
  - 预处理（视频帧转化为tensor，仅使用 CPU）：7.5s（240p, 64帧，折合0.117s/帧）；6.8s（768p, 32帧，折合0.212s/帧）
  - 预测（使用 GPU）：2s（240p, 64帧）；2.6s（768p, 32帧）

总体性能估计：
  - 每一帧用时：0.117+2/64=0.148s; 0.212+2.6/32=0.293s
  - fps: 6.7;3.4
  
30s 32fps 的视频，每 2s 预测一次，用时 238s。考虑到 GPU 利用率 50% 左右，折合 0.267 s/s 。
估计 1080Ti 可以支持 5 个摄像头。

### 行为分类（SLOWFAST）

确保 `SLOWFAST_8x8_R50.pkl` 模型在项目目录下。

修改 `demo/Kinetics/SLOWFAST_8x8_R50.yaml` 文件。`DATA_SOURCE_PATH: "xxxx"` 是视频文件路径，`DATA_SOURCE: 0` 则表示从摄像头采集；两者只能保留一个。

```
python ./tools/run_net.py --cfg demo/Kinetics/SLOWFAST_8x8_R50.yaml
```
需要显存 0.9GB

时间构成：
  - 视频解码：~1ms，共64帧
  - 预处理（视频帧转化为tensor）：7.8s
  - 预测：1.1s

### 行为分类（I3D）

确保 `I3D_NLN_8x8_R50.pkl` 模型在项目目录下。

修改 `demo/Kinetics/I3D_NLN_8x8_R50.yaml` 文件。`DATA_SOURCE_PATH: "xxxx"` 是视频文件路径，`DATA_SOURCE: 0` 则表示从摄像头采集；两者只能保留一个。

```
python ./tools/run_net.py --cfg demo/Kinetics/I3D_NLN_8x8_R50.yaml
```
需要显存 0.9GB

时间构成：
  - 视频解码：~1ms，共64帧
  - 预处理（视频帧转化为tensor）：7.8s
  - 预测：1.1s


## 运行测试（AVA）

### 准备数据
[2.4]

```
ln -s /home/duanyao/project/mmaction.git/data/ava/videos_trainval/1j20qq1JyX4.mp4 data/ava/videos/
bash -x cut.sh
bash -x extframes.sh
```

将以下文件备份，然后只保留需要测试的视频相关的部分：

```
data/ava/annotations/ava_val_predicted_boxes.csv
data/ava/frame_lists/val.csv
```

### 运行 `SLOWFAST_32x2_R101_50_50.pkl` 模型：

```
python tools/run_net.py \
  --cfg configs/AVA/c2/SLOWFAST_32x2_R101_50_50.yaml \
  DATA.PATH_TO_DATA_DIR data/ava \
  TEST.CHECKPOINT_FILE_PATH ./SLOWFAST_32x2_R101_50_50.pkl \
  TEST.BATCH_SIZE 1 \
  NUM_GPUS 1 \
  
```

显存不足：

```
RuntimeError: CUDA out of memory. Tried to allocate 16.00 MiB (GPU 0; 1.96 GiB total capacity; 1.53 GiB already allocated; 6.00 MiB free; 1.55 GiB reserved in total by PyTorch)
```

### 使用 `C2D_8x8_R50.pkl` 模型

修改 configs/AVA/c2/SLOW_8x8_R50.yaml：

```
AVA:
  #TEST_PREDICT_BOX_LISTS: ["person_box_67091280_iou75/ava_detection_val_boxes_and_labels.csv"]
  TEST_PREDICT_BOX_LISTS: ["ava_val_predicted_boxes.csv"]
```

```
python tools/run_net.py \
  --cfg configs/AVA/c2/SLOW_8x8_R50.yaml \
  DATA.PATH_TO_DATA_DIR data/ava \
  TEST.CHECKPOINT_FILE_PATH ./C2D_8x8_R50.pkl \
  TEST.CHECKPOINT_TYPE caffe2 \
  TEST.BATCH_SIZE 1 \
  NUM_GPUS 1 \
  AVA.FRAME_DIR data/ava/frames \
  AVA.FRAME_LIST_DIR data/ava/frame_lists \
  AVA.ANNOTATION_DIR data/ava/annotations \
```

```
[INFO: ava_eval_helper.py:  158]: Evaluating with 50250 unique GT frames.
[INFO: ava_eval_helper.py:  160]: Evaluating with 834 unique detection frames
[INFO: ava_eval_helper.py:  300]: AVA results wrote to detections_latest.csv
[INFO: ava_eval_helper.py:  301]: 	took 0 seconds.
[INFO: ava_eval_helper.py:  300]: AVA results wrote to groundtruth_latest.csv
[INFO: ava_eval_helper.py:  301]: 	took 0 seconds.

  'PascalBoxes_Precision/mAP@0.5IOU': 0.0010719635006804164
```

内存占用 1.4G，显存 1.24G 。

## 代码分析

### 行为检测

# tools/demo_net.py: 214
preds = model(inputs, boxes)  # model(): SlowFastModel.forward()

# slowfast/models/video_model_builder.py

class SlowFastModel
  # bboxes 中间帧检出的人的包围盒. torch.Tensor<float,(22,5)> . 第1轴可能是人的序号，第2轴可能是：(0, x1,y2,x2,y2) ，单位应该是像素，第一个元素没用。高度帧缩放到 TEST_CROP_SIZE，即 256，宽度随动。
  forward(self, x, bboxes=None): torch.Tensor<float,(22,80)>

# slowfast/models/head_helper.py
class ResNetRoIHead
  # bboxes 中间帧检出的人的包围盒
  forward(self, inputs, bboxes):
    # out = roi_align(out, bboxes) # roi_align: from detectron2.layers import ROIAlign
    
# /home/duanyao/project/detectron2.git/detectron2/layers/roi_align.py
# ROI Align 是在Mask-RCNN这篇论文里提出的一种区域特征聚集方式


[1.4 p119]，slowfast/models/video_model_builder.py: 523

head_helper.ResNetRoIHead(
  dim_in=[width_per_group * 32],
  num_classes=NUM_CLASSES,
  pool_size=POOL_SIZE,
  resolution=ROI_XFORM_RESOLUTION,
  scale_factor=SPATIAL_SCALE_FACTOR, 
  dropout_rate=cfg.MODEL.DROPOUT_RATE,
  act_func="sigmoid", 
)
  
