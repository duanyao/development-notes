## 参考资料

[1.1] ffmpeg中的roi encoding介绍
https://zhuanlan.zhihu.com/p/350700335

---------

中科大的AI图像/视频编解码综述
https://zhuanlan.zhihu.com/p/379450898

Deep Learning-Based Video Coding: A Review and a Case Study
https://arxiv.org/abs/1904.12462

AIVC: Artificial Intelligence based Video Codec
https://arxiv.org/abs/2202.04365

Improving Video Compression with AI
https://www.mediakind.com/blog/improving-video-compression-with-ai/

Topaz Video AI focuses solely on completing a few video enhancement tasks really well: deinterlacing, upscaling, and motion interpolation.
https://www.topazlabs.com/topaz-video-ai

CompressAI：InterDigital开源基于学习的图像视频压缩研究库
https://zhuanlan.zhihu.com/p/314961069

CompressAI (compress-ay) is a PyTorch library and evaluation platform for end-to-end compression research.
https://github.com/InterDigitalInc/CompressAI/

CompressAI：InterDigital开源基于学习的图像视频压缩研究库
https://blog.csdn.net/moxibingdao/article/details/110212431

王豪：AI和编码联合优化为视频压缩提供了更多可能
https://cloud.tencent.com/developer/article/1554799

公开课总结：深度学习之视频图像压缩
https://zhuanlan.zhihu.com/p/38248372

用深度学习设计图像视频压缩算法：更简洁、更强大
https://www.leiphone.com/category/ai/rUFVbAmPDRP1bwgP.html


## 基于 ROI 的编码优化
ROI == Reigon Of Interest 感兴趣的区域。
图像和视频编码领域很早就引入了 ROI 机制（例如jpeg、h.264），可以对指定的感兴趣的区域分配更多的信息量，使其较其它区域更清晰。
不过，获得ROI一般并不由图像和视频编码器本身负责，而是由外部信息源提供。
在可以使用AI模型的情况下，让AI模型生成ROI，再传递给图像和视频编码器，就是个比较理想的方案。
