## 安装
### pytorch 版
参照 `https://github.com/MVIG-SJTU/AlphaPose/tree/pytorch/README.md` 操作。

检出 pytorch 分支，运行：
```
sudo pip3 install -r requirements.txt  
```
注意 alpha pose 用的是 python3。文档里说命令是 pip ，但是在同时安装了 python2 和 python3 的环境里，必须写 pip3 。

有时候 pip 下载大文件可能失败，可以记下它下载的 url，用其它工具（如wget）下载，然后用 pip3 install xxx.whl 安装。

如果出现 “Could not find a version that satisfies the requirement ntpath” 错误，可以从 requirements.txt 中删除 ntpath，因为只有 windows 上需要它。

下载 model 到相关目录。

### 更换 yolo 模型
默认的 yolov3-spp 模型占用显存过多。

```
cd /models/yolo
wget https://pjreddie.com/media/files/yolov3.weights
wget https://pjreddie.com/media/files/yolov3-tiny.weights

cd /yolo/cfg
wget https://raw.githubusercontent.com/pjreddie/darknet/master/cfg/yolov3-tiny.cfg
```

修改 `https://github.com/MVIG-SJTU/AlphaPose/blob/pytorch/dataloader.py#L275`，将文件名中的 '-spp' 去掉，或者改成 '-tiny'。
```
        # self.det_model = Darknet("yolo/cfg/yolov3-spp.cfg")
        # self.det_model.load_weights('models/yolo/yolov3-spp.weights')
        self.det_model = Darknet("yolo/cfg/yolov3.cfg")
        self.det_model.load_weights('models/yolo/yolov3.weights')
```

如果要改用 yolov3-tiny 模型，需要更新一下 yolo 的代码，并合并 alpha pose 的修改。

## 准备测试数据

来源：/media/DATA/saved/record-video/3540577920702202872/2018-12-11GMT+8/1544512879478.mp4

命令：

cd /home/duanyao/project/sample_record/body_point_image/576p/
ffmpeg -i /media/DATA/saved/record-video/3540577920702202872/2018-12-11GMT+8/1544512879478.mp4 -vf scale=h=576:w=-1 -r 0.1 -q:v 3 %d.jpg

## 检测图像
注意，输入目录下不能有子目录，否则不能检测所有的图像。

修改源码减少内存占用：

```demo.py:

@@ -34,23 +34,25 @@ if __name__ == "__main__":

     # Load input images
-    data_loader = ImageLoader(im_names, batchSize=args.detbatch, format='yolo').start()
+    data_loader = ImageLoader(im_names, batchSize=args.detbatch, format='yolo', queueSize=1).start()
 
     # Load detection loader
     print('Loading YOLO model..')
     sys.stdout.flush()
-    det_loader = DetectionLoader(data_loader, batchSize=args.detbatch).start()
-    det_processor = DetectionProcessor(det_loader).start()
+    det_loader = DetectionLoader(data_loader, batchSize=args.detbatch, queueSize=1).start()
+    det_processor = DetectionProcessor(det_loader, queueSize=1).start()

```
queueSize 的默认值比较大，所以占用内存多。


```
\time -v python3 demo.py --indir examples/demo --outdir examples/res --save_img --sp --posebatch 1 --detbatch 1

\time -v python3 demo.py --indir /home/duanyao/project/sample_record/body_point_image/576p/ --outdir /home/duanyao/project/sample_record/body_point_image/out/576p-alphapose --save_img --sp --posebatch 1 --detbatch 1 --profile --inp_dim=1024
```

`--sp --posebatch 1 --detbatch 1` 均有助于减少内存占用。

inp_dim=1024 用来提高检测精度，因为默认的608比较小。这个值的意思是，将图像缩小到宽度 inp_dim（像素）再检测。不过，输出的图像似乎仍然不如原图清晰。

用 time 测量（apt install time）CPU 和内存占用，大约需要2.5GB内存，1GB显存。

用 `watch -n 0.5 nvidia-smi` 测量 GPU 和显存占用，或者用 psensor 测量。

纯CPU模式，需要内存 1.6GB。

用教室中的照片来测试，大约能达到 0.2 fps（GeForce GTX 950 2GB显存）；纯CPU模式，占用 2.2 vCPU@3GHz，大约达到 0.05 fps。

## 摄像头
拍摄视频并输出到检测的结果到文件。

python3 webcam_demo.py --posebatch 1 --detbatch 1 --profile --save_video --outdir=./webcam_image

此程序存在内存占用过多的问题，原因是一些数据队列的默认值太高，我们给调低到5。此外，输出视频的帧率太高（25），实际上跟不上，所以调低到2。视频格式也改为MP4：

```webcam_demo.py:

@@ -38,18 +38,18 @@ if __name__ == "__main__":
     mode = args.mode
     if not os.path.exists(args.outputpath):
         os.mkdir(args.outputpath)
 
     # Load input video
-    data_loader = WebcamLoader(webcam).start()
+    data_loader = WebcamLoader(webcam, batchSize=1, queueSize=5).start()
     (fourcc,fps,frameSize) = data_loader.videoinfo()
 
     # Load detection loader
     print('Loading YOLO model..')
     sys.stdout.flush()
-    det_loader = DetectionLoader(data_loader, batchSize=args.detbatch).start()
-    det_processor = DetectionProcessor(det_loader).start()
+    det_loader = DetectionLoader(data_loader, batchSize=args.detbatch, queueSize=5).start()
+    det_processor = DetectionProcessor(det_loader, queueSize=5).start()
     
     # Load pose model
     pose_dataset = Mscoco()
     if args.fast_inference:
         pose_model = InferenNet_fast(4 * 1 + 1, pose_dataset)
@@ -57,12 +57,14 @@ if __name__ == "__main__":
         pose_model = InferenNet(4 * 1 + 1, pose_dataset)
     pose_model.cuda()
     pose_model.eval()
 
     # Data writer
-    save_path = os.path.join(args.outputpath, 'AlphaPose_webcam'+webcam+'.avi')
-    writer = DataWriter(args.save_video, save_path, cv2.VideoWriter_fourcc(*'XVID'), fps, frameSize).start()
+    # save_path = os.path.join(args.outputpath, 'AlphaPose_webcam'+webcam+'.avi')
+    # writer = DataWriter(args.save_video, save_path, cv2.VideoWriter_fourcc(*'XVID'), fps, frameSize).start()
+    save_path = os.path.join(args.outputpath, 'AlphaPose_webcam'+webcam+'.mp4')
+    writer = DataWriter(args.save_video, save_path, cv2.VideoWriter_fourcc(*'mp4v'), 2, frameSize).start()
```

改后的结果是，CPU占用35%（8vCPU），内存占用 2.4G，显存占用 1G，GPU占用100%，每一帧耗时1.4秒。

## 性能测试
图片：/home/duanyao/project/sample_record/key_point_test/

3张教室图片，缩放到800x450，人数25。分别有5、3、2人未检出。

作为对比，百度有3，2，0人未检出。

\time -v python3 demo.py --indir /home/duanyao/project/sample_record/key_point_test/ --outdir examples/res --save_img --sp --posebatch 1 --detbatch 1 --profile
Loading YOLO model..
Loading pose model from ./models/sppe/duc_se.pth
det time: 0.085 | pose time: 4.36 | post processing: 0.0451: 100%| 3/3 [00:13<00:00,  4.64s/it]
===========================> Finish Model Running.
===========================> Rendering remaining images in the queue...
===========================> If this step takes too long, you can enable the --vis_fast flag to use fast rendering (real-time).
	Command being timed: "python3 demo.py --indir /home/duanyao/project/sample_record/key_point_test/ --outdir examples/res --save_img --sp --posebatch 1 --detbatch 1 --profile"
	User time (seconds): 23.05
	System time (seconds): 5.95
	Percent of CPU this job got: 134%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 0:21.59
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 2259444
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 20467
	Minor (reclaiming a frame) page faults: 698414
	Voluntary context switches: 33444
	Involuntary context switches: 2640
	Swaps: 0
	File system inputs: 1400136
	File system outputs: 1000
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0

	
