## 参考资料

[3.1] ssd in mmdetection
  https://github.com/open-mmlab/mmdetection/blob/master/configs/ssd/

## 各种实现的简介

### SSD in mmdetection
有3种变体：

* SSD-VGG16-300
* SSD-VGG16-512
* SSD-MobileNetV2-320
