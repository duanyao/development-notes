## 参考资料
[1.1] tensorflow
  https://github.com/tensorflow/tensorflow

[1.2] TensorFlow Documentation
  https://github.com/tensorflow/docs

[1.3] TensorFlow Documentation - Install
 https://github.com/tensorflow/docs/tree/master/site/en/install

[1.7] TensorFlow 中文社区
  http://tensorfly.cn

[1.8] 『深度长文』Tensorflow代码解析（一）
  https://zhuanlan.zhihu.com/p/25646408
  
[1.9] TensorFlow知识点
  https://www.jianshu.com/p/9772de4ceb0e

[1.10] How to prevent tensorflow from allocating the totality of a GPU memory?
  https://stackoverflow.com/questions/34199233/how-to-prevent-tensorflow-from-allocating-the-totality-of-a-gpu-memory

[1.11] CUDA_VISIBLE_DEVICES 环境变量说明
  https://www.jianshu.com/p/0816c3a5fa5c

[1.12] CUDA_DEVICE_ORDER 环境变量说明
  https://www.jianshu.com/p/d10bfee104cc

[1.13] 使用 GPU
  https://www.tensorflow.org/guide/gpu
  
[2.1] TensorFlow deepin 安装
  https://wiki.deepin.org/index.php?title=TensorFlow

[2.2] 机器学习进阶笔记之一 | TensorFlow安装与入门
  https://zhuanlan.zhihu.com/p/22410917

[2.3] Tensorflow不同版本要求与CUDA及CUDNN版本对应关系
  https://blog.csdn.net/K1052176873/article/details/114526086

[2.4] CUDA 和 CUDNN 的linux包下载地址
  https://www.zhihu.com/question/37082272

[2.5] libcudnn和libnccl deb 仓库
  https://developer.download.nvidia.cn/compute/machine-learning/repos/ubuntu1604/x86_64/

[2.6] 显卡驱动和cuda开发套件 deb 仓库
  https://developer.download.nvidia.cn/compute/cuda/repos/ubuntu1604/x86_64/
  
[2.7] tensorrt-support-matrix
  https://docs.nvidia.com/deeplearning/sdk/tensorrt-support-matrix/index.html
  
[2.8] Tested build configurations
  https://tensorflow.google.cn/install/source?hl=en#tested_build_configurations

[3.0] 从源码安装(略)

[4.1] Ubuntu装好CUDA之后过段时间提示NVIDIA-SMI has failed because it couldn't communicate with the NVIDIA driver.
  https://www.cnblogs.com/feifanrensheng/p/9974249.html
  
[5.2] Low GPU usage for inference when multithreaded vs multiprocess c++
  https://github.com/tensorflow/tensorflow/issues/26356

[5.3] Get 10x Speedup in Tensorflow Multi-Task Learning using Python Multiprocessing
  https://hanxiao.github.io/2017/07/07/Get-10x-Speedup-in-Tensorflow-Multi-Task-Learning-using-Python-Multiprocessing/
  
[6.1] conda的安装与使用（2019-6-28更新）
  https://www.jianshu.com/p/edaa744ea47d
  
[6.2] deepin 15.11 安装 tensorflow or pytorch (基于conda)
  https://jansora.com/post/deepin-deeplearning-gpu-dependency
  
[10.1] python3: Relink issue #19375
  https://github.com/tensorflow/tensorflow/issues/19375

## 数据操作

### 转换类型

A = X.numpy()
B = tf.constant(A)
tf.cast(x, dtype, name=None) # uint8, uint16, uint32, uint64, int8, int16, int32, int64, float16, float32, float64, complex64, complex128, bfloat16


### 原地修改变量

可修改的变量：

tf.Variable(tf.zeros_like(Y))

计算图：
```
@tf.function
def computation(X, Y):
    Z = tf.zeros_like(Y)  # 这个未使用的值将被删除
    A = X + Y  # 当不再需要时，分配将被复用
    B = A + Y
    C = B + Y
    return C + Y

computation(X, Y)
```
计算图会优化临时变量使用的内存。
计算图不能用于python交互环境。

### 矩阵

```
A = tf.reshape(tf.range(20), (5, 4))

<tf.Tensor: shape=(5, 4), dtype=int32, numpy=
array([[ 0,  1,  2,  3],
       [ 4,  5,  6,  7],
       [ 8,  9, 10, 11],
       [12, 13, 14, 15],
       [16, 17, 18, 19]], dtype=int32)>

tf.transpose(A)  # 转置

B = tf.constant([[1, 2, 3], [2, 0, 4], [3, 4, 5]])

B == tf.transpose(B)

<tf.Tensor: shape=(3, 3), dtype=bool, numpy=
array([[ True,  True,  True],
       [ True,  True,  True],
       [ True,  True,  True]])>

```
## 安装
### pip 非 GPU 版
* 安装 pip
* 安装 tf
 pip install tf-nightly
 
 pip install tensorflow==version

 
python3 可以改为 pip3
 
可以在 python virtualenv 中安装。

### conda/minicoda 安装
除了显卡驱动，其它一切（包括 cuda）的安装都可以由 conda/minicoda 包办，所以是很方便的，参考[6.1, 6.2]。

* 安装 nvidia 的驱动。

* 下载 minconda 安装器，从 https://docs.conda.io/en/latest/miniconda.html 下载 Miniconda3-latest-Linux-x86_64.sh 。

* 执行 bash Miniconda3-latest-Linux-x86_64.sh

  这会询问一些问题。例如 miniconda 的安装位置，默认是 ~/miniconda3 ，可以更改，但一开始最好不要装到全局位置。我们假设装到了 ~/opt/miniconda3 。
  另一个问题是“Do you wish the installer to initialize Miniconda3 by running conda init? ”最好回答no ，这样可避免与 miniconda 外的环境互相干扰。
  回答完问题就开始安装，~/opt/miniconda3 下将会包含 conda 本身和 python ，python 一般都是很新的版本，如 3.7 （2019.1）。

* 激活 miniconda 环境。在任何目录下执行 `. ~/opt/miniconda3/bin/activate` 即可，这样会把 `~/opt/miniconda3/bin` 添加到 PATH 中，这个目录里包含了 conda 和 python 。
  其实不做激活也能用，只要用全路径执行 `~/opt/miniconda3/bin` 里的命令即可。

* 添加镜像源。编辑 ~/.condarc
  
  ```
  channels:
    - https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/conda-forge/
    - https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/
    - https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/
    - defaults
  ```

  或者用命令添加 `conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/`。

* (可选) 降级 python 版本。tensorflow 对 python 的最高版本可能有限制，例如 tensorflow 1.12 最高允许 python3.6 。如果 miniconda 自带的 python 更高，可以降级：
  `conda install python=3.6` 。

* 安装 tensorflow 。先列出可安装的版本 `conda search tensorflow-gpu` ，然后安装一个：`conda install tensorflow-gpu=1.15.0` 。
  这也会同时安装匹配 tensorflow 的依赖，包括 cudatoolkit, cudnn, cupti 等。安装的位置也是 `~/opt/miniconda3/` 。

* 使用 tensorflow 。启动  `~/opt/miniconda3/bin/python` ，执行 `import tensorflow` 如果无错误就表明安装成功了。以后用 `~/opt/miniconda3/bin/python` 即可使用 tensorflow 。

* 在 miniconda 环境中安装其它 python 包。这可以用 `~/opt/miniconda3/bin/pip` 来实现，用法与全局 pip 相同，只不过会装到 `~/opt/miniconda3/` 下。
  对于 python 包，pip 和 conda 包似乎是等效的，但 conda 还管理非 python 包。

### GPU 版
安装 GPU 驱动程序及其工具，参考 nvidia-linux.md 。

指定或不指定版本的安装：

```
pip install tensorflow-gpu
pip install tensorflow-gpu==1.5.0
```
建议在 python venv 中安装。

注意 CUDA 和 CUDNN 和 tensorflow, Bazel互相的版本要求，可以参考 [2.3, 2.8]——也不完全准确，只有大版本号是准确的，但 tensorflow 对 CUDA 的版本号依赖会精确到小版本号，
例如 tensorflow 1.0-1.4 依赖 CUDA 8.0，tensorflow 1.5-1.12 依赖 CUDA 9.0，CUDA 9.1 和 CUDA 9.2 则没有被任何 pip 版 tensorflow 支持（但从源码编译 tensorflow 1.5-1.7则支持）。

有时候 pip 下载大文件可能失败，可以记下它下载的 url，用其它工具（如wget）下载，然后用 pip install xxx.whl 安装。

## python 版本与 pip tensorflow

每个版本的 pip tensorflow 只会编译有限范围的 python 版本，所以较新的 python 版本无法安装较旧的 pip tensorflow ，较旧的 python 版本也无法安装较新的 pip tensorflow 。
版本依赖关系可以看 [2.8] 。

## 验证安装 /hello world
以下可以验证 GPU 是否正常工作：
```
python -c "import tensorflow as tf;x = tf.range(12);x.device"
```
或者
```
import tensorflow as tf
x = tf.range(12)
x.device # '/job:localhost/replica:0/task:0/device:GPU:0'
```
如果失败，会显示错误，如：
```
E tensorflow/stream_executor/cuda/cuda_driver.cc:328] failed call to cuInit: CUDA_ERROR_UNKNOWN: unknown error
```
如果正常，会显示：
```
I tensorflow/core/common_runtime/gpu/gpu_device.cc:1418] Created TensorFlow device ...
```
并且 nvidia-smi 会显示这个 python 进程占用了一定的 GPU 内存。

[2.2]

1.x:
```
import tensorflow as tf
hello = tf.constant('Hello, TensorFlow!')
sess = tf.Session()
print(sess.run(hello))
```

2.x:
```
import tensorflow as tf
s = tf.compat.v1.Session()
with tf.compat.v1.get_default_graph().as_default():
  h = tf.constant('Hello, this is TensorFlow')
  print(s.run(h))
```

如果报错，请参考 nvidia-linux.md 。

### 安装 docker 版

https://github.com/NVIDIA/nvidia-docker
https://github.com/NVIDIA/nvidia-container-runtime

安装 nvidia-container-toolkit：

```
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey |   sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/debian9/nvidia-docker.list |   sudo tee /etc/apt/sources.list.d/nvidia-docker.list
sudo apt-get update
sudo apt-get install nvidia-container-toolkit nvidia-container-runtime
```

其中 “debian9” 也可以适用于 deepin 15.11, 其它发行版根据 `/etc/os-release` 里的 `ID` 和 `VERSION_ID` 对应修改。

版本标记在这里找：https://hub.docker.com/r/tensorflow/tensorflow/tags/

sudo docker pull tensorflow/tensorflow:latest-gpu-py3
sudo docker pull tensorflow/tensorflow:1.12.3-gpu-py3

docker run -it tensorflow/tensorflow:latest-gpu-py3 bash

新建或修改 /etc/docker/daemon.json

```
{
    "runtimes": {
        "nvidia": {
            "path": "/usr/bin/nvidia-container-runtime",
            "runtimeArgs": []
        }
    }
}
```

sudo systemctl restart docker

sudo docker run --runtime=nvidia -it --rm tensorflow/tensorflow:1.12.3-gpu-py3 sh

注意，tensorflow:1.12 是最高的可以与 nvidia-driver 390 以及 cuda 9 一起工作的版本，但升级 nvidia-driver 很麻烦。

错误：

docker: Error response from daemon: containerd: container not started.

去掉 --runtime=nvidia 是可以启动的。

修改 /etc/nvidia-container-runtime/config.toml

[nvidia-container-runtime]
debug = "/var/log/nvidia-container-runtime.log"

sudo docker run --runtime=nvidia --rm nvidia/cuda nvidia-smi

错误：

docker: Error response from daemon: containerd: container not started.

cat /var/log/nvidia-container-runtime.log
```
2020/01/09 17:02:46 Running /usr/bin/nvidia-container-runtime
2020/01/09 17:02:46 Using bundle file: /var/run/docker/libcontainerd/718ae3616e7d9e475ec8954da3a016712c8466ee4ba1b956d4472cf7deea9169/config.json
2020/01/09 17:02:46 ERROR: unmarshal OCI spec file: json: cannot unmarshal array into Go struct field Process.capabilities of type specs.LinuxCapabilities
2020/01/09 17:02:46 Running /usr/bin/nvidia-container-runtime
2020/01/09 17:02:46 Command is not "create", executing runc doing nothing
2020/01/09 17:02:46 Looking for "docker-runc" binary
2020/01/09 17:02:46 Runc path: /usr/bin/docker-runc
2020/01/09 17:02:46 Running /usr/bin/nvidia-container-runtime
2020/01/09 17:02:46 Command is not "create", executing runc doing nothing
2020/01/09 17:02:46 Looking for "docker-runc" binary
2020/01/09 17:02:46 Runc path: /usr/bin/docker-runc
```

这是因为 docker 的版本低，即 docker-engine 1.12 。改为安装 docker-ce 19.03 可以解决。

## 多线程、多进程

tensorflow 允许使用多线程进行推理，随着线程的增加，吞吐量会有一定的增加，GPU利用率会提高。但是，单进程多线程不一定能充分利用 GPU。

多进程共享GPU推理也是可以的，而且似乎能获得比多线程更高的吞吐量和GPU利用率，能够达到100%利用 GPU。

## 限制显存的用量

默认情况下，一个 tensorflow session 会试图利用所有的 GPU 的所有显存。如果要使用多进程，则必须限制 tensorflow 使用的 GPU 和显存。

为了在一个 GPU 上跑多个 tensorflow 进程，可以设置 `per_process_gpu_memory_fraction`[1.9,1.10]：

```
gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.333)
sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
```
tf 1.x 只能设置占用显存的比例，不能设置绝对量。

2.x 可以让显存用量随实际需求动态增长[1.13]（不过，目前只能增长，不能缩小）：

```
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
  try:
    # Currently, memory growth needs to be the same across GPUs
    for gpu in gpus:
      tf.config.experimental.set_memory_growth(gpu, True)
    logical_gpus = tf.config.experimental.list_logical_devices('GPU')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
  except RuntimeError as e:
    # Memory growth must be set before GPUs have been initialized
    print(e)
```

第二个启用此选项的方式是将环境变量 TF_FORCE_GPU_ALLOW_GROWTH 设置为 true（只能小写）[1.13]。

也可以设置显存用量的绝对上限[1.13]：

```
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
  # Restrict TensorFlow to only allocate 1GB of memory on the first GPU
  try:
    tf.config.experimental.set_virtual_device_configuration(
        gpus[0],
        [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=1024)])
    logical_gpus = tf.config.experimental.list_logical_devices('GPU')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
  except RuntimeError as e:
    # Virtual devices must be set before GPUs have been initialized
    print(e)
```

## 限制对进程可见的 GPU

为了在多个GPU的机器上跑多个 tensorflow 进程，可以给每个进程设置 `CUDA_VISIBLE_DEVICES` 环境变量，限制每个进程可用的 GPU [1.11]。例如：

```
CUDA_VISIBLE_DEVICES=1
CUDA_VISIBLE_DEVICES=0,1
```

GPU 是从0开始编号的，但其顺序并未定义。要采用确定编号的顺序，则最好设置环境变量 `CUDA_DEVICE_ORDER = PCI_BUS_ID` [1.12]。
如果 CUDA_VISIBLE_DEVICES 设置了但格式不对，会导致 GPU 对进程不可用，有的程序会改用 CPU 模式，如 tensorflow，这也是强制 tensorflow 不使用 GPU 的一个方法。

## 模型的保存、优化、加载
加载 checkpoint：

```
ckpt = tf.train.get_checkpoint_state( train_dir, latest_filename="checkpoint") # ckpt: tensorflow.python.training.checkpoint_state_pb2.CheckpointState
saver.restore( session, ckpt.model_checkpoint_path )  # saver: tensorflow.python.training.saver.Saver 对象， session: tensorflow.python.client.session.Session 对象
```
train_dir 是个目录，下面应有名为 latest_filename 的文件，其内容为：

```
model_checkpoint_path: "checkpoint-4874200"
all_model_checkpoint_paths: "checkpoint-4654861"
all_model_checkpoint_paths: "checkpoint-4874200"
```
每个 checkpoint-nnnnnnn 表示一个 checkpoint，每个checkpoint 由以下文件组成：

```
checkpoint-4874200.data-00000-of-00001
checkpoint-4874200.index
checkpoint-4874200.meta
```

`get_checkpoint_state()` 返回一个对象，其包含了 latest_filename 中列出的的所有 checkpoint 。
ckpt.model_checkpoint_path 则是 latest_filename 中的 model_checkpoint_path 所指的路径，即 `"<train_dir>/checkpoint-4874200"`。

保存为 pb 文件
```
def save_graph_to_file(sess, graph, graph_file_name):
  output_graph_def = graph_util.convert_variables_to_constants(
      sess, graph.as_graph_def(), [FLAGS.final_tensor_name])
  with gfile.FastGFile(graph_file_name, 'wb') as f:
    f.write(output_graph_def.SerializeToString())
  return
```

## 故障排除

### Relink 问题

[10.1]
执行 `import tensorflow` 时可能抛出异常：

```
python3: Relink /lib/x86_64-linux-gnu/libudev.so.1' with /lib/x86_64-linux-gnu/librt.so.1' for IFUNC symbol `clock_gettime'
Segmentation fault (core dumped)
```

libudev.so.1 也可能是其他文件。
这个错误的原因可能很多，一般是 cuda 或 cudnn 不能正常工作。可参照 nvidia-linux.md ，先确保 nvidia-smi 正确显示 cuda 的版本号。
