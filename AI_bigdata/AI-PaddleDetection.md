## 参考资料

[1.1] PaddleDetection docs
https://paddledetection.readthedocs.io/

[3.1] Paddle Serving服务化部署框架
https://www.paddlepaddle.org.cn/tutorials/projectdetail/1975340

[3.2] 模型导出
https://paddledetection.readthedocs.io/advanced_tutorials/deploy/EXPORT_MODEL.html

[5.1] Windows 10 编译 Pycocotools 踩坑记 https://www.jianshu.com/p/de455d653301

## 安装

### 安装 paddle

#### linux python venv
```
source ~/opt/venv/v39-1/bin/activate
python -m pip install paddlepaddle-gpu==2.2.0.post112 -f https://www.paddlepaddle.org.cn/whl/linux/mkl/avx/stable.html
# 其它版本： 2.3.2.post112, 2.4.2.post112
```

#### windows 

conda + pip

```
conda activate c39
conda install cudatoolkit cudnn
python -m pip install paddlepaddle-gpu==2.2.1.post112 -f https://www.paddlepaddle.org.cn/whl/windows/mkl/avx/stable.html
```

conda
```
conda activate c39
conda install paddlepaddle-gpu==2.2.1 -c https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/Paddle/ -c conda-forge
```

#### nvidia docker 镜像 + python venv

不论是推理还是训练，paddlepaddle-gpu==2.2.0.post112 都需要基于 cudnn-devel 镜像，例如 `nvidia/cuda:11.2.2-cudnn8-devel-ubuntu20.04`，cudnn8-runtime 镜像如 `nvidia/cuda:11.2.2-cudnn8-runtime-ubuntu20.04` 仍然缺少一些东西，如 libcudnn.so 和 libcublas.so 。

#### paddle docker 镜像

基于 ubuntu 18.04, python 3.7
paddle 包的位置：
'/usr/local/lib/python3.7/dist-packages/paddle/__init__.py'

#### 验证 paddle 安装正确

```
python

import paddle
paddle.utils.run_check()
```

### 安装 visualdl

pip install visualdl

### 安装 PaddleDetection.git

#### 选择源码的分支

develop 分支是最新的代码，但可能有较多错误，甚至一些脚本会跑步起来。
release/2.4 等是发布分支，相对稳定一些。

#### linux

安装 python venv 和开发包：
```
sudo apt install python3.8-venv python3.8-dev
```

安装 cocoapi 及其依赖（假定源码在 ~/project/cocoapi.git）：
```
pip install Cython matplotlib  # coco api 需要
cd ~/project/cocoapi.git/PythonAPI
python setup.py install
```
或者用 pip git 安装命令：
```
pip install git+https://github.com/cocodataset/cocoapi.git#subdirectory=PythonAPI
```
或者使用镜像仓库：
```
pip install git+https://gitee.com/mlyin/cocoapi#subdirectory=PythonAPI
```

不能用 `pip install pycocotools` ，会出错（2022.1）。

安装 PaddleDetection 本体：
```
cd ~/project/PaddleDetection.git
pip install -r requirements.txt
python setup.py install
```

#### windows

先安装 visual studio 生成工具或 visual studio 。要选中“C++桌面开发”组件，并确保选中 windows 10 SDK 或一个其它版本的 windows SDK 。
在 conda 环境中，先安装以下依赖：
```
pip install Cython
```

下载 cocoapi 的源码，在 cocoapi/PythonAPI 源码目录下，修改 setup.py 文件，去掉 '-Wno-cpp', '-Wno-unused-function' 编译器参数（因为gcc 支持，cl 不支持），然后 [5.1] ：
```
python setup.py install
```
下载 cython_bbox 的源码（ https://github.com/samson-wang/cython_bbox ），安装。pip 上的 cython_bbox 有类似 cocoapi 的问题。
```
python setup.py install
```

安装本体：
```
cd ~/project/PaddleDetection.git
pip install -r requirements.txt
python setup.py install # 安装了 paddledet 包
```

#### 测试安装是否正确

```
python ppdet/modeling/tests/test_architectures.py

python tools/infer.py -c configs/ssd/ssd_mobilenet_v1_300_120e_voc.yml -o weights=https://paddledet.bj.bcebos.com/models/ssd_mobilenet_v1_300_120e_voc.pdparams use_gpu=true --infer_img=demo/000000570688.jpg
python tools/infer.py -c configs/centernet/centernet_r50_140e_coco.mod1.yml -o weights=https://bj.bcebos.com/v1/paddledet/models/centernet_r50_140e_coco.pdparams use_gpu=true --infer_img=demo/000000570688.jpg
```

#### 版本升级/降级

PaddleDetection.git 的源码有较大跨度的升级（次版本号改变，如2.4/2.5）后，train.py 等脚本可能运行出错，因此需要升级安装，即运行 
```
pip uninstall paddledet
pip install -r requirements.txt
python setup.py install
```

## 设置数据目录（可选）
训练/测试/导出模型时，可能产生很多数据文件。为了避免把 PaddleDetection 源码目录弄得很大很复杂，可以做如下设计。

```
~/project/PaddleDetection.git/ # PaddleDetection 源码目录
    eval -> ../data/PaddleDetection/eval
    inference_model -> ../data/PaddleDetection/inference_model
    infer_output -> ../data/PaddleDetection/infer_output
    output -> ../data/PaddleDetection/output

~/project/data/PaddleDetection/ # PaddleDetection 产生的数据目录
    eval            # tools/eval.py 产生的评估数据
    inference_model # tools/export_model.py 产生的用于预测的模型目录
    infer_output    # deploy/python/infer.py 产生的预测结果文件
    output          # tools/train.py 产生的检查点文件等。
```

配置方法：

```
mkdir -p ~/project/data/PaddleDetection/
cd ~/project/data/PaddleDetection/
mkdir eval inference_model infer_output output
cd ~/project/PaddleDetection.git/
ln -s ../data/PaddleDetection/* .
```

## PaddleSlim
安装：
```
pip install paddleslim -i https://pypi.tuna.tsinghua.edu.cn/simple
```

修改 configs/slim/{SLIM_CONFIG.yml} ，将 pretrain_weights 改为训练好的模型，如：
```
pretrain_weights: output/ssd_r34_70e_432p_head_06/best_model.pdparams
```

slim 配置文件有量化（quant）、蒸馏（distill）、裁剪（prune），综合（extensions）。

运行：

```
python tools/train.py -c configs/{MODEL.yml} --slim_config configs/slim/{SLIM_CONFIG.yml}
python tools/eval.py -c configs/{MODEL.yml} --slim_config configs/slim/{SLIM_CONFIG.yml} -o weights=output/{SLIM_CONFIG}/model_final
python tools/infer.py -c configs/{MODEL.yml} --slim_config configs/slim/{SLIM_CONFIG.yml} -o weights=output/{SLIM_CONFIG}/model_final --infer_img={IMAGE_PATH}

python tools/export_model.py -c configs/{MODEL.yml} --slim_config configs/slim/{SLIM_CONFIG.yml} -o weights=output/{SLIM_CONFIG}/model_final
```

## 非兼容性变更

* 2.4 以后（2022.5.1）， train.py 的 --fp16 参数改名为 --amp （auto mixed precision）。

## 本地部署

本地部署（非 serving 部署）时，部署的环境不需要安装 PaddleDetection ，只需要安装以下 pip 包：`paddlepaddle PyYAML opencv-python scipy`。
如果需要 GPU 版，安装 paddlepaddle-gpu 而非 paddlepaddle ，方法见“安装 paddle”一节。

将模型用 tools/export_model.py 导出，模型目录复制到部署的环境，目录下的文件名不要修改。

然后，将 PaddleDetection.git/deploy/python/ 目录下的代码复制出来，放到要部署的环境里，运行 infer.py 即可，命令行参数参考其 readme。

如果希望在预测时使用 tensorrt 来加速，应当导出模型时加上 -o trt 参数：
```
python tools/export_model.py -c configs/ppyoloe/{MODEL.yml} -o weights=output/{MODEL}/model_final trt=True
```
在预测时，给 `PaddleDetection.git/deploy/python//infer.py:load_predictor()` 传入 `run_mode=‘trt_fp32|trt_fp16|trt_int8‘`（默认是 `‘paddle‘`）和 `device='gpu'`。

## 模型的训练

以 ssd_r34_70e_head_06 为例：

```
cd ~/project/PaddleDetection.git
source ~/opt/venv/v39-1/bin/activate

python tools/train.py -c /home/rd/project/PaddleDetection.git/configs/ssd/ssd_r34_70e_head_06.yml -o use_gpu=true --fp16  --eval 

python -m paddle.distributed.launch --gpus 0,1 tools/train.py -c /home/rd/project/PaddleDetection.git/configs/ssd/ssd_r34_70e_head_06.yml -o use_gpu=true --fp16  --eval -r output/ssd_r34_70e_head_06/14.pdparams

python tools/eval.py -c /home/rd/project/PaddleDetection.git/configs/ssd/ssd_r34_70e_head_06.yml --output_eval eval/ssd_r34_70e_head_06 --classwise -o use_gpu=true  weights=output/ssd_r34_70e_head_06/best_model.pdparams

python tools/infer.py -c configs/ssd/ssd_r34_70e_head_06.yml -o use_gpu=true weights=output/ssd_r34_70e_head_06/best_model.pdparams --infer_img=/home/rd/ai-dataset-e/head_chest_06_test/images/1625191718840-5.jpg

python tools/export_model.py -c configs/ssd/ssd_r34_70e_head_06.yml --output_dir=./inference_model -o weights=output/ssd_r34_70e_head_06/best_model.pdparams  #model_final

python deploy/python/infer.py --model_dir inference_model/ssd_r34_70e_head_06/ --device CPU --enable_mkldnn True --cpu_threads 1 --output_dir infer_output/ssd_r34_70e_head_06/ --image_dir /home/rd/ai-dataset-e/head_chest_06_test/images/ 

python deploy/python/infer.py --model_dir inference_model/ssd_r34_70e_head_06/ --device CPU --enable_mkldnn True --cpu_threads 1 --output_dir infer_output/ssd_r34_70e_head_06/ --image_file /home/rd/ai-dataset-e/head_chest_06_test/images/1625191718840-5.jpg
```

```
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.460
 Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.859
 Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets=100 ] = 0.449
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.271
```

## 数据增广方法

### RandomExpand
ppdet/data/transform/operators.py
在原图外围加填充区，填充区的大小是随机的，其最大尺寸可配置，填充颜色可配置，是固定的单色。

Args:
        ratio (float): maximum expansion ratio. 最小 1.01，默认4。是填充后的最大尺寸与原图尺寸之比。实际填充后尺寸与原图尺寸之比介于1与ratio之间，均匀分布。
        prob (float): probability to expand. 默认 0.5
        fill_value (list): color value used to fill the canvas. in RGB order. 默认值 (127.5, 127.5, 127.5)

### RandomCrop
Args:
        aspect_ratio (list): aspect ratio of cropped region.
            in [min, max] format. 默认 [.5, 2.]，min, max 可以相同，下同。注意，这并非裁剪区域的宽/高，而是进一步的改变宽/高比。
            h_scale = scale / np.sqrt(aspect_ratio)，w_scale = scale * np.sqrt(aspect_ratio)，crop_h = h * h_scale， crop_w = w * w_scale
        thresholds (list): iou thresholds for decide a valid bbox crop. 默认 [.0, .1, .3, .5, .7, .9]。标注框与裁剪后区域的iou阈值，高于阈值的标注框保留用于训练；提供多个阈值时，每次随机选择一个。
        scaling (list): ratio between a cropped region and the original image. 
             in [min, max] format. 默认 [.3, 1.] 实际缩放系数scale为[min, max] 之间的随机数，均匀分布；裁剪后面积/原面积 == scale^2
        num_attempts (int): number of tries before giving up. 默认 50
        allow_no_crop (bool): allow return without actually cropping them. 默认 True
        cover_all_box (bool): ensure all bboxes are covered in the final crop. 默认 False
        is_mask_crop(bool): whether crop the segmentation. 默认 False

### RandomFlip
左右翻转图像。
Args:
            prob (float): the probability of flipping image

### RandomDistort(BaseOperator):
Random color distortion.
Args:
    hue (list): hue settings. in [lower, upper, probability] format.
    saturation (list): saturation settings. in [lower, upper, probability] format.
    contrast (list): contrast settings. in [lower, upper, probability] format.
    brightness (list): brightness settings. in [lower, upper, probability] format.
    random_apply (bool): whether to apply in random (yolo) or fixed (SSD)
        order.
    count (int): the number of doing distrot
    random_channel (bool): whether to swap channels randomly

### AutoAugment
Args:
    autoaug_type (str): autoaug type, support v0, v1, v2, v3, test

## 日志和模型保存

```
snapshot_epoch: 1
snapshot_iter: 1000
log_iter: 20

max_iters: 90000
epoch: 150
```

## 数据集的操作 dataset

参考: 

PaddleDetection.git/docs/tutorials/PrepareDataSet.md
PaddleDetection.git/static/docs/tutorials/Custom_DataSet.md

### VOC 格式的数据集

VOC 格式的数据集不能直接使用，需要用 create_list.py 工具生成 trainval.txt 和 test.txt 列表。命令：

```
python dataset/voc/create_list.py [voc 数据集目录]
```
不过，这个脚本目前（2.2, 2021.11）有一些bug或怪癖（这与 static/docs/tutorials/Custom_DataSet.md 描述的行为不同）：

(1) 只能处理 dataset/voc/ 目录，参数 [voc 数据集目录] 是没有用的。
要使用自定义数据集，得把自定义数据集的 VOCdevkit 目录链接到 dataset/voc/ 下。
或者改代码：
```
# voc_path = osp.split(osp.realpath(sys.argv[0]))[0]
voc_path = sys.argv[1]
```

(2) 只搜索 <类别>_trainval.txt 和 <类别>_test.txt 文件，不搜索 trainval.txt 和 test.txt 文件。类别只能是小写英文字母。但类别不需要真实。trainval和test不需要都存在。
(3) <类别>_trainval.txt 和 <类别>_test.txt 文件中不能有空行。
(4) 图像文件的扩展名写死为 .jpg，如果实际上不是，则无法找到图像文件。cvat视频标注导出的voc数据集为 .PNG。

```
cd ~/ai-dataset-e/custom_head_voc_01/VOCdevkit/VOC2007/JPEGImages
for f in *.PNG; do ffmpeg -y -i "${f}" -q:v 3 "${f%%.*}.jpg"; done
rm *.PNG

cd ~/ai-dataset-e/custom_head_voc_01/VOCdevkit/VOC2012/JPEGImages
for f in *.PNG; do ffmpeg -y -i "${f}" -q:v 3 "${f%%.*}.jpg"; done
rm *.PNG

find . -type f -name '*.PNG' -exec ffmpeg -y -i "${f}" -q:v 3 "${f%%.*}.jpg" \;
```
(5) 年份只能有 2007 和 2012，其它的被忽略。

转换一个自定义数据集：

```
python dataset/voc/create_list.mod.py ~/ai-dataset-e/custom_head_voc_01/
```
创建 `~/ai-dataset-e/custom_head_voc_01/label_list.txt` ，内容是：`头\n`。

数据集定义文件：

```
metric: VOC
map_type: 11point
num_classes: 2

TrainDataset:
  !VOCDataSet
    #with_background: true
    dataset_dir: /home/rd/ai-dataset-e/custom_head_voc_01
    anno_path: /home/rd/ai-dataset-e/custom_head_voc_01/trainval.txt
    label_list: /home/rd/ai-dataset-e/custom_head_voc_01/label_list.txt
    data_fields: ['image', 'gt_bbox', 'gt_class', 'difficult']

EvalDataset:
  !VOCDataSet
    dataset_dir: /home/rd/ai-dataset-e/custom_head_voc_01
    anno_path: /home/rd/ai-dataset-e/custom_head_voc_01/test.txt
    label_list: /home/rd/ai-dataset-e/custom_head_voc_01/label_list.txt
    data_fields: ['image', 'gt_bbox', 'gt_class', 'difficult']

TestDataset:
  !ImageFolder
    anno_path: /home/rd/ai-dataset-e/custom_head_voc_01/label_list.txt
```
num_classes 是实际类别数+1，默认有个背景类型（with_background: true）。
label_list 属性是必需的，指向前面创建的 label_list.txt。

TestDataset 只被 infer.py 调用。注意其类型不是 VOCDataSet 而是 ImageFolder 。infer.py 应该是只利用 anno_path 中的标签定义，不需要其中的标注。

### COCO 格式的数据集

```
metric: COCO
num_classes: 1

TrainDataset:
  !COCODataSet
    image_dir: images
    anno_path: annotations/instances_default.json
    #label_list: label_list.txt
    dataset_dir: /home/rd/ai-dataset-e/custom_head_coco_04
    data_fields: ['image', 'gt_bbox', 'gt_class', 'is_crowd']

EvalDataset:
  !COCODataSet
    image_dir: images
    anno_path: annotations/instances_default.json
    #label_list: label_list.txt
    dataset_dir: /home/rd/ai-dataset-e/custom_head_coco_03

TestDataset:
  !ImageFolder
    anno_path: annotations/instances_default.json
```

with_background 属性和 label_list 属性不允许出现。num_classes 是刚好的类型数，不能包括背景类型，否则会出错。

TestDataset 只被 infer.py 调用。注意其类型不是 COCODataSet 而是 ImageFolder 。infer.py 应该是只利用 anno_path 中的标签定义，不需其中的标注。


### COCO 数据集裁剪

```
var af = './instances_default.json';
var anno = JSON.parse(fs.readFileSync(af, 'utf-8'));
anno.annotations = anno.annotations.filter(a => a.category_id == 1);
fs.writeFileSync(af, JSON.stringify(anno), 'utf-8');
```

## 模型导出和服务部署
[3.1-3.2]

例子：
```
# 导出FasterRCNN模型, 模型中data层默认的shape为3x800x1333
python tools/export_model.py -c configs/faster_rcnn_r50_1x.yml \
        --output_dir=./inference_model \
        -o weights=output/faster_rcnn_r50_1x/model_final
```
预测模型会导出到inference_model/faster_rcnn_r50_1x目录下，模型名和参数名分别为__model__和__params__

Paddle Serving部署模型导出：
```
# 导出Serving模型需要安装paddle-serving-client
pip install paddle-serving-client
# 导出FasterRCNN模型, 模型中data层默认的shape为3x800x1333
python tools/export_serving_model.py -c configs/faster_rcnn_r50_1x.yml \
        --output_dir=./inference_model \
        -o weights=output/faster_rcnn_r50_1x/model_final
```

## bug 或怪癖或兼容性问题

### 小训练集的问题
（1）训练样本数 T 如果不能被 batch_size 整除，则余数部分样本可能被舍弃。
（2）如果训练样本数小于 batch_size，则训练会被跳过，且不报错、不输出日志。
（3）每迭代n步才输出日志，但如果一个epoch的步数小于n，则总是不输出日志。
总之，要确保 batch_size 小于训练样本数 T ，log_iter 小于 T/batch_size 。

### PaddleDetection 2.0 之后的版本删除了 with_background
COCODataSet、VOCDataSet 等类型不能再写 with_background 。
