## 部分 task/job 无法打开

### 错误现象

部分 task/job 无法打开。打不开的都是 projects/8 下的任务(task)的job，例如 tasks/16/jobs/14 和 tasks/16/jobs/15 。

点击打开job时，查看 cvat 日志：


```
docker logs cvat --tail=10 -f
```

日志输出：
```
2021-12-22 03:24:17,426 DEBG 'runserver' stderr output:
[Wed Dec 22 03:24:17.425994 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250] [2021-12-22 03:24:17,425] ERROR django.request: Internal Server Error: /api/v1/jobs/14/annotations
[Wed Dec 22 03:24:17.426020 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250] Traceback (most recent call last):
[Wed Dec 22 03:24:17.426024 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]   File "/opt/venv/lib/python3.8/site-packages/django/core/handlers/exception.py", line 47, in inner
[Wed Dec 22 03:24:17.426027 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]     response = get_response(request)
[Wed Dec 22 03:24:17.426030 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]   File "/opt/venv/lib/python3.8/site-packages/django/core/handlers/base.py", line 181, in _get_response
[Wed Dec 22 03:24:17.426033 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]     response = wrapped_callback(request, *callback_args, **callback_kwargs)
[Wed Dec 22 03:24:17.426036 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]   File "/opt/venv/lib/python3.8/site-packages/django/views/decorators/csrf.py", line 54, in wrapped_view
[Wed Dec 22 03:24:17.426039 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]     return view_func(*args, **kwargs)
[Wed Dec 22 03:24:17.426041 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]   File "/opt/venv/lib/python3.8/site-packages/rest_framework/viewsets.py", line 114, in view
[Wed Dec 22 03:24:17.426044 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]     return self.dispatch(request, *args, **kwargs)
[Wed Dec 22 03:24:17.426046 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]   File "/opt/venv/lib/python3.8/site-packages/rest_framework/views.py", line 505, in dispatch
[Wed Dec 22 03:24:17.426049 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]     response = self.handle_exception(exc)
[Wed Dec 22 03:24:17.426051 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]   File "/opt/venv/lib/python3.8/site-packages/rest_framework/views.py", line 465, in handle_exception
[Wed Dec 22 03:24:17.426054 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]     self.raise_uncaught_exception(exc)
[Wed Dec 22 03:24:17.426056 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]   File "/opt/venv/lib/python3.8/site-packages/rest_framework/views.py", line 476, in raise_uncaught_exception
[Wed Dec 22 03:24:17.426058 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]     raise exc
[Wed Dec 22 03:24:17.426060 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]   File "/opt/venv/lib/python3.8/site-packages/rest_framework/views.py", line 502, in dispatch
[Wed Dec 22 03:24:17.426063 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]     response = handler(request, *args, **kwargs)
[Wed Dec 22 03:24:17.426065 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]   File "/home/django/cvat/apps/engine/views.py", line 852, in annotations
[Wed Dec 22 03:24:17.426067 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]     data = dm.task.get_job_data(pk)
[Wed Dec 22 03:24:17.426069 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]   File "/home/django/cvat/apps/profiler.py", line 11, in wrapped
[Wed Dec 22 03:24:17.426072 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]     return f(*args, **kwargs)
[Wed Dec 22 03:24:17.426074 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]   File "/usr/lib/python3.8/contextlib.py", line 75, in inner
[Wed Dec 22 03:24:17.426076 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]     return func(*args, **kwds)
[Wed Dec 22 03:24:17.426078 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]   File "/home/django/cvat/apps/dataset_manager/task.py", line 654, in get_job_data
[Wed Dec 22 03:24:17.426084 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]     annotation.init_from_db()
[Wed Dec 22 03:24:17.426086 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]   File "/home/django/cvat/apps/dataset_manager/task.py", line 532, in init_from_db
[Wed Dec 22 03:24:17.426089 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]     self._init_shapes_from_db()
[Wed Dec 22 03:24:17.426091 2021] [wsgi:error] [pid 180:tid 140325487359744] [remote 172.30.0.5:60250]   File "/home/django/cvat/apps/dataset_manager/task.py", line 442, in _init_shapes_from_db
```

正常的job案例是：projects/14 tasks/212/jobs/160 。


### 分析

需要连接到 cvat 的数据库进行分析。先把数据库映射到本机：
```
ssh -p22 -CNg -L 5432:172.30.0.3:5432 root@label-1.ai-parents.cn
```
然后用 dbeaver 连接 localhost:5432 ，数据库类型 postgresql，数据库名 cvat（不能省略），用户名 root ，密码空。

错误的案例分析：
projects/8 tasks/16/jobs/14

tasks/16/jobs/14|15 在 engine_labeledshape 中只有一种 label，即 label_id:7 。label_id:7 在 engine_label 的 task_id 为 16, name 为“人脸”。
projects/8 的 label 有 “77:人脸”和“125:头”（engine_label.id:engine_label.name）。
label_id:77|125 在 engine_labeledshape 中不存在。

正常的案例分析：
projects/14 tasks/212/jobs/160

projects/14 在 engine_label 中只有 头:140 。
jobs/160 在 engine_labeledshape 中只有 头:140 。
tasks/212 在 engine_label 中没有记录 。

另一个错误案例：
projects/12  tasks/39/jobs/41 。

jobs/41 在 engine_labeledshape 中的 label_id 只有 32 。
projects/12 在 engine_label 中只有 头:124 。
tasks/39 在 engine_label 中只有 头:32。

问题结论：

cvat 的 task 如果属于某个 project ，则自己不能有 label 的定义，label 必须在 project 层面定义，labeledshape 必须使用 project 定义的 label。否则，cvat 就找不到 task 对应的 label 。

projects/8 下的 tasks 的 engine_labeledshape.label_id 仍然是属于各个 task 的，而不是属于 projects/8 的，这不符合 cvat 的约束，因此出错。

可以这样查看：

```
select distinct el.label_id, es.task_id, et.project_id from engine_labeledshape el join engine_job ej on el.job_id = ej.id 
join engine_segment es on ej.segment_id = es.id 
join  engine_task et on es.task_id = et.id 
join engine_project ep on et.project_id = ep.id 
where et.project_id=8
```

### 解决问题

解决方式：将 projects/8 下的 tasks 的 engine_labeledshape.label_id 都设置为 125:头（projects/8 定义的标签）。

```
update engine_labeledshape 
set label_id = 125
from engine_job,engine_segment,engine_task
where 
engine_labeledshape.job_id = engine_job.id 
and
engine_job.segment_id = engine_segment.id 
and
engine_segment.task_id = engine_task.id
and
engine_task.project_id=8
```
但这时标签的属性还是关联着属于各个 task 的 label_id ，需要修正。可这样查看：

```
select engine_attributespec.* from engine_attributespec, engine_label, engine_task
where 
engine_attributespec.label_id = engine_label.id
and engine_label.task_id = engine_task.id
and engine_task.project_id = 8

select engine_labeledshapeattributeval.*, engine_attributespec.*
from engine_labeledshapeattributeval, engine_attributespec, engine_label, engine_task
where
engine_labeledshapeattributeval.spec_id = engine_attributespec.id
and engine_attributespec.label_id = engine_label.id
and engine_label.task_id = engine_task.id
and engine_task.project_id = 8
and engine_attributespec.name = 'blur'
```
可以看到 engine_attributespec.label_id 的值有多种，不全是 125,需要修正为125。engine_labeledshapeattributeval 也需要修正 spec_id 。

先在 engine_attributespec 中新增两行（dbeaver 中复制），label_id 设置为 125 。

```
id	label_id	default_value	input_type	mutable	name	values
101	125	0	number	true	blur	"0 1 0.5"
102	125	0	number	true	face_occlusion	"0 1 0.25"
```
然后更新 engine_labeledshapeattributeval ，让 projects/8 下的 engine_labeledshapeattributeval.spec_id 指向 101 或 102。

```
update engine_labeledshapeattributeval
set spec_id = 101
from engine_attributespec, engine_label, engine_task
where
engine_labeledshapeattributeval.spec_id = engine_attributespec.id
and engine_attributespec.label_id = engine_label.id
and engine_label.task_id = engine_task.id
and engine_task.project_id = 8
and engine_attributespec.name = 'blur'

update engine_labeledshapeattributeval
set spec_id = 102
from engine_attributespec, engine_label, engine_task
where
engine_labeledshapeattributeval.spec_id = engine_attributespec.id
and engine_attributespec.label_id = engine_label.id
and engine_label.task_id = engine_task.id
and engine_task.project_id = 8
and engine_attributespec.name = 'face_occlusion'
```

确认 engine_labeledshapeattributeval 的修复结果：
```
select engine_labeledshapeattributeval.*, engine_attributespec.*
from engine_labeledshapeattributeval, engine_attributespec, engine_label, engine_task
where
engine_labeledshapeattributeval.spec_id = engine_attributespec.id
and engine_attributespec.label_id = engine_label.id
and engine_label.project_id = 8
```
至此，修复完毕。

## 导入zip工具重新压缩的备份task出错

解决：

```
docker exec -it cvat bash
cp /home/django/src/cvat/apps/engine/backup.1.py /home/django/cvat/apps/engine/backup.py
```
supervisorctl 进入提示符，再执行命令 restart all ，重启 supervisor 管理的全部服务。


cvat          | 2021-12-08 11:59:35,080 DEBG 'rqworker_default_0' stderr output:
cvat          | INFO - 2021-12-08 11:59:35,080 - worker - default: cvat.apps.engine.backup.import_task('/tmp/cvat_ygmy_ru5', 1) (duanyao@/api/v1/tasks/38087c6f-6714-47aa-8248-77f7a8dd37fc/import)
cvat          | 
cvat          | 2021-12-08 11:59:35,082 DEBG 'rqworker_default_0' stderr output:
cvat          | DEBUG - 2021-12-08 11:59:35,082 - worker - Sent heartbeat to prevent worker timeout. Next one should arrive within 480 seconds.
cvat          | 
cvat          | 2021-12-08 11:59:35,102 DEBG 'rqworker_default_0' stderr output:
cvat          | DEBUG - 2021-12-08 11:59:35,101 - worker - Sent heartbeat to prevent worker timeout. Next one should arrive within 90 seconds.
cvat          | 2021-12-08 11:59:35,169 DEBG 'rqworker_default_0' stderr output:
cvat          | [2021-12-08 11:59:35,169] WARNING cvat.server: >>>backup.py:_import_task:self._filename=/tmp/cvat_ygmy_ru5
cvat          | 
cvat          | 2021-12-08 11:59:35,170 DEBG 'rqworker_default_0' stderr output:
cvat          | WARNING - 2021-12-08 11:59:35,169 - backup - >>>backup.py:_import_task:self._filename=/tmp/cvat_ygmy_ru5
cvat          | 
cvat          | 2021-12-08 11:59:35,171 DEBG 'rqworker_default_0' stderr output:
cvat          | [2021-12-08 11:59:35,170] WARNING cvat.server: >>>backup.py:_import_task:data_path=/home/django/data/data/13/raw, f=data/, self.DATA_DIRNAME=data
cvat          | 
cvat          | 2021-12-08 11:59:35,171 DEBG 'rqworker_default_0' stderr output:
cvat          | WARNING - 2021-12-08 11:59:35,170 - backup - >>>backup.py:_import_task:data_path=/home/django/data/data/13/raw, f=data/, self.DATA_DIRNAME=data
cvat          | 
cvat          | 2021-12-08 11:59:35,179 DEBG 'rqworker_default_0' stderr output:
cvat          | DEBUG - 2021-12-08 11:59:35,179 - worker - Handling failed execution of job duanyao@/api/v1/tasks/38087c6f-6714-47aa-8248-77f7a8dd37fc/import
cvat          | 
cvat          | 2021-12-08 11:59:35,182 DEBG 'rqworker_default_0' stderr output:
cvat          | ERROR - 2021-12-08 11:59:35,181 - worker - Traceback (most recent call last):
cvat          |   File "/opt/venv/lib/python3.8/site-packages/rq/worker.py", line 936, in perform_job
cvat          |     rv = job.perform()
cvat          |   File "/opt/venv/lib/python3.8/site-packages/rq/job.py", line 684, in perform
cvat          |     self._result = self._execute()
cvat          |   File "/opt/venv/lib/python3.8/site-packages/rq/job.py", line 690, in _execute
cvat          |     return self.func(*self.args, **self.kwargs)
cvat          |   File "/usr/lib/python3.8/contextlib.py", line 75, in inner
cvat          |     return func(*args, **kwds)
cvat          |   File "/home/django/cvat/apps/engine/backup.py", line 552, in import_task
cvat          |     db_task = task_importer.import_task()
cvat          |   File "/home/django/cvat/apps/engine/backup.py", line 544, in import_task
cvat          |     self._import_task()
cvat          |   File "/home/django/cvat/apps/engine/backup.py", line 512, in _import_task
cvat          |     with open(target_file, "wb") as out:
cvat          | IsADirectoryError: [Errno 21] Is a directory: '/home/django/data/data/13/raw/.'
cvat          | Traceback (most recent call last):
cvat          |   File "/opt/venv/lib/python3.8/site-packages/rq/worker.py", line 936, in perform_job
cvat          |     rv = job.perform()
cvat          |   File "/opt/venv/lib/python3.8/site-packages/rq/job.py", line 684, in perform
cvat          |     self._result = self._execute()
cvat          |   File "/opt/venv/lib/python3.8/site-packages/rq/job.py", line 690, in _execute
cvat          |     return self.func(*self.args, **self.kwargs)
cvat          |   File "/usr/lib/python3.8/contextlib.py", line 75, in inner
cvat          |     return func(*args, **kwds)
cvat          |   File "/home/django/cvat/apps/engine/backup.py", line 552, in import_task
cvat          |     db_task = task_importer.import_task()
cvat          |   File "/home/django/cvat/apps/engine/backup.py", line 544, in import_task
cvat          |     self._import_task()
cvat          |   File "/home/django/cvat/apps/engine/backup.py", line 512, in _import_task
cvat          |     with open(target_file, "wb") as out:
cvat          | IsADirectoryError: [Errno 21] Is a directory: '/home/django/data/data/13/raw/.'
cvat          | 
cvat          | 2021-12-08 11:59:35,182 DEBG 'rqworker_default_0' stderr output:
cvat          | DEBUG - 2021-12-08 11:59:35,182 - worker - Invoking exception handler <function rq_handler at 0x7fde291e3ee0>
