## 参考资料
[3.1] PyTorch Image Models
https://github.com/huggingface/pytorch-image-models?tab=readme-ov-file#models

[4.1] 
https://github.com/Alibaba-MIIL/ImageNet21K

[5.1] ML-Decoder: Scalable and Versatile Classification Head, 2021
https://arxiv.org/abs/2111.12933
https://github.com/Alibaba-MIIL/ML_Decoder

[6.1] Open Vocabulary Multi-Label Classification with Dual-Modal Decoder on Aligned Visual-Textual Features 2022,2023
https://arxiv.org/abs/2208.09562
源码尚未放出（2024.6）
