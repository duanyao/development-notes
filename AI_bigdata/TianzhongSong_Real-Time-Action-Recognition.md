## 参考资料
[2.1] https://stackoverflow.com/questions/26559179/cap-prop-frame-count-constant-is-missing-in-opencv-cv2-python-module

## 安装

```
sudo apt install python-pyqt5 # 这个包是 python2 的，python3 可以用此命令安装 pip3 install PyQt5

sudo apt install nvidia-cuda-dev nvidia-cuda-toolkit # deepin 2019.5 安装了 cuda 9.1

sudo pip install opencv-python

sudo pip install tensorflow-gpu==1.3.0

sudo pip install filterpy sklearn
```


sudo pip install tensorflow-gpu==1.3.0 改为 1.5.0

ImportError: libcublas.so.9.0: cannot open shared object file: No such file or directory
ImportError: libcusolver.so.9.0: cannot open shared object file: No such file or directory

ImportError: libcudnn.so.7: cannot open shared object file: No such file or directory

dpkg -S libcublas.so.9
libcublas9.1:amd64: /usr/lib/x86_64-linux-gnu/libcublas.so.9.1.85
libcublas9.1:amd64: /usr/lib/x86_64-linux-gnu/libcublas.so.9.1

dpkg -S libcusolver.so.9
libcusolver9.1:amd64: /usr/lib/x86_64-linux-gnu/libcusolver.so.9.1
libcusolver9.1:amd64: /usr/lib/x86_64-linux-gnu/libcusolver.so.9.1.85

sudo ln -s /usr/lib/x86_64-linux-gnu/libcublas.so.9.1 /usr/lib/x86_64-linux-gnu/libcublas.so.9.0
sudo ln -s /usr/lib/x86_64-linux-gnu/libcusolver.so.9.1 /usr/lib/x86_64-linux-gnu/libcusolver.so.9.0
sudo ln -s /usr/lib/x86_64-linux-gnu/libcudart.so.9.1 /usr/lib/x86_64-linux-gnu/libcudart.so.9.0

没有任何 pip 版 tensorflow-gpu 与 cuda 9.1 兼容，所以从源码编译安装 tensorflow-gpu 1.7.1（1.5是源码兼容 cuda 9.1的最低版本，但1.7更成熟一些）。

运行 python run.py ，错误：

```
ImportError: No module named sklearn.utils.linear_assignment_
```
需要 pip 安装 sklearn 。

继续运行 run.py，点击“姿态估计”，错误：

```
Traceback (most recent call last):
  File "run.py", line 137, in button_event
    self.cap.set(cv2.CAP_PROP_FRAME_WIDTH, settings.winWidth)
AttributeError: 'module' object has no attribute 'CAP_PROP_FRAME_WIDTH'
```
相关代码：

```
import cv2  #L9
self.cap.set(cv2.CAP_PROP_FRAME_WIDTH, settings.winWidth) #L137
```

查看 cv2 模块的依赖：

```
>>> import cv2
>>> cv2
<module 'cv2' from '/usr/local/lib/python2.7/dist-packages/cv2.so'>
```

可能的原因是：opencv-python 依赖系统中安装的 opencv，但是它并不在意其版本是 2.x 还是 3.x，而 opencv 2.x 和 3.x 的有些变量名是不同的。
本机安装的是 opencv 2.4。
本程序使用 3.x 的变量名 cv2.CAP_PROP_FRAME_WIDTH，对应的 2.4 变量名是 cv2.cv.CV_CAP_PROP_FRAME_WIDTH [2.1]。
但 2.4v5 似乎改为与 3.x 相同了？

运行姿态估计，警告：

```
2019-06-06 09:22:32.324376: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1394] Ignoring visible gpu device (device: 0, name: GeForce 940MX, pci bus id: 0000:01:00.0, compute capability: 5.0) with Cuda compute capability 5.0. The minimum required Cuda capability is 5.2.
```

所以GPU没有启用，是使用CPU识别的，性能只有3fps左右。
