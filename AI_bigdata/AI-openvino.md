## 参考资料

[1.0] 下载 openvino-toolkit
  https://software.intel.com/en-us/openvino-toolkit

[1.1] 安装 openvino-toolkit
  https://docs.openvinotoolkit.org/latest/_docs_install_guides_installing_openvino_linux.html
  https://docs.openvinotoolkit.org/2020.1/_docs_install_guides_installing_openvino_linux.html#install-external-dependencies

[1.2] intel compute-runtime 
  https://github.com/intel/compute-runtime/releases

[1.3] downloader https://github.com/opencv/open_model_zoo/blob/efd238d02035f8a5417b7b1e25cd4c997d44351f/tools/model_tools//README.md

[2.1] OpenVINO™ Toolkit - Deep Learning Deployment Toolkit repository
  https://github.com/openvinotoolkit/openvino

[2.2] CPU Requirements for OpenVINO CPU inference
  https://community.intel.com/t5/Intel-Distribution-of-OpenVINO/CPU-Requirements-for-OpenVINO-CPU-inference/td-p/1149229

[2.3] Build on Linux* Systems
  https://github.com/openvinotoolkit/openvino/wiki/BuildingForLinux
  
[3.1] Intel at the Edge (Leveraging Pre-Trained Models)
  https://krbnite.github.io/Intel-at-the-Edge-Leveraging-Pre-Trained-Models/

[4.1] Error in converting custom ssd model using Tensorflow2 Object detection API
  https://community.intel.com/t5/Intel-Distribution-of-OpenVINO/Error-in-converting-custom-ssd-model-using-Tensorflow2-Object/m-p/1281338/highlight/true
  
[5.1] INT8 vs FP32 Comparison on Select Networks and Platforms
  https://docs.openvinotoolkit.org/latest/openvino_docs_performance_int8_vs_fp32.html

[10.1] openvino / open_model_zoo 的训练代码库 PyTorch
    https://github.com/opencv/openvino_training_extensions/tree/develop/pytorch_toolkit
  
[10.2] Real-time 2D Multi-Person Pose Estimation on CPU: Lightweight OpenPose - training - Intel 2018
  https://github.com/opencv/openvino_training_extensions/tree/develop/pytorch_toolkit/human_pose_estimation


## 运行环境和依赖

openvino_2021.4.582 的 python API 提供支持 python 3.6-3.9 的版本。


## 安装 openvino-toolkit （centos + yum）

https://docs.openvinotoolkit.org/latest/openvino_docs_install_guides_installing_openvino_yum.html

```
yum install yum-utils
yum-config-manager --add-repo https://yum.repos.intel.com/openvino/2021/setup/intel-openvino-2021.repo
rpm --import https://yum.repos.intel.com/openvino/2021/setup/RPM-GPG-KEY-INTEL-OPENVINO-2021
yum repolist | grep -i openvino
yum list intel-openvino*
yum list intel-openvino-runtime*
yum install intel-openvino-runtime-centos7-2021.4.582 # yum install intel-openvino-runtime-centos7 #也可以，但不一定安装最新版。
```
安装位置是 /opt/intel/

```
ls -l /opt/intel/
total 4
lrwxrwxrwx 1 root root  19 Aug  3 11:44 openvino_2021 -> openvino_2021.4.582
drwxr-xr-x 1 root root 208 Aug  3 11:44 openvino_2021.4.582
```

建议创建符号链接 openvino：

```
cd /opt/intel/
ln -sf openvino_2021 openvino
```

python3 最好有，但不是硬性要求：
```
yum install python3
```

## 安装 openvino-toolkit (ubuntu,debian + apt)

```
wget https://apt.repos.intel.com/openvino/2021/GPG-PUB-KEY-INTEL-OPENVINO-2021 
sudo apt-key add GPG-PUB-KEY-INTEL-OPENVINO-2021
sudo apt-key list
echo "deb https://apt.repos.intel.com/openvino/2021 all main" | sudo tee /etc/apt/sources.list.d/intel-openvino-2021.list
sudo apt-key adv --recv-keys --keyserver keyserver.ubuntu.com ACFA9FC57E6C5DBE
sudo apt update
apt search openvino
apt search -n openvino runtime
apt search -n openvino samples
apt search -n openvino optim
apt search -n openvino py
apt search intel-openvino-dev-ubuntu18

sudo apt install intel-openvino-runtime-ubuntu18-2021.4.582  # 运行库，必选。也有 ubuntu20 版可用。
sudo apt install intel-openvino-ie-samples-2021.4.582        # 代码样例，可选。如果打算使用 open_model_zoo 的样例代码，可以不装。
sudo apt install intel-openvino-model-optimizer-2021.4.582   # 模型优化器，用来转换模型格式，可选。开发环境建议安装，生产环境不需要。
sudo apt install intel-openvino-ie-bin-python-tools-ubuntu-focal-2021.4.582 # 提供 python 包 openvino.tools.benchmark 等。

# 旧版本
sudo apt install intel-openvino-runtime-ubuntu18-2021.3.394
sudo apt install intel-openvino-model-optimizer-2021.3.394

cd /opt/intel/
sudo ln -sf openvino_2021 openvino
```


## 环境设置/激活 openvino 环境

运行

```
source /opt/intel/openvino/bin/setupvars.sh
```

将会设置若干环境变量，例如：

```
LD_LIBRARY_PATH=/opt/intel/openvino/data_processing/dl_streamer/lib:/opt/intel/openvino/data_processing/gstreamer/lib:/opt/intel/openvino/opencv/lib:/opt/intel/openvino/deployment_tools/ngraph/lib:/opt/intel/openvino/deployment_tools/inference_engine/external/tbb/lib::/opt/intel/openvino/deployment_tools/inference_engine/external/hddl/lib:/opt/intel/openvino/deployment_tools/inference_engine/external/omp/lib:/opt/intel/openvino/deployment_tools/inference_engine/external/gna/lib:/opt/intel/openvino/deployment_tools/inference_engine/external/mkltiny_lnx/lib:/opt/intel/openvino/deployment_tools/inference_engine/lib/intel64
GST_PLUGIN_PATH=/opt/intel/openvino/data_processing/dl_streamer/lib:/opt/intel/openvino/data_processing/gstreamer/lib/gstreamer-1.0
INTEL_CVSDK_DIR=/opt/intel/openvino
MODELS_PATH=/root/intel/dl_streamer/models
OpenCV_DIR=/opt/intel/openvino/opencv/cmake
TBB_DIR=/opt/intel/openvino/deployment_tools/inference_engine/external/tbb/cmake
InferenceEngine_DIR=/opt/intel/openvino/deployment_tools/inference_engine/share
GI_TYPELIB_PATH=/opt/intel/openvino/data_processing/gstreamer/lib/girepository-1.0
LIBRARY_PATH=/opt/intel/openvino/data_processing/dl_streamer/lib:/opt/intel/openvino/data_processing/gstreamer/lib:
LC_NUMERIC=C
ngraph_DIR=/opt/intel/openvino/deployment_tools/ngraph/cmake
TERM=xterm
GST_SAMPLES_DIR=/opt/intel/openvino/data_processing/dl_streamer/samples
PYTHONPATH=/opt/intel/openvino/python/python3.6:/opt/intel/openvino/python/python3:/opt/intel/openvino/deployment_tools/model_optimizer:/opt/intel/openvino/data_processing/dl_streamer/python:/opt/intel/openvino/data_processing/gstreamer/lib/python3.8/site-packages:
GST_PLUGIN_SCANNER=/opt/intel/openvino/data_processing/gstreamer/bin/gstreamer-1.0/gst-plugin-scanner
INTEL_OPENVINO_DIR=/opt/intel/openvino
PATH=/opt/intel/openvino/deployment_tools/model_optimizer:/opt/intel/openvino/data_processing/gstreamer/bin:/opt/intel/openvino/data_processing/gstreamer/bin/gstreamer-1.0:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PKG_CONFIG_PATH=/opt/intel/openvino/data_processing/dl_streamer/lib/pkgconfig:/opt/intel/openvino/data_processing/gstreamer/lib/pkgconfig:
HDDL_INSTALL_DIR=/opt/intel/openvino/deployment_tools/inference_engine/external/hddl
```

当运行链接到 openvino 的程序时，应当至少设置 LD_LIBRARY_PATH 。
要编译依赖 opencv 的程序时，应设置 OpenCV_DIR 。

## 配置和使用 openvino 的 python API

如果使用了 python venv ，一般应该先激活 venv ，再激活 openvino ，这样 openvino 的脚本会根据检测到的 python 版本设置环境变量 PYTHONPATH（参考“激活 openvino 环境”），以便加载匹配 python 版本的 `openvino.*` 包。

```
source ~/opt/venv/v37/bin/activate
source /opt/intel/openvino/bin/setupvars.sh
```
其它的 python 环境可参照处理。否则，openvino 根据系统默认的 python 版本设置 PYTHONPATH 。

openvino 的 python API 在 python 中主要是 `openvino.*` 包。可以在 python 交互环境中这样验证它是否正常：

```
>>> import openvino
>>> openvino.inference_engine
<module 'openvino.inference_engine' from '/opt/intel/openvino/python/python3.7/openvino/inference_engine/__init__.py'>
>>> openvino.inference_engine.ie_api
<module 'openvino.inference_engine.ie_api' from '/opt/intel/openvino/python/python3.7/openvino/inference_engine/ie_api.so'>
```
可见 python openvino 包是在 openvino 自带的，在其安装目录下的 `python/python3.*` 子目录下，每个 python3.* 子目录支持一个 python 版本。
openvino 支持的 python 版本是有限的，例如，openvino_2021.3.394 支持 python3.6, python3.7 。
如果当前的 python 版本不在支持范围内，则 setupvars.sh 会报告错误，并且不会正确设置 PYTHONPATH ，导致 python openvino 包不可用。
如果先在 openvino 支持的 python 环境下（如3.7）激活 openvino ，再切换到不被 openvino 支持的 python 环境下(如3.9)，或者手动设置 PYTHONPATH，则 python openvino.inference_engine.ie_api 包或许可用，但运行时可能输出警告，但尚不清楚实际的后果。如：

```
<frozen importlib._bootstrap>:228: RuntimeWarning: compiletime version 3.7 of module 'openvino.inference_engine.ie_api' does not match runtime version 3.9
```

不过，openvino 的 model_optimizer 并不强依赖具体的 python 版本，高于 openvino 支持的 python 版本也可以用。

## pip openvino 包

注意，pip 仓库中也有个 openvino 包，这个包含有 openvino 的 python API 和 inference_engine 的本地代码，可以单独使用，无需安装 openvinotoolkit 。

```
pip install openvino # 实际版本可能是 openvino-2021.4.0 或 openvino-2021.3.0
```

使用 pip openvino 包时，一定不要再激活其它 openvino 环境，否则会有冲突。

pip openvino 包没有 C++ 头文件和 cmake 文件，因此不能用来开发 C++ 代码。
pip openvino 包也不自带 opencv ，你可以单独安装 opencv-python 包。
目前，pip openvino 最高版本为 2021.4.0 ，支持 python 3.9 。pip openvino 支持的 python 版本范围可以在 https://pypi.org/project/openvino/ 查看。

pip openvino 不使用 C++11 ABI 。

## 数据宽度和性能

openvino 在 Intel CPU 上可以使用几种不同的数据宽度来做推理，如 FP32 (单精度浮点)、FP16（半精度浮点）、FP16-INT8（半精度浮点和8位整数）。
数据宽度变小后，运算速度会有一定的提升，但准确度可能轻微下降。实际效果与CPU型号和模型都有关系，一些实测结果见[5.1]。FP16-INT8 速度方面最多可提高到 3-4 倍，1-2倍之间也常见。
由于 Intel CPU 大多不具备 FP16 计算功能，所以速度提升可能不明显，但优势在于节约内存带宽。

## openvino 样例代码

位于
`/opt/intel/openvino_xxx/deployment_tools/inference_engine/samples/`

## accuracy_checker(open_model_zoo)

accuracy_checker 是个 python 版命令行工具。源码在 openvino_model_zoo 仓库中。

安装（在 venv 环境中）：

```
cd openvino_model_zoo.git/tools/accuracy_checker/
python setup.py install_core
```
这也会在 venv 的bin目录下安装 accuracy_check 和 convert_annotation 等命令行工具。

使用的例子：

```
cd ~/project/openvino_model_zoo.git/
accuracy_check -td CPU -c tools/accuracy_checker/configs/person-detection-retail-0013.yml -s ~/project/ai-dataset/COCO/ -m intel/person-detection-retail-0013/FP16-INT8/ -d tools/accuracy_checker/dataset_definitions.yml

accuracy_check -td CPU -c tools/accuracy_checker/configs/person-detection-retail-0013.yml  -m intel/person-detection-retail-0013/FP32/ -d ~/'project/annotation/openvino_accuracy_checker/cvat-coco-person-detection.yml' -s ~/'project/annotation/task_人体检测聊城江北水城幼儿园 托二班 1572483602284.576p.r_5.mp4-2021_10_18_11_06_43-coco 1.0'
```

其中 `-s ~/project/ai-dataset/COCO/` 指示 COCO 数据集所在的目录，数据集可以从网上下载，其布局有一定的要求，见后面。
输出结果形如：

```
5000 / 5000 processed in 0.059s
5000 objects processed in 304.302 seconds
map: 47.62%
```

`map` 是指 mean average precision ，一种用于目标检测的指标。也有其它指标可选，见 accuracy_checker/metrics/README.md 。
目前 `miss_rate` 似乎有bug，会崩溃 `IndexError: index 0 is out of bounds for axis 0 with size 0`

要调试 accuracy_checker ，可以创建 `openvino_model_zoo.git/tools/accuracy_checker/ac_main.py` 文件：

```
from accuracy_checker.main import main

if __name__ == '__main__':
    main()
```

效果与 accuracy_check 命令行工具相同。

```
tools/accuracy_checker/configs/person-detection-retail-0013.yml
models/intel/person-detection-retail-0013/accuracy-check.yml    # 与上面的相同

models:
  - name: person-detection-retail-0013

    launchers:
      - framework: dlsdk
        adapter: ssd

    datasets:
      - name: person_detection
```

data/datasets.md

下载数据集：
```
wget https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz
wget http://images.cocodataset.org/zips/val2017.zip
wget http://images.cocodataset.org/annotations/annotations_trainval2017.zip
```
数据集目录布局：

* COCO
    * `val2017` - directory containing the COCO 2017 validation images
    * `instances_val2017.json` - annotation file which used for object detection and instance segmentation tasks
    * `person_keypoints_val2017.json` - annotation file which used for human pose estimation tasks

数据集的定义文件为：

```
tools/accuracy_checker/dataset_definitions.yml

  - name: person_detection
    data_source: val2017
    annotation_conversion:
      converter: mscoco_detection
      annotation_file: person_keypoints_val2017.json
      has_background: True
      sort_annotations: True
      use_full_label_map: True
    annotation: mscoco_person_detection.pickle
    dataset_meta: mscoco_person_detection.json

  - name: mscoco_person_detection
    data_source: val2017
    annotation_conversion:
      converter: mscoco_detection
      annotation_file: person_keypoints_val2017.json
      has_background: True
      sort_annotations: True
      use_full_label_map: True
    annotation: mscoco_person_detection.pickle
    dataset_meta: mscoco_person_detection.json
```

其中，`name: person_detection` 要与 tools/accuracy_checker/configs/person-detection-retail-0013.yml 中的 `datasets:- name: person_detection` 匹配；`data_source: val2017` 是图像文件的目录，相对于数据集目录；`annotation_file: person_keypoints_val2017.json` 是标注文件的路径，也相对于数据集目录。`annotation: mscoco_person_detection.pickle` 和 `dataset_meta: mscoco_person_detection.json` 是标注转换器生成的临时文件的名字，可以随便起名。

如果有自定义的数据集，可以参照上面的自己编写数据集的定义文件。注意，自定义数据集的标签的id要与参照的数据集的标签的id相同，否则测试的结果不对。例如，COCO 数据集中 person_keypoints_val2017.json 中定义的 categories（即标签）的 person 的 id 是 1，那么自定义数据集中 categories 的 person 的 id 也必须是 1 ，才能参照 COCO 数据集。

```
accuracy_checker/annotation_converters/README.md

cifar
mnist
imagenet image classification
voc_detection - converts Pascal VOC annotation for detection task
voc_segmentation - converts Pascal VOC annotation for semantic segmentation
mscoco_detection - converts MS COCO dataset for object detection
mscoco_segmentation - converts MS COCO dataset for object instance segmentation task
mscoco_keypoints - converts MS COCO dataset for keypoints localization
vgg_face - converts VGG Face 2 dataset for facial landmarks regression
```

accuracy_check -td CPU -c tools/accuracy_checker/configs/person-detection-retail-0013.yml -m intel/person-detection-retail-0013/FP16-INT8/ -d ~/'project/annotation/openvino_accuracy_checker/cvat-coco-person-detection.yml' -s ~/'project/annotation/task_人体检测 广东湛江市小红帽幼儿园 大一班 1624496810975.576p.rk.mp4-2021_10_11_09_37_11-coco 1.0' --stored_predictions pd1.pickle

accuracy_check -td CPU -c tools/accuracy_checker/configs/person-detection-retail-0013.yml -m intel/person-detection-retail-0013/FP32/ -d ~/'project/annotation/openvino_accuracy_checker/cvat-coco-person-detection.yml' -s ~/'project/annotation/task_人体检测 广东湛江市小红帽幼儿园 大一班 1624496810975.576p.rk.mp4-2021_10_11_09_37_11-coco 1.0' --stored_predictions pd2.pickle


cd "/home/duanyao/project/annotation/task_人体检测 广东湛江市小红帽幼儿园 大一班 1624496810975.576p.rk.mp4-2021_10_11_09_37_11-coco 1.0/"
mkdir -p det1 det2 det3 met1 met2 met3

python /home/duanyao/project/ai-parents-app.git/tools-py/convert_detection_result.py -i /home/duanyao/project/openvino_model_zoo.git/pd1.pickle -o det1/det1.json
python /home/duanyao/project/ai-parents-app.git/tools-py/convert_detection_result.py -i /home/duanyao/project/openvino_model_zoo.git/pd2.pickle -o det2/det2.json
python /home/duanyao/project/ai-parents-app.git/tools-py/convert_detection_result.py -i /home/duanyao/project/openvino_model_zoo.git/pd3.pickle -o det3/det3.json

'--stored_predictions',
'--store_only'
_stored_data

launcher = DummyLauncher({
    'framework': 'dummy',
    'loader': 'pickle',
    'data_path': stored_predictions,
}, adapter=self.adapter, identifiers=identifiers, progress=progress_reporter)

def store_predictions(stored_predictions, predictions):

process_dataset_sync|process_dataset_async
    prepare_prediction_to_store
        store_predictions

async_mode

<class 'accuracy_checker.launcher.loaders.loader.StoredPredictionBatch'>

'count', 'identifiers', 'index', 'meta', 'raw_predictions'

{
  raw_predictions: [ { 'detection_out': numpy.ndarray } ], 
  identifiers: ['frame_000000.PNG'],
  meta: [{ 'image_size': (576, 1024, 3), ... }],
}

detection_out.shape == (1,1,N,7)

The net outputs blob with shape: `1, 1, 200, 7` in the format `1, 1, N, 7`, where `N` is the number of detected
bounding boxes. Each detection has the format [`image_id`, `label`, `conf`, `x_min`, `y_min`, `x_max`, `y_max`], where:

- `image_id` - ID of the image in the batch
- `label` - predicted class ID (1 - person)
- `conf` - confidence for the predicted class
- (`x_min`, `y_min`) - coordinates of the top left bounding box corner
- (`x_max`, `y_max`) - coordinates of the bottom right bounding box corner

~/project/annotation/task_人体检测 广东湛江市小红帽幼儿园 大一班 1624496810975.576p.rk.mp4-2021_10_11_09_37_11-coco 1.0/annotations
~/project/annotation/task_人体检测 广东湛江市小红帽幼儿园 大一班 1624496810975.576p.rk.mp4-2021_10_11_09_37_11-coco 1.0/images
~/project/annotation/task_人体检测 广东湛江市小红帽幼儿园 大一班 1624496810975.576p.rk.mp4-2021_10_11_09_37_11-coco 1.0/annot_det1

## open_model_zoo

### 下载和安装代码

```
git clone https://github.com/openvinotoolkit/open_model_zoo.git openvino_model_zoo.git
cd openvino_model_zoo.git
```

（可选）用 git 检出与 openvino-toolkit 版本匹配的 tag ，如 2019R3。

安装 downloader 必要的 python 依赖（最好在 venv，python 版本）

```
pip install -r tools/model_tools//requirements.in
```
安装模型转换器的依赖，通常需要特定版本的框架，请事先检查：
```
pip install -r tools/model_tools//requirements-caffe2.in
pip install -r tools/model_tools//requirements-pytorch.in
pip install -r tools/model_tools//requirements-tensorflow.in
```

### 模型的文档

模型的列表：`models/intel/index.md` 和 `models/public/index.md`。

每个模型的文档，在 `models/{intel|public}/<model_name>` 下面，例如：`models/intel/human-pose-estimation-0001/description/human-pose-estimation-0001.md` 。

一些有用的模型：

https://github.com/openvinotoolkit/open_model_zoo/blob/master/models/intel/person-detection-retail-0013/README.md
https://github.com/openvinotoolkit/open_model_zoo/blob/master/models/intel/person-detection-0200/README.md
https://github.com/openvinotoolkit/open_model_zoo/blob/master/models/public/facenet-20180408-102900/README.md
https://github.com/openvinotoolkit/open_model_zoo/blob/master/models/public/face-recognition-resnet100-arcface-onnx/README.md
https://github.com/openvinotoolkit/open_model_zoo/blob/master/models/public/Sphereface/README.md

https://github.com/openvinotoolkit/open_model_zoo/blob/master/models/public/human-pose-estimation-3d-0001/README.md
https://github.com/openvinotoolkit/open_model_zoo/blob/master/models/public/higher-hrnet-w32-human-pose-estimation/README.md
https://github.com/openvinotoolkit/open_model_zoo/blob/master/models/public/single-human-pose-estimation-0001/README.md

https://github.com/openvinotoolkit/open_model_zoo/blob/master/models/intel/head-pose-estimation-adas-0001/README.md
https://github.com/openvinotoolkit/open_model_zoo/blob/master/models/intel/emotions-recognition-retail-0003/README.md

https://github.com/openvinotoolkit/open_model_zoo/blob/master/models/intel/facial-landmarks-35-adas-0002/README.md
https://github.com/openvinotoolkit/open_model_zoo/blob/master/models/intel/landmarks-regression-retail-0009/README.md

https://github.com/openvinotoolkit/open_model_zoo/blob/master/models/intel/person-attributes-recognition-crossroad-0230/README.md

https://github.com/openvinotoolkit/open_model_zoo/blob/master/models/intel/face-reidentification-retail-0095/README.md

https://github.com/openvinotoolkit/open_model_zoo/blob/master/models/intel/person-reidentification-retail-0288/README.md

### 模型下载和转换

模型配置在形如 models/public/faster_rcnn_inception_v2_coco/model.yml 的文件中，定义了下载地址、转换模型格式的参数。

下载模型：

```
python tools/model_tools//downloader.py --print_all
python tools/model_tools//downloader.py --list demos/human_pose_estimation_demo/python/models.lst  # .lst 文件可指定多个模型
python tools/model_tools//downloader.py --name  faster_rcnn_inception_v2_coco
python tools/model_tools//downloader.py --name  mask_rcnn_inception_resnet_v2_atrous_coco
python tools/model_tools//downloader.py --name  person-detection-action-recognition-0006
```

下载后的位置在 intel/ 和 public/ 目录下。例如 `faster_rcnn_inception_v2_coco` 下载到了 `public/faster_rcnn_inception_v2_coco/faster_rcnn_inception_v2_coco_2018_01_28` ，这是个 tensorflow 模型。

转换模型格式（在 venv 中进行）：

```
source /opt/intel/openvino/bin/setupvars.sh
python tools/model_tools//converter.py --list demos/human_pose_estimation_demo/python/models.lst  # .lst 文件可指定多个模型
python tools/model_tools//converter.py --name faster_rcnn_inception_v2_coco
python tools/model_tools//converter.py --name mask_rcnn_inception_resnet_v2_atrous_coco
```
转换后的模型位于：

```
public/faster_rcnn_inception_v2_coco/FP32/faster_rcnn_inception_v2_coco.{xml|bin}
```

如果提示有缺少的 python 模块，检查 `/opt/intel/openvino/deployment_tools/model_optimizer/requirements_xxx.txt` 是否安装了。

### 演示程序

#### 编译演示程序

这个步骤编译 C++ 演示程序和 python 演示程序的本地模块。

进入 venv 和 openvno 环境。

```
pip install -r openvino_model_zoo.git/demos/requirements.txt
cd demos/
mkdir build
cd build
cmake -DENABLE_PYTHON=ON .. # 不包含 -DENABLE_PYTHON=ON 则不编译 python 演示程序的本地模块。 
cmake --build .
```

似乎无法进入个别 demo 目录编译，因为其 CMakeList 文件可能不完整。

编译出来的二进制文件在 demos/build/intel64/Release/ ；python 本地库在 demos/build/intel64/Release/lib/ 。

#### （过时）编译演示程序

源码在 https://github.com/openvinotoolkit/openvino/tree/master/inference-engine/samples

演示程序有多个，源码和文档在 demos/xxx/ 下。演示程序的命令行参数、所需的模型详见 readme.md 和 models.lst 文件。

编译演示程序：
```
cd demos/
source /opt/intel/openvino/bin/setupvars.sh
./build_demos.sh
```

注意 openvino 的版本号要比 open_model_zoo 要求的新，才能编译成功。open_model_zoo 要求的 openvino 版本号是其 git tag 。

产物在 ~/omz_demos_build/intel64/Release/ ，

#### 行为识别
```
source /opt/intel/openvino/bin/setupvars.sh

~/omz_demos_build/intel64/Release/smart_classroom_demo -m_act ~/t-project/open_model_zoo.git/intel/person-detection-action-recognition-0006/FP32/person-detection-action-recognition-0006.xml \
                       -m_fd ~/t-project/open_model_zoo.git/intel/face-detection-adas-0001/FP32/face-detection-adas-0001.xml \
                       -m_reid ~/t-project/open_model_zoo.git/intel/face-reidentification-retail-0095/FP32/face-reidentification-retail-0095.xml \
                       -m_lm ~/t-project/open_model_zoo.git/intel/landmarks-regression-retail-0009/FP32/landmarks-regression-retail-0009.xml \
                       -i ~/t-project/open_model_zoo.git/1561101023349-576-9s.mp4 \
                       -out_v ~/t-project/open_model_zoo.git/1561101023349-576-9s.out.avi
                    
~/omz_demos_build/intel64/Release/smart_classroom_demo -m_act ~/t-project/open_model_zoo.git/intel/person-detection-action-recognition-0006/FP32/person-detection-action-recognition-0006.xml \
                       -m_fd ~/t-project/open_model_zoo.git/intel/face-detection-adas-0001/FP32/face-detection-adas-0001.xml \
                       -m_reid ~/t-project/open_model_zoo.git/intel/face-reidentification-retail-0095/FP32/face-reidentification-retail-0095.xml \
                       -m_lm ~/t-project/open_model_zoo.git/intel/landmarks-regression-retail-0009/FP32/landmarks-regression-retail-0009.xml
                       
~/omz_demos_build/intel64/Release/smart_classroom_demo -m_act ~/t-project/open_model_zoo.git/intel/person-detection-action-recognition-0006/FP32/person-detection-action-recognition-0006.xml \
                       -m_fd ~/t-project/open_model_zoo.git/intel/face-detection-adas-0001/FP32/face-detection-adas-0001.xml \
                       -m_reid ~/t-project/open_model_zoo.git/intel/face-reidentification-retail-0095/FP32/face-reidentification-retail-0095.xml \
                       -m_lm ~/t-project/open_model_zoo.git/intel/landmarks-regression-retail-0009/FP32/landmarks-regression-retail-0009.xml \
                       -i ~/my_video-1.mp4 \
                       -out_v ~/my_video-1.out.avi
```
#### 人体关键点检测

```
source /opt/intel/openvino/bin/setupvars.sh
~/omz_demos_build/intel64/Release/human_pose_estimation_demo -i <path_to_video>/input_video.mp4 -m <path_to_model>/human-pose-estimation-0001.xml -d CPU
```

解析模型输出的代码在
```
demos/common/cpp/models/src/hpe_model_openpose.cpp:46
void HPEOpenPose::prepareInputsOutputs(CNNNetwork& cnnNetwork) 
```

#### 人体关键点检测(python) human_pose_estimation_demo

可以使用 pip openvino 包或者 openvinotoolkit 。openvinotoolkit 的 opencv 无法解码 h.264 视频，故推荐 pip openvino 。

```
cd ~/project/openvino_model_zoo.git/demos/human_pose_estimation_demo/python/

./human_pose_estimation_demo.py -i 0 -m ~/project/openvino_model_zoo.git/intel/human-pose-estimation-0001/FP32/human-pose-estimation-0001.xml -at openpose

./human_pose_estimation_demo.py -i 0 -m ~/project/openvino_model_zoo.git/intel/human-pose-estimation-0005/FP32/human-pose-estimation-0005.xml -at ae

./human_pose_estimation_demo.py -i ~/project/annotation/1572483602284.mp4 --loop -m ~/project/openvino_model_zoo.git/intel/human-pose-estimation-0005/FP32/human-pose-estimation-0005.xml -at ae

./human_pose_estimation_demo.py -i ~/project/annotation/1624496810975.mp4 --loop -m ~/project/openvino_model_zoo.git/intel/human-pose-estimation-0005/FP32/human-pose-estimation-0005.xml -at ae

./human_pose_estimation_demo.py -i ~/project/annotation/1624496810975.mp4 --loop -m ~/project/openvino_model_zoo.git/intel/human-pose-estimation-0006/FP32/human-pose-estimation-0006.xml -at ae

./human_pose_estimation_demo.py -i ~/project/annotation/1624496810975.mp4 --loop -m ~/project/openvino_model_zoo.git/intel/human-pose-estimation-0007/FP32/human-pose-estimation-0007.xml -at ae

./human_pose_estimation_demo.py -i ~/project/annotation/a1.jpg --loop -m ~/project/openvino_model_zoo.git/intel/human-pose-estimation-0006/FP32/human-pose-estimation-0006.xml -at ae
```
-at openpose 双核 CPU 上 4-6 fps，占用近 50% 。帧率随着人数下降。openpose 模式存在 bug ，修复方法见后面。
-at ae 双核 CPU 上 5-6fps，占用近 50% 。加上 -nthreads 4 参数并不能增加帧率和 CPU 占用。

human-pose-estimation-0001.xml: openpose
human-pose-estimation-0005.xml: EfficientHRNet 288x288
human-pose-estimation-0006.xml: EfficientHRNet 352x352
human-pose-estimation-0007.xml: EfficientHRNet 448x448

EfficientHRNet 人体关键点序号定义（openpose 也转换到这个格式，丢掉了 'neck'）：

```
'nose',# 0
'neck',# -
'right_shoulder',# 6
'right_elbow',# 8
'right_wrist',# 10
'left_shoulder',# 5
'left_elbow',# 7
'left_wrist',# 9
'right_hip',# 12
'right_knee',# 14
'right_ankle',# 16
'left_hip',# 11
'left_knee',# 13
'left_ankle',# 15
'right_eye',# 2
'left_eye',# 1
'right_ear',# 4
'left_ear',# 3
```

EfficientHRNet 容易将不同的人的关键点混淆，在人员密级的场合不太适用。openpose 也有类似的问题。

openpose 的代码似乎有 bug ，颠倒了输出中的两个部分：heatmaps（ Mconv7_stage2_L2，形状 1, 19, 32, 57 ） 和 part affinity fields(PAF, Mconv7_stage2_L1，形状 1, 38, 32, 57)，导致程序崩溃。

参考：
models/intel/human-pose-estimation-0001/description/human-pose-estimation-0001.md
https://docs.openvinotoolkit.org/latest/omz_models_model_human_pose_estimation_0001.html

修改方法：

```
demos/common/python/models/open_pose.py
@@ -33,14 +33,14 @@ class OpenPose(Model):
 
         function = ng.function_from_cnn(self.net)
-        paf = function.get_output_op(0)
+        paf = function.get_output_op(1) # duanyao
         paf = paf.inputs()[0].get_source_output().get_node()
         paf.set_friendly_name(self.pafs_blob_name)
-        heatmap = function.get_output_op(1)
+        heatmap = function.get_output_op(0) # duanyao
```

似乎仓库里 2021-06-22 修复了这个问题：9c242ea74 ("demos/py/human_pose_estimation/open_pose: don't rely on output_ops order in ng Function", 2021-06-22)
这个修复似乎表明，openvino 不保证 function.get_output_op() 和 function.get_results() 的顺序有什么意义，客户代码应视之为随机的。

但是，c++ 代码中的顺序似乎与 python 不一致。参考 `demos/common/cpp/models/src/hpe_model_openpose.cpp:46` 。

python 代码中，self.net.outputs 的顺序是正确的（ Mconv7_stage2_L1 在前），function.get_ops(), function.get_ordered_ops() 顺序也正确，但不知为什么 function.get_output_op() 和 function.get_results() 的顺序就反过来了（ Mconv7_stage2_L2 在前 ）。function 的实现在这里（暂时没看出问题）：
https://github.com/openvinotoolkit/openvino/blob/722891756fe9d9fbd7f29147b7b08b69723b924c/ngraph/core/src/function.cpp
ng.function_from_cnn 的实现在这里：
https://github.com/openvinotoolkit/openvino/blob/f89b3d770b0ad2c5875d3cb3d05e12d986e93625/ngraph/python/src/ngraph/helpers.py

function_from_cnn 的实现是 IENetwork::getFunction() 。查看代码后发现，function 是 net 创建时就已经创建。故应该参考 `InferenceEnginePython::read_network()`:

https://github.com/openvinotoolkit/openvino/blob/f89b3d770b0ad2c5875d3cb3d05e12d986e93625/inference-engine/ie_bridges/python/src/openvino/inference_engine/ie_api_impl.cpp#L204

openvino.git/inference-engine/src/inference_engine/src/ie_network_reader.cpp:/ReadNetwork()

#### human_pose_estimation_3d_demo

设置环境。假定 python 本地库 pose_extractor 已经编译到了 demos/build/intel64/Release/lib 目录下。
```
source /opt/intel/openvino/bin/setupvars.sh
source ~/opt/venv/v37/bin/activate
export PYTHONPATH=$PYTHONPATH:~/project/openvino_model_zoo.git/demos/build/intel64/Release/lib
```

或者
```
cp ~/project/openvino_model_zoo.git/demos/build/intel64/Release/lib/pose_extractor.so ~/project/openvino_model_zoo.git/demos/human_pose_estimation_3d_demo/python/pose_extractor.cpython-37m-x86_64-linux-gnu.so
```

如果想在演示程序目录下编译 python 本地库，则在 venv 环境下编译 pose_extractor。不需要激活 openvino，pose_extractor 只依赖 python 本身。不同的 python 版本的 so 文件可以用 `cpython-3X-x86_64-linux-gnu` 后缀区分。

```
source ~/opt/venv/v37/bin/activate
cd ~/project/openvino_model_zoo.git/demos/human_pose_estimation_3d_demo/python/pose_extractor
cmake -D PYTHON_EXECUTABLE=python -D PYTHON_INCLUDE_DIRS=/usr/include/python3.7m . 
make
cp pose_extractor.so ../pose_extractor.cpython-37m-x86_64-linux-gnu.so
```

```
source ~/opt/venv/v39/bin/activate
cd ~/project/openvino_model_zoo.git/demos/human_pose_estimation_3d_demo/python/pose_extractor
cmake -D PYTHON_EXECUTABLE=python -D PYTHON_INCLUDE_DIRS=/usr/include/python3.9 . 
make
cp pose_extractor.so ../pose_extractor.cpython-39-x86_64-linux-gnu.so
```

PYTHON_INCLUDE_DIRS 可以用 python 代码 `import sysconfig; print(sysconfig.get_path('include'))` 获得。

执行程序：
```
./human_pose_estimation_3d_demo.py -i 0 -m ~/project/openvino_model_zoo.git/public/human-pose-estimation-3d-0001/FP32/human-pose-estimation-3d-0001.xml

./human_pose_estimation_3d_demo.py -i ~/project/annotation/1572483602284.m4v.mp4 --loop -m ~/project/openvino_model_zoo.git/public/human-pose-estimation-3d-0001/FP32/human-pose-estimation-3d-0001.xml

./human_pose_estimation_3d_demo.py --loop -m ~/project/openvino_model_zoo.git/public/human-pose-estimation-3d-0001/FP32/human-pose-estimation-3d-0001.xml -i ~/project/annotation/1625185842605/1.jpg

./human_pose_estimation_3d_demo.py --loop -m ~/project/openvino_model_zoo.git/public/human-pose-estimation-3d-0001/FP32/human-pose-estimation-3d-0001.xml -i ~/project/annotation/a1.jpg

./human_pose_estimation_3d_demo.py --loop -m ~/project/openvino_model_zoo.git/public/human-pose-estimation-3d-0001/FP32/human-pose-estimation-3d-0001.xml -i ~/project/annotation/1624496810975.mp4
```

在笔记本电脑上，用摄像头检测但人，帧率 4.5 ，CPU（双核）占用 60% 左右，内存占用 113 MB左右。FP16与FP32模型差别不大。
随着人数增多，帧率变化不大。

二维关键点定义（19点）：

```
[0, 1],  # neck - nose
[1, 16], [16, 18],  # nose - l_eye - l_ear
[1, 15], [15, 17],  # nose - r_eye - r_ear
[0, 3], [3, 4], [4, 5],     # neck - l_shoulder - l_elbow - l_wrist
[0, 9], [9, 10], [10, 11],  # neck - r_shoulder - r_elbow - r_wrist
[0, 6], [6, 7], [7, 8],        # neck - l_hip - l_knee - l_ankle
[0, 12], [12, 13], [13, 14]])  # neck - r_hip - r_knee - r_ankle
```

#### single_human_pose_estimation_demo

思路是先进行全帧的人体检测，得到包围框，然后对每个包围框内进行人体关键点检测。这样降低了对人体关键点检测程序的要求（区分重叠人体、人数上限、不同尺度的人）。

--person_label 参数用于指定检测器中“人”的分类。默认是 15,但不同的检测器可以不同，可查看模型的 README.md 文件。

cd ~/project/openvino_model_zoo.git

demos/single_human_pose_estimation_demo/python/single_human_pose_estimation_demo.py -m_od intel/pedestrian-and-vehicle-detector-adas-0001/FP32/pedestrian-and-vehicle-detector-adas-0001.xml --person_label 2 -m_hpe ~/project/openvino_model_zoo.git/public/single-human-pose-estimation-0001/FP32/single-human-pose-estimation-0001.xml -i ~/project/annotation/1625185842605/1.jpg  # 检测阈值 0.4

demos/single_human_pose_estimation_demo/python/single_human_pose_estimation_demo.py -m_od public/mobilenet-ssd/FP32/mobilenet-ssd.xml -m_hpe ~/project/openvino_model_zoo.git/public/single-human-pose-estimation-0001/FP32/single-human-pose-estimation-0001.xml --loop -i ~/project/annotation/1625185842605/1.jpg  # 检测阈值 0.4

demos/single_human_pose_estimation_demo/python/single_human_pose_estimation_demo.py -m_od intel/person-detection-retail-0013/FP32/person-detection-retail-0013.xml  --person_label 1 -m_hpe public/single-human-pose-estimation-0001/FP32/single-human-pose-estimation-0001.xml --loop -i ~/project/annotation/1625185842605/1.jpg # 检测阈值 0.4

demos/single_human_pose_estimation_demo/python/single_human_pose_estimation_demo.py -m_od public/ssd512/FP32/ssd512.xml -m_hpe ~/project/openvino_model_zoo.git/public/single-human-pose-estimation-0001/FP32/single-human-pose-estimation-0001.xml --loop -i ~/project/annotation/1625185842605/1.jpg   # 检测阈值 0.4，一个人两次检出

demos/single_human_pose_estimation_demo/python/single_human_pose_estimation_demo.py -m_od intel/person-vehicle-bike-detection-crossroad-1016/FP32/person-vehicle-bike-detection-crossroad-1016.xml --person_label 2 -m_hpe ~/project/openvino_model_zoo.git/public/single-human-pose-estimation-0001/FP32/single-human-pose-estimation-0001.xml --loop -i ~/project/annotation/1625185842605/1.jpg # 检测阈值 0.4

#### object_detection_demo

对象检测演示。可以使用多种模型。

demos/object_detection_demo/python/README.md

demos/object_detection_demo/python/object_detection_demo.py --loop -at ssd -m intel/pedestrian-and-vehicle-detector-adas-0001/FP32/pedestrian-and-vehicle-detector-adas-0001.xml -i  ~/project/annotation/1625185842605/1.jpg

demos/object_detection_demo/python/object_detection_demo.py --loop -at ssd -m intel/person-detection-retail-0013/FP32/person-detection-retail-0013.xml -i  ~/project/annotation/1625185842605/1.jpg

demos/object_detection_demo/python/object_detection_demo.py --loop -at ssd -m public/ssd512/FP32/ssd512.xml --labels data/dataset_classes/voc_20cl_bkgr.txt -t 0.0001 -i  ~/project/annotation/1625185842605/1.jpg

demos/object_detection_demo/python/object_detection_demo.py --loop -at ssd -m public/mobilenet-ssd/FP32/mobilenet-ssd.xml --labels data/dataset_classes/voc_20cl_bkgr.txt -t 0.0001 -i  ~/project/annotation/1625185842605/1.jpg 

demos/object_detection_demo/python/object_detection_demo.py --loop -at ssd -m intel/person-vehicle-bike-detection-crossroad-1016/FP32/person-vehicle-bike-detection-crossroad-1016.xml -i  ~/project/annotation/1625185842605/1.jpg

demos/object_detection_demo/python/object_detection_demo.py --loop -at ssd -m public/faster_rcnn_inception_v2_coco/FP32/faster_rcnn_inception_v2_coco.xml -i ~/project/annotation/1625185842605/1.jpg

demos/object_detection_demo/python/object_detection_demo.py --loop -at ssd -m public/faster_rcnn_inception_resnet_v2_atrous_coco/FP32/faster_rcnn_inception_resnet_v2_atrous_coco.xml -i ~/project/annotation/1625185842605/1.jpg

demos/object_detection_demo/python/object_detection_demo.py --loop -at ssd -m public/faster_rcnn_resnet50_coco/FP32/faster_rcnn_resnet50_coco.xml -i ~/project/annotation/1625185842605/1.jpg

demos/object_detection_demo/python/object_detection_demo.py --loop -at ssd -m intel/person-detection-retail-0013/FP32/person-detection-retail-0013.xml -i  ~/project/annotation/1572483602284.mp4

demos/object_detection_demo/python/object_detection_demo.py --loop -at ssd -m intel/person-detection-retail-0013/FP16-INT8/person-detection-retail-0013.xml -i  ~/project/annotation/1572483602284.mp4

demos/object_detection_demo/python/object_detection_demo.py --loop -at ssd -m public/mobilenet-ssd/FP32/mobilenet-ssd.xml --labels data/dataset_classes/voc_20cl_bkgr.txt -t 0.1 -i /home/duanyao/project/annotation/合成人体检测数据集A/人-cs-ext-300,300,300,600.png

demos/object_detection_demo/python/object_detection_demo.py --loop -at ssd -m intel/person-detection-retail-0013/FP32/person-detection-retail-0013.xml --labels data/dataset_classes/coco_91cl_bkgr.txt --resize_type fit_to_window_letterbox -t 0.1 -i /home/duanyao/project/annotation/合成人体检测数据集A/人-cs-ext-300,300,300,600.png

demos/object_detection_demo/python/object_detection_demo.py --loop -at ssd -m intel/person-detection-retail-0013/FP32/person-detection-retail-0013.xml --labels data/dataset_classes/coco_91cl_bkgr.txt -t 0.1 -i /home/duanyao/project/annotation/合成人体检测数据集A/人-cs-ext-300,300,300,600.png

不同的模型输出的标签定义不同。

voc_20cl_bkgr.txt: mobilenet-ssd, ssd512
coco_91cl_bkgr.txt: faster_rcnn_inception_v2_coco, faster_rcnn_inception_resnet_v2_atrous_coco, faster_rcnn_resnet50_coco, person-detection-retail-0013（只能检出 person 一类）
0 - non-vehicle, 1 - vehicle, 2 - person: pedestrian-and-vehicle-detector-adas-0001, person-vehicle-bike-detection-crossroad-1016

标签定义文件的格式为：
每一行是一个类型名，而类型名对应的整数标签为行号，从0开始计算。有些标签定义文件中有 background 类型，一般在第零行，实际检测中是不会出现的，因此其作用是将所有的整数标签加一。

测试结果：

- pedestrian-and-vehicle-detector-adas-0001/FP32:  输入分辨率 384, 672， 内存 320MB，17fps，幼儿园检出率约 70%。

- person-detection-retail-0013/FP32: 输入分辨率 320, 544。内存 300MB，22fps，幼儿园检出率约 90%。适用 coco_91cl_bkgr.txt（其实只能检出 person 一类）

- ssd512/FP32: 输入分辨率 512, 512。内存 1.1GB，0.6fps，幼儿园检出率约 80%。

- mobilenet-ssd/FP32:  输入分辨率 300, 300， 内存 300MB，28fps，幼儿园检出率约 60%。输入缩放 resize_type 的影响巨大，保持高宽比的缩放（fit_to_window_letterbox）作用反而可能是负面的。

- person-vehicle-bike-detection-crossroad-1016/FP32:  输入分辨率 512, 512，内存 350MB，18fps，幼儿园检出率约 70%。

- faster_rcnn_inception_v2_coco ：输入分辨率 600, 1024。内存 615MB，1.5fps，幼儿园检出率约 80%。

- faster_rcnn_inception_resnet_v2_atrous_coco ：输入分辨率 600, 1024。内存 3.9GB，0.1fps，幼儿园检出率约 70%。包围框很准确。

- faster_rcnn_resnet50_coco ：输入分辨率 600, 1024。内存 935MB，0.9fps，幼儿园检出率约 110%，有重复。


虽然没有明确支持 faster_rcnn 类模型，如 faster_rcnn_inception_resnet_v2_atrous_coco, faster_rcnn_inception_v2_coco, faster_rcnn_resnet50_coco ，但它们可以看作 ssd 类模型。

共同点是有第二输入：image_info vector of three values in a format `H, W, S`, where `H` is an image height, `W` is an image width, `S` is an image scale factor (usually 1)
输入分辨率均为 600, 1024 。

- faster_rcnn_inception_resnet_v2_atrous_coco
  
- faster_rcnn_inception_v2_coco
  
- faster_rcnn_resnet50_coco
  - 第二输入：同 faster_rcnn_inception_resnet_v2_atrous_coco


输入输出格式：

mobilenet-ssd:
输入：
Image, name - `prob`,  shape - `1, 3, 300, 300`, format is `B, C, H, W`, where:

- `B` - batch size
- `C` - channel
- `H` - height
- `W` - width
    Expected color order: `BGR`.

输出：

name - `detection_out`,  shape - `1, 1, 100, 7` in the format `1, 1, N, 7`, where `N` is the number of detected bounding boxes. For each detection, the description has the format:
[`image_id`, `label`, `conf`, `x_min`, `y_min`, `x_max`, `y_max`], where:

- `image_id` - ID of the image in the batch
- `label` - predicted class ID (1..20 - PASCAL VOC defined class ids). Mapping to class names provided by `<omz_dir>/data/dataset_classes/voc_20cl_bkgr.txt` file.
- `conf` - confidence for the predicted class
- (`x_min`, `y_min`) - coordinates of the top left bounding box corner (coordinates are in normalized format, in range [0, 1])
- (`x_max`, `y_max`) - coordinates of the bottom right bounding box corner  (coordinates are in normalized format, in range [0, 1])

faster_rcnn_inception_v2_coco:
输入：

1. Image, name: `image_tensor`, shape: `1, 3, 600, 1024`, format: `B, C, H, W`, where:

   - `B` - batch size
   - `C` - number of channels
   - `H` - image height
   - `W` - image width

    Expected color order: `BGR`.
2. Information of input image size, name: `image_info`, shape: `1, 3`, format: `B, C`, where:

    - `B` - batch size
    - `C` - vector of 3 values in format `H, W, S`, where `H` - image height, `W` - image width, `S` - image scale factor (usually 1)

注意1可能排在后面，这也是 openvino 的默认顺序。

输出：
name: `reshape_do_2d`, shape: `1, 1, 100, 7` in the format `1, 1, N, 7`, where `N` is the number of detected
bounding boxes. For each detection, the description has the format:
[`image_id`, `label`, `conf`, `x_min`, `y_min`, `x_max`, `y_max`]

`label` - predicted class ID in range [1, 91], mapping to class names provided in `<omz_dir>/data/dataset_classes/coco_91cl_bkgr.txt` file

#### 保存检测结果

```
cd /home/duanyao/project/openvino_model_zoo.git/

python demos/object_detection_demo/python/object_detection_demo.mod.py -at ssd -m public/mobilenet-ssd/FP32/mobilenet-ssd.xml --labels data/dataset_classes/voc_20cl_bkgr.txt --resize_type standard -t 0.0001 -i "/home/duanyao/project/annotation/task_人体检测 广东湛江市小红帽幼儿园 大一班 1624496810975.576p.rk.mp4-2021_10_11_09_37_11-coco 1.0/images/" --coco_output_json "/home/duanyao/project/annotation/task_人体检测  广东湛江市小红帽幼儿园 大一班 1624496810975.576p.rk.mp4-2021_10_11_09_37_11-coco 1.0/det5/det5.json"

python demos/object_detection_demo/python/object_detection_demo.mod.py -at ssd -m public/mobilenet-ssd/FP32/mobilenet-ssd.xml --labels data/dataset_classes/voc_20cl_bkgr.txt --resize_type standard -t 0.0001 -i "/home/duanyao/project/ai-dataset/VOC/VOCtest_06-Nov-2007/VOCdevkit/VOC2007/JPEGImages/" --coco_output_json "/home/duanyao/project/annotation/detection_results/voc/mobilenet-ssd/1/det1.json"
```

#### 源码分析

demos/common/python/openvino/model_zoo/model_api/pipelines/async_pipeline.py

```
class AsyncPipeline: # model_api.pipelines.AsyncPipeline
    def submit_data(self, inputs, id, meta=None):
    def get_raw_result(self, id):
    def get_result(self, id):
```

demos/common/python/openvino/model_zoo/model_api/models/utils.py

```
class Detection: # model_api.models.utils.Detection
    def __init__(self, xmin, ymin, xmax, ymax, score, id):
        self.xmin = xmin
        self.ymin = ymin
        self.xmax = xmax
        self.ymax = ymax
        self.score = score
        self.id = id        # 标签或类型 
```
id 是标签或类型，整数。
xmin 等可以是相对量（0~1范围）或绝对量。
所有的数据类型都可能是 numpy 的标量类型。

demos/common/python/openvino/model_zoo/model_api/models/ssd.py

class SingleOutputParser:

demos/common/python/openvino/model_zoo/model_api/models/detection_model.py

```
class DetectionModel(ImageModel):
    def __init__(self, ie, model_path, resize_type=None,
                 labels=None, threshold=None, iou_threshold=None):
    def resize_detections(detections, original_image_size): # 将 model_api.models.utils.Detection 缩放为绝对量
    def clip_detections(detections, size): # 将 model_api.models.utils.Detection 整数化并限制在图像范围内
```

threshold: 根据置信度过滤输出。默认一般 0.5 ，用于评测时可以取接近 0。
iou_threshold: Non-maximum Suppression (NMS) 的过滤参数。
labels(Iterable[str], str, Path): list of labels for detection classes or path to file with them。默认一般 None。

demos/common/python/openvino/model_zoo/model_api/models/model.py
```
class Model:
    def __init__(self, ie, model_path):
```

demos/common/python/openvino/model_zoo/model_api/models/image_model.py
```
class ImageModel(Model):
    def __init__(self, ie, model_path, resize_type=None): # resize_type 默认 'standard'
```

resize_type：
'standard' 输出尺寸严格符合给定值，可能拉伸画面
'fit_to_window_letterbox' 保持原始高宽比，输出不大于给定值，不足给定尺寸部分两侧填充黑边（实际实现也可能填充其它颜色，如灰色）。
'fit_to_window' 保持原始高宽比，输出不大于给定值，不足给定尺寸部分填充黑边。与 fit_to_window_letterbox 不同的地方在于填充颜色不同，且单侧填充。

多数情况下 resize_type 默认 'standard'。反直觉之处：（1）保持高宽比的缩放不一定更好，可能是因为填充黑边损失了更多像素，而模型训练中已经适应了拉伸的画面。（2）填充黑边的位置和颜色对准确度也是有影响的。

demos/common/python/openvino/model_zoo/model_api/models/ssd.py

### 实例分割 demos/instance_segmentation_demo/python/README.md

对每个实例，给出包围框、分类、位图遮罩（mask）。
同时，利用mask的重叠程度在视频中进行对象跟踪（tracker.py）。

* instance-segmentation-security-0002 ：Mask R-CNN with ResNet50， 768, 1024，已测试，456ms，幼儿园检出率约 90%。
* instance-segmentation-security-0091 ：mask R-CNN with ResNet101， 800, 1344
* instance-segmentation-security-0228 ：Mask R-CNN with ResNet101， 608, 608
* instance-segmentation-security-1039 ：Mask R-CNN with EfficientNet-B2， 480, 480
* instance-segmentation-security-1040 ：Mask R-CNN with EfficientNet-B2， 608, 608，已测试，389ms，幼儿园检出率约 90%。
* yolact-resnet50-fpn-pytorch ：YOLACT ResNet 50 + pytorch ，550, 550，已测试

demos/instance_segmentation_demo/python/instance_segmentation_demo.py -m intel/instance-segmentation-security-1040/FP32/instance-segmentation-security-1040.xml --labels data/dataset_classes/coco_80cl.txt --show_boxes --show_scores --loop -i ~/project/annotation/1625185842605/1.jpg

demos/instance_segmentation_demo/python/instance_segmentation_demo.py -m intel/instance-segmentation-security-1040/FP32/instance-segmentation-security-1040.xml --labels data/dataset_classes/coco_80cl.txt --show_boxes --show_scores --loop -i ~/project/annotation/1625185842605/1.jpg

demos/instance_segmentation_demo/python/instance_segmentation_demo.py -m intel/instance-segmentation-security-1040/FP32/instance-segmentation-security-1040.xml --labels data/dataset_classes/coco_80cl.txt --show_boxes --show_scores --loop -i ~/project/annotation/1625185842605/1.jpg

### 语义分割 demos/segmentation_demo/python/README.md

与 instance_segmentation_demo 不同的是，给出每个像素的一个分类，但不能直接区分同类的不同实例。

* architecture_type = segmentation
  - deeplabv3, 513, 513，已测试，1.9fps，幼儿园检出率约 70%
  - fastseg-large
  - fastseg-small
  - hrnet-v2-c1-segmentation, 320, 320
  - icnet-camvid-ava-0001
  - icnet-camvid-ava-sparse-30-0001
  - icnet-camvid-ava-sparse-60-0001
  - pspnet-pytorch
  - road-segmentation-adas-0001
  - semantic-segmentation-adas-0001, 2048x1024
  - unet-camvid-onnx-0001
* architecture_type = salient_object_detection
  - f3net


demos/segmentation_demo/python/segmentation_demo.py -at segmentation -m public/deeplabv3/FP32/deeplabv3.xml --loop -i ~/project/annotation/1625185842605/1.jpg


### 声音分类 demo
openvino_model_zoo.git/demos/sound_classification_demo/python/README.md

python tools/model_tools//downloader.py --name aclnet
python tools/model_tools//converter.py --name aclnet

模型：
openvino_model_zoo.git/public/aclnet/aclnet_des_53.onnx
openvino_model_zoo.git/public/aclnet/FP32/aclnet.xml

python demos/sound_classification_demo/python/sound_classification_demo.py -m public/aclnet/FP32/aclnet.xml --sample_rate 16000 -i /home/duanyao/project/annotation/声音测试/温州贝心吉幼儿园-宝宝二班-2022-06-16.wav

python demos/sound_classification_demo/python/sound_classification_demo.py -m public/aclnet/FP32/aclnet.xml --sample_rate 16000 -i /home/duanyao/ai-dataset/ESC-50/ESC-50-master/audio/1-22694-A-20.wav

类别定义： openvino_model_zoo.git/data/dataset_classes/aclnet_53cl.txt ，第n行类别为n-1。类别定义基本同 ESC-50 数据集。

运算量：单核CPU上，240秒的音频分类需要11.2秒，速度 11（秒/秒）。

## 运算量估计

open_model_zoo 中的一些模型给出了单次推理运算量的估计，以 flops/Gflops 为单位，但是没有说明是如何估算的。
在 linux 上，使用 perf stat 命令可以测量一个程序执行的浮点操作的数量(详见 linux-perf.md)，例如：

FP32 模型的 256b_packed_single 占绝大部分浮点操作，计算其flops：3578256335*8=28626050680 ~ 28Gflops。
文档给出的 instance-segmentation-security-1040 计算量为 29.334 GFlops，基本符合。
opencv 的图像、视频解码、缩放等操作似乎较少使用CPU浮点运算，所以不会与神经网络运算混淆。

FP16 模型的数据与 FP32 很接近的，说明 FP16 是转换为 FP32 再执行的。

但 FP16-INT8 模型的数据有较大的偏离，对比一次推理和只加载模型不推理的总指令数的差值，说明 FP16-INT8 的很大一部分推理运算为整数运算，但是没有单独的统计。
FP16-INT8 一次推理的 256b_packed_single 指令数为 452147390 = 0.452G，整数指令数为 13400011311 - 8694352606 - 452147390 = 4253511315 = 4.25G 。

根据 openvino 文档，INT8 操作是通过 SSE4.2 ~ AVX* 指令集进行的：
https://github.com/openvinotoolkit/openvino/blob/master/docs/IE_DG/Int8Inference.md
例如，AVX2 的 `__m256i _mm256_add_epi8 (__m256i a, __m256i b) ` Add packed 8-bit integers in a and b, and store the results in dst.
SSE2 的 `__m128i _mm_add_epi8 (__m128i a, __m128i b)` Add packed 8-bit integers in a and b, and store the results in dst.

根据 task-clock 计算的推理时间：FP32 0.53s，FP16-INT8 0.82s。

FP32 加载模型，一次推理：
```
 Performance counter stats for 'python demos/instance_segmentation_demo/python/instance_segmentation_demo.py -m intel/instance-segmentation-security-1040/FP32/instance-segmentation-security-1040.xml --labels data/dataset_classes/coco_80cl.txt -i /home/duanyao/project/annotation/1625185842605/1.jpg':

          3,277.33 msec task-clock                #    1.071 CPUs utilized          
     8,590,336,178      cycles                    #    2.621 GHz                      (37.04%)
    10,626,958,220      instructions              #    1.24  insn per cycle           (36.81%)
        38,672,266      fp_arith_inst_retired.scalar_single #   11.800 M/sec                    (37.70%)
           452,437      fp_arith_inst_retired.128b_packed_single #    0.138 M/sec                    (38.38%)
     3,411,371,063      fp_arith_inst_retired.256b_packed_single # 1040.898 M/sec                    (39.90%)
           270,371      fp_arith_inst_retired.scalar_double #    0.082 M/sec                    (39.66%)
                59      fp_arith_inst_retired.128b_packed_double #    0.018 K/sec                    (38.34%)
                 2      fp_arith_inst_retired.256b_packed_double #    0.001 K/sec                    (37.06%)

       3.059767212 seconds time elapsed

       2.721094000 seconds user
       0.566399000 seconds sys
```

FP32 只加载模型，不推理：
```
 Performance counter stats for 'python demos/instance_segmentation_demo/python/instance_segmentation_demo.py -m intel/instance-segmentation-security-1040/FP32/instance-segmentation-security-1040.xml --labels data/dataset_classes/coco_80cl.txt -i /home/duanyao/project/annotation/1625185842605/1.jpg':

          2,743.91 msec task-clock                #    1.060 CPUs utilized          
     7,212,921,532      cycles                    #    2.629 GHz                      (38.42%)
     6,140,293,332      instructions              #    0.85  insn per cycle           (38.30%)
            68,678      fp_arith_inst_retired.scalar_single #    0.025 M/sec                    (37.99%)
               466      fp_arith_inst_retired.128b_packed_single #    0.170 K/sec                    (38.17%)
                 5      fp_arith_inst_retired.256b_packed_single #    0.002 K/sec                    (37.27%)
           205,730      fp_arith_inst_retired.scalar_double #    0.075 M/sec                    (37.08%)
                18      fp_arith_inst_retired.128b_packed_double #    0.007 K/sec                    (37.92%)
                13      fp_arith_inst_retired.256b_packed_double #    0.005 K/sec                    (38.32%)

       2.589186176 seconds time elapsed

       1.935839000 seconds user
       0.811737000 seconds sys
```

INT8 加载模型，一次推理：
```
 Performance counter stats for 'python demos/instance_segmentation_demo/python/instance_segmentation_demo.py -m intel/instance-segmentation-security-1040/FP16-INT8/instance-segmentation-security-1040.xml --labels data/dataset_classes/coco_80cl.txt -i /home/duanyao/project/annotation/1625185842605/1.jpg':

          4,462.45 msec task-clock                #    1.131 CPUs utilized          
    11,743,080,102      cycles                    #    2.632 GHz                      (37.85%)
    12,955,516,262      instructions              #    1.10  insn per cycle           (37.60%)
        80,242,284      fp_arith_inst_retired.scalar_single #   17.982 M/sec                    (37.06%)
         5,429,996      fp_arith_inst_retired.128b_packed_single #    1.217 M/sec                    (37.69%)
       518,557,763      fp_arith_inst_retired.256b_packed_single #  116.205 M/sec                    (37.98%)
           631,915      fp_arith_inst_retired.scalar_double #    0.142 M/sec                    (38.48%)
                44      fp_arith_inst_retired.128b_packed_double #    0.010 K/sec                    (38.34%)
                31      fp_arith_inst_retired.256b_packed_double #    0.007 K/sec                    (38.27%)

       3.944971332 seconds time elapsed

       3.793182000 seconds user
       0.687141000 seconds sys
```

INT8 只加载模型，不推理：
```
 Performance counter stats for 'python demos/instance_segmentation_demo/python/instance_segmentation_demo.py -m intel/instance-segmentation-security-1040/FP16-INT8/instance-segmentation-security-1040.xml --labels data/dataset_classes/coco_80cl.txt -i /home/duanyao/project/annotation/1625185842605/1.jpg':

          3,643.52 msec task-clock                #    1.000 CPUs utilized          
     9,604,055,902      cycles                    #    2.636 GHz                      (37.59%)
     8,694,352,606      instructions              #    0.91  insn per cycle           (37.60%)
         4,302,989      fp_arith_inst_retired.scalar_single #    1.181 M/sec                    (37.66%)
            13,358      fp_arith_inst_retired.128b_packed_single #    0.004 M/sec                    (37.65%)
                44      fp_arith_inst_retired.256b_packed_single #    0.012 K/sec                    (37.82%)
           555,157      fp_arith_inst_retired.scalar_double #    0.152 M/sec                    (38.33%)
                 2      fp_arith_inst_retired.128b_packed_double #    0.001 K/sec                    (37.98%)
                10      fp_arith_inst_retired.256b_packed_double #    0.003 K/sec                    (37.85%)

       3.643728370 seconds time elapsed

       3.039664000 seconds user
       0.607149000 seconds sys
```

## 线程

可以用 `ps -T -p <pid>` 和 `top -H -p <pid>` 查看 oepnvino 程序的线程。

oepnvino python 模块的线程没有指定有特征的名字，所以只能根据其活动特征来推断：

* python 自身的线程只有一个（也是主线程），而 openvino 会启动多个线程，应该不会占用主线程做计算，其中承载主要计算的线程数量默认与系统的 vCPU 数量相同。因此，理论上单个 python openvino 进程默认可以让 CPU 满载。
* 主线程的负载可能并不低，在目标检测项目中，在双核四线程CPU上可达50%左右，需要注意不要让它成为瓶颈。

## C++ 编译

编译使用 openvino 库的 C++ 程序时，也需要先设置环境（即 `source /opt/intel/openvino/bin/setupvars.sh` ），这设置了 cmake 相关的环境变量以及 LD_LIBRARY_PATH 。

cmake 文件中可以这样写：

```
# 禁用 c++11 abi 。注意不同的 openvino 版本可能不一样。
add_definitions(-D_GLIBCXX_USE_CXX11_ABI=0)

find_package(InferenceEngine 2021.4.0 REQUIRED) # 过去用过版本号 2.0 ，也可以不写版本号
if (NOT(InferenceEngine_FOUND))
    message(FATAL_ERROR ${IE_NOT_FOUND_MESSAGE})
endif()
                                      
find_package(ngraph REQUIRED)
if (NOT(ngraph_FOUND))
    message(FATAL_ERROR "ngraph not found")
endif()

find_package(TBB REQUIRED)
if (NOT(TBB_FOUND))
    message(FATAL_ERROR "TBB not found")
endif()

find_package(OpenCV COMPONENTS ${IE_SAMPLE_OPENCV_DEPENDENCIES} QUIET)
include_directories(${OpenCV_INCLUDE_DIRS})

set(exe_required_libs a b) # 其他依赖库
list(APPEND exe_required_libs ${OpenCV_LIBS})
list(APPEND exe_required_libs ${InferenceEngine_LIBRARIES})
list(APPEND exe_required_libs "IE::inference_engine_transformations") # InferenceEngine_LIBRARIES 可能漏掉了此项
list(APPEND exe_required_libs ${NGRAPH_LIBRARIES})

list(APPEND exe_required_libs "TBB::tbb") # 或者 list(APPEND exe_required_libs ${TBB_IMPORTED_TARGETS}) ， TBB_IMPORTED_TARGETS 包含更多的库，但只有 TBB::tbb 是必需的

target_link_libraries(foo ${exe_required_libs})

# target_link_libraries(${IE_SAMPLE_NAME} PRIVATE ${OpenCV_LIBRARIES} ${InferenceEngine_LIBRARIES} ${IE_SAMPLE_DEPENDENCIES} gflags)
```

其中使用的 NGRAPH_LIBRARIES TBB_IMPORTED_TARGETS 等变量可以从 /opt/intel/**/XXConfig.make 文件中找到，例如

```
openvino_2021.4.582/opencv/cmake/OpenCVConfig.cmake
openvino_2021.4.582/deployment_tools/inference_engine/external/tbb/cmake/TBBConfig.cmake
openvino_2021.4.582/deployment_tools/ngraph/cmake/ngraphConfig.cmake
```
openvino 自带 opencv（2021.8 是 opencv 4.x），cmake 会自动找到它。

需要注意 openvino 以及其携带的 opencv 的 c++ ABI 。centos yum 2021.8 版本是 c++03 ABI，但 gcc 5 以上已经默认 c++11 ，两者不兼容，链接会出错。所以视情况加上编译器宏 `-D_GLIBCXX_USE_CXX11_ABI=0` 禁用 c++11 abi。

ubuntu 2018 apt 2021.8 版本是 c++11 ABI，无需添加上述编译器宏。

python 包 openvino 的本地代码也采用 c++03 ABI ，因此与 ubuntu 2018 apt 2021.8 版本不兼容。

## 源码

```
git clone https://github.com/opencv/dldt.git  # -> git clone https://github.com/openvinotoolkit/openvino.git openvino.git --depth=1
cd dldt/inference-engine/                     # -> cd openvino.git/inference-engine/
git submodule init
git submodule update --recursive              # -> git submodule update --recursive --depth=1
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..           # -> cmake -DCMAKE_SOURCE_DIR=../.. -DCMAKE_BUILD_TYPE=Release ..
make -j13
 ../bin/intel64/Release/InferenceEngineUnitTests
```

## python API

要使用 openvino 的 python API ，也需要激活 `source /opt/intel/openvino/bin/setupvars.sh` ，这会设置 PYTHONPATH ：
```
PYTHONPATH=/opt/intel/openvino/python/python3.7:/opt/intel/openvino/python/python3:/opt/intel/openvino/deployment_tools/model_optimizer:/opt/intel/openvino/data_processing/dl_streamer/python:/opt/intel/openvino/data_processing/gstreamer/lib/python3.8/site-packages
```
如果你使用 python 的 venv ，接下来激活它 `source ~/opt/venv/v39/bin/activate` 。

openvino 的 python API 中已经包括了 opencv 的 python API ，无需单独设置或安装。其位置在：`/opt/intel/openvino_2021.4.582/python/python3/cv2/python-3/cv2.abi3.so` 。

## 使用 Pre-Trained 模型

[3.1]
