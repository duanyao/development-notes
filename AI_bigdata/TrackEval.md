## 参考资料

[1.1] TrackEval
  https://github.com/JonathonLuiten/TrackEval

## TrackEval 工具使用方法

根目录：
TrackEval.git

真例和预测结果文件分别放在以下位置：
```
data/gt/mot_challenge/seqmaps/MOT20-train.txt
data/gt/mot_challenge/MOT20-train/MOT20-01/gt/gt.txt
data/gt/mot_challenge/MOT20-train/MOT20-01/seqinfo.ini
data/trackers/mot_challenge/MOT20-train/MPNTrack/data/MOT20-01.txt
```
目录结构解释：

真例：
```
gt                  # 真例
    mot_challenge   # 测试名
        seqmaps     # 序列列表
          MOT20-train.txt
        MOT20-train # 数据集
            MOT20-01  # 序列名（一个视频）
                seqinfo.ini # 序列的元数据
                gt  # 真例
                    gt.txt # 真例，MOT 格式
```
MOT20-train.txt 的内容和格式：

```
name
MOT20-01
MOT20-02
MOT20-03
MOT20-05
```

seqinfo.ini 是序列的元数据，内容和格式：
```
[Sequence]
name=MOT20-01
imDir=img1
frameRate=25
seqLength=429
imWidth=1920
imHeight=1080
imExt=.jpg
```

seqinfo.ini 是必须的，但 CVAT 输出的 MOT 1.1格式没有这个文件。seqLength 是总帧数，应大于等于 gt.txt 中的最大帧序号。

gt.txt 的内容和格式：
```
# <frame_id>, <track_id>, <bb_left>, <bb_top>, <bb_width>, <bb_height>, <conf>, <x>, <y>, <z>
1,1,199,813,140,268,1,1,0.83643
2,1,201,812,140,268,1,1,0.84015
3,1,203,812,140,268,1,1,0.84015
...
1,2,347,814,124,267,1,1,0.88469
2,2,349,813,124,268,1,1,0.88761
3,2,352,813,123,268,1,1,0.89051
...
```
注意，有些数据集的列数不是规定的10（如 MOT20，以及 CVAT 导出的 MOT1.1），而是9，缺少最后的z。
帧序号从1开始，一般是连续的，但不连续（即存在缺帧）也能用。一般来说，一个轨迹的行排列在一起。

预测结果：
```
trackers/
    mot_challenge/   # 测试名
        MOT20-train/ # 数据集
            MPNTrack/ # 预测算法名
                data/
                    MOT20-01.txt # 序列名（一个视频），预测结果，MOT 格式
```

要在自定义数据集上评测，建议直接替换一个已有的数据集，例如 MOT20-train（不要改这个名字，因为是代码中写死的）。步骤如下：

创建/修改 data/gt/mot_challenge/seqmaps/MOT20-train.txt ，替换为自定义的视频名，如

```
name
mot-duanyao-201
```

将视频 `mot-duanyao-201` 的 Ground Truth 文件放到 `data/gt/mot_challenge/MOT20-train/mot-duanyao-201/gt/gt.txt` 。
创建 `data/gt/mot_challenge/MOT20-train/mot-duanyao-201/seqinfo.ini` ，内容要符合视频 mot-duanyao-201 的实际情况。
将跟踪器 `duanyao-tracker` 的跟踪结果文件放到 `data/trackers/mot_challenge/MOT20-train/duanyao-tracker/data/mot-duanyao-201.txt` 。

最后，运行评测 `python  scripts/run_mot_challenge.py --BENCHMARK MOT20` 。
