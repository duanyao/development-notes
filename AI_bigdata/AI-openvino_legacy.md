## 参考资料

[1.0] 下载 openvino-toolkit
  https://software.intel.com/en-us/openvino-toolkit

[1.1] 安装 openvino-toolkit
  https://docs.openvinotoolkit.org/latest/_docs_install_guides_installing_openvino_linux.html
  https://docs.openvinotoolkit.org/2020.1/_docs_install_guides_installing_openvino_linux.html#install-external-dependencies

[1.2] intel compute-runtime 
  https://github.com/intel/compute-runtime/releases

[1.3] downloader https://github.com/opencv/open_model_zoo/blob/efd238d02035f8a5417b7b1e25cd4c997d44351f/tools/downloader/README.md

[2.1] OpenVINO™ Toolkit - Deep Learning Deployment Toolkit repository
  https://github.com/openvinotoolkit/openvino

[2.2] CPU Requirements for OpenVINO CPU inference
  https://community.intel.com/t5/Intel-Distribution-of-OpenVINO/CPU-Requirements-for-OpenVINO-CPU-inference/td-p/1149229

[2.3] Build on Linux* Systems
  https://github.com/openvinotoolkit/openvino/wiki/BuildingForLinux
  
[3.1] Intel at the Edge (Leveraging Pre-Trained Models)
  https://krbnite.github.io/Intel-at-the-Edge-Leveraging-Pre-Trained-Models/

[4.1] Error in converting custom ssd model using Tensorflow2 Object detection API
  https://community.intel.com/t5/Intel-Distribution-of-OpenVINO/Error-in-converting-custom-ssd-model-using-Tensorflow2-Object/m-p/1281338/highlight/true

[10.1] openvino / open_model_zoo 的训练代码库 PyTorch
    https://github.com/opencv/openvino_training_extensions/tree/develop/pytorch_toolkit
  
[10.2] Real-time 2D Multi-Person Pose Estimation on CPU: Lightweight OpenPose - training - Intel 2018
  https://github.com/opencv/openvino_training_extensions/tree/develop/pytorch_toolkit/human_pose_estimation


## (过时) 安装 openvino-toolkit （debian）

### (过时) 准备

sudo apt install libpng12-dev libcairo2-dev libpango1.0-dev libglib2.0-dev libgtk2.0-dev

/etc/lsb-release 例子：
```
DISTRIB_ID=Ubuntu
DISTRIB_RELEASE=16.04
DISTRIB_CODENAME=xenial
DISTRIB_DESCRIPTION="Ubuntu 16.04.6 LTS"
```

```
sudo mv /etc/lsb-release /etc/lsb-release-origin
sudo dedit /etc/lsb-release-ubuntu-1604
```

变更、复原：
```
sudo ln -sf /etc/lsb-release-ubuntu-1604 /etc/lsb-release
sudo ln -sf etc/lsb-release-origin /etc/lsb-release
```

### (过时) 安装 openvino-toolkit
从 https://software.intel.com/en-us/openvino-toolkit 下载 openvino-toolkit ，需要注册。

Serial number : CCBP-4PJRX4XB

将下载的文件 `l_openvino_toolkit_p_<version>.tgz` 解压到 `l_openvino_toolkit_p_<version>` ，在目录下执行：
```
sudo ./install_openvino_dependencies.sh
sudo ./install_GUI.sh
```
安装到了 `opt/intel/`：

```
ls -l /opt/intel/
总用量 8
lrwxrwxrwx 1 root root  41 4月   6 01:57 intel_sdp_products.db -> /opt/intel/.pset/db/intel_sdp_products.db
drwxr-xr-x 1 root root 106 1月  27 23:55 mediasdk
lrwxrwxrwx 1 root root  30 4月   6 01:57 openvino -> /opt/intel/openvino_2020.1.023
drwxr-xr-x 1 root root 222 4月   6 01:57 openvino_2020.1.023
```

可以同时安装多个版本的 openvino-toolkit 到同一个位置（ /opt/intel/ ），用后缀的版本号区分，如 `/opt/intel/openvino_2020.1.023` ，符号链接 `/opt/intel/openvino` 指向默认版本。

使用时，执行 `source /opt/intel/openvino/bin/setupvars.sh` 来设置 PATH 等环境变量。

Use Intel-optimized version of OpenCV
Another version of OpenCV has been detected. To use the Intel-optimized version of OpenCV, read the installation guide to run /opt/intel/openvino_2019.3.334/bin/setupvars.sh to update the OpenCV_DIR variable.

Missing required libraries or packages. You will be prompted to install them later
Install the following libraries or packages required for Intel-optimized OpenCV, the Inference Engine, and the Model Optimizer tools: 
libpng12-dev
After completing this part of the installation, use the installation guide to install this dependency.


### (过时) 卸载 openvino-toolkit
```
ls -l /opt/intel/
/opt/intel/openvino_<version>/openvino_toolkit_uninstaller/uninstall_GUI.sh
/opt/intel/openvino_<version>/openvino_toolkit_uninstaller/uninstall.sh
```

### (过时) 安装 opencl runtime
```
mkdir intel-opencl
cd intel-opencl

wget https://github.com/intel/compute-runtime/releases/download/20.15.16524/intel-gmmlib_20.1.1_amd64.deb
wget https://github.com/intel/compute-runtime/releases/download/20.15.16524/intel-igc-core_1.0.3771_amd64.deb
wget https://github.com/intel/compute-runtime/releases/download/20.15.16524/intel-igc-opencl_1.0.3771_amd64.deb
wget https://github.com/intel/compute-runtime/releases/download/20.15.16524/intel-opencl_20.15.16524_amd64.deb
wget https://github.com/intel/compute-runtime/releases/download/20.15.16524/intel-ocloc_20.15.16524_amd64.deb
wget https://github.com/intel/compute-runtime/releases/download/20.15.16524/intel-level-zero-gpu_0.8.16524_amd64.deb

sudo dpkg -i *.deb
```

### (过时) 安装 model_optimizer

安装依赖库：

```
sudo apt install libgfortran3
```

在 venv 中运行（根据需要的框架）：

```
pip install -r /opt/intel/openvino/deployment_tools/model_optimizer/requirements_tf.txt
pip install -r /opt/intel/openvino/deployment_tools/model_optimizer/requirements_caffe.txt
pip install -r /opt/intel/openvino_2019.3.334/deployment_tools/model_optimizer/requirements_onnx.txt
```
不要用 /opt/intel/openvino/deployment_tools/model_optimizer/install_prerequisites/*.sh ，因为这些脚本不会处理 venv 。

其中 tf 的版本要求是：`tensorflow>=1.2.0,<2.0.0` 。注意，openvino_2019R3 与 networkx 2.4 有 bug，需要降级：

```
pip install "networkx>=1.11,<2.4"
```

## (过时) 安装 openvino-toolkit （centos + installer）

将下载的文件 `l_openvino_toolkit_p_<version>.tgz` 解压到 `l_openvino_toolkit_p_<version>` ，在目录下执行：
```
sudo ./install_openvino_dependencies.sh
sudo ./install.sh
```
