## 参考资料

[0.1] scikit-learn (sklearn) Metrics and scoring: quantifying the quality of predictions
https://scikit-learn.org/stable/modules/model_evaluation.html

[0.2] The proper way to use Machine Learning metrics
https://towardsdatascience.com/the-proper-way-to-use-machine-learning-metrics-4803247a2578

[1.1] Breaking Down Mean Average Precision (mAP) Another metric for your data science toolkit
https://towardsdatascience.com/breaking-down-mean-average-precision-map-ae462f623a52#1a59

[1.2] mAP (mean Average Precision) for Object Detection
https://jonathan-hui.medium.com/map-mean-average-precision-for-object-detection-45c121a31173

[1.3] mean Average Precision (mAP)
https://blog.csdn.net/william_hehe/article/details/80006758

[2.1] Metrics for object detection
https://github.com/rafaelpadilla/Object-Detection-Metrics

[2.2] Open-Source Visual Interface for Object Detection Metrics
https://github.com/rafaelpadilla/review_object_detection_metrics

[2.3] Is true positive defined as iou > iou_threshold or iou >= iou_threshold?
https://github.com/rafaelpadilla/review_object_detection_metrics/issues/84

[3.1] Calculating Precision & Recall for Multi-Class Classification
https://medium.com/data-science-in-your-pocket/calculating-precision-recall-for-multi-class-classification-9055931ee229

[3.2] Precision, Recall, Accuracy, and F1 Score for Multi-Label Classification
https://medium.com/synthesio-engineering/precision-accuracy-and-f1-score-for-multi-label-classification-34ac6bdfb404

[4.1] fiftyone Loading data into FiftyOne
https://voxel51.com/docs/fiftyone/user_guide/dataset_creation/index.html

[4.2] fiftyone evaluation
https://voxel51.com/docs/fiftyone/user_guide/evaluation.html

[5.1] A General Toolbox for Identifying Object Detection Errors
https://dbolya.github.io/tide/

[5.2] TIDE: A General Toolbox for Identifying Object Detection Errors
https://dbolya.github.io/tide/paper.pdf

!! TorchMetrics is a collection of 80+ PyTorch metrics implementations and an easy-to-use API to create custom metrics. 
https://github.com/Lightning-AI/metrics

## 综合
[0.1~0.2]

## 目标检测

### AP, mAP
参考 [1.1] 。

目标检测可能涉及多个分类。先选择其中一个分类的检测框和真目标框，在给定的置信度阈值和 IOU 阈值下，可定义以下各项指标：

* TP：检测确信、准确、唯一。确信是指置信度大于置信度阈值，准确是指与真目标框的IOU大于IOU阈值，唯一是指检测框与真目标框的关系不存在多重匹配。具体如何处理多重匹配见后面的细节讨论。
* FP：非 TP 的检测框。有以下情况：（1）置信度低于阈值。（2）有其它检测框与此检测框对同一个真目标框的IOU都大于阈值，且置信度足够。（3）与真目标框存在多重匹配。
* TN：无。
* FN：真目标没有相应的TP检测框。

给定测试数据集和识别程序（包括参数），精确率定义为 precision = TP/(TP+FP) 即 TP/DET , 召回率定义为 recall = TP/(TP+FN) 即 TP/GT，DET 为检测框（的数量），GT 为基准目标框（的数量）。
召回率-精确率曲线下的面积，被定义为 Average Precision （AP）。

涉及多个分类时，应分别计算各个分类的上述指标。所有分类的 AP 的平均值，为平均精确率 mAP 。也有人将 mAP 简称为 AP ，而 AP 称为分类平均精确率 AP per class。

虽然 AP 的定义在业界基本一致，但不同的标准和工具实现也有着细节上的不同，比如：

* 如何处理检测框和真目标框的多重匹配
  * 检测框对应的真目标框不唯一的情况的处理。如果一个确信的检测框对于多个真目标框都是准确的，可能有不同的处理方式：（1）算作FP。（2）算作TP，选其中一个真目标框（一般选iou最大的）作为匹配目标。
    review_object_detection_metrics 中 PASCAL VOC 和 COCO 的算法都采用后者。
  * 真目标框对应的检测框不唯一情况的处理。如果一个真目标框对应了多个确信、准确的检测框，可能有不同的处理方式：（1）都算作FP。（2）其中一个（一般选置信度最大的或iou最大的）算作TP，其余的算作FP。
    review_object_detection_metrics 中 PASCAL VOC 和 COCO 的算法都采用后者。
  * 具体实现中，可以先把检测框按置信度倒排序，逐一处理；对一个检测框，候选真目标框中选iou最大的作为匹配目标，设置检测框为TP；对于匹配目标被已经匹配的检测框，设置为FP。

* 坐标的数值采用浮点数还是整数？如果是整数，计算面积时，是左闭右开还是左右都闭（意思是，x_min=1,x_max=3,那么宽度是2还是3）？
  COCO 数据集采用浮点坐标，而 VOC 采用整数坐标，左闭右开（参考 [2.2] 的源码）。

* 对 IOU 阈值的理解：是大于 IOU 阈值为真正，还是大于等于 IOU 阈值为真正？似乎大部分文章中采用前者，但各种工具的实现中两者都可能见到，后者更常见。
  coco api, openvino accuracy checker(coco) 和 review_object_detection_metrics (coco, pascal voc)都采用后者 [2.3]。

* 如果用 N-point interpolation ，N 的点数是多少？VOC 采用 11 点，COCO采用 101 点。但如果用 All-point interpolation ，可能不存在实现差异。

* 检测置信度（confidence 或 score）阈值，增大它会让 AP 下降。理论上阈值应该是无穷小，但实际上无法这样做，一般是设置一个较小的数，如 0.05~0.1 ，但这个阈值的大小可能会影响检测出来的目标数，影响 recall-precision 曲线的右端（ recall 较大）。

* 允许目标数。有的工具的某些算法（openvino accuracy checker, coco_precision）会限制每一帧的最大目标数，丢弃超过这个数且检测置信度排名靠后的目标。在实际目标数大于允许目标数时，这可能降低 AP。

* 浮点精度。有的工具采用双精度（python float默认），有的采用单精度，还有的采用两者混合。例如 openvino accuracy checker 的 IOU 是单精度，但 IOU 阈值却是双精度，有时候会产生微妙的误差，例如 numpy.float32(7)/numpy.float32(10) < float(7)/float(10) 。review_object_detection_metrics 采用双精度。

* 对无效分类的处理。例如某个分类的正例数（ground truth positives）为零，则 recall 无意义，因此此分类的 AP 也无意义。有的工具在计算 mAP 时忽略无效分类的 AP，有的则把无效分类的 AP 视为 0 参与平均，造成不同的结果。openvino accuracy checker 的 COCO AP 属于前者，VOC mAP 属于后者，review_object_detection_metrics 则总是属于前者。为了避免麻烦，最好避免出现某个分类的正例数为零的情况。

* 是否对数据集和/或检测结果过滤。有的模型不能/不擅长检测小目标，那么可以从数据集和/或检测结果过滤掉小目标，这样会提高检测成绩。例如 openvino accuracy checker configs/person-detection-retail-0013.yml 。

### TIDE: A General Toolbox for Identifying Object Detection Errors
[5.1-5.2]


## 工具

### review_object_detection_metrics
[2.2]
#### 安装

git clone https://github.com/rafaelpadilla/review_object_detection_metrics.git review_object_detection_metrics.git

安装以下依赖的 python 包（在 venv 环境下，最好是 python 3.9+）。左边为文档要求，右边为实际安装结果

```
pyqt=5.12   # PyQt5-5.15.5 PyQt5-Qt5-5.15.2 PyQt5-sip-12.9.0
pytest      # pytest-6.2.5
awscli==1.18.180
chardet==3.0.4  # chardet-4.0.0
click==7.1.2   # click-8.0.3
flake8==3.8.4  # flake8-4.0.1
python-dotenv==0.15.0  # python-dotenv-0.19.1
pyyaml==5.3.1  # PyYAML 5.4.1
sphinx==3.3.1  # sphinx-4.2.0
```

安装本体：
```
python setup.py install
```
运行：

```
python run.py
```

#### 数据格式

##### COCO json

用于检测结果时，在 annotation 对象中增加了一个 score 字段，存储检测的置信度。标准的 COCO json 是没有这个字段的。


#### 问题解决

##### Qt platform plugin "xcb"

python run.py 出错：

```
qt.qpa.plugin: Could not load the Qt platform plugin "xcb" in "/home/duanyao/opt/venv/v39/lib/python3.9/site-packages/cv2/qt/plugins" even though it was found.
This application failed to start because no Qt platform plugin could be initialized. Reinstalling the application may fix this problem.
```
解决：在 python 代码中确保先激活 PyQt5 而不是 opencv-python 的 qt。具体做法是导入任何 opencv 包之前，先执行以下代码：
```
import sys
from PyQt5 import QtWidgets
QtWidgets.QApplication(sys.argv)
```
创建 QApplication 对象将触发加载 PyQt5 的 plugins 。



### COCO API

安装
```
git clone https://github.com/cocodataset/cocoapi.git cocoapi.git
cd cocoapi.git/PythonAPI/

# 在 venv 环境执行
pip install Cython
make
python setup.py install  # 安装了 pycocotools==2.0
```

数据格式：
标注部分，按照 COCO annotation JSON 格式书写，参考 AI-数据集_图像_dataset_images.md 。注意其中 area 字段不可省略。
检测数据部分，采用 COCO annotation JSON 格式中的 annotations 属性的格式，即 annotation 的数组，在 annotation 对象中增加了一个 score 字段，存储检测的置信度。例如：

```
[
        {
            "id": 1,
            "image_id": 2,
            "category_id": 1,
            "segmentation": [],
            "bbox": [
                11,
                0,
                92,
                100
            ],
            "score": 0.89
        },
        {
            "id": 2,
            "image_id": 2,
            "category_id": 1,
            "segmentation": [],
            "bbox": [
                311,
                0,
                84,
                100
            ],
            "score": 0.82
        },
]
```

使用

```
from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval

gt_file='/home/duanyao/project/object-detection-metrics-validation.git/annotation/1.coco/gt1.coco.json'
dt_file='/home/duanyao/project/object-detection-metrics-validation.git/detection/det.1.coco_eval/det1.coco_eval.json'

annType='bbox'

cocoGt = COCO(gt_file)
cocoDt = cocoGt.loadRes(dt_file)

cocoEval = COCOeval(cocoGt, cocoDt, annType)
cocoEval.evaluate()
cocoEval.accumulate()

cocoEval.summarize()
```

```
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.604
 Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.890
 Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets=100 ] = 0.509
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = -1.000
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.500
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.682
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=  1 ] = 0.567
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets= 10 ] = 0.675
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.675
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = -1.000
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.500
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.691
```

### FiftyOne

#### 安装

```
pip install fiftyone
```

#### 使用


```
import fiftyone as fo

data_path = "/home/duanyao/project/annotation/task_人体检测 广东湛江市小红帽幼儿园 大一班 1624496810975.576p.rk.mp4-2021_10_11_09_37_11-coco 1.0/images"
labels_path = "/home/duanyao/project/annotation/task_人体检测 广东湛江市小红帽幼儿园 大一班 1624496810975.576p.rk.mp4-2021_10_11_09_37_11-coco 1.0/annotations/instances_default.json"

dataset = fo.Dataset.from_dir(
     dataset_type=fo.types.COCODetectionDataset,
     data_path=data_path,
     labels_path=labels_path,
     name="my-dataset", # 可选
)

dataset.default_classes # 输出 `['0', 'person']` 

# 启动 web 界面 。FiftyOne 会启动一个 http 服务器，并引导浏览器去访问，默认地址是： http://localhost:5151
session = fo.launch_app(dataset)
```
在web界面中，显示了每个帧的画面和标注/检测结果。

加入预测结果，评估（接上面）：
```
import fiftyone.utils.coco as fouc

# classes = dataset.distinct("predictions.detections.label")

# And add model predictions
fouc.add_coco_labels(
    dataset,
    "predictions",
    "/home/duanyao/project/annotation/detection_results/ds1/faster_rcnn_resnet50_coco-fp32/det1/det1.json",
    #classes=classes,
)

# Verify that ground truth and predictions were imported as expected
print(dataset.count("ground_truth.detections"))  # 913
print(dataset.count("predictions.detections"))  # 915

# Evaluate the objects in the `predictions` field with respect to the
# objects in the `ground_truth` field
results = dataset.evaluate_detections(
    "predictions",
    gt_field="ground_truth",
    eval_key="eval_predictions",
    compute_mAP=True,
)

# Get the 10 most common classes in the dataset
counts = dataset.count_values("ground_truth.detections.label")  # {'person': 913}
classes = sorted(counts, key=counts.get, reverse=True)[:10]  # ['person']

# Print a classification report for the top-10 classes
results.print_report(classes=classes)
```

结果：
```
              precision    recall  f1-score   support

      person       0.92      0.92      0.92       913

   micro avg       0.92      0.92      0.92       913
   macro avg       0.92      0.92      0.92       913
weighted avg       0.92      0.92      0.92       913
```

接上面：
```
print(results.mAP())  # 0.5594409291278265

# 显示 PR 曲线，分类显示。classes 指定类别，classes 省略时，显示top 3 AP类别。
plot = results.plot_pr_curves(classes=classes)
plot.show()
```

## 目标跟踪

### TrackEval 工具

可以评测多目标跟踪的指标，如 HOTA 等。
见 TrackEval.md 。

### py-motmetrics 工具

可评测 CLEAR-MOT 和 ID 指标。

依赖：Python 3.5/3.6/3.9 and numpy, pandas and scipy

```
pip install motmetrics
```
使用：
```
import motmetrics as mm
import numpy as np

# Create an accumulator that will be updated during each frame
acc = mm.MOTAccumulator(auto_id=True)

# Call update once for per frame. For now, assume distances between
# frame objects / hypotheses are given.
acc.update(
    [1, 2],                     # Ground truth objects in this frame
    [1, 2, 3],                  # Detector hypotheses in this frame
    [
        [0.1, np.nan, 0.3],     # Distances from object 1 to hypotheses 1, 2, 3
        [0.5,  0.2,   0.3]      # Distances from object 2 to hypotheses 1, 2, 3
    ]
)

print(acc.events) # a pandas DataFrame containing all events

print(acc.mot_events) # a pandas DataFrame containing MOT only events
```
## 分类

### 多重分类 Multiple classes
[3.1]
Macro averaging precision： 对每个类别求 precision （其它类别视为负类），然后求各类别的 precision 算术平均。recall 同理。
Micro averaging precision： 对每个类别求 TP 和 PP，将所有类的 TP 的和作为总体 TP，所有类的 PP 的和作为总体 PP，求出总体 precision 。

micro averaging 会给样本数更多的类别更高的权重，Macro averaging 则不会。
