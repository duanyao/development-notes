## 参考资料

## 训练 Stanford 40

### 数据集准备

```
cd ~/data-development/ai-dataset/Stanford40/
mkdir -p image_by_action
cd image_by_action
for c in applauding running waving_hands
do
  mkdir -p $c
  cd $c
  rm -f $c*.jpg
  ln -s -t . ../../JPEGImages/$c*.jpg
  cd ..
done
```

形成了如下目录和文件：

```
image_by_action/
image_by_action/applauding
image_by_action/applauding/applauding_001.jpg
image_by_action/applauding/applauding_002.jpg
image_by_action/applauding/applauding_003.jpg

image_by_action/running
image_by_action/running/running_001.jpg
image_by_action/running/running_002.jpg
image_by_action/running/running_003.jpg

image_by_action/waving_hands
image_by_action/waving_hands/waving_hands_001.jpg
image_by_action/waving_hands/waving_hands_002.jpg
image_by_action/waving_hands/waving_hands_003.jpg
```

每个类约 200 个图像。


### 执行训练
在 venv 中运行：

```
$ cd ~/project/human-action-classification.git
$ python scripts/retrain.py --image_dir ~/data-development/ai-dataset/Stanford40/image_by_action --architecture mobilenet_1.0_224


INFO:tensorflow:2020-05-11 01:52:25.254275: Step 0: Train accuracy = 72.0%
I0511 01:52:25.255323 140678546343744 retrain.py:1082] 2020-05-11 01:52:25.254275: Step 0: Train accuracy = 72.0%

...
INFO:tensorflow:2020-05-11 01:52:33.980653: Step 140: Train accuracy = 100.0%
I0511 01:52:33.980736 140678546343744 retrain.py:1082] 2020-05-11 01:52:33.980653: Step 140: Train accuracy = 100.0%
INFO:tensorflow:2020-05-11 01:52:33.980939: Step 140: Cross entropy = 0.043079
I0511 01:52:33.980976 140678546343744 retrain.py:1084] 2020-05-11 01:52:33.980939: Step 140: Cross entropy = 0.043079
INFO:tensorflow:2020-05-11 01:52:34.041420: Step 140: Validation accuracy = 79.0% (N=100)
I0511 01:52:34.041485 140678546343744 retrain.py:1100] 2020-05-11 01:52:34.041420: Step 140: Validation accuracy = 79.0% (N=100)
...

INFO:tensorflow:2020-05-11 01:56:50.839037: Step 3999: Train accuracy = 100.0%
I0511 01:56:50.839141 140678546343744 retrain.py:1082] 2020-05-11 01:56:50.839037: Step 3999: Train accuracy = 100.0%
INFO:tensorflow:2020-05-11 01:56:50.840821: Step 3999: Cross entropy = 0.004798
I0511 01:56:50.840869 140678546343744 retrain.py:1084] 2020-05-11 01:56:50.840821: Step 3999: Cross entropy = 0.004798
INFO:tensorflow:2020-05-11 01:56:51.039639: Step 3999: Validation accuracy = 78.0% (N=100)
I0511 01:56:51.039728 140678546343744 retrain.py:1100] 2020-05-11 01:56:51.039639: Step 3999: Validation accuracy = 78.0% (N=100)
INFO:tensorflow:Final test accuracy = 79.2% (N=77)
I0511 01:56:51.816673 140678546343744 retrain.py:1126] Final test accuracy = 79.2% (N=77)
```

可以看出，140步、8s 后训练精度就已经达到100% 。

运行中，下载了 /tmp/imagenet/mobilenet_v1_1.0_224_frozen.tgz 文件，解压后为 /tmp/imagenet/mobilenet_v1_1.0_224/frozen_graph.pb ，大小 17.1MB 。

训练后的输出为 /tmp/output_graph.pb（大小 17.1MB）和 /tmp/output_labels.txt ，后者的内容是：

```
applauding
running
waving hands
```


再次尝试
```
$ python scripts/retrain.py --image_dir ~/data-development/ai-dataset/Stanford40/image_by_action --architecture mobilenet_1.0_224 --how_many_training_steps 200 --train_batch_size 40 --print_misclassified_test_images

INFO:tensorflow:2020-05-11 04:49:42.609207: Step 199: Train accuracy = 100.0%
I0511 04:49:42.609287 140522445039424 retrain.py:1082] 2020-05-11 04:49:42.609207: Step 199: Train accuracy = 100.0%
INFO:tensorflow:2020-05-11 04:49:42.609465: Step 199: Cross entropy = 0.053565
I0511 04:49:42.609483 140522445039424 retrain.py:1084] 2020-05-11 04:49:42.609465: Step 199: Cross entropy = 0.053565
INFO:tensorflow:2020-05-11 04:49:42.669694: Step 199: Validation accuracy = 71.0% (N=100)
I0511 04:49:42.669811 140522445039424 retrain.py:1100] 2020-05-11 04:49:42.669694: Step 199: Validation accuracy = 71.0% (N=100)
INFO:tensorflow:Final test accuracy = 81.6% (N=76)
I0511 04:49:43.014863 140522445039424 retrain.py:1126] Final test accuracy = 81.6% (N=76)
```
