## 参考资料
[1.1] 人工智能基础课 王天一
https://time.geekbang.org/column/intro/62

[1.2] 阿杜白话大数据
https://www.jianshu.com/u/c67a0f275408

[1.3] 点金大数据
http://www.datagold.com.cn/

[1.4] AI技术内参
https://time.geekbang.org/column/intro/43

[1.5] 
http://www.fast.ai/

[1.6] 天池大数据竞赛
https://tianchi.aliyun.com

[1.7] AI Insight
https://zhuanlan.zhihu.com/ai-insight

[1.8] 斯坦福大学公开课 ：机器学习课程
http://open.163.com/special/opencourse/machinelearning.html

[1.9] fast.ai Lesson 1 Notes
http://wiki.fast.ai/index.php/Lesson_1_Notes

[1.10] 百度 AI课堂
http://bit.baidu.com/course/index/column/7.html

[1.11] 机器学习笔记 - 数学系的数据挖掘民工
https://www.cnblogs.com/charlotte77/category/764838.html

发布基于飞桨的前沿研究工作，包括CV、NLP、KG、STDM等领域的顶会论文和比赛冠军模型。
https://github.com/PaddlePaddle/Research

Google Research
https://github.com/google-research

facebook research
https://github.com/facebookresearch

AI前线

[2.1] 论人工智能的泡沫、价值与应用困境
https://www.jianshu.com/p/098250071529

[2.2] AlphaGo与人工智能
https://www.jianshu.com/p/6b2e6c807462

[2.3] 大数据独角兽Palantir之核心技术探秘
http://www.datagold.com.cn/archives/7204.html

[2.4] PyTorch发布一年团队总结：运行资源降低至十分之一，单机王者 
https://mp.weixin.qq.com/s/3vFhgTG9LZo2eyDd818hug##

[2.5] 开发易、通用难，深度学习框架何时才能飞入寻常百姓家？
https://mp.weixin.qq.com/s?__biz=MzU1NDA4NjU2MA==&mid=2247487846&idx=1&sn=68784589a384fa7fe2ee58b922fb83b7&chksm=fbe9a8a9cc9e21bf0dcee2c6053e634aeb5c184b21fc81cb1efd669e41744d527c68d74ae772&scene=21#wechat_redirect

[3.1] 数据科学家基础能力之机器学习
https://time.geekbang.org/column/article/311

[3.2] 转行AI之学习方法
https://zhuanlan.zhihu.com/p/31959375

[3.3] 转行AI之小书单
https://zhuanlan.zhihu.com/p/31808191

[3.4] 转行AI之小视频
https://zhuanlan.zhihu.com/p/31869315

[3.5] 如果概率统计水平不高，有哪些适合的概率统计书来学习机器学习？
https://www.zhihu.com/question/25956333

[3.6] 如何用3个月零基础入门「机器学习」？
https://zhuanlan.zhihu.com/p/29704017

[5.1] AI可能真的要代替插画师了……
https://zhuanlan.zhihu.com/p/28488946

[5.2] 天池医疗AI大赛[第一季] Rank8解决方案[附TensorFlow/PyTorch/Caffe实现方案]
https://tianchi.aliyun.com/forum/new_articleDetail.html?spm=5176.9876270.0.0.acd2bc8K8ROFQ&raceId=231601&postsId=3099

[5.3] “零基础”实现人脸表情识别
https://zhuanlan.zhihu.com/p/28168781

[6.1] Hidden Limits of Machine Learning
http://ramugedia.com/limits-of-machine-learning

[7.x] 框架和库

[7.1] PerfBLAS BLAS library for deep learning on ARM 
  http://perfxlab.com/

[7.2] InferXLite lightweight deep learning inference framework for embedded systems. It supports ARM CPUs, ARM Mali GPUs, AMD APU SoCs, and NVIDIA GPU.
  http://perfxlab.com/

[7.3] 一个易学、易用的开源深度学习框架
  http://www.paddlepaddle.org

[9.0] 书籍
[9.1] An Introduction to Statistical Learning - with Applications in R
  http://www-bcf.usc.edu/~gareth/ISL/ISLR%20Seventh%20Printing.pdf

[9.2] The Elements of Statistical Learning - Data Mining, Inference, and Prediction
  http://www.web.stanford.edu/~hastie/ElemStatLearn/

[9.3] Python Machine Learning book code repository (仅附带代码)
  https://github.com/rasbt/python-machine-learning-book

## 概述

几十年来，人工智能技术研究的五大门派（如下图）一直以来都在彼此争夺主导权。 (1)符号派：使用符号、规则和逻辑来表征知识和进行逻辑推理，最喜欢的算法是：规则和决策树。(2)贝叶斯派：获取发生的可能性来进行概率推理，最喜欢的算法是：朴素贝叶斯或马尔可夫。(3)进化派：生成变化，然后为特定目标获取其中最优的，最喜欢的算法是：遗传算法。(4)类推派：根据约束条件来优化函数（尽可能走到更高，但同时不要离开道路），最喜欢的算法是：支持向量机。(5)联结派：使用概率矩阵和加权神经元来动态地识别和归纳模式，最喜欢的算法是：神经网络。[2.1]


### 机器学习
机器学习主要解决的是两类问题：监督学习和无监督学习。

* 如何把现实场景中的问题抽象成相应的数学模型，并知道在这个抽象过程中，数学模型有怎样的假设。

* 如何利用数学工具，对相应的数学模型参数进行求解。

* 如何根据实际问题提出评估方案，对应用的数学模型进行评估，看是否解决了实际问题。

监督学习的最终目标，是使模型可以更准确地对我们所需要的响应变量建模。
比如，我们希望通过一系列特征来预测某个地区的房屋销售价格，希望预测电影的票房，或者希望预测用户可能购买的商品。这里的“销售价格”、“电影票房”以及“可能购买的商品”都是监督学习中的响应变量。

通常情况下，无监督学习并没有明显的响应变量。无监督学习的核心，往往是希望发现数据内部的潜在结构和规律，为我们进行下一步决断提供参考。典型的无监督学习就是希望能够利用数据特征来把数据分组，机器学习语境下叫作“聚类”。

监督学习的基础是三类模型：

* 线性模型

* 决策树模型

* 神经网络模型

这三类监督学习模型又可以细分为处理两类问题：

* 分类问题

* 回归问题
