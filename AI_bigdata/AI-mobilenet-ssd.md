chuanqi305 / MobileNet-SSD
https://github.com/chuanqi305/MobileNet-SSD/

【手把手AI项目】六、Caffe实现MobileNetSSD以及文件说明，利用自己的数据集训练模型
https://zhuanlan.zhihu.com/p/83153852

Result on VOC
https://github.com/chuanqi305/MobileNet-SSD/issues/152

SSD300 Pascal VOC 07+12 Training Summary
https://github.com/pierluigiferrari/ssd_keras/blob/master/training_summaries/ssd300_pascal_07+12_training_summary.md

## 精度测试

Model name 	InputSize 	TrainSet 	TestSet 	mAP
YOLOv3-Mobilenet 	320x320 	VOC07+12 	VOC07 	74.56%
MobileNet-SSD 	300x300 	VOC07+12+coco 	VOC07 	72.7%
