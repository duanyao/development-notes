## 参考资料

[2.1] https://github.com/open-mmlab/mmaction/blob/master/INSTALL.md

[2.2] https://github.com/open-mmlab/mmaction/blob/master/DATASET.md

[2.3] https://github.com/open-mmlab/mmaction/blob/master/data_tools/ucf101/PREPARING_UCF101.md

[2.4] https://github.com/open-mmlab/mmaction/blob/master/GETTING_STARTED.md

[2.5] mmcv/runner/checkpoint.py
  https://github.com/open-mmlab/mmcv/blob/d6ce3a2e1b42d6cb1b3505e51f9323419cc9e8a4/mmcv/runner/checkpoint.py
  
[3.1] MMAction2 is an open-source toolbox for video understanding based on PyTorch
  https://github.com/open-mmlab/mmaction2

## 安装

下载更新源码
```
git clone https://github.com/open-mmlab/mmaction.git mmaction.git

cd mmaction.git

# 初次
git submodule update --init --recursive

# 以后
git submodule update --recursive
```

如果使用虚拟环境，则修改 ./compile.sh ，将所有 `setup.py install --user` 的 `--user` 去掉。这是因为 python 虚拟环境与 `--user` 安装选项不兼容，详见 python.md 。

```
source ~/opt/venv/v37/bin/activate

sh ./compile.sh

python setup.py develop
```

### 安装 dense_flow

前置依赖：
```
apt install libzip-dev
```

`dense_flow` 可以使用 opencv 的 2.4, 3.1, 4.1 这3个版本，可检出相应的分支（如 `git checkout -b opencv-2.4 --track origin/opencv-2.4`），其中 opencv 4.1 是 master 分支依赖的。
因为 `dense_flow` 依赖 opencv 的 nonfree 扩展，所以不能使用 debian 自带的 opencv 。建议下载 opencv 4.1 的源码编译安装，启用 nonfree、contrib、cuda 等扩展。详见 build_opencv.md 。

```
git clone --recursive http://github.com/yjxiong/dense_flow.git dense_flow.git
cd dense_flow.git
git submodule update --recursive

mkdir build && cd build

# 以下 cmake 语句选择一个即可。不指定 PYTHON 的位置时，cmake 自动检测 python ，一般会找到版本最高的那个。如果 opencv 没有安装在标准位置（建议这样做），则用 OpenCV_DIR 指定。
cmake .. -D OpenCV_DIR=/usr/local/opencv4
cmake .. -D PYTHON_INCLUDE_DIR=/usr/local/include/python3.7m -D PYTHON_LIBRARY=/usr/local/lib/libpython3.7m.so -D OpenCV_DIR=/usr/local/opencv4
cmake .. -D PYTHON_INCLUDE_DIR=/usr/include/python3.5m -D PYTHON_LIBRARY=/usr/lib/x86_64-linux-gnu/libpython3.5m.so -D OpenCV_DIR=/usr/local/opencv4

make -j4
```

## 下载模型

每个测试程序需要的模型见 `configs/*/*.py`，里面有 `pretrained='open-mmlab://bninception_caffe'` 这样的语句，然后在 mmcv 的 checkpoint.py 文件[2.5]里搜索 `bninception_caffe` 即可找到下载链接。
用 wget 下载，然后链接到 `~/.cache/torch/checkpoints/` 目录里。

例如：

```
wget -c https://open-mmlab.s3.ap-northeast-2.amazonaws.com/pretrain/third_party/bn_inception_caffe-ed2e8665.pth -P ~/data-development/ai-model/mmcv-model/
wget -c https://open-mmlab.s3.ap-northeast-2.amazonaws.com/mmaction/models/kinetics400/i3d_kinetics_rgb_r50_c3d_inflated3x1x1_seg1_f32s2_f32s2-b93cc877.pth -P ~/data-development/ai-model/mmcv-model/

ln -s -f ~/data-development/ai-model/mmcv-model/* ~/.cache/torch/checkpoints/
```

## 下载和准备数据集

### UCF-101
[2.2]

```
cd data_tools/ucf101/
sh download_annotations.sh
sh download_videos.sh

cd ../..
python data_tools/build_rawframes.py --df_path ../dense_flow.git/ data/ucf101/videos/ data/ucf101/rawframes/ --level 2 --flow_type tvl1 --num_gpu 1 --num_worker 3 --resume --partial 40
```

注意，`--num_gpu`（默认8）不能多于实际的 GPU 数量，否则会出现以下错误：

```
terminate called after throwing an instance of 'cv::Exception'
  what():  OpenCV(4.1.0) /home/duanyao/t-project/opencv-4.1.0/modules/core/src/cuda_info.cpp:73: error: (-217:Gpu API call) invalid device ordinal in function 'setDevice'
```

`--num_worker`（8）不宜太大，能占满GPU即可。

`--resume` 并不能真的跳过已完成的任务，待查。

`--partial N` 是自行新加的参数，选择其中1/N的任务。这是为了减少计算量和硬盘占用。UCF-101 在 `--partial 40` 时，实际处理视频333个（62.3K 帧，2076秒，1.9GB），GeForce 940MX 耗时约5小时。效率：3.46帧/秒，或0.12秒/秒。

Generate filelist

```
python data_tools/build_file_list.py ucf101 data/ucf101/rawframes/ --level 2 --format rawframes
```
生成了 `data/ucf101/ucf101_train_split_1_rawframes.txt` 和 `data/ucf101/ucf101_val_split_1_rawframes.txt` 等文件。


```
python data_tools/build_file_list.py ucf101 data/ucf101/videos/ --level 2 --format videos
```

生成了 `data/ucf101_train_split_1_videos.txt` 和 `data/ucf101/ucf101_val_split_3_videos.txt` 等文件。

## 测试运行

### tsn_2d_rgb_bninception, ucf101

减少显存占用：修改 `configs/ucf101/tsn_rgb_bninception.py`：

```
@@ -80,11 +80,11 @@ data = dict(
     test=dict(
         type=dataset_type,
         ann_file='data/ucf101/ucf101_val_split_1_rawframes.txt',
         img_prefix=data_root,
         img_norm_cfg=img_norm_cfg,
-        num_segments=25,
+        num_segments=4,
```

num_segments 是指图像批次，num_segments=4 和 2 分别大约需要 2GB 和 4GB 显存。

这个测试需要 tsn_2d_rgb_bninception 模型和 ucf101 数据集。还需要 bn_inception_caffe-ed2e8665.pth 在 ~/.cache/pytorch/checkpoints/ 。

```
# 1 就是 --nproc_per_node
./tools/dist_test_recognizer.sh configs/ucf101/tsn_rgb_bninception.py modelzoo/tsn_2d_rgb_bninception_seg3_f1s1_b32_g8-98160339.pth 1

# 等效于：
python -m torch.distributed.launch --nproc_per_node=1 tools/test_recognizer.py configs/ucf101/tsn_rgb_bninception.py modelzoo/tsn_2d_rgb_bninception_seg3_f1s1_b32_g8-98160339.pth --launcher pytorch --out test-tsn_rgb_bninception.pkl
```

输出：

```
Top-1 Accuracy = 86.60
Top-5 Accuracy = 97.94
```

处理 ucf101 的 1/40 时（333个视频，62.3K 帧，2076秒，1.9GB），GeForce 940MX 耗时 100 秒。平均性能623K帧/秒，或者21秒/秒。

其中 8 是 `PYTHON -m torch.distributed.launch` 的 `--nproc_per_node` 参数。

```
python tools/test_${ARCH}.py ${CONFIG_FILE} ${CHECKPOINT_FILE} [--out ${RESULT_FILE}] [other task-specific arguments]

python tools/test_detector.py configs/ava/ava_fast_rcnn_nl_r50_c4_1x_kinetics_pretrain_crop.py fast_rcnn_ava2.1_nl_r50_c4_1x_f32s2_kin-e2495b48.pth --out ava_fast_rcnn_test.txt

```

