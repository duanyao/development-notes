## 参考资料
光流 | flownet | CVPR2015 | 论文+pytorch代码
https://cloud.tencent.com/developer/article/1818292

FlowNet到FlowNet2.0：基于卷积神经网络的光流预测算法
https://zhuanlan.zhihu.com/p/37736910

港中大等打造光流预测新模型SelFlow，自监督学习攻克遮挡难题 | CVPR 2019
https://cloud.tencent.com/developer/article/1458068?from=article.detail.1818292

OpenCV Optical Flow 
https://docs.opencv.org/4.5.5/d4/dee/tutorial_optical_flow.html
