## 参考资料
[1] 目前哪里可以租用到GPU服务器？
  https://www.zhihu.com/question/51707286
  
[2] 极客云
  https://www.jikecloud.net/
  https://www.jikecloud.net/docs/how-to-use.html#htu3
  
[3] 杭州虎牛
  https://www.hnidc.net.cn/rgzn.html#jx
  
[4] 易学智能
  https://www.easyaiforum.cn/
  https://bbs.easyaiforum.cn/thread-1039-1-1.html
  https://bbs.easyaiforum.cn/thread-1132-1-1.html

[5] 百度AI Studio测评与免费GPU获取方法
  https://zhuanlan.zhihu.com/p/73361554
  
[6] 极链AI云 GPU 服务器租赁
  https://cloud.videojj.com/?inviter=10026
  
[7] Colaboratory（简称 Colab）
  https://colab.research.google.com/notebooks/intro.ipynb

[8] 百度飞桨
  
## 简述

极客云[2]提供家用版GPU的机器，比较便宜，从低档到高档比较全面。
1070ti=2.58/小时。1080Ti=3.8元/小时。
网速一般下行100m上行20m，但也有少数带宽更高的。
其物理机可能是由个人提供的家用PC实现的。
采用 docker 方式，按实际使用秒数收费。
缺点是，关机后再开机，或者企图占用GPU时，可能遇到缺货，需要手动迁移到其他机器。

杭州虎牛[3]提供家用版GPU的机器，但目前只有 Titan X Pascal 一种（价值约9000元）。
按月收费，单GPU 每月 1699。但是外网带宽比较贵，100M 是 3500元/月。

易学智能[4]提供1050Ti、GTX1080Ti、TitanX 等家用版GPU。
按半小时计价。1050Ti=2元/小时，1070=4元/小时，1080Ti=5元/小时，2080Ti=6元/小时。
网络带宽不明。

百度AI Studio[5]提供一些免费测试机会，提供 V100 GPU。

