## 参考资料

[1.1] A Recipe for Training Neural Networks
  http://karpathy.github.io/2019/04/25/recipe/

[1.2] Yes you should understand backprop
  https://karpathy.medium.com/yes-you-should-understand-backprop-e2f06eab496b

[5.1] yolov5 Tips for Best Training Results
  https://github.com/ultralytics/yolov5/wiki/Tips-for-Best-Training-Results
  
