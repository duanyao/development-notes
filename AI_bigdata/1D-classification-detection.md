## 参考资料
[2.1] A 1D-CNN Based Deep Learning Technique for Sleep Apnea Detection in IoT Sensors 
  https://paperswithcode.com/paper/a-1d-cnn-based-deep-learning-technique-for
  https://github.com/arlenejohn/CNN_sleep_apnea
  https://drive.google.com/drive/folders/12bksbKpkro5s4RljIe8nF1gbC0Dl-d3Z?usp=sharing
  
[2.2] A few filters are enough: Convolutional Neural Network for P300 Detection
  https://paperswithcode.com/paper/a-few-filters-are-enough-convolutional-neural
  https://github.com/gibranfp/P300-CNNT
  
[2.3] 时间序列数据分析101 - (15) 1D CNN 一维卷积神经网络
  https://zhuanlan.zhihu.com/p/393565498#
  
[2.4] EEGNet: A Compact Convolutional Network for EEG-based Brain-Computer Interfaces 2016
  https://paperswithcode.com/paper/eegnet-a-compact-convolutional-network-for
  
[2.5] Deep 1D-Convnet for accurate Parkinson disease detection and severity prediction from gait
  https://paperswithcode.com/paper/deep-1d-convnet-for-accurate-parkinson

[2.6] 计步器与Python图解
  https://zhuanlan.zhihu.com/p/167900043
