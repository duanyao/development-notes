## metrics

### type
map 是指 PASCAL VOC mAP，IOU 阈值为 0.5。

coco_precision 是指 COCO AP。其 threshold 参数是 IOU 阈值，如果不指定，则默认是 ".5:.05:.95"，即从 0.5 到 0.95 的平均 AP 。

如果存在同一个 type 的多个 metric，则可以还用 name 属性来区分。

### 举例

```
metrics:
    - type: map
    ignore_difficult: True
    include_boundaries: True
    allow_multiple_matches_per_ignored: False
    distinct_conf: False
    reference: 0.8862
    - type: recall
    ignore_difficult: True
    include_boundaries: True
    allow_multiple_matches_per_ignored: False
    distinct_conf: False
    - name: coco_ap_50
    type: coco_precision
    threshold: 0.50
    - name: coco_ap_55
    type: coco_precision
    threshold: 0.55
    - name: coco_ap_60
    type: coco_precision
    threshold: 0.60
    - name: coco_ap_65
    type: coco_precision
    threshold: 0.65
    - name: coco_ap_70
    type: coco_precision
    threshold: 0.70
    - name: coco_ap_75
    type: coco_precision
    threshold: 0.75
    - name: coco_ap_80
    type: coco_precision
    threshold: 0.80
    - name: coco_ap_85
    type: coco_precision
    threshold: 0.85
    - name: coco_ap_90
    type: coco_precision
    threshold: 0.90
    - name: coco_ap_95
    type: coco_precision
    threshold: 0.95
    - name: coco_ap
    type: coco_precision
    threshold: .5:.05:.95
    - type: coco_recall
    threshold: 0.75
```
## 使用已经推理过的数据

加入 --stored_predictions xxx.pickle 参数，并且 xxx.pickle 文件存在。xxx.pickle 是多个 openvino.tools.accuracy_checker.launcher.loaders.loader.StoredPredictionBatch 对象的连接，每个对应一张图片的推理结果。
-m xxx 参数可以省略。
-s xxx 参数暂时不能省略，且图像文件应存在。

配置文件中，models.datasets[i].preprocessing 属性可以没有，其内容也没影响。models.datasets[i].postprocessing 必须存在，其内容是：
```
- type: resize_prediction_boxes
```
其它形式的 postprocessing 可能导致错误的结果。

## 数据集的格式

### COCO

iscrowd 属性是必要的，默认值 0。


## 代码分析

```
class openvino.tools.accuracy_checker.evaluators.model_evaluator.ModelEvaluator
  dataset: openvino.tools.accuracy_checker.dataset.Dataset


openvino.tools.accuracy_checker.adapters.ssd.SSDAdapter
openvino.tools.accuracy_checker.launcher.dlsdk_launcher.DLSDKLauncher

openvino.tools.accuracy_checker.dataset.Dataset

openvino.tools.accuracy_checker.representation.detection_representation.DetectionPrediction
openvino.tools.accuracy_checker.launcher.loaders.loader.StoredPredictionBatch
```
步骤：

```
evaluator = evaluator_class.from_configs(config_entry) # ModelEvaluator


evaluator.process_dataset(self, stored_predictions: string, progress_reporter, *args, **kwargs) # process_dataset_sync()
  # 如果 stored_predictions 不空或使用 DummyLauncher，且 store_only == false，则加载后返回，否则进行预测
  if (self._is_stored(stored_predictions) or isinstance(self.launcher, DummyLauncher)) and not store_only:
    self._load_stored_predictions(stored_predictions, progress_reporter)
      self.load(self, stored_predictions: string, progress_reporter)
        launcher = DummyLauncher('data_path': stored_predictions, 'loader': 'pickle',)
        predictions = launcher.predict(identifiers) # identifiers: 图片文件名列表
      
    return self._annotations, self._predictions

  # 
  # filled_inputs[0]['data'] 为图像数据，已解压但未缩放。 batch_meta 含有图像和模型输入的元数据。
  filled_inputs, batch_meta, _ = self._get_batch_input(batch_annotation, batch_input)
  # self.launcher: DLSDKLauncher
  batch_predictions = self.launcher.predict(filled_inputs, batch_meta, **kwargs)
  
  self._process_batch_results(
                    batch_predictions, batch_annotation, batch_identifiers,
                    batch_input_ids, batch_meta, enable_profiling, output_callback)
  
  # self._annotations, self._predictions: list[openvino.tools.accuracy_checker.representation.detection_representation.DetectionPrediction]
  # 每个元素对应一个样本图片。返回值没有被利用。
  return self._annotations, self._predictions

# 计算测试分数
metrics_results, metrics_meta =
evaluator.extract_metrics_results(print_results=True, ignore_results_formatting=args.ignore_result_formatting)

  self.compute_metrics(False, ignore_results_formatting, ignore_metric_reference)
  
  return extracted_results, extracted_meta
```
加载 .pickle 文件的是 PickleLoader 类。除此之外尚有 JSONLoader、XMLLoader 。
json 的格式是 `StoredPredictionBatch[]` ，并非 coco json 格式。

```
StoredPredictionBatch = namedtuple('StoredPredictionBatch', ['raw_predictions', 'identifiers', 'meta'])

StoredPredictionBatch = namedtuple(
  raw_predictions: list[ dict{ detection_out: numpy.ndarray<1,1,N,7>, 1} ], 
  identifiers: list[filename:str, 1], # 图像文件名，如 'frame_000000.PNG'
  meta: list[ dict{
    image_size: tuple(h, w, c),
    geometric_operations: list[GeometricOperationMetadata,1],
    preferable_width: int,
    preferable_height: int,
    input_shape: dict{'data': list[int, 4 = [1, 3, 320, 544] ]},
    output_layout: dict{ detection_out: str='NCHW' },
    output_precision: dict{ detection_out: class = numpy.float32 },
  }, 1],
)

storedPredictionBatch.raw_predictions['detection_out']: outputs blob with shape: `1, 1, 200, 7` in the format `1, 1, N, 7`, where `N` is the number of detected
bounding boxes. Each detection has the format [`image_id`, `label`, `conf`, `x_min`, `y_min`, `x_max`, `y_max`], where:

- `image_id` - ID of the image in the batch
- `label` - predicted class ID (1 - person)
- `conf` - confidence for the predicted class
- (`x_min`, `y_min`) - coordinates of the top left bounding box corner, 相对于图像宽度/高度
- (`x_max`, `y_max`) - coordinates of the bottom right bounding box corner, 相对于图像宽度/高度


GeometricOperationMetadata = namedtuple(
  type: str='resize',
  parameters: dict {
    'preferable_width': 544,
    'preferable_height': 320,
    'image_info': [320, 544, 1],
    'scale_x': 0.53125,
    'scale_y': 0.5555555555555556,
    'original_width': 1024,
    'original_height': 576
  }
)

```

### 不同的 metrics 的计算程序

map(Pascal VOC), recall, detection_accuracy, miss_rate, youtube_faces_accuracy: openvino/tools/accuracy_checker/metrics/detection.py



### IOU 比较

openvino/tools/accuracy_checker/metrics/coco_metrics.py

```
def evaluate_image(
        ground_truth, gt_difficult, iscrowd, detections, dt_difficult, scores, iou, thresholds, profile=False
):

# continue to next gt unless better match made
    if iou[dtind, gtind] < iou_current:
        continue
```

openvino/tools/accuracy_checker/metrics/detection.py

```
def bbox_match(annotation: List[DetectionAnnotation], prediction: List[DetectionPrediction], label, overlap_evaluator,
               overlap_thresh=0.5, ignore_difficult=True, allow_multiple_matches_per_ignored=True,
               include_boundaries=True, use_filtered_tp=False):

        if max_overlap < overlap_thresh:
            fp[image] = set_false_positive(image)
            continue
```

### IOU 计算

openvino/tools/accuracy_checker/metrics/overlap.py

```
class IOU(Overlap):
    __provider__ = 'iou'

    def evaluate(self, prediction_box, annotation_boxes):
        intersections_area = self.area(self.intersections(prediction_box, annotation_boxes))
        unions = self.area(prediction_box) + self.area(annotation_boxes) - intersections_area
        return np.divide(
            intersections_area, unions, out=np.zeros_like(intersections_area, dtype=float), where=unions != 0
        )
```

### metrics 最终统计

openvino/tools/accuracy_checker/evaluators/model_evaluator.py
```
    def extract_metrics_results(self, print_results=True, ignore_results_formatting=False,
                                ignore_metric_reference=False):
                                
    def compute_metrics(self, print_results=True, ignore_results_formatting=False, ignore_metric_reference=False):
```

openvino/tools/accuracy_checker/metrics/metric_executor.py

```
    def iterate_metrics(self, annotations, predictions):
```

openvino/tools/accuracy_checker/metrics/detection.py
```
class DetectionMAP(BaseDetectionMetricMixin, FullDatasetEvaluationMetric, PerImageEvaluationMetric):
    def evaluate(self, annotations, predictions):
    def _calculate_map(self, annotations, predictions, profile_boxes=False, return_labels_stats=False):
    # 不会忽略无效的类的结果（例如正例数为0），这种类的结果都是0
    def per_class_detection_statistics(self, annotations, predictions, labels, profile_boxes=False): 
```

openvino/tools/accuracy_checker/metrics/coco_metrics.py
```
class MSCOCOAveragePrecision(MSCOCOBaseMetric):
    def evaluate(self, annotations, predictions):

def compute_precision_recall(thresholds, matching_results):

def finalize_metric_result(values, names):  # 会忽略无效的类的结果（例如正例数为0）
    for value, name in zip(values, names):
        if np.isnan(value):
            continue
```
