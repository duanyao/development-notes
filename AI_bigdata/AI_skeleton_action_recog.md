## 参考资料
[0.1] Awesome Action Recognition
  https://github.com/jinwchoi/awesome-action-recognition
  
[0.2] Deep Learning for Videos: A 2018 Guide to Action Recognition
  http://blog.qure.ai/notes/deep-learning-for-videos-action-recognition-review
  
[0.3] A Comprehensive Survey of Vision-Based Human
 Action Recognition Methods 2019
  https://www.mdpi.com/1424-8220/19/5/1005/pdf
  
[0.4] A survey on still image based human action recognition
  https://www.sciencedirect.com/science/article/abs/pii/S0031320314001642?via%3Dihub

[1.1] The Kinetics Human Action Video Dataset 
  https://deepmind.com/research/open-source/kinetics

[1.1.1] The Kinetics Human Action Video Dataset 2017
  https://arxiv.org/pdf/1705.06950.pdf

[1.1.2] A Short Note on the Kinetics-700 Human Action Dataset
  https://arxiv.org/pdf/1907.06987.pdf

[1.3.1] UCF101 - Action Recognition Data Set
  https://www.crcv.ucf.edu/data/UCF101.php

[1.3.2] 极客云公共数据集 UCF-101
  https://www.jikecloud.net/oss.html
  
[1.3.3] 时空行为检测数据集 JHMDB & UCF101_24 详解
  https://zhuanlan.zhihu.com/p/347756091
  
[1.3.4] SpatioTemporal Annotations of 24 for Classes of UCF101
  https://github.com/gurkirt/corrected-UCF101-Annots/tree/master

[1.4] HMDB51 可直接下载
  https://serre-lab.clps.brown.edu/resource/hmdb-a-large-human-motion-database/

[1.5] dataset  "NTU RGB+D" and "NTU RGB+D 120"
  http://rose1.ntu.edu.sg/Datasets/actionRecognition.asp
  
[1.5.1] with dataset NTU RGB + D 百度网盘
  https://github.com/Hrener/3D-Action-recognition
  
[1.5.2] Info and sample codes for "NTU RGB+D Action Recognition Dataset" 谷歌网盘
  https://github.com/shahroudy/NTURGB-D
  
[1.6] 想训练动作识别模型？这里有六个数据源供你参考 2018
  https://zhuanlan.zhihu.com/p/39737327

[1.7.1] AVA dataset densely annotates 80 atomic visual actions in 430 15-minute movie clips
  https://research.google.com/ava/

[1.7.2] mmaction PREPARING_AVA
  https://github.com/open-mmlab/mmaction/blob/master/data_tools/ava/PREPARING_AVA.md
  
[1.7.3] AVA: A Video Dataset of Spatio-temporally Localized Atomic Visual Actions
  https://arxiv.org/abs/1705.08421
  
[1.7.4] International Challenge on Activity Recognition (ActivityNet)
  http://activity-net.org/challenges/2019/
  
[1.8.1] Charades Dataset
  https://prior.allenai.org/projects/charades
  
[1.9] PKU-MMD: A Large Scale Benchmark for Continuous Multi-Modal Human Action Understanding
  http://www.icst.pku.edu.cn/struct/Projects/PKUMMD.html

[1.9.1] PKU-MMD 数据集
  https://pan.baidu.com/s/1X6GpJnlqGd1v4zeL8_Gjvg#list/path=%2FPKUMMDv2
  
[1.9.2] PKU-MMD: A Large Scale Benchmark for Continuous Multi-Modal Human Action Understanding (paper)
  https://arxiv.org/abs/1703.07475
  
[1.10.1] Official repository for CVPR2021: UAV-Human: A Large Benchmark for Human Behavior Understanding with Unmanned Aerial Vehicles
https://github.com/SUTDCV/UAV-Human
  
[1.10.1] Stanford 40 Actions
  http://vision.stanford.edu/Datasets/40actions.html
  
[1.11.1] HICO & HICO-DET Benchmarks for Recognizing Human-Object Interactions in Images
  http://www-personal.umich.edu/~ywchao/hico/

[2.1] Deep Progressive Reinforcement Learning for Skeleton-based Action Recognition
  http://openaccess.thecvf.com/content_cvpr_2018/papers/Tang_Deep_Progressive_Reinforcement_CVPR_2018_paper.pdf
  
[2.2] Spatial Temporal Graph Convolutional Networks for Skeleton-Based Action Recognition
  https://www.aaai.org/ocs/index.php/AAAI/AAAI18/paper/view/17135/16343

[2.2.1] 如何评价ST-GCN动作识别算法？
  https://www.zhihu.com/question/276101856

[2.2.2]
  https://github.com/open-mmlab/mmskeleton
  
[2.2.3] PP-Human中集成了基于骨骼点的行为识别模块，行为识别模型使用了ST-GCN，并基于PaddleVideo套件完成模型训练
  https://gitee.com/paddlepaddle/PaddleDetection/blob/release/2.4/deploy/pphuman/docs/action.md

[2.3] Quo Vadis, Action Recognition? A New Model and the Kinetics Dataset
  http://openaccess.thecvf.com/content_cvpr_2017/papers/Carreira_Quo_Vadis_Action_CVPR_2017_paper.pdf
  
[2.4] Skeleton-based Action Recognition with Convolutional Neural Networks 2017
  https://arxiv.org/abs/1704.07595v1
  
[2.4.1] PyTorch implementation of 3D-Action-recognition
  https://github.com/Hrener/3D-Action-recognition

[2.5.1] action detector for the Smart Classroom scenario
  https://docs.openvinotoolkit.org/latest/_models_intel_person_detection_action_recognition_0006_description_person_detection_action_recognition_0006.html

[2.6.1] Skeleton-based Action Recognition with Convolutional Neural Networks
  https://arxiv.org/abs/1704.07595

[3.1] Human Action Classification in Still Images (code)
  https://www.di.ens.fr/willow/research/stillactions/
  
[3.2] Human Action Classification on Stanford 40 dataset (code)
  https://github.com/dronefreak/human-action-classification

[3.3] Action Recognition from Still Images Based on Deep VLAD Spatial Pyramids
  https://livrepository.liverpool.ac.uk/3006517/1/Image4453R2.pdf

[4.1] 光流估计——从传统方法到深度学习
  https://zhuanlan.zhihu.com/p/74460341
  
[5.1.0] OpenMMD：没有专业摄像设备也能动作捕捉！K帧动作设计苦手的福音~
  https://www.bilibili.com/read/cv2835857
  
[5.1.0] OpenMMD represents the OpenPose-Based Deep-Learning
  https://github.com/peterljq/OpenMMD
  
[5.1.0] OpenMMD_V1.0
  https://pan.baidu.com/s/1L-TxEsgXD3zRHTAM8YQd7w#list/path=%2F&parentPath=%2Fsharelink3156223315-1050112483171531

[5.2.0]【一】如何快速使用视频生成MMD运动数据
  https://www.bilibili.com/read/cv3400259/
  
[6.1] HAKE-Action
  https://github.com/DirtyHarryLYL/HAKE-Action
  
[7.1] PaddleVideo 动作识别、时空动作检测、基于骨架的动作识别
  https://github.com/PaddlePaddle/PaddleVideo

------
Continual Learning in Human Activity Recognition: an Empirical Analysis of Regularization
https://paperswithcode.com/paper/continual-learning-in-human-activity
https://arxiv.org/abs/2007.03032v1
https://github.com/srvCodes/continual-learning-benchmark

PoseGU: 3D Human Pose Estimation with Novel Human Pose Generator and Unbiased Learning 2022
https://arxiv.org/abs/2207.03618

Exploring 3D Human Pose Estimation and Forecasting from the Robot's Perspective: The HARPER Dataset 2024
https://arxiv.org/abs/2403.14447

Synthesizing Training Images for Boosting Human 3D Pose Estimation
https://ai.stanford.edu/~haosu/papers/3DV_humanpose.pdf

Learning 3D Human Pose Estimation from Dozens of Datasets using a Geometry-Aware Autoencoder to Bridge Between Skeleton Formats 2022
https://arxiv.org/abs/2212.14474

A Spatio-temporal Transformer for 3D Human Motion Prediction
https://arxiv.org/abs/2004.08692

MovePose: A High-performance Human Pose Estimation Algorithm on Mobile and Edge Devices 2023,
452+ fps on an NVIDIA RTX3090 GPU, 暂无代码
https://arxiv.org/abs/2308.09084


## 视频数据集

###  DeepMind Kinetics human action videodatase
Kinetics-400 发布在 2017，Kinetics-700 是 2019.

Kinetics-700: 650,000 video clips that covers 700 human action classes, including human-object interactions such as playing instruments, as well as human-human interactions such as shaking hands and hugging. Each action class has at least 600 video clips. Each clip is human annotated with a single action class and lasts around 10s.
  
总量是 306245 个视频。

标记内容：label	youtube_id	time_start	time_end
 。

Kinetics challenge.There was a first Kinetics challenge atthe ActivityNet workshop in CVPR 2017, using Kinetics-400. 
The  second  challenge  occurred  at  the  ActivityNetworkshop in CVPR 2018, this time using Kinetics-600. 
The performance criterion used in the challenge is the average ofTop-1 and Top-5 error. There was an improvement between

the winning systems of the two challenges, with error get-ting down from 12.4% (in 2017) to 11.0% (in 2018) [1, 6].The 2019 challenge featured the new Kinetics-700 datasetand had 15 participating teams. The top team was from JDAI Research and obtained 17.9% error, considerably belowour baseline – a single RGB I3D model – which obtained29.3% error.

Kinetics-400 的训练使用了 64个GPU。

Kinetics-700 不同的类别的难度差异较大，I3D 模型测试最难的类精度小于 20%，最容易的大于90%。

The 2019 challenge featured the new Kinetics-700 datasetand had 15 participating teams. The top team was from JDAI Research and obtained 17.9% error, considerably belowour baseline – a single RGB I3D model – which obtained 29.3% error.

### UCF101
[1.3]
13320 videos from 101 action categories.

动作分类如下：

Apply Eye Makeup, Apply Lipstick, Archery, Baby Crawling, Balance Beam, Band Marching, Baseball Pitch, Basketball Shooting, Basketball Dunk, Bench Press, Biking, Billiards Shot, Blow Dry Hair, Blowing Candles, Body Weight Squats, Bowling, Boxing Punching Bag, Boxing Speed Bag, Breaststroke, Brushing Teeth, Clean and Jerk, Cliff Diving, Cricket Bowling, Cricket Shot, Cutting In Kitchen, Diving, Drumming, Fencing, Field Hockey Penalty, Floor Gymnastics, Frisbee Catch, Front Crawl, Golf Swing, Haircut, Hammer Throw, Hammering, Handstand Pushups, Handstand Walking, Head Massage, High Jump, Horse Race, Horse Riding, Hula Hoop, Ice Dancing, Javelin Throw, Juggling Balls, Jump Rope, Jumping Jack, Kayaking, Knitting, Long Jump, Lunges, Military Parade, Mixing Batter, Mopping Floor, Nun chucks, Parallel Bars, Pizza Tossing, Playing Guitar, Playing Piano, Playing Tabla, Playing Violin, Playing Cello, Playing Daf, Playing Dhol, Playing Flute, Playing Sitar, Pole Vault, Pommel Horse, Pull Ups, Punch, Push Ups, Rafting, Rock Climbing Indoor, Rope Climbing, Rowing, Salsa Spins, Shaving Beard, Shotput, Skate Boarding, Skiing, Skijet, Sky Diving, Soccer Juggling, Soccer Penalty, Still Rings, Sumo Wrestling, Surfing, Swing, Table Tennis Shot, Tai Chi, Tennis Swing, Throw Discus, Trampoline Jumping, Typing, Uneven Bars, Volleyball Spiking, Walking with a dog, Wall Pushups, Writing On Board, Yo Yo. 

### UCF101_24
UCF101_24 是基于 UCF101 的时空检测数据集。
动作分类被缩减到24个，并且只标注了部分视频、部分对象。

WalkingWithDog

Diving

PoleVault

SkateBoarding

CricketBowling

GolfSwing

Skijet

RopeClimbing

FloorGymnastics

Basketball

Biking

VolleyballSpiking

Fencing

CliffDiving

HorseRiding

SoccerJuggling

TennisSwing

LongJump

SalsaSpin

TrampolineJumping

IceDancing

Skiing

Surfing

BasketballDunk


### HMDB-51
[1.4]
每个视频2-3秒，1-2人。51个动作分类，如下：

brush_hair    dive        flic_flac  kick_ball  punch        shoot_ball  smile           sword  wave
cartwheel     draw_sword  golf       kick       push         shoot_bow   smoke           talk
catch         dribble     handstand      kiss       pushup       shoot_gun   somersault      throw
chew          drink       handstand  laugh      ride_bike              
clap          eat         hit        pick       ride_horse   sit         stand           turn
climb         fall_floor  hug        pour       run          situp           swing_baseball  walk
climb_stairs  fencing     jump       pullup     shake_hands  sword_exercise

与手部相关的是：

shake_hands clap

### JHMDB
[1.3.4]
JHMDB 是基于 HMDB 的时空检测数据集。

    JHMDB只标注了HMDB的一部分，21类只包括一个人的行为，也删除了一些这21类行为中人不明显的样本。21类每一类有36-55个样本，每个样本包括了行为的起始与终止时间，每个样本包括14-40帧。 JHMDB一共标注了31838张图片。

类别列表（21类）

sit
run
pullup
walk
shoot_gun
brush_hair
jump
pour
pick
kick_ball
golf
shoot_bow
catch
clap
swing_baseball
climb_stairs
throw
wave
shoot_ball
push
stand

每个视频最多只有一类目标行为，bbox只标了做目标行为的那几个人

### AVA
[1.7.1~1.7.2]

要下载，参考 [1.7.2]，数据已经从 youtube 复制到 amazon，国内可以直接下载。

动作分类：

bend/bow (at the waist)
crawl
crouch/kneel
dance
fall down
get up
jump/leap
lie/sleep
martial art
run/jog
sit
stand
swim
walk
answer phone
brush teeth
carry/hold (an object)
catch (an object)
chop
climb (e.g., a mountain)
clink glass
close (e.g., a door, a box)
cook
cut
dig
dress/put on clothing
drink
drive (e.g., a car, a truck)
eat
enter
exit
extract
fishing
hit (an object)
kick (an object)
lift/pick up
listen (e.g., to music)
open (e.g., a window, a car door)
paint
play board game
play musical instrument
play with pets
point to (an object)
press
pull (an object)
push (an object)
put down
read
ride (e.g., a bike, a car, a horse)
row boat
sail boat
shoot
shovel
smoke
stir
take a photo
text on/look at a cellphone
throw
touch (an object)
turn (e.g., a screwdriver)
watch (e.g., TV)
work on a computer
write
fight/hit (a person)
give/serve (an object) to (a person)
grab (a person)
hand clap
hand shake
hand wave
hug (a person)
kick (a person)
kiss (a person)
lift (a person)
listen to (a person)
play with kids
push (another person)
sing to (e.g., self, a person, a group)
take (an object) from (a person)
talk to (e.g., self, a person, a group)
watch (a person)


其中与手部动作相关的是：

carry/hold (an object)
catch (an object)
give/serve (an object) to (a person)
grab (a person)
point to (an object)
hand clap
hand shake
hand wave
hug (a person)
lift (a person)

### Charades
[1.8.1]

在 amazon 上，国内可以直接下载。


## 静态图像数据集

### PASCAL VOC 

动作分类（10类）：Jumping Phoning Playing instrument Reading Riding bike Riding horse Running Taking photo Using computer Walking。

### Willow-Actions

动作分类（与　PASCAL VOC）：Interacting with computer, Photographing, Playing Instrument, Riding Bike, Riding Horse, Running, Walking


### Human Action Classification on Stanford 40 dataset (code)
[3.2]

## 骨架行为数据集

### NTU+RGBD Dataset (NTU)
NTU+RGBD Dataset (NTU) [22]:This is the current-ly largest dataset for action recognition with more than 56thousand sequences and 4 million frames. 

下载地址[1.5.1] 百度网盘。

### SYSU-3D Dataset (SYSU) 

[43]:The SYSU-3D datasetcontains 480 sequences and 12 different actions performedby 40 persons.

### UT-Kinect Dataset (UT)

[44] This dataset includes 200 skeleton sequences with 20 skeleton joints per frame.

### PKU-MMD

[1.9] We collect 1000+ long action sequences, eachof which lasts about 3∼4 minutes (recording ratio set to 30FPS) and contains approximately 20 action instances.  Thetotal scale of our dataset is 5,312,580 frames of 3,000 min-utes with 20,000+ temporally localized actions.We choose 51 action classes in total, which are dividedinto  two  parts:   41  daily  actions  (drinking,  waving  hand,putting on the glassed,etc.) and 10 interaction actions (hug-ging, shaking hands,etc.).

行为分类：

1	bow					
2	brushing hair					
3	brushing teeth					
4	check time (from watch)					
5	cheer up					
6	clapping					
7	cross hands in front (say stop)					
8	drink water					
9	drop					
10	eat meal/snack					
11	falling					
12	giving something to other person					
13	hand waving					
14	handshaking					
15	hopping (one foot jumping)					
16	hugging other person					
17	jump up					
18	kicking other person					
19	kicking something					
20	make a phone call/answer phone					
21	pat on back of other person					
22	pickup					
23	playing with phone/tablet					
24	point finger at the other person					
25	pointing to something with finger					
26	punching/slapping other person					
27	pushing other person					
28	put on a hat/cap					
29	put something inside pocket					
30	reading					
31	rub two hands together					
32	salute					
33	sitting down					
34	standing up					
35	take off a hat/cap					
36	take off glasses					
37	take off jacket					
38	take out something from pocket					
39	taking a selfie					
40	tear up paper					
41	throw					
42	touch back (backache)					
43	touch chest (stomachache/heart pain)					
44	touch head (headache)					
45	touch neck (neckache)					
46	typing on a keyboard					
47	use a fan (with hand or paper)/feeling warm					
48	wear jacket					
49	wear on glasses					
50	wipe face					
51	writing					


## 典型行为识别方法

### Two-Stream Inflated 3D ConvNets(I3D) model (2017)
Kinetics-700 的训练使用了 32个 P100 GPU。[1.1.2]

very deep image classification networks, such as Inception [13], VGG-16 [26] and ResNet [12], can be trivially inflated into spatio-temporal feature extractors, and that their pre-trained weights provide a valuable initializa-tion. 

starting with a 2D architecture, andinflatingall the filtersand pooling kernels – endowing them with an additionaltemporal dimension. Filters are typically square and we justmake them cubic –N×N filters becomeN×N×N.

使用 pre-trained ImageNet model（Inception-V1）: 把静态图像转换为静止视频（boring video），那么输出结果应该不变。

引入两个3D流：RGB 输入和光流输入。

性能：miniKinetics 78.7%

This demonstrates transfer learning from onedataset (Kinetics) to another dataset (UCF-101/HMDB-51)

### ConvNets with an LSTM on top (2014)

ConvNets 用来给每一帧做图像分类，LSTM （长短期记忆）用来加入一个循环层，使其具有分辨时序的能力。单独用 ConvNets 然后池化不能分辨开门/关门的区别。[1.1.1]

ConvNets 的具体实现是 ResNet50（2015）和 ResNet101。ResNet50 可以用作预训练模型，也可以从头开始，看输入数据集够不够大。[1.1.1]

缺点是效率较低，丢失底层运动细节。

### two-stream networks (2014)
双流是指：位置和速度信息。后者见 [4.1]。

使用两个 ImageNet-pretrained ConvNet。[1.1.1]

性能和效率都比较好。[1.1.1]

### 3D ConvNet (2013)
二维卷积网络加上 spatio-temporal filters 。[1.1.1]

参数过多，训练较困难。无法利用 ImageNet pre-training 。[1.1.1]

性能还没有达到最佳，但有希望。[1.1.1]

### HAKE-Action

HAKE-Action (TensorFlow) is a project to open the SOTA action understanding studies based on our Human Activity Knowledge Engine. [6.1]

## 骨架行为识别
骨架行为识别方法分为两大类：hand-crafted feature based and deep learning feature based.

手写
  Lie group  Lie algebra
  Spatio-Temporal Naive-Bayes Nearest-Neighbor (NBNN) 
  undirected complete graph representation
  covariance matrices of joint trajectories
  relative positions of joints
  rotations and translations between body parts
深度学习
  RNN
  CNN
    转为柱坐标
    转为彩色位图
    two-stream CNN
    temporal CNN

骨架行为识别可以用 RNN 或 CNN 实现。

RNN-based model has the capability to model the temporaldependency, but it is difficult to train the stacked RNN inpractice

CNN-based model, which captures the relationship of neighboring framesat lower layers and long-term dependency at higher layer-s [18], is more effective and obtains promising performancerecently

CNN 接收关节在欧式空间中的坐标，但忽略了其拓扑关系。

将骨架表示成图，顶点坐标表示关节点，邻接矩阵表示关节之间的关系，则可以用 graph-based convolutional neural network (GCNN) 来进行学习。

### Deep Progressive Reinforcement Learning for Skeleton-based Action Recognition 2018
[2.1]

用双 GPU 机器完成 NTU+RGBD 等的训练。

NTU 数据集性能在 83%-89%，SYSU 76%，UT 98%。

### Spatial-Temporal Graph Convolutional Networks (ST-GCN) 2018
au-tomatically learning both the spatial and temporal patternsfrom data.

Graph Convolutional Network 是 CNNs 的一般化形式，不仅包含坐标，还包含连接信息。

顶点是关节，边有两类：空间边和时间边。

不需要预先指定关节的名字。

训练使用了 8 TITANX GPUs，数据集包括 Kinetics 和 NTU+RGBD 。

Kinetics 性能 71%，不如 optical flow 模型。

### SKELETON-BASED ACTION RECOGNITION WITH CONVOLUTIONAL NEURAL
 NETWORKS
[2.6.1]
在 NTU RGB+D 上性能 89.3%，在 PKU-MMD 上 93.7% mAP。数据集上训练。


### Human Action Classification on the Stanford 40 dataset

单帧、骨架行为识别。

For binary classification of poses, (sitting or upright), MobileNet (a CNN originally trained on the ImageNet Large Visual Recognition Challenge dataset), was retrained (final layer) on a dataset of ~1500 images of poses. For classifying scenes around the person in consideration, we retrain Inception-v3 architecture on the Stanford 40 dataset (9523 images), which incorporates 40 different day-to-day activites.

可以加入自己的图像来进行训练。
