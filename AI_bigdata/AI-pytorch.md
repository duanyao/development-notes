## 参考资料
[1.1] pytorch
http://pytorch.org/

[1.2] pytorch 各种版本
https://pytorch.org/get-started/previous-versions/

[1.3] How to tell PyTorch which CUDA version to take?
https://stackoverflow.com/questions/66116155/how-to-tell-pytorch-which-cuda-version-to-take

[2.1] PyTorch深度学习：60分钟入门(Translation)
https://zhuanlan.zhihu.com/p/25572330
http://pytorch.org/tutorials/beginner/blitz/cifar10_tutorial.html

[2.2] PyTorch Examples
https://github.com/pytorch/examples

[2.3] pytorchbook.cn

[3.1] CUDA SDK
https://developer.nvidia.com/cuda-downloads

[3.2]
https://github.com/pytorch/pytorch/issues/686

[3.3] 
https://developer.nvidia.com/cuda-gpus

[3.4] making .cuda() falls back to an identity function when gpu is not available
https://github.com/pytorch/pytorch/issues/3600

[3.5] Device-agnostic code
https://github.com/pytorch/pytorch/issues/3202

[3.6]  soumith / fakecuda 
https://github.com/soumith/fakecuda

[3.7] Easy way to switch between CPU and cuda
https://github.com/pytorch/pytorch/issues/1668

[4.1] The CIFAR-10 dataset
https://www.cs.toronto.edu/~kriz/cifar.html

[5.1] Best way to save a trained model in PyTorch?
https://stackoverflow.com/questions/42703500/best-way-to-save-a-trained-model-in-pytorch

[6.1] torch.utils.tensorboard
https://pytorch.org/docs/stable/tensorboard.html

[6.2] 使用Tensorboard在 PyTorch 中进行可视化
https://pytorchbook.cn/chapter4/4.2.2-tensorboardx/

[7.1] 可视化理解卷积神经网络
https://pytorchbook.cn/chapter4/4.2.3-cnn-visualizing/

[8.1] Pytorch：model.train()和model.eval()用法和区别，以及model.eval()和torch.no_grad()的区别
https://zhuanlan.zhihu.com/p/357075502

[9.1] 随机性 Reproducibility
https://pytorch.org/docs/stable/notes/randomness.html

[10.1] Performance Tuning Guide
https://pytorch.org/tutorials/recipes/recipes/tuning_guide.html

[10.2] Set the Number of Threads to Use in PyTorch
https://jdhao.github.io/2020/07/06/pytorch_set_num_threads

[10.3] CPU threading and TorchScript inference
https://pytorch.org/docs/stable/notes/cpu_threading_torchscript_inference.html

[11.0] 测试
[11.1] testing
https://pytorch.org/docs/stable/testing.html

[12.1] 部署方案： ExecuTorch
https://pytorch.org/executorch/stable/intro-overview.html

[13.1] torch.export Tutorial
https://pytorch.org/tutorials/intermediate/torch_export_tutorial.html

[14.1] torch.onnx
https://pytorch.org/docs/main/onnx.html
[14.2] TorchDynamo-based ONNX Exporter
https://pytorch.org/docs/main/onnx_dynamo.html
[14.3] TorchScript-based ONNX Exporter
https://pytorch.org/docs/main/onnx_torchscript.html



## 安装

### venv PIP 安装（推荐）

venv 的使用请参考 python 。

主 pip 仓库安装：
```
python3 -m venv ~/opt/venv/e1  # 创建
source ~/opt/venv/e1/bin/activate  # 激活
pip install torch
pip install torchvision
```
不指定版本时，torch、torchvision 一般是针对最新 cuda 编译的，例如目前（2025.1）是 cu121。但安装后cuda版本号不会体现在torch版本号中。

要指定cuda版本号，参考[1.2]，用以下命令安装，cu118 表示 cuda11.8：

```
pip install torch==2.4.0+cu118 torchvision==0.19.0+cu118 --extra-index-url https://download.pytorch.org/whl/cu118
```

从 torch 1.9（2021.6.28） 到 2.1.2（2023.12）的版本，可以支持 python 3.9，且无需区分 cuda 或 CPU 版本，此 torch 也能在 cuda 上工作。

不过，上面的方法无法指定 torch 依赖的 cuda / cudnn 版本，不能确保其在任意版本的 cuda /cudnn 系统上工作，运行时可能会遇到 `CUDNN_STATUS_NOT_INITIALIZED` 错误。

这时最好查看 https://download.pytorch.org/whl/torch_stable.html 上的版本，并选择一个版本安装：

```
# 或者 torch==1.10.2+cu111 torchvision==0.11.2+cu111
pip install torch==1.10.0+cu113 -f https://download.pytorch.org/whl/torch_stable.html
pip install torchvision==0.11.1+cu113 -f https://download.pytorch.org/whl/torch_stable.html

pip install torch==1.13.1+cu116 torchvision==0.14.1+cu116 -f https://download.pytorch.org/whl/torch_stable.html

pip install torch-2.1.1==cu118 torchvision==0.16.1+cu118 -f https://download.pytorch.org/whl/torch_stable.html
```
torch 包的 cuda 版本可以低于系统实际安装的 cuda 版本，但不能高于它。

### conda 安装（不推荐）

安装 miniconda
从这里下载 Miniconda2-latest-Linux-x86_64.sh https://conda.io/miniconda.html

#### 全局安装（不建议）
sudo sh  Miniconda2-latest-Linux-x86_64.sh
选择安装位置：/opt/miniconda2/
安装 pytorch 和 cuda80
sudo /opt/miniconda2/bin/conda install pytorch cuda80 -c soumith

#### 当前用户安装
sh  Miniconda3-latest-Linux-x86_64.sh
选择安装位置：~/miniconda3/
安装 pytorch 和 cuda80
~/miniconda3/bin/conda install pytorch cuda80 -c soumith

下载的包在 /home/duanyao/miniconda3/pkgs
可能很大。

https://conda.anaconda.org/soumith/linux-64/pytorch-0.2.0-py36h53baedd_4cu80.tar.bz2

不过下载容易失败。


### virtualenv PIP 安装

安装文件：http://pytorch.org/previous-versions/

wget -c http://download.pytorch.org/whl/cu80/torch-0.2.0.post3-cp35-cp35m-manylinux1_x86_64.whl

cp35 是指 python 版本。用 wget 是为了防止下载大文件失败。

sudo apt-get install python3-virtualenv
virtualenv -p python3 pytorch1
cd pytorch1/
bin/pip install ~/path/to/torch-0.2.0.post3-cp35-cp35m-manylinux1_x86_64.whl

从网络仓库装其它的 pip 包：

bin/pip install torchvision
bin/pip install matplotlib

图形界面库：
sudo apt-get install python3-tk

### 测试
bin/python #当前virtualenv目录下的python

```
import torch
x = torch.tensor([[1,2,3],[4,5,6]])  # 构造一个用python数组初始化的矩阵
x = torch.rand(5, 3)  # 构造一个随机初始化的矩阵
x # 此处在notebook中输出x的值来查看具体的x内容
x.size()
x.to('cuda') # 测试 GPU
```


### 安装 cuda
参照 nvidia-linux.md 安装 cuda 。torch 不依赖 cudnn 。
torch 并不依赖于特定的 cuda 版本。

安装后，在python里用

```
python -c "import torch;x = torch.Tensor(5, 3);x.cuda()"
```
可以将数据复制到GPU上。
GPU上的数据打印时会输出类似这样的一行：
```
[torch.cuda.FloatTensor of size 5x3 (GPU 0)]
```
而内存里的则是：

```
[torch.FloatTensor of size 5x3]
```
不过，pytorch 支持的最低 CUDA Capability 是 3.0，比较老的 nv 显卡是不支持的，例如 540M 是 2.1。相比之下，750M 是 3.0，1080是6.1。

此外，可以用
```
import torch
torch.cuda.is_available()
```
来验证 torch + cuda是否可用。如果不可用，返回 False ，且可能显示 "CUDA initialization: CUDA unknown error" 等错误信息。


## 从 cuda 回退到 CPU

原则上，让 .cuda() 方法什么都不做即可回退到 CPU。

检测 cuda 的存在再回退[3.4]：

```
def cudaize(obj):
    return obj.cuda() if torch.cuda.is_available() else obj
```

另一个方法是用 .to() 代替 cuda() ，以及指定 device 参数[3.7]:

```
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

obj.to(device) # obj.cuda()

t = torch.tensor(range(15), device=device)

model = model.to(x_tensor.device) # model = model.cuda(x_tensor)
```

fakecuda [3.6] 通过动态修改 torch 的代码，让 .cuda() 变成无用的操作。不过 fakecuda 是 lua 写的。

## pypy 兼容性

与 pypy 的兼容性：当前（2021.8）的最新版，pytorch 与 pypy-3.7-7.3.5 是不兼容的 。但 pytorch 的开发在推进：https://github.com/pytorch/pytorch/issues/17835 。


## 数组/张量

```
>>> torch.zeros([2, 4], dtype=torch.int32)
tensor([[ 0,  0,  0,  0],
        [ 0,  0,  0,  0]], dtype=torch.int32)

>>> cuda0 = torch.device('cuda:0')
>>> torch.ones([2, 4], dtype=torch.float64, device=cuda0)
tensor([[ 1.0000,  1.0000,  1.0000,  1.0000],
        [ 1.0000,  1.0000,  1.0000,  1.0000]], dtype=torch.float64, device='cuda:0')
        
data = [[1, 2],[3, 4]]
x_data = torch.tensor(data)

x16=x.to(dtype=torch.float16)

np_array = np.array(data)
x_np = torch.from_numpy(np_array)
```

## 数据集/数据加载器

### ImageFolder

目录结构：
```
../data/hotdog/
    train/
        hotdog/
            0.png
            999.png
        not-hotdog/
            0.png
            999.png
    test/
        hotdog/
        not-hotdog/
```
hotdog 和 not-hotdog 目录被当作分类的名字。

加载图片：
```
data_dir = '../data/hotdog'
train_imgs = torchvision.datasets.ImageFolder(os.path.join(data_dir, 'train'))
test_imgs = torchvision.datasets.ImageFolder(os.path.join(data_dir, 'test'))

len(train_imgs) # 图片的数量，2000

train_imgs.classes # ['hotdog', 'not-hotdog']
train_imgs.class_to_idx # {'hotdog': 0, 'not-hotdog': 1}
train_imgs[1500] # (<PIL.Image.Image image mode=RGB size=414x628 at 0x7FD6528DC9D0>, 1)
train_imgs[0] # (<PIL.Image.Image image mode=RGB size=122x144 at 0x7FD6528DCCD0>, 0)
train_imgs.imgs[0] # ('../data/hotdog/train/hotdog/0.png', 0)

hotdogs = [train_imgs[i][0] for i in range(8)]
not_hotdogs = [train_imgs[-i - 1][0] for i in range(8)]
d2l.show_images(hotdogs + not_hotdogs, 2, 8, scale=1.4);
```

每个分类名被映射到一个索引号（class_to_idx），从0开始。排序似乎是按字符串升序。

### DataLoader

```
data_dir = '../data/hotdog'

# 使用三个RGB通道的均值（参数1）和标准偏差（参数2），以标准化每个通道
# 这些数值的来源见： https://pytorch.org/vision/0.8/models.html#classification
# 如果使用 torch 自带的预训练模型，应当使用这些数值。如果自己从头训练，可以有所不同。
normalize = torchvision.transforms.Normalize([0.485, 0.456, 0.406],
                                             [0.229, 0.224, 0.225])

train_augs = torchvision.transforms.Compose([
    torchvision.transforms.RandomResizedCrop(224),
    torchvision.transforms.RandomHorizontalFlip(),
    torchvision.transforms.ToTensor(), normalize])

test_augs = torchvision.transforms.Compose([
    torchvision.transforms.Resize(256),
    torchvision.transforms.CenterCrop(224),
    torchvision.transforms.ToTensor(), normalize])

train_iter = torch.utils.data.DataLoader(
        torchvision.datasets.ImageFolder(os.path.join(data_dir, 'train'),
                                         transform=train_augs),
        batch_size=batch_size, shuffle=True)
test_iter = torch.utils.data.DataLoader(
    torchvision.datasets.ImageFolder(os.path.join(data_dir, 'test'),
                                      transform=test_augs),
    batch_size=batch_size)
    
len(train_iter) # 63，批次数，ceil(sample_count/batch_size)

features, labels = next(iter(train_iter)) # 转化为迭代器，然后访问一个批次。features 是 Tensor<batch_size, C, H, W>， labels 是 Tensor<batch_size, class_count>。

for i, (features, labels) in enumerate(train_iter): # 循环访问批次
    # do train
```

DataLoader 不能当作 list 或 iter，但可以转化为 iter，也可以 enumerate() 。DataLoader 转化为 list 虽然可行，但不建议，因为这会导致整个数据集被加载到内存里。

## 训练可视化（tensorboard）
[6.1-6.2]
tensorboard 最初是配套 tensorflow 的训练可视化工具，它读取特定格式的训练日志并显示出来。pytorch 1.1 以后也可以输出 tensorboard 格式的训练日志，因此也可以用 tensorboard 来可视化 pytorch 的训练。
训练日志中的内容需要客户代码写入，具体内容没有规定，可以包括x-y散点图等。

tensorboard 本体用 pip 安装即可： `pip install tensorboard` 。

tensorboard 可视化的数据类型包括[6.2]：

SCALAR

对标量数据进行汇总和记录，通常用来可视化训练过程中随着迭代次数准确率(val acc)、损失值(train/test loss)、学习率(learning rate)、每一层的权重和偏置的统计量(mean、std、max/min)等的变化曲线
IMAGES

可视化当前轮训练使用的训练/测试图片或者 feature maps
GRAPHS

可视化计算图的结构及计算图上的信息，通常用来展示网络的结构
HISTOGRAMS

可视化张量的取值分布，记录变量的直方图(统计张量随着迭代轮数的变化情况）
PROJECTOR

全称Embedding Projector 高维向量进行可视化

```
from torch.utils.tensorboard import SummaryWriter

def train_ch13(net, train_iter, test_iter, loss, trainer, num_epochs,
               devices=d2l.try_all_gpus()):
    
    tb_writer = SummaryWriter(comment='train_fine_tuning') # 默认写入到 './runs' 目录，此目录作为参数传给 
    
    timer, num_batches = d2l.Timer(), len(train_iter)

    net = nn.DataParallel(net, device_ids=devices).to(devices[0])
    for epoch in range(num_epochs):
        # Sum of training loss, sum of training accuracy, no. of examples,
        # no. of predictions
        metric = d2l.Accumulator(4)
        epoch_100 = 0 # epoch 的 100 倍，作为进度的衡量。
        for i, (features, labels) in enumerate(train_iter):
            epoch_100 = round(100 * (epoch + (i + 1) / num_batches))
            timer.start()
            l, acc = d2l.train_batch_ch13(net, features, labels, loss, trainer,
                                      devices)
            metric.add(l, acc, labels.shape[0], labels.numel())
            timer.stop()
            if (i + 1) % (num_batches // 5) == 0 or i == num_batches - 1:
                if tb_writer is not None:
                    sum_train_loss, sum_train_acc, sample_count, _ = metric
                    tb_writer.add_scalars('train',
                        {'train_loss': sum_train_loss / sample_count, 'train_acc': sum_train_acc / sample_count },
                        epoch_100
                    )
        test_acc = d2l.evaluate_accuracy_gpu(net, test_iter)
        animator.add(epoch + 1, (None, None, test_acc))
        if tb_writer is not None:
            tb_writer.add_scalar('test_acc', test_acc, epoch_100)
            tb_writer.flush() # 刷新到文件。默认是 120 秒刷新一次。
    tb_writer.close() # 关闭日志文件。
```

`SummaryWriter.add_scalar(tag: str, value: float, step: int)` 用来添加一个数据点， `add_scalars(tag: str, subtag_value_map: dict, step: int)` 用来添加多个数据点。

SummaryWriter 还可以添加图像到日志，但现在还不清楚有何用处。

SummaryWriter 默认写入到 './runs' 目录，此目录作为参数传给 tensorboard 命令即可，例如：

```
tensorboard --logdir ./runs/
```
tensorboard 默认监听 6006 端口，用浏览器访问即可： `http://localhost:6006/`。

## 可视化理解卷积神经网络
[7.1] 

## 算例/数据集
### CIFAR-10 
60000 32x32 colour images in 10 classes, with 6000 images per class. There are 50000 training images and 10000 test images [4.1]. 

大约 170M（压缩后）。

trainset = torchvision.datasets.CIFAR10(root='./data', train=True, download=True, transform=transform)


### CIFAR-10 分类实验
[2.1]

```
from __future__ import print_function
import numpy as np
import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import matplotlib


# 装载和归一化图像数据

transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
trainset = torchvision.datasets.CIFAR10(root='./data', train=True,
                                        download=True, transform=transform)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=4,
                                          shuffle=True, num_workers=2)
testset = torchvision.datasets.CIFAR10(root='./data', train=False,
                                       download=True, transform=transform)
testloader = torch.utils.data.DataLoader(testset, batch_size=4,
                                         shuffle=False, num_workers=2)
classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')


# functions to show an image

def imshow(img):
    img = img / 2 + 0.5     # unnormalize
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))


# get some random training images
dataiter = iter(trainloader)
images, labels = dataiter.next()
# show images
imshow(torchvision.utils.make_grid(images))
# print labels
print(' '.join('%5s' % classes[labels[j]] for j in range(4)))


plt.show() # 在GUI中显示结果。仅仅执行 plt.imshow() 是不够的。



#Define a Convolution Neural Network

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5) # 6 是宽度，增加宽度对精度影响不大
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)
    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 5 * 5)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

net = Net()

#Define a Loss function and optimizer

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

#Train the network. 可以重复运行几次，loss 值会逐渐下降

for epoch in range(2):  # loop over the dataset multiple times
    running_loss = 0.0
    for i, data in enumerate(trainloader, 0):
        # get the inputs
        inputs, labels = data
        # wrap them in Variable
        inputs, labels = Variable(inputs), Variable(labels)
        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
        # print statistics
        running_loss += loss.data[0]
        if i % 2000 == 1999:    # print every 2000 mini-batches
            print('[%d, %5d] loss: %.3f' %
                  (epoch + 1, i + 1, running_loss / 2000))
            running_loss = 0.0


#Test the network on the test data
dataiter = iter(testloader)
images, labels = dataiter.next()

# print images
imshow(torchvision.utils.make_grid(images))
print('GroundTruth: ', ' '.join('%5s' % classes[labels[j]] for j in range(4)))
plt.show()

# see what the neural network thinks these examples above are:
outputs = net(Variable(images))
_, predicted = torch.max(outputs.data, 1)
print('Predicted: ', ' '.join('%5s' % classes[predicted[j]]
                              for j in range(4)))


#how the network performs on the whole dataset.
correct = 0
total = 0
for data in testloader:
    images, labels = data
    outputs = net(Variable(images))
    _, predicted = torch.max(outputs.data, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()

print('Accuracy of the network on the 10000 test images: %d %%' % (
    100 * correct / total))
    
#what are the classes that performed well

class_correct = list(0. for i in range(10))
class_total = list(0. for i in range(10))
for data in testloader:
    images, labels = data
    outputs = net(Variable(images))
    _, predicted = torch.max(outputs.data, 1)
    c = (predicted == labels).squeeze()
    for i in range(4):
        label = labels[i]
        class_correct[label] += c[i]
        class_total[label] += 1


for i in range(10):
    print('Accuracy of %5s : %2d %%' % (
        classes[i], 100 * class_correct[i] / class_total[i]))


#保存模型[5.1]

torch.save(net.state_dict(), 'CIFAR-10.2.pytm')

#加载模型：
net.load_state_dict(torch.load('CIFAR-10.2.pytm'))
net.eval()

```

训练运行一次，正确率54%，运行3-4次可提高到63% 。增加 .conv1 和 .conv2 的宽度对正确率的影响不大。

保存 net.state_dict() 可以用于推断，但不能用于继续训练，继续训练还需要保存更多的状态，例如：

```
state = {
    'epoch': epoch, # 不知道是什么？
    'state_dict': net.state_dict(),
    'optimizer': optimizer.state_dict(),
    ...
}
torch.save(state, filepath)
```
恢复：
```
state = torch.load(filepath)
net.load_state_dict(state['state_dict'])
optimizer.load_state_dict(stata['optimizer'])
```
保存的模型并不大，CIFAR-10.2.pytm 大约 253KB。

### train ResNet in torchvision

https://pytorch.org/blog/how-to-train-state-of-the-art-models-using-torchvision-latest-primitives/
copy from: ClassificationPresetTrain, torchvision.git/references/classification/presets.py,  (0.17, 2023.12)

## 多线程、多进程控制

### 多线程
torch 的许多操作，如图像预处理，一般是在 CPU 上执行的，torch 会自动利用多线程加速处理。torch 多线程有两种机制，一是不同的操作使用不同的线程（Inter-op parallelism），二是同一个操作利用多线程（Intra-op parallelism）。
这两种机制分别使用至少一个线程池，所以总体线程数是两者相加，而总体并行度一般是两者中的较小者。两者分别用以下API来设置和查看：

```
# Intra-op parallelism
torch.get_num_threads()
torch.set_num_threads(n_thread)

#Inter-op parallelism
torch.get_num_interop_threads()
torch.set_num_interop_threads(n_thread)
```
`set_num_threads()` 可以在运行期间重新设置，及时生效。`set_num_interop_threads()` 一般只能在首次计算前设置一次，否则可能抛出异常。

此外，可以查看整体的线程数配置：
```
print(torch.__config__.parallel_info())
ATen/Parallel:
	at::get_num_threads() : 1
	at::get_num_interop_threads() : 12
OpenMP 201511 (a.k.a. OpenMP 4.5)
	omp_get_max_threads() : 1
Intel(R) Math Kernel Library Version 2020.0.0 Product Build 20191122 for Intel(R) 64 architecture applications
	mkl_get_max_threads() : 1
Intel(R) MKL-DNN v2.2.3 (Git Hash 7336ca9f055cf1bfa13efb658fe15dc9b41f0740)
std::thread::hardware_concurrency() : 24
Environment variables:
	OMP_NUM_THREADS : [not set]
	MKL_NUM_THREADS : [not set]
ATen parallel backend: OpenMP
```
目前(torch 1.10)中 `torch.get_num_threads() torch.get_num_interop_threads()` 都是映射到 ATen/Parallel 库，并且利用 OpenMP 实现。`at::get_num_threads()` 也映射到 `omp_get_max_threads()`，`mkl_get_max_threads()`。

在不设置的情况下，torch 的 Inter-op 和 Intra-op 的线程数都是 CPU 物理核心数。

```
import time
import os
import numpy as np
import torch

def run_thread_test(n_thread: int):
  # os.environ['OM_NUM_THREADS'] = '8'
  torch.set_num_threads(n_thread)
  # torch.set_num_interop_threads(n_thread + 1)

  INDEX = 10000
  NELE = 1000
  a = torch.rand(INDEX, NELE)
  index = np.random.randint(INDEX-1, size=INDEX*8)
  b = torch.from_numpy(index)

  start = time.time()
  for _ in range(10):
      res = a.index_select(0, b)
  print("num_threads: {}, num_interop_threads: {}, time: {}".format(torch.get_num_threads(), torch.get_num_interop_threads(), time.time()-start))

torch.set_num_interop_threads(6)
for n in [1, 2, 4, 8, 12, 16, 24, 48, 96]:
   run_thread_test(n)
```

## 显存使用量控制
指定一个进程的最大占用比例：`torch.cuda.set_per_process_memory_fraction(fraction, device=None) `

fraction (float) – Range: 0~1. Allowed memory equals total_memory * fraction.
device (torch.device or int, optional) – selected device. If it is None the default CUDA device is used.

释放未使用的显存：`torch.cuda.empty_cache()`

例子：
```
import torch
torch.cuda.set_per_process_memory_fraction(0.5, 0)
torch.cuda.empty_cache()
total_memory = torch.cuda.get_device_properties(0).total_memory
# less than 0.5 will be ok:
tmp_tensor = torch.empty(int(total_memory * 0.499), dtype=torch.int8, device='cuda')
del tmp_tensor
torch.cuda.empty_cache()
# this allocation will raise a OOM:
torch.empty(total_memory // 2, dtype=torch.int8, device='cuda')

"""
It raises an error as follows: 
RuntimeError: CUDA out of memory. Tried to allocate 5.59 GiB (GPU 0; 11.17 GiB total capacity; 0 bytes already allocated; 10.91 GiB free; 5.59 GiB allowed; 0 bytes reserved in total by PyTorch)
"""
```

torch 对于 set_per_process_memory_fraction() 的实现是自定义的，参考 https://github.com/pytorch/pytorch/pull/48172/files

## 预测
预测时，使用模型的方式与训练时有2点不同[8.1]：
1. 加载模型后，要调用 `model.eval()` 将模型置于预测模式（默认是训练模式，用 model.train() 设置为训练模式）。不这样做，可能导致预测结果不对。这是因为模型中一些操作仅在训练模式启用，如 DropOut、BatchNormalization，预测模式应当关闭。model.eval() 会关闭这些操作。
2. 在 `with torch.no_grad()` 块中执行预测。其作用是停止autograd模块的工作，以起到加速和节省显存的作用，但这不影响结果的正确性。`model.eval()` 不能停止autograd模块的工作。

注意，导出 onnx 文件的时候，也要调用 `model.eval()`，否则预测结果也是不对的。

## 随机性 Reproducibility randomness
不论训练还是预测，都可能出现随机性，即使随机数种子设置是相同的。这源于部分 CUDA 算子本身是有随机性的，而且没有非随机性的替代版本。
 
使用以下方法可以尽可能的选择非随机性算子，如果不存在，则会抛出异常[9.1]。
```
import torch
torch.manual_seed(0)
torch.use_deterministic_algorithms(True)
```
同时应设置 CUDA 环境变量 `CUBLAS_WORKSPACE_CONFIG=":4096:8"` 。

## 测试
[11.0]
### assert_close
[11.1]
用来测试浮点数之间是否接近。同时考虑绝对偏差和相对偏差。
```
torch.testing.assert_close(actual, expected, *, allow_subclasses=True, rtol=None, atol=None, equal_nan=False, check_device=True, check_dtype=True, check_layout=True, check_stride=False, msg=None)
```
计算公式是：
`∣actual−expected∣≤atol+rtol⋅∣expected∣`

关于 atol 和 rtol ，assert_close 对不同数值格式各有一组默认值，在 1E-3 到 1E-6 量级。 

## 部署方案 ExecuTorch
[12.1]
ExecuTorch 用于在小型设备上部署 torch 模型。目前可用的执行方式（后端）是 XNNPACK （ X86 或 ARM CPU）或 Vulkan（支持 Vulkan 的 GPU）。

## 部署方案 torch.export
[13.1]

## 部署方案 torch.onnx
[14.1-14.3]
torch.onnx 有两个实现：
TorchDynamo-based[14.2]，即 `torch.onnx.dynamo_export()` 使用 Python frame evaluation API，支持分支结构（if），基本上没有限制。
TorchScript-based[14.3]，即 `torch.onnx.export()` 采用 JIT 追踪法，不支持分支结构（if）。

