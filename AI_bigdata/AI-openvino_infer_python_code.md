## 参考资料

[1.1] openvino infer in Paddle2ONNX
  https://github.com/PaddlePaddle/Paddle2ONNX/blob/release/0.9/experimental/openvino_ppdet_cn.md

[3.2] Maximize CPU Inference Performance with Improved Threads and Memory Management in Intel Distribution of OpenVINO Toolkit 
  https://www.edge-ai-vision.com/2020/03/maximize-cpu-inference-performance-with-improved-threads-and-memory-management-in-intel-distribution-of-openvino-toolkit/

[3.3] How do I do async inference on OpenVino
  https://stackoverflow.com/questions/56643774/how-do-i-do-async-inference-on-openvino

## 例子

来自 https://github.com/PaddlePaddle/Paddle2ONNX/blob/release/0.9/experimental/openvino_ppdet_cn.md
模型为 yolov3 。

### 模型加载

```
from openvino.inference_engine import IECore

ie = IECore()

# xml_file 和 bin_file 分别是 openvino 模型的 xml 和 bin 文件的路径。net: openvino.inference_engine.ie_api.IENetwork

net = ie.read_network(model=xml_file, weights=bin_file) 

# 批量
net.batch_size = 1
network_config = dict()

if device == "MYRIAD":
    network_config = {'VPU_HW_STAGES_OPTIMIZATION': 'NO'}

# CPU 上，device_name== 'CPU'，config == None
self.exec_net = ie.load_network(network=net, device_name=device, config=network_config)
```

### 图像预处理

```
# 读图和解码。此处使用 opencv，输出格式为 HWC 的 numpy 张量，颜色一般为 BGR888 格式：

im = cv2.imread(image_file)

# 颜色通道通道转置。可选，是否需要这一步，取决于模型规定的输入颜色通道顺序是不是 BGR：

im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

# 缩放。目标尺寸取决对于模型规定的输入图像尺寸。缩放时是否保持高宽比都是可行的，但对精度有一定影响，影响取决于具体的模型：

im, scale_h, scale_w = resize(im, self.model_input_shape[0], self.model_input_shape[1])
                                      
# 颜色正规化（ mean 和 std 为三元向量，mean 在 0~1 ，std 在 0~1。c = (ci/255 - mean / std) ，ci 为 RGB 值之一，在 0~255，结果 c 可正可负 ）：

im = normalize(im, self.mean, self.std)

# 轴转置（ 从 opencv image 的 HWC 格式变成 CHW 格式）。是否需要这一步取决于具体模型。：

im = im.transpose((2, 0, 1))

# 批量打包，把多个 image 打包到一个张量里，形成 NCHW 格式。但此处批量为1：

im = np.expand_dims(im, axis=0)

# 附加元数据：

inputs = dict()
inputs["image"] = im
inputs["im_shape"] = np.array(im.shape[2:]).astype("float32") # 缩放后的图像尺寸 HW
inputs["scale_factor"] = np.array([scale_h, scale_w]).astype("float32") # 图像尺寸缩放因子
```

### 推理

```
# 推理。exec_net: openvino.inference_engine.ie_api.ExecutableNetwork ，inputs: { image, im_shape, scale_factor }
results = exec_net.infer(inputs=input_dict)
```

results 为 dict ，内容是：
```
{
  'TopK_0.0': numpy.ndarray[(100,)],
  'TopK_3.0': numpy.ndarray[(100,)],
  'translated_layer/scale_0.tmp_0': numpy.ndarray[(100, 6)],
  'translated_layer/scale_1.tmp_0': numpy.ndarray[(1,)],
}
```

其中，numpy.ndarray[(100, 6)] 这项为输出的检测框。框数量 N==100，框定义为 [类别ID，置信度Score, 左上角xmin, 左上角ymin，右下角xmax, 右下角ymax]，坐标为绝对值 。

### 后处理

```
# 找出 results 中表示检测框的字段，特征是2轴、第二轴为6元。

boxes = None
for k, v in results.items():
    if len(v.shape) == 2 and v.shape[1] == 6:
        boxes = v
        break

# 过滤类别ID小于0的结果
filtered_boxes = boxes[boxes[:, 0] > -1e-06]

# 过滤低置信度结果
filtered_boxes = filtered_boxes[filtered_boxes[:, 1] >= threshold]
return filtered_boxes
```

## 异步处理

