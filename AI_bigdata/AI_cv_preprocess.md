## 参考资料
Pillow Performance
https://python-pillow.org/pillow-perf/

scikit-image transform.resize is prohibitively slow (x15 compared to OpenCV or PIL)
https://github.com/scikit-image/scikit-image/issues/7041

TinyScaler
https://github.com/Farama-Foundation/tinyscaler

## 缩放性能

