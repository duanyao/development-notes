
TensorFlow Object Detection API
https://github.com/tensorflow/models/blob/master/research/object_detection/README.md

TensorFlow 2 Detection Model Zoo
https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2_detection_zoo.md

Training and Evaluation with TensorFlow 2
https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2_training_and_evaluation.md

Exporting a trained model for inference
https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/exporting_models.md

TensorFlow 2 meets the Object Detection API 
https://blog.tensorflow.org/2020/07/tensorflow-2-meets-object-detection-api.html

Tensorflow Object Detection with Tensorflow 2: Creating a custom model
https://gilberttanner.com/blog/tensorflow-object-detection-with-tensorflow-2-creating-a-custom-model

Training Custom Object Detector
https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/training.html

How to Train Your Own Object Detector Using TensorFlow Object Detection API
https://neptune.ai/blog/how-to-train-your-own-object-detector-using-tensorflow-object-detection-api

## 训练一个模型

### docker
cd ~/project/tensorflow-models.git
docker build -f research/object_detection/dockerfiles/tf2/Dockerfile -t od .
# cp object_detection/dockerfiles/tf2_ai_platform/Dockerfile . # 基于 tensorflow/tensorflow:latest-gpu


### venv
sudo apt install protobuf-compiler
wget -nv https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz

cd ~/project/tensorflow-models.git
