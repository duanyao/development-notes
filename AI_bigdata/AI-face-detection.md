## 参考资料

S3FD: Single Shot Scale-invariant Face Detector
https://openaccess.thecvf.com/content_ICCV_2017/papers/Zhang_S3FD_Single_Shot_ICCV_2017_paper.pdf

InsightFace: 2D and 3D Face Analysis Project
https://github.com/deepinsight/insightface

PaddleDetection模块化地实现了多种主流目标检测算法
https://github.com/PaddlePaddle/PaddleDetection

Small Face Detection Using Deep Learning on Surveillance Videos
http://www.ijmlc.org/vol9/785-L0182.pdf

moli232777144 / small_model_face_recognition 
https://github.com/moli232777144/small_model_face_recognition
