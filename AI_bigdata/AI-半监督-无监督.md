## 参考资料
[1.1] Awesome Semi-Supervised Learning 2023
https://github.com/yassouali/awesome-semi-supervised-learning

[1.2] An Overview of Deep Semi-Supervised Learning 9 Jun 2020 ***
https://paperswithcode.com/paper/an-overview-of-deep-semi-supervised-learning

[1.3] 长文总结半监督学习（Semi-Supervised Learning），“An Overview of Deep Semi-Supervised Learning” 的笔记
https://zhuanlan.zhihu.com/p/252343352

[1.4] A Survey on Deep Semi-supervised Learning 2021 ***
https://arxiv.org/pdf/2103.00550.pdf

[1.5] Weak supervision
https://en.wikipedia.org/wiki/Weak_supervision

[1.6] Image Classification semi-supervised-learning
https://github.com/yassouali/awesome-semi-supervised-learning/blob/master/files/img_classification.md

[1.7] 自监督、半监督和有监督全涵盖，四篇论文遍历对比学习的研究进展 2020
https://www.jiqizhixin.com/articles/2020-09-15-8

[2.1] Billion-scale semi-supervised learning for image classification 2019 ***
https://arxiv.org/abs/1905.00546
https://paperswithcode.com/paper/billion-scale-semi-supervised-learning-for

[2.2] Implementing Billion-scale semi-supervised learning for image classification using Pytorch
https://github.com/leaderj1001/Billion-scale-semi-supervised-learning

[2.3] SoftMatch: Addressing the Quantity-Quality Trade-off in Semi-supervised Learning 2023
https://arxiv.org/abs/2301.10921

[2.5] Towards Semi-supervised Learning with Non-random Missing Labels. [pdf] 2023
https://arxiv.org/abs/2308.08872
https://github.com/NJUyued/PRG4SSL-MNAR

[2.6] FreeMatch: Self-adaptive Thresholding for Semi-supervised Learning 2023
https://arxiv.org/abs/2205.07246

[3.1] Semi-supervised learning
https://scikit-learn.org/stable/modules/semi_supervised.html

[3.2] PaddleDetection/configs/semi_det/ DenseTeacher ARSL
https://github.com/PaddlePaddle/PaddleDetection/tree/develop/configs/semi_det

[3.3] Cut and Learn for Unsupervised Object Detection and Instance Segmentation
https://paperswithcode.com/paper/cut-and-learn-for-unsupervised-object

Semi-Supervised and Semi-Weakly Supervised ImageNet Models
https://github.com/facebookresearch/semi-supervised-ImageNet1K-models

MVP: Multimodality-guided Visual Pre-training 2022
https://arxiv.org/abs/2203.05175

中科大和华为提出MVP：基于CLIP来进行视觉无监督训练
https://zhuanlan.zhihu.com/p/479221008

End-to-End Semi-Supervised Object Detection with Soft Teacher
https://arxiv.org/abs/2106.09018

Semi-DETR: Semi-Supervised Object Detection with Detection Transformers CVPR 2023 
https://paperswithcode.com/paper/semi-detr-semi-supervised-object-detection-1
https://github.com/JCZ404/Semi-DETR
https://github.com/PaddlePaddle/PaddleDetection/tree/develop/configs/semi_det/semi_detr

Ambiguity-Resistant Semi-Supervised Learning for Dense Object Detection (ARSL)
https://paperswithcode.com/paper/ambiguity-resistant-semi-supervised-learning
https://github.com/PaddlePaddle/PaddleDetection/tree/develop/configs/semi_det/arsl

Dense Teacher: Dense Pseudo-Labels for Semi-supervised Object Detection
https://paperswithcode.com/paper/dense-teacher-dense-pseudo-labels-for-semi

PaddleDetection/configs/semi_det/ DenseTeacher ARSL
https://github.com/PaddlePaddle/PaddleDetection/tree/develop/configs/semi_det

Consistent-Teacher: Towards Reducing Inconsistent Pseudo-targets in Semi-supervised Object Detection CVPR 2023
https://paperswithcode.com/paper/consistent-teacher-provides-better-1
https://github.com/Adamdad/ConsistentTeacher

Big Self-Supervised Models are Strong Semi-Supervised Learners NeurIPS 2020
https://paperswithcode.com/paper/big-self-supervised-models-are-strong-semi

Semi-Supervised Domain Generalizable Person Re-Identification 11 Aug 2021 
https://paperswithcode.com/paper/semi-supervised-domain-generalizable-person




## 框架/工具包
### microsoft / Semi-supervised-learning / USB Unified Semi-supervised learning Benchmark
[4.1]
USB is a Pytorch-based Python package for Semi-Supervised Learning (SSL). It is easy-to-use/extend, affordable to small groups, and comprehensive for developing and evaluating SSL algorithms. USB provides the implementation of 14 SSL algorithms based on Consistency Regularization, and 15 tasks for evaluation from CV, NLP, and Audio domain.

DeFixmatch  FreeMatch and SoftMatch.
