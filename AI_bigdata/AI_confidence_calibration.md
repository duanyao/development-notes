# Confidence Calibration（置信度校正）

## 参考资料
[1] Confidence Calibration Problem in Machine Learning 2021
https://dasha.ai/en-us/blog/confidence-calibration-problem-in-machine-learning

[2] Probability calibration
https://scikit-learn.org/stable/modules/calibration.html

[3] MACEst (Model Agnostic Confidence Estimator) A Python library for calibrating Machine Learning models' confidence scores 2021
https://pythonawesome.com/a-python-library-for-calibrating-machine-learning-models-confidence-scores/
https://github.com/oracle/macest
https://arxiv.org/abs/2109.01531

[4] Variable-Based Calibration for Machine Learning Classifiers
https://arxiv.org/abs/2209.15154
https://arxiv.org/pdf/2209.15154.pdf

[5] Beyond sigmoids: How to obtain well-calibrated probabilities from binary classifiers with beta calibration 2017
https://projecteuclid.org/journals/electronic-journal-of-statistics/volume-11/issue-2/Beyond-sigmoids--How-to-obtain-well-calibrated-probabilities-from/10.1214/17-EJS1338SI.full

[6] Brier Score: Understanding Model Calibration 2023
https://neptune.ai/blog/brier-score-and-model-calibration
