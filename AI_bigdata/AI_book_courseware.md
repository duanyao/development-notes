## 参考资料
[1.0] 《动手学深度学习》第二版预览版 https://zh-v2.d2l.ai/

[2.0] 4.1 Fine tuning 模型微调 
https://pytorchbook.cn

[3.0] Stanford CS class CS231n: Convolutional Neural Networks for Visual Recognition
https://cs231n.github.io
