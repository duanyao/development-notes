## 参考资料

[1.1] LightGBM 算法总结 https://blog.csdn.net/weixin_39807102/article/details/81912566

## GBDT

GBDT (Gradient Boosting Decision Tree) 是机器学习中一个长盛不衰的模型，其主要思想是利用弱分类器（决策树）迭代训练以得到最优模型，该模型具有训练效果好、不易过拟合等优点。GBDT 在工业界应用广泛，通常被用于点击率预测，搜索排序等任务。

## LightGBM

