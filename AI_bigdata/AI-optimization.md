[2.1] GPU捉襟见肘还想训练大批量模型？谁说不可以
https://www.jiqizhixin.com/articles/2018-10-17-11

[2.2] 深度神经网络模型训练时GPU显存不足怎么办？
https://zhuanlan.zhihu.com/p/138534708

[2.3] 科普帖：深度学习中GPU和显存分析
https://zhuanlan.zhihu.com/p/31558973

[2.4] MegEngine 中动态图显存优化（DTR）的实现与优化
https://zhuanlan.zhihu.com/p/375642263

[2.5] Dynamic Tensor Rematerialization(Checkpointing)
https://github.com/pytorch/pytorch/pull/42056

[2.6] 深度学习训练模型时，GPU显存不够怎么办？
https://www.zhihu.com/question/461811359

## 训练显存优化
[2.1-2.2]
