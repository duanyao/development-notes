## 参考资料

[0.1] Awesome Dataset Tools
  https://github.com/jsbroks/awesome-dataset-tools

[1.1] 万维识别 图像类采集标注服务
  http://www.wvsense.com/data.html

[1.2] AI数据标注服务 定制化数据解决方案
  https://www.zzcrowd.com/landing

[1.3] 易学智能
  https://www.easyaiforum.cn/
  
[1.4] 京东众智 Wise开放标注平台
  https://biao.jd.com/wise

[2.1] 有哪些比较好的图像标注工具？
https://www.zhihu.com/question/30626971

[2.2] A curated list of awesome data labeling tools
https://github.com/heartexlabs/awesome-data-labeling

[2.3] 7大数据标注工具分享
http://www.awkvector.com/20191107-2/

[2.4] 计算机视觉领域最好用的 5 个开源图像标注工具
https://www.infoq.cn/article/OOFtcgKNej1TJvW6u7FR

[3.1] Efficiently Scaling Up Video Annotation with Crowdsourced Marketplaces
https://github.com/cvondrick/vatic

[3.2] 深度学习半自动化视频标注工具——VATIC使用教程
https://blog.csdn.net/weixin_39837402/article/details/86602880

[3.3] vatic-docker-contrib
https://github.com/jldowns/vatic-docker-contrib

[3.4]  NPSVisionLab /vatic-docker 
https://github.com/NPSVisionLab/vatic-docker

[3.5] vatic.js A pure Javascript video annotation tool (在线视频标注工具)
https://dbolkensteyn.github.io/vatic.js/

[3.6] vatic--视频标注工具
https://blog.csdn.net/baidu_26788951/article/details/80053760

[3.7] vatic标注工具安装步骤（非docker安装）以及错误解决办法
https://blog.csdn.net/weixin_43159628/article/details/89156717

[4.1] CVAT is free, online, interactive video and image annotation tool for computer vision
https://github.com/opencv/cvat

[4.2] Intel OpenVINO toolkit
https://github.com/opencv/cvat/tree/master/components/openvino

[4.3] mask_rcnn_inception_resnet_v2_atrous_coco
https://github.com/opencv/cvat/tree/develop/utils/open_model_zoo/mask_rcnn_inception_resnet_v2_atrous_coco

[4.4] Faster R-CNN with Inception v2
https://github.com/opencv/cvat/tree/develop/utils/open_model_zoo/faster_rcnn_inception_v2_coco

[4.5] Support for openVINO 2020 is missing
https://github.com/opencv/cvat/issues/1179

[4.6] Re-Identification Application
https://github.com/opencv/cvat/tree/master/cvat/apps/reid

[4.7] Computer Vision Annotation Tool: A Universal Approach to Data Annotation
https://software.intel.com/en-us/articles/computer-vision-annotation-tool-a-universal-approach-to-data-annotation

[5.1] An annotation tool for action labeling in videos. Best for machine learning/computer vision action recognition research. 
https://github.com/devyhia/action-annotation

[6.1] BBox-Label-Tool
https://github.com/puzzledqs/BBox-Label-Tool

[7.1] LabelImg
https://github.com/tzutalin/labelImg

[8.1] MATLAB R2019a版本自带的imagelabel/VideoLabeler/GroundTruth Labeler工具
https://ww2.mathworks.cn/help/vision/ref/videolabeler-app.html?searchHighlight=VideoLabeler&s_tid=doc_srchtitle

[9.1] UltimateLabeling
https://github.com/alexandre01/UltimateLabeling

[10.1] 如何用精灵标注助手标注视频跟踪
http://www.jinglingbiaozhu.com/?type=post&pid=7

[11.1] lableme
https://zhuanlan.zhihu.com/p/112512069

[12.1] PaddleSeg
https://github.com/PaddlePaddle/PaddleSeg/tree/develop/EISeg

[13.1] 在线标注平台
https://labelhub.cn/home

[14.1]  microsoft /VoTT

https://github.com/microsoft/VoTT

[15.1] VGG VIA
https://www.robots.ox.ac.uk/~vgg/software/via/
https://gitlab.com/vgg/via

[16.1] Vidat
https://github.com/anucvml/vidat

## VGG VIA
[15.1] 
图像、视频、音频标注工具。
基于浏览器，代码集成于单一html文件，单机版，无服务器端。
有可能支持标注在线视频。

注意它的 2.x 仅支持图片标注，3.x 才支持视频标注。

优点：
* 界面响应快
* 按视频的绝对时间来标注。

缺点：
* 标注方式比较难懂。
* 缺乏文档。

## Vidat
视频标注工具。
基于浏览器，单机版，无服务器端。
可以标注在线视频，提交标注数据到指定url。
可以接入云标注平台。

### 部署
将vidat的源码下载，解压；用静态 http 提供服务，例如

```
cd  ~/project/vidat-v2.0.3
source ~/opt/venv/v39/bin/activate
python -m http.server
```
要标注的视频文件放到 vidat 根目录下。
```
wget https://labelstud-1.ai-bama.cn/childactionsj_1/desensitize_video_with_audio/a347/8021296762186057252__1670571993686.mp4
```
用浏览器访问： ` http://127.0.0.1:8000/?video=8021296762186057252__1670571993686.mp4 `

优点：
* 界面响应快
* 标注方式（左右帧对比）有特点

缺点：
* 标注方式比较难懂。
* 不是按视频的绝对时间来标注的。帧时间戳不是从视频帧中读取的，而是基于一个固定的帧率去采样得到的（默认10fps，可配置）。

## vatic

### 本地安装
wget http://mit.edu/vondrick/vatic/vatic-install.sh
wget http://cs.columbia.edu/~vondrick/vatic/vatic-install.sh

查看其内容。

dpkg -l git python-setuptools python-dev libavcodec-dev libavformat-dev libswscale-dev libjpeg62 libjpeg62-dev libfreetype6 libfreetype6-dev apache2 libapache2-mod-wsgi gfortran

注意 libjpeg62-turbo 和 libjpeg62 冲突，你可能已经安装了前者。

dpkg -l mysql-server-5.7 mysql-client-5.7  libmysqlclient-dev

参考 mysql.md ，安装 mysql-server-5.7

### docker 安装
[3.2,3.3]

docker pull jldowns/vatic-docker-contrib

docker run -it -p 7080:80 -v /home/duanyao/data-development/label-work/vatic:/home/vagrant/vagrant_data jldowns/vatic-docker-contrib:0.1

/home/duanyao/data-development/label-work/vatic 是主机数据目录，/home/vagrant/vagrant_data 是容器内的数据目录。7080 是本机端口，80 是容器内的端口。

cd /home/vagrant/vatic
turkic extract /home/vagrant/vagrant_data/1561101023349-576.mp4 /home/vagrant/vagrant_data/1561101023349-576.frames

turkic load k /home/vagrant/vagrant_data/1561101023349-576.frames/ sit stand walk hand_up --offline
turkic publish --offline

http://localhost/?id=1&hitId=offline
http://localhost/?id=2&hitId=offline
http://localhost/?id=3&hitId=offline
http://localhost/?id=4&hitId=offline

http://localhost:7080/?id=1&hitId=offline

turkic load k2 /home/vagrant/vagrant_data/1561101023349-576.frames/ person --offline

## opencv/cvat

### 安装

先安装 docker-ce 和 docker-compose

git clone https://github.com/opencv/cvat
cd cvat
docker-compose up -d   # -d 后台运行

docker exec -it cvat bash -ic 'python3 ~/manage.py createsuperuser'

设置 admin 账号和密码。完毕后自动退出。

http://localhost:8080/

docker-compose down  # 停止


在服务器上运行：

export CVAT_HOST=label-1.ai-parents.cn
export ACME_EMAIL=duanyao@chengzhangguiji.cn
docker-compose -f docker-compose.yml -f docker-compose.https.yml up -d
https://label-1.ai-parents.cn


### cvat OpenVINO
cvat 已经自带了 OpenVINO

从 https://software.intel.com/en-us/openvino-toolkit 下载 openvino-toolkit ，需要注册。当前（2020.4.5）的 cvat 需要 openvino-toolkit 2019R3 ，更新的版本是不兼容的。

Serial number : CCBP-4PJRX4XB

将下载的文件 l_openvino_toolkit_p_<version>.tgz 放到 cvat.git/components/openvino/ 目录。

# From project root directory
docker-compose -f docker-compose.yml -f components/openvino/docker-compose.openvino.yml build  # 生成 images

docker-compose -f docker-compose.yml -f components/openvino/docker-compose.openvino.yml up -d  # -d 后台运行


git clone https://github.com/opencv/open_model_zoo.git open_model_zoo.git

pip install -r tools/downloader/requirements.in
pip install -r /opt/intel/openvino_2020.1.023/deployment_tools/model_optimizer/requirements_tf.txt

python tools/downloader/downloader.py --print_all
python tools/downloader/downloader.py --name faster_rcnn_inception_v2_coco
python tools/downloader/downloader.py --name mask_rcnn_inception_resnet_v2_atrous_coco
source /opt/intel/openvino/bin/setupvars.sh
python tools/downloader/converter.py --name faster_rcnn_inception_v2_coco
python tools/downloader/converter.py --name mask_rcnn_inception_resnet_v2_atrous_coco

docker-compose down

## VoTT

停止维护。
