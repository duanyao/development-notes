## 参考资料
[1.1] Action-Conditioned 3D Human Motion Synthesis with Transformer VAE 2021
https://mathis.petrovich.fr/actor/
https://openaccess.thecvf.com/content/ICCV2021/papers/Petrovich_Action-Conditioned_3D_Human_Motion_Synthesis_With_Transformer_VAE_ICCV_2021_paper.pdf
https://github.com/Mathux/ACTOR
https://www.youtube.com/watch?v=De_1NoCXlEw

[1.5] Official mindspore implementation of the paper "Action-Conditioned 3D Human Motion Synthesis with Transformer VAE", ICCV 2021.
https://github.com/cai-jianfeng/ACTOR_mindspore

[2.1] MDM: Human Motion Diffusion Model 2023
https://guytevet.github.io/mdm-page/
https://arxiv.org/abs/2209.14916
https://github.com/GuyTevet/motion-diffusion-model
https://www.youtube.com/watch?v=8PjOb6N7q-Q

[2.2] MDM 演示程序
https://replicate.com/daanelson/motion_diffusion_model

[3.1] DepthHuman: A tool for depth image synthesis for human pose estimation: synthesizing large quantities of depth images with annotations for depth-based human pose estimation from frontal profiles
https://github.com/idiap/depth_human_synthesis

[4.1] IMos: Intent-Driven Full-Body Motion Synthesis for Human-Object Interactions
https://arxiv.org/abs/2004.08692
https://github.com/anindita127/IMoS
https://vcai.mpi-inf.mpg.de/projects/IMoS/
https://www.youtube.com/watch?v=3Ngi9k41-7c

[5.1] MotionDiffuse: Text-Driven Human Motion Generation with Diffusion Model 2022
https://paperswithcode.com/paper/motiondiffuse-text-driven-human-motion
https://arxiv.org/abs/2208.15001v1
https://mingyuan-zhang.github.io/projects/MotionDiffuse.html 
https://github.com/mingyuan-zhang/MotionDiffuse
https://github.com/viiika/diffusion-conductor

[6.1] ReMoDiffuse: Retrieval-Augmented Motion Diffusion Model 2023
https://paperswithcode.com/paper/remodiffuse-retrieval-augmented-motion
https://github.com/mingyuan-zhang/ReMoDiffuse

[7.1] Executing your Commands via Motion Diffusion in Latent Space (MLD) 2023
https://paperswithcode.com/paper/executing-your-commands-via-motion-diffusion
https://github.com/chenfengye/motion-latent-diffusion

[8.1] FineMoGen: Fine-Grained Spatio-Temporal Motion Generation and Editing 2023
https://paperswithcode.com/paper/finemogen-fine-grained-spatio-temporal-motion-1
https://github.com/mingyuan-zhang/FineMoGen

[9.1] MoMask: Generative Masked Modeling of 3D Human Motions 2023
https://paperswithcode.com/paper/momask-generative-masked-modeling-of-3d-human
https://github.com/EricGuo5513/momask-codes

[10.1] KIT Motion-Language is a dataset linking human motion and natural language
https://paperswithcode.com/dataset/kit-motion-language
[10.2] Motion Synthesis on KIT Motion-Language
https://paperswithcode.com/sota/motion-synthesis-on-kit-motion-language

[11.1] Human Motion Diffusion as a Generative Prior
https://paperswithcode.com/paper/human-motion-diffusion-as-a-generative-prior
https://github.com/priormdm/priormdm
https://github.com/zhenzhiwang/intercontrol


## ACTOR: Action-Conditioned 3D Human Motion Synthesis with Transformer VAE
[1.1]
Action2Motion [20],
a per-frame VAE on actions, using a GRU-based architec-
ture.

we adopt
VIBE [35] to obtain training motion sequences from action-
labelled video datasets.

 SMPL body model [43],
which is a disentangled body representation (similar to re-
cent models [50, 51, 54, 64])

NTU-13, HumanAct12 and UESTC datasets

NTU RGB+D dataset [42, 55]. To be able to compare to
the work of [20], we use their subset of 13 action categories.

UESTC dataset [30]. This recent dataset consists of 25K
sequences across 40 action categorie

We mea-
sure FID, action recognition accuracy, overall diversity, and
per-action diversity (referred to as multimodality in [20])

we obtain much
cleaner motions. Since it is difficult to show motion quality
results on static figures, we refer to our supplemental video
at [53] to see this effect. 

e demonstrate that
our model is capable of generating different ways of performing a given action. More results can be found in Section B of the appendix
and the supplemental video at [53].

## MDM: Human Motion Diffusion Model 2023
[2.1]
文本到动作生成，输出骨骼动画。
