
## paddle-serving-client

### pip 安装

当前（2022.1）pip paddle-serving 最高支持 python3.8 。

```
pip install paddle-serving-client==0.6.3
pip install paddle-serving-app==0.6.3
pip install paddle-serving-server==0.6.3
```

当前（2022.1）最新发布版 pip 为0.7 。

paddle-serving-client 与 paddle-serving-app 都属于客户端模块，前者是基础，后者则添加了CV和NLP常用的预处理和后处理功能，使用更方便。
paddle-serving-client 的另一个作用是导出 serving 模型。

### 源码编译安装

git clone https://gitee.com/PaddlePaddle/Serving PaddlePaddle-Serving.git
cd PaddlePaddle-Serving.git/ && git submodule update --init --recursive

#sudo bash tools/paddle_env_install.sh

sudo aptitude install libcurl4-openssl-dev libbz2-dev

## 导出 PaddleDetection serving 模型

1. 安装 paddle-serving-client ，用 pip
2. 安装 paddlepaddle ，用 pip ，可以是 cpu 或 gpu 版。
3. 安装 PaddleDetection，从源码安装

在 PaddleDetection 目录下：
```
python tools/export_model.py -c configs/yolov3/yolov3_darknet53_270e_head_chest_06.yml --output_dir=./serving_model -o use_gpu=false weights=output/yolov3_darknet53_270e_head_chest_06/model_final --export_serving_model=True
```
如果 paddlepaddle 是 cpu 版，则要加上 use_gpu=false 。

产生的文件如下：
```
find ./serving_model/yolov3_darknet53_270e_head_chest_06
./serving_model/yolov3_darknet53_270e_head_chest_06
./serving_model/yolov3_darknet53_270e_head_chest_06/serving_client
./serving_model/yolov3_darknet53_270e_head_chest_06/serving_client/serving_client_conf.prototxt
./serving_model/yolov3_darknet53_270e_head_chest_06/serving_client/serving_client_conf.stream.prototxt
./serving_model/yolov3_darknet53_270e_head_chest_06/model.pdiparams.info
./serving_model/yolov3_darknet53_270e_head_chest_06/infer_cfg.yml
./serving_model/yolov3_darknet53_270e_head_chest_06/serving_server
./serving_model/yolov3_darknet53_270e_head_chest_06/serving_server/__params__
./serving_model/yolov3_darknet53_270e_head_chest_06/serving_server/__model__
./serving_model/yolov3_darknet53_270e_head_chest_06/serving_server/serving_server_conf.prototxt
./serving_model/yolov3_darknet53_270e_head_chest_06/serving_server/serving_server_conf.stream.prototxt
./serving_model/yolov3_darknet53_270e_head_chest_06/model.pdiparams
./serving_model/yolov3_darknet53_270e_head_chest_06/model.pdmodel
```
以下文件是模型的主体，内容是完全一样的：
```
./serving_model/yolov3_darknet53_270e_head_chest_06/serving_server/__params__
./serving_model/yolov3_darknet53_270e_head_chest_06/model.pdiparams
```

## 启动服务

python3 -m paddle_serving_server.serve --model ./serving_model/yolov3_darknet53_270e_head_chest_06/serving_server/ --thread 10 --port 9292
python3 -m paddle_serving_server.serve --model ./serving_model/yolov3_darknet53_270e_head_chest_06/serving_server/ --thread 10 --port 9292 --name head_det

可能会出现错误：

```
  File "/home/rd/opt/venv/v38-1/lib/python3.8/site-packages/flask/app.py", line 19, in <module>
    from werkzeug.local import ContextVar
```
这是因为 Flask 和 Werkzeug 包的版本不匹配：

```
(v38-1) rd@gtrd:~/project/PaddleDetection.git$ pip list | grep -i flask
Flask                  2.0.2    
(v38-1) rd@gtrd:~/project/PaddleDetection.git$ pip list | grep -i werkzeug
Werkzeug               1.0.1
```
根据 paddle-serving-server==0.6.3 的要求，安装如下版本的 Flask 和 Werkzeug：
```
pip install Flask==1.1.1 Werkzeug==1.0.1
```

## 使用客户端



