Relative Human multi-person in-the-wild RGB images with rich human annotations Genders, Age, Bounding box, 2D pose.
https://paperswithcode.com/dataset/relative-human

Human Wrist Image Dataset | Human Body Parts: images of wrist captured using mobile phones in real-world scenario.
https://paperswithcode.com/dataset/human-wrist-image-dataset-human-body-parts

MPII Human Pose: 40k people covers 410 human activities.
https://paperswithcode.com/dataset/mpii-human-pose
https://arxiv.org/abs/1906.11367
http://human-pose.mpi-inf.mpg.de/

Synthetic Human Model Dataset: A synthetic dataset for evaluating non-rigid 3D human reconstruction based on conventional RGB-D cameras.
https://paperswithcode.com/dataset/synthetic-human-model-
https://research.csiro.au/robotics/our-work/databases/synthetic-human-model-dataset/

Exploring 3D Human Pose Estimation and Forecasting from the Robot's Perspective: The HARPER Dataset 2024
https://arxiv.org/abs/2403.14447

H²O Interaction (Human-to-Human-or-Object Interaction)
https://paperswithcode.com/dataset/h2o-interaction

Human Optical Flow (Human Optical Flow dataset)
https://paperswithcode.com/dataset/coma-1
