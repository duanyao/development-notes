[1.1] awesome anomaly detection ***
https://github.com/hoya012/awesome-anomaly-detection

[1.2] task: Anomaly Detection
1047 papers with code • 59 benchmarks • 79 datasets
https://paperswithcode.com/task/anomaly-detection

[1.3] A Unified Survey on Anomaly, Novelty, Open-Set, and Out-of-Distribution Detection: Solutions and Future Challenges 2021 **
https://arxiv.org/abs/2110.14051

[1.4]【入门】异常检测Anomaly Detection 2021 **
https://zhuanlan.zhihu.com/p/116235115

[1.5] 异常检测 | Anomaly Detection综述 2020 **
https://zhuanlan.zhihu.com/p/266513299

[1.6] one class classification
https://zhuanlan.zhihu.com/p/58029015

[2.1] 异常检测 | MemAE模型复现与思考
https://zhuanlan.zhihu.com/p/344615097

[2.2] Memorizing Normality to Detect Anomaly: Memory-Augmented Deep Autoencoder for Unsupervised Anomaly Detection (MemAE) 2019 ***
https://openaccess.thecvf.com/content_ICCV_2019/html/Gong_Memorizing_Normality_to_Detect_Anomaly_Memory-Augmented_Deep_Autoencoder_for_Unsupervised_ICCV_2019_paper.html
https://github.com/donggong1/memae-anomaly-detection
https://github.com/lyn1874/memAE

[2.3] Anomaly Detection using One-Class Neural Networks (OC-NN) 2018
https://arxiv.org/abs/1802.06360

[2.4] Deep Weakly-supervised Anomaly Detection 2023
https://arxiv.org/abs/1910.13601
https://github.com/mala-lab/PReNet

[2.5] One-Class Convolutional Neural Network
https://arxiv.org/abs/1901.08688
https://github.com/otkupjnoz/oc-cnn


Real-world Anomaly Detection in Surveillance Videos | [arXiv' 18] |
https://arxiv.org/abs/1801.04264
https://www.crcv.ucf.edu/research/real-world-anomaly-detection-in-surveillance-videos/

Detecting Abnormality without Knowing Normality: A Two-stage Approach for Unsupervised Video Abnormal Event Detection | [ACMMM' 18]
https://dl.acm.org/doi/10.1145/3240508.3240615

DeepOD supports tabular anomaly detection and time-series anomaly detection. 2023
https://github.com/xuhongzuo/DeepOD

Anomalib is a deep learning library that aims to collect state-of-the-art anomaly detection algorithms for benchmarking 2023
https://github.com/openvinotoolkit/anomalib

CFLOW-AD: Real-Time Unsupervised Anomaly Detection with Localization via Conditional Normalizing Flows
https://arxiv.org/pdf/2107.12571v1.pdf

A Robust AUC Maximization Framework with Simultaneous Outlier Detection and Feature Selection for Positive-Unlabeled Classification
https://arxiv.org/abs/1803.06604

[3.1] UCSD Ped2 (UCSD Anomaly Detection Dataset)
https://paperswithcode.com/dataset/ucsd

[3.2] The MVTec anomaly detection dataset (MVTec AD) **
https://www.mvtec.com/company/research/datasets/mvtec-ad

[3.3] CUHK Avenue
Avenue Dataset contains 16 training and 21 testing video clips. The videos are captured in CUHK campus avenue with 30652 (15328 training, 15324 testing) frames in total.
https://paperswithcode.com/dataset/chuk-avenue
https://www.cse.cuhk.edu.hk/leojia/projects/detectabnormal/dataset.html

[4.1]  microsoft / Semi-supervised-learning /USB: A Unified Semi-supervised learning Benchmark
https://github.com/microsoft/Semi-supervised-learning

## 概述

在现在的异常检测领域，为了检测出某个图像/视屏信息的异常情况，通常的方向有两种：一是进行reconstruction重构任务，二是进行prediction预测任务。现在，我们先说预测任务，大体思路是通过时刻的图像经过网络预测下一时刻的图像，并与真实的ground truth标签进行对比，从而达到任务。重构任务是本文所采取的方法，一般配合AutoEncoder使用，是指对某一时刻的图像进行编码Encoder，然后变成一个low-level的特征，然后通过一个与Encoder逆向的网络作为解码Decoder，输出图像。最后用与计算重构损失，用来检测异常。[2.1]

## 数据集
UCSD Ped2 [3.1]：街道视频数据集。
CUHK Avenue [3.3]：街道视频数据集。

MVTec [3.2]。工业品图像缺陷数据集。


