A Review On Multi-Label Learning Algorithms 2014
https://www.researchgate.net/publication/263813673_A_Review_On_Multi-Label_Learning_Algorithms

多标签分类：定义、思想和算法 2020
https://zhuanlan.zhihu.com/p/183957063?utm_id=0

A Tutorial on Multilabel Learning 2015
https://www.researchgate.net/publication/270337594_A_Tutorial_on_Multi-Label_Learning

Asymmetric Loss For Multi-Label Classification 2021
https://arxiv.org/abs/2009.14119
https://github.com/Alibaba-MIIL/ASL

Query2Label: A Simple Transformer Way to Multi-Label Classification 2021
https://arxiv.org/abs/2107.10834v1
https://paperswithcode.com/paper/query2label-a-simple-transformer-way-to-multi
https://github.com/SlongLiu/query2labels
https://github.com/curt-tigges/query2label
