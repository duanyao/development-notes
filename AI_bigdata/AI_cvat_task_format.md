## zip 档案目录结构
图像序列任务：
```
task.json
annotations.json
data/
  截图_选择区域_20210823193303.png
  截图_选择区域_20210823193328.png
  manifest.jsonl
  index.json
```

视频任务：
```
task.json
annotations.json
data/
  1624496810975.576p.rk.mp4
```

视频任务仅允许存在一个视频文件。

## task.json
任务元数据。

```
{
    "name": "tfd-02",
    "bug_tracker": "",
    "subset": "Test",
    "version": "1.0",
    "status": "annotation",
    "labels": [  // 标签定义
        {
            "name": "i",
            "color": "#08e91b",
            "attributes": []
        },
        {
            "name": "j",
            "color": "#218441",
            "attributes": [
                {
                    "name": "blur",
                    "mutable": true,
                    "input_type": "number",
                    "default_value": "0",
                    "values": [
                        "0",
                        "1",
                        "0.5"
                    ]
                },
                //...
            ]
        },
        //...
    ],
    

    "data": {
        "chunk_size": 72,
        "image_quality": 80,
        "start_frame": 0,  // 帧编号，从0开始计数
        "stop_frame": 1,  // 结束帧编号，含
        "frame_filter": "step=4", // 帧过滤器，可选。对视频比较有用，图片数据集一般不需要。"step=4" 表示每4帧抽一帧进行标注。
        "storage_method": "cache", // 其他值有 "file_system",
        "storage": "local",
        "chunk_type": "imageset"
    },
    "jobs": [
        {
            "start_frame": 0, // 帧编号，从0开始计数。如果 "data" 有 "frame_filter" ，则这是过滤后的帧序列的编号。
            "stop_frame": 1, // 结束帧编号，含
            "status": "annotation",
            "reviews": []
        }
    ]
```

## annotations.json

```
[
    {
        "version": 13, // 随着修改次数增加
        "tags": [],
        "shapes": [
            {
                "type": "rectangle",
                "occluded": false,
                "z_order": 0,
                "points": [  // 格式： x,y,w,h 绝对坐标。
                    170.1157957315445,
                    78.86752015352249,
                    247.38214433193207,
                    259.8861930370331
                ],
                "frame": 0, // 帧编号，从0开始计数
                "group": null,
                "source": "auto", // auto 表示自动标注， manual 表示人工标注
                "attributes": [
                    {
                        "value": "甲",
                        "name": "name"
                    },
                    {
                        "value": "0",
                        "name": "blur"
                    },
                    {
                        "value": "0",
                        "name": "face_shown"
                    },
                    {
                        "value": "0",
                        "name": "person"
                    }
                ],
                "label": "头"
            },
            {
                "type": "rectangle",
                "occluded": false,
                "z_order": 0,
                "points": [
                    335.29691874980927,
                    25.287589758634567,
                    409.0310615301132,
                    137.81028819084167
                ],
                "frame": 0,
                "group": null,
                "source": "auto",
                "attributes": [
                    {
                        "value": "甲",
                        "name": "name"
                    },
                    {
                        "value": "0",
                        "name": "blur"
                    },
                    {
                        "value": "0",
                        "name": "face_shown"
                    },
                    {
                        "value": "0",
                        "name": "person"
                    }
                ],
                "label": "头"
            },
        ],
        
        "tracks": [
            //...
        ]
            
```

## manifest.jsonl
图像列表。
视频任务可以没有此文件。
图片任务也可以没有，但这样图片的顺序就没有明确定义了（可能是根据其在zip包中的排序？），所以建议还是得有。
name 是不带扩展名的图片文件名。

```
{"version":"1.1"}
{"type":"images"}
{"name":"\u622a\u56fe_\u9009\u62e9\u533a\u57df_20210823193303","extension":".png","width":410,"height":262,"meta":{"related_images":[]}}
{"name":"\u622a\u56fe_\u9009\u62e9\u533a\u57df_20210823193328","extension":".png","width":282,"height":277,"meta":{"related_images":[]}}
```

## index.json
意义不明。对视频或图片任务，可以没有。
```
{"0":36,"1":173}
```
