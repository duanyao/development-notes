[1.1] 卡内基梅隆大学的开源项目 OpenPose
  https://github.com/CMU-Perceptual-Computing-Lab/openpose

[1.2] 如何评价卡内基梅隆大学的开源项目 OpenPose？
  https://www.zhihu.com/question/59750782

[2.5] OpenPose - Prerequisites
  https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/prerequisites.md

[2.6] CMake error with "CUDA_cublas_device_LIBRARY"
  https://github.com/clab/dynet/issues/1457

[2.7] CMake
  https://github.com/Kitware/CMake/tree/v3.16.4

[2.8] Bug 1530828 - Cuda 9 fails to compile when using boost 
  https://bugzilla.redhat.com/show_bug.cgi?id=1530828

[2.9] Update for CUDA version macro changes.#175
  https://github.com/boostorg/config/pull/175/files#diff-672763afc774e7888a21e9b439e15f36L17

[2.10] Custom Caffe
  https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/installation.md#custom-caffe
  
[3.1] OpenPose python_module
  https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/modules/python_module.md

[3.2] OpenPose Demo - Output
  https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/output.md

[CVPR 2017] OpenPose: Realtime Multi-Person 2D Pose Estimation using Part Affinity Fields
https://towardsdatascience.com/cvpr-2017-openpose-realtime-multi-person-2d-pose-estimation-using-part-affinity-fields-f2ce18d720e8

## 下载代码

```
cd ~/t-project
git clone https://github.com/CMU-Perceptual-Computing-Lab/openpose openpose.git
cd openpose.git
git submodule update --init --recursive --remote # 以后用 git pull 更新主仓库后，也要再次运行这个命令更新子仓库的代码
```
## 安装其它依赖

参考 openpose.git/scripts/ubuntu/install_deps.sh
```
sudo apt-get install libatlas-base-dev libprotobuf-dev libleveldb-dev libsnappy-dev libhdf5-serial-dev protobuf-compiler
sudo apt-get install libgoogle-glog-dev #libgflags-dev  liblmdb-dev
sudo apt-get install --no-install-recommends libboost-all-dev
```

libhdf5-serial-dev 在 debian 10 上不存在，可以被 libhdf5-dev 替代。

opencv 依赖？
```
$ ldd /usr/local/python/openpose/pyopenpose.cpython-37m-x86_64-linux-gnu.so | grep opencv
	libopencv_videostab.so.2.4 => /lib/x86_64-linux-gnu/libopencv_videostab.so.2.4 (0x00007ff280307000)
	libopencv_ts.so.2.4 => /lib/x86_64-linux-gnu/libopencv_ts.so.2.4 (0x00007ff28005d000)
	libopencv_superres.so.2.4 => /lib/x86_64-linux-gnu/libopencv_superres.so.2.4 (0x00007ff27fe2b000)
	libopencv_stitching.so.2.4 => /lib/x86_64-linux-gnu/libopencv_stitching.so.2.4 (0x00007ff27fbaf000)
	libopencv_ocl.so.2.4 => /lib/x86_64-linux-gnu/libopencv_ocl.so.2.4 (0x00007ff27f7fe000)
	libopencv_gpu.so.2.4 => /lib/x86_64-linux-gnu/libopencv_gpu.so.2.4 (0x00007ff27f5a2000)
	libopencv_contrib.so.2.4 => /lib/x86_64-linux-gnu/libopencv_contrib.so.2.4 (0x00007ff27f2c7000)
	libopencv_photo.so.2.4 => /lib/x86_64-linux-gnu/libopencv_photo.so.2.4 (0x00007ff27e283000)
	libopencv_legacy.so.2.4 => /lib/x86_64-linux-gnu/libopencv_legacy.so.2.4 (0x00007ff27df77000)
	libopencv_video.so.2.4 => /lib/x86_64-linux-gnu/libopencv_video.so.2.4 (0x00007ff27dd26000)
	libopencv_objdetect.so.2.4 => /lib/x86_64-linux-gnu/libopencv_objdetect.so.2.4 (0x00007ff27dab4000)
	libopencv_ml.so.2.4 => /lib/x86_64-linux-gnu/libopencv_ml.so.2.4 (0x00007ff27d83b000)
	libopencv_calib3d.so.2.4 => /lib/x86_64-linux-gnu/libopencv_calib3d.so.2.4 (0x00007ff27d5a8000)
	libopencv_features2d.so.2.4 => /lib/x86_64-linux-gnu/libopencv_features2d.so.2.4 (0x00007ff27d311000)
	libopencv_highgui.so.2.4 => /lib/x86_64-linux-gnu/libopencv_highgui.so.2.4 (0x00007ff27d0cc000)
	libopencv_imgproc.so.2.4 => /lib/x86_64-linux-gnu/libopencv_imgproc.so.2.4 (0x00007ff27cc4a000)
	libopencv_flann.so.2.4 => /lib/x86_64-linux-gnu/libopencv_flann.so.2.4 (0x00007ff27c9e3000)
	libopencv_core.so.2.4 => /lib/x86_64-linux-gnu/libopencv_core.so.2.4 (0x00007ff27c5c9000)
```

CUDA 和 cuDNN: 参照 nvdia-linux.md，最好是以下两对版本组合：CUDA 8.0 (cuDNN 5.1) and CUDA 10.0 (cuDNN 7.5) 。

如果需要 python 绑定，要安装 python 开发包和 numpy, opencv-python：

```
# Python 3 (default and recommended)
sudo apt-get install python3-dev
sudo pip3 install numpy opencv-python

# Python 2
sudo apt-get install python-dev
sudo pip install numpy opencv-python
```

默认配置下，openpose 使用自带的 caffe 而不是主机环境中安装的 caffe。如果要用后者，可以按[2.10]配置。安装 caffe 的方法是：

```
sudo apt install caffe-cuda libcaffe-cuda-dev
```

### 安装 cmake

如果想用 cmake 的 gui：
```
sudo apt install cmake-qt-gui
```

如果 cuda >= 10.0，则需要 CMake >= 3.12.2 ，否则 make 时可能遇到这样的错误[2.6]：

```
CMake Error: The following variables are used in this project, but they are set to NOTFOUND.
Please set them or make sure they are set and tested correctly in the CMake files:
CUDA_cublas_device_LIBRARY (ADVANCED)
    linked by target "caffe" in directory /home/duanyao/t-project/openpose.git/3rdparty/caffe/src/caffe
```

cmake 的官方二进制版本在 github 上，下载困难。和编译 CMake 源码[2.7]：

```
sudo apt install libssl-dev  # openssl 开发包
./bootstrap
make
sudo make install
```

### 修复 boost 的 bug

如果 libboost（libboost-all-dev）的版本 < 1.65，且 cuda 版本 >= 9.0，则需要修复 libboost 的 bug ，否则编译时 nvcc 会报 CUDACC_VER 错。
修改 /usr/include/boost/config/compiler/nvcc.hpp，将 
```
#if !defined(__CUDACC_VER__) || (__CUDACC_VER__ < 70500)
```
改为
```
#if !defined(__CUDACC_VER_MAJOR__) || (__CUDACC_VER_MAJOR__ * 1000 + __CUDACC_VER_MINOR__ < 7005)
```

参考 nvidia-linux.md 。

## 手动下载模型

cmake 执行过程中会自动下载依赖，但是网络可能不稳定，下载很慢。这时可以手动用 wget 下载：

```
~/t-project/openpose.git/models/pose/body_25$ wget -c http://posefs1.perception.cs.cmu.edu/OpenPose/models/pose/body_25/pose_iter_584000.caffemodel
~/t-project/openpose.git/models/pose/mpi$ wget -c http://posefs1.perception.cs.cmu.edu/OpenPose/models/pose/mpi/pose_iter_160000.caffemodel
~/t-project/openpose.git/models/pose/coco$ wget -c http://posefs1.perception.cs.cmu.edu/OpenPose/models/pose/coco/pose_iter_440000.caffemodel
~/t-project/openpose.git/models/face$ wget -c http://posefs1.perception.cs.cmu.edu/OpenPose/models/face/pose_iter_116000.caffemodel
~/t-project/openpose.git/models/hand$ wget -c http://posefs1.perception.cs.cmu.edu/OpenPose/models/hand/pose_iter_102000.caffemodel
```

## gcc 版本

cuda 10 需要 gcc 6 或者更低的版本，所以如果编译 cuda 版本，则需要确保 gcc/g++ 版本。

```
sudo update-alternatives --config gcc
sudo update-alternatives --config g++
```

## 正确加载 nvidia GPU 内核模块
如果 cmake 显示：
```
Automatic GPU detection failed. Building for all known architectures.
```
说明 nvidia GPU 内核模块没有正确加载。另一个表现是，/dev/nvidia0  /dev/nvidiactl  /dev/nvidia-uvm 这几个文件不都存在。

参考 nvidia-linux.md ，运行 nvidia-fix 脚本。

## cmake 编译安装

创建 build 目录，运行 cmake 或者 cmake-gui：
```
~/t-project/openpose.git/build$ cmake -D BUILD_PYTHON=ON ..
~/t-project/openpose.git/build$ cmake-gui ..
```

要编译 python 绑定，需要打开 BUILD_PYTHON 选项[3.1]。
默认情况下，选择链接到系统中最高版本的 python 。
如果要链接其它版本，可设置 cmake 变量 PYTHON_EXECUTABLE 和 PYTHON_LIBRARY 变量。两者都需要指定，否则可能找到不匹配的版本。
如果找不到 libpythonX.Ym.so ，可能是 python configure 时没有使用 `--enable-shared`，需要重新编译安装。
要编译针对多个 python 版本的库，可以变更 PYTHON_EXECUTABLE 和 PYTHON_LIBRARY ，多次 cmake 和 make install，中间无需 make clean 。
```
cmake -D BUILD_PYTHON=ON -D PYTHON_EXECUTABLE=/usr/bin/python2.7 -D PYTHON_LIBRARY=/usr/lib/x86_64-linux-gnu/libpython2.7m.so ..
cmake -D BUILD_PYTHON=ON -D PYTHON_EXECUTABLE=/usr/local/bin/python3.7 PYTHON_LIBRARY=/usr/local/lib/libpython3.7m.so ..
```

要编译 CPU 版本，可以设置 GPU_MODE 为 CPU_ONLY 。

开始编译、安装：
```
~/t-project/openpose.git/build$ make VERBOSE=1 -j4
~/t-project/openpose.git/build$ sudo make install
```

安装的结果是C++头文件和库文件。

python 绑定库会被安装到 /usr/local/python/openpose 目录。其中包含一个本地库，如 pyopenpose.cpython-35m-x86_64-linux-gnu.so ，所以虽然 openpose 的 python 代码没有严格的 python 版本依赖，它并不能轻易用于其它 python 小版本。

要卸载
```
sudo make uninstall
```

## conda 环境编译

目前并不成功。

因为 conda 环境的 python 版本可能与主机环境的不同，所以其中一个编译出来的 python 本地模块无法在另一个环境里使用，所以需要在 conda 里编译。

sudo apt-get autoremove libprotobuf-dev protobuf-compiler

CMake Error at /usr/local/share/cmake-3.16/Modules/FindPackageHandleStandardArgs.cmake:146 (message):
  Could NOT find Protobuf (missing: Protobuf_INCLUDE_DIR)
Protobuf_INCLUDE_DIR

经过测试，cmake 是通过 /usr/include/google/protobuf/* 可能还有 /usr/lib/x86_64-linux-gnu/libprotobuf.so 和 protoc 程序来检测 protobuf 的位置的。

cmake -DBUILD_PYTHON=ON -DProtobuf_INCLUDE_DIR=/home/duanyao/opt/miniconda3/include/ -DProtobuf_LIBRARY_DEBUG=/home/duanyao/opt/miniconda3/lib/libprotobuf.so -DProtobuf_LIBRARY_RELEASE=/home/duanyao/opt/miniconda3/lib/libprotobuf.so -DProtobuf_LITE_LIBRARY_DEBUG=/home/duanyao/opt/miniconda3/lib/libprotobuf-lite.so -DProtobuf_LITE_LIBRARY_RELEASE=/home/duanyao/opt/miniconda3/lib/libprotobuf-lite.so -DProtobuf_PROTOC_EXECUTABLE=/home/duanyao/opt/miniconda3/bin/protoc ..

cmake -DBUILD_PYTHON=ON -DProtobuf_PROTOC_EXECUTABLE=/usr/bin/protoc ..


错误：
```
../lib/libcaffe.so.1.0.0：对‘google::protobuf::io::EpsCopyOutputStream::WriteRawFallback(void const*, int, unsigned char*)’未定义的引用
../lib/libcaffe.so.1.0.0：对‘google::protobuf::internal::ArenaImpl::AllocateAlignedAndAddCleanup(unsigned long, void (*)(void*))’未定义的引用
```

## 为 conda 环境编译

目前并不成功。

```
cd ~/t-project/openpose.git/build2
cmake -D BUILD_PYTHON=ON -D PYTHON_LIBRARY=/home/duanyao/opt/miniconda3/envs/e2/lib/libpython3.so -D PYTHON_EXECUTABLE=/home/duanyao/opt/miniconda3/envs/e2/bin/python ..
make -j4
```

编译可以成功，但加载 python 模块错误：
```
$ python
Python 3.7.6 | packaged by conda-forge | (default, Jan  7 2020, 22:33:48) 
[GCC 7.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import sys
>>> sys.path.append('/home/duanyao/t-project/openpose.git/build2/python')
>>> import openpose
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/home/duanyao/t-project/openpose.git/build2/python/openpose/__init__.py", line 1, in <module>
    from . import pyopenpose as pyopenpose
ImportError: /lib/x86_64-linux-gnu/libz.so.1: version `ZLIB_1.2.9' not found (required by /home/duanyao/opt/miniconda3/envs/e2/bin/../lib/libpng16.so.16)
>>> 
```

## 测试和使用

### C++ 演示程序
```
./build/examples/openpose/openpose.bin --help
./build/examples/openpose/openpose.bin --model_pose COCO
./build/examples/openpose/openpose.bin --model_pose COCO --video examples/media/video.avi
```

会打开笔记本摄像头进行关键点识别。但是内存/显存不够可能会崩溃。
`--model_pose COCO` 使用较少的显存（1.5GB），默认的 BODY_25 使用 2.2GB。

其他参数：
```
    -num_gpu (The number of GPU devices to use. If negative, it will use all
      the available GPUs in your machine.) type: int32 default: -1
      
    -write_video (Full file path to write rendered frames) .avi - mjpg, mp4 - mp4v
    
    -number_people_max N limit the maximum number of people detected, by keeping the people with top scores. -1 keeps all
```
### 使用 python API

详细的接口可参考 `python/openpose/openpose_python.cpp` 。

openpose python 库依赖 python 包 numpy opencv-python，请用 pip 安装它们。

在引用的 python 源码中，应给 sys.path 添加 `/usr/local/python/`；
或者，添加 `build/python/` 目录也可以。

在 python 代码中， `import openpose` 导入模块。

运行测试程序（在 build/examples/tutorial_api_python 目录下）

```
python3 01_body_from_image.py --model_pose COCO --image_path xxx.jpg
python3 02_whole_body_from_image.py --model_pose COCO  --image_path xxx.jpg  # 显存超过 2GB
```
示例代码：

```
    sys.path.append('/usr/local/python/')
    from openpose import pyopenpose as op

    # Custom Params (refer to include/openpose/flags.hpp for more parameters)
    params = dict()
    params["model_folder"] = "../../../models/"
    params["model_pose"] = "COCO"
    params["face"] = True
    params["hand"] = True
    params["logging_level"] = 255    # 范围 [0, 255] ，实际有效范围 [0,4] ，越大则输出越少。

    # Starting OpenPose
    opWrapper = op.WrapperPython()
    opWrapper.configure(params)
    opWrapper.start()

    # Process Image
    datum = op.Datum()
    imageToProcess = cv2.imread(args[0].image_path)
    datum.cvInputData = imageToProcess
    opWrapper.emplaceAndPop([datum])

    # Display Image
    print("Body keypoints: \n" + str(datum.poseKeypoints))
    cv2.imshow("OpenPose 1.5.1 - Tutorial Python API", datum.cvOutputData)
    cv2.waitKey(0)
```

datum.poseKeypoints 表示人体关键点，是个3维数组（numpy.ndarray<float32>），形如：

```
[[[3.30695526e+02 2.09347595e+02 7.89797306e-01]
  [3.22870087e+02 2.17175858e+02 8.27622175e-01]
  [2.96825714e+02 2.23656158e+02 8.74126554e-01]
  [2.77137054e+02 2.51094589e+02 8.72107863e-01]
  [2.98079742e+02 2.74592743e+02 8.05892587e-01]
  [3.48960327e+02 2.13280090e+02 9.26894903e-01]
  [3.68522980e+02 2.35486877e+02 8.56039643e-01]
  [3.68562042e+02 2.64096466e+02 7.90570974e-01]
  [3.11124573e+02 2.86343689e+02 8.15169692e-01]
  [3.29349762e+02 3.35943970e+02 8.28685164e-01]
  [3.38507965e+02 3.94678528e+02 7.53254414e-01]
  [3.39891998e+02 2.83731384e+02 8.21358860e-01]
  [3.41199768e+02 3.33302643e+02 6.58944130e-01]
  [3.50295715e+02 3.76407013e+02 6.65692449e-01]
  [3.28128082e+02 2.04134186e+02 8.31253886e-01]
  [3.34612579e+02 2.04132263e+02 7.50680387e-01]
  [3.18926056e+02 2.02787109e+02 7.23795891e-01]
  [3.38558990e+02 2.02858765e+02 1.91599309e-01]]

 [[0.00000000e+00 0.00000000e+00 0.00000000e+00]
  [4.22018433e+02 3.05918823e+02 9.16581571e-01]
  [4.48189331e+02 3.08558624e+02 7.89462566e-01]
  [4.69048492e+02 3.48946564e+02 7.50825047e-01]
  [4.50774811e+02 3.69787750e+02 6.03651524e-01]
  [3.90741302e+02 2.99405212e+02 9.42914188e-01]
  [3.59373627e+02 3.30695496e+02 8.06404173e-01]
  [3.82890167e+02 3.56811279e+02 7.72375107e-01]
  [4.31216766e+02 3.85544830e+02 7.28283405e-01]
  [4.22021240e+02 4.48173157e+02 7.93440700e-01]
  [4.15540497e+02 4.76944672e+02 8.46055150e-02]
  [3.93391998e+02 3.82908173e+02 7.05052257e-01]
  [3.82949738e+02 4.40314117e+02 7.46244669e-01]
  [3.77657349e+02 4.76936768e+02 1.12249665e-01]
  [0.00000000e+00 0.00000000e+00 0.00000000e+00]
  [0.00000000e+00 0.00000000e+00 0.00000000e+00]
  [4.35176758e+02 2.82431091e+02 6.58771694e-01]
  [4.10313385e+02 2.78502808e+02 9.69453275e-01]]
]
```
第一层：每个元素对应一个人。
第二层：每个元素对应一个关键点。
第三层：x, y, 置信度。

关键点的顺序及其含义为：
```
// Result for BODY_25 (25 body parts consisting of COCO + foot)
// const std::map<unsigned int, std::string> POSE_BODY_25_BODY_PARTS {
//     {0,  "Nose"},
//     {1,  "Neck"},
//     {2,  "RShoulder"},
//     {3,  "RElbow"},
//     {4,  "RWrist"},
//     {5,  "LShoulder"},
//     {6,  "LElbow"},
//     {7,  "LWrist"},
//     {8,  "MidHip"},
//     {9,  "RHip"},
//     {10, "RKnee"},
//     {11, "RAnkle"},
//     {12, "LHip"},
//     {13, "LKnee"},
//     {14, "LAnkle"},
//     {15, "REye"},
//     {16, "LEye"},
//     {17, "REar"},
//     {18, "LEar"},
//     {19, "LBigToe"},
//     {20, "LSmallToe"},
//     {21, "LHeel"},
//     {22, "RBigToe"},
//     {23, "RSmallToe"},
//     {24, "RHeel"},
//     {25, "Background"}
// };
```

使用 COCO 模型时，只能检测前 18 个点，使用 BODY_25 则可检测 25 个点。

datum.poseScores 是一维数组(numpy.ndarray<float32>)形如：
```
[0.7939919  0.6204197  0.28680718 0.52333456]
```
每个元素对应一个人，表示整体的置信度。

op.WrapperPython 可以重复使用，op.Datum 则不应该重复使用。

## 资源占用

使用 GPU 时，COCO 模型、不检测脸和手时，到 opWrapper.start() 之后大约使用 890 MB 显存；开始检测图片后 (opWrapper.emplaceAndPop()) 占用 1541~1868 MB（与图片分辨率相关）。

python 环境中，如果 openpose 与 torch 同时使用，应注意导入的顺序。我们发现，如果 torch (1.4.0) 早于 openpose (1.5.1) 导入，则会在 opWrapper.start() 之后增加显存占到 1161MB，反之则没有影响。
即应采用如下顺序来确保不浪费显存：

```
from openpose import pyopenpose
import torch
```
## 日志

openpose 的日志输出到 stdout， 错误输出到 stderror， 这两者不可配置。日志的级别可以设置。openpose 启动时一般会输出两行日志：

```
Starting OpenPose Python Wrapper...
Auto-detecting all available GPUs... Detected 1 GPU(s), using 1 of them starting at GPU 0.
```

python/openpose/openpose_python.cpp:84:            opLog("Starting OpenPose Python Wrapper...", Priority::High);
include/openpose/wrapper/wrapperAuxiliary.hpp:185:                    opLog("Auto-detecting all available GPUs... Detected " + std::to_string(totalGpuNumber)

include/openpose/utilities/errorAndLog.hpp:81:    OP_API void opLog(

DEFINE_int32(logging_level,             3,              "The logging level. Integer in the range [0, 255]. 0 will output any opLog() message,"
                                                        " while 255 will not output any. Current OpenPose library messages are in the range 0-4:"
                                                        " 1 for low priority messages and 4 for important ones.");

OP_API void setPriorityThreshold(const Priority priorityThreshold);
include/openpose/utilities/errorAndLog.hpp:131:        OP_API void setLogModes(const std::vector<LogMode>& loggingModes)

```
include/openpose/utilities/enumClasses.hpp:14:    enum class LogMode : unsigned char

enum class ErrorMode : unsigned char
{
    StdRuntimeError,
    FileLogging,
    StdCerr,
    All,
};

enum class LogMode : unsigned char
{
    FileLogging,
    StdCout,
    All,
};
```

```
openpose.git/src/openpose/utilities/errorAndLog.cpp:

void opLog(
    const std::string& message, const Priority priority, const int line, const std::string& function,
    const std::string& file)
{
    if (priority >= ConfigureLog::getPriorityThreshold())
    {
        const auto infoMessage = createFullMessage(message, line, function, file);

        // std::cout
        if (checkIfLoggingHas(LogMode::StdCout))
            std::cout << infoMessage << std::endl;

        // File logging
        if (checkIfLoggingHas(LogMode::FileLogging))
            fileLogging(infoMessage);

        // Unity log
        #ifdef USE_UNITY_SUPPORT
            UnityDebugger::opLog(infoMessage);
        #endif
    }
}
```

## 操作系统升级后的问题

./build/examples/openpose/openpose.bin --model_pose COCO
./build/examples/openpose/openpose.bin: error while loading shared libraries: libhdf5_cpp.so.100: cannot open shared object file: No such file or directory

ldd ./build/examples/openpose/openpose.bin
ldd /usr/local/python/openpose/pyopenpose.cpython-37m-x86_64-linux-gnu.so

libgflags.so.2 => not found
libhdf5_cpp.so.100 => not found
libhdf5_serial.so.100 => not found

dpkg -S libgflags.so                                                                                                  
libgflags2.2: /usr/lib/x86_64-linux-gnu/libgflags.so.2.2                                                                                                                  
libgflags-dev: /usr/lib/x86_64-linux-gnu/libgflags.so                                                                                                                     
libgflags2.2: /usr/lib/x86_64-linux-gnu/libgflags.so.2.2.2

dpkg -S libhdf5_cpp.so
libhdf5-dev: /usr/lib/x86_64-linux-gnu/libhdf5_cpp.so
libhdf5-cpp-103:amd64: /usr/lib/x86_64-linux-gnu/libhdf5_cpp.so.103.0.0
libhdf5-cpp-103:amd64: /usr/lib/x86_64-linux-gnu/libhdf5_cpp.so.103
libhdf5-dev: /usr/lib/x86_64-linux-gnu/hdf5/serial/libhdf5_cpp.so

dpkg -S libhdf5_serial.so
libhdf5-103:amd64: /usr/lib/x86_64-linux-gnu/libhdf5_serial.so.103
libhdf5-103:amd64: /usr/lib/x86_64-linux-gnu/libhdf5_serial.so.103.0.0
libhdf5-dev: /usr/lib/x86_64-linux-gnu/libhdf5_serial.so

cd /usr/lib/x86_64-linux-gnu/
sudo ln -s libgflags.so.2.2 libgflags.so.2
sudo ln -s libhdf5_cpp.so.103 libhdf5_cpp.so.100
sudo ln -s libhdf5_serial.so.103 libhdf5_serial.so.100

## openvino 中的 openpose 模型
文档：

models/intel/human-pose-estimation-0001/description/human-pose-estimation-0001.md

### Inputs

Name: `data`, shape: [1x3x256x456]. An input image in the [BxCxHxW] format,
where:
  - B - batch size
  - C - number of channels
  - H - image height
  - W - image width
Expected color order is BGR.

### Outputs

The net outputs two blobs with the [1, 38, 32, 57] and [1, 19, 32, 57] shapes. The first blob contains keypoint pairwise relations (part affinity fields, PAF), while the second blob contains keypoint heatmaps.

https://docs.openvinotoolkit.org/latest/omz_models_model_human_pose_estimation_0001.html

Name: Mconv7_stage2_L1, shape: 1, 38, 32, 57 contains keypoint pairwise relations (part affinity fields).
Name: Mconv7_stage2_L2, shape: 1, 19, 32, 57 contains keypoint heatmaps.

## tf-openpose 中的 openpose 模型

tf_pose/pafprocess/pafprocess.cpp
