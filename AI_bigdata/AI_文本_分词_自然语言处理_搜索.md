## 参考资料

[3.x] 大数据+自然语言处理的一般资料

[3.1] 书 干净的数据：数据清洗入门与实践
  http://www.ituring.com.cn/book/1702
  https://www.zhihu.com/pub/book/119565016

[3.2] 书 Python网络数据采集 
  http://www.ituring.com.cn/book/1709
  https://github.com/REMitchell/python-scraping/  （附带源码）

[3.3] 文本挖掘（英文版）
  http://www.ituring.com.cn/book/292

[3.4] 资源整理 自然语言处理
  http://www.52nlp.cn/resources

[3.5] 书 Python自然语言处理(Natural Language Processing with Python)
  https://book.douban.com/subject/5336893/

[3.6] 书 大数据：互联网大规模数据挖掘与分布式处理（第2版）
  http://www.ituring.com.cn/book/1561

[3.7] 论文 基于python的中文分词的实现及应用 刘新亮 严姗姗
  http://www.cnblogs.com/appler/archive/2012/02/02/2335834.html

[3.8] 如何从非结构化文本中提取知识？
  https://www.zhihu.com/question/59624229

[3.9] 非结构化数据提取方案
  https://wenku.baidu.com/view/35e1635c16fc700aba68fc3f.html

[3.10] 从非结构化文本中提取知识
  https://www.jianshu.com/p/81d131e8adea

[3.11] 放弃幻想，全面拥抱Transformer：NLP三大特征抽取器（CNN/RNN/TF）比较
  https://blog.csdn.net/dQCFKyQDXYm3F8rB0/article/details/86446077

[3.12] 从Word Embedding到Bert模型——自然语言处理预训练技术发展史 
  https://mp.weixin.qq.com/s?__biz=MzI0ODcxODk5OA==&mid=2247499341&idx=2&sn=31a806928f2d94d2904e1c2f857c2580&chksm=e99ecdb4dee944a25f63829aba6a3a89efc4486711c885abd88c6c76c1106afa672271718bd9&scene=21#wechat_redirect

[4.x] 软件/工具
[4.1] Natural Language Toolkit
  http://www.nltk.org/

[4.2] “结巴”中文分词：做最好的 Python 中文分词组件
  https://github.com/fxsjy/jieba

[4.3] 百度中文词法分析（LAC）：中文分词、词性标注、专名识别
  https://github.com/baidu/lac

[4.4] NLPIR汉语分词系统：中文分词；英文分词；词性标注；命名实体识别；新词识别；关键词提取；支持用户专业词典与微博分析
  http://ictclas.nlpir.org/
  https://github.com/NLPIR-team/NLPIR

[4.5] 自然语言处理与信息检索共享平台
  http://www.nlpir.org/

[4.6] Classify Text With NLTK
  https://blog.csdn.net/fxjtoday/article/details/5862041

[4.7] 开源的，基于java语言开发的轻量级的中文分词工具包
  https://github.com/wks/ik-analyzer (停止维护)

[4.8] enterprise search platform built on Apache Lucene
  http://lucene.apache.org/solr/

[4.9] 百度的开源情感分类系统
  https://ctolib.com/baidu-Senta.html
  http://ai.baidu.com/tech/nlp/sentiment_classify

[4.10] 百度自然语言处理API服务
  http://ai.baidu.com/docs#/NLP-API/top

[5.x] NLP 简单算法

[5.1] 贝叶斯推断及其互联网应用（一）：定理简介
http://www.ruanyifeng.com/blog/2011/08/bayesian_inference_part_one.html

[5.2] 贝叶斯推断及其互联网应用（二）：过滤垃圾邮件
http://www.ruanyifeng.com/blog/2011/08/bayesian_inference_part_two.html
https://www.cnblogs.com/csguo/p/7804683.html

[5.3] 贝叶斯推断及其互联网应用（三）：拼写检查
http://www.ruanyifeng.com/blog/2012/10/spelling_corrector.html

[5.8] TF-IDF与余弦相似性的应用（一）：自动提取关键词
  http://www.ruanyifeng.com/blog/2013/03/tf-idf.html

[5.9] TF-IDF与余弦相似性的应用（二）：找出相似文章
  http://www.ruanyifeng.com/blog/2013/03/cosine_similarity.html

[5.10] TF-IDF与余弦相似性的应用（三）：自动摘要
  http://www.ruanyifeng.com/blog/2013/03/automatic_summarization.html

[6.x] 高级 NLP 算法

[6.1] PaddlePaddle 自然语言处理
http://paddlepaddle.org/docs/develop/models/README.cn.html

[6.2] 词向量
http://www.paddlepaddle.org/docs/0.14.0/documentation/fluid/zh/new_docs/beginners_guide/basics/word2vec/index.html

https://github.com/PaddlePaddle/book/blob/high-level-api-branch/04.word2vec/README.cn.md

[6.10] 情感倾向分析
http://ai.baidu.com/tech/nlp/sentiment_classify


[7.x] 数据清洗方法

[7.1] 一种无监督的对话数据清洗方法
 http://blog.einplus.cn/2017/08/07/einplus-purify-data.html

[7.2] 数据挖掘中常用的数据清洗方法有哪些？
 https://www.zhihu.com/question/22077960

[7.3] 机器学习基础与实践（一）----数据清洗
  http://www.cnblogs.com/charlotte77/p/5606926.html

[7.4] 用R可以做数据清洗吗？或者有更好的数据清洗工具？
  https://www.zhihu.com/question/39009270

[8.x] 大型语言模型

[8.1] LLaMA: Open and Efficient Foundation Language Models
  https://research.facebook.com/publications/llama-open-and-efficient-foundation-language-models/
  https://github.com/facebookresearch/llama

  
## 概念/关键词

教育数据挖掘（EDM）：应用统计学、 机器学习和数据
挖掘的技术和开发方法，对教学和学习过程中收集
的数据进行分析，教育数据挖掘检验学习理论并引
导教育实践[1.2]。

学习分析技术（LA）：挖掘学习者学习过程数据中的有价值信息，进而优化学习，助力教学决策[1.1]。

文本挖掘（text mining）：是对自然语言正文中所包含数据的分析

自然语言处理(NLP) ：

## 简单处理方法
### 贝叶斯推断
过滤垃圾邮件、拼写检查[5.1-5.3]。
 
### TF-IDF
TF-IDF：term frequency（词频）–inverse document frequency（逆文档频率）

用途 自动提取关键词、找出相似文章、自动摘要[5.8-5.10]。

## 高级算法
[6.1-6.10]
词向量
RNN 语言模型
文本分类
排序学习
深度结构化语义模型
命名实体识别
序列到序列学习
阅读理解
自动问答
情感倾向分析[6.10]

## 软件/工具

## 大型语言模型

### LLaMA

We introduce LLaMA, a collection of foundation language models ranging from 7B to 65B parameters. We train our models on trillions of tokens, and show that it is possible to train state-of-the-art models using publicly available datasets exclusively, without resorting to proprietary and inaccessible datasets. In particular, LLaMA-13B outperforms GPT-3 (175B) on most benchmarks, and LLaMA-65B is competitive with the best models, Chinchilla70B and PaLM-540B. We release all our models to the research community.
