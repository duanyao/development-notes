## 参考资料

[1.3]  detectron2/INSTALL.md
  https://github.com/facebookresearch/detectron2/blob/master/INSTALL.md

[2.1] Detectron2 Model Zoo and Baselines
  https://github.com/facebookresearch/detectron2/blob/master/MODEL_ZOO.md

## 安装

安装 detectron2：[1.3]

```
git clone https://github.com/facebookresearch/detectron2.git detectron2.git
cd detectron2.git

pip install -e .  #在 venv 中

# 或者
python setup.py sdist bdist_wheel
```
安装中涉及C++代码的编译。如果要重新编译，应删除以下文件

```
rm -Rf build/
rm ./detectron2/_C.cpython-39-x86_64-linux-gnu.so
```

也可以安装预编译版本（cu100 对应 cuda 10.0，如果版本不同请相应修改，更高版本有 cu110、cu113等）：
```
pip install detectron2 -f https://dl.fbaipublicfiles.com/detectron2/wheels/cu100/index.html
```

经过测试，detectron2 0.6+cu113 预编译版本在 ubuntu 20.04 上无法运行，有链接错误。

## 模型

### 模型下载

url `detectron2://...` 在远程对应 `https://dl.fbaipublicfiles.com/detectron2/...`，本地对应 `~/.torch/iopath_cache/detectron2/...` 或者 `~/.torch/fvcore_cache/detectron2/...` （旧版） 。

### FPN

### 对象检测（给出包围盒）
R50-FPN: 

例如，模型 `https://dl.fbaipublicfiles.com/detectron2/COCO-Detection/faster_rcnn_R_50_FPN_3x/137849458/model_final_280758.pkl` 。
下载后复制或链接为 `~/.torch/fvcore_cache/detectron2/COCO-Detection/faster_rcnn_R_50_FPN_3x/137849458/model_final_280758.pkl` 。

### 实例分割（轮廓，Instance Segmentation）

R50-FPN:

https://dl.fbaipublicfiles.com/detectron2/COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x/137849600/model_final_f10217.pkl

~/.torch/fvcore_cache/detectron2/COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x/137849600/model_final_f10217.pkl

### 人体关键点检测

https://dl.fbaipublicfiles.com/detectron2/COCO-Keypoints/keypoint_rcnn_R_50_FPN_3x/137849621/model_final_a6e10b.pkl

~/.torch/fvcore_cache/detectron2/COCO-Keypoints/keypoint_rcnn_R_50_FPN_3x/137849621/model_final_a6e10b.pkl

### Panoptic Segmentation

https://dl.fbaipublicfiles.com/detectron2/COCO-PanopticSegmentation/panoptic_fpn_R_50_3x/139514569/model_final_c10459.pkl

## 运行演示程序

### 对象检测（给出包围盒）

cd demo/

python demo.py --config-file ../configs/COCO-Detection/faster_rcnn_R_50_FPN_3x.yaml \
  --input /media/DATA/temp/cr1/16.jpg \
  --opts MODEL.WEIGHTS detectron2://COCO-Detection/faster_rcnn_R_50_FPN_3x/137849458/model_final_280758.pkl

python demo.py --config-file ../configs/COCO-Detection/faster_rcnn_R_50_FPN_3x.yaml \
  --input /media/DATA/temp/cr1/*.jpg --output /media/DATA/temp/cr1-det/ \
  --opts MODEL.WEIGHTS detectron2://COCO-Detection/faster_rcnn_R_50_FPN_3x/137849458/model_final_280758.pkl
  
python demo.py --config-file ../configs/COCO-Detection/faster_rcnn_R_50_FPN_3x.yaml  --video-input /home/duanyao/Downloads/ch1/教师打孩子.mp4 --output /home/duanyao/Downloads/ch1/教师打孩子-is.mp4 --opts MODEL.WEIGHTS detectron2://COCO-Detection/faster_rcnn_R_50_FPN_3x/137849458/model_final_280758.pkl

python demo.py --config-file ../configs/COCO-Detection/faster_rcnn_R_50_FPN_3x.yaml \
  --webcam \
  --opts MODEL.WEIGHTS detectron2://COCO-Detection/faster_rcnn_R_50_FPN_3x/137849458/model_final_280758.pkl

效率：
幼儿园场景，1024x576，1.6-1.7s/帧，内存1.9GB，显存 1.1GB 。
摄像头，1.5s/帧，，内存1.7GB，显存 0.99GB 。

效率与图像分辨率、对象数量有一定的关系。

### 人体关键点检测

cd demo/

python demo.py --config-file ../configs/COCO-Keypoints/keypoint_rcnn_R_50_FPN_3x.yaml \
  --webcam \
  --opts MODEL.WEIGHTS detectron2://COCO-Keypoints/keypoint_rcnn_R_50_FPN_3x/137849621/model_final_a6e10b.pkl

python demo.py --config-file ../configs/COCO-Keypoints/keypoint_rcnn_R_50_FPN_3x.yaml \
  --video-input /media/DATA/emulated-oss/temp/body-key-point-input/-1177908937204806502/2019-06-21GMT+8/1561101023349.mp4 \
  --opts MODEL.WEIGHTS detectron2://COCO-Keypoints/keypoint_rcnn_R_50_FPN_3x/137849621/model_final_a6e10b.pkl

python demo.py --config-file ../configs/COCO-Keypoints/keypoint_rcnn_R_50_FPN_3x.yaml \
  --input /media/DATA/temp/cr1/*.jpg --output /media/DATA/temp/cr1-key/ \
  --opts MODEL.WEIGHTS detectron2://COCO-Keypoints/keypoint_rcnn_R_50_FPN_3x/137849621/model_final_a6e10b.pkl

效率（GeForce 940MX）：
幼儿园场景，1024x576，2.0-2.2s/帧，内存1.7GB，显存 1.3GB 。
摄像头，1.6s/帧，，内存1.1GB，显存 0.99GB 。

效率与图像分辨率、对象数量有一定的关系。

效率不如 openpose 的 1s/帧。

### 实例分割（轮廓，Instance Segmentation）

cd demo/
  
python demo.py --config-file ../configs/COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml \
  --webcam \
  --opts MODEL.WEIGHTS detectron2://COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x/137849600/model_final_f10217.pkl

python demo.py --config-file ../configs/COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml \
  --video-input /media/DATA/emulated-oss/temp/body-key-point-input/-1177908937204806502/2019-06-21GMT+8/1561101023349.mp4 \
  --opts MODEL.WEIGHTS detectron2://COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x/137849600/model_final_f10217.pkl

python demo.py --config-file ../configs/COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml \
  --input /media/DATA/temp/cr1/*.jpg --output /media/DATA/temp/cr1-seg/ \
  --opts MODEL.WEIGHTS detectron2://COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x/137849600/model_final_f10217.pkl
  
python demo.py --config-file ../configs/COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml  --video-input /home/duanyao/Downloads/ch1/幼儿互相打闹.mp4 --output /home/duanyao/Downloads/ch1/幼儿互相打闹-is.mp4 --opts MODEL.WEIGHTS detectron2://COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x/137849600/model_final_f10217.pkl
  
效率：
幼儿园场景，1024x576，1.8-1.9s/帧，内存1.8GB，显存 1.4GB 。
摄像头，1.5s/帧，，内存1.7GB，显存 0.99GB 。

效率与图像分辨率、对象数量有一定的关系。

## 代码分析

### detectron2.utils.visualizer.Visualizer

src:detectron2/utils/visualizer.py


Visualizer.draw_instance_predictions(predictions: Instances) # Instances = detectron2.structures.instances.Instances

draw_binary_mask(self, binary_mask, color=None, *, edge_color=None, text=None, alpha=0.5, area_threshold=4096)

draw_polygon

### GenericMask
有两种数据结构：多边形（组）；二值图像（组）。demo 的 InstanceSegmentation 采用后者。
