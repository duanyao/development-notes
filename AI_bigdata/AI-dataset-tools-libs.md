## 参考资料

[5.1] https://github.com/dvschultz/dataset-tools

5 Tools To Create A Custom Object Detection Dataset
https://medium.com/@lekorotkov/5-tools-to-create-a-custom-object-detection-dataset-27ca37f91e05

VGG Image Annotator

Pytorch的ConcatDataset
https://zhuanlan.zhihu.com/p/222772996

fiftyone-examples 
https://github.com/voxel51/fiftyone-examples

针对特定领域扩展 TensorFlow 的应用软件包。
https://www.tensorflow.org/resources/libraries-extensions

a set of tools to help create COCO datasets. It includes functions to generate annotations in uncompressed RLE ("crowd") and polygons in the format COCO requires.
https://github.com/waspinator/pycococreator/

Tutorial 2: Customize Datasets Concatenate dataset
https://mmdetection.readthedocs.io/en/latest/tutorials/customize_dataset.html#concatenate-dataset

img2dataset Easily turn large sets of image urls to an image dataset. Can download, resize and package 100M urls in 20h on one machine.
https://github.com/rom1504/img2dataset

## 标注工具

## 转换、编辑工具

### datumaro
见 AI-openvino-datumaro-dataset-tool.md

### FiftyOne

Merging datasets is an easy way to:

Combine multiple datasets with information about the same underlying raw media (images and videos)

Add model predictions to a FiftyOne dataset, to compare with ground truth annotations and/or other models

### polimorfo

https://github.com/fabiofumarola/polimorfo

a dataset loader and converter library for object detection segmentation and classification. The goal of the project is to create a library able to process dataset in format:

    COCO: Common Objects in Context
    Pascal VOC: Visual Object Classes Challenge
    Google Open Images: Object Detection and Segmentation dataset released by Google

and transform these dataset into a common format (COCO).

Moreover, the library offers utilies to handle (load, convert, store and transform) the various type of annotations. This is important when you need to: - convert mask to polygons - store mask in a efficient format - convert mask/poygons into bounding boxes

### dataset_format_conversion_for_remote_sensing_object_detection (RSOD)

https://github.com/zhangxin-xd/dataset_format_conversion

## 评估工具

### COCO API

### review_object_detection_metrics
见 AI-模型指标评价-model-metrics-performace.md

## 加载工具

Pytorch的ConcatDataset

## 增广工具

## 综合工具
Tools for quickly normalizing image datasets for machine learning.  [5.1]
