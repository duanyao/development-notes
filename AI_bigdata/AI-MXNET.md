## 参考资料

[1.1] Apache MXNet
http://mxnet.incubator.apache.org/
https://github.com/apache/incubator-mxnet

[1.2] 
https://mxnet.apache.org/get_started

[10.1] MXNet专栏 | 李沐：为什么强大的MXNet一直火不起来？
 http://www.sohu.com/a/119098283_465975

## 安装

2021.6.23

python 采用 venv 环境（参考 python.md），版本 3.7-3.9。
cuda 版本 10.0-11.0（参考 nvidia-linux.md ），同时安装 cudnn 和 NCCL ，采用系统级安装。如果不打算用cuda可以忽略。
mxnet 采用 1.8.0 。

```
pip install mxnet-cu110=1.8.0 # cuda 版，cuda 11.0
pip install mxnet=1.8.0       # CPU 版
```
cuda 版与 CPU 版不可以同时安装（但 pip 不会阻止），它们有一些冲突的文件。

验证安装：

```
python
>>>import mxnet as mx
>>>a = mx.nd.ones((2, 3)) # a = mx.nd.ones((2, 3), mx.gpu()) # GPU 版
>>>b = a * 2 + 1
>>>b.asnumpy()
```

与 pypy 的兼容性：当前（2021.8）的最新版，mxnet-1.8.0 与 pypy-3.7-7.3.5 是不兼容的， `import mxnet` 会报错 `AttributeError: module 'ctypes' has no attribute 'pythonapi'` 。
pypy 尚未实现 ctypes.pythonapi 。

## 数组操作
```
>>> import mxnet
>>> nd=mxnet.nd
>>> nd.array([1,2,3])

>>> a=nd.array([[1,2],[3,4],[5,6]])
>>> a

[[1. 2.]
 [3. 4.]
 [5. 6.]]
<NDArray 3x2 @cpu(0)>

>>> a.shape
(3, 2)
```

看做多维数组时，最外层的数组是第一轴，次外层的数组是第二轴，以此类推。

```
>>> b=nd.zeros((3, 4))
>>> b

[[0. 0. 0. 0.]
 [0. 0. 0. 0.]
 [0. 0. 0. 0.]]
<NDArray 3x4 @cpu(0)>

>>> b=nd.ones((3, 4))
>>> b

[[1. 1. 1. 1.]
 [1. 1. 1. 1.]
 [1. 1. 1. 1.]]
<NDArray 3x4 @cpu(0)>

>>> y = nd.random_normal(0, 1, shape=(3, 4))
>>> y

[[ 2.2122064   0.7740038   1.0434403   1.1839255 ]
 [ 1.8917114  -1.2347414  -1.771029   -0.45138445]
 [ 0.57938355 -1.856082   -1.9768796  -0.20801921]]
<NDArray 3x4 @cpu(0)>

>>> z = nd.zeros_like(y)
>>> z

[[0. 0. 0. 0.]
 [0. 0. 0. 0.]
 [0. 0. 0. 0.]]
<NDArray 3x4 @cpu(0)>

>>> y.shape
(3, 4)
>>> y.size
12
>>> y.dtype
<class 'numpy.float32'>

```

## 故障排除

### cuda 版本依赖问题
mxnet 的 pip 安装包依赖特定 cuda 二级版本。例如 mxnet-cu110-1.8.0.post0 依赖 cuda 11.0 ，高或低都不行。
如果 cuda 版本较高，创建低版本的 .so 的符号链接并设置 LD_LIBRARY_PATH 也不一定可行（ mxnet-cu110-1.8.0.post0 不行）。

例如 python 里 import mxnet 报错：

```
OSError: libcusolver.so.10: cannot open shared object file: No such file or directory
```

在 mxnet 安装位置下找到  libmxnet.so ，用 ldd 测试：

```
ldd /home/duanyao/opt/venv/v39/lib/python3.9/site-packages/mxnet/libmxnet.so | grep not
        libcusolver.so.10 => not found
        libnvrtc.so.11.0 => not found
```

libnccl.so.2 在包 libnccl2 里，参考 nvidia-linux.md 安装即可。

```
ls -l /usr/local/cuda-11.3/targets/x86_64-linux/lib/libcusolver.so*
lrwxrwxrwx 1 root root        17 5月  14 07:26 /usr/local/cuda-11.3/targets/x86_64-linux/lib/libcusolver.so -> libcusolver.so.11
lrwxrwxrwx 1 root root        25 5月  14 07:26 /usr/local/cuda-11.3/targets/x86_64-linux/lib/libcusolver.so.11 -> libcusolver.so.11.1.2.109
-rw-r--r-- 1 root root 214657768 5月  14 07:26 /usr/local/cuda-11.3/targets/x86_64-linux/lib/libcusolver.so.11.1.2.109

ls -l /usr/local/cuda-11.3/targets/x86_64-linux/lib/libnvrtc.so*
lrwxrwxrwx 1 root root       16 5月   5 01:24 /usr/local/cuda-11.3/targets/x86_64-linux/lib/libnvrtc.so -> libnvrtc.so.11.2
lrwxrwxrwx 1 root root       20 5月   5 01:24 /usr/local/cuda-11.3/targets/x86_64-linux/lib/libnvrtc.so.11.2 -> libnvrtc.so.11.3.109
-rw-r--r-- 1 root root 44387152 5月   5 01:24 /usr/local/cuda-11.3/targets/x86_64-linux/lib/libnvrtc.so.11.3.109
```

创建相应的链接后，并设置 LD_LIBRARY_PATH 问题依旧。


