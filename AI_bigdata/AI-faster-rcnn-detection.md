## 参考资料

detectron2 (contains Faster R-CNN)
  https://github.com/facebookresearch/detectron2

mmdetection (contains Faster R-CNN)
  https://github.com/open-mmlab/mmdetection/tree/master/configs/faster_rcnn

物体检测 Faster R-CNN(一) 加载训练数据
  https://blog.csdn.net/WYXHAHAHA123/article/details/86099768

A Faster Pytorch Implementation of Faster R-CNN
  https://github.com/jwyang/faster-rcnn.pytorch

py-faster-rcnn
  https://github.com/rbgirshick/py-faster-rcnn

Batch size of faster rcnn
  https://github.com/rbgirshick/py-faster-rcnn/issues/487
