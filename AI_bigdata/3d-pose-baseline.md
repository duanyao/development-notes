## 训练
数据集 h36m 放在 `data/h36m` 下，约 1GB。

一轮训练：

`python src/predict_3dpose.py --camera_frame --residual --batch_norm --dropout 0.5 --max_norm --evaluateActionWise --use_sh --epochs 1`

参数：
```
"camera_frame", False, "Convert 3d poses to camera coordinates"
"use_sh", False, "Use 2d pose predictions from StackedHourglass"
"max_norm", False, "Apply maxnorm constraint to the weights"
"batch_norm", False, "Use batch_normalization"
"evaluateActionWise",False, "The dataset to use either h36m or heva"
"procrustes", False, "Apply procrustes analysis at test time"
"load", 0, "Try to load a previous checkpoint." # 0 新建，不加载
"sample", False, "Set to True for sampling." "Get samples from a model and visualize them"
```

产物：

```
experiments/All/dropout_0.5/epochs_1/lr_0.001/residual/depth_2/linear_size1024/batch_size_64/no_procrustes/maxnorm/batch_normalization/use_stacked_hourglass/predict_17/
  checkpoint-24371.data-00000-of-00001  # ~50MB
  checkpoint-24371.index
  checkpoint-24371.meta
  checkpoint
```

## 演示

```
python src/predict_3dpose.py --camera_frame --residual --batch_norm --dropout 0.5 --max_norm --evaluateActionWise --use_sh --epochs 1 --sample --load 24371
```

## 3d-pose-baseline-vmd 的输出

产物：

pos.txt

```
关节号1 x1 y1 x1,关节号2 x2 y2 x2...\n
关节号1 x1 y1 x1,关节号2 x2 y2 x2...\n
...
```
关节号采用 H3.6M 的定义，从小到大排列。

## 代码分析

###
src/predict_3dpose.py:400
def sample():

### 预测

_, _, poses3d = model.step(sess, enc_in, dec_out, dp, isTraining=False)

enc_in: numpy.ndarray<float64,(1,64)>, 32 个关节点（H3.6M）的 x,y 数据。
poses3d: numpy.ndarray<float64,(1,96)>, 32 个关节点（H3.6M）的 x,y,z 数据。
dec_out: numpy.ndarray<float64,(1,48)>, 不清楚作用，全0。
dp: 1.0

batch size: 123

GPU: 65%
sample(): time_per_infer=0.012,time_per_infer_sample=0.000095

CPU: 75%
sample(): time_per_infer=0.032,time_per_infer_sample=0.000241

batch size 减少到 1 时，time_per_infer (GPU) 是 0.009 ，并非成比例下降。

## 附录

### 关节定义：

```
# Joints in H3.6M -- data has 32 joints, but only 17 that move; these are the indices.
H36M_NAMES = ['']*32
H36M_NAMES[0]  = 'Hip'
H36M_NAMES[1]  = 'RHip'
H36M_NAMES[2]  = 'RKnee'
H36M_NAMES[3]  = 'RFoot'
H36M_NAMES[6]  = 'LHip'
H36M_NAMES[7]  = 'LKnee'
H36M_NAMES[8]  = 'LFoot'
H36M_NAMES[12] = 'Spine'
H36M_NAMES[13] = 'Thorax'
H36M_NAMES[14] = 'Neck/Nose'
H36M_NAMES[15] = 'Head'
H36M_NAMES[17] = 'LShoulder'
H36M_NAMES[18] = 'LElbow'
H36M_NAMES[19] = 'LWrist'
H36M_NAMES[25] = 'RShoulder'
H36M_NAMES[26] = 'RElbow'
H36M_NAMES[27] = 'RWrist'
```

```
# Stacked Hourglass produces 16 joints. These are the names.
SH_NAMES = ['']*16
SH_NAMES[0]  = 'RFoot'
SH_NAMES[1]  = 'RKnee'
SH_NAMES[2]  = 'RHip'
SH_NAMES[3]  = 'LHip'
SH_NAMES[4]  = 'LKnee'
SH_NAMES[5]  = 'LFoot'
SH_NAMES[6]  = 'Hip'
SH_NAMES[7]  = 'Spine'
SH_NAMES[8]  = 'Thorax'
SH_NAMES[9]  = 'Head'
SH_NAMES[10] = 'RWrist'
SH_NAMES[11] = 'RElbow'
SH_NAMES[12] = 'RShoulder'
SH_NAMES[13] = 'LShoulder'
SH_NAMES[14] = 'LElbow'
SH_NAMES[15] = 'LWrist'
```

```
// openpose: https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/output.md 
// Result for BODY_25 (25 body parts consisting of COCO + foot)
// const std::map<unsigned int, std::string> POSE_BODY_25_BODY_PARTS {
//     {0,  "Nose"},
//     {1,  "Neck"},
//     {2,  "RShoulder"},
//     {3,  "RElbow"},
//     {4,  "RWrist"},
//     {5,  "LShoulder"},
//     {6,  "LElbow"},
//     {7,  "LWrist"},
//     {8,  "MidHip"},
//     {9,  "RHip"},
//     {10, "RKnee"},
//     {11, "RAnkle"},
//     {12, "LHip"},
//     {13, "LKnee"},
//     {14, "LAnkle"},
//     {15, "REye"},
//     {16, "LEye"},
//     {17, "REar"},
//     {18, "LEar"},
//     {19, "LBigToe"},
//     {20, "LSmallToe"},
//     {21, "LHeel"},
//     {22, "RBigToe"},
//     {23, "RSmallToe"},
//     {24, "RHeel"},
//     {25, "Background"}
// };
```
