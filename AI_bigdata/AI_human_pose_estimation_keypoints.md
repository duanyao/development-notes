## 参考资料

[4.1] mmpose toolbox for pose estimation based on PyTorch
https://github.com/open-mmlab/mmpose

[8.1] 卡内基梅隆大学的开源项目 OpenPose
  https://github.com/CMU-Perceptual-Computing-Lab/openpose

[9.1] AlphaPose
  https://github.com/MVIG-SJTU/AlphaPose/

[10.1] A simple yet effective baseline for 3d human pose estimation. In ICCV, 2017
  https://github.com/una-dinosauria/3d-pose-baseline
  https://arxiv.org/pdf/1705.03098.pdf

[11.1] Single-Shot Multi-Person 3D Pose Estimation From Monocular RGB
  https://arxiv.org/abs/1712.03453

[11.2] human-pose-estimation-3d-0001 open_model_zoo
  https://github.com/opencv/open_model_zoo/blob/master/models/public/human-pose-estimation-3d-0001/description/human-pose-estimation-3d-0001.md

[12.1] Unsupervised Adversarial Learning of 3D Human Pose from 2D Joint Locations 2018
  https://arxiv.org/abs/1803.08244
  https://github.com/DwangoMediaVillage/3dpose_gan

[13.1] MoveNet fast 17 keypoints of a body. pose-detection
  https://github.com/tensorflow/tfjs-models/tree/master/pose-detection/src/movenet

[100]
  /home/duanyao/doc.personal/development/AI_bigdata/AI_计算机视觉_图像_视频.md
