[1.1]
  https://pypi.org/project/ai-benchmark/
  http://ai-benchmark.com/alpha

[1.2] RTX 2080 Ti Deep Learning Benchmarks with TensorFlow - 2019
  https://lambdalabs.com/blog/2080-ti-deep-learning-benchmarks/
  
[2.1] tensorflow模型基准测试benchmark
  https://zhuanlan.zhihu.com/p/86439306
  
[2.2] perfzero
  https://github.com/tensorflow/benchmarks/tree/master/perfzero

[3.1] cuda-z
  http://cuda-z.sourceforge.net/

[4.1] Compute Performance
  http://tech4gizmos.com/compute-performance-of-your-system/

[2.1]
 /home/duanyao/opt/venv/v37/lib/python3.7/site-packages/tensorflow_core/python/platform/benchmark.py
 
/home/duanyao/opt/venv/v37/lib/python3.7/site-packages/tensorflow_core/python/data/benchmarks


## ai-benchmark

from ai_benchmark import AIBenchmark
benchmark = AIBenchmark(use_CPU=None, verbose_level=1)
results = benchmark.run()
