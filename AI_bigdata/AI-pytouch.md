## 参考资料
[1.1] pytorch
http://pytorch.org/

[1.2] pytorch 各种版本
http://pytorch.org/previous-versions/

[3.1] CUDA SDK
https://developer.nvidia.com/cuda-downloads

[3.2]
https://github.com/pytorch/pytorch/issues/686

[3.3] 
https://developer.nvidia.com/cuda-gpus

[2.1] PyTorch深度学习：60分钟入门(Translation)
https://zhuanlan.zhihu.com/p/25572330
http://pytorch.org/tutorials/beginner/blitz/cifar10_tutorial.html

[2.2] PyTorch Examples
https://github.com/pytorch/examples

[4.1] The CIFAR-10 dataset
https://www.cs.toronto.edu/~kriz/cifar.html

[5.1] Best way to save a trained model in PyTorch?
https://stackoverflow.com/questions/42703500/best-way-to-save-a-trained-model-in-pytorch

## 安装
### conda 安装（不推荐）

安装 miniconda
从这里下载 Miniconda2-latest-Linux-x86_64.sh https://conda.io/miniconda.html

#### 全局安装（不建议）
sudo sh  Miniconda2-latest-Linux-x86_64.sh
选择安装位置：/opt/miniconda2/
安装 pytorch 和 cuda80
sudo /opt/miniconda2/bin/conda install pytorch cuda80 -c soumith

#### 当前用户安装
sh  Miniconda3-latest-Linux-x86_64.sh
选择安装位置：~/miniconda3/
安装 pytorch 和 cuda80
~/miniconda3/bin/conda install pytorch cuda80 -c soumith

下载的包在 /home/duanyao/miniconda3/pkgs
可能很大。

https://conda.anaconda.org/soumith/linux-64/pytorch-0.2.0-py36h53baedd_4cu80.tar.bz2

不过下载容易失败。

### virtualenv PIP 安装（推荐）

安装文件：http://pytorch.org/previous-versions/

wget -c http://download.pytorch.org/whl/cu80/torch-0.2.0.post3-cp35-cp35m-manylinux1_x86_64.whl

cp35 是指 python 版本。用 wget 是为了防止下载大文件失败。

sudo apt-get install python3-virtualenv
virtualenv -p python3 pytorch1
cd pytorch1/
bin/pip install ~/path/to/torch-0.2.0.post3-cp35-cp35m-manylinux1_x86_64.whl

从网络仓库装其它的 pip 包：

bin/pip install torchvision
bin/pip install matplotlib

图形界面库：
sudo apt-get install python3-tk

### 测试
bin/python #当前virtualenv目录下的python

from __future__ import print_function
import torch
x = torch.Tensor(5, 3)  # 构造一个未初始化的5*3的矩阵
x = torch.rand(5, 3)  # 构造一个随机初始化的矩阵
x # 此处在notebook中输出x的值来查看具体的x内容
x.size()

### 安装 cuda
debian linux 系列：
sudo apt install nvidia-cuda-dev nvidia-cuda-toolkit
这个比较大，安装后1.5GB。
也可以从 nvidia 官网下载。

安装后，在python里用
torch.cuda.is_available()
来验证是否可用。

x.cuda()
可以将数据复制到GPU上。
GPU上的数据打印时会输出类似这样的一行：
[torch.cuda.FloatTensor of size 5x3 (GPU 0)]
而内存里的则是：
[torch.FloatTensor of size 5x3]

不过，pytorch 支持的最低 CUDA Capability 是 3.0，比较老的 nv 显卡是不支持的，例如 540M 是 2.1。相比之下，750M 是 3.0，1080是6.1。


## 使用
### 导入、准备
from __future__ import print_function
import numpy as np
import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import matplotlib
%matplotlib inline

### 算例/数据集
#### CIFAR-10 
60000 32x32 colour images in 10 classes, with 6000 images per class. There are 50000 training images and 10000 test images [4.1]. 

大约 170M（压缩后）。

trainset = torchvision.datasets.CIFAR10(root='./data', train=True, download=True, transform=transform)


#### CIFAR-10 分类实验
[2.1]

```
from __future__ import print_function
import numpy as np
import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import matplotlib


# 装载和归一化图像数据

transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
trainset = torchvision.datasets.CIFAR10(root='./data', train=True,
                                        download=True, transform=transform)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=4,
                                          shuffle=True, num_workers=2)
testset = torchvision.datasets.CIFAR10(root='./data', train=False,
                                       download=True, transform=transform)
testloader = torch.utils.data.DataLoader(testset, batch_size=4,
                                         shuffle=False, num_workers=2)
classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')


# functions to show an image

def imshow(img):
    img = img / 2 + 0.5     # unnormalize
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))


# get some random training images
dataiter = iter(trainloader)
images, labels = dataiter.next()
# show images
imshow(torchvision.utils.make_grid(images))
# print labels
print(' '.join('%5s' % classes[labels[j]] for j in range(4)))


plt.show() # 在GUI中显示结果。仅仅执行 plt.imshow() 是不够的。



#Define a Convolution Neural Network

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5) # 6 是宽度，增加宽度对精度影响不大
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)
    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 5 * 5)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

net = Net()

#Define a Loss function and optimizer

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

#Train the network. 可以重复运行几次，loss 值会逐渐下降

for epoch in range(2):  # loop over the dataset multiple times
    running_loss = 0.0
    for i, data in enumerate(trainloader, 0):
        # get the inputs
        inputs, labels = data
        # wrap them in Variable
        inputs, labels = Variable(inputs), Variable(labels)
        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
        # print statistics
        running_loss += loss.data[0]
        if i % 2000 == 1999:    # print every 2000 mini-batches
            print('[%d, %5d] loss: %.3f' %
                  (epoch + 1, i + 1, running_loss / 2000))
            running_loss = 0.0


#Test the network on the test data
dataiter = iter(testloader)
images, labels = dataiter.next()

# print images
imshow(torchvision.utils.make_grid(images))
print('GroundTruth: ', ' '.join('%5s' % classes[labels[j]] for j in range(4)))
plt.show()

# see what the neural network thinks these examples above are:
outputs = net(Variable(images))
_, predicted = torch.max(outputs.data, 1)
print('Predicted: ', ' '.join('%5s' % classes[predicted[j]]
                              for j in range(4)))


#how the network performs on the whole dataset.
correct = 0
total = 0
for data in testloader:
    images, labels = data
    outputs = net(Variable(images))
    _, predicted = torch.max(outputs.data, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()

print('Accuracy of the network on the 10000 test images: %d %%' % (
    100 * correct / total))
    
#what are the classes that performed well

class_correct = list(0. for i in range(10))
class_total = list(0. for i in range(10))
for data in testloader:
    images, labels = data
    outputs = net(Variable(images))
    _, predicted = torch.max(outputs.data, 1)
    c = (predicted == labels).squeeze()
    for i in range(4):
        label = labels[i]
        class_correct[label] += c[i]
        class_total[label] += 1


for i in range(10):
    print('Accuracy of %5s : %2d %%' % (
        classes[i], 100 * class_correct[i] / class_total[i]))


#保存模型[5.1]

torch.save(net.state_dict(), 'CIFAR-10.2.pytm')

#加载模型：
net.load_state_dict(torch.load('CIFAR-10.2.pytm'))
net.eval()

```

训练运行一次，正确率54%，运行3-4次可提高到63% 。增加 .conv1 和 .conv2 的宽度对正确率的影响不大。

保存 net.state_dict() 可以用于推断，但不能用于继续训练，继续训练还需要保存更多的状态，例如：

```
state = {
    'epoch': epoch, # 不知道是什么？
    'state_dict': net.state_dict(),
    'optimizer': optimizer.state_dict(),
    ...
}
torch.save(state, filepath)
```
恢复：
```
state = torch.load(filepath)
net.load_state_dict(state['state_dict'])
optimizer.load_state_dict(stata['optimizer'])
```
保存的模型并不大，CIFAR-10.2.pytm 大约 253KB。
