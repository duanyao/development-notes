
## 安装

### 直接 pip 安装
直接 pip 安装的命令：
```
pip install pycocotools
```
2024.8.21（pycocotools==2.0.8，py=3.9|3.11, os=ubuntu20.04|deepin 23RC2, x86-64），直接 pip 安装是可行的，无需编译。
2023.8.9（2023.8.9, pycocotools==2.0.6， os=ubuntu20.04），直接 pip 安装是会失败的，暂时找不到解法。可能遇到以下错误：

1. `error: invalid command 'bdist_wheel'`
  这可以通过安装 wheel 包解决。

2. 找不到 `maskApi.c`
  ```
  x86_64-linux-gnu-gcc: error: ../common/maskApi.c: No such file or directory                                                                                             
  x86_64-linux-gnu-gcc: fatal error: no input files
  ```
  这个错误暂时无法解决，请使用 pip git 安装。

pip 中的包使用的源码来自： https://github.com/ppwwyyxx/cocoapi

###  pip git 安装
pip git 安装命令：
```
pip install git+https://github.com/cocodataset/cocoapi.git#subdirectory=PythonAPI
```
或者使用镜像仓库：
```
pip install git+https://gitee.com/mlyin/cocoapi#subdirectory=PythonAPI
```
因为 github.com 可能无法稳定访问，可以尝试 gitee.com 上的 cocoapi 镜像。如果上面的镜像仓库消失了，可在 gitee 上搜索。

## 改进和扩展

### Extended COCO API (xtcocotools)
https://github.com/jin-s13/xtcocoapi

We aim to provide a unified evaluation tools to support multiple human pose-related datasets, including COCO, COCO-WholeBody, CrowdPose, AI Challenger and so on.

xtcocotools has been used in MMPose framework.
