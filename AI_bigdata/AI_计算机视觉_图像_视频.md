## 参考资料

[1.0] 图像数据集

[1.1.0] COCO is a large-scale object detection, segmentation, and captioning dataset
  http://cocodataset.org/

[1.1.１] COCO 目标检测数据集 a large image dataset designed for object detection, segmentation, person keypoints detection
  https://github.com/cocodataset/cocoapi

[1.1.2] MS COCO数据集目标检测评估（Detection Evaluation）
  https://blog.csdn.net/u014734886/article/details/78831884
  
[1.1.3] Microsoft COCO Caption Evaluation
  https://github.com/tylin/coco-caption

[1.1.4] COCO 2015 Image Captioning Task
  http://cocodataset.org/#captions-2015
  
[1.2.0] DensePose-COCO Dataset
  http://densepose.org/
  
[1.3.1] DensePose: Dense Human Pose Estimation In The Wild
  https://arxiv.org/abs/1802.00434
  
  为何coco dataset 的准确率那么低？
  https://www.zhihu.com/question/61117971
  
[1.2.0] PASCAL VOC
  http://host.robots.ox.ac.uk/pascal/VOC/
  
[1.3.0] ImageNet Det

[] Learning from Synthetic Humans
 https://arxiv.org/abs/1701.01370

[2.0] 微软计算机视觉
https://docs.azure.cn/zh-cn/cognitive-services/computer-vision/home

[2.2] 标记图像
https://docs.azure.cn/zh-cn/cognitive-services/computer-vision/concept-tagging-images

[2.3] 对图像进行分类
https://docs.azure.cn/zh-cn/cognitive-services/computer-vision/concept-categorizing-images

[2.4] 描述图像
https://docs.azure.cn/zh-cn/cognitive-services/computer-vision/concept-describing-images

[2.5] 包括 86 种类别的分类
https://docs.azure.cn/zh-cn/cognitive-services/computer-vision/category-taxonomy

[3.0] 百度计算机视觉
[3.1] 百度人体分析
http://ai.baidu.com/tech/body

[3.2] 百度 人体分析 API文档
http://ai.baidu.com/docs#/Body-API/top

[3.3] 百度 人体分析 demo
http://ai.baidu.com/tech/body/pose

[3.8] 百度 PaddlePaddle AI 大赛——综艺节目精彩片段预测
http://www.cnblogs.com/charlotte77/p/8192571.html

[4.0] face++ 计算机视觉
[4.2] face++ 人体关键点 demo
https://www.faceplusplus.com.cn/skeleton-detection/

[5.0] 人体关键点检测
[5.1] 卡内基梅隆大学的开源项目 OpenPose
  https://github.com/CMU-Perceptual-Computing-Lab/openpose

[5.2] 如何评价卡内基梅隆大学的开源项目 OpenPose？
  https://www.zhihu.com/question/59750782
  
[5.3] Realtime Multi-Person 2D Pose Estimation using Part Affinity Fields
  https://arxiv.org/abs/1611.08050
  https://github.com/ZheC/Realtime_Multi-Person_Pose_Estimation

[5.4] neuhub.jd 人体关键点检测
http://neuhub.jd.com/ai/api/body/pose

[5.5] 万维识别 14点人体关键点
http://www.wvsense.com/core.html

[5.6] SigAI 人体骨骼关键点检测综述
https://zhuanlan.zhihu.com/p/37933909

[5.7] 基于OpenCV的简易四肢位置识别与动作识别
  https://blog.csdn.net/shenmintao/article/details/78011647
  https://github.com/shenmintao/BodyDetect

[5.8] 人体姿态识别研究综述
  https://blog.csdn.net/qq_38522972/article/details/82953477
  https://pan.baidu.com/s/1wVQJfPgyXVQAmdSIKQNPAA 提取码：077o
  
[5.9] 谈谈NITE 2与OpenCV结合的第二个程序（提取人体骨骼坐标）
  http://www.cnblogs.com/yemeishu/archive/2013/01/16/2862514.html

[5.10] 人体姿态估计数据集整理（Pose Estimation/Keypoint）
  https://blog.csdn.net/qq_36165459/article/details/78332172
  
[5.11] Alpha Pose is an accurate multi-person pose estimator, which is the first real-time open-source system
  https://github.com/MVIG-SJTU/AlphaPose

[5.12] 人体姿态识别年度总结
 欧阳万里
  https://pan.baidu.com/s/1wVQJfPgyXVQAmdSIKQNPAA 提取码：077o

[5.13] Openpose implemented using Tensorflow for real-time processing on the CPU or low-power embedded devices
  https://github.com/ildoonet/tf-pose-estimation
  
[5.14] openpifpaf multi-person 2D human pose estimation
  https://github.com/vita-epfl/openpifpaf
  
[5.15] Pose Detection comparison : wrnchAI vs OpenPose
  https://www.learnopencv.com/pose-detection-comparison-wrnchai-vs-openpose/
  
[5.16.1] MobileNetV2-PoseEstimation
  https://github.com/PINTO0309/MobileNetV2-PoseEstimation
  
[5.16.2] CPU only 40 FPS++ Tensorflow based Fast Pose estimation. OpenVINO, Tensorflow Lite, NCS, NCS2 + Python
  https://devmesh.intel.com/projects/tensorflow-based-fast-pose-estimation-openvino-tensorflow-lite-ncs-ncs2-python

[5.17.1] Real-time 2D Multi-Person Pose Estimation on CPU:Lightweight OpenPose - Intel 2018
  https://arxiv.org/pdf/1811.12004.pdf

[5.17.2] intel human-pose-estimation-0001
  https://github.com/opencv/open_model_zoo/blob/master/models/intel/human-pose-estimation-0001/description/human-pose-estimation-0001.md
  
[5.17.3] Real-time 2D Multi-Person Pose Estimation on CPU: Lightweight OpenPose - training - Intel 2018
  https://github.com/opencv/openvino_training_extensions/tree/develop/pytorch_toolkit/human_pose_estimation
  
[5.17.4] Low accuracy for pose estimation?
  https://github.com/opencv/open_model_zoo/issues/27

[5.18] MultiPoseNet: Fast Multi-Person Pose Estimation using Pose Residual Network
  https://arxiv.org/abs/1807.04067

[5.18.2] Pose Residual Network pytorch
  https://github.com/salihkaragoz/pose-residual-network-pytorch

[5.18.3] Pose Residual Network
  https://github.com/danielperezr88/multiposenet-aries
  
[5.19.1]  Global Context for Convolutional Pose Machines
  https://arxiv.org/abs/1906.04104

[5.19.2] single-human-pose-estimation-0001
  https://github.com/opencv/open_model_zoo/blob/master/models/public/single-human-pose-estimation-0001/description/single-human-pose-estimation-0001.md

[5.19.3] Global Context for Convolutional Pose Machines - training - Intel 2018
  https://github.com/opencv/openvino_training_extensions/blob/develop/pytorch_toolkit/human_pose_estimation/README_single.md
  
[5.21] A simple yet effective baseline for 3d human pose estimation. In ICCV, 2017
  https://github.com/una-dinosauria/3d-pose-baseline
  https://arxiv.org/pdf/1705.03098.pdf

[5.22] Single-Shot Multi-Person 3D Pose Estimation From Monocular RGB
  https://arxiv.org/abs/1712.03453

[5.22.2] human-pose-estimation-3d-0001 open_model_zoo
  https://github.com/opencv/open_model_zoo/blob/master/models/public/human-pose-estimation-3d-0001/description/human-pose-estimation-3d-0001.md
  
[5.22.3] Real-time 3D Multi-person Pose Estimation Demo
  https://github.com/Daniil-Osokin/lightweight-human-pose-estimation-3d-demo.pytorch
  
[5.23.1] VIBE: Video Inference for Human Body Pose and Shape Estimation
  https://www.paperswithcode.com/paper/vibe-video-inference-for-human-body-pose-and

[5.23.2] VIBE: Video Inference for Human Body Pose and Shape Estimation [CVPR-2020]
  https://github.com/mkocabas/VIBE

[5.23.2] VIBE-RT: Real-time Video Inference for Human Body Pose and Shape Estimation 
  https://github.com/darkAlert/vibe-rt
  
[5.24.1] Unsupervised Adversarial Learning of 3D Human Pose from 2D Joint Locations 2018
  https://arxiv.org/abs/1803.08244

[5.24.2] Unsupervised Adversarial Learning of 3D Human Pose from 2D Joint Locations code
  https://github.com/DwangoMediaVillage/3dpose_gan
  
[5.25.1] 帧率提升至200fps 云从科技3D人体技术远超世界纪录
  https://zhuanlan.zhihu.com/p/59984067
  
[5.26.1] MoveNet fast 17 keypoints of a body. pose-detection
  https://github.com/tensorflow/tfjs-models/tree/master/pose-detection/src/movenet

[5.26.2] 使用 MoveNet 和 TensorFlow.js 的下一代姿态检测 
  https://mp.weixin.qq.com/s/Ri1BnS5nbGbxDDemMIFuJA

[6.0] 对象检测，人体检测
[6.1]【特征检测】HOG特征算法
  https://blog.csdn.net/hujingshuang/article/details/47337707
  
[6.2] opencv HOG 人体检测
  https://docs.opencv.org/3.1.0/d5/d33/structcv_1_1HOGDescriptor.html#a9c7a0b2aa72cf39b4b32b3eddea78203
  
[6.3] 用opencv自带hog实现行人检测
  http://www.cnblogs.com/tornadomeet/archive/2012/08/03/2621814.html
  
[6.4] 救援环境中的人体检测以及姿态识别方法
  http://www.soopat.com/Patent/201810710454

[6.5] YOLO: Real-Time Object Detection
  https://pjreddie.com/darknet/yolo/
  
[6.6] R. Girshick. Fast R-CNN. In Proc. ICCV, 2015 (*)
  https://www.cv-foundation.org/openaccess/content_iccv_2015/papers/Girshick_Fast_R-CNN_ICCV_2015_paper.pdf

[6.7] Faster R-CNN: Towards Real-Time ObjectDetection with Region Proposal Networks (slides)
  https://pdfs.semanticscholar.org/2e70/08c76a53c1d7aaa0b237c17a9ae29afe8008.pdf
  
[6.8] Faster R-CNN: Towards Real-Time Object Detection with Region Proposal Networks 2016
  https://arxiv.org/abs/1506.01497

[6.9] Mask R-CNN 2018
  https://arxiv.org/pdf/1703.06870
  
[6.10] 从R-CNN到Mask R-CNN
  https://zhuanlan.zhihu.com/p/30967656
  
[6.11] 深度卷积神经网络在目标检测中的进展
  https://zhuanlan.zhihu.com/p/22045213
  
[6.12] A Brief History of CNNs in Image Segmentation: From R-CNN to Mask R-CNN
  https://blog.athelas.com/a-brief-history-of-cnns-in-image-segmentation-from-r-cnn-to-mask-r-cnn-34ea83205de4?gi=62a21f601897
  
[6.13] 从CNN到SSD目标检测机器学习方法总结
  https://blog.csdn.net/xiaomu_347/article/details/82346424
  
[6.14] CNN目标检测（三）：SSD详解
  https://blog.csdn.net/a8039974/article/details/77592395
  
[6.15] DensePose-RCNN human pose estimation
  https://github.com/facebookresearch/Densepose

[7.0] 对象跟踪，目标跟踪
[7.1] SORT A simple online and realtime tracking algorithm for 2D multiple object tracking in video sequences.
  https://github.com/abewley/sort
  
[8.1] seetatech 视频结构化分析
  http://www.seetatech.com/hxjs.html

[11.0] 单帧姿势/简单动作识别

[11.1] 基于Kinect的人体姿势识别算法与实现
  https://max.book118.com/html/2017/0508/105196952.shtm

[11.2] Kinect 开发 —— 姿势识别
  http://www.cnblogs.com/sprint1989/p/3849746.html
  
[11.3] 基于Kinect的人体姿势识别和校正
  https://max.book118.com/html/2016/0124/33939813.shtm

[11.4] 基于Kinect的实时人体姿势识别
  https://wenku.baidu.com/view/bf071b121ed9ad51f01df2a3.html
  
[11.5] OpenNI 人体姿势识别
  https://blog.csdn.net/mingspy/article/details/7290339
  
[11.6] 一种人体姿态识别方法及相关装置 骨架特征提取 神经网络训练
  http://www.soopat.com/Patent/201811268507
  
[11.7] 救援环境中的人体检测以及姿态识别方法 审中-实审
  http://www.soopat.com/Patent/201810710454

[11.8] 一种基于人体关键点的姿态识别方法、系统及存储器
  http://www.soopat.com/Patent/201810772931
  
[11.9] 一种基于深度学习的人体姿态识别方法
  http://www.soopat.com/Patent/201811177283

[11.10] C/C++/Python based computer vision models using OpenPose, OpenCV, DLIB, Keras and Tensorflow libraries. Object Detection, Tracking, Face Recognition, Gesture, Emotion and Posture Recognition
  https://github.com/srianant/computer_vision
  
[11.11] Real-time pose estimation and action recognition
  https://github.com/TianzhongSong/Real-Time-Action-Recognition
  
[12.0] 行为识别

[12.1] 行为识别：让机器学会“察言观色”第一步
  https://blog.csdn.net/heyc861221/article/details/80128180

[12.2] 行为识别数据集汇总
  https://blog.csdn.net/u012507022/article/details/52876179
  
[12.3] 基于Kinect传感器的人体行为分析算法
  https://max.book118.com/html/2017/0507/104973401.shtm

[12.4] Action Recognition——基于深度学习的动作识别综述
  https://blog.csdn.net/MIKASA3/article/details/84981035
  
[12.5] Going Deeper into Action Recognition: A Survey
  https://arxiv.org/pdf/1605.04988.pdf
  
[12.6] P-CNN: Pose-based CNN Features for Action Recognition, 2015
  https://www.di.ens.fr/willow/research/p-cnn/
  https://github.com/gcheron/P-CNN

[12.7] Spatial Temporal Graph Convolutional Networks (ST-GCN) for Skeleton-Based Action Recognition in PyTorch
  https://arxiv.org/abs/1801.07455
  https://github.com/yysijie/st-gcn
  
[12.8] A curated list of action recognition and related area (e.g. object recognition, pose estimation) resources
  https://github.com/jinwchoi/awesome-action-recognition

[12.9] Online Human Action Detection using Joint Classification-Regression Recurrent Neural Networks
  http://www.icst.pku.edu.cn/struct/Projects/OAD.html

[12.10] Multi-Modality Multi-Task Recurrent Neural Network for Online Action Detection
  http://www.icst.pku.edu.cn/struct/Projects/mmmt.html

[12.11] An End-to-End Spatio-Temporal Attention Model for Human Action Recognition from Skeleton Data
  http://www.icst.pku.edu.cn/struct/Projects/AAAI_SSJ/aaai_2017_ssj.html

[12.12] STRUCT Research Group: Spatial and Temporal Restoration, Understanding and Compression Team
  http://39.96.165.147/Projects.html

[12.13] 刘家瑛 副教授 研究领域：图像/视频编码优化、增强重建、分析与理解
  http://www.icst.pku.edu.cn/xztd/xztd_01/1222618.htm

[12.14] Temporal Bilinear Networks for Video Action Recognition
  https://lyttonhao.github.io/projects/TBN/TBN.html

[12.15] MMAction is an open source toolbox for action understanding based on PyTorch
  https://github.com/open-mmlab/mmaction

[12.16] Temporal Segment Networks (TSN)
  https://github.com/yjxiong/temporal-segment-networks

[12.17] Two-Stream Convolutional Networks for Action Recognition in Videos
  https://blog.csdn.net/paranoid_cnn/article/details/53257031

[12.18] two-stream-action-recognition
  https://github.com/jeffreyyihuang/two-stream-action-recognition

[12.19] PyTorch implementation of popular two-stream frameworks for video action recognition
  https://github.com/bryanyzhu/two-stream-pytorch

[12.20] Convolutional Two-Stream Network Fusion for Video Action Recognition
  https://github.com/feichtenhofer/twostreamfusion

[12.21] Hidden Two-Stream Convolutional Networks for Action Recognition
  https://github.com/bryanyzhu/Hidden-Two-Stream

[13.0] 视频前景背景分离
[13.1] 图像处理——目标检测与前背景分离
  https://blog.csdn.net/u010402786/article/details/50596263
[13.2] OpenCV学习(20) grabcut分割算法
  https://www.cnblogs.com/mikewolf2002/p/3330390.html

## 总论
此处“计算机视觉”是狭义的，不包括人脸识别，可以包括人体识别、图像分类、OCR等。

## 图像数据集

### PASCAL VOC 
[1.2.0]

20 classes:

    Person: person
    Animal: bird, cat, cow, dog, horse, sheep
    Vehicle: aeroplane, bicycle, boat, bus, car, motorbike, train
    Indoor: bottle, chair, dining table, potted plant, sofa, tv/monitor

Train/validation/test: 9,963 images containing 24,640 annotated objects. 

People in action classification dataset are additionally annotated with a reference point on the body.

动作分类：跳、跑、打电话、骑车等10类。


 For each person in an image the 'layout' of the person is annotated by the bounding boxes of three body parts:

    head
    hands
    feet

Each person annotated has at least their head and one other body part visible. 

## 视频结构化分析

## 微软计算机视觉
这是狭义的计算机视觉，人脸相关的功能是另外的API。

主要功能是[2.0-2.5]：

标记视觉特性 	根据 2000 多个可识别对象、生物、风景和操作确定和标记图像中的视觉特性。 如果标记含混不清或者不常见，响应会提供“提示”，明确已知设置上下文中的标记的含义。 标记并不局限于主体（如前景中的人员），还包括设置（室内或室外）、家具、工具、工厂、动物、附件、小工具等。

对图像分类 	使用具有父/子遗传层次结构的类别分类对整个图像进行标识和分类。 类别可单独使用或与我们的新标记模型结合使用。目前，英语是唯一可以对图像进行标记和分类的语言。

描述图像 	使用完整的句子，以人类可读语言生成整个图像的说明。 计算机视觉算法可根据图像中标识的对象生成各种说明。 会对说明一一进行评估，并生成置信度。 然后返回一个列表，将置信度从高到低进行排列。

检测人脸 	检测图像中的人脸，提供每个检测到的人脸的相关信息。 计算机视觉返回每个检测到的人脸的坐标、矩形、性别和年龄。

计算机视觉提供可以在人脸中发现的部分功能。可以使用人脸服务进行更详细的分析，例如人脸识别和姿势检测。

检测图像类型 	检测图像特征，例如图像是否为素描，或者图像是剪贴画的可能性。

检测特定领域的内容 	使用域模型来检测和标识图像中特定领域的内容，例如名人和地标。 例如，如果图像中包含人物，则计算机视觉可以使用服务随附的针对名人的域模型来确定图像中检测到的人物是否与已知名人匹配。

检测颜色方案 	分析图像中的颜色使用情况。 计算机视觉可以确定图像是黑白的还是彩色的，而对于彩色图像，又可以确定主色和主题色。

生成缩略图 	分析图像的内容，生成该图像的相应缩略图。 计算机视觉首先生成高质量缩略图，然后通过分析图像中的对象来确定感兴趣区域 (ROI)。 然后，计算机视觉会裁剪图像以满足感兴趣区域的要求。 可以用户需求使用与原始图像的纵横比不同的纵横比显示生成的缩略图。

计算机视觉通过 OCR 来提取文本

## 百度计算机视觉

### 百度人体分析
识别图像中的人体相关信息，提供人体检测与追踪、关键点定位、人流量统计、属性分析、人像分割、手势识别等能力 [3.1]。

接口名称	接口能力简要描述[3.2]

人体关键点识别	检测图像中的所有人体并返回每个人体的矩形框位置，精准定位14个核心关键点，包含四肢、脖颈、鼻子等部位，更多关键点持续扩展中；支持多人检测、人体位置重叠、遮挡、背面、侧面、中低空俯拍、大动作等复杂场景

人体属性识别	检测图像中的所有人体并返回每个人体的矩形框位置，识别人体的静态属性和行为，共支持20余种属性，包括：性别、年龄阶段、衣着（含类别/颜色）、是否戴帽子、是否戴眼镜、是否背包、是否使用手机、身体朝向等；支持中低空俯拍视角、人体重叠、遮挡、背面、侧面、动作变化等复杂场景

人流量统计	识别和统计图像当中的人体个数（静态统计，不支持追踪和去重），适用于3米以上的中远距离俯拍，以人头为主要识别目标统计人数，无需正脸、全身照，适应各类人流密集场景；默认识别整图中的人数，支持指定不规则区域的人数统计，同时可输出渲染图片

手势识别	识别图片中的手势类型，返回手势名称、手势矩形框、概率分数，可识别23种手势，支持动态手势识别，适用于手势特效、智能家居手势交互等场景；支持的手势列表：手指、掌心向前、拳头、OK、祈祷、作揖、作别、单手比心、点赞、diss、我爱你、rock、掌心向上、双手比心（3种）、数字（7种）

人像分割	识别人体的轮廓范围，与背景进行分离，适用于拍照背景替换、照片合成、身体特效等场景；输入正常人像图片，返回分割后的二值结果图，返回的二值图像需要进行二次处理才可查看分割效果

驾驶行为分析（邀测）	针对车载场景，检测图片中是否有驾驶员，并识别驾驶员是否有使用手机、抽烟、未系安全带、双手离开方向盘 等行为，可用于分析预警危险驾驶行为

人流量统计-动态版（邀测）	动态人数统计和跟踪，主要适用于低空俯拍、出入口场景，以人体头肩为主要识别目标，核心功能：传入监控视频抓拍图片序列，进行人体追踪，根据目标轨迹判断进出区域行为，进行动态人数统计，返回区域进出人数。

### 百度图像识别

通用图像分析	图像主体检测	识别图像中的主体具体坐标位置。
	通用物体和场景识别高级版	识别图片中的场景及物体标签，支持8w+标签类型。
	
细粒度图像识别	菜品识别	检测用户上传的菜品图片，返回具体的菜名、卡路里、置信度信息。

	自定义菜品识别	入库自定义的单菜品图，实现上传多菜品图的精准识别，返回具体的菜名、位置、置信度信息
	车型识别	检测用户上传的车辆图片，识别所属车型，包括车辆品牌及具体型号、颜色、年份、位置信息。
	logo商标识别	识别图片中包含的商品LOGO信息，返回LOGO品牌名称、在图片中的位置、置信度。
	动物识别	检测用户上传的动物图片，返回动物名称、置信度信息。
	植物识别	检测用户上传的植物图片，返回植物名称、置信度信息。
	花卉识别	检测用户上传的花卉图片，返回花卉名称、置信度信息。
	果蔬食材识别	检测用户上传的果蔬类图片，返回果蔬名称、置信度信息。
	地标识别	检测用户上传的地标图片，返回地标名称。
	

## seetatech
### 视频结构化分析
[8.1]
    目标检测跟踪再识别 基于视频结构化分析技术，对目标进行检测、跟踪、以及精准的再识别。
    目标属性分析 基于视频结构化分析技术，对目标进行属性细节的分析。
    目标轨迹分析 基于视频结构化分析技术，对目标的运动轨迹进行分析。

## 人体检测，对象检测
人体检测 [6.x] 是从图像中给出人体存在的区域。

目前，基于视觉的人体检测算法主要分为两类：基于手工特征的人体检测和基于深度学习的人体检测。其中，最常用的两种手动特征方法是HOG算法和DPM算法。基于手工特征方法的缺点是鲁棒性差，检测率低。同时，由于其检测时间长，难以满足实际应用。深度学习技术可以自动学习图像的内在特征，弥补手工特征泛化能力差的问题，如今也开始被应用于人体检测问题。比较出色的算法包括ConvNet、JointDeep等。但是网络模型不够深，检测精度低。同时检测时间长，不能满足实时性要求。

HOG（Histogram of Oriented Gridients的简写）特征检测算法，最早是由法国研究员Dalal等在CVPR-2005上提出来的，一种解决人体目标检测的图像描述子，是一种用于表征图像局部梯度方向和梯度强度分布特性的描述符。其主要思想是：在边缘具体位置未知的情况下，边缘方向的分布也可以很好的表示行人目标的外形轮廓。
Dalal等提出的HOG+SVM算法，在进行行人检测取得了极大地成功后，更多新算法不断涌现，不过大都是以HOG+SVM的思路为主线。
 [6.1]

HOG行人特征及所对应的SVM分类器的参数，在opencv中已经训练好了，我们只需要得到HOG特征，然后调用SVM即可得到判别结果。[6.1]。

R-CNN 家族方法[6.5-6.7] 是，比较如下：

R-CNN: Region-based Convolutional Neural Networks (2013)
  ●Selective Search for region proposals
  ●CNN extracts feature map to classify regions using SVM

Fast R-CNN (2015)
  ●Share computation of convolutional layers for proposals (ROI pooling)

Faster R-CNN (2015)
  ●Introduces Region Proposal Network(RPN) with shared network weights to reduce proposal time
  
从 R-CNN 到 Faster R-CNN（微软开发的），性能持平或略增，速度则大大提高。faster R-CNN 达到 0.2s/帧。

Mask R-CNN：Faster R-CNN 在物体检测中已达到非常好的性能，Mask R-CNN在此基础上更进一步：得到像素级别的检测结果。 对每一个目标物体，不仅给出其边界框，并且对边界框内的各个像素是否属于该物体进行标记。

### YOLO
YOLO 是个基于神经网络的快速对象检测（含人体检测）程序[6.5]。alpa pose 使用它进行人体检测。
在 GPU 上可以达到对视频进行实时检测，在CPU上也可以运行。

### opencv HOG PeopleDetector 
[6.2, 6.3]
从实验的结果来看，图片检测的准确率一般，当人体遮挡情况比较严重，且背景比较复杂时，有些误检和漏检。不过程序的检查速度还行，因为源码中用做了些优化处理[6.3]。

## 目标跟踪
SORT [7.1] 实现了对视频序列中的多个目标的跟踪。

##  人体关键点检测 (pose estimation)
目前的人体姿态估计方法主要可以分为基于部件的框架与基于整体的框架两种。基于部件的框架首先检测人体部位，然后组装人体部位形成多个人体姿势，具有代表性的是openpose，缺点就是当人员密集时，会将不属于同一个人的部位进行拼接，并且丧失了从全局进行部位识别的能力。基于整体的框架就是在行人检测的基础上，对每一个人整体进行关键点检测，缺点是受检测框影响大，姿态估计的准确性主要取决于人体边界框的质量。[6.4]

一些术语：
* human pose, Pose Estimation 人体关节点识别、人体关键点检测。也译作“姿态估计”[6.4]，但不是很准确。
* person keypoints detection 人体关键点检测
* Skeleton 骨架

Human 2D pose estimation—the problem of localizinganatomical  keypoints  or  “parts”—has  largely  focused  onfinding body parts ofindividuals [5.3]。

可用于关键点检测的数据集有 COCO [1.1, 1.2]、MPII [5.10] 等。

### OpenPose
openpose 采用的关键点检测算法：[5.1-5.3]。
人体关键点检测，综合了身体、脸、四肢、手的 135 个关键点。
OpenPose represents the first real-time multi-person system to jointly detect human body, hand, facial, and foot keypoints (in total 135 keypoints) on single images.

### Alpha Pose
上交大开发。an accurate multi-person pose estimator, which is the first real-time open-source system. [5.11]
性能是20fps，单显卡（nv 1080ti），每帧4.6人。

年授权费 $1000-$2,000，非商业使用免费。

### OpenNI (NITE 2) 
OpenNI2 (NITE 2) 的关键点检测[5.9]。kinect 的关键点检测是用的是 OpenNI2。
但这部分并不开源，识别的姿势也不多，如投降姿势、双手抱胸

### face++
[4.2]
人体关键点 定位并返回人体各部位关键点坐标位置。关键点定位了头、颈、肩、肘、手、臀、膝、脚等部位。
    
## 单帧姿势识别
一般称作姿势、姿态（posture），相关的还有手势（gesture） 。

论文 [11.1] 从人体关键点计算肢体角度，然后用支持向量机分类器进行训练和识别姿势。人体关键点从 kinect 获得。

文章 [11.2] 从人体关键点计算肢体角度，然后直接与设定的标准和阈值比较，得出姿势。

论文 [11.3] 介绍了隐马尔可夫模型（HMM）和均值 Hausdorff 距离（MHD）两种种动作识别方法，以及根据关节角度直接判别识别动作的方法（与11.2同）。

论文 [11.3] 用关节角度数据（样本量5000+）训练逻辑回归（LR）分类器，可识别54种姿势。

专利 [11.6] 以人体关键点为特征，进行神经网络训练。训练样本最少300张，分类为站、坐、躺、走、跳等。

专利 [11.7] 检测姿势的方法是：发现当人体框的宽高比小于0.5时，可以初步断定检测到的人体处于躺卧姿势，同时，当人体躯干角度小于±30°时，可以进一步确定人体处于躺卧姿势。

专利 [11.8] 以人体关键点为特征，过支持向量机SVM进行训练。训练样本不小于10000。

专利 [11.9] 使用 kinect 获取关键点，再用 CNN 进行动作识别。

代码 [11.10] 使用 openpose 获取关键点，用神经网络训练识别姿势。

代码 [11.11] 使用 openpose 获取关键点，用对象跟踪（SORT程序）和手写规则识别坐、站、走等动作。

## 行为、动作识别
指对持续一段时间的动作的识别，区别于静态的、可以单帧识别的姿势。

[12.0-12.x] 


一些术语：
* (biological) motion （身体或肢体）的运动，指最基础的运动。
* Action Recognition 动作识别。Action 一般定义为比较基本的肢体运动。
* Activity Recognition - 活动/行为/行动识别。Activity 一般被定义为一系列的 Action，或者多人参与的活动/行为。
* Skeleton

文档 [12.8] 收集了动作识别以及人体关键点领域的论文和代码。
论文 [12.4, 12.5] 综述了利用深度学习的动作识别算法。

论文 [12.3] 利用 kinect 深度图像和条件随机场(CRF)算法进行短视频（10帧）动作识别，可识别站、坐、走等行为。

论文 [12.6] 使用关键点估计和卷积神经网络(Pose-based CNN) 进行动作识别。有matlab代码。

论文 [12.7] 使用关键点估计和 Graph Convolutional Networks 进行动作识别。有 pytorch 代码。

人体骨架怎么获得呢？主要有两个途径：通过RGB图像进行关节点估计（Pose Estimation）获得，或是通过深度摄像机直接获得（例如Kinect）。每一时刻（帧）骨架对应人体的K个关节点所在的坐标位置信息，一个时间序列由若干帧组成。
行为识别就是对时域预先分割好的序列判定其所属行为动作的类型，即“读懂行为”。但在现实应用中更容易遇到的情况是序列尚未在时域分割（Untrimmed），因此需要同时对行为动作进行时域定位（分割）和类型判定，这类任务一般称为行为检测 [12.1]。

北大论文[12.9] 使用人体关键点 Joint Classification-Regression Recurrent Neural Networks 。提供了数据集  Online Action Datase ，但没有代码。

北大论文[12.10] 使用人体关键点和光流 Multi-Modality Multi-Task Recurrent Neural Network for Online Action Detection。没有代码。

北大论文[12.11] 使用人体关键点 Spatio-Temporal Attention Model。没有代码。

北大 STRUCT 是研究图像/视频理解的一个组[12.12]，包括动作（action）识别。

动作识别的一些方法包括two-stream[12.17], temporal segment networks[12.16, 12.15], 后者更为先进一些。[12.15] 提供了可运行的TSN代码。[12.18-12.20] 提供了 two-stream 及其变种的代码。

## 图像分割，视频前景背景分离

