## 参考资料

[1.1] PaddleClas
https://gitee.com/paddlepaddle/PaddleClas
https://github.com/PaddlePaddle/PaddleClas


[1.2] 环境准备
https://gitee.com/paddlepaddle/PaddleClas/blob/release/2.5/docs/zh_CN/installation.md
https://github.com/PaddlePaddle/PaddleClas/blob/release/2.5/docs/zh_CN/installation.md

[2.1] PULC 佩戴安全帽分类模型
https://gitee.com/paddlepaddle/PaddleClas/blob/release/2.5/docs/zh_CN/models/PULC/PULC_safety_helmet.md

## 简介

PaddleClas是飞桨为工业界和学术界所准备的一个图像识别和图像分类任务的工具集

## 安装

### 获得源码
参考[1.1]，二选一：
```
git clone https://gitee.com/paddlepaddle/PaddleClas.git -b release/2.5 PaddleClas.git
git clone https://github.com/PaddlePaddle/PaddleClas.git -b release/2.5 PaddleClas.git
```

### 安装本体

```
source ~/opt/venv/v39-1/activate

cd ~/project/PaddleClas.git
pip install -r requirements.txt

# 以下都是安装 paddleclas，二选一即可。后者从当前源码中安装，版本号 0.0.0。
# pip install paddleclas==2.5.1
python setup.py install
```

## 训练

### 训练案例：是否戴头盔检测
参考[2.1]

#### 准备数据集
在 gtrd 工作站上：
```
cd /media/data2/ai-dataset
wget https://paddleclas.bj.bcebos.com/data/PULC/safety_helmet.tar
tar -xf safety_helmet.tar

cd ~/project/PaddleClas.git/dataset
ln -s ../../../../../media/data2/ai-dataset/safety_helmet .
```

数据集的目录结构：
```
├── images
│   ├── VOC2028_part2_001209_1.jpg
│   ├── HHD_hard_hat_workers23_1.jpg
│   ├── CelebA_077809.jpg
│   ├── ...
│   └── ...
├── train_list.txt
└── val_list.txt
```
#### 准备配置文件

将 `ppcls/configs/PULC/safety_helmet/PPLCNet_x1_0.yaml` 复制到 `configs/pplcnet/PPLCNet_x1_0_safety_helmet.yaml`
修改 `batch_size: 128 # 64 for 4 GPU, 128 for 2 GPU`

#### 训练

训练，2GPU：
```
python3 -m paddle.distributed.launch --gpus=0,1 tools/train.py -c configs/pplcnet/pplcnet_x1_0_safety_helmet.yaml
```

训练图像数量：`540*128*2=138240`
预训练模型（没有写在配置文件中）： `https://paddle-imagenet-models-name.bj.bcebos.com/dygraph/legendary_models/PPLCNet_x1_0_ssld_pretrained.pdparams`
模型保存位置（模型名没有写在配置文件中）： `./output/PPLCNet_x1_0/`

GPU 占用：95%
显存占用：4.5GB
训练吞吐量：1640.06721 samples/s（3090显卡）

评估：
```
python3 tools/eval.py -c configs/pplcnet/pplcnet_x1_0_safety_helmet.yaml -o Global.pretrained_model=output/PPLCNet_x1_0/best_model

[2023/01/13 18:49:21] ppcls INFO: [Eval][Epoch 0][Avg]CELoss: 0.07440, loss: 0.07440, threshold: 0.5732, fpr: 0.0, tpr: 0.98929, top1: 0.99589
```

## 部署

### 导出模型

gtrd:

```
mkdir -p /media/data2/train_data/PaddleCls/model
mkdir -p /media/data2/train_data/PaddleCls/inference_model

cd ~/project/PaddleCls.git
ln -sf ../../../../media/data2/train_data/PaddleCls/inference_model . 
ln -sf ../../../../media/data2/train_data/PaddleCls/model . 

wget -P ./model/ https://paddle-imagenet-models-name.bj.bcebos.com/dygraph/legendary_models/ResNet50_vd_pretrained.pdparams
wget -P ./model/ https://paddle-imagenet-models-name.bj.bcebos.com/dygraph/legendary_models/PPLCNet_x1_0_pretrained.pdparams
wget -P ./model/ https://paddle-imagenet-models-name.bj.bcebos.com/dygraph/legendary_models/PPLCNet_x1_0_ssld_pretrained.pdparams

python tools/export_model.py \
    -c ./ppcls/configs/ImageNet/ResNet/ResNet50_vd.yaml \
    -o Global.pretrained_model=./model/ResNet50_vd_pretrained \
    -o Global.save_inference_dir=./inference_model/ResNet50_vd_ImageNet

python3 tools/export_model.py -c ppcls/configs/ImageNet/PPLCNet/PPLCNet_x1_0.yaml -o Global.pretrained_model=./model/PPLCNet_x1_0_pretrained -o Global.save_inference_dir=./inference_model/PPLCNet_x1_0_ImageNet

python3 tools/export_model.py -c ppcls/configs/ImageNet/PPLCNet/PPLCNet_x1_0.yaml -o Global.pretrained_model=./model/PPLCNet_x1_0_ssld_pretrained -o Global.save_inference_dir=./inference_model/PPLCNet_x1_0_ssld_ImageNet
```

注意，输入模型文件参数 `Global.pretrained_model` 不支持 url 参数，所以要先下载模型到本地，再导出；输入模型文件的扩展名 `.pdparams` 也必须省略。

## issues

### site-packages/paddleclas/deploy/python/postprocess.py:ThreshOutput 输出的 scores 属性类型不一致

ThreshOutput 是个后处理函数，将输入的 numpy 数组转化为字典输出。输入的 numpy 数组为 numpy.float32 类型，但输出的 scores 却是 numpy.float32 和 numpy.float64 两种可能。
原因是，如果 a 是 numpy.float32，则 1 - a 是 numpy.float64 类型。
