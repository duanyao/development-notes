## 软标签交叉熵损失函数

### mxnet/gluon
实现在 SoftmaxCrossEntropyLoss 中

https://mxnet.apache.org/api/python/docs/api/gluon/loss/index.html#mxnet.gluon.loss.SoftmaxCrossEntropyLoss

即 sparse_label = False 的情形。

定义：
```
# i 是样本序号，j、k 是类别序号。
softmax[i, j] = exp(logit[i, j]) / sum(exp(logit[i, k]), k)
p[i,j] = softmax[i, j]
L=-sum(sum(label[j]*log(p[i,j]), j), i) # L 是 SoftmaxCrossEntropyLoss
```

```
L[i] = - sum(label[j]*log(p[i,j]), j)
= - sum(label[j]*log(softmax[i,j]), j)
= - sum(label[j]*log(exp(logit[i, j]) / sum(exp(logit[i, k]), k) ), j)
= - sum(label[j]* ( logit[i, j] - log( sum(exp(logit[i, k]), k) ) ), j)
```

logit 是预测值，未归一化，范围一般是`(-无穷, +无穷)`， p 是预测概率，范围是 `(0, 1)`，即 softmax 无法达到 0 或 1,只能接近。

### paddle
实现在 paddle.nn. CrossEntropyLoss 
https://www.paddlepaddle.org.cn/documentation/docs/en/api/paddle/nn/CrossEntropyLoss_en.html

即 soft_label=True 的情形。定义与 mxnet 相同。
