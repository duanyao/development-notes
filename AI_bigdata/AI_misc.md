没有足够多的数据怎么办？计算机视觉数据增强方法总结 https://zhuanlan.zhihu.com/p/31558973

自监督、半监督和有监督全涵盖，四篇论文遍历对比学习的研究进展 https://www.jiqizhixin.com/articles/2020-09-15-8

YAD2K：Keras版本的YOLO算法损失函数源码解读 https://zhuanlan.zhihu.com/p/46309428

使用Deepstream部署你的YOLOV4模型 https://zhuanlan.zhihu.com/p/299183571

如何提高深度学习数据传输效率？ https://www.zhihu.com/lives/1126595102189187072

通用型议论文创作人工智能框架 https://github.com/EssayKillerBrain/EssayKiller_V2/tree/2.0

变形卷积核、可分离卷积？卷积神经网络中十大拍案叫绝的操作。 https://zhuanlan.zhihu.com/p/28749411

不止显卡！这些硬件因素也a影响着你的深度学习模型性能 https://zhuanlan.zhihu.com/p/67785062

float16 vs float32 for convolutional neural networks https://stackoverflow.com/questions/46613748/float16-vs-float32-for-convolutional-neural-networks

BFloat16: The secret to high performance on Cloud TPUs https://cloud.google.com/blog/products/ai-machine-learning/bfloat16-the-secret-to-high-performance-on-cloud-tpus

Performance Benefits of Half Precision Floats  https://software.intel.com/content/www/us/en/develop/articles/performance-benefits-of-half-precision-floats.html

聊聊 Pandas 的前世今生 https://mp.weixin.qq.com/s/5tDJOoFKo4Ox9wQpKGei3g

无法访问GPT-3？强大的克隆版GPT-J带着60亿参数来了 https://mp.weixin.qq.com/s/XsFoD2SvURBQfEcSe6DZXQ

deep face recognition model with ResNet100 backbone and ArcFace loss. 
https://github.com/openvinotoolkit/open_model_zoo/tree/master/models/public/face-recognition-resnet100-arcface-onnx

higher-hrnet-w32-human-pose-estimation
https://github.com/openvinotoolkit/open_model_zoo/tree/master/models/public/higher-hrnet-w32-human-pose-estimation

HigherHRNet Pose-Estimation
https://github.com/HRNet/HigherHRNet-Human-Pose-Estimation

Bottom-Up Human Pose Estimation Via Disentangled Keypoint Regression
https://github.com/HRNet/DEKR

Single-shot multi-person 3d pose estimation from monocular rgb
https://scholar.google.com/scholar?cites=1988965050516084859&as_sdt=2005&sciodt=0,5&hl=zh-CN

Moulding Humans: Non-Parametric 3D Human Shape Estimation From Single Images
https://openaccess.thecvf.com/content_ICCV_2019/html/Gabeur_Moulding_Humans_Non-Parametric_3D_Human_Shape_Estimation_From_Single_Images_ICCV_2019_paper.html

LCR-Net++: Multi-Person 2D and 3D Pose Detection in Natural Images
https://ieeexplore.ieee.org/abstract/document/8611390

Monocular Total Capture: Posing Face, Body, and Hands in the Wild
https://openaccess.thecvf.com/content_CVPR_2019/html/Xiang_Monocular_Total_Capture_Posing_Face_Body_and_Hands_in_the_CVPR_2019_paper.html

Real-time 2D Multi-Person Pose Estimation on CPU: Lightweight OpenPose
https://arxiv.org/abs/1811.12004

Overview of Human Pose Estimation Neural Networks — HRNet + HigherHRNet, Architectures and FAQ — 2d3d.ai
https://towardsdatascience.com/overview-of-human-pose-estimation-neural-networks-hrnet-higherhrnet-architectures-and-faq-1954b2f8b249

Running Google MoveNet Multipose models on OpenVINO. of several persons (6 max)
https://github.com/geaxgx/openvino_movenet_multipose

MoveNet Single Pose tracking on OpenVINO
https://github.com/geaxgx/openvino_movenet

Deep High-Resolution Representation Learning for Human Pose Estimation 
https://paperswithcode.com/paper/deep-high-resolution-representation-learning

Using OpenCV to detect face key points with C++
https://bewagner.net/programming/2020/04/23/detecting-face-keypoints-with-opencv/

Run Inference of a Face Detection Model Using OpenCV* API
https://software.intel.com/content/www/us/en/develop/articles/add-a-time-date-stamp-to-your-output-files-for-opencv-frames-in-python.html

Introduction to Intel OpenVINO Toolkit
https://learnopencv.com/introduction-to-intel-openvino-toolkit/

PaddleDetection模块化地实现了多种主流目标检测算法
https://github.com/PaddlePaddle/PaddleDetection

Gitee 极速下载 / faceswap FaceSwap is a tool that utilizes deep learning to recognize and swap faces in pictures and videos
https://gitee.com/mirrors/faceswap

ONNX Runtime
https://github.com/microsoft/onnxruntime

Omni-sourced Webly-supervised Learning for Video Recognition
https://arxiv.org/abs/2003.13042

Fully convolutional networks in Keras
https://github.com/bmezaris/fully_convolutional_networks

pytorch-estimate-flops
https://github.com/1adrianb/pytorch-estimate-flops

how to calculate a net's FLOPs in CNN
https://stackoverflow.com/questions/43490555/how-to-calculate-a-nets-flops-in-cnn

Feature request: add a functionality to return number of parameters and number of flops
https://github.com/BVLC/caffe/issues/2507

Count number of floating point operations per layer
https://github.com/keras-team/keras/pull/6203

web-based tool for visualizing neural network topologies, network complexity (number of operations) and network size (number of parameters) 
https://github.com/dgschwend/netscope
http://dgschwend.github.io/netscope/quickstart.html

Neural network runtime characteristics - FLOPS and memory requirements
https://github.com/mlperf-deepts/nns

Estimate/count FLOPS for a given neural network using pytorch
https://reposhub.com/python/deep-learning/1adrianb-pytorch-estimate-flops.html

pthflops pytorch-estimate-flops
https://pypi.org/project/pthflops/

Mixed precision training using float16
https://mxnet.apache.org/versions/master/api/faq/float16

Optimizing CPU Performance for Convolutional Neural Networks
http://cs231n.stanford.edu/reports/2015/pdfs/fabuzaid_final_report.pdf

How I improved a Human Action Classifier to 80% Validation Accuracy in 6 Easy Steps
https://towardsdatascience.com/6-steps-to-quickly-train-a-human-action-classifier-with-validation-accuracy-of-over-80-655fcb8781c5

What do we learn from region based object detectors (Faster R-CNN, R-FCN, FPN)?
https://jonathan-hui.medium.com/what-do-we-learn-from-region-based-object-detectors-faster-r-cnn-r-fcn-fpn-7e354377a7c9

Non-maximum Suppression (NMS) A technique to filter the predictions of object detectors.
https://towardsdatascience.com/non-maximum-suppression-nms-93ce178e177c

MediaPipe offers cross-platform, customizable ML solutions for live and streaming media.
https://github.com/google/mediapipe

使用OpenCV进行目标跟踪（C++/Python）
https://www.cnblogs.com/annie22wang/p/9366610.html

OpenCV上八种不同的目标追踪算法
http://www.360doc.com/content/20/0907/15/29968938_934408099.shtml

实操教程 | GPU多卡并行训练总结（以pytorch为例）
https://zhuanlan.zhihu.com/p/403339155

VisualDL, a visualization analysis tool of PaddlePaddle
https://github.com/PaddlePaddle/VisualDL

List of Nvidia graphics processing units
https://en.wikipedia.org/wiki/List_of_Nvidia_graphics_processing_units#Tesla9198

请问英伟达GPU的tensor core和cuda core是什么区别？
https://www.zhihu.com/question/451127498

理解Tensor Core
https://zhuanlan.zhihu.com/p/75753718

腾讯 机智加速 混合精度训练（上）
https://zhuanlan.zhihu.com/p/68692579

腾讯 机智加速 混合精度训练（下）— Tensor Core使用
https://zhuanlan.zhihu.com/p/71067422

Tips for Optimizing GPU Performance Using Tensor Cores
https://developer.nvidia.com/blog/optimizing-gpu-performance-tensor-cores/

mixed precision and automatic mixed precision, how to optimize with Tensor Cores
https://docs.nvidia.com/deeplearning/performance/mixed-precision-training/index.html

NVIDIA深度学习Tensor Core全面解析（上篇）
https://www.leiphone.com/category/ai/RsZr7QRlQMfhMwOU.html

NVIDIA深度学习Tensor Core全面解析（下篇） 
https://www.leiphone.com/category/ai/2J6ql9o0gseuIBYY.html

深度学习加速库Bolt领跑端侧AI
https://zhuanlan.zhihu.com/p/317111024
https://github.com/huawei-noah/bolt

MLCommons 基准测试 数据集 最佳实践
https://mlcommons.org/zh/

MLPerf™ Inference Benchmark Suite
https://github.com/mlcommons/inference

MLPerf™ Training Reference Implementations
https://github.com/mlcommons/training

MLPerf Training v1.0 Results
https://github.com/mlcommons/training_results_v1.0

MLPerf跑分 看不懂AI芯片推理性能跑分结果？专家教你！
https://www.ednchina.com/news/2019112709556.html

AI训练的最大障碍不是算力，而是“内存墙”
https://zhuanlan.zhihu.com/p/361624563

无需预训练分类器，清华&旷视提出专用于目标检测的骨干网络DetNet
https://zhuanlan.zhihu.com/p/35863669

李沐等将目标检测绝对精度提升 5%，不牺牲推理速度
https://zhuanlan.zhihu.com/p/56848556

SimpleCVReproduction
https://github.com/pprp/SimpleCVReproduction/

R-CenterNet(中文) 基于CenterNet的旋转目标检测
https://github.com/ZeroE04/R-CenterNet

Gluon CV Toolkit
https://github.com/dmlc/gluon-cv

Nvidia Geforece GPU用于深度学习的几点建议--防烧卡
https://zhuanlan.zhihu.com/p/164907210

Open Neural Network Exchange (ONNX) 
https://github.com/onnx/onnx

SimpleCVReproduction
https://github.com/pprp/SimpleCVReproduction/

Building Resnet-34 model using Pytorch – A Guide for Beginners
https://www.analyticsvidhya.com/blog/2021/09/building-resnet-34-model-using-pytorch-a-guide-for-beginners/

模型量化了解一下？
https://zhuanlan.zhihu.com/p/132561405

模型量化详解
https://blog.csdn.net/WZZ18191171661/article/details/103332338

OpenCV Vehicle Detection, Tracking, and Speed Estimation
https://pyimagesearch.com/2019/12/02/opencv-vehicle-detection-tracking-and-speed-estimation/

A Comprehensive Guide on How to Monitor Your Models in Production
https://neptune.ai/blog/how-to-monitor-your-models-in-production-guide

Image Classification with Confidence Scores (well-calibrated confidence score)
https://cloud.google.com/ai-workshop/experiments/image-confidence

The proper way to use Machine Learning metrics
https://towardsdatascience.com/the-proper-way-to-use-machine-learning-metrics-4803247a2578

A gentle guide to deep learning object detection
https://pyimagesearch.com/2018/05/14/a-gentle-guide-to-deep-learning-object-detection/

使用Kubernetes默认GPU调度
https://help.aliyun.com/document_detail/86490.html?spm=5176.2020520152.501.3.49fd16ddGSavs5

分析Faster RCNN中的RPN
https://blog.csdn.net/leviopku/article/details/80875368

focal loss 通俗讲解
https://zhuanlan.zhihu.com/p/266023273

线性分配问题（linear assignment problem）-python
https://www.codetd.com/article/5369441

蚂蚁集团开源可信隐私计算框架「隐语」：开放、通用
https://baijiahao.baidu.com/s?id=1737494621386821360&wfr=spider&for=pc

PaddleSlim推出全新自动化压缩工具（Auto Compression Toolkit, ACT）
https://github.com/PaddlePaddle/PaddleSlim/tree/develop/example/auto_compression

Visualizing and Understanding Convolutional Networks
https://arxiv.org/abs/1311.2901

深度学习可视化工具Visual DL——“所见即所得”
https://mp.weixin.qq.com/s?__biz=Mzg2OTEzODA5MA==&mid=2247486283&idx=1&sn=4f4d147a40599751b559fb73d3e306da&source=41#wechat_redirect

飞桨图像识别套件PaddleClas是一个图像识别和图像分类任务的工具集
https://github.com/PaddlePaddle/PaddleClas

图解Swin Transformer
https://zhuanlan.zhihu.com/p/367111046

PaddleClas-图像分类中的8种数据增广方法(cutmix, autoaugment,..)_littletomatodonkey的博客-程序员ITS301_图像分类数据增广
https://its301.com/article/u012526003/106346087

ConvNeXt详解
https://zhuanlan.zhihu.com/p/459163188

ConvNeXt网络详解
https://blog.csdn.net/qq_37541097/article/details/122556545

ConvNeXt：全面超越Swin Transformer的CNN
https://zhuanlan.zhihu.com/p/458016349

violetweir / convnext_PaddleClas
https://github.com/violetweir/convnext_PaddleClas

近亿级数据集下线，MIT道歉，ImageNet 亦或遭殃
https://mp.weixin.qq.com/s?__biz=MzA5ODEzMjIyMA==&mid=2247509265&idx=1&sn=02c329cb062829a6762491324d06447d&chksm=9094ac82a7e32594d25e5ef54e61864d520d3ed6baf4446dc9e9854a4682ebaae51beb185640&&xtrack=1&scene=90&subscene=93&sessionid=1593677236&clicktime=1593677240&enterid=1593677240&version=3.0.25.2257&platform=mac&rd2werd=1#wechat_redirect

收藏|浅谈多任务学习（Multi-task Learning）
https://zhuanlan.zhihu.com/p/348873723

Logit究竟是个啥？——离散选择模型之三
https://zhuanlan.zhihu.com/p/27188729

「神经网络就像任性的小孩」港中文MMLab博士揭开OpenSelfSup自监督学习的秘密
https://cloud.tencent.com/developer/article/1652281?from=article.detail.1458068

光流估计——从传统方法到深度学习
https://zhuanlan.zhihu.com/p/74460341

PyTorch数据集标准化-Torchvision.Transforms.Normalize()(pytorch系列-31)
https://blog.csdn.net/peacefairy/article/details/108020179

Pedestrian Attribute Recognition At Far Distance PETA dataset
http://mmlab.ie.cuhk.edu.hk/projects/PETA.html

mmclassification
https://mmclassification.readthedocs.io/en/latest/getting_started.html

mmdetection
https://github.com/open-mmlab/mmdetection

TensorRTx implement popular deep learning networks with tensorrt network definition APIs
https://github.com/wang-xinyu/tensorrtx

PaddlePaddle / awesome-DeepLearning
https://github.com/PaddlePaddle/awesome-DeepLearning

PaddlePaddle /PLSC 
https://github.com/PaddlePaddle/PLSC

机器学习中的距离计算方式
https://zhuanlan.zhihu.com/p/80660109

How transferable are features in deep neural networks?
https://arxiv.org/abs/1411.1792

Distilling the Knowledge in a Neural Network
https://arxiv.org/abs/1503.02531

TSM: Temporal Shift Module for Efficient Video Understanding
https://arxiv.org/pdf/1811.08383.pdf

PP-TSM视频分类模型
https://gitee.com/paddlepaddle/PaddleVideo/blob/develop/docs/zh-CN/model_zoo/recognition/pp-tsm.md

Ensemble of Averages: Improving Model Selection and Boosting Performance in Domain Generalization
https://arxiv.org/pdf/2110.10832.pdf
https://github.com/salesforce/ensemble-of-averages

SSLD 知识蒸馏实战
https://github.com/PaddlePaddle/PaddleClas/blob/22636b2ef65687af9c67bfa4d2450d539ebc3eaa/docs/zh_CN/training/advanced/ssld.md

CFNet: Head detection network based on multi-layer feature fusion and attention mechanism
https://ietresearch.onlinelibrary.wiley.com/doi/full/10.1049/ipr2.12770

论文解读：CFNet: Cascade and Fused Cost Volume for Robust Stereo Matching
https://zhuanlan.zhihu.com/p/412620703

CFNet: Cascade and Fused Cost Volume for Robust Stereo Matching
https://arxiv.org/abs/2104.04314

Flash is a high-level deep learning framework for fast prototyping, baselining, finetuning and solving deep learning problems. 
https://lightning-flash.readthedocs.io/en/latest/quickstart.html

PyTorchVideo Documentation
https://pytorchvideo.readthedocs.io/en/latest/index.html

Facial-Expression-Recognition.Pytorch
https://github.com/WuJie1010/Facial-Expression-Recognition.Pytorch

Project Name : Emotion-recognition
https://github.com/omar-aymen/Emotion-recognition

面部表情识别2：Pytorch实现表情识别(含表情识别数据集和训练代码)
https://mp.weixin.qq.com/s/Nhu2jD-KYtG9hqe29SyMzg

Real-time Convolutional Neural Networks for Emotion and Gender Classification
https://arxiv.org/pdf/1710.07557v1.pdf

飞桨产业级开源模型库
https://github.com/PaddlePaddle/models

PaddleHub
https://www.paddlepaddle.org.cn/hub

“模型即服务”（Model-as-a-Service）理念的模型应用生态
https://modelscope.cn/home

PaddleHub
https://github.com/PaddlePaddle/PaddleHub

AI知识地图：如何对AI技术进行分类
https://cloud.tencent.com/developer/news/305936

AI基础知识总结
https://www.jianshu.com/p/9d41f9c2e0f8

人工智能知识体系大全，看完这张图你也成为AI专家
https://zhuanlan.zhihu.com/p/27920278

什么是机器学习PAI
https://help.aliyun.com/document_detail/69223.html?spm=5176.14066474.J_5834642020.6.6369426aXRbdrj

机器学习PAI
https://pai.console.aliyun.com/?accounttraceid=10f948d963ea4d80b5516382954e01fclaql&regionId=cn-hangzhou&spm=5176.14066474.J_5834642020.3.6369426aXRbdrj#/workspace/overview

基于GPU的图像处理加速库CV-CUDA全面解读
https://zhuanlan.zhihu.com/p/584600231

NVIDIA Data Loading Library (DALI) is a GPU-accelerated library for data loading and pre-processing 
https://github.com/NVIDIA/DALI

美团视觉GPU推理服务部署架构优化实践
https://zhuanlan.zhihu.com/p/605094862

隐私计算概念、技术及应用介绍
https://zhuanlan.zhihu.com/p/383607686

从入门到放弃：深度学习中的模型蒸馏技术
https://zhuanlan.zhihu.com/p/93287223

模型压缩-剪枝算法详解
https://zhuanlan.zhihu.com/p/622519997

当前有那些深度学习模型剪枝工具？
https://www.zhihu.com/question/361052781

【AI不惑境】模型剪枝技术原理及其发展现状和展望
https://zhuanlan.zhihu.com/p/134642289

AI模型生产全流程介绍--模型部署那些事
https://zhuanlan.zhihu.com/p/548710767

浅谈Class Activation Mapping（CAM） 模型解释
https://zhuanlan.zhihu.com/p/51631163

Towards Better Explanations of Class Activation Mapping (CAM)
https://arxiv.org/abs/2102.05228

Understand your Algorithm with Grad-CAM
https://towardsdatascience.com/understand-your-algorithm-with-grad-cam-d3b62fce353

Improved image classification explainability with high-accuracy heatmaps
https://www.sciencedirect.com/science/article/pii/S2589004222002036

A Complete Guide to Image Classification in 2023
https://viso.ai/computer-vision/image-classification/

Beyond Appearance: a Semantic Controllable Self-Supervised Learning Framework for Human-Centric Visual Tasks
https://github.com/tinyvision/SOLIDER
https://arxiv.org/abs/2303.17602

Computer Vision in the Wild (CVinW)
https://github.com/Computer-Vision-in-the-Wild/CVinW_Readings

GLIP: Grounded Language-Image Pre-training
https://github.com/microsoft/GLIP

RegionCLIP: Region-based Language-Image Pretraining
https://github.com/microsoft/RegionCLIP

Detecting Twenty-thousand Classes using Image-level Supervision
https://github.com/facebookresearch/Detic/

Object Detection in the Wild 
https://eval.ai/web/challenges/challenge-page/1839/overview

Grounded-Segment-Anything
https://github.com/IDEA-Research/Grounded-Segment-Anything

Grounding DINO Open-Set Detection. Detect everything with language
https://github.com/IDEA-Research/GroundingDINO

"DINO: DETR with Improved DeNoising Anchor Boxes for End-to-End Object Detection".
https://github.com/IDEA-Research/DINO

NMS Strikes Back (Detection Transformers with Assignment, DETA) 2022
https://arxiv.org/abs/2212.06137
https://github.com/jozhang97/DETA

NMS又回来了！DETR中的NMS反击！（DETA 得克萨斯大学） 2022
https://zhuanlan.zhihu.com/p/594459397

Openai在人工智能领域的一个重要进展：Q-transformers 和 Q-learning 
https://mp.weixin.qq.com/s?__biz=Mzg3NTk0ODAxNg==&mid=2247484728&idx=1&sn=983d5973ec52e09d60b12ce66b2272be

Ultimate Vocal Remover GUI v5.6 人声伴奏分离软件
https://github.com/Anjok07/ultimatevocalremovergui

Action-Conditioned 3D Human Motion Synthesis with Transformer VAE
https://openaccess.thecvf.com/content/ICCV2021/papers/Petrovich_Action-Conditioned_3D_Human_Motion_Synthesis_With_Transformer_VAE_ICCV_2021_paper.pdf

Paint by Inpaint: Learning to Add Image Objects by Removing Them First 2024
https://github.com/RotsteinNoam/Paint-by-Inpaint
https://paperswithcode.com/paper/paint-by-inpaint-learning-to-add-image

Grounding Everything: Emerging Localization Properties in Vision-Language Transformers 2023
zero-shot open-vocabulary segmentation
https://github.com/walbouss/gem

ViM: Out-Of-Distribution with Virtual-logit Matching 2022
OOD scoring method named Virtual-logit Matching (ViM), which combines the class-agnostic score from feature space and the In-Distribution (ID) class-dependent logits.
https://paperswithcode.com/paper/vim-out-of-distribution-with-virtual-logit
https://github.com/haoqiwang/vim

MusePose: a Pose-Driven Image-to-Video Framework for Virtual Human Generation.
https://github.com/TMElyralab/MusePose

Animate Anyone: Consistent and Controllable Image-to-Video Synthesis for
Character Animation
https://arxiv.org/pdf/2311.17117
https://github.com/HumanAIGC/AnimateAnyone
https://github.com/MooreThreads/Moore-AnimateAnyone

Yolov5 Quantization Aware Training QAT
https://github.com/cshbli/yolov5_qat

YOLOv5-QAT量化部署
https://blog.csdn.net/qq_40672115/article/details/133774516

YOLOv5-PTQ量化部署
https://blog.csdn.net/qq_40672115/article/details/133325424

TensoRT量化第四课：PTQ与QAT
https://blog.csdn.net/qq_40672115/article/details/130489067

(原型) PyTorch 2 导出量化感知训练 (QAT)
https://pytorch.ac.cn/tutorials/prototype/pt2e_quant_qat.html

Quantize ONNX Models
https://onnxruntime.ai/docs/performance/model-optimizations/quantization.html
