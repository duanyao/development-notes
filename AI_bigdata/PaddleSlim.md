## 参考资料

[2.1] 在PaddleDetection中, 提供了基于PaddleSlim进行模型压缩的完整教程和benchmark
  https://gitee.com/paddlepaddle/PaddleDetection/tree/release/2.6/configs/slim
  
[2.2] PaddleDetectionDistillation(蒸馏)
  https://gitee.com/paddlepaddle/PaddleDetection/tree/release/2.6/configs/slim/distill

[2.3] PPYOLOE+ Distillation(PPYOLOE+ 蒸馏)
  https://gitee.com/paddlepaddle/PaddleDetection/tree/release/2.6/configs/ppyoloe/distill/README.md
