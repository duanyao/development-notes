Real-time object detection with YOLO (YOLOv2, Tiny YOLO)
  https://machinethink.net/blog/object-detection-with-yolo/

yolov5
  https://github.com/ultralytics/yolov5/

YOLOv5 Documentation 
  https://docs.ultralytics.com/

Tips for Best Training Results
  https://github.com/ultralytics/yolov5/wiki/Tips-for-Best-Training-Results

TFLite, ONNX, CoreML, TensorRT Export 
  https://github.com/ultralytics/yolov5/issues/251

Example of performing inference with ultralytics YOLO V5, OpenCV 4.5.4 DNN, C++ and Python
  https://github.com/doleron/yolov5-opencv-cpp-python

Model Pruning/Sparsity
  https://docs.ultralytics.com/tutorials/pruning-sparsity/
  
yolov3
  https://github.com/ultralytics/yolov3/

yolof (2021) in mmdetection
  https://github.com/open-mmlab/mmdetection/blob/master/configs/yolof/README.md

YOLOX: Exceeding YOLO Series in 2021
  https://arxiv.org/abs/2107.08430

YOLOX(2021) original
  https://github.com/Megvii-BaseDetection/YOLOX

YOLOX (2021) in mmdetection
  https://github.com/open-mmlab/mmdetection/blob/master/configs/yolox/README.md
  
YOLOv6：又快又准的目标检测框架
  https://mp.weixin.qq.com/s/RrQCP4pTSwpTmSgvly9evg

YOLOv6
  https://github.com/meituan/YOLOv6

YOLOv5 CPU Export Benchmarks #6613 
  https://github.com/ultralytics/yolov5/pull/6613/
  
YOLOv5 Export Benchmarks for GPU #6963 
  https://github.com/ultralytics/yolov5/pull/6963

Supercharging YOLOv5: How I Got 182.4 FPS Inference Without a GPU
  https://dicksonneoh.com/portfolio/supercharging_yolov5_180_fps_cpu/
  
Faster than GPU: How to 10x your Object Detection Model and Deploy on CPU at 50+ FPS
  https://dicksonneoh.com/portfolio/how_to_10x_your_od_model_and_deploy_50fps_cpu/

How can we speed up Yolov5?? #8065 
  https://github.com/ultralytics/yolov5/issues/8065

Multi-label detection on ONE bounding box #8200 
  https://github.com/ultralytics/yolov5/issues/8200

How to do multi-label classification? #1435 
  https://github.com/ultralytics/yolov5/issues/1435

add multi-label training #5593 （2022.6.21 尚未合并）
  https://github.com/ultralytics/yolov5/pull/5593

