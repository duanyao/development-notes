## 参考资料
http://www.paddlepaddle.org/docs/develop/documentation/fluid/en/build_and_install/docker_install_en.html

## 安装

### 安装依赖项

下载 git-lfs （ https://git-lfs.github.com/），解压后命令行执行：
sudo ./install.sh
git lfs install

### linux 环境，以 debian 系为例。

sudo apt-get install -y patchelf

git clone https://github.com/PaddlePaddle/Paddle.git Paddle.git --depth 5

cd Paddle.git/

mkdir build

cd build

cmake -DCMAKE_BUILD_TYPE=Release -DWITH_MKLDNN=OFF -DWITH_GPU=OFF -DWITH_FLUID_ONLY=ON ..

make -j4

初次 make 时会 git 下载一些依赖库到 build 目录，耗时较长。

当前（2018.7）的 Paddle 可以用 g++ 5.4 编译。经测试 g++ 7.2 会出错，见“编译错误的处理”。

### 编译错误的处理：
1.

  ```
  Paddle.git/paddle/fluid/string/tinyformat/tinyformat.h:688:15: error: this statement may fall through [-Werror=implicit-fallthrough=]
        out.setf(std::ios::uppercase);
  ```
  修改：
  Paddle.git/paddle/fluid/string/tinyformat/tinyformat.h
  ```
    case 'X':
      out.setf(std::ios::uppercase);
      [[fallthrough]]; // 增加一行
  ```
2.

  ```
  /home/duanyao/project/Paddle.git/build/third_party/boost/src/extern_boost/boost_1_41_0/boost/variant/variant.hpp:465:9: error: ‘*((void*)& temp +8)’ may be used uninitialized in this function [-Werror=maybe-uninitialized]
          lhs_content = *static_cast< const T* >(rhs_storage_);
  ```
  修改：

  Paddle.git/cmake/flags.cmake
  `-Werror` 改为 `#-Werror` ，即注释掉。

3.

  ```
  In file included from /home/duanyao/project/Paddle.git/paddle/fluid/operators/concurrency/channel_util.h:17:0,
                  from /home/duanyao/project/Paddle.git/paddle/fluid/operators/concurrency/channel_util.cc:15:
  /home/duanyao/project/Paddle.git/paddle/fluid/framework/channel.h:49:32: error: ‘std::function’ has not been declared
                            std::function<bool(ChannelAction)> cb) = 0;
                                  ^~~~~~~~
  /home/duanyao/project/Paddle.git/paddle/fluid/framework/channel.h:49:40: error: expected ‘,’ or ‘...’ before ‘<’ token
  ```
暂时无解。

### 安装 paddle 预编译版（通过 docker）
`sudo docker pull docker.paddlepaddlehub.com/paddle`

这是个 ubuntu xenial 镜像，预装了 paddle、gcc（5.4）、python2，但缺少 cmake、git、patchelf、automake、bison。

```
cd /home/duanyao/project

sudo docker run -it -v $PWD:/project -w /project docker.paddlepaddlehub.com/paddle /bin/bash
```

-i 交互式， -t 使用终端。如果要一次性运行一个程序则不需要带这两个参数。
-v $PWD:/project 挂载当前目录（$PWD）到容器内的 /project 目录。
-w /project 进入容器后，进入 /project 目录。
docker.paddlepaddlehub.com/paddle 容器名。
/bin/bash 进入容器后要运行的程序，即bash。

启动 docker 容器后，可以附加更多的 shell 上去：

```
sudo docker exec -it xxx bash
```

“xxx” 是 docker 容器的id的前几位，可以用 sudo docker ps 来查看。

```
apt-get update
apt-get install cmake git patchelf automake bison

mkdir build_d
cd build_dsudo ./install.sh
cmake -DCMAKE_BUILD_TYPE=Release -DWITH_MKLDNN=OFF -DWITH_GPU=OFF -DWITH_FLUID_ONLY=ON ..
make -j4

pip install --upgrade pip
pip install python/dist/*.whl

make -j4 inference_lib_dist
```

Fluid预测库的编译产出会生成在build_d/fluid_install_dir目录。

```
sudo docker volume create paddle-1
sudo docker volume ls
sudo docker volume inspect paddle-1

sudo docker run -it -v paddle-1:/var -v $PWD:/project -w /project docker.paddlepaddlehub.com/paddle /bin/bash 

cd lac.git
mkdir build
cd build

cmake -DPADDLE_ROOT=../../Paddle.git/build_d/fluid_install_dir/ ..
make
make install # 编译产出在 ../output 下
```

编译错误的处理

1. 
```
In file included from /project/lac.git/src/main_tagger.cpp:15:0:
/project/lac.git/src/main_tagger.h:21:41: fatal error: paddle/fluid/framework/init.h: No such file or directory

find -name init.h
./build_d/fluid_install_dir/paddle/fluid/platform/init.h
```

疑似 "framework" 应为 "platform" ，更改后解决。

### 运行推测程序
在主机环境下进入 lac.git 目录，执行：
```
git lfs fetch
git lfs checkout
```
在 docker 环境下执行：
`output/demo/lac_demo conf 32 4`

输入文本，按 ctrl+D 确认，就会输出结果。
