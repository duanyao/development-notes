## 参考资料

[4.1] 《PaddlePaddle从入门到炼丹》系列教程 
  https://blog.doiduoyi.com/articles/1585978647203.html

[5.0] paddle.inference 推理 API
[5.1] 预测示例 (Python)
  https://paddle-inference.readthedocs.io/en/latest/quick_start/python_demo.html

[6.0] paddle 模型转换为其它模型

[6.1] paddle2onnx
  https://github.com/PaddlePaddle/Paddle2ONNX
[6.2] OpenVINO部署PaddleDetection模型
  https://github.com/PaddlePaddle/Paddle2ONNX/blob/release/0.9/experimental/openvino_ppdet_cn.md
[6.3] paddle2onnx报错 #415 
  https://github.com/PaddlePaddle/Paddle2ONNX/issues/415

[6.4] Paddle2onnx 转化Paddledetection的fasterrcnn模型出现ops不支持错误
  https://github.com/PaddlePaddle/Paddle2ONNX/issues/462

## 安装

### 安装 paddle

#### linux python venv
```
source ~/opt/venv/v39-1/bin/activate
python -m pip install paddlepaddle-gpu==2.2.0.post112 -f https://www.paddlepaddle.org.cn/whl/linux/mkl/avx/stable.html
# 其它版本： 2.3.2.post112, 2.4.2.post112, 2.5.2.post112
```

#### windows 

conda + pip

```
conda activate c39
conda install cudatoolkit cudnn
python -m pip install paddlepaddle-gpu==2.2.1.post112 -f https://www.paddlepaddle.org.cn/whl/windows/mkl/avx/stable.html
```

conda
```
conda activate c39
conda install paddlepaddle-gpu==2.2.1 -c https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/Paddle/ -c conda-forge
```

#### nvidia docker 镜像 + python venv

不论是推理还是训练，paddlepaddle-gpu==2.2.0.post112 都需要基于 cudnn-devel 镜像，例如 `nvidia/cuda:11.2.2-cudnn8-devel-ubuntu20.04`，cudnn8-runtime 镜像如 `nvidia/cuda:11.2.2-cudnn8-runtime-ubuntu20.04` 仍然缺少一些东西，如 libcudnn.so 和 libcublas.so 。

#### paddle docker 镜像

基于 ubuntu 18.04, python 3.7
paddle 包的位置：
'/usr/local/lib/python3.7/dist-packages/paddle/__init__.py'

#### 验证 paddle 安装正确
```
python -c "import paddle; paddle.utils.run_check()"
```
这一步会验证 cuda、nccl是否正确安装，但似乎不会验证cudnn。
需要保证 nccl、cudnn 匹配 cuda 的小版本号。

如果报错与 nccl 有关，且指向不明，可以进一步让 nccl 输出警告信息：
```
NCCL_DEBUG=WARN python -c "import paddle; paddle.utils.run_check()"
```

例如，在容器内使用多 GPU 时，paddle可能报错：
```
a4881a87de8e:9:59 [1] include/shm.h:28 NCCL WARN Call to posix_fallocate failed : No space left on device

a4881a87de8e:9:59 [1] include/shm.h:48 NCCL WARN Error while creating shared memory segment nccl-shm-recv-662d9f0ae52cdf1-1-0-1 (size 9637888)
```
这是因为 docker 默认的共享内存太小。可以加上 `--shm-size=1g --ulimit memlock=-1`：

```
docker run --gpus all --shm-size=1g --ulimit memlock=-1 -it --rm local/ubuntu2004-cuda112-cudnn8-dev-py39-cv-paddle242cu112 bash
```

### 安装 visualdl

pip install visualdl

### 安装 tensorrt

paddle 实际上只需要 tensorrt 中的 libnvinfer 和 libnvinfer-plugin，它们也在 cuda 的仓库里。在 debian/ubuntu 系列中：

```
sudo apt install libnvinfer8=8.6.1.6-1+cuda11.8 libnvinfer-dev=8.6.1.6-1+cuda11.8 libnvinfer-headers-dev=8.6.1.6-1+cuda11.8
sudo apt install libnvinfer-plugin8=8.6.1.6-1+cuda11.8 libnvinfer-plugin-dev=8.6.1.6-1+cuda11.8 libnvinfer-headers-plugin-dev=8.6.1.6-1+cuda11.8
```
其中， libnvinfer8 提供 libnvinfer.so.8， 而 libnvinfer-dev 提供 libnvinfer.so（指向 libnvinfer.so.8.x.x ）。
有些程序依赖 libnvinfer.so ，就需要安装 libnvinfer-dev 而不仅是 libnvinferX 。
类似的，libnvinfer-plugin-dev 提供 libnvinfer_plugin.so 

PaddlePaddle 就依赖 libnvinfer.so 和 libnvinfer_plugin.so（见 /paddle/phi/backends/dynload/tensorrt.cc#L67），如果缺少其中之一，会报告错误：

```
(NotFound) TensorRT is needed, but TensorRT dynamic library is not found.
  [Hint: dso_handle should not be null.] (at /paddle/paddle/fluid/platform/dynload/tensorrt.cc:47)
```

PaddlePaddle 2.3 声称可使用 TensorRT 8.0/8.2，但更新的 TensorRT 8.x 应该也能用。PaddlePaddle 2.4 在这方面应该没有变化。

## Tensor 操作

### 创建，转换
```
import paddle
x = paddle.to_tensor([1.0, 2.0, 3.0], dtype='float32', place=paddle.CPUPlace()) # or CUDAPlace
print("tensor's type is: {}".format(x.dtype))
```
paddle. to_tensor ( data, dtype=None, place=None, stop_gradient=True )

通过已知的 data 来创建一个 Tensor，Tensor 类型为 paddle.Tensor。 data 可以是 scalar，tuple，list，numpy.ndarray，paddle.Tensor。

如果 data 已经是一个 Tensor，且 dtype 、 place 没有发生变化，将不会发生 Tensor 的拷贝并返回原来的 Tensor。 否则会创建一个新的 Tensor，且不保留原来计算图。

### 转置，移轴
与 numpy 类似，但返回新的 Tensor 。
paddle.transpose(x, perm, name=None)
```
x = paddle.randn([2, 3, 4])
x_transposed = paddle.transpose(x, perm=[1, 0, 2])
```
paddle.moveaxis(x, source, destination, name=None)
```
paddle.moveaxis(x, 0, 1)
paddle.moveaxis(x, [0, 1], [1, 2])
```

## CPU 多线程控制
https://github.com/PaddlePaddle/PARL/issues/319

https://github.com/OpenMathLib/OpenBLAS

```
def paddle_set_thread_count(n: int = 0):
    if n > 0:
        # https://github.com/PaddlePaddle/Paddle/issues/17615
        import os
        os.environ["MKL_NUM_THREADS"] = f'{n}'
        os.environ["NUMEXPR_NUM_THREADS"] = f'{n}'
        os.environ["OMP_NUM_THREADS"] = f'{n}'

def opencv_set_thread_count(n: int = 0):
    import cv2
    n0 = cv2.getNumThreads()
    cv2.setNumThreads(n)
    n1 = cv2.getNumThreads()
    logging.info(f'opencv_set_thread_count:n={n}, before={n0}, after={n1}')
```

```
WARNING: OMP_NUM_THREADS set to 4, not 1. The computation speed will not be optimized if you use data parallel. It will fail if this PaddlePaddle binary is compiled with OpenBlas since OpenBlas does not support multi-threads.
PLEASE USE OMP_NUM_THREADS WISELY.
```

## paddle infer

### TensorRT


/home/rd/opt/venv/v39-1/lib/python3.9/site-packages/paddleclas/deploy/utils/predictor.py

```
config.enable_tensorrt_engine(
                precision_mode=precision,
                max_batch_size=args.batch_size,
                workspace_size=1 << 30,
                use_static=True, # duanyao
                min_subgraph_size=30,
                use_calib_mode=False)
```

参数解释：
void EnableTensorRtEngine(int workspace_size = 1 << 20, int max_batch_size = 1, int min_subgraph_size = 3, Precision precision = Precision::kFloat32, bool use_static = false, bool use_calib_mode = true)

Turn on the TensorRT engine. The TensorRT engine will accelerate some subgraphes in the original Fluid computation graph. In some models such as resnet50, GoogleNet and so on, it gains significant performance acceleration.

Parameters

        workspace_size: The memory size(in byte) used for TensorRT workspace.

        max_batch_size: The maximum batch size of this prediction task, better set as small as possible for less performance loss.

        min_subgrpah_size: The minimum TensorRT subgraph size needed, if a subgraph is smaller than this, it will not be transferred to TensorRT engine.

        precision: The precision used in TensorRT.

        use_static: Serialize optimization information to disk for reusing.

        use_calib_mode: Use TRT int8 calibration(post training quantization).

如果不使用磁盘缓存优化结果（ use_static=False，默认 ），则每次创建 predictor 的时候要重复优化。这可能有两个问题：（1）优化时间较长，可达几十秒。（2）初期预测速度可能不稳定，达不到最佳效果。

设置缓存路径
paddle infer Config 类 10. 设置缓存路径
https://www.paddlepaddle.org.cn/inference/master/api_reference/python_api_doc/Config/OtherFunction.html#shezhihuancunlujing
paddle infer 默认在模型目录下创建 _opt_cache 目录来存储 TensorRT 优化结果。也可以用 `config.set_optim_cache_dir()` 来改变。

```
# 引用 paddle inference 预测库
import paddle.inference as paddle_infer

# 创建 config
config = paddle_infer.Config("./mobilenet_v1.pdmodel", "./mobilenet_v1.pdiparams")

# 设置缓存路径
config.set_optim_cache_dir("./OptimCacheDir")
```

使用磁盘缓存优化结果可能存在兼容性问题：在一个版本的 TensorRT 上优化的结果，在另一个版本的 TensorRT 上使用，输出的结果可能完全错误。目前尚不清楚哪些组件的版本是关键因素，怀疑对象有 libnvinfer、paddle、cuda、cudnn 等。

## 书/系列教程
### PaddlePaddle从入门到炼丹
[4.1]
这个专栏是深度学习框架 PaddlePaddle Fluid 版本的教程，开发环境主要是 PaddlePaddle 1.6.0 和 Python 3.5。内容涉及了 PaddlePaddle 的安装，并从简单执行 1+1 运算例子入门 PaddlePaddle，借助各个实例一步步入手 PaddlePaddle，通过本系列教程你可以学到如何使用 PaddlePaddle 搭建卷积神经网络，循环神经网络，并能够训练自定义数据集，最后还可以部署到自己的实际项目中。
本系列目录

第一章 新版本 PaddlePaddle 的安装
第二章 计算 1+1
第三章 线性回归
第四章 卷积神经网络
第五章 循环神经网络
第六章 生成对抗网络
第七章 强化学习
第八章 模型的保存与使用
第九章 迁移学习
第十章 VisualDL 训练可视化
第十一章 自定义图像数据集识别
第十二章 自定义文本数据集分类
第十三章 自定义图像数生成
第十四章 把预测模型部署在服务器
第十五章 把预测模型部署到 Android 手机上


## 推理 API

paddle.inference 包参考[5.1]：

```
import argparse
import numpy as np

# 引用 paddle inference 预测库。来自 site-packages/paddle/inference/__init__.py
import paddle.inference as paddle_infer

def main():
    args = parse_args()

    # 创建 config
    config = paddle_infer.Config(args.model_file, args.params_file)

    # 根据 config 创建 predictor
    predictor = paddle_infer.create_predictor(config)

    # 获取输入的名称
    input_names = predictor.get_input_names()
    input_handle = predictor.get_input_handle(input_names[0])

    # 设置输入
    fake_input = np.random.randn(args.batch_size, 3, 318, 318).astype("float32")
    input_handle.reshape([args.batch_size, 3, 318, 318])
    input_handle.copy_from_cpu(fake_input)

    # 运行predictor
    predictor.run()

    # 获取输出
    output_names = predictor.get_output_names()
    output_handle = predictor.get_output_handle(output_names[0])
    output_data = output_handle.copy_to_cpu() # numpy.ndarray类型
    print("Output data size is {}".format(output_data.size))
    print("Output data shape is {}".format(output_data.shape))

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--model_file", type=str, help="model filename")
    parser.add_argument("--params_file", type=str, help="parameter filename")
    parser.add_argument("--batch_size", type=int, default=1, help="batch size")
    return parser.parse_args()

if __name__ == "__main__":
    main()

```

不过，这个例子并不完整，没有输入的预处理和输出结果的解析。完整的例子可参考 PaddleDetection（ deploy/python/infer.py ）。

```
class Detector(object)

    def __init__(self,
                 pred_config, # config of model, defined by `Config(model_dir)`
                 model_dir, # model_dir (str): root path of model.pdiparams, model.pdmodel and infer_cfg.yml
                 device='CPU', # CPU/GPU/XPU
                 run_mode='fluid', # mode of running(fluid/trt_fp32/trt_fp16)
                 batch_size=1,
                 cpu_threads=1,
                 enable_mkldnn=False)
   
   # 返回值为 dict， { boxes: np.array<float32, (N, 6)>, boxes_num: np.array<int32, (B,)> }。B为批量大小（image_list的长度），N为预测值数量。
   # boxes_num 表示每张输入图像中的预测值的数量。boxes 表示全部图像的预测值，按输入的顺序排列。
   # boxes 的第2轴定义： [class, score, x_min, y_min, x_max, y_max]，
   # 坐标单位是像素，在图像空间；class 是从 0 开始的整数，要翻译成类名，应使用 Detector::pred_config.labels 属性。
   def predict(self,
        image_list, # 图像文件的路径的列表。列表的长度即是批量（batch size），不能太大。
        threshold=0.5, # 预测值的置信度阈值，低于此值的应该被丢弃。注意：目前（PaddleDetection 2.3）没有实现此功能，此参数不起作用，而是在可视化阶段进行过滤。
        warmup=0,  # 仅 benchmark 时 > 0
        repeats=1 # 仅 benchmark 时 > 1
        ):
```

## 显存、内存使用量控制
https://www.paddlepaddle.org.cn/documentation/docs/en/guides/flags/memory_en.html
What's the real meaning of FLAGS_fraction_of_gpu_memory_to_use? https://github.com/PaddlePaddle/Paddle/issues/15426

```
FLAGS_fraction_of_gpu_memory_to_use
FLAGS_eager_delete_scope
FLAGS_initial_cpu_memory_in_mb
```

以上参数对于 paddle infer 引擎（启用 tensorrt）似乎是没效果的。按文档来说，没有参数能限制显存使用量的上限。

同样的模型在不同显卡上的显存使用量是不同的，原因不明。

## 预处理 API （ paddle.vision.transforms ）
数据预处理
https://www.paddlepaddle.org.cn/documentation/docs/zh/guides/beginner/data_preprocessing_cn.html

`paddle.vision.transforms` 与 `torch.vision.transforms` 是类似的。

### Resize
class paddle.vision.transforms. Resize ( size, interpolation='bilinear', keys=None ) 
size (int|list|tuple) - 输出图像大小。如果 size 是一个序列，例如（h，w），输出大小将与此匹配。如果 size 为 int，图像的较小边缘将与此数字匹配，即如果 height > width，则图像将重新缩放为(size * height / width, size)
后端有 PIL 和 CV2 两种。

### normalize

paddle.vision.transforms. normalize ( img, mean, std, data_format='CHW', to_rgb=False )

img (PIL.Image|np.array|paddle.Tensor) – input data to be normalized.

mean (list|tuple) – Sequence of means for each channel.

std (list|tuple) – Sequence of standard deviations for each channel.

### Normalize
https://www.paddlepaddle.org.cn/documentation/docs/zh/api/paddle/vision/transforms/Normalize__upper_cn.html

class paddle.vision.transforms. Normalize ( mean=0.0, std=1.0, data_format='CHW', to_rgb=False, keys=None )

mean (int|float|list|tuple，可选) - 用于每个通道归一化的均值。

std (int|float|list|tuple，可选) - 用于每个通道归一化的标准差值。

data_format (str，可选) - 数据的格式，必须为 'HWC' 或 'CHW'。 默认值为 'CHW'。

to_rgb (bool，可选) - 是否转换为 rgb 的格式。默认值为 False。

keys (list[str]|tuple[str]，可选) - 与 BaseTransform 定义一致。默认值为 None。

### 静态图/jit 预处理

```
transforms = torch.nn.Sequential(
    transforms.CenterCrop(10),
    transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
)
scripted_transforms = torch.jit.script(transforms)
```

https://www.paddlepaddle.org.cn/documentation/docs/zh/api/paddle/nn/Sequential_cn.html
```
>>> import paddle

>>> data = paddle.uniform(shape=[30, 10], dtype='float32')
>>> # create Sequential with iterable Layers
>>> model1 = paddle.nn.Sequential(
...     paddle.nn.Linear(10, 1), paddle.nn.Linear(1, 2)
>>> )
>>> model1[0]  # access the first layer
>>> res1 = model1(data)  # sequential execution

>>> # create Sequential with name Layer pairs
>>> model2 = paddle.nn.Sequential(
...     ('l1', paddle.nn.Linear(10, 2)),
...     ('l2', paddle.nn.Linear(2, 3))
>>> )
>>> model2['l1']  # access l1 layer
>>> model2.add_sublayer('l3', paddle.nn.Linear(3, 3))  # add sublayer
>>> res2 = model2(data)  # sequential execution
```

```
>>> import paddle
>>> from paddle.jit import to_static

>>> @to_static
>>> def func(x):
...     if paddle.mean(x) < 0:
...         x_v = x - 1
...     else:
...         x_v = x + 1
...     return x_v
...
>>> x = paddle.ones([1, 2], dtype='float32')
>>> x_v = func(x)
>>> print(x_v)
Tensor(shape=[1, 2], dtype=float32, place=Place(cpu), stop_gradient=True,
[[2., 2.]])
```

## Paddle2ONNX: paddle -> onnx -> openvino
[6.2-6.4]

paddle2onnx 目前并非支持所有的 paddle 输出的模型。yolov3 是确定支持的，rcnn 类还在开发[6.4]，ssd 应该是支持的。

安装：

```
pip install onnx==1.9.0
cd Paddle2ONNX.git/
python setup.py install
```

将以下脚本置于 Paddle2ONNX.git/experimental 目录（含有 openvino_ppdet 模块），运行：

```
import paddle2onnx
import paddle
from openvino_ppdet import nms_mapper
# 通过上面的`nms_mapper`的import来启用插件，替换了paddle2onnx原始的nms_mapper

model_prefix = "/home/duanyao/project/PaddleDetection.git/inference_model/faster_rcnn_r101_vd_fpn_2x_custom_head_coco_05/model"
model = paddle.jit.load(model_prefix)
input_shape_dict = {
    "image": [1, 3, 576, 1024], # [1, 3, 608, 608],
    "scale_factor": [1, 2],
    "im_shape": [1, 2]
    }
onnx_model = paddle2onnx.run_convert(model, input_shape_dict=input_shape_dict, opset_version=11)

with open("./frcnn.onnx", "wb") as f:
    f.write(onnx_model.SerializeToString())
```

出错：

```
There's 7 ops are not supported yet
=========== while ===========
=========== tensor_array_to_tensor ===========
=========== select_input ===========
=========== write_to_array ===========
=========== conditional_block ===========
=========== lod_array_length ===========
=========== generate_proposals_v2 ===========
```

这是因为Paddle2ONNX目前尚不支持带有控制流模型的转换，faster_rcnn 属于此类。

尝试 ssd_r34：
```
import paddle2onnx
import paddle
from openvino_ppdet import nms_mapper
# 通过上面的`nms_mapper`的import来启用插件，替换了paddle2onnx原始的nms_mapper

model_prefix = "/home/duanyao/project/PaddleDetection.git/inference_model/ssd_r34_70e_custom_head_coco_05/model"
model = paddle.jit.load(model_prefix)
input_shape_dict = {
    "image": [1, 3, 300, 300], # [1, 3, 608, 608],
    "scale_factor": [1, 2],
    "im_shape": [1, 2]
    }
onnx_model = paddle2onnx.run_convert(model, input_shape_dict=input_shape_dict, opset_version=11)

with open("./ssd_r34_70e_custom_head_coco_05.onnx", "wb") as f:
    f.write(onnx_model.SerializeToString())
```
可以成功转换。

再从 onnx 转换为 openvino 模型：
```
mo --framework onnx --input_model ssd_r34_70e_custom_head_coco_05.onnx --output_dir ov_model

Model Optimizer arguments:
Common parameters:
        - Path to the Input Model:      /home/duanyao/project/Paddle2ONNX.git/experimental/ssd_r34_70e_custom_head_coco_05.onnx
        - Path for generated IR:        /home/duanyao/project/Paddle2ONNX.git/experimental/ov_model
        - IR output name:       ssd_r34_70e_custom_head_coco_05
        - Log level:    ERROR
        - Batch:        Not specified, inherited from the model
        - Input layers:         Not specified, inherited from the model
        - Output layers:        Not specified, inherited from the model
        - Input shapes:         Not specified, inherited from the model
        - Mean values:  Not specified
        - Scale values:         Not specified
        - Scale factor:         Not specified
        - Precision of IR:      FP32
        - Enable fusing:        True
        - Enable grouped convolutions fusing:   True
        - Move mean values to preprocess section:       None
        - Reverse input channels:       False
ONNX specific parameters:
        - Inference Engine found in:    /home/duanyao/opt/venv/v39/lib/python3.9/site-packages/openvino
Inference Engine version:       2021.4.2-3976-0943ed67223-refs/pull/539/head
Model Optimizer version:        2021.4.2-3976-0943ed67223-refs/pull/539/head

[ SUCCESS ] Generated IR version 10 model.
[ SUCCESS ] XML file: /home/duanyao/project/Paddle2ONNX.git/experimental/ov_model/ssd_r34_70e_custom_head_coco_05.xml
[ SUCCESS ] BIN file: /home/duanyao/project/Paddle2ONNX.git/experimental/ov_model/ssd_r34_70e_custom_head_coco_05.bin
[ SUCCESS ] Total execution time: 99.15 seconds. 
[ SUCCESS ] Memory consumed: 310 MB.
```

测试：

```
python /home/duanyao/project/openvino_model_zoo.git/demos/object_detection_demo/python/object_detection_demo.py --loop -at ssd -m /home/duanyao/project/Paddle2ONNX.git/experimental/ov_model/ssd_r34_70e_custom_head_coco_05.xml -i  ~/project/annotation/1625185842605/1.jpg
```
失败，输出格式不兼容。

尝试 yolov3_darknet53_270e_coco：

```
import paddle2onnx
import paddle
from openvino_ppdet import nms_mapper
# 通过上面的`nms_mapper`的import来启用插件，替换了paddle2onnx原始的nms_mapper

model_prefix = "/home/duanyao/project/PaddleDetection.git/inference_model/yolov3_darknet53_270e_coco/model"
model = paddle.jit.load(model_prefix)
input_shape_dict = {
    "image": [1, 3, 608, 608],
    "scale_factor": [1, 2],
    "im_shape": [1, 2]
    }
onnx_model = paddle2onnx.run_convert(model, input_shape_dict=input_shape_dict, opset_version=11)

with open("./yolov3_darknet53_270e_coco.onnx", "wb") as f:
    f.write(onnx_model.SerializeToString())
```

```
mo --framework onnx --input_model yolov3_darknet53_270e_coco.onnx --output_dir ov_model
```
