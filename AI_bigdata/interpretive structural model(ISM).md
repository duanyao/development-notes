## 参考资料

Interpretive Structural Modelling (ISM) approach: An Overview
http://www.isca.in/IJMS/Archive/v2/i2/2.ISCA-RJMS-2012-054.pdf

Interpretive Structural Modeling (ISM) Software and Training Package 
http://www.sorach.com/ism.html

Interpretive structural modelling: a methodology for structuring complex issues
http://www.sorach.com/resource/janes/janes.php

Hierarchical matrix
https://en.wikipedia.org/wiki/Hierarchical_matrix

## 知识点脉络图算法

* 邻接矩阵 
* 可达矩阵
* 层次矩阵（看起来与 Hierarchical matrix 没什么关系？）

主要思想是：
（1）知识点之间有“因果关系”（或叫依赖关系），表现为有向图，进一步表现为邻接矩阵，进一步计算出可达矩阵。
（2）从可达矩阵中找出“根知识点”，即没有其它“因”的知识点，作为一个“层”。
（3）除去上述“层”中的知识点，重复1-2，得到更多的“层”。
（4）按“层”的顺序整理知识点。

## 应用
### 出题算法
学生做错一道题，可能是直接涉及的知识点不掌握，也可能是前序知识点不掌握。
通过知识点的依赖关系可以推出关于前序知识点的题目进行测试。

不过，仅仅是出题似乎还没有必要整理层次。

### 个人学习路径规划
按知识点层级规划学习的顺序。
不过课程规划还需要考虑掌握知识点需要的时间。
