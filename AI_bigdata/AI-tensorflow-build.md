## 参考资料

[2.1] TensorFlow deepin 安装
  https://wiki.deepin.org/index.php?title=TensorFlow

[2.2] 机器学习进阶笔记之一 | TensorFlow安装与入门
  https://zhuanlan.zhihu.com/p/22410917

[2.3] Tensorflow不同版本要求与CUDA及CUDNN版本对应关系
  https://blog.csdn.net/omodao1/article/details/83241074

[2.4] CUDA 和 CUDNN 的linux包下载地址
  https://www.zhihu.com/question/37082272

[2.5] libcudnn和libnccl deb 仓库
  https://developer.download.nvidia.cn/compute/machine-learning/repos/ubuntu1604/x86_64/

[2.6] 显卡驱动和cuda开发套件 deb 仓库
  https://developer.download.nvidia.cn/compute/cuda/repos/ubuntu1604/x86_64/
  
[2.7] tensorrt-support-matrix
  https://docs.nvidia.com/deeplearning/sdk/tensorrt-support-matrix/index.html

[3.0] 从源码安装

[3.1] How to install Tensorflow GPU with CUDA Toolkit 9.1 and cuDNN 7.1.2 for Python 3 on Ubuntu 16.04-64bit 
  https://www.pytorials.com/install-tensorflow141-gpu/

[3.2] How to really install tensorflow-gpu from source on a clean Ubuntu
  https://github.com/rnreich/ubuntu-tensorflow-gpu-all-versions

[3.3] Protobuf error, mismatch with system installed version
  https://github.com/tensorflow/tensorflow/issues/23375

[3.4] No module named tensorflow.python.platform
  https://github.com/tensorflow/tensorflow/issues/374

[3.5] bazel error: name 'http_archive' is not defined
  https://github.com/tensorflow/tensorflow/issues/27189

[3.6] Cannot find libdevice.10.bc under /usr/local/cuda-8.0
  https://github.com/tensorflow/tensorflow/issues/17801

[3.7] A problem about gcc 5.5.0 after upgrade. It does not compile TensorFlow. 
  https://www.linuxquestions.org/questions/slackware-14/a-problem-about-gcc-5-5-0-after-upgrade-it-does-not-compile-tensorflow-4175623391/

[3.8] [Compile Error] Compiling tf r1.8 from source in cuda9.0_cudnn7.0 container failed.
  https://github.com/tensorflow/tensorflow/issues/22527

[3.9] Build Tensorflow 1.7 on Cuda 8.0
  https://github.com/tensorflow/tensorflow/issues/19203

[3.10] [Tutorial CUDA] Nvidia GPU: CUDA Compute Capability
  https://www.myzhar.com/blog/tutorials/tutorial-nvidia-gpu-cuda-compute-capability/

[3.11] deepin 15.10.2 安装 tensorflow-GPU 1.12(Cuda 9.0, Cudnn 7.6)
  https://jansora.com/p/deepin-tensorflow

[3.12] undefined symbol: _ZTVN6icu_639ErrorCodeE while building v1.12.2
  https://github.com/tensorflow/tensorflow/issues/28900

[3.13] Output Directory Layout
  https://docs.bazel.build/versions/master/output_directories.html
  
[3.14] 从源代码编译 tensorflow
  https://tensorflow.google.cn/install/source

### 从源码安装
tf 1.10 以上的编译需要安装 keras_applications 和 keras_preprocessing：
```
sudo pip3 install keras_applications --no-deps
sudo pip3 install keras_preprocessing --no-deps
```

安装 cuda，cudnn，cudnn-dev：
```
sudo apt install nvidia-cuda-dev nvidia-cuda-toolkit 
sudo apt-get install libcupti-dev
```

deepin 2019.5 安装了 cuda 9.1，不过 2019.6 默认又改回 8.0 了。从 debian 9 backports 中可以安装 cuda 9.1 。
libcupti-dev 版本号与 nvidia-cuda-toolkit 一致，但并非硬性依赖。libcupti-dev 也能在 debian 9 backports 中找到。

从 [2.5] 下载 libcudnn7 和 libcudnn7-dev，注意对应 nvidia-cuda-dev nvidia-cuda-toolkit 的版本。安装：

```
sudo dpkg -i libcudnn7_7.4.2.24-1+cuda9.1_amd64.deb
sudo dpkg -i libcudnn7-dev_7.4.2.24-1+cuda9.1_amd64.deb
```

不过他们与 cuda 的版本似乎也不是硬性规定，cuda 的版本允许更高。

```
git clone https://github.com/tensorflow/tensorflow.git
git fetch origin r1.7:r1.7
git checkout r1.7

# checkout
git fetch origin r1.12:r1.12
git checkout r1.12
```

安装 bazel：

方法（1）：
```
wget https://github.com/bazelbuild/bazel/releases/download/0.11.1/bazel_0.11.1-linux-x86_64.deb
wget https://github.com/bazelbuild/bazel/releases/download/0.15.2/bazel_0.15.2-linux-x86_64.deb
sudo dpkg -i bazel_0.11.1-linux-x86_64.deb
```
方法（2）：

```
sudo apt-get install openjdk-8-jdk

echo "deb [arch=amd64] http://storage.googleapis.com/bazel-apt stable jdk1.8" | sudo tee /etc/apt/sources.list.d/bazel.list
curl https://bazel.build/bazel-release.pub.gpg | sudo apt-key add -

sudo apt-get update && sudo apt-get install bazel
```
此方法安装的是最新版，

方法（2.1）：

从 http://storage.googleapis.com/bazel-apt 找出要下载的版本的 deb 的地址，如
```
wget http://storage.googleapis.com/bazel-apt/pool/jdk1.8/b/bazel/bazel_0.18.1_amd64.deb
```

因为 deepin 的 nvidia-cuda-dev 是安装到标准位置的（/usr/lib/x86_64-linux-gnu/），但 tensorflow 1.7 期待安装到 xxx/lib64，所以

如果 /usr/lib64 不存在，链接一下：

```
sudo ln -s /usr/lib/x86_64-linux-gnu/ /usr/lib64
```

但如果存在，看一下内容是什么：

```
$ ls -l /usr/lib64
总用量 0
lrwxrwxrwx 1 root root 32 6月   1 23:07 ld-linux-x86-64.so.2 -> /lib/x86_64-linux-gnu/ld-2.24.so

$ ls -l /lib/x86_64-linux-gnu/ld-*
-rwxr-xr-x 1 root root 153288 6月   1 23:06 /lib/x86_64-linux-gnu/ld-2.24.so
lrwxrwxrwx 1 root root     10 6月   1 23:06 /lib/x86_64-linux-gnu/ld-linux-x86-64.so.2 -> ld-2.24.so

```

如果兼容，则用 bind mount（不要删除 /usr/lib64 后做链接，因为删除 /usr/lib64/ld-linux-x86-64.so.2 会立即导致大部分程序不可用）。
命令是：

```
sudo mount -o bind /lib/x86_64-linux-gnu/     /usr/lib64
```

/etc/fstab 的写法是：

```
/lib/x86_64-linux-gnu     /usr/lib64      none    bind,noauto    0 0
```

然后运行 configure，回答一些问题：
```
./configure

Please specify the location of python. [Default is /usr/bin/python]: # 如果需要 python3 则填写 /usr/bin/python3
Please input the desired Python library path to use.  Default is [/usr/local/lib/python2.7/dist-packages]
Do you wish to build TensorFlow with jemalloc as malloc support? [Y/n]: Y
Do you wish to build TensorFlow with Google Cloud Platform support? [Y/n]: N
Do you wish to build TensorFlow with Hadoop File System support? [Y/n]: N
Do you wish to build TensorFlow with Amazon S3 File System support? [Y/n]: N
Do you wish to build TensorFlow with Apache Kafka Platform support? [y/N]: N
Do you wish to build TensorFlow with XLA JIT support? [y/N]: N
Do you wish to build TensorFlow with GDR support? [y/N]: N
Do you wish to build TensorFlow with VERBS support? [y/N]: N
Do you wish to build TensorFlow with OpenCL SYCL support? [y/N]: N
Do you wish to build TensorFlow with CUDA support? [y/N]: Y
Please specify the CUDA SDK version you want to use, e.g. 7.0. [Leave empty to default to CUDA 9.0]: 9.1
Please specify the location where CUDA 9.1 toolkit is installed. Refer to README.md for more details. [Default is /usr/local/cuda]: /usr
Please specify the cuDNN version you want to use. [Leave empty to default to cuDNN 7.0]: 7.4 # 根据 libcudnn7_xxx.deb 的版本，精确到点后一节即可（实际上只精确到大版本号）。
Please specify the location where cuDNN 7 library is installed. Refer to README.md for more details. [Default is /usr]: #默认
Do you wish to build TensorFlow with TensorRT support? [y/N]: N
Please specify a list of comma-separated Cuda compute capabilities you want to build with.
You can find the compute capability of your device at: https://developer.nvidia.com/cuda-gpus.
Please note that each additional compute capability significantly increases your build time and binary size. [Default is: 3.5,5.2]5.2 # 应该根据显卡的实际情况填写
Do you want to use clang as CUDA compiler? [y/N]:N
Please specify which gcc should be used by nvcc as the host compiler. [Default is /usr/bin/x86_64-linux-gnu-gcc-7]:/usr/bin/x86_64-linux-gnu-gcc-6
Do you wish to build TensorFlow with MPI support? [y/N]: 
Please specify optimization flags to use during compilation when bazel option "--config=opt" is specified [Default is -march=native]: 
Would you like to interactively configure ./WORKSPACE for Android builds? [y/N]:
```

编译 1.12 增加的选项:
```

Do you wish to build TensorFlow with Apache Ignite support? [Y/n]: N
No Apache Ignite support will be enabled for TensorFlow.


Do you wish to build TensorFlow with ROCm support? [y/N]: N
No ROCm support will be enabled for TensorFlow.

Please specify the NCCL version you want to use. If NCCL 2.2 is not installed, then you can use version 1.3 that can be fetched automatically but it may have worse performance with multiple GPUs. [Default is 2.2]:1.3 # 实际上可能没装 NCCL
```
注意 python 路径的选择也将决定编译出来的 tenorflow 适用于 python2 还是 python3。

注意：不同的 cuda 要求不同的 gcc 版本，此处的检测不一定对。cuda8 对应 /usr/bin/x86_64-linux-gnu-gcc-5 或者 /usr/bin/x86_64-linux-gnu-gcc-4.9，cuda9.1 对应 /usr/bin/x86_64-linux-gnu-gcc-6 。

注意 gcc 5.5 有 bug，不能用，gcc 4.9-5.4 是可以用于 cuda 8 的。

注意 Cuda compute capabilities 要根据显卡的实际情况填写，如果填写的比显卡实际能力高，则无法运行（回落到CPU模式）。例如 940MX 是 5.0，GTX950 是 5.2 。
显卡的 cuda 能力可以在 [3.10] 查看；tf运行时，如果因为GPU能力回落到CPU模式也会输出警告，例如：

```
2019-06-06 09:22:32.324376: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1394] Ignoring visible gpu device (device: 0, name: GeForce 940MX, pci bus id: 0000:01:00.0, compute capability: 5.0) with Cuda compute capability 5.0. The minimum required Cuda capability is 5.2.
```

编译：

```
bazel build --config=opt --config=cuda --incompatible_load_argument_is_label=false //tensorflow/tools/pip_package:build_pip_package
```

出错：

```
ERROR: Unrecognized option: --incompatible_load_argument_is_label=false
```
去掉 --incompatible_load_argument_is_label=false 选项试试。

bazel build --config=opt --config=cuda //tensorflow/tools/pip_package:build_pip_package

出错：

```
ERROR: Config value cuda is not defined in any .rc file
```
这是 bazel 0.19+ 的问题。可以改为安装 bazel 0.18.1。

出错：
```
ERROR: /home/duanyao/project/tensorflow.git/WORKSPACE:3:1: name 'http_archive' is not defined
```
还是 bazel 版本太新，改为安装 bazel 0.18.1[3.5]。

出错：

```
File "/home/duanyao/project/tensorflow.git/third_party/gpus/cuda_configure.bzl", line 607, in _find_cupti_header_dir
...
Cuda Configuration Error: Cannot find cupti.h under /usr
```

根据 https://github.com/tensorflow/tensorflow/pull/19224/commits/5c027c6a94e629847da33adc7731a686ad01275c 修改

third_party/gpus/cuda_configure.bzl:607

```
- auto_configure_fail("Cannot find cupti.h under %s" % cuda_toolkit_path)
+ auto_configure_fail("Cannot find cupti.h under %s" % ", ".join([cuda_toolkit_path + "/" + s for s in CUPTI_HEADER_PATHS]))
```

重新运行 bazel，错误信息变为：

```
Cuda Configuration Error: Cannot find cupti.h under /usr/extras/CUPTI/include/, /usr/include/cuda/CUPTI/
```

查找实际的 cupti.h 位置：

```
dpkg -S cupti.h
libcupti-dev:amd64: /usr/include/cupti.h

ls /usr/include/cuda/
ls: 无法访问'/usr/include/cuda/': 没有那个文件或目录
ls /usr/extras/CUPTI
ls: 无法访问'/usr/extras/CUPTI': 没有那个文件或目录
```

做一下链接：

```
sudo mkdir -p /usr/extras/CUPTI
sudo ln -s /usr/include /usr/extras/CUPTI/include
```

重新运行 bazel，又一个错误：

```
File "/home/duanyao/project/tensorflow.git/third_party/gpus/cuda_configure.bzl", line 724, in _find_nvvm_libdevice_dir
...
Cuda Configuration Error: Cannot find libdevice.10.bc under /usr
```

修改 third_party/gpus/cuda_configure.bzl:724

```
  auto_configure_fail("Cannot find libdevice.10.bc under %s" % cuda_toolkit_path)
  auto_configure_fail("Cannot find libdevice.10.bc under %s" % ", ".join([cuda_toolkit_path + "/" + s for s in NVVM_LIBDEVICE_PATHS]))
```
重新运行 bazel，错误信息变为：
```
Cuda Configuration Error: Cannot find libdevice.10.bc under /usr/nvvm/libdevice/, /usr/share/cuda/
```

查找 libdevice.10.bc 的实际位置：

```
dpkg -S libdevice.10.bc

nvidia-cuda-toolkit: /usr/lib/nvidia-cuda-toolkit/libdevice/libdevice.10.bc

ls /usr/nvvm/
ls: 无法访问'/usr/nvvm/': 没有那个文件或目录
```

做一下链接：

```
sudo mkdir -p /usr/nvvm/
sudo ln -s /usr/lib/nvidia-cuda-toolkit/libdevice /usr/nvvm/libdevice
```

重新运行 bazel 。

cuda8可能错误依旧，libdevice.10.bc 不存在：

```
$ dpkg -S libdevice.10.bc
dpkg-query: 没有找到与 *libdevice.10.bc* 相匹配的路径
duanyao@duanyao-laptop-c:~/project/tensorflow.git$ dpkg -L nvidia-cuda-toolkit | grep libdevice
/usr/lib/nvidia-cuda-toolkit/libdevice
/usr/lib/nvidia-cuda-toolkit/libdevice/libdevice.compute_20.10.bc
/usr/lib/nvidia-cuda-toolkit/libdevice/libdevice.compute_30.10.bc
/usr/lib/nvidia-cuda-toolkit/libdevice/libdevice.compute_35.10.bc
/usr/lib/nvidia-cuda-toolkit/libdevice/libdevice.compute_50.10.bc

```

解决办法是做个链接[3.6]：

```
cd /usr/lib/nvidia-cuda-toolkit/libdevice
sudo ln -s libdevice.compute_50.10.bc libdevice.10.bc
```

可能会遇到类似这样的错误：

```
ERROR: /home/duanyao/project/tensorflow.git/tensorflow/tools/pip_package/BUILD:114:1: no such package '@grpc//': java.io.IOException: thread interrupted and referenced by '//tensorflow/tools/pip_package:licenses'
```

这实际上是下载某些源码时的网络错误，重试几次可能就好了。

另一个错误：

```
bazel-out/k8-opt/genfiles/external/local_config_cuda/cuda/cuda/include/crt/host_config.h:121:2: error: #error -- unsupported GNU version! gcc versions later than 6 are not supported!
 #error -- unsupported GNU version! gcc versions later than 6 are not supported!
```

参照 https://askubuntu.com/questions/26498/how-to-choose-the-default-gcc-and-g-version 安装 gcc-6 和 g++-6 。
注意，cuda9.1 要求 gcc-6 及以下，cuda8 则要求 gcc-5 及以下（但是 deepin 的源里没有gcc-5）。

```
ERROR: /home/duanyao/.cache/bazel/_bazel_duanyao/03c61e4a80ec02cfd05c2483b9c25101/external/highwayhash/BUILD:22:1: undeclared inclusion(s) in rule '@highwayhash//:arch_specific':
this rule is missing dependency declarations for the following files included by 'external/highwayhash/highwayhash/arch_specific.cc':
  '/usr/lib/gcc/x86_64-linux-gnu/6/include/stdint.h'
  '/usr/lib/gcc/x86_64-linux-gnu/6/include/stddef.h'
  '/usr/lib/gcc/x86_64-linux-gnu/6/include/cpuid.h'
Target //tensorflow/tools/pip_package:build_pip_package failed to build
INFO: Elapsed time: 0.925s, Critical Path: 0.12s
FAILED: Build did NOT complete successfully
```
这是因为，切换gcc版本后，需要清除一下缓存：
```
rm -Rf /home/duanyao/.cache/bazel
bazel clean
```

另一个错误：

```
ERROR: /home/duanyao/project/tensorflow.git/tensorflow/contrib/nccl/BUILD:23:1: error while parsing .d file: /home/duanyao/.cache/bazel/_bazel_duanyao/03c61e4a80ec02cfd05c2483b9c25101/execroot/org_tensorflow/bazel-out/k8-opt/bin/tensorflow/contrib/nccl/_objs/python/ops/_nccl_ops_gpu/tensorflow/contrib/nccl/kernels/nccl_manager.pic.d (No such file or directory)
In file included from ./tensorflow/core/framework/variant.h:26:0,
                 from ./tensorflow/core/framework/allocator.h:26,
                 from ./tensorflow/core/framework/tensor.h:20,
                 from ./tensorflow/core/framework/log_memory.h:19,
                 from ./tensorflow/core/common_runtime/gpu/gpu_event_mgr.h:21,
                 from ./tensorflow/contrib/nccl/kernels/nccl_manager.h:24,
                 from tensorflow/contrib/nccl/kernels/nccl_manager.cc:15:
bazel-out/k8-opt/genfiles/tensorflow/core/framework/tensor.pb.h:12:2: error: #error This file was generated by a newer version of protoc which is
 #error This file was generated by a newer version of protoc which is
  ^~~~~
bazel-out/k8-opt/genfiles/tensorflow/core/framework/tensor.pb.h:13:2: error: #error incompatible with your Protocol Buffer headers. Please update
 #error incompatible with your Protocol Buffer headers.  Please update
  ^~~~~
bazel-out/k8-opt/genfiles/tensorflow/core/framework/tensor.pb.h:14:2: error: #error your headers.
 #error your headers.
  ^~~~~
```

查看文件 bazel-out/k8-opt/genfiles/tensorflow/core/framework/tensor.pb.h:1

```
#if GOOGLE_PROTOBUF_VERSION < 3005000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3005000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

```
查看系统中安装的 protobuf 头文件版本：

```
dpkg -S /usr/include/google/protobuf

libprotobuf-dev:amd64: /usr/include/google/protobuf

dpkg -l libprotobuf-dev

ii  libprotobuf-dev:amd64              3.0.0-9.1              amd64                  protocol buffers C++ library (development files)

```
根据[3.3]，这是 tensorflow 与系统中安装的 protobuf 版本有冲突（tensorflow本该用自带的protobuf源码的）。暂时屏蔽系统中安装的 protobuf：

```
sudo mv /usr/include/google/protobuf /usr/include/google/protobuf.tmp
```

继续编译。

另一个错误：

```
ERROR: /home/duanyao/.cache/bazel/_bazel_duanyao/03c61e4a80ec02cfd05c2483b9c25101/external/local_config_cuda/cuda/BUILD:197:1: declared output 'external/local_config_cuda/cuda/cuda/include/google/protobuf/any.h' is a dangling symbolic link
```

这是因为屏蔽系统中安装的 protobuf 后没有清除缓存。重新执行

```
rm -Rf ~/.cache/bazel
bazel clean
```

错误（仅cuda8）：

```
INFO: From Compiling tensorflow/contrib/framework/kernels/zero_initializer_op_gpu.cu.cc:
/usr/lib/gcc/x86_64-linux-gnu/5/include/avx512fintrin.h(9220): error: argument of type "const void *" is incompatible with parameter of type "const float *"
```
这是因为 gcc 5 的 bug。改装 4.9 即可。

错误（仅cuda8）：

```
ERROR: /home/duanyao/project/tensorflow.git/tensorflow/contrib/lite/toco/python/BUILD:22:1: Linking of rule '//tensorflow/contrib/lite/toco/python:_tensorflow_wrap_toco.so' failed (Exit 1): crosstool_wrapper_driver_is_not_gcc failed: error executing command 
  (cd /home/duanyao/.cache/bazel/_bazel_duanyao/03c61e4a80ec02cfd05c2483b9c25101/execroot/org_tensorflow && \
  exec env - \
    CUDA_TOOLKIT_PATH=/usr \
    CUDNN_INSTALL_PATH=/usr \
    GCC_HOST_COMPILER_PATH=/usr/bin/x86_64-linux-gnu-gcc-4.9 \
    PATH=/home/duanyao/.nvm/versions/node/v12.3.1/bin:/home/duanyao/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/sbin:/usr/sbin \
    PWD=/proc/self/cwd \
    PYTHON_BIN_PATH=/usr/bin/python3 \
    PYTHON_LIB_PATH=/usr/local/lib/python3.5/dist-packages \
    TF_CUDA_CLANG=0 \
    TF_CUDA_COMPUTE_CAPABILITIES=5.2 \
    TF_CUDA_VERSION=8.0 \
    TF_CUDNN_VERSION=7 \
    TF_NEED_CUDA=1 \
    TF_NEED_OPENCL_SYCL=0 \
  external/local_config_cuda/crosstool/clang/bin/crosstool_wrapper_driver_is_not_gcc -shared -o bazel-out/k8-py3-opt/bin/tensorflow/contrib/lite/toco/python/_tensorflow_wrap_toco.so '-Wl,-rpath,$ORIGIN/../../../../../_solib_local/_U_S_Stensorflow_Scontrib_Slite_Stoco_Spython_C_Utensorflow_Uwrap_Utoco.so___Utensorflow' '-Wl,-rpath,$ORIGIN/../../../../../_solib_local/_U@local_Uconfig_Ucuda_S_Scuda_Ccudart___Uexternal_Slocal_Uconfig_Ucuda_Scuda_Scuda_Slib' -Lbazel-out/k8-py3-opt/bin/_solib_local/_U_S_Stensorflow_Scontrib_Slite_Stoco_Spython_C_Utensorflow_Uwrap_Utoco.so___Utensorflow -Lbazel-out/k8-py3-opt/bin/_solib_local/_U@local_Uconfig_Ucuda_S_Scuda_Ccudart___Uexternal_Slocal_Uconfig_Ucuda_Scuda_Scuda_Slib -Wl,--version-script tensorflow_wrap_toco_versionscript.lds '-Wl,-rpath,$ORIGIN/,-rpath,$ORIGIN/..,-rpath,$ORIGIN/../..,-rpath,$ORIGIN/../../..,-rpath,$ORIGIN/../../../..' -Wl,-soname,_tensorflow_wrap_toco.so -pthread -Wl,-rpath,../local_config_cuda/cuda/lib64 -Wl,-rpath,../local_config_cuda/cuda/extras/CUPTI/lib64 -Wl,-no-as-needed -B/usr/bin/ -Wl,-z,relro,-z,now -no-canonical-prefixes -pass-exit-codes '-Wl,--build-id=md5' '-Wl,--hash-style=gnu' -Wl,--gc-sections -Wl,@bazel-out/k8-py3-opt/bin/tensorflow/contrib/lite/toco/python/_tensorflow_wrap_toco.so-2.params)
x86_64-linux-gnu-gcc-4.9: error: tensorflow_wrap_toco_versionscript.lds: No such file or directory
Target //tensorflow/tools/pip_package:build_pip_package failed to build
INFO: Elapsed time: 2183.113s, Critical Path: 73.99s
INFO: 3288 processes: 3288 local.
FAILED: Build did NOT complete successfully
```
这是 bazel 与 tf 的兼容性问题，采用 bazel 0.16.1 。

错误（仅cuda8+tf1.8）：
```
tensorflow/core/kernels/sparse_tensor_dense_matmul_op_gpu.cu.cc(44): error: calling a __host__ function("__builtin_expect") from a __global__ function("tensorflow::SparseTensorDenseMatMulKernel<float, long long, (bool)1, (bool)1> ") is not allowed
```
这是 tf 1.8 + cuda8 的 bug，已经修复，可以手动修复：
tensorflow/core/platform/macros.h:82
```
#if TF_HAS_BUILTIN(__builtin_expect) || (defined(__GNUC__) && __GNUC__ >= 3)
```
改为：
```
//
// We need to disable this for GPU builds, though, since nvcc8 and older
// don't recognize `__builtin_expect` as a builtin, and fail compilation.
#if (!defined(__NVCC__)) && \
    (TF_HAS_BUILTIN(__builtin_expect) || (defined(__GNUC__) && __GNUC__ >= 3))
```

### tf 1.12 与全局 libicu-dev 的冲突问题
```
ERROR: /home/duanyao/project/tensorflow.git/tensorflow/BUILD:533:1: Executing genrule //tensorflow:tf_python_api_gen_v1 failed (Exit 1)
Traceback (most recent call last):
...
ImportError: /home/duanyao/.cache/bazel/_bazel_duanyao/03c61e4a80ec02cfd05c2483b9c25101/execroot/org_tensorflow/bazel-out/host/bin/tensorflow/create_tensorflow.python_api_1.runfiles/org_tensorflow/tensorflow/python/_pywrap_tensorflow_internal.so: undefined symbol: _ZTVN6icu_579ErrorCodeE
```

deepin 15.11 系统的 libicu-dev 版本是 57.1 或 60.2 但 tf 1.12 自带了 62-1（ third_party/icu/workspace.bzl ）。由于未知的bug，tf 1.12 编译时找到了系统的 icu ，但链接时找到了自带的，所以不兼容了 [3.12]。

解决办法是暂时卸载 libicu-dev ：`sudo dpkg -r --force-depends libicu-dev` ，等编译完 tf 后再重新安装它。

卸载 libicu-dev 之后，要运行 `bazel clean`, 并且删除 `~/.cache/bazel/_bazel_<用户名>/xxxx` 目录（xxxx是16进制数字，例如 `~/.cache/bazel/_bazel_duanyao/03c61e4a80ec02cfd05c2483b9c25101` ）。
删除整个 `~/.cache/bazel` 也可以，但下次编译较慢。或者运行 `bazel clean --expunge` 。
 tf 的编译要全部重新从 configure 开始，否则还是会出错。

~/.cache/bazel/_bazel_duanyao/03c61e4a80ec02cfd05c2483b9c25101
~/.cache/bazel/_bazel_duanyao/d41d8cd98f00b204e9800998ecf8427e

### 编译可能突然中断，这可能是因为内存不足。

可以加上 `--jobs=n` 参数，逐步调低并监视内存占用。长时间运行后，bazel 的后台程序内存占用会比较大，可以关闭一下：

```
bazel shutdown
```

### 生成 whl 包
编译完成后，运行以下命令生成 whl 包：

```
bazel-bin/tensorflow/tools/pip_package/build_pip_package tensorflow_pkg

ls tensorflow_pkg

tensorflow-1.7.1-cp27-cp27mu-linux_x86_64.whl
```

安装 whl：

```
cd tensorflow_pkg

#for python 2:

pip2 install tensorflow*.whl

#for python 3:

pip3 install tensorflow*.whl
```
要全局安装则加上 sudo。
验证是否可以运行：

```
python

import tensorflow as tf
hello = tf.constant('Hello, TensorFlow!')
sess = tf.Session()
print(sess.run(hello))

# If the system outputs the following, then you are ready to begin writing TensorFlow programs:

    Hello, TensorFlow!
```
注意不要在 tensorflow 的源码目录下执行 python tensorflow，否则会报错：

```
>>> import tensorflow as tf

Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "tensorflow/__init__.py", line 24, in <module>
    from tensorflow.python import *  # pylint: disable=redefined-builtin
  File "tensorflow/python/__init__.py", line 49, in <module>
    from tensorflow.python import pywrap_tensorflow
  File "tensorflow/python/pywrap_tensorflow.py", line 25, in <module>
    from tensorflow.python.platform import self_check
ImportError: No module named platform
```
