## 参考资料

[1.5] 对比学习（Contrastive Learning）综述
https://zhuanlan.zhihu.com/p/346686467

[2.1] Big Self-Supervised Models are Strong Semi-Supervised Learners (SimCLR V2)
https://arxiv.org/abs/2006.10029

[3.1] SimCLR - A Simple Framework for Contrastive Learning of Visual Representations
https://github.com/google-research/simclr

