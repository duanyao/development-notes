## 参考资料

[2.1] PaddleDetection模型导出教程
https://github.com/PaddlePaddle/PaddleDetection/blob/release/2.2/deploy/EXPORT_MODEL.md#

[2.2] Convert_Model_From_Paddle
https://github.com/openvinotoolkit/openvino/blob/master/docs/MO_DG/prepare_model/convert_model/Convert_Model_From_Paddle.md

[2.3] OpenVINO部署PaddleDetection模型
https://github.com/PaddlePaddle/Paddle2ONNX/blob/release/0.9/experimental/openvino_ppdet_cn.md

## 安装 model_optimizer

pip 安装：
```
pip install openvino-dev
```
当前版本（2022.1）为 openvino_dev-2021.4.2-3976 。

pip openvino-dev 中含有程序 mo ，即是 model_optimizer 。

上述安装方式下，model_optimizer 仅支持 onnx 和 tensorflow2 ，等效于：`pip install openvino-dev[onnx,tensorflow2]` 。

其他模型格式为 'tf', 'caffe', 'mxnet', 'kaldi', 'onnx' 等。

从源码安装：

```
cd openvino/tools/mo/
pip install -e .
```
或者：

```
cd openvino/tools/
pip install mo/
```
安装了：
```
<venv>/bin/mo
<venv>/lib/python3.9/site-packages/openvino/tools/mo/*
<venv>/lib/python3.9/site-packages/openvino_mo-0.0.0.dist-info/*
```

其中与 paddle 有关的：

/home/duanyao/opt/venv/v39/lib/python3.9/site-packages/openvino/tools/mo/mo_paddle.py
/home/duanyao/opt/venv/v39/lib/python3.9/site-packages/openvino/tools/mo/main_paddle.py

## 转换 paddle detection 模型

在 PaddleDetection 目录下，导出模型：

```
python tools/export_model.py -c configs/faster_rcnn/faster_rcnn_r101_vd_fpn_2x_custom_head_coco_05.yml --output_dir=./inference_model -o weights=output/faster_rcnn_r101_vd_fpn_2x_custom_head_coco_05/model_final
```
实际输入文件是 output/faster_rcnn_r101_vd_fpn_2x_custom_head_coco_05/model_final.pdparams
导出到 ./inference_model/faster_rcnn_r101_vd_fpn_2x_custom_head_coco_05 ，有以下文件：

infer_cfg.yml
model.pdiparams
model.pdiparams.info
model.pdmodel

/opt/intel/openvino_2021.4.582/deployment_tools/model_optimizer/mo.py --model_name yolov3_darknet53_270e_coco --output_dir <OUTPUT_MODEL_DIR> --framework=paddle --data_type=FP32 --reverse_input_channels --input_shape=[1,3,608,608],[1,2],[1,2] --input=image,im_shape,scale_factor --output=save_infer_model/scale_0.tmp_1,save_infer_model/scale_1.tmp_1 --input_model=yolov3.pdmodel

mo --model_name faster_rcnn_r101_vd_fpn_2x_custom_head_coco_05 --output_dir ./openvino_model --framework=paddle --data_type=FP32 --reverse_input_channels --input_shape=[1,3,800,1333],[1,2],[1,2] --input=image,im_shape,scale_factor --output=save_infer_model/scale_0.tmp_1,save_infer_model/scale_1.tmp_1 --input_model=./inference_model/faster_rcnn_r101_vd_fpn_2x_custom_head_coco_05/model.pdmodel

/home/duanyao/project/openvino.git/tools/mo/openvino/tools/mo/mo.py --model_name faster_rcnn_r101_vd_fpn_2x_custom_head_coco_05 --output_dir ./openvino_model --framework=paddle --data_type=FP32 --reverse_input_channels --input_shape=[1,3,800,1333],[1,2],[1,2] --input=image,im_shape,scale_factor --output=save_infer_model/scale_0.tmp_1,save_infer_model/scale_1.tmp_1 --input_model=./inference_model/faster_rcnn_r101_vd_fpn_2x_custom_head_coco_05/model.pdmodel

## paddle -> onnx -> openvino

## 转换 tensorflow faster_rcnn 模型

参考 openvino_model_zoo 的 converter.py faster_rcnn_resnet50_coco ：

```
$ python tools/model_tools/converter.py --name faster_rcnn_resnet50_coco

========== Converting faster_rcnn_resnet50_coco to IR (FP16)
Conversion command: /home/duanyao/opt/venv/v39/bin/python -- /home/duanyao/opt/venv/v39/bin/mo --framework=tf --data_type=FP16 --output_dir=/home/duanyao/project/openvino_model_zoo.git/public/faster_rcnn_resnet50_coco/FP16 --model_name=faster_rcnn_resnet50_coco --reverse_input_channels '--input_shape=[1,600,1024,3]' --input=image_tensor --transformations_config=/home/duanyao/opt/venv/v39/lib/python3.9/site-packages/mo/extensions/front/tf/faster_rcnn_support.json --tensorflow_object_detection_api_pipeline_config=/home/duanyao/project/openvino_model_zoo.git/public/faster_rcnn_resnet50_coco/faster_rcnn_resnet50_coco_2018_01_28/pipeline.config --output=detection_scores,detection_boxes,num_detections --input_model=/home/duanyao/project/openvino_model_zoo.git/public/faster_rcnn_resnet50_coco/faster_rcnn_resnet50_coco_2018_01_28/frozen_inference_graph.pb

Model Optimizer arguments:
Common parameters:
        - Path to the Input Model:      /home/duanyao/project/openvino_model_zoo.git/public/faster_rcnn_resnet50_coco/faster_rcnn_resnet50_coco_2018_01_28/frozen_inference_graph.pb
        - Path for generated IR:        /home/duanyao/project/openvino_model_zoo.git/public/faster_rcnn_resnet50_coco/FP16
        - IR output name:       faster_rcnn_resnet50_coco
        - Log level:    ERROR
        - Batch:        Not specified, inherited from the model
        - Input layers:         image_tensor
        - Output layers:        detection_scores,detection_boxes,num_detections
        - Input shapes:         [1,600,1024,3]
        - Mean values:  Not specified
        - Scale values:         Not specified
        - Scale factor:         Not specified
        - Precision of IR:      FP16
        - Enable fusing:        True
        - Enable grouped convolutions fusing:   True
        - Move mean values to preprocess section:       None
        - Reverse input channels:       True
TensorFlow specific parameters:
        - Input model in text protobuf format:  False
        - Path to model dump for TensorBoard:   None
        - List of shared libraries with TensorFlow custom layers implementation:        None
        - Update the configuration file with input/output node names:   None
        - Use configuration file used to generate the model with Object Detection API:  /home/duanyao/project/openvino_model_zoo.git/public/faster_rcnn_resnet50_coco/faster_rcnn_resnet50_coco_2018_01_28/pipeline.config
        - Use the config file:  None
        - Inference Engine found in:    /home/duanyao/opt/venv/v39/lib/python3.9/site-packages/openvino
Inference Engine version:       2021.4.2-3976-0943ed67223-refs/pull/539/head
Model Optimizer version:        2021.4.2-3976-0943ed67223-refs/pull/539/head
2022-01-12 13:57:26.398318: I tensorflow/stream_executor/platform/default/dso_loader.cc:53] Successfully opened dynamic library libcudart.so.11.0
/home/duanyao/opt/venv/v39/lib/python3.9/site-packages/tensorflow/python/autograph/impl/api.py:22: DeprecationWarning: the imp module is deprecated in favour of importlib; see the module's documentation for alternative uses
  import imp
```

## 转换 yolo-v3-onnx 模型

```
python tools/model_tools/downloader.py --name yolo-v3-onnx
```
