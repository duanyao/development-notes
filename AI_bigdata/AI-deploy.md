## 参考资料
[2.1] CLIP Output differs on same input
https://github.com/openai/CLIP/issues/13

[2.2] Embedding is not deterministic
https://github.com/openai/CLIP/issues/114

[3.1] service-streamer
https://github.com/ShannonAI/service-streamer/wiki/Vision-Recognition-Service-with-Flask-and-service-streamer

## 精度问题
### 批量依赖性
在很多张量计算库中，计算结果都是依赖于批量大小的。同样的输入以不同的批量进行计算，输出结果可能会有细微的差别[2.1, 2.2]。这是因为计算库对不同的数据规模可能会有不同的优化。
这在各种计算设备（CPU、GPU）上都可能发生。
不过，在批量不变、计算库版本不变的情况下，计算结果还是一致的。

## 自动打包拆包
service-streamer [3.1] 可以将多个批量较小的预测请求组合成一个批量较大的，从而提高预测效率。
