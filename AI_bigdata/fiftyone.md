## 参考资料

[1.2] FiftyOne Installation
https://voxel51.com/docs/fiftyone/getting_started/install.html

[1.3] Using FiftyOne Datasets
https://voxel51.com/docs/fiftyone/user_guide/using_datasets.html

[1.4] Dataset Views
https://voxel51.com/docs/fiftyone/user_guide/using_views.html

[1.5] evaluation
https://voxel51.com/docs/fiftyone/user_guide/evaluation.html

[1.6] fiftyone brain
https://voxel51.com/docs/fiftyone/user_guide/brain.html

[1.7] Confusion matrices
https://voxel51.com/docs/fiftyone/user_guide/plots.html#confusion-matrix-plots

[2.1] FiftyOne Command-Line Interface (CLI)
https://voxel51.com/docs/fiftyone/cli/index.html?highlight=command%20line

[3.1] FiftyOne Teams
https://voxel51.com/fiftyone-teams/

[4.1] fiftyone-examples
https://github.com/voxel51/fiftyone-examples

[5.1] [BUG] Restart in debug mode in vscode raised an error 100 in Ubuntu
https://github.com/voxel51/fiftyone/issues/1334

[5.2] [BUG] DatasetView.save() after Dataset.sort_by() has no effect after v0.14.4
https://github.com/voxel51/fiftyone/issues/2666

[6.1] FiftyOne Recipes
https://voxel51.com/docs/fiftyone/recipes/index.html

[7.1] mongodb-database-tools installation
  https://www.mongodb.com/docs/database-tools/installation/installation/
  https://www.mongodb.com/try/download/database-tools

[7.2] Restore a MongoDB database(mongorestore)
  https://coderwall.com/p/3hx06a/restore-a-mongodb-database-mongorestore

[8.1] MongoDB Community Server Download
  https://www.mongodb.com/try/download/community
  https://repo.mongodb.org/apt/debian/dists/buster/mongodb-org/6.0/main/binary-amd64/mongodb-org-server_6.0.4_amd64.deb
  
[8.2] Configuring a MongoDB connection
  https://docs.voxel51.com/user_guide/config.html#configuring-mongodb-connection

[9.1] Slow Video Playback
  https://github.com/voxel51/fiftyone/issues/1365

[10.1] [FR] Add support for keypoint skeleton annotation in CVAT #2194 
  https://github.com/voxel51/fiftyone/issues/2194

[10.2] Creating Pose Skeletons from Scratch – FiftyOne Tips and Tricks – September 15, 2023
  https://voxel51.com/blog/creating-pose-skeletons-from-scratch-fiftyone-tips-and-tricks-sep-15-2023/

## fiftyone 总体介绍

### fiftyone 的组成部分

fiftyone 不只是一个单一的工具，而是多层结构的工具集。

* 数据集的数据库。用来存储、查询、修改数据集。外部的数据集首先要被导入这个数据库，才能通过 fiftyone 使用。数据库是一个 mongodb 。用户不需要直接操作此数据库，而是与下面几个部分打交道。
* 数据集操作 API。即一组 python API，在数据库基础上实现存储、查询、修改数据集的功能。用户可以在交互式 python 环境中使用此 API，或编写批处理脚本。
* web 应用程序，被称为 fiftyone app。在数据集操作 API 的基础上提供一个基于浏览器的 GUI 界面，用来展示、查询数据集。但目前用户无法用此 GUI 界面完成全部工作，还是经常需要搭配数据集操作 API。fiftyone app 虽然是一个 web 应用，但更多地在单机环境下使用。fiftyone app 可以从本地交互式 python shell、Jupyter notebook 等环境中启动并本地使用，也可以远程访问。fiftyone 还提供了一个专门的桌面客户端（FiftyOne Desktop App），可以代替浏览器来呈现 fiftyone app 的 GUI。
* 命令行界面[2.1]。可以完成部分数据集操作、fiftyone app 的启动。
* fiftyone brain，一个数据集分析工具，比如利用AI模型分析样本的相似度。
* fiftyone for teams[3.1]，一个数据集团队协作、数据集资产管理的工具。不能公开获得，需要咨询 fiftyone 的开发公司。

### 安装

```
pip install fiftyone # 安装 fiftyone 数据集操作 API、数据库、web 应用程序。
pip install fiftyone[desktop] # 安装 FiftyOne Desktop App 桌面客户端（约80MiB，也支持 linux 环境）。
```

### libssl1.1 的问题
libssl 是 openssl 的一部分。openssl 的版本号正在从 1.1.x 升级到 3.x ，这造成了一些兼容性问题。当前（2023.8） fiftyone 依赖 libssl1.1，但 ubuntu22.04 以后仅提供 libssl3，所以默认安装的 fiftyone 无法在 ubuntu22.04+ 上运行。
解决办法：在 ubuntu22.04+ 上平行安装 libssl1.1，具体来说是安装 ubuntu20.04 仓库里的 libssl1.1：

```
wget https://mirrors.tuna.tsinghua.edu.cn/ubuntu/pool/main/o/openssl/libssl1.1_1.1.1f-1ubuntu2.19_amd64.deb
sudo apt install ./libssl1.1_1.1.1f-1ubuntu2.19_amd64.deb
```

要得知 libssl1.1 的下载链接，可以在一台 ubuntu20.04 的系统里查询：
```
apt download --print-uris libssl1.1
'https://mirrors.tuna.tsinghua.edu.cn/ubuntu/pool/main/o/openssl/libssl1.1_1.1.1f-1ubuntu2.19_amd64.deb' libssl1.1_1.1.1f-1ubuntu2.19_amd64.deb 1321244 SHA512:8c033d73b2ee6cb5e7a33096501189d953fc4881f4864fb24d127ac70d79208fe0ed99691c0e0927d5b8099b4347516e7b2a5949a76f2d6a52f9454bea71c6fb
```
### 降级
降级前要先降级数据库格式，然后安装旧版本：
```
# The version that you wish to downgrade to
VERSION=0.15.1

# Migrate the database
fiftyone migrate --all -v $VERSION

# Now install the older version of `fiftyone`
pip install fiftyone==$VERSION

# Optional: verify that your datasets were migrated
fiftyone migrate --info
```

注意，`fiftyone migrate --info` 是有副作用的，它会将数据库版本升级到当前fiftyone的版本，所以在 `fiftyone migrate -v` 降级后，不能立即再执行前者。

### fiftyone 的数据管理方式和进程

fiftyone 的数据库为 mongodb，存储使用中的数据集的信息，但数据集中的图片、视频除外。
此 mongodb 数据库由本机的一个 mongod 进程维护，这个进程在第一个应用服务器进程导入 fiftyone 包（`import fiftyone as fo`）时被启动。 后续启动的应用服务器进程则继续连接到
此 mongodb 数据库的数据目录在 ~/.fiftyone/var/lib/mongo 。

一般来说，一个操作系统用户的 fiftyone 应用服务器进程可以有多个，但数据库进程和数据库目录只有一个。
每个数据集要有一个独特的名字，重复创建同名数据集会报错。不指定时，创建时自动产生一个名字。

fiftyone 的数据集分为持久和不持久两类，后者会在 mongodb 数据库关闭时删除。当没有任应用服务器连接到 mongodb 数据库时， mongodb 数据库就会关闭。用户可以设置数据集的持久性。
不过，如果 mongodb 数据库是被强行关闭的（如杀死其 mongod 进程），则不持久数据集也不会被删除，但是会在下一次正常关闭时删除。
如果应用服务器是被异常终止的，则 mongodb 数据库可能不会关闭，mongod 进程不会退出。这时就需要手动终止（kill） mongod 进程，否则新的应用服务器将无法启动。

### fiftyone app 的使用流程

#### 编程式使用

1. 启动应用服务器（和数据库服务器）。
2. 导入数据集/创建数据集/加载数据库中的数据集，产生一个数据集对象。
3. 启动一个 web 应用，指定一个数据集对象，并开始在给定的端口服务。如果此时不指定，也可在GUI中打开数据库中已存在的数据集。
4. 打开浏览器，访问上述端口，打开GUI。查看数据集。或者用
5. 使用完毕，销毁 web 应用，停止应用服务器（和数据库服务器）。

一般来说，fiftyone 在同一时间只能启动一个应用服务器、一个数据库服务器、一个 web 应用。

```
# 导入 fiftyone，会导致启动和连接 fiftyone 的数据库，耗时数秒。
import fiftyone as fo

# 启动 web app，同时引导浏览器打开页面，耗时数秒。此方法不阻塞。
# 默认在 http://localhost:5151 服务。这个语句不会阻塞。web 页面一开始也可能显示无数据，要等一会儿。
session = fo.launch_app()

# 启动 web app，同时引导 FiftyOne Desktop App 打开页面。需要先安装了 fiftyone[desktop] 。
# session = fo.launch_app(desktop=True)

# 允许远程访问。0.16 之后需要 address = '0.0.0.0'，见 "非本机无法通过 http 连接 app，即使设置了 remote = True"。
# session = fo.launch_app(dataset, remote=True, port=5151, address = '0.0.0.0')

# 阻塞，避免 web app 立即退出。在交互式 python 环境下，并不需要这样做，web app 会一直运行。
# 如果已经没有任何客户端（包括浏览器页面）连接到此 web app，并持续 <wait> 秒（默认3秒，-1表示无限大），此方法会返回，解除阻塞。
session.wait(wait = 3)
```

#### web 应用的使用

```
import fiftyone as fo
import fiftyone.zoo as foz
from fiftyone import ViewField as F

dataset = foz.load_zoo_dataset("quickstart")

session = fo.launch_app(dataset)

# Convert to ground truth patches
gt_patches = dataset.to_patches("ground_truth")
print(gt_patches)

# View patches in the App
session.view = gt_patches # 将视图变更为 ground_truth 的 PatchesView 。这也可以通过 GUI 操作实现，通过“Patches”按钮。视图的变更是可逆的。
```

#### 命令行式使用
[2.1] 

```
fiftyone --help
fiftyone --all-help
fiftyone <command> --help

# Print your entire config
# 包括本地持久目录如 "database_dir": "/home/rd/.fiftyone/var/lib/mongo"
fiftyone config

# Print the location of your config on disk (if one exists)
fiftyone config --locate

# Print constants from fiftyone.constants
fiftyone constants

# Convert an image classification directory tree to TFRecords format
fiftyone convert \
    --input-dir /path/to/image-classification-directory-tree \
    --input-type fiftyone.types.ImageClassificationDirectoryTree \
    --output-dir /path/for/tf-image-classification-dataset \
    --output-type fiftyone.types.TFImageClassificationDataset
    
# List FiftyOne datasets.
fiftyone datasets list

# Print basic information about all datasets
fiftyone datasets info
fiftyone datasets info --sort-by created_at
fiftyone datasets info --sort-by name --reverse

# Print information about a specific dataset
fiftyone datasets info <name>

# Prints the first few samples in a dataset
fiftyone datasets head <name>

# Stream samples in a FiftyOne dataset to the terminal.
fiftyone datasets stream

# Export the dataset to disk in the specified format
fiftyone datasets export <name> \
    --export-dir <export-dir> --type <type> --label-field <label-field>

# Write annotated versions of the media in the dataset with the
# specified label field(s) overlaid to disk
fiftyone datasets draw <name> \
    --output-dir <output-dir> --label-fields <list>,<of>,<fields>

# Rename the dataset
fiftyone datasets rename <old-name> <new-name>

# Migrate to a specific revision
fiftyone migrate --all --version <VERSION>

# Populate all missing `metadata` sample fields
fiftyone utils compute-metadata <dataset-name>

# 启动 web 应用。-r 允许远程， -A 0.0.0.0 监听地址，-p 10151监听端口，dataset-name 打开的数据集名。参数均为可选。
fiftyone app launch -r -A 0.0.0.0 -p 10151 dataset-name 
```

#### 远程连接 fiftyone web 服务

除了直接通过 http 远程访问，也可以通过 ssh 隧道远程访问：
```
# On local machine
ssh -N -L 5151:127.0.0.1:XXXX [<username>@]<hostname>
```
也可以让 FiftyOne App 启动 ssh 隧道：

```
# On local machine
fiftyone app connect --destination [<username>@]<hostname>

fiftyone app connect --destination [<username>@]<hostname> --port XXXX --local-port YYYY
```

## 应用案例 fiftyone-examples
[4.1] https://github.com/voxel51/fiftyone-examples

## 网络和代理服务器

fiftyone 的大部分操作不需要访问广域网。少数操作需要访问广域网，如下载 fiftyone.zoo 中的数据集样例。
fiftyone 访问网络时，接受 http_proxy/https_proxy 环境变量，可使用代理服务器。

## 数据集对象

### 通用数据集操作

数据集新建、导入、加载、列出、删除：
```
# 在数据库中新建一个空数据集，名字是 'ds-01'
dataset = fo.Dataset('ds-01') # 类型 class 'fiftyone.core.dataset.Dataset'
dataset.name # 'ds-01'
# 数据集默认不持久，当前会话退出后，此数据集会被删除。
dataset.persistent # False

dataset.persistent = True

# 保存修改到数据库。注意，这只是保存数据库级别的信息的修改，如 Dataset.info() ；样本（Sample）级别的修改仍需要调用 Sample.save() 来保存。
dataset.save()

# 加载已存在于数据库中的数据集，名字是 "ds-02"
dataset = fo.load_dataset("ds-02")

# 导入一个文件系统上的 coco 格式的数据集到数据库中。
dataset = fo.Dataset.from_dir(
     dataset_type=fo.types.COCODetectionDataset, # 指定导入格式为 coco 目标检测
     data_path="/home/duanyao/project/annotation/a-coco/images", # 图像目录
     labels_path="/home/duanyao/project/annotation/a-coco/annotations/instances_default.json", # 标注文件
     label_field={"detections":"ground_truth"}, # 指定用来存储标注的字段的名字，此处为 "ground_truth"，可省略
     name="ds-03", # 创建的数据集的名字
)
# 将数据集设置为持久，from_dir() 产生的数据集是非持久的。
dataset.persistant=True

# 显示 dataset 的相关信息。
dataset

Name:        ds-03
Media type:  image
Num samples: 1710
Persistent:  False
Tags:        []
Sample fields:
    id:           fiftyone.core.fields.ObjectIdField
    filepath:     fiftyone.core.fields.StringField
    tags:         fiftyone.core.fields.ListField(fiftyone.core.fields.StringField)
    metadata:     fiftyone.core.fields.EmbeddedDocumentField(fiftyone.core.metadata.ImageMetadata)
    ground_truth: fiftyone.core.fields.EmbeddedDocumentField(fiftyone.core.labels.Detections)

# 列出数据集中定义的分类
print(dataset.default_classes)

['0', '明确接触', '可能接触', '不太可能接触']

# 导入一个图片目录，产生尚未标注的数据集
dataset = fo.Dataset.from_dir(
    dataset_dir="/home/duanyao/project/annotation/a-coco/images",
    dataset_type=fo.types.ImageDirectory,
    name="ds-04",
)


# 列出数据库中存在的数据集：
fo.list_datasets() # ['ds-01', 'ds-02', 'ds-03', 'ds-04']

# 删除数据集
dataset.delete()
```
数据集的名字（name 属性）是全局唯一的；导入和新建数据集时，名字都不能与已有的数据集重复。

`fo.Dataset.from_dir()` 的 `label_field` 参数非必须，可指定用来存储标注的字段的名字。默认的标注字段名在不同的 fiftyone 版本中是不同的，0.15.1 是 "ground_truth"，
0.20.1 是 "detections"。为了获得稳定的结果，建议总是指定字段名：`label_field={"detections":"ground_truth"}`，其中 "detections" 表示标注的类型是目标检测。
COCO 数据集还支持实例分割、关键点等标注，要给实例分割指定字段名，可以写 `label_field={"detections":"ground_truth", "segmentations", "ground_truth_seg", "keypoints": "ground_truth_kp"}`。

### 克隆数据集

```
new_dataset = dataset.clone() # 不指定新数据集的名字，将自动生成名字，如‘2022.10.11.16.10.40’。
new_dataset.persistent = True # clone 默认不持久。
new_dataset = dataset.clone('new_dataset_name', persistent = True) # 指定新数据集的名字为‘new_dataset_name’。

```

克隆的数据集的 persistent 属性被初始化为 False，如果要保存它，应设置 `new_dataset.persistent = True` 或者加上 `clone(persistent = True)` 参数 。

克隆的数据集也会保留原数据集的标注任务（`list_annotation_runs()` 可查看）。

### 数据集改名/重命名

```
dataset.name = 'new_name' # 无需调用 dataset.save() 即可生效。
```

### 编程式创建数据集

```
dataset = fo.Dataset()
dataset.add_sample(fo.Sample(filepath="/path/to/image.png"))
```

### 数据集的元数据

```
dataset.created_at # datetime.datetime(2022, 10, 11, 8, 10, 40, 828000)

fo.pprint(dataset.stats(include_media=True))
```

自定义元数据
```
dataset.info # 初始值为空字典 {}
dataset.info['comment']='脱敏的版本' # 加入自定义元数据
dataset.save() 保存自定义元数据
```

### 可视化数据集

```
import fiftyone as fo

dataset = fo.load_dataset("ds-01")

# 启动app并展示指定的数据集：
session = fo.launch_app(dataset)

# 启动app，之后再切换显示的数据集：
session = fo.launch_app()
session.dataset = dataset

# 切换到显示一个 DatasetView：随机抽取10个样本。这也可以在 GUI 中完成。
session.view = dataset.take(10)
```

目前（2024.8，版本 0.24），fiftyone 支持可视化图像分类、目标检测、关键点、实例分割、视频时域检测等。但部分类型存在一些局限性：
* 关键点：无法针对某种类型的关键点（如 nose、left_knee 等）来操作，只能对整个对象的多个关键点一同操作。

### 样本的操作

访问数据集的样本：
```
# 样本（sample）总数
len(dataset) 

# 获得第一个或最后一个样本
s0 = dataset.first() # class 'fiftyone.core.sample.Sample'
s0.id # '62f1d9907b8f908c96d431f2'
s0.filepath # '/path/to/image1.jpg' 
s0.ground_truth # 样本的标注信息。对于目标检测数据集类型是 fiftyone.core.labels.Detections,  s0.ground_truth.detections 的类型是 list[fiftyone.core.labels.Detection]。
s0.dataset # 所属的 Dataset 对象

# 样本中有哪些字段，可以用 get_field_schema() 获得：

dataset.get_field_schema()

OrderedDict([
('id', <fiftyone.core.fields.ObjectIdField object at 0x7f87b694f520>), 
('filepath', <fiftyone.core.fields.StringField object at 0x7f87b694f550>), 
('tags', <fiftyone.core.fields.ListField object at 0x7f87b694f580>), 
('metadata', <fiftyone.core.fields.EmbeddedDocumentField object at 0x7f8795f546d0>), 
('ground_truth', <fiftyone.core.fields.EmbeddedDocumentField object at 0x7f87737b77c0>)
])

# schema 信息也可以通过直接打印 dataset 获得：
print(dataset)


dataset[0] # raise ValueError， 并不允许通过下标访问。
dataset[i:].first() # 绕开不允许通过下标访问的限制，得到第 i 个样本。这里用到了视图，详见后面。
dataset[s0.id] # 可以通过 id 访问 Sample

# 可以迭代访问 dataset 中的 Sample
for sample in dataset:
    print(sample.filepath)

# 可以将 dataset 转为 list[Sample]。但这对于大型数据集很耗时：
list(dataset)

# 增加样本
dataset.add_samples( 
    [
        fo.Sample(filepath="/path/to/image1.jpg"), # fo.Sample() 新建一个 Sample 对象。
        fo.Sample(filepath="/path/to/image2.jpg"),
    ]
)

# 删除样本，根据 Sample.id 。delete_samples() 的参数还可以是 Sample.id 、Sample、DatasetView 等
dataset.delete_samples([sample_id2, sample_id3])

# 可以给 Sample 增加字段，这会导致所属的 Dataset 对象增加一个 integer_field 字段。
# 赋值后，Dataset 会立即增加一个 integer_field 字段，但直到调用 save()，才会将值保存到数据库中。保存后，才会在视图等结构中可见。
s0["integer_field"] = 51
s0.save()

# 从数据集删除一个字段
dataset.delete_sample_field("integer_field")

# 给 Sample 设置 tags。tags 可以用来分片或过滤数据集。初始状态为空列表。
s0.tags = ["test", "low_quality"]
s0.save()
# 查询数据集中样本具有的 tags，不重复。
s0.dataset.distinct("tags") # ['low_quality', 'test']
```

可以将一个数据集的样本加入（`.add_samples()`）另一个数据集，语义是复制，新样本的id将是新的。

可以批量修改/创建、读取样本的属性：

```
dataset.set_values('index', range(len(dataset))) # 批量设置。参数1：样本的属性名；参数2：各个样本的值，按样本的顺序存放，可以是列表或迭代器。属性如果不存在，会创建。
dataset.first().index # 0
dataset.last().index # len(dataset) - 1

dataset.values('index') # 批量读取。返回数组，[0, 1, ...]
```

在迭代中批量、自动保存：

```
# Save in batches of 10
for sample in dataset.iter_samples(progress=True, autosave=True, batch_size=10):
    sample.ground_truth.label = make_label()

# Save every 0.5 seconds
for sample in dataset.iter_samples(
    progress=True, autosave=True, batch_size=0.5
):
    sample.ground_truth.label = make_label()
```

查找样本在数据集/视图中的顺序号：

```
# 定义一个查找顺序号的函数，第一个参数是数据集/视图，第二个参数是sample的id或filepath：
def find_idx(v, id_or_path):
    for i,s in enumerate(v):
        if s.id == id_or_path or s.filepath == id_or_path:
            return i
    return -1

find_idx(dataset, dataset[9:].first().id) # 返回 9
find_idx(dataset, dataset[9:].first().filepath) # 返回 9
```

fiftyone 没有提供方便的查看样本的顺序号的方法，所以需要自行编写代码来实现。

### 样本的标注：Label 类型

样本的真值（ground_truth）或预测值（prediction）用 Label 类型的子类表示，有以下类型：

* Regression 表示一个标量数值（value: float 属性）
* Classification 表示分类（label: str 属性）
* Classifications 表示多分类（classifications: list[Classification] 属性）。
* Detections 表示一帧上的多个目标检测结果（detections: list[Detection]）。
* Detection 表示单个目标检测结果（label: str 属性， bounding_box: list[float] 属性（xywh，相对值））。
* Polylines 多边形
* Keypoints 关键点
* Segmentation 实例分割
* Heatmap
* TemporalDetections 表示视频中发生的多个事件，实际上是 TemporalDetection 的列表。
* TemporalDetection 表示视频中一段时间（或一组帧）发生的事件（label: str 属性，support: list[int] 属性）。

Label 的子类用来表示预测值时，一般具有置信度（confidence: float）属性，Classification 还有 logits 属性。

样本的真值和预测值习惯上用 ground_truth 和 prediction 属性来表示，例如：

```
sample = fo.Sample(filepath="/path/to/image.png")

sample["ground_truth"] = fo.Detections(
    detections=[fo.Detection(label="cat", bounding_box=[0.5, 0.5, 0.4, 0.3])]
)
sample["prediction"] = fo.Detections(
    detections=[
        fo.Detection(
            label="cat",
            bounding_box=[0.480, 0.513, 0.397, 0.288],
            confidence=0.96,
        ),
    ]
)
```

`fo.Detection.bounding_box` 属性的值是相对值。

但属性名字并不需要固定，例如：

```
sample = fo.Sample(filepath="/path/to/video.mp4")
sample["events"] = fo.TemporalDetections(
    detections=[
        fo.TemporalDetection(label="meeting", support=[10, 20]),
        fo.TemporalDetection(label="party", support=[30, 60]),
    ]
)
```

标注也有 tag。可以这样使用它：

```
# Create a view that contains only low confidence predictions
view = dataset.filter_labels("predictions", F("confidence") < 0.06)

# Add a tag to all predictions in the view
for sample in view:
    for detection in sample["predictions"].detections:
        detection.tags.append("low_confidence")

    sample.save()

print(dataset.count_label_tags())
# {'low_confidence': 447}
```

### 数据的导出

```
import fiftyone as fo
import fiftyone.zoo as foz
import fiftyone.utils.coco as fouc

dataset = foz.load_zoo_dataset("quickstart")
classes = dataset.distinct("predictions.detections.label")

# Export images and ground truth labels to disk
dataset.export(
    export_dir="/tmp/coco",
    dataset_type=fo.types.COCODetectionDataset,
    label_field="ground_truth",
    classes=classes,
)

# Export predictions
dataset.export(
    dataset_type=fo.types.COCODetectionDataset,
    labels_path="/tmp/coco/predictions.json",
    label_field="predictions",
    classes=classes,
)
```

导出的 coco 格式数据集结构如下

```
data/
  xxx.jpg   # 样本图像
labels.json # 标注数据
```

## 数据集的视图（DatasetView）

### 基本操作
简单的创建视图：

```
# 从数据集中随机选择10个样本，组成一个视图，类型为 fiftyone.core.view.DatasetView 。
view = dataset.take(10)

# 选取从第 50 开始的 10 个样本，组成一个视图。
view1 = dataset[50:60] # 等效于 dataset.skip(50).limit(10)

# 整个数据集的视图
view2 = dataset.view()
```

前面提到，不能用下标访问样本。但是，借助视图可以做到这一点：

```
dataset[i:].first() # 得到第 i 个样本。
```

DatasetView 的方法和属性与 Dataset 基本相同。视图的中获得的 Sample 的类型是 SampleView，但 SampleView 的属性类型就不再是视图，而是与原版无异：

```
type(view1.first()) # <class 'fiftyone.core.sample.SampleView'>
type(view1.first().ground_truth) # <class 'fiftyone.core.labels.Detections'>
```

复杂视图：
```
import fiftyone as fo
import fiftyone.zoo as foz
from fiftyone import ViewField as F

dataset = foz.load_zoo_dataset("quickstart")
dataset.compute_metadata() # metadata 主要包含图像的宽高、大小等。初始状态下，metadata可能是空的，所以需要重新计算 metadata。

# Create a view containing the 5 samples from the validation split whose
# images are >= 48 KB that have the most predictions with confidence > 0.9
complex_view = (
    dataset
    .match_tags("validation")
    .match(F("metadata.size_bytes") >= 48 * 1024)  # >= 48 KB
    .filter_labels("predictions", F("confidence") > 0.9)
    .sort_by(F("predictions.detections").length(), reverse=True)
    .limit(5)
)

# Check to see how many predictions there are in each matching sample
print(complex_view.values(F("predictions.detections").length()))
# [29, 20, 17, 15, 15]
```

视图之间可以连接，用重载的`+`或 `concat()` 方法：

```
view = view1 + view2
view = view1.concat(view2)
```
注意，连接操作不会去重，有可能造成拥有相同id的多个Sample共存，这可能造成后续的问题，例如 FiftyOne App 的显示错误。

视图的运算都是创建一个新的视图，数据源是不会受影响的。

### DatasetView 视图的原理

fiftyone 的视图 DatasetView 是动态的，本身不存储数据集的数据，而是存储产生视图的方法。创建了视图以后，如果其来源的数据集被修改了，则也会反映在视图中。
产生视图的方法是一系列的 ViewStage 组成的，每个 ViewStage 表示一种对数据集的操作，如跳过、截取、过滤、随机抽样等。用此方法列出数据集支持的 ViewStage 操作：

dataset.list_view_stages()

```
'exclude', 'exclude_by', 'exclude_fields', 'exclude_frames', 'exclude_labels', 'exists', 'filter_field', 'filter_labels', 'filter_classifications', 'filter_detections', 'filter_polylines', 'filter_keypoints', 'geo_near', 'geo_within', 'group_by', 'limit', 'limit_labels', 'map_labels', 'set_field', 'match', 'match_frames', 'match_labels', 'match_tags', 'mongo', 'select', 'select_by', 'select_fields', 'select_frames', 'select_labels', 'shuffle', 'skip', 'sort_by', 'sort_by_similarity', 'take', 'to_patches', 'to_evaluation_patches', 'to_clips', 'to_frames'
```

ViewField 重载了逻辑运算符，可以创建表达式对象，用于过滤样本（Sample）或标注（Label），操作符有 `==, >, >=, <, <=, |（或） , & （与）, ~ (非/否)`等：

```
F("uniqueness") > 0.7 # 类型为 fiftyone.core.expressions.ViewExpression，值： {'$gt': ['$uniqueness', 0.7]}

match = (F("confidence") > 0.99) | (F("label").is_in(("cat", "dog"))) 

match = ~F("label").is_in(("cat", "dog"))

# predictions 类型为 
matching_view = dataset.match(
    F("predictions.detections").filter( (F("confidence") > 0.99) | (F("label").is_in(("cat", "dog"))) ).length() > 0 # ’|‘ 表示’或‘，不能写成’||‘
)

```

使用复合的 ViewField 逻辑表达式时，最好多写一些括号来明确优先级和结合性，因为这些操作符的优先级和结合性不容易记住，且 ViewField 表达式出错的信息比较难解读。

对于样本的操作：

* match(expr: ViewExpression) , 匹配符合表达式的样本。等效于 dataset[expr]
  * match_tags(tag: str, bool=True|False)，match_tags(tags: list[str])，匹配具有指定 tag 属性的样本。参数为多个时，结果为’或‘的关系。第二个参数为False时，反向选择，即选中不具有该tag的样本。
  * exists(sample_field_name: str)，匹配具有指定字段的样本。
  * select(id_list: list[str]) ，匹配指定 id 的样本。
  * exclude(id_list: list[str])，匹配不具有指定 id 的样本。
* sort_by(sample_field_name: str, reverse=False)，按样本的字段排序
* sort_by(expr: ViewExpression, reverse=False) ，expr应产生可排序的值，例如 F("ground_truth.detections").length()
* shuffle(seed:number = None) ，随机化样本顺序。指定种子可以确保结果的确定性。
* take(n: int, seed:number = None)，随机抽样样本。

注意：使用 seed 只能确保在一个进程内的确定性；如果进程重启，即使使用与上次相同的 seed，也不能确保确定性。

对于样本内容的操作：

* select_fields(sample_field_name: str)，select_fields(sample_field_names: list[str])， 返回的 DatasetView 仅可见样本的指定字段，例如 `dataset.select_fields("ground_truth")` 。例外：id、filepath、tags、metadata 等特殊字段总是可见的。参数表为空时，仅保留特殊字段。
* exclude_fields(sample_field_name: str) ，与 select_fields 相反。
* filter_labels(sample_field_name, expr: ViewExpression)，根据标注内容过滤标注或样本，字段应是 Detections, Classifications, Polylines, Keypoints 类型。例子： `filter_labels("ground_truth", (F("label") == "slug") | (F("label") == "conch"))` ； `filter_labels("predictions", F("bounding_box")[2] * F("bounding_box")[3] >= 0.5)`
* map_labels(sample_field_name, dict[str, str])， 重映射类别。例如 animals_view = datasis_inet.map_labels("predictions", { "bear": "animal", "bear": "animal", "bear": "animal" })
* set_field(sample_field_name, expr: ViewExpression)，设置字段的值。例如 bounded_view = dataset.set_field("predictions.detections.confidence", F("confidence").max(0.5)) 设置 confidence = max(confidence, 0.5) 。
* select_labels(labels=None, ids=None, tags=None, fields=None, omit_empty=True)

几个要注意的点：

* 对于样本内容的过滤（filter_labels() ）并非只是过滤样本内容。如果一个样本的内容经过过滤器没有任何匹配，则这个样本也会被排除，从而造成样本数量的减少。
* filter_labels() 方法指定的属性与后面的过滤器其实是不完全匹配的。例如 `dataset.filter_labels("predictions", F("confidence") > 0.9)` ， 其实 `sample.predictions` 类型是 Detections，没有 confidence 属性，而是 `sample.predictions.detections[i]` 有 confidence 属性，但第一个参数并不能写成 "predictions.detections"。这说明 filter_labels() 自动展开了指定的标注属性。
* match()、sort_by() 等方法则不会自动展开样本的标注属性，表达式中引用标注时要写成 "predictions.detections" 或 "ground_truth.detections" 等， 如果 predictions 或 ground_truth 是 Detections 类型。 

### ViewField 的方法

ViewField （通常重命名为 `F`），除了可以用关系运算符（`==` 等）操作，还有很多方法。
可以用以下方式来探索：
```
from fiftyone import ViewField as F
vf = F('filepath')
vf. # 按下tab 两次

Display all 102 possibilities? (y or n)
vf.abs(           vf.ceil(          vf.exists(        vf.is_missing(    vf.lower(         vf.prepend(       vf.set_equals(    vf.sum(


help(F('filepath').contains_str) # 查看某个方法的文档

# json 序列化 ViewField。ViewField.to_mongo() 转化为 dict 类型
print(json.dumps(F('filepath').contains_str('_1652317626422_').to_mongo()))

# 显示 {"$regexMatch": {"input": "$filepath", "regex": "_1652317626422_", "options": null}}
```

一些常用的 ViewField 的方法：

starts_with(str_or_strs, case_sensitive=True) Determines whether this expression, which must resolve to a string, starts with the given string or string(s).

ends_with(str_or_strs, case_sensitive=True) Determines whether this expression, which must resolve to a string, ends with the given string or string(s).

contains_str(str_or_strs, case_sensitive=True) Determines whether this expression, which must resolve to a string, contains the given string or string(s).

re_match(regex, options=None) Performs a regular expression pattern match on this expression,    which must resolve to a string. e.g. `F("filepath").re_match("\.jpg$")`.
    
### 在 GUI 中使用视图 DatasetView 和 ViewStage , ViewField

GUI 上部有一个横向的视图栏（view bar），左边有“add stage”输入框，在此可以创建视图的过滤器（ViewStage）。一些过滤器，如 `Take`，只有简单的参数，(count, seed)；一些过滤器有复杂的参数，是以某种 json 语法写成的。记忆并手写复杂参数并不太可行，但有2个解决办法：

1. 在 python 控制台中用 python 语法创建视图，让 GUI 显示这个视图，则视图栏就会显示这个视图的名字和参数，可以将参数复制保存下来，以备后用。

2. 在 python 控制台中 json 序列化 ViewField 表达式。例如：

    ```
    print(json.dumps((F('filepath').length() > 1).to_mongo()))
    # 输出 {"$gt": [{"$size": {"$ifNull": ["$filepath", []]}}, 1]}
    ```
    ViewField 不能直接传入 json.dumps，但可以先用 to_mongo() 转为 dict。
    然后添加外层结构：
    ```
    { "$expr": {"$gt": [{"$size": {"$ifNull": ["$filepath", []]}}, 1]} }
    ```
    以上就可以输入到视图栏中了。

### DatasetView 操作举例

修改属性的范围，获得最大值：

```
# Lower bound all confidences in the `predictions` field to 0.5
bounded_view = dataset.set_field(
    "predictions.detections.confidence",
    F("confidence").max(0.5),
)

print(bounded_view.bounds("predictions.detections.confidence"))
# (0.5, 0.9999035596847534)
```

增加属性，获得列表属性的长度：

```
# Record the number of predictions in each sample in a new field
dataset.add_sample_field("num_predictions", fo.IntField)
view = dataset.set_field("num_predictions", F("predictions.detections").length())

view.save("num_predictions")  # save the new field's values on the dataset
print(dataset.bounds("num_predictions"))  # (1, 100)
```

保存：
```
dataset = foz.load_zoo_dataset("quickstart")

# Capitalize some labels
rename_view = dataset.map_labels("predictions", {"cat": "CAT", "dog": "DOG"})
rename_view.save()
```
仅有位于视图中的样本的修改会保存，其它的不变。

将数据集按 filepath 排序，并保存：
```
v1 = dataset.sort_by('filepath')
v1.save()
```
注意，fiftyone 0.14.2～0.14.3 可以保存 sort_by 的排序结果，但 0.14.4～0.19.1 不能保存，源数据集的样本顺序保持不变，这可能是bug[5.2]。
要绕过此问题，可以用排序后的视图代替数据库本身。

丢弃不在视图中的样本/标注：
```
# Discard all samples with no people
people_view = dataset.filter_labels("ground_truth", F("label") == "person")
people_view.keep()

# Delete the `predictions` field
view = dataset.exclude_fields("predictions")
view.keep_fields()
```

删除视图中的样本：

```
# Choose 10 samples at random
unlucky_samples = dataset.take(10)

dataset.delete_samples(unlucky_samples)
```

只加载样本的部分字段，可以提高速度：

```
start = time.time()
oiids = [s.open_images_id for s in dataset]
print(time.time() - start)
# 38.212332010269165

start = time.time()
oiids = [s.open_images_id for s in dataset.select_fields("open_images_id")]
print(time.time() - start)
# 0.20824909210205078
```

### PatchesView 视图

对于目标检测类数据集，Dataset 和 DatasetView 的直接子元素是样本（Sample/SampleView），代表一张图片，样本之下的子元素是检测框（Detection、Polyline）。
而 PatchView 则把以上数据结构平坦化了，PatchView 之下的直接元素就是检测框。即：
```
aPatchView: fiftyone.core.patches.PatchesView
  []: fiftyone.core.patches.PatchView
    predictions: fiftyone.core.labels.Detection
    metadata
    tags
    sample_id
    filepath
```

创建 PatchesView
```
import fiftyone as fo
import fiftyone.zoo as foz
from fiftyone import ViewField as F

dataset = foz.load_zoo_dataset("quickstart")

session = fo.launch_app(dataset)

# Convert to ground truth / predictions patches
gt_patches = dataset.to_patches("ground_truth", other_fields=True) # 包含 sample 上的其它字段
pd_patches = dataset.to_patches("predictions") # 不包含 sample 上的其它字段

len(dataset) # 200 ，是样本数量
len(gt_patches) # 1232 ，是真值框的数量
len(pd_patches) # 5620 ，是检测框的数量

session = fo.launch_app(dataset)
# View patches in the App
session.view = gt_patches
```

将 ground_truth 和 predictions 对比评估后，可以产生“Evaluation patches”：

```
import fiftyone as fo
import fiftyone.zoo as foz

dataset = foz.load_zoo_dataset("quickstart")

# Evaluate `predictions` w.r.t. labels in `ground_truth` field
dataset.evaluate_detections("predictions", gt_field="ground_truth", eval_key="eval")

session = fo.launch_app(dataset)

# Convert to evaluation patches，返回 EvaluationPatchesView 类型
eval_patches = dataset.to_evaluation_patches("eval")
print(eval_patches)

print(eval_patches.count_values("type"))
# {'fn': 246, 'fp': 4131, 'tp': 986}

# 查看评估后的 eval_patches 视图。
session.view = eval_patches
```

产生和查看 patch view 也可以在 GUI 中完成（工具栏patches按钮），可以选择不同的标注字段（如 ground_truth 和 predictions）、或者 evaluation key 。
在 GUI 中，如果要设置包含 sample 上的其它字段，可以在视图栏（view bar）中，给 ToPatches 操作的第二个参数填写 `{ "other_fields" : true }`（json格式，默认是 None）。

EvaluationPatchesView 的数据结构：
```
aEvaluationPatchesView: fiftyone.core.patches.EvaluationPatchesView
  []: fiftyone.core.patches.EvaluationPatchView
    type: 'tp'|'fp'|'fn' ， True Positive 等
    iou: float，真值和预测值的交并比
    ground_truth: Detections
      detections: list[Detection], 长度只有1
    predictions: Detections # 名字不固定，与原 dataset 中的预测值字段相同。
      detections: list[Detection], 长度只有1
```

PatchView 和 EvaluationPatchView （统称 Patch）都可以设置 tags（通过 GUI 或 tag_labels() ），并可以通过 match_tags() 来过滤。
但是 Patch 的 tags 是无法持久的，进程重启后就会丢失。如果需要持久化，可以在 Patch 背后的 label 上设置 tags（通过 GUI 或者 `patch.<label_field>.detections[0].tags` 访问）。

### Clip views

Clip views 用于时域检测（Temporal Detection）的视频数据集。一个 clip 表示一个视频文件。

### Frame views

Frame view 用于视频数据集，但已经将视频抽帧了。

## 高级导入导出操作

### 导出 coco

```
# 仅输出标注文件
dataset.export(dataset_type=fo.types.COCODetectionDataset,
    labels_path="/media/data2/annotation/人体接触/人体接触拟真测试集/object_detection/touch_7_1_test__2022-09-20/annotations/instances_default.json",
    label_field="ground_truth",
    #classes=classes,
)

# 输出标注文件和图像文件，后者采用符号链接。
dataset.export(dataset_type=fo.types.COCODetectionDataset,
    labels_path="/media/data2/annotation/人体接触/人体接触拟真测试集/object_detection/touch_7_1_test__2022-09-20/annotations/instances_default.json",
    data_path="/media/data2/annotation/人体接触/人体接触拟真测试集/object_detection/touch_7_1_test__2022-09-20/images/",
    export_media="symlink",
    label_field="ground_truth",
    #classes=classes,
)
```

导出的 coco 标注文件中，images 和 annotions 的 id 是从 1 开始，而 categories 的 id 可能从 0 开始。
导出的图像目录是平坦化的，即不存在子目录；images 中的 file_name 也仅有文件名部分。
classes 参数是可选的，如果指定，则输出的 categories 列表包含 classes 中的分类；如果不指定，则输出数据集中 label_field 参数指定的字段中出现过的分类，这可能只是 classes 的一个子集。此外，classes 参数也可能影响 categories 类表中各分类的 id 顺序（未验证）。

### 导出时的标注类型转换
如果导出类型是某种目标检测格式（如 fo.types.COCODetectionDataset ），但要导出的标注字段的类型是某种单体标注格式（ Detection Patch, Classification），则进行自动转换。
图像分类会转换为整个图像的目标检测。
Classifications 导出为 COCODetectionDataset 是不支持的，但可以尝试先转换为 patch

### 导出 FiftyOneDataset

fo.types.FiftyOneDataset 是 fiftyone 的原生数据集存储格式，能够保留最多的信息，因此最适合用于备份 fiftyone 数据集。

```
dataset.export(
    export_dir=dirname,
    dataset_type=fo.types.FiftyOneDataset,
    export_media=False,
)
```

导出的目录结构为：

```
annotaions/  # 标注任务的元数据，以下每个 json 文件对应一个标注任务，即 dataset.list_annotation_runs() 中的项目。
  <annotaion_run_name_1>.json # 其中， task_ids 为 cvat task_id；job_ids 为 cvat task_id 到 cvat job_id 的映射。frame_id_map 的键为 cvat task_id。labels_task_map 的值为 cvat task_id 。
  <annotaion_run_name_2>.json
metadata.json # 整个数据集的元数据。如果使用了外部标注工具，则 annotation_runs 属性表示标注任务的元数据。如果用 cvat 后端， project_id 属性是 cvat 的 project_id 。
samples.json # 样本及其属性、标注。
```

恢复备份时：

```
dataset_restored = fo.Dataset.from_dir(
    dataset_dir=dirname, # 数据集和真值
    dataset_type=fo.types.FiftyOneDataset,
    name='test_cvat_2_i',
)
```

### 在同一数据集中导入真值和预测值

目前（2022.9）fiftyone 支持在一个数据集中追加 coco 格式的标注/预测值（labels），其它格式则不能直接支持，但可以先导入多个数据集，然后复制标注字段。

追加 coco 格式的标注，可以用 fouc.add_coco_labels()：
```
import fiftyone as fo
import fiftyone.utils.coco as fouc

data_path = "/home/duanyao/project/annotation/ds1/images"
gt_path = "/home/duanyao/project/annotation/ds1/annotations/instances_default.json"

dataset = fo.Dataset.from_dir(
     dataset_type=fo.types.COCODetectionDataset,
     data_path=data_path,
     labels_path=gt_path,
     label_field={"detections":"ground_truth"},
     name="my-dataset", # 可选
)

# And add model predictions
fouc.add_coco_labels(
    dataset,
    "det1",
    "/home/duanyao/project/annotation/detection_results/ds1/faster_rcnn_resnet50_coco-fp32/det1/det1.json",
    #classes=classes,
)
```
注意，add_coco_labels() 忽略标注文件（det1.json）中的 images 列表（如果有），而是直接根据 annotations 列表中的 image_id 去匹配目标数据集（dataset参数）中的样本，而匹配规则是将样本在目标数据集中的顺序号当作 image_id（细节还有待测试）。因此，如果希望追加的标注能正确匹配，就要保证目标数据集的样本顺序与标注文件中的 image_id 的顺序是一致的，这可以通过排序操作来实现：
```
fouc.add_coco_labels(
    dataset.sort_by('filepath'),
    #...
)
```

目前（2024.8.15, 0.24），`add_coco_labels()` 无法导入 coco 人体关键点，但 fo.Dataset.from_dir() 则可以。

## 统计操作

```
import fiftyone as fo
import fiftyone.zoo as foz

dataset = foz.load_zoo_dataset("quickstart")
# 统计离散值（预测值的各个分类）的数量
dataset.count_values("predictions.detections.label") # {'zebra': 25, 'dog': 49, 'sports ball': 11, 'scissors': 9, 'bus': 16 ... }

# 统计连续值（预测值的置信度）的上限和下限
dataset.bounds("predictions.detections.confidence") # (0.05003104358911514, 0.9999035596847534)
```
注意引用样本属性的方式，predictions.detections 是个列表，label 并非 predictions.detections 的属性，而是 predictions.detections[i] 的属性，但在 count_values() 的参数中应当写成 "predictions.detections.label" 。

## 模型指标评估

### 图像分类的评估

### 目标检测的评估

#### 评估方法

评估是通过 Dataset 或 DatasetView 的 evaluate_detections() 方法发起。评估方法（method参数）有多种，默认为 'coco'，还有 'open-images'。

COCO-style evaluation：

* 默认 IoU 阈值是 0.5，但可以通过 iou 参数修改。
* 默认采用“同类别”（classwise=True）的方式来匹配预测值和真值。可以修改为“允许不同类别”（classwise=False）。
* 一个真值一般最多匹配一个预测值，除非真值具有 iscrowd=True 的属性，表明此真值代表挤在一起的多个同类目标，这时允许一个真值匹配多个预测值。


关于匹配方式和classwise参数：目标检测评估时，首先要将检测框与真值框进行匹配，匹配有两种方式：（1）对每个检测框，与其分类相同的真值框中，与其IOU最大的作为匹配的真值框（有多个IOU相同的再看置信度）。（2）对每个检测框，不论真值框的分类，与其IOU最大的作为匹配的真值框（有多个IOU相同的再看置信度）；当然，如果分类不同，在之后会被算作一个FP。一般来说，每个真值框最多允许一个检测框与其匹配，除非iscrowd属性为真，如果确有多个检测框与其IOU大于阈值，则选IOU最大的。方式1就是 classwise=True，方式2就是 classwise=False 。这段参考了 fiftyone 源码 `fiftyone/utils/eval/coco.py:571:_compute_matches()` 。

关于置信度阈值：evaluate_detections() 似乎无法指定置信度阈值的参数。根据自行测试，似乎是将置信度阈值为0的结果。如果希望设定置信度阈值，可以先手动过滤预测值：
```
v_50 = dataset.filter_labels("predictions", F("confidence") >= 0.50, only_matches = False) # only_matches = False 确保过滤后没有检测值的样本也保留在结果中，默认是 true。
v_50.evaluate_detections("predictions", gt_field="ground_truth", eval_key="eval_c_50", iou=0.50)
```

Open Images-style evaluation：

* Non-exhaustive image labeling
* Class hierarchies

#### 查看评估结果

load_evaluation_view() 返回当初执行评估的那个视图：
```
ev = dataset.load_evaluation_view(eval_key)
```

load_evaluation_results() 返回上次的评估结果：
```
es = dataset.load_evaluation_results(eval_key)
es.metrics() # 形如 {'accuracy': 0.300, 'precision': 0.429, 'recall': 0.5, 'fscore': 0.46, 'support': 748}
```

to_evaluation_patches() 返回 evaluation patches view
```
ep = dataset.to_evaluation_patches(eval_key)
```

#### 例1：目标检测的评估[1.6]：
```
import fiftyone as fo
import fiftyone.zoo as foz

dataset = foz.load_zoo_dataset("quickstart")
print(dataset)

# Evaluate the objects in the `predictions` field with respect to the
# objects in the `ground_truth` field
# results 的类型为 fiftyone.utils.eval.detection.DetectionResults
# eval_key="eval_predictions" 指定了在 Sample.predictions.detections[i]（Detection类型）对象下创建 eval_predictions 属性。
results = dataset.evaluate_detections(
    "predictions",
    gt_field="ground_truth",
    eval_key="eval_predictions",
    iou=0.75, # IOU 阈值，默认为 0.5。计算mAP 时不需要，但可以指定 iou_threshs。
    compute_mAP=True # 计算 mAP 和 PR 曲线
)

# Get the 10 most common classes in the dataset
counts = dataset.count_values("ground_truth.detections.label")
classes = sorted(counts, key=counts.get, reverse=True)[:10]

# Print a classification report for the top-10 classes
results.print_report(classes=classes)

               precision    recall  f1-score   support

       person       0.45      0.74      0.56       783
         kite       0.55      0.72      0.62       156
          car       0.12      0.54      0.20        61
         bird       0.63      0.67      0.65       126
       carrot       0.06      0.49      0.11        47
       
    micro avg       0.32      0.68      0.44      1311
    macro avg       0.27      0.57      0.35      1311
 weighted avg       0.42      0.68      0.51      1311
 
# 打印数据集信息，注意 dataset 多出了 eval_predictions_tp、eval_predictions_fp、eval_predictions_fn 几个字段，表示每个样本中tp、fp、fn的数量。
dataset

Name:        quickstart
Media type:  image
Num samples: 200
Persistent:  False
Tags:        ['validation']
Sample fields:
    id:                  fiftyone.core.fields.ObjectIdField
    filepath:            fiftyone.core.fields.StringField
    tags:                fiftyone.core.fields.ListField(fiftyone.core.fields.StringField)
    metadata:            fiftyone.core.fields.EmbeddedDocumentField(fiftyone.core.metadata.Metadata)
    ground_truth:        fiftyone.core.fields.EmbeddedDocumentField(fiftyone.core.labels.Detections)
    uniqueness:          fiftyone.core.fields.FloatField
    predictions:         fiftyone.core.fields.EmbeddedDocumentField(fiftyone.core.labels.Detections)
    eval_predictions_tp: fiftyone.core.fields.IntField
    eval_predictions_fp: fiftyone.core.fields.IntField
    eval_predictions_fn: fiftyone.core.fields.IntField

# 按一个样本上的FP数量倒排序样本，并显示FP。eval_predictions_fp 是 Sample 的属性， eval_predictions 是 Sample.predictions.detections[i] 的属性。
view = (
    dataset
    .sort_by("eval_predictions_fp", reverse=True)
    .filter_labels("predictions", F("eval_predictions") == "fp")
)

# Visualize results in the App
session = fo.launch_app(view=view)
```

dataset.evaluate_detections() 除了返回一个 fiftyone.utils.eval.detection.DetectionResults 对象，还修改了 dataset 对象：

* dataset 对象上增加了 eval_predictions_tp、eval_predictions_fp、eval_predictions_fn 几个int字段，表示每个样本中tp、fp、fn的数量。
* dataset[i].predictions.detections[j] （labels.Detection 类型）增加了以下属性（'eval_predictions'是个前缀，由用户通过 dataset.evaluate_detections(eval_key="eval_predictions") 指定）：
  * eval_predictions: 'tp'|'fp'|'fn'，表示评估的结果。
  * eval_predictions_id: str, 类似 '5f452471ef00e6374aac53c8'。
  * eval_predictions_iou: float, 与真值框的交并比。


#### 例2：从文件分别加载coco格式的真值和预测值：

```
import fiftyone as fo
import fiftyone.utils.coco as fouc

data_path = "/home/duanyao/project/annotation/task_人体检测 广东湛江市小红帽幼儿园 大一班 1624496810975.576p.rk.mp4-2021_10_11_09_37_11-coco 1.0/images"
labels_path = "/home/duanyao/project/annotation/task_人体检测 广东湛江市小红帽幼儿园 大一班 1624496810975.576p.rk.mp4-2021_10_11_09_37_11-coco 1.0/annotations/instances_default.json"

dataset = fo.Dataset.from_dir(
     dataset_type=fo.types.COCODetectionDataset,
     data_path=data_path,
     labels_path=labels_path,
     label_field={"detections":"ground_truth"}
     name="my-dataset", # 可选
)

# And add model predictions
fouc.add_coco_labels(
    dataset,
    "predictions",
    "/home/duanyao/project/annotation/detection_results/ds1/faster_rcnn_resnet50_coco-fp32/det1/det1.json",
    #classes=classes,
)

# Verify that ground truth and predictions were imported as expected
print(dataset.count("ground_truth.detections"))  # 913
print(dataset.count("predictions.detections"))  # 915

# Evaluate the objects in the `predictions` field with respect to the
# objects in the `ground_truth` field
results = dataset.evaluate_detections(
    "predictions",
    gt_field="ground_truth",
    eval_key="eval_predictions",
    compute_mAP=True,
)
```

#### 例3：绘制混淆矩阵（查看每个错误个例）。
建议在 jupyter notebook 中运行混淆矩阵的代码。

```
import fiftyone as fo
import fiftyone.zoo as foz

dataset = foz.load_zoo_dataset("quickstart")

# Perform evaluation, allowing objects to be matched between classes
results = dataset.evaluate_detections("predictions", gt_field="ground_truth", classwise=False)

# Generate a confusion matrix for the specified classes
plot = results.plot_confusion_matrix(classes=["car", "truck", "motorcycle"])
plot.show() # 在文档中嵌入显示混淆矩阵。如果是在 python shell 中执行，则在随机端口让浏览器显示。显示 plot 并不需要先 launch_app()。

# 在 notebook 中，51的界面会直接嵌入到文档中。每个不同的notebook应指定不同的端口号，以免冲突。
session = fo.launch_app(dataset, port=6151)

# 将混淆矩阵视图和51的界面联系在一起，可以互动。
session.plots.attach(plot)
```

点击混淆矩阵中的每个单元时，51的界面中会显示相应的实例的patch。

如果在 python shell 中执行上述代码，混淆矩阵将在单独的浏览器页面中显示，但功能不全，例如点击混淆矩阵中的每个单元时，51的界面并不会有反应。

注意 classwise=False 参数。coco协议的目标检测评估方法（也是fiftyone的默认方法）在默认情况下是 classwise=True，大多数人也采用此模式。
classwise=True 的混淆矩阵不是很有用，因为根据定义，不同类别的真值框和预测框绝不会配对，所以混淆矩阵中不存在类别间混淆，只可能存在“某类别”与“无类别（混淆矩阵中标记为 none 类别）”的混淆。
classwise=False 的混淆矩阵可能更有用，可能存在类别间混淆，不过仍然有“无类别（none）”的混淆。

#### 例4： 查看 evaluation patches view，查看bad case

```
import fiftyone as fo
import fiftyone.zoo as foz
from fiftyone import ViewField as F

dataset = foz.load_zoo_dataset("quickstart")

results = dataset.evaluate_detections("predictions", gt_field="ground_truth", eval_key="eval_predictions")

session = fo.launch_app(dataset)

# 切换到 evaluation_patches view
v1 = dataset.to_evaluation_patches(eval_key='eval_predictions')
session.view = v1

# 查看其中的 false positive 实例
v2 = v1.filter_labels('predictions', F('eval_predictions')=='fp')
session.view = v2

# 将 false positive 实例按 confidence 倒排。
v3 = v2.sort_by('predictions.detections.confidence', reverse=True)
session.view = v3

# 查看其中的 false positive 或 false negtive 实例。
v4 = v1.filter_field('type', F().is_in(('fn','fp')))
session.view = v4

# 对单独的一帧进行评估和查看 evaluation patches
v5 = v1.select('62fb875980d044258f570496')
session.view = v5
r5 = v5.evaluate_detections("predictions", gt_field="ground_truth", eval_key='eval_predictions', classwise=False)
pv5 = v5.to_evaluation_patches(eval_key='eval_predictions')
pv5 = pv5.sort_by('predictions.detections.confidence', reverse=True)
session.view = pv5
```

注意 to_evaluation_patches() 创建的 EvaluationPatchesView 增加了几个属性，对应 ground_truth/predictions 中的值：
* type: 等效于 predictions.detections[0].eval_predictions
* iou: 等效于 predictions.detections[0].eval_predictions_iou
* crowd: 等效于 ground_truth.detections[0].iscrowd

所以在操作 EvaluationPatchesView 时， `v1.filter_field('type', F() == 'fp')` 等效于 `v1.filter_labels('predictions', F('eval_predictions')=='fp')` 。

#### 例5：mAP 和 PR 曲线。

```
import fiftyone as fo
import fiftyone.zoo as foz

dataset = foz.load_zoo_dataset("quickstart")
print(dataset)

# Evaluate the objects in the `predictions` field with respect to the
# objects in the `ground_truth` field
# results 的类型为 fiftyone.utils.eval.detection.DetectionResults
# eval_key="eval_predictions" 指定了在 Sample.predictions.detections[i]（Detection类型）对象下创建 eval_predictions 属性。
results = dataset.evaluate_detections(
    "predictions",
    gt_field="ground_truth",
    eval_key="eval_predictions",
    compute_mAP=True # 计算 mAP 和 PR 曲线
)

plot = results.plot_pr_curves(classes=["person", "kite", "car"])
plot.show() # 在新的浏览器窗口里显示。
```

### 评估时域检测/Temporal Detections

#### 时域检测数据集 FiftyOneTemporalDetectionDataset

表示视频和时域检测信息。

硬盘数据集格式：
```
<dataset_dir>/
    data/
        <uuid1>.<ext> # 视频文件。uuid1 实际上可以是任意字符串。
        <uuid2>.<ext>
        ...
    labels.json
```
labels.json 的格式：
```
{
    "classes": [ # 可选。
        "<labelA>",
        "<labelB>",
        ...
    ],
    "labels": {
        "<uuid1>": [
            {
                "label": <target>, # 可以是字符串，直接表明类别；或者是整数，根据 classes[target] 查询类别。
                "support": [<first-frame>, <last-frame>],
                "confidence": <optional-confidence>,
                "attributes": {
                    <optional-name>: <optional-value>,
                    ...
                }
            },
            {
                "label": <target>,
                "support": [<first-frame>, <last-frame>], # support 与 timestamps 字段二选一，表示事件的时段。
                "confidence": <optional-confidence>,
                "attributes": {
                    <optional-name>: <optional-value>,
                    ...
                }
            },
            ...
        ],
        "<uuid2>": [
            {
                "label": <target>,
                "timestamps": [<start-timestamp>, <stop-timestamp>],
                "confidence": <optional-confidence>,
                "attributes": {
                    <optional-name>: <optional-value>,
                    ...
                }
            },
            ...
        ]
```

导入后的数据结构：

```
dataset: fiftyone.core.dataset.Dataset
    [i]: fiftyone.core.sample.Sample
        ground_truth: fiftyone.core.labels.TemporalDetections
            detections: mongoengine.base.datastructures.BaseList
                [i]: fiftyone.core.labels.TemporalDetection
                
interface TemporalDetection {
    'id': '630d79b67182e01a2d90d2a0',
    'tags': BaseList([]),
    'label': 'str',
    'support': BaseList([785, 801]), # 时间段，按帧数计算。
    'confidence': None | float,
}
```

FiftyOneTemporalDetectionDataset 中每个 Sample 表示一个视频文件。此视频文件应当存在，fo 会访问它的元数据，特别是帧的时间戳。
尽管 FiftyOneTemporalDetectionDataset 的 json 存储格式可以用 timestamps 字段（单位是秒）来指示时间段，但导入后还是会转化为 support 字段（单位是帧）。

#### 时域检测评估方法
evaluate_detections() 对于时域数据集会自动应用 ActivityNet-style 时域检测方法。或者也可以显式指定参数 `method='activitynet'`。

ActivityNet-style evaluation：
* 默认 iou 阈值为 0.5，可用 iou 参数修改。
* 默认只匹配同类的真值和预测值，可用 classwise=False 修改。


#### 例1：导入硬盘数据集和预测文件

```
import fiftyone as fo

# Create the dataset
t_td = fo.Dataset.from_dir(
    dataset_dir='/home/duanyao/annotation/人体接触/人体接触拟真测试集/temporal_detection/ground_truth/', # 数据集和真值
    dataset_type=fo.types.FiftyOneTemporalDetectionDataset,
    name='touch_td_1', # td=="temporal_detection"
)

prd_1 = fo.Dataset.from_dir(
    data_path="/home/duanyao/annotation/人体接触/人体接触拟真测试集/temporal_detection/ground_truth/data/", # 视频目录，采用真值的
    labels_path="/home/duanyao/annotation/人体接触/人体接触拟真测试集/temporal_detection/prediction/picodet_m_416p_touch_1n2_1_m2__step2_th20_ma1/labels.json", # 预测文件
    dataset_type=fo.types.FiftyOneTemporalDetectionDataset,
    name='td_picodet_m_416p_touch_1n2_1_m2__step2_th20_ma1',
)

# 如果导入中或导入后发现出错，删掉数据集重来：
#ds=fo.load_dataset('td_picodet_m_416p_touch_1n2_1_m2__step2_th20_ma1')
#ds.delete()

# 验证 gt 和 prd 的样本一致
prd_1_files = { s.filepath for s in prd_1 }
gt_files = { s.filepath for s in t_td }
prd_1_files - gt_files # 应为 set() 空集
gt_files - prd_1_files # 应为 set() 空集

for src in prd_1:
    dest = t_td[src.filepath]
    dest['picodet_m_416p_touch_1n2_1_m2__step2_th20_ma1'] = src.ground_truth
    dest.save()
    
for src in prd_1:
    dest = t_td[src.filepath]
    dest['pred'] = src.ground_truth
    dest.save()
    
t_td.persistent = True
t_td.save()
```

#### 例2：时域检测评估

```
import fiftyone as fo
t_td = fo.load_dataset('touch_td_1')

er_picodet_m_416p_touch_1n2_1_m2__step2_th20_ma1 = t_td.evaluate_detections(
    "picodet_m_416p_touch_1n2_1_m2__step2_th20_ma1",
    gt_field="ground_truth",
    eval_key="eval_picodet_m_416p_touch_1n2_1_m2__step2_th20_ma1",
    method='activitynet',
    classwise=True,
    compute_mAP=True # 计算 mAP 和 PR 曲线
)

er_picodet_m_416p_touch_1n2_1_m2__step2_th20_ma1.print_report()

plot = er_picodet_m_416p_touch_1n2_1_m2__step2_th20_ma1.plot_pr_curves()
plot.show() # 在新的浏览器窗口里显示。
```

### 评估语义分割/Semantic segmentations
略

### 其它评估技巧

* 评估数据可以保存在 Dataset 层级，可以保存并在以后再加载出来。
* 一个 Dataset 上可以保存多个评估数据，只要使用不同的 eval_key 来区分。用 `dataset.list_evaluations()` 可以列出保存的多个 eval_key。
* 如果有的评估是在视图的层级执行的，它的评估数据也是根据 eval_key 存储在 Dataset 层级的。其视图可以用 eval1_view = dataset.load_evaluation_view("my_eval1") 重新加载出来。
* 使用 select_fields 参数（`dataset.load_evaluation_view("my_eval1", select_fields=True)`）可以隐藏其它无关的评估数据。

## 数据标注（用 cvat 后端）

可以将 fiftyone 数据集或 DateView 导入 cvat 等标注工具进行标注，然后回传标注结果到 fiftyone 数据集中。

编辑 fiftyone 的 cvat 配置文件： ~/.fiftyone/annotation_config.json
```
{
  "backends": {
      "cvat": {
          "url": "http://gtrd.local:8080",
          "username": "admin",
          "password": "123456"
      }
  }
}
```

如果要创建的标注任务帧数很多，应当先提高 ulimit -n （默认只有 1024），在运行 fiftyone 前，在其 shell 中运行：

```
ulimit -n 1000000
ulimit -n # 确认生效
```

创建标注任务：

```
import fiftyone as fo
dataset = fo.load_dataset("nz_od_1_05")
classes = ["明确接触", "可能接触", "不太可能接触", "成人"]

v1 = dataset[0:5000]

# 对一个 DataView 创建标注任务。 "nz_od_1_05_t1" 为标注任务名；label_type="detections" 表示这是目标检测类标注； launch_editor=True 会显示对应的 cvat 任务 url，并打开浏览器。
# segment_size 是 cvat task 下 job 的最大帧数。
# 如果 DataView 的样本很多，此操作会打开大量文件，故需要设置 ulimit -n。
# cvat 对单个 task 的大小有限制，具体值不明。我们实验 14k 帧会出错，局域网内 5000 帧可正常导入，但广域网上可能还要减少，比如 2000 帧。
# 此操作会上传样本到 cvat，如果是通过远程网络，可能会耗时较长。
v1.annotate("nz_od_1_05_t1", backend='cvat',  project_id = 55, label_field="ground_truth", label_type="detections", classes=classes, launch_editor=True, segment_size=500)

---------
Uploading samples to CVAT...
Upload complete
Launching editor at 'http://gtrd.local:8080/tasks/12/jobs/19'...
---------

# 接下来就可以在 cvat 中打开 task 12，开始标注。

dataset.list_annotation_runs() # ['nz_od_1_05_t1']

dataset.load_annotations('nz_od_1_05_t1') # 从 cvat 回传标注结果。可以多次调用，边标注边回传。
```
backend='cvat' 是可选的，不指明时由 fiftyone.annotation_config.default_backend 指定，一般是 'cvat'。
对于不含标注的数据集（如通过 ImageDirectory 创建），classes 参数是必选的。但如果指定了 project_id 参数，classes 参数实际上不起作用，这时可以随便指定一个无意义的分类，例如 `classes=['_']`。

~/.fiftyone/annotation_config.json 中的配置项均可以在调用 DataView.annotate() 方法时覆盖，只要用对应的关键字参数：

```
view.annotate(
    ...
    backend="cvat",
    url="http://localhost:8080",
    username=...,
    password=...,
    segment_size: 1000
)
```
segment_size 是 task 下 job 的最大帧数。

但是，不建议覆盖 url、username、password 字段，因为 results.cleanup() 等方法仍然自动从 annotation_config.json 获得这些配置，会引起混乱。

还可以指定task所属的project（应事先创建）：
```
view.annotate(
    ...
    project_id = 55,
    # project_name = '',
)
```

查看对应的 cvat 配置（task_id、project_id ）等：

```
res = dataset.load_annotation_results('nz_od_1_05_t1') # 返回 CVATAnnotationResults 对象
res.task_ids # [1485] # 对应的 cvat task_id
res.config.project_id # 55 # 对应的 cvat project_id
res.job_ids # {1485: [1386, 1385, 1384, 1383]} # 对应的 cvat task_id 和 job_id
res.labels_task_map # {'ground_truth': [1485]} # 'ground_truth' 为51数据集中标注所在的字段，1485 为对应的 cvat task_id
res.frame_id_map # {1485: {0: {'sample_id': '644a3726714d25329f7fbe38'}, 1: {'sample_id': '644a3726714d25329f7fbe39'}, 2: {'sample_id': '644a3726714d25329f7fbe3a'}}} # 1485 为 cvat task_id
res.project_ids # [] # 总是空的，不知有什么用。
res.config.url # 'https://label-1.ai-parents.cn'
# 标签定义
res.config.label_schema # {'ground_truth': {'type': 'detections', 'classes': [{'classes': ['明确接触'], 'attributes': {}}, {'classes': ['可能接触'], 'attributes': {}}, {'classes': ['成人'], 'attributes': {}}], 'attributes': {}, 'existing_field': True, 'allow_additions': True, 'allow_deletions': True, 'allow_label_edits': True, 'allow_spatial_edits': True}}
```

或者用 `.print_status()` 命令显示标注任务属性。

```
res.print_status()

Status for label field 'ground_truth':

        Task 1485 (FiftyOne_nz_od_1_05_t1):
                Status: annotation
                Assignee: None
                Last updated: 2023-04-27T08:56:04.194921Z
                URL: https://label-1.ai-parents.cn/tasks/1485

                Job 1386:
                        Status: annotation
                        Assignee: None
                        Reviewer: None
```

从 cvat 同步（下载）标注结果：
```
dataset.load_annotations('nz_od_1_05_t1')
```

调用 `dataset.clone()` 产生的副本数据集，也是带有同样的与 cvat 关联的信息的，都可以从 cvat 同步标注结果。

关于 cvat project 设置的一些注意事项：

1. 如果用 project_id 指定 project，可能会遇到“Project 'xx' not found”的问题，这多发于新创建的 project，等一会儿重试可能会解决。
2. 如果用 project_name 指定 project，可能会导致新建一个同名的 project，而不是利用已有的 project。所以建议使用 project_id 而不是 project_name 。
3. 如果在 CVAT 中修改了 task 所属的 project，fiftyone 无法自动感知到这个变化，数据集关联的 project 仍然是原来的。
4. fiftyone 是利用 `CVATAnnotationResults.config.project_id` 来记录所属的 cvat project。可以手动修改此属性，下次调用 `load_annotations()` 时，将会自动同步 `CVATAnnotationResults.config.label_schema` 等属性 。例子：
    ```
    for key in dataset.list_annotation_runs():
        res = dataset.load_annotation_results(key)
        res.config.project_id = 59 # 修改关联的 cvat project_id 为 59
        res.load_annotations() # 下载标注，将使用 project_id 为 59 的标签定义。
    ```
    但是，修改 project_id 是非持久的，下次重新加载此数据库时， project_id 又会恢复为 55。所以下次调用 `load_annotations()` 前，仍需要修改 `project_id` 。

5. 如果在 CVAT 中修改了 task 或其所属的 project 的标签定义，会怎么样？经过测试，fiftyone 似乎是用标签名（而非标签id）来识别标签的。如果 fiftyone 数据集中有某个标签而 cvat 中没有，则 `load_annotations()` 会抛异常（ `KeyError: 'XX'` ）。如果 cvat 中有某个标签而 fiftyone 数据集中没有，则`load_annotations()`不会报错，但也不会下载这个标签的标注。

关于 cvat task 设置的一些注意事项：
1. 不指定 project_id 或指定 `project_id=None` 时，标注任务没有所属的 project。
2. 如果在 CVAT 中增加了 task 的标签类别，且 task 无 project，则 `load_annotations()` 无法下载新增的类别的标注。文档说 `load_annotations(key, unexpected='return')` 可以下载新增的类别，但实测没有效果（版本0.16.6）。解决办法是手动修改 `CVATAnnotationResults.config.label_schema['ground_truth']['classes']`（是个 str 的列表），增加新的类别后，再调用 `load_annotations()`。如前所述，随 config 的修改是不持久的。此外，修改 dataset.default_classes 来新增类别也是无效的。


查看进行中的标注结果：
```
view = dataset.load_annotation_view('nz_od_1_05_t1')
session = fo.launch_app(dataset, remote=True, port=5151, address = '0.0.0.0')
session.view = view
```

当然，也可以同步标注后再查看。

删除标注任务：

```
res = dataset.load_annotation_results('nz_od_1_05_t1')
res.cleanup() # 从 cvat 上删除任务
dataset.delete_annotation_run('nz_od_1_05_t1') # 从 fiftyone 中删除任务。
```

从 fiftyone 中删除任务应该在从 cvat 上删除任务之后。否则，就需要手动从 cvat 中删除任务。
确保在删除标注任务前，已经同步或备份了有用的标注结果！

查找样本所在的标注任务和帧序号：

```
# 定义一个查找顺序号的函数，第一个参数是数据集/视图，第二个参数是sample的id或filepath：
def find_idx(v, id_or_path):
    for i,s in enumerate(v):
        if s.id == id_or_path or s.filepath == id_or_path:
            return i
    return -1
    
def find_task_idx(dataset, id_or_path):
    for key in dataset.list_annotation_runs():
        view = dataset.load_annotation_view(key)
        idx = find_idx(view, id_or_path)
        if idx > 0:
            res = dataset.load_annotation_results(key)
            return (res.task_ids[0], idx)
    return (-1, -1)

find_task_idx(dataset, '/media/...1649728346594__0_-1__00045.jpg') # 返回 (1485, 1200)，即 (task_id, 帧序号)
```

给样本设置所属的 task_id 和帧序号：

```
def fill_cvat_info(dataset):
    for key in dataset.list_annotation_runs():
        view = dataset.load_annotation_view(key)
        res = dataset.load_annotation_results(key)
        task_id = str(res.task_ids[0])
        for i, s in enumerate(view):
            s['task_id'] = task_id
            s['frame_no'] = i
            s.save()

fill_cvat_info(dataset)
```

这样，每一帧的 `task_id` 属性表示对应的 cvat task_id，`frame_no` 表示其在 cvat task 内的帧序号。

目前（2024.8, 0.24），fo 的 cvat 后端无法从 cvat 2.x 下载人体关键点的标注，只能从 cvat 导出 coco 格式的关键点标注，再导入到 fo 中 [10.1]。不过，fo 可以使用旧版 cvat 的点标注功能实现关键点标注，但操作比较不便[10.2]。

## 标注/预测值合并

此处合并（merge）的意思是，将两套或以上基于同样底层样本（如图片）的标注/预测值合并到一起，并非是将基于不同底层样本的数据集合并在一起。

```
ds1 = fo.load_dataset('ds1')
ds2 = fo.load_dataset('ds2')

ds1.merge_samples(ds2) # 默认合并方式：两个数据集中具有相同 filepath 属性（按绝对路径算）的 Sample 视为同一 Sample，合并其同名的 Detections 、tags 等。

ds1.merge_samples(ds2, key_fcn = lambda sample: os.path.basename(sample.filepath)) # 指定用于确定同一 Sample 的方式，这里采用 filepath 属性的文件名部分。
```

## brain

### 安装
brain 是随着 fiftyone 的 pip 包一起安装的，无需单独安装。

### 计算图像相似度的例子

```
import fiftyone as fo
import fiftyone.brain as fob
import fiftyone.zoo as foz

# Load dataset
dataset = foz.load_zoo_dataset("quickstart")

# Index images by similarity。利用模型 'https://download.pytorch.org/models/mobilenet_v2-b0353104.pth'，首次使用时下载。
# 计算速度：13.3fps，GeForce 940MX 显卡。
fob.compute_similarity(dataset, brain_key="image_sim")

# Launch App
session = fo.launch_app(dataset)
```

在 GUI 中，勾选任意一张图片，点击工具栏上的  sort by similarity ，即将数据集中的图片按与所选图片的相似度从高到低排序（选中reverse则顺序相反）。这个操作可瞬间完成。

用编程的方式做相似度排序：
```
# Choose a random image from the dataset
query_id = dataset.take(1).first().id

# Programmatically construct a view containing the 15 most similar images
view = dataset.sort_by_similarity(query_id, k=15, brain_key="image_sim")

# View results in App
session.view = view
```

以上是以整张图为相似度计算单位，但对于目标检测数据集，通常希望按目标计算相似度。

```
import fiftyone as fo
import fiftyone.brain as fob
import fiftyone.zoo as foz

# Load dataset
dataset = foz.load_zoo_dataset("quickstart")

# Index ground truth objects by similarity
fob.compute_similarity(
    dataset, patches_field="ground_truth", brain_key="gt_sim"
)

# Launch App
session = fo.launch_app(dataset)
```

点击GUI工具栏patches按钮，选择ground_truth，切换到patches视图，然后选中patch，即可按相似度排序。

## issues

### vscode 中的调试错误
[5.1]

```
{"t":{"$date":"2022-08-05T07:56:19.459Z"},"s":"I",  "c":"CONTROL",  "id":20697,   "ctx":"main","msg":"Renamed existing log file","attr":{"oldLogPath":"/home/duanyao/.fiftyone/var/lib/mongo/log/mongo.log","newLogPath":"/home/duanyao/.fiftyone/var/lib/mongo/log/mongo.log.2022-08-05T07-56-19"}}
Subprocess ['/home/duanyao/opt/venv/v39/lib/python3.9/site-packages/fiftyone/db/bin/mongod', '--dbpath', '/home/duanyao/.fiftyone/var/lib/mongo', '--logpath', '/home/duanyao/.fiftyone/var/lib/mongo/log/mongo.log', '--port', '0', '--nounixsocket'] exited with error 100:
Uncaught exception
```

```
  File "/home/duanyao/opt/venv/v39/lib/python3.9/site-packages/fiftyone/core/service.py", line 177, in find_port
    raise ServiceListenTimeout(etau.get_class_name(self), port)
fiftyone.core.service.ServiceListenTimeout: fiftyone.core.service.DatabaseService failed to bind to port
```

解决办法是：先查看相关数据库进程（mongod）是否还存在，如果存在就终止它。客户端的异常终止可能会让数据库进程不退出，这就会阻止新的客户端正常启动。

### ulimit 偏小导致启动失败
很多桌面 linux 的默认 ulimit -n 只有 1024，这可能导致 fiftyone 的 mongodb 启动失败（可查看 ~/.fiftyone/var/lib/mongo/log/mongo.log ）。
在shell中设置 `ulimit -n 1000000` 可临时解决此问题。

### 非本机无法通过 http 连接 app，即使设置了 remote = True

fiftyone 0.14 的版本，执行 `fo.launch_app(remote = True)` 后，非本机即可以通过 `http://<address>:5151` 访问 app。
但 fiftyone 0.16.6 ~ 0.17.2 的版本，这不再可行，执行 `fo.launch_app(remote = True)` 后，也只有本机才能访问 `http://localhost:5151`。netstat -lntp 显示，其只监听了地址 `127.0.0.1:5151`。
解决办法是：显式指定监听的 IP 地址，例如： `fo.launch_app(remote = True, address = '0.0.0.0')`。

## 全数据库备份和恢复

### 安装 mongodb 客户端工具
下载 mongodb-database-tools[7.1]，解压后得到 mongodump 和 mongorestore 命令行工具。

### 备份

可以在 fiftyone 运行时进行，但应避免大规模的写操作。如果 fiftyone 没有运行，应单独启动 fiftyone 的 mongodb：
```
$HOME/opt/venv/v39-1/lib/python3.9/site-packages/fiftyone/db/bin/mongod --dbpath $HOME/.fiftyone/var/lib/mongo --logpath $HOME/.fiftyone/var/lib/mongo/log/mongo.log --port 0 --nounixsocket
```

启动 mongodb 后，要确定 mongod 的监听端口：
```
netstat -lnp | grep mongo
tcp        0      0 127.0.0.1:42893         0.0.0.0:*               LISTEN      737155/mongod
```
导出到一个压缩包：
```
cd /media/data2/annotation/fiftyone_bk
mongodump --uri=mongodb://127.0.0.1:42893 --archive=fo_20221020_1.gz --gzip
```
如果 `--uri=xxx` 后面还有其它参数，就不要把`=`用空格代替，否则 mongodump 会报告解析错误。

也可以导出为目录（在当前目录下生成 `dump/` 子目录，里面为 mongodb 的备份文件）：
```
cd /media/data2/annotation/fiftyone_bk
mongodump --uri mongodb://127.0.0.1:42893
```

### 恢复
应在 fiftyone 关闭时进行，并单独启动 fiftyone 的 mongodb：
```
$HOME/opt/venv/v39-1/lib/python3.9/site-packages/fiftyone/db/bin/mongod --dbpath $HOME/.fiftyone/var/lib/mongo --logpath $HOME/.fiftyone/var/lib/mongo/log/mongo.log --port 0 --nounixsocket
```
然后用 mongorestore 恢复。
```
cd /media/data2/annotation/fiftyone_bk
mongorestore  --uri=mongodb://127.0.0.1:42893 dump/ --drop
mongorestore  --uri=mongodb://127.0.0.1:42893 --archive=fo_20221020_1.gz --gzip --drop
```
加入 --drop 参数通常是必要的，因为 mongorestore 默认不删除/覆盖同名的旧数据，而--drop 参数要求它删除同名的旧数据。

## 使用独立的 mongodb 服务器

可以使用一个独立的 mongodb 服务器，让 fiftyone 使用这个服务器，而不是自行启动一个 mongodb 服务器。

注意，fiftyone 对3种IO延迟比较敏感：（1）fiftyone app 和 mongodb 服务器之间。（2）fiftyone app 和浏览器之间。（3）fiftyone app 和存储样本文件（图片和视频等）的服务器之间。
其中，第（1）种可能是影响最大的，（2）次之。这意味着最好让 fiftyone app 、 mongodb 服务器和浏览器运行在同一机器上。第（2）种延迟的可以在 0.16.6 导致GUI界面卡住，但在 0.19.1 有所缓解。

### 在 fiftyone 的服务器上

安装 mongodb-org-server：
```
sudo apt install /media/data2/software/mongodb-org-server_4.4.18_amd64.deb
```
当前（2023.3），mongodb-org-server 的最新版 6.0.4 但 fiftyone 需要 4.4.x 。

仍以 $HOME/.fiftyone/var/lib/mongo 为数据目录，启动一个 mongod 进程（确保 fiftyone 并未启动）：

```
/usr/bin/mongod --dbpath $HOME/.fiftyone/var/lib/mongo --logpath $HOME/.fiftyone/var/lib/mongo/log/mongo.log --port 13810 --bind_all_ip --nounixsocket
```

修改 `~/.fiftyone/config.json`:

```
{
    "database_uri": "mongodb://127.0.0.1:13810",
    "database_admin": false
}
```

或者：
```
export FIFTYONE_DATABASE_URI=mongodb://127.0.0.1:13810
export FIFTYONE_DATABASE_ADMIN=false
```

database_admin=false 可以确保此 fiftyone 实例不会自动升级 mongodb 数据库的版本，从而确保此数据库可以接受多个不同版本的 fiftyone 实例连接。

### 在 fiftyone 的客户机上

#### 1. 修改 `~/.fiftyone/config.json`:

```
{
    "database_uri": "mongodb://127.0.0.1:13810",
    "database_admin": false
}
```

#### 2. ssh 端口映射：

```
autossh -CNg -L 13810:127.0.0.1:13810 rd@gtrd.local
```
假定ssh服务器是 gtrd.local，用户名是 rd。

#### 3. 通过 nfs 挂载服务器的文件系统

挂载步骤略。

然后链接相关目录：

sudo mkdir 
