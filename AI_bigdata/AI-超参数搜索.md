## 参考资料

[3.1] PaddleClas-图像分类迁移学习-超参搜索
https://github.com/PaddlePaddle/PaddleClas/blob/5128d0bb0217e98341d1b1a85d6263023b0ae2f8/docs/zh_CN/algorithm_introduction/transfer_learning.md

[3.2] 超轻量图像分类方案PULC - 超参搜索
https://github.com/PaddlePaddle/PaddleClas/blob/c4653b7e26a5be0ba92f80c991c9e5ee4a0dfdf6/docs/zh_CN/training/PULC.md#4

[3.2] PULC 文本行方向分类模型

[4.1] PaddlePaddle AutoDL
https://github.com/PaddlePaddle/AutoDL
