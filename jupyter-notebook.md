## 关于 jupyter notebook
jupyter notebook 是在浏览器中运行的交互式 python shell 。
通过 pip 安装 notebook 包后，再执行 jupyter notebook 即可启动它。
jupyter notebook 有一些特殊语句，是在普通 python 环境下无法使用的，如：

```
# 将matplotlib的图表直接嵌入到Notebook之中
%matplotlib inline 
```
jupyter notebook 支持的语言其实不止 python，也包括 R, Julia, Scala。

## 安装和运行
最好在 venv 中运行：
```
pip install notebook # 2022.8 最新稳定版为 6.4.x

jupyter notebook # 启动 notebook

jupyter notebook --port=8888 --ip=0.0.0.0 # 指定监听地址。默认是 localhost:8888 ，并不支持非本机访问。
```

jupyter notebook 在当前 shell 启动一个 web 应用，并引导浏览器访问它，url 模式为 http://127.0.0.1:8888/?token=27ce65a43df4ce2f114a4da7c28c4492fea1472543cbf827 。

在浏览器界面中，首先显示jupyter notebook的当前目录，如果这里有 .ipynb 文件就可以直接打开它。
