## 参考资料

[1.2] Python：一篇文章掌握Numpy的基本用法
  https://zhuanlan.zhihu.com/p/29791319

[1.3] The Scientific Python ecosystem
  https://ncar-hackathons.github.io/scientific-computing/intro.html

[2.1] Filter a Numpy Array – With Examples
https://datascienceparichay.com/article/filter-numpy-array-with-examples/

[3.1] NumPy RuntimeWarning: divide by zero encountered in log10
https://bobbyhadz.com/blog/python-runtimewarning-divide-by-zero-encountered-in-log10

[3.2] numpy.seterr
https://numpy.org/doc/stable/reference/generated/numpy.seterr.html

[4.1] Memory Layout of Multi-dimensional Arrays
https://ncar-hackathons.github.io/scientific-computing/numpy/02_memory_layout.html

## 数组

创建：

```
>>> a=numpy.array([1,2,3], 'float32')
array([1., 2., 3.], dtype=float32)

>>> np.zeros((2,3), 'float32')
array([[0., 0., 0.],
       [0., 0., 0.]], dtype=float32)
       
>>> np.linspace(1., 4., 6) # 线性内插填充，参数 start, end, count。start 和 end 相同时，可填充全同的元素。
array([1. ,  1.6,  2.2,  2.8,  3.4,  4. ])
```

### 属性

```
>>> a=numpy.array([1,2,3], 'float32')
>>> a.size
3
>>> a.shape
(3,)
>>> a.dtype
dtype('float32')
```

### 改变形状 reshape
```
a = np.array([[1,2,3], [4,5,6]])
np.reshape(a, 6) # 变成1轴，指定第一轴长度，等效于 np.reshape(a, (6,)) 。array([1, 2, 3, 4, 5, 6])
a.reshape(-1) # 变成1轴，且不指定第一轴长度，等效于 np.reshape(a, (6,)) 。
np.reshape(a, (3,-1)) # 变成2轴，仅指定第一轴，等效于 np.reshape(a, (3,2)) array([[1, 2], [3, 4], [5, 6]])
np.reshape(a, (-1,2)) # 变成2轴，仅指定第2轴，等效于 np.reshape(a, (3,2))
```

### 合并

concatenate 不增加轴数，而是增加一个已有轴的长度。注意其默认增长的轴是0。
```
img0 = np.concatenate((frame, skeleton), axis=0)
```

stack 增加一个轴，增加的位置可以指定。stack 也可以作用于单个张量，结果是增加一个长度1的轴。
```
a = np.array([1, 2, 3])

b = np.array([4, 5, 6])

np.stack((a, b))
array([[1, 2, 3],
       [4, 5, 6]])

np.stack([a])
array([[1, 2, 3]])

arrays = [np.random.randn(3, 4) for _ in range(10)]

np.stack(arrays, axis=0).shape
(10, 3, 4)

np.stack(arrays, axis=1).shape
(3, 10, 4)

np.stack(arrays, axis=2).shape
(3, 4, 10)
```
expand_dims 在任意位置增加一个轴，长度为1：
```
numpy.expand_dims(a, axis)
```

等效于 torch 等的 unsqueeze 。

### 减少轴（维）
```
numpy.squeeze(a, axis=None)
```
减少的轴的长度必须是1。

### 切片

```
numpy.split(ary, indices_or_sections, axis=0)
```

### 复制

这两个语法与 python list 相同。

```
b = a.copy()
b = a[:]
```

### 重复填充

numpy.repeat(a, repeats, axis=None)

```
np.repeat(3, 4)
array([3, 3, 3, 3])

x = np.array([[1,2],[3,4]])

np.repeat(x, 2)
array([1, 1, 2, 2, 3, 3, 4, 4])

np.repeat(x, 3, axis=1)
array([[1, 1, 1, 2, 2, 2],
       [3, 3, 3, 4, 4, 4]])

np.repeat(x, [1, 2], axis=0)
array([[1, 2],
       [3, 4],
       [3, 4]])
```

### 转置/移轴
```
# transpose 要重新指定每个轴的新位置，即使没有变化的轴，返回视图：
np.transpose(x, (2, 0, 1))
# moveaxis(a, source, destination) 指定一个轴的新位置，其他轴的顺序不变，返回视图：
numpy.moveaxis(x, -1, -3)
# swapaxes(a, axis1, axis2) 交换两个轴的位置，返回视图：
np.swapaxes(x,0,1)
```

### 转换

np 数组转换成list，使用 np_array.tolist() 。np 标量也有 tolist() 方法，可以转换为 python 原生数值类型（float和int），也可以用 python 原生数值类型的构造函数来转换：`float(np_float32)` 。

np 数组转换成 tuple，使用 tuple(np_array.tolist())。

使用 list(np_array) 或者 tuple(np_array) 看似也可以将 np 数组转换成list或tuple，但与 np_array.tolist() 有重大区别：np_array.tolist() 还会将内部元素的类型转换为 python 原生的数值类型，而 list(np_array) 和 tuple(np_array) 则不做这个转换。

要将 np 数组转换成不同的标量类型，可使用 astype(type)函数。
np_array.astype('float32' | 'float64' | 'float16' | float | numpy.float32 | numpy.float64)  # float 与 'float64' 等效，字符串与对应的 class 等效。

np 与 python 原生数值类型做混合运算时会进行隐式转换，转换为较宽的 np 类型。例如
```
type(np.int32(1)+1.0) # <class 'numpy.int64'>
type(np.float32(1)*1.0) # <class 'numpy.float64'>
```

### 类型层次

np 数组的类型层次是:
```
object
  numpy.ndarray
```
不同的 dtype 并不做区分。

np 标量的类型层次是（以int64为例）:

```
object
  numpy.generic
    numpy.number
      numpy.integer
        numpy.signedinteger
          numpy.int64
```

可见 np 数组和标量并没有共同的基类，除了 object。

### 过滤

方法一：
```
# arr is a numpy array
# boolean array of which elements to keep, here elements less than 4
mask = arr < 4
# filter the array
arr_filtered = arr[mask]
# above filtering in a single line
arr_filtered = arr[arr < 4]
```

方法二：
```
# arr is a numpy array
# indexes to keep based on the condition, here elements less than 4
indexes_to_keep = np.where(arr < 4)
# filter the array
arr_filtered = arr[indexes_to_keep]
# above filtering in a single line
arr_filtered = arr[np.where(arr < 4)]
```

### 算术

求和
```
a.sum(axis=None)
np.sum(a, axis=None)
```
axis 可以是整数或者组元。不指定时为所有的轴，导致返回标量。

累加（从左到右）：
```
np.cumsum([0,0,1,0,1]) # == array([0, 0, 1, 1, 2])
```

### 最大，最小
一元：
```
np.max(arr, axis=None)
np.min(arr, axis=None)
```
axis 是被求值的轴，可以是整数或者组元，被求值的轴会消失。不指定时为所有的轴，导致返回标量。

二元：
```
max = np.maximum(a1, a2)
min = np.minimum(a1, a2)
```

如果两个操作数形状不一样，先扩展成一样的，再计算。

### 改变轴数

expand_dims 在某个位置增加一个轴，该轴的长度为 1 。例如：

```
np.expand_dims(a, 1) 
```
如果 a 的形状为 (m,n)，则返回的数组形状为 (m,1,n)。

## 排序
numpy.argsort(a, axis=-1)

array_like

    Array to sort.
axisint or None, optional

    Axis along which to sort. The default is -1 (the last axis). If None, the flattened array is used.

如果要降序排列，用 np.flip()。

## 内插
一维分段线性内插：
```
numpy.interp(x, xp, fp, left=None, right=None, period=None)
```
 (xp, fp) 是已知数据点的成对的 x, y 值，x 是输入的自变量，可以是多个。
 left 和 right 是超范围的输出值，默认采用边界值，即 fp[0] 和 fp[-1]。

xp 必须递增，否则会输出无意义的结果，但不会抛异常。验证 xp 递增的方法：`np.all(np.diff(xp) > 0)` 。

## 内积（点积）和模
```
import numpy as np

# 定义两个向量
vector_a = np.array([1, 2, 3])
vector_b = np.array([4, 5, 6])

# 计算点积
dot_product = np.dot(vector_a, vector_b)

# 计算向量的模长
norm_a = np.linalg.norm(vector_a)
norm_b = np.linalg.norm(vector_b)

# 计算余弦相似度
cosine_similarity = dot_product / (norm_a * norm_b)

print("余弦相似度:", cosine_similarity)
```

## 序列化，保存加载

### 文本格式
```
arr = np.loadtxt(file_name, delimiter=',')
np.savetxt(file_name, arr, fmt = '%d,%d,%.2f,%.2f,%.2f,%.2f,%d,%d,%d,%d')
```
可以用 io.StringIO 对象代替 file_name ，从而序列化到字符串中。
```
from io import StringIO
sb = StringIO('1.1,2\n-1,3.1')
arr = np.loadtxt(sb, delimiter=',')
np.savetxt(sb, arr)
sb.getvalue() # '1.1,2\n-1,3.11.100000000000000089e+00 2.000000000000000000e+00\n-1.000000000000000000e+00 3.100000000000000089e+00\n'
```

### JSON 序列化
json.dump(), json.dumps() 序列化时，如果含有 np 对象，一般会抛出异常，但也有例外，那就是 np.float64 可以被正常序列化。因此，如果被 json 序列化的对象会随机混入各种类型的 np 对象，会有随机的异常，这是不好的。
要解决此类问题，最好在json 序列化前，就调用 np_array.tolist() 转换类型，可以用 `isinstance(o, numpy.number) or isinstance(o, numpy.ndarray)` 来鉴别 。

参考： https://pynative.com/python-serialize-numpy-ndarray-into-json/

```
class NumpyArrayEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, numpy.ndarray):
            return obj.tolist()
        return JSONEncoder.default(self, obj)
```

## 随机数
```
# 设置随机数种子
np.random.seed(0)
```

## 多线程控制
参考： https://github.com/numpy/numpy/issues/11826

numpy 的基本运算（通过运算符重载）和大部分库函数并不会利用多线程，不过一部分库函数可以，例如 numpy.linalg 包。后者目前主要靠环境变量控制线程数：
```
OMP_NUM_THREADS=1
OPENBLAS_NUM_THREADS=1
```
numpy 的操作的本地代码一般会释放 GIL，因此 python 多线程代码调用 numpy 是可以一定程度上实现并行计算的；不过，并行度仍可能受到 GIL 的限制。

## 错误处理

### 零除错误

默认状态下，NumPy 对零除输出警告：`RuntimeWarning: divide by zero`，计算结果是 Inf 或 NaN 。
要取消这种警告，可以用 numpy.seterr() 或者 np.errstate context manager [3.1]。

np.errstate ：
```
import numpy as np

arr = np.array([4, 12, 0, 16, 160, 320])

with np.errstate(divide='ignore'):
    print(np.log10(arr))
```
np.seterr ：
```
import numpy as np

arr = np.array([4, 12, 0, 16, 160, 320])

np.seterr(divide='ignore')

print(np.log10(arr))

np.seterr(divide='warn')
```

divide 参数的可选值有 ‘ignore’, ‘warn’, ‘raise’, ‘call’, ‘print’, ‘log’ [3.2]。

### 其他算术错误
np.seterr() 的参数[3.2]：
```
over{‘ignore’, ‘warn’, ‘raise’, ‘call’, ‘print’, ‘log’}, optional

    Treatment for floating-point overflow.
under{‘ignore’, ‘warn’, ‘raise’, ‘call’, ‘print’, ‘log’}, optional

    Treatment for floating-point underflow.
invalid{‘ignore’, ‘warn’, ‘raise’, ‘call’, ‘print’, ‘log’}, optional

    Treatment for invalid floating-point operation.
```

## 性能问题和调优
### 内存布局和性能
连续内存布局的 numpy 张量的性能一般是最好的。所谓连续内存布局是原始的、没有通过视图间接改变内存布局的 numpy 张量。
很多 numpy 操作为了提高速度，默认返回视图，而非直接改变张量的内存布局，例如转置（transpose, moveaxis, swapaxes）。但一些操作可能没有对非连续内存布局做好优化（例如 np.stack），则可能产生巨大的性能损失，这时候先把非连续内存布局张量转换为连续（`b = np.ascontiguousarray()`），再执行后续操作，可以减少性能损失。

在非连续内存布局上有大的性能损失的常见操作：
* np.stack()
* cv2.resize()

性能损失可能随着相关库的版本发生变化，建议通过 cProfile 剖析后决定是否进行优化。

注意 cv2.cvtColor(frame, cv2.COLOR_BGR2RGB) 和 np.swapaxes(frame, (-1, -3)) 尽管逻辑上等效，但前者返回连续内存布局的张量，而后者仅通过视图进行转置，一般返回非连续内存张量。

通过 x.flags 和 x.strides 可以观察张量的内存布局[4.4]。

例子：
```
x = np.arange(6).reshape(2,3)

x.flags
  C_CONTIGUOUS : True # 这表示连续内存布局，C语言标准（行主序）
  F_CONTIGUOUS : False
  OWNDATA : False # False 表示视图
  WRITEABLE : True
  ALIGNED : True
  WRITEBACKIFCOPY : False

x
array([[0, 1, 2],
       [3, 4, 5]])

y = np.transpose(x, [1, 0])
y.shape
(3, 2)
y.flags
  C_CONTIGUOUS : False
  F_CONTIGUOUS : True # 这表示连续内存布局，Fortran 语言标准（列主序）
  OWNDATA : False
  WRITEABLE : True
  ALIGNED : True
  WRITEBACKIFCOPY : False

z = np.ascontiguousarray(y) # 转换为连续内存布局，C 语言标准
z.flags
  C_CONTIGUOUS : True
  F_CONTIGUOUS : False
  OWNDATA : True
  WRITEABLE : True
  ALIGNED : True
  WRITEBACKIFCOPY : False

>>> y2 = np.stack([y,y])
>>> y2.flags
  C_CONTIGUOUS : False
  F_CONTIGUOUS : False
  OWNDATA : True
  WRITEABLE : True
  ALIGNED : True
  WRITEBACKIFCOPY : False

>>> z2 = np.stack([z,z])
>>> z2.flags
  C_CONTIGUOUS : True
  F_CONTIGUOUS : False
  OWNDATA : True
  WRITEABLE : True
  ALIGNED : True
  WRITEBACKIFCOPY : False
```