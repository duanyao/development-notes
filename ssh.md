## 参考资料
[1] 配置手册
http://manpages.ubuntu.com/manpages/trusty/en/man5/sshd_config.5.html

[2] SSH keys
https://wiki.archlinux.org/index.php/SSH_keys

[3] SSH Key - The Forgotten Access Credential
https://www.ssh.com/ssh/key/

[4] Host Key
https://www.ssh.com/ssh/host-key

[5] Effect of entries in /etc/securetty
https://unix.stackexchange.com/questions/41840/effect-of-entries-in-etc-securetty

[6] Why do people advise against using RSA-4096?
https://www.gnupg.org/faq/gnupg-faq.html#please_use_ecc

[7] key exchange methode curve25519-sha256@libssh.org for SSH version 2 protocol.
https://git.libssh.org/projects/libssh.git/tree/doc/curve25519-sha256@libssh.org.txt#n4

[8] EdDSA + Ed25519
https://en.wikipedia.org/wiki/EdDSA

[9] Curve25519
https://en.wikipedia.org/wiki/Curve25519

[10] Nothing up my sleeve number
https://en.wikipedia.org/wiki/Nothing_up_my_sleeve_number

[11] 如何通过反向 SSH 隧道访问 NAT 后面的 Linux 服务器 
https://linux.cn/article-5975-1.html

[12] mozilla Security/Guidelines/OpenSSH
https://wiki.mozilla.org/Security/Guidelines/OpenSSH

[13] Hash-based message authentication code
https://en.wikipedia.org/wiki/Hash-based_message_authentication_code

[14] Guide to Deploying Diffie-Hellman for TLS
https://weakdh.org/sysadmin.html

[15] SSH Key Management – What, Why, How
https://www.ssh.com/iam/ssh-key-management/

[16] [ Issue ] 377322: Nautilus sftp connection breaks spontaneously after a while, needing re-login to fix it
https://freedomsponsors.org/issue/75/nautilus-sftp-connection-breaks-spontaneously-after-a-while-needing-re-login-to-fix-it

[17] 
https://unix.stackexchange.com/questions/34004/how-does-tcp-keepalive-work-in-ssh

[18] SSH PasswordAuthentication vs ChallengeResponseAuthentication
https://blog.tankywoo.com/linux/2013/09/14/ssh-passwordauthentication-vs-challengeresponseauthentication.html

[19] Bug 723274 - Can't handle ED25519 keys gnome-keyring
https://bugzilla.gnome.org/show_bug.cgi?id=723274

[20] Bug 754028 - No support for ed25519 and ecdsa SSH keys seahorse
https://bugzilla.gnome.org/show_bug.cgi?id=754028

[21] Tell SSH to use a graphical prompt for key passphrase
https://unix.stackexchange.com/questions/83986/tell-ssh-to-use-a-graphical-prompt-for-key-passphrase

[25] keychain: Set Up Secure Passwordless SSH Access For Backup Scripts on Linux
https://www.cyberciti.biz/faq/ssh-passwordless-login-with-keychain-for-scripts/

[26] funtoo/keychain
https://github.com/funtoo/keychain

[27] autossh(1) - Linux man page
https://linux.die.net/man/1/autossh

[28] SSH tunnelling for fun and profit: Autossh
https://www.everythingcli.org/ssh-tunnelling-for-fun-and-profit-autossh/

[29] Force ssh client to use only password auth authentication when pubkey auth configured
https://www.cyberciti.biz/faq/howto-force-ssh-client-login-to-use-only-password-authentication/

[30] sshfs mount-a-sftp-connection-to-a-folder-in-ubuntu-linux
https://furick.com/icbd/2017/08/mount-a-sftp-connection-to-a-folder-in-ubuntu-linux/

[31] What are the difference between nfs over ssh and sshfs?
https://superuser.com/questions/991654/what-are-the-difference-between-nfs-over-ssh-and-sshfs

[32] Security and NFS
http://nfs.sourceforge.net/nfs-howto/ar01s06.html

[33] ssh-agent: sometimes SSH_AUTH_SOCK is not set
https://gitlab.gnome.org/GNOME/gnome-keyring/-/issues/11

[34] Remove key from known_hosts
https://superuser.com/questions/30087/remove-key-from-known-hosts


## 安装
debian 包名：openssh-client openssh-server

## 登录策略
### root 用户的登录
在 `/etc/ssh/sshd_config`

```
PermitRootLogin yes|no|prohibit-password|forced-commands-only

 yes 允许任何方式登录。
 prohibit-password 禁止用口令登录（可以用密钥文件登录）。
 no 不许登录。
```
 默认值可以在编译时指定，因此不同的发行版默认值也不同。centos 是 yes，ubuntu 和 alpine 等是 prohibit-password

```
PermitEmptyPasswords yes|no
```

是否允许空口令。默认 no。

```
PasswordAuthentication yes|no
ChallengeResponseAuthentication yes|no
```

两者都控制是否允许口令登录。PasswordAuthentication 是指普通口令，ChallengeResponseAuthentication 是指键盘交互方式的口令，前者可以用自动化工具输入口令，但实际上两者差别不大。
对于 root 账号，PermitRootLogin 和这两者之一要同时为 yes，才可以用口令登录。要完全禁止口令登录，则应把两者都设置为 no 。

注意，/etc/securetty 不影响 ssh root 登录（它主要影响本地 root 登录：物理终端和虚拟终端）。
一个空的 /etc/securetty 文件会阻止大部分本地登录（单用户模式和 sudo 等是例外），但允许 ssh 登录。[5]

### ssh 密钥文件登录

ssh 可以依靠公钥私钥对来实现登录认证。每个远程用户自行生成一对公钥和私钥，私钥对外界保密，且公钥则通过ssh服务器的管理员上传到服务器上。
登录时，远程用户提交自己的id，ssh服务器根据id查找到服务器上的公钥，然后验证该远程用户是否真的持有配对的私钥，如果是，则允许登录，否则不允许。

ssh 将上述公钥称为 Authorized key，而私钥称为 identity key，两者合称 user key。

特点：

* 一个ssh密钥对可以用于登录多台服务器的多个账户。这并不像一个口令用于多个服务器那样有风险：泄露公钥不构成安全问题。
* 私钥文件的保密是客户端防护的重点。
* 及时的管理和吊销过期的公钥是服务器端防护的重点[3]。
* ssh密钥对是自签名的，且永不过期。

identity key 文件一般存储在用户的本地 `.ssh` 目录下，例如 `.id_rsa` 和 `id_ed25519`，其中 rsa 和 ed25519 都是非对称加密算法的名字。

authorized key 一般存储在服务器的用户目录的 `.ssh/authorized_keys` 文件中。

本用户的公钥文件也放在 `~/.ssh/` 目录下，名字是私钥文件名加上 ".pub" 后缀，如 `id_rsa.pub` 和 `id_ed25519.pub`。

`~/.ssh/authorized_keys` 文件中可以包含多个公钥，简单地将新的 ".pub" 文件的内容追加在其末尾即可，每行一个公钥。

`~/.ssh/authorized_keys` 文件应该有 `600` 的访问权限。

除了直接修改 authorized_keys 文件，也可以用 ssh-copy-id 来增加 Authorized key：

```
  ssh-copy-id -i ~/.ssh/id_ed25519.pub username@remote-server.org
```

指定对某些服务器使用某个 user key [12]：

```
# Add configuration to ~/.ssh/config
host *.mozilla.com
IdentityFile ~/.ssh/id_...mozilla... # <= replace by your key's path
```

### 客户端指定登录认证方式

如果服务器允许以多种认证方式登录，则客户端优先尝试秘钥文件登录，如果不行再尝试口令登录。如果只想以口令登录，则执行[29]：

```
ssh -o PreferredAuthentications=password -o PubkeyAuthentication=no user@server
```

### 客户端对服务器端的认证

为了让客户端能确认自己连接的服务器确实是意图连接的服务器，而不是遭到钓鱼或中间人攻击，ssh也提供了客户端对服务器端的认证机制。
这也是利用公钥私钥对（称为 host key）。将公钥私钥放在服务器，在用户首次登录时会被询问是否接受这个服务器，如果接受，会把服务器公钥加入本地用户目录下的
  `.ssh/known_hosts` 文件。

客户端每次登录前，会验证服务器是否真的持有known_hosts中记录的公钥对应的私钥，如果是则继续登录，否则报错。
客户端对服务器端的认证是与 IP地址或域名（主机名）绑定的，如果它们发生变更，则需要重新认证。

通常每个主机应该有独特的 host key。多台主机共享一个 host key 可能导致攻破一台主机影响到其它主机的用户的安全（通过中间人攻击）。不过少数情况下（如集群系统）可能是可以接受的。[4]

host key 的私钥应该周期性的更换。

有的 ssh 服务器支持标准的 X.509 格式的私钥，但 OpenSSH 仅支持一种自定义的私钥格式。

OpenSSH 的 host key 存放在以下文件中：

```
/etc/ssh/ssh_host_ed25519_key
/etc/ssh/ssh_host_ed25519_key.pub
/etc/ssh/ssh_host_ecdsa_key
/etc/ssh/ssh_host_ecdsa_key.pub
/etc/ssh/ssh_host_rsa_key
/etc/ssh/ssh_host_rsa_key.pub
/etc/ssh/ssh_host_dsa_key
/etc/ssh/ssh_host_dsa_key.pub
```

sshd 启动时，如果这些文件不存在，则会新建。
sshd 使用哪些 host key 文件可以通过 /etc/ssh/sshd_config 来配置：

```
HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_dsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key
```

host key 与 user key 的格式相同，都可以用 ssh-keygen 来生成。当然 host key 不应该加口令。

### 密钥的生成和管理

ssh-keygen 工具用来生成密钥对。例如
```
ssh-keygen -t ed25519 -f ~/.ssh/id_ed25519 -C "key for xyz"
```

-t 是加密算法的类型，默认是 rsa，但 ed25519 更安全。-f 是文件位置，默认是 `~/.ssh/id_<type>`。密钥对中，私钥一般是用一个口令来加密的，ssh-keygen 会询问这个口令（空口令则不加密）。

私钥的样例：

```
cat /home/duanyao/.ssh/id_rsa

-----BEGIN RSA PRIVATE KEY-----
Proc-Type: 4,ENCRYPTED
DEK-Info: AES-128-CBC,A31C9ABED8B351CB738A53864E99DF35

tYVV9FYdrG+3nrIVwTOx55TASdA0FUUseRv7WBDpPFo6Z4lTKRF7EvoYuU1dsltM
v/rKtSeb3fW/QZv2w+Et2Du7baKEeVggwZn3PqRDh2IH3KSlyg8ik40RHvC2RLIu
...
7lccE2UD2Qy5nQrwMnKuf3y01NT+tIRqqhxjGRaGCWYP+8GQ/iCqCtTsgOCSnwsy
-----END RSA PRIVATE KEY-----


cat /home/duanyao/.ssh/id_ed25519    
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
...
/zvweyTCWpjP6+xyMY+kAAAAI0R1YW55YW8ncyBrZXkgZm9yIHVjbGFzcy1jYWNoZSBub2
RlAQI=
-----END OPENSSH PRIVATE KEY-----
```

公钥的样例：

```
cat /home/duanyao/.ssh/id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDdMcUNr6jpFwSYFNQGoxL6XwqDVFaWqRFzQG3KIcUJD8aXg5l8rni5LuUi03iFKQsDeMN94gu4ItV6MPcQA8TdOgCy20OtjHdBUjrSZ1x7z7aNPbzj9mgabZ4TqltS7cJUbBz6SWNJyHVzqRxwdxZbiTdkmp9Hcu4Zxq9KGtdIqvywYgBsnl+bBWp9sStuYKs8gZ7Jb708v0WDwM0NT2KS7EBIC2EWqwVVaExHLOhN4y+z1i45efxYPT/exhZSHW9OmNoAMIOl3E7tjl3kr9ZIlYJ1n5ESL8ZDpqa5A8ZFrzU82VV8+zB/ygQ7CsfhhyCd/G2/8IwZQ8XPaWRxS8s/ duanyao@ustc.edu

cat /home/duanyao/.ssh/id_ed25519_uclass_cache_2017-05-15.pub
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJkZ7N4OPUviHLrXsg9w6BKD/zvweyTCWpjP6+xyMY+k Duanyao's key for uclass-cache node
```

事后也可以更改私钥的口令（空口令清除口令）：
```
 ssh-keygen -f ~/.ssh/id_rsa -p
```

显示密钥信息：

```
$ ssh-keygen ~/.ssh/id_rsa
2048 bc:4f:46:2b:3d:f1:e2:0f:ac:40:99:49:ed:c9:81:a2 Mozilla key for xyz (RSA)
^^   ^^^^^^^^^                                       ^^^^                 ^^^
|__ Size     |__ Fingerprint                             |__ Comment        |__ Type
```

## 记忆私钥的口令

### 加入 ssh-agent 的缓存
ssh-add ~/.ssh/id_rsa
大部分linux 发行版默认启用了 ssh-agent；通过 ssh-add 将私钥加入缓存，可以避免每次输入口令。

不带参数运行 ssh-add 会加入所有识别到的私钥。

ssh-add -l 可以查看已经加入的私钥。

### 避免使用 ssh-agent 缓存
清除掉 `SSH_AUTH_SOCK` 环境变量即可，这是 ssh-agent 的监听端口。
```
$ export SSH_AUTH_SOCK=
$ ssh user@host
```
### keychain
[25,26]
ssh-add 不是永久的，注销后就失效了。要永久有效，可以安装 keychain：
sudo apt-get install keychain

在 ~/.profile 中加入
```
keychain id_dsa id_ed25519
source $HOME/.keychain/$HOSTNAME-sh
```

第一行也可以写作 `keychain $HOME/.ssh/id_dsa $HOME/.ssh/id_ed25519`。如果私钥是加密的，这一步将会询问密码。

keychain 会启动一个 ssh-agent 进程，并把 `SSH_AUTH_SOCK` 指向它，这些在 `$HOME/.keychain/$HOSTNAME-sh` 文件里指定。

但是，keychain 与 deepin linux 桌面协调不好，登录时不会自动解锁，而且不会提示输入密码，因此会失败。

### 让 ssh-agent 随着桌面启动
配置文件 /etc/X11/Xsession.options

```
# ssh-agent 随着桌面启动
use-ssh-agent
# 禁止 ssh-agent 随着桌面启动
no-use-ssh-agent 
```
注意，如果装了 gnome-keyring（很多桌面系统预装），就应该在 /etc/X11/Xsession.options 中设置 no-use-ssh-agent ，两者不能同时生效（见“ssh-agent 协议”），甚至有冲突。

### gnome-keyring
包名 gnome-keyring 和 gnome-keyring-pkcs11。一般是默认安装、自动启动的，可以取代 ssh-agent 的作用。

它存储密码的文件是 /home/duanyao/.local/share/keyrings/* ,删除可重置。

直到 3.28 才开始支持 ed25519 加密算法。如果低于此版本，ed25519 私钥就无法实现自动解锁。

gnome-keyring 配置好以后，登录桌面后就应该自动加载 ssh 私钥了，可以用 ssh-add -l 查看。

gnome-keyring 的自启动方式有多种：

#### PAM method
例如 deepin 中，
```
$ grep -Irn gnome_keyring /etc/

/etc/pam.d/common-password:33:password	optional	pam_gnome_keyring.so 
/etc/pam.d/lightdm-greeter:3:auth    optional        pam_gnome_keyring.so
/etc/pam.d/lightdm-greeter:11:session optional        pam_gnome_keyring.so auto_start
/etc/pam.d/lightdm:5:auth    optional        pam_gnome_keyring.so
/etc/pam.d/lightdm:13:session optional        pam_gnome_keyring.so auto_start
```
这些文件属于 lightdm 包。

而 /lib/x86_64-linux-gnu/security/pam_gnome_keyring.so 属于包 libpam-gnome-keyring 。

#### autostart .desktop 

$ ls /etc/xdg/autostart/gnome-keyring*
/etc/xdg/autostart/gnome-keyring-pkcs11.desktop  /etc/xdg/autostart/gnome-keyring-secrets.desktop  /etc/xdg/autostart/gnome-keyring-ssh.desktop

这些文件里都有 `OnlyShowIn=GNOME;Unity;MATE;` 选项，并不会在 deepin 中自动启动。

如果要自动启动他们，可以复制到 ~/.config/autostart 目录下，并去掉 `OnlyShowIn=GNOME;Unity;MATE;` ；不过，如果有了 /etc/pam.d/ 中的设置，就没必要这样做。

#### 查看日志

```
journalctl --user -b | grep keyring
```

#### systemd 系统服务

ubuntu 20.04 的服务为 gnome-keyring-ssh 。


ubuntu 23.10 的服务为：
```
  app-gnome-gnome\x2dkeyring\x2dpkcs11-11352.scope                                               loaded active     running      Application launched by gnome-session-binary
  gnome-keyring-daemon.service                                                                   loaded active     running      GNOME Keyring daemon
  gnome-keyring-daemon.socket                                                                    loaded active     running      GNOME Keyring daemon
```

#### 私钥未解密的错误
如果 ssh 客户端登录远程主机时报告错误
```
sign_and_send_pubkey: signing failed: agent refused operation
```
而 ssh-add -l 查看到私钥已经加入，那么可能是私钥虽然已经加入 ssh-agent ，但是还没有解密。这时应该重新运行 ssh-add ，输入密码解密。
错误的原因可能是：
（1）私钥文件没有设置正确的权限。正确的权限应该是：本人有 rw，其它人没有任何权限。
（2）如果私钥有密码，且用 gnome-keyring 来自动解锁私钥，则可能是 gnome-keyring 版本太低，不支持某些新的私钥格式。

#### SSH_AUTH_SOCK 未被设置的 bug
gnome-keyring 应该在用户登录时设置 SSH_AUTH_SOCK，一般是 `/run/user/1000/keyring/ssh` 1000 是当前用户id。
但因为未知的原因，有时候 gnome-keyring 并未设置 SSH_AUTH_SOCK ，这造成 ssh-add 会报错 `Could not open a connection to your authentication agent.`，以及 ssh 总是询问密码。
这时，手动设置 SSH_AUTH_SOCK 可以解决问题：
```
export SSH_AUTH_SOCK=/run/user/$(id -u)/keyring/ssh # id -u 显示当前用户id
```
可以将上面的行加到 .profile 或 .bashrc 中，作为临时解决方案。

参考 gnome bug：https://gitlab.gnome.org/GNOME/gnome-keyring/-/issues/11

### seahorse
seahorse 是 gnome-keyring 的 GUI 前端。
不过，seahorse 3.20 似乎仍然不支持 ed25519 [20]，需要更新的版本（3.28+？）。

正常来说，seahorse 应该自动识别和解锁 ~/.ssh 下的秘钥。

也可以用命令 /usr/local/libexec/seahorse/ssh-askpass id_ed25519 来解锁一个ssh秘钥，这应该永久记住passphrase。

### gnome-keyring + seahorse 的 bug
在 deepin 15.10.1 上，gnome-keyring + seahorse 似乎不能正确解锁 id_ed25519 秘钥，
版本是 
gnome-keyring: 3.28.2-1 从 panda deb 源。
seahorse: 3.30.1.1 从源码编译。

seahorse 中 openssh keys 下能看到 id_ed25519 秘钥，ssh-add -l 也能看到 id_ed25519 秘钥，但是并不能用来ssh登录，而是会报错：

```
sign_and_send_pubkey: signing failed: agent refused operation
Permission denied (publickey,gssapi-keyex,gssapi-with-mic).
```

用命令 /usr/local/libexec/seahorse/ssh-askpass id_ed25519 解锁秘钥无效，还是一样的错误。

用户日志里可以看到登录时这样的错误：
```
n月 11 10:15:43 duanyao-laptop-c gcr-prompter[7351]: bus acquired: org.gnome.keyring.SystemPrompter
6月 11 10:15:43 duanyao-laptop-c gcr-prompter[7351]: Gcr: registering prompter
6月 11 10:15:43 duanyao-laptop-c gcr-prompter[7351]: bus acquired: org.gnome.keyring.PrivatePrompter
6月 11 10:15:43 duanyao-laptop-c gcr-prompter[7351]: Gcr: received BeginPrompting call from callback /org/gnome/keyring/Prompt/p9@:1.9
6月 11 10:15:43 duanyao-laptop-c gcr-prompter[7351]: Gcr: preparing a prompt for callback /org/gnome/keyring/Prompt/p9@:1.9
6月 11 10:15:43 duanyao-laptop-c gcr-prompter[7351]: Gcr: creating new GcrPromptDialog prompt
6月 11 10:15:43 duanyao-laptop-c gcr-prompter[7351]: fcitx-connection: _fcitx_connection_create_ic
6月 11 10:15:43 duanyao-laptop-c gcr-prompter[7351]: Gcr: automatically selecting secret exchange protocol
6月 11 10:15:43 duanyao-laptop-c gcr-prompter[7351]: Gcr: generating public key
6月 11 10:15:43 duanyao-laptop-c gcr-prompter[7351]: Gcr: beginning the secret exchange: [sx-aes-1]\npublic=FyIQr4xhppg7jGGr08dOWLonhZyEa9kIXV/ceZnhw9kcOmTFRZoj6VAqCNkjwTyDTo6月 11 10:15:43 duanyao-laptop-c gcr-prompter[7351]: Gcr: calling the PromptReady method on /org/gnome/keyring/Prompt/p9@:1.9
6月 11 10:15:43 duanyao-laptop-c gcr-prompter[7351]: acquired name: org.gnome.keyring.SystemPrompter
6月 11 10:15:43 duanyao-laptop-c gcr-prompter[7351]: acquired name: org.gnome.keyring.PrivatePrompter
6月 11 10:15:43 duanyao-laptop-c gcr-prompter[7351]: fcitx-connection: _fcitx_connection_connection_finished
6月 11 10:15:43 duanyao-laptop-c gcr-prompter[7351]: Gcr: returned from the PromptReady method on /org/gnome/keyring/Prompt/p9@:1.9
6月 11 10:15:43 duanyao-laptop-c gcr-prompter[7351]: Gcr: received PerformPrompt call from callback /org/gnome/keyring/Prompt/p9@:1.9
6月 11 10:15:43 duanyao-laptop-c gcr-prompter[7351]: Gcr: receiving secret exchange: [sx-aes-1]\npublic=8yNZgWcgzl3/i9UgMWTC4zUl/9+LWy5MxMywY0m5AVVDAzBiMlz1W2Db1hR8Q7NkhdQ1HAv月 11 10:15:43 duanyao-laptop-c gcr-prompter[7351]: Gcr: deriving shared transport key
6月 11 10:15:43 duanyao-laptop-c gcr-prompter[7351]: Gcr: deriving transport key
6月 11 10:15:43 duanyao-laptop-c gcr-prompter[7351]: Gcr: starting password prompt for callback /org/gnome/keyring/Prompt/p9@:1.9
6月 11 10:15:43 duanyao-laptop-c gcr-prompter[7351]: GtkDialog mapped without a transient parent. This is discouraged.
6月 11 10:15:44 duanyao-laptop-c daemon/dock[1884]: app_entry.go:225: attach window id: 111149064, wmClass: "Gcr-prompter" "gcr-prompter", wmState: [359 365], wmWindowType: [3月 11 10:16:01 duanyao-laptop-c gcr-prompter[7351]: Gcr: completed password prompt for callback :1.9@/org/gnome/keyring/Prompt/p9
6月 11 10:16:01 duanyao-laptop-c gcr-prompter[7351]: Gcr: encrypting data
6月 11 10:16:01 duanyao-laptop-c gcr-prompter[7351]: Gcr: sending the secret exchange: [sx-aes-1]\npublic=FyIQr4xhppg7jGGr08dOWLonhZyEa9kIXV/ceZnhw9kcOmTFRZoj6VAqCNkjwTyDTo6SX月 11 10:16:01 duanyao-laptop-c gcr-prompter[7351]: Gcr: calling the PromptReady method on /org/gnome/keyring/Prompt/p9@:1.9
6月 11 10:16:01 duanyao-laptop-c gcr-prompter[7351]: Gcr: returned from the PromptReady method on /org/gnome/keyring/Prompt/p9@:1.9
6月 11 10:16:01 duanyao-laptop-c gcr-prompter[7351]: Gcr: received PerformPrompt call from callback /org/gnome/keyring/Prompt/p9@:1.9
6月 11 10:16:01 duanyao-laptop-c gcr-prompter[7351]: Gcr: stopping prompting for operation /org/gnome/keyring/Prompt/p9@:1.9
6月 11 10:16:01 duanyao-laptop-c gcr-prompter[7351]: Gcr: closing the prompt
6月 11 10:16:01 duanyao-laptop-c gcr-prompter[7351]: Gcr: stopping prompting for operation /org/gnome/keyring/Prompt/p9@:1.9
6月 11 10:16:01 duanyao-laptop-c gcr-prompter[7351]: Gcr: couldn't find the callback for prompting operation /org/gnome/keyring/Prompt/p9@:1.9
6月 11 10:16:01 duanyao-laptop-c gcr-prompter[7351]: Gcr: stopping prompting for operation /org/gnome/keyring/Prompt/p9@:1.9
6月 11 10:16:01 duanyao-laptop-c gcr-prompter[7351]: Gcr: couldn't find the callback for prompting operation /org/gnome/keyring/Prompt/p9@:1.9
6月 11 10:16:01 duanyao-laptop-c gcr-prompter[7351]: Gcr: stopping prompting for operation /org/gnome/keyring/Prompt/p9@:1.9
6月 11 10:16:01 duanyao-laptop-c gcr-prompter[7351]: Gcr: couldn't find the callback for prompting operation /org/gnome/keyring/Prompt/p9@:1.9
6月 11 10:16:01 duanyao-laptop-c gcr-prompter[7351]: Gcr: calling the PromptDone method on /org/gnome/keyring/Prompt/p9@:1.9, and ignoring reply
6月 11 10:16:01 duanyao-laptop-c daemon/dock[1884]: dock_manager_entries.go:144: removeAppEntry id: e11T5cff0ed0

```

用 ssh-add （不带参数）解锁秘钥则有效。

### ssh-agent 协议
ssh-agent 不仅是个程序，也是个协议。也就是说，其它程序也可以实现这个协议，取代 ssh-agent 的工作。
符合 ssh-agent 协议的程序监听由环境变量 SSH_AUTH_SOCK 定义的 domain socket 。
一个典型的例子就是 gnome-keyring (/usr/bin/gnome-keyring-daemon) ，安装它以后，就取代了 ssh-agent（手动运行 ssh-agent 也无法工作）。
gnome-keyring 在用户登录时启动，并设置 `SSH_AUTH_SOCK` 指向自己。

查看 `SSH_AUTH_SOCK`
```
$ echo $SSH_AUTH_SOCK
/run/user/1000/keyring/ssh

$ sudo netstat -lp | grep /run/user/1000/keyring/ssh
unix  2      [ ACC ]     STREAM     LISTENING     3683795  21685/gnome-keyring  /run/user/1000/keyring/ssh

$ ps -aux | grep 21685
duanyao  21685  0.0  0.1 428272  8228 ?        Sl   11:19   0:00 /usr/bin/gnome-keyring-daemon --daemonize --login
```

这时候，ssh-add 以及 ssh 客户端实际上是与 gnome-keyring-daemon 打交道的。

### ssh-askpass
https://packages.debian.org/sid/ssh-askpass
/usr/bin/ssh-askpass 程序是用来在 ssh-add 需要输入密码时提供 GUI 的密码对话框。这个程序是个符号链接，其实有多种实现，包括包
ssh-askpass，ksshaskpass，kwalletcli，lxqt-openssh-askpass，ssh-askpass-gnome 等。

ssh-askpass 只在 ssh（以及 ssh-add）客户端不在控制台中运行、并且在 X 中运行，并且 `SSH_ASKPASS` 环境变量指向一个 ssh-askpass 程序时才会起作用[21]，不满足时，仍然从控制台读取密码。

`SSH_ASKPASS` 似乎一般不会被软件包自动设置，需要手动添加到 `~/.profile` 中。

用命令 `ssh-add > /dev/null < /dev/null 2>&1` 可以在关闭控制台的情况下运行 ssh-add，模拟上述情形[21]。

ssh-askpass 并不像 `SSH_AUTH_SOCK` 那样是必需的。
deepin 15.10.1 并没有预装普通的 ssh-askpass 程序。但有一个类似的程序是 gcr (GNOME crypto services)，它提供的 /usr/lib/gcr/gcr-ssh-askpass 和 /usr/lib/gcr/gcr-prompter 会被 gnome-keyring 使用来询问私钥的解锁密码。

#### ubuntu systemd --user 和 ssh-askpass
在 ubuntu 20.04 中，如果 systemd --user 服务中运行 ssh 客户端，如果对方服务器需要密码、需要确认服务器的 fingerprint 时，也会弹出 GUI 的密码对话框，对话框程序是 /usr/bin/ssh-askpass 。
ubuntu 20.04 中，/usr/bin/ssh-askpass 指向 /etc/alternatives/ssh-askpass，指向 /usr/lib/ssh/x11-ssh-askpass 。x11-ssh-askpass 属于包 ssh-askpass 。

如果 systemd --user 服务设置了自动重启，则可能不断地弹出 ssh-askpass 对话框。如果启动了 vnc 会话，还可能在 vnc 桌面中不断弹出 ssh-askpass 对话框。
解决办法：
 1. （未验证）在 systemd --user 服务中取消 `SSH_ASKPASS` 环境变量（但 ubuntu 20.04 默认并没有设置 `SSH_ASKPASS` ）。
 2. 卸载包 ssh-askpass 。这是可行的。
 3. 避免 systemd --user 重复启动。

### deepin workaround gnome-keyring + ssh-askpass-gnome + ssh-add
为了解决 deepin 中 gnome-keyring 无法自动解锁私钥的问题：
（1）安装 gnome-keyring 3.28 以上
（2）安装 ssh-askpass-gnome（提供询问密码对话框程序 ssh-askpass）
（3）在 ~/.profile 里增加一行 `export SSH_ASKPASS=/usr/bin/ssh-askpass`
（4）创建 autostart 脚本（如 `~/.config/autostart/ssh-add.desktop`），内容是：
    ```
    [Desktop Entry]
    Categories=Network;
    Comment=ssh-add gui
    Exec=ssh-add
    Name=ssh-add gui
    Terminal=false
    Type=Application
    ```

效果是：GUI登录后，运行 ssh-add，后者弹出对话框询问私钥的密码，通过后即将私钥加入 ssh-agent（gnome-keyring）。
之后，终端和nautilus的sftp都都可以免密码登录了。

###  deepin workaround 2
升级 gnome-keyring libpam-gnome-keyring gcr (GNOME crypto services) 到 3.28 以上 ：
sudo aptitude install -t panda gnome-keyring libpam-gnome-keyring gcr
删除 gnome-keyring 数据：
rm /home/duanyao/.local/share/keyrings/*

注销，登录，访问一个 ssh 主机，应该会弹出对话框“输入密码以解锁私钥”。输入私钥的密码，并可以选中“每当我登录时自动解锁该秘钥”。这样以后登录后都不用输入密码解锁私钥了。

如果有什么错误，通过 `journalctl --user --since hh:mm | grep -i keyring` 查看错误。

## 密码学算法的选择

### 密钥算法的选择
有2类选择：
* DSA 和 RSA。都是基于分解质因数的算法。
* ECDSA 和 Ed25519[8]。都是基于椭圆曲线离散对数的算法。

其中 DSA 已经被发现明显的弱点，因此不应该再使用。Ed25519 可能是最安全的，但是因为比较新，兼容性较差。
ECDSA 被怀疑有弱点：NSA可能在它使用的椭圆曲线常数中植入后门[10]；Ed25519没有这个问题，因为不使用硬编码的常数[7]。

ssh-keygen 默认采用 2048 位 RSA 密钥。随着密钥位数的进一步增长，RSA 的安全性只是缓慢地增长，因此有些人认为，如果需要显著优于 2048 位 RSA的安全性，
应该改用椭圆曲线算法[6]。

生成 ed25519 密钥：
  `ssh-keygen -t ed25519`
无需指定密钥大小，它总是256位的。

### 消息鉴证码算法的选择
openssh 支持的算法有：
```
 hmac-md5-etm@openssh.com,hmac-sha1-etm@openssh.com,
 umac-64-etm@openssh.com,umac-128-etm@openssh.com,
 hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com
```
等等。可以用 

```
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com
```

指令配置。

注意，hmac 比其使用的散列算法本身安全性要高，因此 HMAC-MD5 和 HMAC-SHA1 仍然可以使用[13]。

### 对称加密算法的选择
```
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
```

### 密钥交换算法的选择
用 KexAlgorithms 指令配置，如

```
KexAlgorithms curve25519-sha256@libssh.org,ecdh-sha2-nistp521,ecdh-sha2-nistp384,ecdh-sha2-nistp256,diffie-hellman-group-exchange-sha256
```

/etc/moduli or /etc/ssh/moduli
  Contains Diffie-Hellman groups used for DH-GEX. The file format is described in moduli(5).

默认的 diffie-hellman-group-exchange （1024位）不够安全[14]。要么强制使用 curve25519：
```
  KexAlgorithms curve25519-sha256@libssh.org
```
要么重新生成 2048 位Diffie-Hellman groups[14]：

```
ssh-keygen -G moduli-2048.candidates -b 2048
ssh-keygen -T moduli-2048 -f moduli-2048.candidates
```

You then need to install moduli-2048 to your system's moduli file. In Debian/Ubuntu, this is located at /etc/ssh/moduli. SSH chooses (practically randomly) from this file, so you should replace your existing moduli file with the new groups you generated instead of appending these new groups. 

## 客户端

### openssh ssh
* -v 参数可以显示调试信息，包括使用的登录方法、密码学算法等。

* 非交互执行命令

  在行内指定命令：
  ```
  ssh user@host cmd arg1 arg2
  ssh user@host "cmd1 'arg 1' > file1" # 可以嵌套引号，引用带空格的参数
  ssh user@host "cmd1 arg1; cmd2 arg2"
  ssh user@host "cmd1 arg1 && cmd2 arg2"
  ```
  注意行 ssh 内命令不要用 sh -c 或 bash -c 执行，因为这会导致语法错误。
  上传一个脚本再执行：
  ```
  scp batch-file user@host
  ssh user@host ./batch-file
  ssh user@host rm batch-file
  ```
* 命令行中指定 Known Hosts 文件 
 ```
 ssh -o UserKnownHostsFile=path_to_known_hosts ...
 ```
 
* 命令行中指定私钥文件

 ```
 ssh -i path_to_private_key ...
 ```
 注意，并没有在命令行中指定私钥文件的口令的方法。所以如果用在非交互脚本里时，私钥文件应该是不加密的。
 
### openssh scp
scp 用来跨网络复制文件和目录。

```
scp [-12346BCpqrv] [-c cipher] [-F ssh_config] [-i identity_file] [-l limit] [-o ssh_option] [-P port] [-S program] [[user@]host1:]file1 ...
         [[user@]host2:]file2

-r 递归复制目录。注意 scp 总是跟踪软连接并复制实际内容。
-q 不显示进度。
-C 压缩
```

例子：
```
scp -r foo root@192.168.0.68:/var/www/
```
如果 foo 是目录，会形成 /var/www/foo 目录。

### openssh sftp
sftp 类似 ftp，但是通过 ssh 加密。可以用 FileZilla 客户端来连接 sftp 服务器。

### 配置客户端/服务器端超时
nautilus 使用 sftp 的时候（通过 gvfs），容易出现 TCP 超时问题，导致浏览目录时卡住不动。

解决办法[16,17]：

客户端配置：文件 /etc/ssh/ssh_config 或者 ~/.ssh/config

```
KeepAlive yes
TCPKeepAlive yes
ServerAliveInterval 15 # 默认值 0
ServerAliveCountMax 3 # 默认值 3
```
ssh 连接上没有活动时，客户端每隔 ServerAliveInterval 秒发送 keep-alive 消息；如果连续 3 次（默认值，由 ServerAliveCountMax 控制）收不到回复，则认为此连接已死，关闭此连接和会话。因此以上配置中检测到死连接的最小时间是 15 * 3 = 45 秒。
如果设置 ServerAliveInterval = 0，则表示禁用客户端 keep-alive 功能，认为连接永不超时。

服务器端配置：/etc/ssh/sshd_config
```
ClientAliveInterval 30 # 默认值 0
ClientAliveCountMax 3 # 默认值 3
TCPKeepAlive yes
```
ssh 连接上没有活动时，服务端每隔 ClientAliveInterval 秒发送 keep-alive 消息；如果连续 3 次（默认值，由 ClientAliveCountMax 控制）收不到回复，则认为此连接已死，关闭此连接和会话。因此以上配置中检测到死连接的最小时间是 30 * 3 = 90 秒。
如果设置 ClientAliveInterval = 0，则表示禁用服务器端 keep-alive 功能，认为连接永不超时。

另一方面，如果因为网络问题，服务器不可达，ssh 客户端连接时可能要很久（数分钟）才报错。为了节省时间，可以：
```
ssh -o ConnectTimeout=2 user@host
```
这样如果2秒内连不上就报错。
ConnectTimeout 也可以写入客户端配置文件。

### 删除过时的/错误的 known_hosts
有时可能看到以下警告/错误：
```
Warning: the ECDSA host key for 'ydjmp.w' differs from the key for the IP address '36.138.103.14'
Offending key for IP in /home/rd/.ssh/known_hosts:21
Matching host key in /home/rd/.ssh/known_hosts:22
Are you sure you want to continue connecting (yes/no)?
```
可以这样删除有问题的 known_hosts [34]：
```
ssh-keygen -R 36.138.103.14
# 如果需要指定非标准端口，非标准 known_hosts 路径：
ssh-keygen -f ~/.ssh/known_hosts -R '[192.168.23.2]:4022'
```

## 端口映射/ socks 代理

通过下面的命令我们可以建立一个通过123.123.123.123的SOCKS服务器 127.0.0.1:1080。

ssh -N -f -D 1080 123.123.123 # 将端口绑定在127.0.0.1上
ssh -N -f -D 0.0.0.0:1080 123.123.123.123 # 将端口绑定在0.0.0.0上

## 端口转发 (port forwarding / port mapping)

通过跳板机访问远程 MySQL 服务器：

```
ssh -p22 -CNg -L 3307:rds1r2cp34235345236.mysql.rds.aliyuncs.com:3306 root@middle-host
```

middle-host 是跳板机，rds1r2cp34235345236.mysql.rds.aliyuncs.com 是远程 MySQL 服务器，
 3306 是远程 MySQL 服务器的端口，3307是映射到本地的端口，22是ssh的端口（默认，可省略）。

如果本地的 3306 端口没有被占用，也可以把 3307 改为 3306。

也可以使用多个 -L 参数，一次完成多个端口的映射：
```
ssh -p22 -N -L 1099:host1:1099 -L 9000:host1:9000 root@middle-host
```

## 跳板机
```
ssh -J root@proxyhost root@target
```
proxyhost 为跳板机地址。
注意，在使用密钥登录时，上述命令并不等效于
```
ssh root@proxyhost
# 在 proxyhost 上：
ssh root@target
```
前者会使用本机上的 ssh 密钥同时登录 proxyhost 和 target ；而后者使用本机上的 ssh 密钥登录 proxyhost，而使用 root@proxyhost 的密钥登录 root@target 。这一点无法改变。
因此，使用前者时，应该把本机的公钥同时加入 root@proxyhost 和 root@target 。

也可以写到 `.ssh/config` 中：
```
Host ydjmp.w
  HostName ydjmp.w
  User root

Host qlygpu01
  HostName 192.168.0.74
  User root
  ProxyJump ydjmp.w
```

然后用 `ssh qlygpu01` 连接。

## 自动重连

如果网络不稳定，端口转发中的 ssh 进程可能会报错退出，例如“Timeout, server xxx not responding” 。
为了解决这个问题，可以安装 autossh（debian包名autossh），然后用 autossh 命令取代 ssh 命令。[27,28]

```
autossh -p22 -CNg -L 3307:rds1r2cp34235345236.mysql.rds.aliyuncs.com:3306 root@middle-host
```

## 网络设置（端口、IPv6）

### 指定服务器端口

允许指定多个端口，同时监听：

```
Port 22
Port 5432
```

### 指定协议族
```
AddressFamily any # 或者 inet, inet6
```
### 指定监听的IP地址
默认监听所有 IPv4 或 IPv6 地址。

```
#ListenAddress 0.0.0.0
#ListenAddress ::
```

## 挂载文件系统

### gvfs 

linux GUI 文件管理器通常利用 gvfs 系统挂载 ssh/sftp 服务器的文件系统。真实挂载目录形如：
```
/run/user/1000/gvfs/sftp:host=gtrd.local,user=rd/
```
其中 1000 是本地用户 id ，host 是远程主机，user 是远程用户名。
有的文件管理器如 nautilus 不显示，也不能复制 gvfs 挂载的文件的真实路径，有的（dde-file-manager）则可以。

gvfs 挂载的 ssh/sftp 文件系统有一些怪癖，例如 git clone 会失败，报告错误：

```
fatal: write error: 不支持的操作
fatal: index-pack 失败
```

也可以用命令行挂载

```
gvfs-mount ssh://your-server/
```

### sshfs

[30]
```
sudo apt-get install sshfs
sudo sshfs -o allow_other -p 6789 user@xxx.xx.xxx.xx:/ /mnt/sshftps

sshfs -o max_conns=4 -o direct_io -o no_readahead -o Ciphers=chacha20-poly1305@openssh.com -o Compression=no root@192.168.0.8:/safe_share /media/sshfs/safe_share
```

-o max_conns=4 允许每个挂载点使用4个 ssh 连接。由于 ssh 对每个连接只能使用一个 CPU 核心，多连接可以提高吞吐量。
-o Ciphers=chacha20-poly1305@openssh.com -o Compression=no是ssh客户端的设置，选择更节省CPU的算法，一般也是ssh的默认设置。

### nfs4 over a ssh

[31-32]

nfs 比 sshfs 功能更完备，但也有一些缺点。
* 默认状态下 nfs 假定网络是持久连接的，如果网络暂时断开，它会冻结挂载的文件系统（导致使用它的程序失去响应），直到网络恢复。
因此，对于非持久网络环境，应当使用 nfs 的 soft 选项，在网络断开时快速失败。
* nfs 协议不加密。这可以通过 ssh 隧道来改善。


