## 参考资料
[1.1] An Informal Introduction to Python
https://docs.python.org/3/tutorial/introduction.html

[1.10] How does python find packages?
https://leemendelowitz.github.io/blog/how-does-python-find-packages.html

[1.11] Command line and environment
https://docs.python.org/3/using/cmdline.html#environment-variables

[1.12] Where is Python's sys.path initialized from?
https://stackoverflow.com/questions/897792/where-is-pythons-sys-path-initialized-from

[1.13] What's the difference between dist-packages and site-packages?
https://stackoverflow.com/questions/9387928/whats-the-difference-between-dist-packages-and-site-packages

[1.14] How do I find the location of my Python site-packages directory?
https://stackoverflow.com/questions/122327/how-do-i-find-the-location-of-my-python-site-packages-directory

[1.15] python学习教程-《Python从入门到精通》
http://www.magedu.com/73198.html?Python_wenzhang_zhihu_jinke_yipianwenzhangzhangwonumpydejibenyongfa_29791319

[2.1] A non-magical introduction to Pip and Virtualenv for Python beginners
https://www.dabapps.com/blog/introduction-to-pip-and-virtualenv-python/

[2.2]
https://stackoverflow.com/questions/6587507/how-to-install-pip-with-python-3

[2.3] 更改pip源至国内镜像，显著提升下载速度
https://blog.csdn.net/lambert310/article/details/52412059

[2.4] List all the packages, modules installed in python pip
http://www.datasciencemadesimple.com/list-packages-modules-installed-python/

[2.5] The Wheel Binary Package Format 1.0
https://www.python.org/dev/peps/pep-0427/#file-name-convention

[2.6] Python - cant find pip.ini or pip.conf in Windows
https://stackoverflow.com/questions/28278207/python-cant-find-pip-ini-or-pip-conf-in-windows

[2.7] Error: Invalid Command Bdist_wheel in Python
https://www.delftstack.com/howto/python/python-bdist_wheel/

[2.8] ensurepip is disabled in Debian/Ubuntu for the system python
https://askubuntu.com/questions/879437/ensurepip-is-disabled-in-debian-ubuntu-for-the-system-python

[2.9] pip installation
https://pip.pypa.io/en/stable/installation/

[3.1] What is the difference between venv, pyvenv, pyenv, virtualenv, virtualenvwrapper, pipenv, etc?
https://stackoverflow.com/questions/41573587/what-is-the-difference-between-venv-pyvenv-pyenv-virtualenv-virtualenvwrappe#

[3.2] pip install --user misbehaves inside venv
https://github.com/pypa/pip/issues/5702

[3.3] Top 6 Ways to List Pip Dependencies without Installing
https://sqlpey.com/python/top-6-ways-to-list-pip-dependencies/


[4.1] Virtual Environments and Packages
https://docs.python.org/3/tutorial/venv.html

[5.1] pipx
https://pipx.pypa.io/

[6.1] Using Python environments in VS Code
https://code.visualstudio.com/docs/python/environments

[7.1] What does the 'm' in a Python ABI tag mean?
https://stackoverflow.com/questions/54097033/what-does-the-m-in-a-python-abi-tag-mean

[7.2] The "m" ABI flag of SOABI for pymalloc is no longer needed
https://bugs.python.org/issue36707

[8.1] PyPy’s documentation
https://doc.pypy.org/en/latest/index.html

[8.2] PyPy Is Faster than Python, but at What Cost?
https://towardsdatascience.com/pypy-is-faster-than-python-but-at-what-cost-12739bf2b8e9

[8.3] PyPy's Python packages compatibility
http://packages.pypy.org/

[9.1] Pyjion drop-in JIT compiler for CPython 3.9
https://pyjion.readthedocs.io/en/latest/

[9.2] 在 Linux 上安装 .NET
https://docs.microsoft.com/zh-cn/dotnet/core/install/linux?WT.mc_id=dotnet-35129-website

[10.1] pyston JIT CPython
https://github.com/pyston/pyston
https://github.com/pyston/pyston/releases

[11.1] Numba is an open source JIT compiler that translates a subset of Python and NumPy code into fast machine code. 
  https://numba.pydata.org/

[11.2] numba，让python速度提升百倍
  https://www.zhihu.com/tardis/zm/art/78882641?source_id=1005
  
[11.3] Python 提速大杀器之 numba 篇
  https://zhuanlan.zhihu.com/p/454057229
  
[11.4] Seven Things You Might Not Know about Numba
  https://developer.nvidia.com/blog/seven-things-numba/

[12.1] The Python Profilers
https://docs.python.org/3/library/profile.html

[12.2] Profiling and debugging Python
https://docs.nersc.gov/development/languages/python/profiling-debugging-python/

[12.3] Python性能分析 (Profiling)
http://linux-wiki.cn/wiki/Python%E6%80%A7%E8%83%BD%E5%88%86%E6%9E%90_(Profiling)

[12.4] snakeviz
https://jiffyclub.github.io/snakeviz/

[12.5] mtprof
https://github.com/dask/mtprof

[12.6] Yappi
https://github.com/sumerc/yappi

[13.1] python的pyc文件详细理解
https://www.cnblogs.com/chentiao/p/17078917.html

[14.1] SyntaxError: multiple statements found while compiling a single statement
https://bobbyhadz.com/blog/syntax-error-multiple-statements-found-while-compiling-a-single-statement

[20.1] Importing cv2 prevents matplotlib from working
https://github.com/opencv/opencv-python/issues/386

[20.2] qt.qpa.plugin: Could not load the Qt platform plugin “xcb” in “” even though it was found. in ubuntu 20.04 when using mpl_toolkits.mplot3d.axes3d
https://github.com/matplotlib/matplotlib/issues/19360

[23.1] pyenv 
https://github.com/pyenv/pyenv

[24.1] uv
https://docs.astral.sh/uv/

## 语言和标准库

参考 python_lang.md 。

## python 搜索和加载包的位置

当前的搜索位置和优先级可以查看 python 的 sys.path 内置变量（字符串的数组）：

```
>>> import sys
>>> sys.path
['', '/usr/lib/python35.zip', '/usr/lib/python3.5', '/usr/lib/python3.5/plat-x86_64-linux-gnu', '/usr/lib/python3.5/lib-dynload', '/home/duanyao/.local/lib/python3.5/site-packages', '/home/duanyao/project/tf-pose-estimation.git', '/usr/local/lib/python3.5/dist-packages', '/usr/lib/python3/dist-packages']
```
sys.path 的优先级从高到低。

运行时也可以修改 sys.path 变量。

运行命令 `python -m site` ，列出大致相同的结果。

debian 上，系统 python 的搜索包位置大致是[1.10]：

* /usr/lib/pythonX/dist-packages/ X 是 2 或 3。

* /usr/local/lib/pythonX.Y/dist-packages/ X.Y 是版本号，下同。

* ~/.local/lib/pythonX.Y/site-packages/

* 入口脚本文件所在目录。

优先级从低到高。根据安装的情况不同，实际上还有其它一些位置。

dist-packages 是 debian 系 linux 特有的目录[1.13]。apt 安装到 /usr/lib/pythonX/dist-packages/，pip 默认安装到 /usr/local/lib/pythonX.Y/dist-packages/ 。如果从源码编译一个 python ，则它的 pip 默认装到 site-packages 目录里。
这样做的目录是避免 debian 系统工具依赖的 python 包被手动安装的 python 包覆盖。
尽管如此，如果用 pip 或其它方式安装、升级 python 包，仍有可能扰乱 debian 仓库里的 python[1.13] 软件，所以不要这样做，
应该用 venv 或者 conda 来安装 apt 无法找到的 python 包。

通过设置环境变量 PYTHONPATH , 可以指定额外的 python 包搜索位置[1.11]。语法与环境变量 PATH 相同。一般情况下 是空的。

venv 利用 pyvenv.cfg 配置文件修改了默认的搜索位置，不会搜索上述默认位置，但仍使用 PYTHONPATH 环境变量。详见 venv 的章节。

python 实际上如何确定搜索位置可能更复杂，参考[1.12]。

## python 启动过程中重要路径的确定

详见源码 Modules/getpath.c 开头的注释。

python 启动过程中的一项重要任务是确定 prefix 和 exec_prefix 两个目录（两者可以相同或不同）。前者用来寻找平台无关库，即 .py 和 .pyc 代码；后者用来寻找平台相关库，即 .so 或 .dll 。具体方法如下：

1. 确定 `python` 可执行程序本身所在的路径，即 argv0_path 。这通过 C 语言的 argv[0] 参数来确定，要么是绝对/相对路径，要么从 PATH 环境变量中搜索。符号链接会被解析。

2. 确定 `python` 可执行程序是在构建目录下执行，还是在安装目录下执行。

3. 如果存在 PYTHONHOME 环境变量，用它来决定 prefix 和 exec_prefix 目录。PYTHONHOME 要么是单个路径，同时赋值给 prefix 和 exec_prefix，要么是一对冒号分割的路径，分别赋值给 prefix 和 exec_prefix 。

4. 如果不存在 PYTHONHOME 环境变量，则从 argv0_path 目录开始，一层一层往上测试，看是否能作为 prefix 和 exec_prefix 目录。prefix 的标志物是 <prefix>/lib/python<VERSION>/os.py ，exec_prefix 的标志物是 <exec_prefix>/lib/python<VERSION>/lib-dynload 。VERSION 是 python 的版本，这是在编译 python 时就写死的。

5. 如果上一步没能确定，则测试 C 语言宏 PREFIX 和 EXEC_PREFIX ，这两者是在编译 python 时就写死的（可以通过 configure 脚本设置）。

## pip

python3 版本的命令是 pip3，python2 则是 pip2，pip 是哪个则要看操作系统配置。

也可以以 python 模块的形式调用 pip：
```
python3 -m pip ...
```

debian 系统的 pip/pip3 脚本做了修改，有时候会有问题，这时候可以用模块形式调用。

### 安装 pip

在 debian 系统上，对于系统默认的 python，不建议用 apt 以外的方法安装全局的 pip 。

debian 系统上[2.2]，安装全局默认 pip：
```
sudo apt install python-pip 
sudo apt install python3-pip
```
但是，如果想同时安装多个版本的 python3 （如 3.8 和 3.10），因为无法同时安装多个版本的 python3-pip，就需要用 get-pip.py 在非默认的 python3 版本中安装 pip，见下面。

或者[2.2]
```
sudo easy_install pip
```
或者[2.2]
```
wget https://bootstrap.pypa.io/get-pip.py
sudo python3.x get-pip.py
```
或者用pip 更新自己：`pip install --upgrade pip` 。

### 用 pip 安装 python 包

在 debian 系统上，最好是只在 venv 等孤立的环境中使用 pip ，以免与 apt 安装的包冲突。虽然 pip install --user 好一点，但还是可能冲突的。

安装软件包：

```
# 从网上的包仓库下载并安装软件包
pip install package_name1 package_name2...

# 从包文件中安装软件包
pip install /path/to/package_name1.whl

# 从源码目录安装软件包
pip install -e /path/to/source_dir/

# 从网上的git仓库安装软件包。多数情况下，@branch_name 和 #egg=package_name 可以省略。
pip install git+https://gitlab.com/user_name/repo_name.git@branch_name#egg=package_name
```

前者是从网上的包仓库下载并安装软件包，后者是安装指定文件名的本地包，pip 会自行猜测是哪一种。

pip install --user package_name1 将会安装到用户目录，不需要特权。
不带 --user 时，系统的 pip 会试图安装到 /usr/local/ ，需要 sudo 。

重装一个包：

```
pip install --force-reinstall package_name1
```

不下载二进制版本，而是下载源码，在本地编译安装：
```
pip install numpy==1.25.2 --no-binary :all:
```

只下载（到当前目录），不安装：
```
pip download numpy==1.16.6
pip download numpy==1.16.6 --no-binary numpy # 下载源码包
# 默认情况下 pip download 也会下载依赖包，--no-deps 可以避免下载依赖包。
pip download xformers==0.0.25+cu118 --no-deps
```

测试安装 dry-run：
```
pip install --dry-run requests
```

### 升级包
```
# 升级到最新版本
pip install -U <package> # -U 等于 --upgrade
# 升级到指定版本
pip install <package>==version
```

### 列出可安装的版本号
```
# pip 版本 23-
pip install <package>==
# 适用任何 pip 版本。一般来说不存在版本号0，所以以下命令会引发错误，并列出所有版本号。
pip install <package>==0 
```

### 批量安装和 requirements.txt

```
pip install -r requirements.txt
```

requirements.txt 文件的语法是，每一行列出一个包，可以加上版本限定。例如

```
# 或者 torch==1.10.2
torch>=1.7.1
torchvision
ftfy
regex
tqdm
```

requirements.txt 文件名不是固定的，而且可以一次指定多个：
```
pip install -r a.txt -r b.txt
```

### pip 的配置文件

不同的系统、版本、虚拟环境中，pip 查找配置文件的位置可能不同，可以用以下命令查看[2.6]：
```
pip config -v debug
```
pip 会在多个位置查找配置文件。例如在 windows 上[2.6]

```
global:
  C:\ProgramData\pip\pip.ini, exists: False
site:
  c:\py\myvenv\pip.ini, exists: False
user:
  C:\Users\myname\pip\pip.ini, exists: False
  C:\Users\myname\AppData\Roaming\pip\pip.ini, exists: False
```

在 linux 上，配置文件一般是 `~/.pip/pip.conf` 。

### 安装 wheel 包
```
pip install wheel
```

一些 pip 的安装过程中要进行编译，要用到 wheel 包。如果没有安装过，可能出现如下错误[2.7]：

```
error: invalid command 'bdist_wheel'
  ---------------------------------------- 
  ERROR: Failed building wheel for XXXX
```

### 列出已安装的包

pip3 freeze
pip3 list
pip3 list --user # 列出安装在用户目录下的包

或者在 python 代码中使用 `help("modules")` 函数。

注意，pip freeze 和 pip list 会列出所有安装的 python 包，而不只是通过 pip 安装的包。

非 pip 安装的包包括：apt 安装的；其它安装脚本安装的。

### 列出一个pip包的内容和元数据
对于已安装的包：
`pip show -f <package>`

对于已下载未安装的包：
```
# 列出 whl 包的文件列表，寻找 */METADATA 文件
unzip -l xxx.whl
# 显示 */METADATA 文件的内容，-c 表示解压到标准输出
unzip -c xxx.whl xxy.dist-info/METADATA
```

输出形如：
```
Archive:  xformers-0.0.25+cu118-cp310-cp310-manylinux2014_x86_64.whl
  inflating: xformers-0.0.25+cu118.dist-info/METADATA  
Metadata-Version: 2.1
Name: xformers
Version: 0.0.25+cu118
Summary: XFormers: A collection of composable Transformer building blocks.
Home-page: https://facebookresearch.github.io/xformers/
Author: Facebook AI Research
Author-email: oncall+xformers@xmail.facebook.com
Classifier: Programming Language :: Python :: 3.7
Classifier: Programming Language :: Python :: 3.8
Classifier: Programming Language :: Python :: 3.9
Classifier: Programming Language :: Python :: 3.10
Classifier: License :: OSI Approved :: BSD License
Classifier: Topic :: Scientific/Engineering :: Artificial Intelligence
Classifier: Operating System :: OS Independent
Requires-Python: >=3.7
Description-Content-Type: text/markdown
License-File: LICENSE
Requires-Dist: numpy
Requires-Dist: torch ==2.2.1

XFormers: A collection of composable Transformer building blocks.XFormers aims at being able to reproduce most architectures in the Transformer-family SOTA,defined as compatible and combined building blocks as opposed to monolithic models
```

对于未下载、未安装的包，可以用 pypi 的 json 接口获取元数据和依赖信息。例如：
```
import requests

package_name = "pandas"
url = f'https://pypi.org/pypi/{package_name}/json'
response = requests.get(url).json()
# response['info'] 为元数据，response['info']['requires_dist'] 为依赖信息。
dependencies = response['info']['requires_dist']
print(dependencies)
```
但此 API 只包含最新版包的完整元数据（包括依赖信息），如果不是最新版，需要先下载whl后查看元数据。

### 搜索
`pip search [package-name]`

pip search 和 install 的包名有微妙的不同，可以 install 的包名未必可以 search 到，有时需要调整大小写，或者把下划线 `_` 替换为减号 `-` 。

### 不同的安装位置

debian 上，apt 安装的 python 包的安装位置是 /usr/lib/pythonX/dist-packages/ X 是 2 或 3。

sudo pip install 的默认安装位置是 /usr/local/lib/pythonX.Y/dist-packages/ X.Y 是版本号，下同。

pip install --user 的默认安装位置是 ~/.local/lib/pythonX.Y/site-packages/ 。

如果分别用以上方法安装同一个包，不会造成包的升级，而是安装了多个，python 只会使用其中一个，优先级从高到低是 ~/.local/lib , /usr/local/lib ,  /usrlib/ 。
pip freeze | list | show 没有 --user 选项，只会显示优先级最高的一个。

### 忽略依赖的安装

```
pip install --no-deps xxx
```
在包本身指定的依赖不符合实际的情况下，可以用这个参数，忽略安装依赖，然后手动安装正确的依赖。

### 固定某些包的版本/避免某些包被升级

安装一个包时，pip 可能升级其依赖的包，有时这会导致其它兼容性问题。要避免此问题，可以在安装时明确指定其依赖的包的版本。
例如，已经安装了 a==1.0，但 b 依赖 a==1.1。如果执行 `pip install b` 则会升级 a 到 1.1 。要固定a的现有版本，避免升级，可以：
```
pip install a==1.0 b
```
如果不想手动指定包的版本，可以固定所有现有包的版本：
```
pip freeze > /tmp/pip_freeze.txt
# 实际要安装的包在 requirements.txt
pip install -r /tmp/pip_freeze.txt -r requirements.txt
rm -f /tmp/pip_freeze.txt
```

### 用 pip 删除软件包
```
pip uninstall package_name1 package_name2...
# -y 不再交互式确认，-r 用文件列出要删除的包，一行一个，可以用 pip freeze 的输出。
pip uninstall -y -r cv-py39-torch110cu113_uninstall.txt
用 pip install --user 安装的包也用 pip uninstall --user 删除。
pip uninstall --user package_name1 package_name2...
```

### pip 镜像站点

pip 默认从 python 默认站点下载包的文件，这可能比较慢。可以用 -i (--index-url) 参数指定一个较快的镜像站点，例如[2.3]：
```
sudo pip install -i https://pypi.tuna.tsinghua.edu.cn/simple  package_name1
```
官方默认 pip 仓库是 `https://pypi.org/simple`。

或者用配置文件 `~/.pip/pip.conf` 或者 `~/.config/pip/pip.conf` (没有就创建一个，后者优先级更高，此文件同时影响 pip2和pip3，也影响 venv)指定为默认的[2.3]：

```
[global]
index-url = https://pypi.tuna.tsinghua.edu.cn/simple
```

其它的源：

https://mirrors.aliyun.com/pypi/simple
清华：https://pypi.tuna.tsinghua.edu.cn/simple
中国科技大学 https://pypi.mirrors.ustc.edu.cn/simple
华中理工大学：http://pypi.hustunique.com/
山东理工大学：http://pypi.sdutlinux.org/
豆瓣：http://pypi.douban.com/simple/

注意，如果候需要用 root/sudo 来运行 pip ，就需要建立 `/root/.pip/pip.conf` 。

有的软件包从其单独的仓库发布，这时候可以用 --extra-index-url 指定额外的仓库：
```
pip install onnxruntime-gpu --extra-index-url https://aiinfra.pkgs.visualstudio.com/PublicPackages/_packaging/onnxruntime-cuda-11/pypi/simple/
```
### pip cache 缓存

https://pip.pypa.io/en/stable/topics/caching/

默认缓存目录：

```
~/.cache/pip
%USERPROFILE%\AppData\Local\pip
```

要清除缓存，删除整个目录即可。或者：
```
pip cache remove setuptools
pip cache purge
```

~/.cache 实际上是由 XDG_CACHE_HOME 环境变量指定的。

此外，pip 缓存的目录可以由环境变量 PIP_DOWNLOAD_CACHE 来修改。

此外，pip 缓存的目录可以由 ~/.pip/pip.conf 的 download_cache 属性来配置：

```
# ~/.pip/pip.conf

[global]
download_cache = ~/.cache/pip
```

查看缓存：

```
pip cache list
pip cache list setuptools
```

不使用缓存：
```
pip --no-cache-dir
```

### 其它 pip 加速方案

https://stackoverflow.com/questions/4806448/how-do-i-install-from-a-local-cache-with-pip/22576769#22576769


pip2pi 自建 pip 镜像

pip-accel https://pypi.org/project/pip-accel/

basket

```
pip download -d "$SOME_DIRECTORY" some-package
pip install --no-index --no-cache-dir --find-links="$SOME_DIRECTORY"
```

https://stackoverflow.com/questions/4806448/how-do-i-install-from-a-local-cache-with-pip/12147405#12147405

```
pip install --no-install --use-mirrors -I --download=$SDIST_CACHE <package name>
pip install --find-links=file://$SDIST_CACHE --no-index --index-url=file:///dev/null <package name> 
```

### 多用户 pip 缓存

目前 pip 缓存的设计没有考虑多用户共享缓存目录的情况，如果让多用户共享缓存目录，则会出现权限问题。

File permissions for cache shared between multiple users
https://github.com/pypa/pip/issues/11012

### pip 包（whl, wheel）文件的命名

whl 文件的命名中就包含了它依赖的平台（操作系统和CPU架构）、python 版本，python ABI 版本，例如：

```
tensorflow-1.12.3-cp36-cp36m-linux_x86_64.whl
tensorboard-1.12.2-py3-none-any.whl
mxnet-1.8.0.post0-py2.py3-none-manylinux2014_x86_64.whl
numpy-1.21.2-pp37-pypy37_pp73-manylinux_2_12_x86_64.manylinux2010_x86_64.whl
```

文件名的格式是 "包名-语言/运行环境版本-ABI-操作系统版本.whl"，各部分用'-'分割，其中 "语言/运行环境版本-ABI-操作系统版本" 为 “Compatible tags” 。
如果一个 whl 同时支持多种语言、ABI、操作系统，则用 '.' 分割。

py2 py3 cp36 pp37 是 python 语言/运行环境版本。py2 是 python2，py3 是 python3，cp36 是 cpython 3.6，pp37 是 pypy 3.7。
cp36m pypy37_pp73 是 ABI 版本。pypy37_pp73 是 pypy 3.7-7.3 。
manylinux2014_x86_64 和 linux_x86_64 和 any 是操作系统的版本 。manylinux 是编译 python 本地模块的标准环境，通常基于 centos ，并可适用于多数基于 centos 或 debian 的发行版。

只有带本地代码的 whl 才需要指定 ABI 和操作系统的版本，纯 python 代码只需要指定 python 版本。

pip 安装 whl 时会严格检查这些依赖是否满足，如果不满足则抛出“xxx.whl is not a supported wheel on this platform” 错误。

pip 不认可 python 的向后兼容，例如 cp36m 的 whl 并不能安装到 python 3.7 的系统中。不过，只要修改文件名中的版本号，也可以强行安装，很多时候也可以运行。

为了调试 pip 安装错误，特别是兼容性问题，可以：

* 运行 `pip debug --verbose` ，在 “Compatible tags” 下会显示与本 python 发行版兼容的所有 Compatible tags 。
* 运行 `pip -v install xxx` ，会显示所有候选的 whl 文件，以及是否与本 python 发行版兼容。

比较上面两个结果，就可以得知为何安装不上。

### 奇怪的 pip install 错误

这是 debian 系自带的 pip 特有的：https://github.com/pypa/pip/issues/3943
```
File "/usr/share/python-wheels/urllib3-1.19.1-py2.py3-none-any.whl/urllib3/util/retry.py", line 315, in increment
    total -= 1
TypeError: unsupported operand type(s) for -=: 'Retry' and 'int'
```

实际上是网络错误而非python语法错误。换一个源，例如加上 -i https://pypi.tuna.tsinghua.edu.cn/simple 参数试试。

### 可能是 pip 临时文件导致的问题
`lib/pythonX.Y/site-packages` 下有时可能出现 `~` 开头的文件/目录，例如 '~vicorn' '~vicorn-0.22.0.dist-info'，猜测可能是安装中断遗留的临时文件。
这会导致一些工具发生错误，例如 `pip freeze`。删除这些文件/目录即可。

## 虚拟环境

### 虚拟环境是什么

前述 apt、pip 安装方式安装的包的作用域都是全局的。即使使用 --user 选项，对本用户来说，安装的包也是全局的，不同的 python 程序无法选择使用一个 python 库的不同版本。
一些 linux 软件仓库里的应用程序是 python 写的，可能依赖仓库里特定版本的 python 库，随便用 pip 升级可能会出问题。例如 deepin 的 gimp 2.8.18 依赖 python-numpy 1.12.1 。

虚拟环境是用来解决这个问题的。它创建一些彼此隔离的 python 包安装目录，只有激活其中一个时，这个目录里的包才起作用。

### 虚拟环境不是什么

不同的 python 程序可能依赖不同的 python 小版本（本地代码尤其如此）。但并非所有的虚拟环境都可以用来解决这个问题，至少并不都适合解决这个问题。

要解决这个问题，可以考虑 pyenv 或 conda ，在一个系统内安装多个 python 小版本。

不同的 python 程序还可能依赖其它本地库的不同版本。虚拟环境也不是用来解决这个问题的，可以考虑使用 docker 或者虚拟机。conda 有时候可以解决这个问题，但并不彻底。

### 有哪些虚拟环境解决方案

实际上很多，如 venv, pyvenv, pyenv, virtualenv, virtualenvwrapper, pipenv [3.1]。

为了简单起见尽量用 venv ，因为它是 python 3.3 自带的。

### 虚拟环境与 `--user` 安装选项不兼容

在任何虚拟环境中，都不要使用 pip、setuptools 的 `--user` 安装选项。

以下通过 venv 为例说明[3.2]，其它虚拟环境是类似的。

`pip --user install ...` 安装方式会将包安装到用户全局位置，一般是 `~/.local/lib/pythonX.Y/site-packages` 。但这个位置对于 venv 是不可见的。
当前版本的 python（2020.2）中，venv 中使用 `pip --user` 时存在bug：仍然安装到了用户全局位置，但安装后则无法正常使用，还污染了用户全局位置。
未来 pip 可能会改为，在 venv 中使用`--user` 安装选项会报错。但在目前，需要自行注意不去使用 `--user` 。
注意，如果在 pip 配置文件 `~/.pip/pip.conf` 中设置了 

```
[install]
user = true
```
则等效于 `--user` 选项一直开启。不要这样做。

在 venv 中使用 setuptools 的 `--user` 安装选项时（`python setup.py --user`），确实会报告错误，不过比较隐晦：
```
TEST FAILED: ~/.local/lib/pythonX.Y/site-packages/ does NOT support .pth files
error: bad install directory or PYTHONPATH
```
这是因为用户全局位置对 python 不可见。如果设置 
```
export PYTHONPATH=$HOME/.local/lib/pythonX.Y/site-packages/
```
的确可以安装到用户全局位置，但还是不能正常使用的，因此不要这样做，应该去掉 --user 选项。

virtualenv 和 conda 中使用 `--user` 安装选项也可能遇到类似的问题，因此都不要使用它。

### venv
参考[4.1]。

ubuntu 20.04 上，自带的 python3.9 还需要额外安装deb包 python3.9-dev python3.9-venv （主要是后者）才可以使用 venv （系统默认 python 是 3.8）。

```
python3 -m venv ~/opt/venv/e1  # 创建
source ~/opt/venv/e1/bin/activate  # 激活
```

~/opt/venv/e1 目录下会生成一个 python 安装目录，~/opt/venv/e1/bin 下有 python、pip 等命令，pip 安装的包位于 ~/opt/venv/e1/lib/pythonX.Y/site-packages 目录。

激活的作用主要是将 ~/opt/venv/e1/bin 添加到 PATH 中。不激活，而是直接使用 ~/opt/venv/e1/bin 下的命令也是一样的。

~/opt/venv/e1/bin/python 是到 /usr/bin/python3 的符号链接，因此其版本会随着系统的 python 版本而升/降级。也可以选择将 python 可执行文件直接复制到 venv 中，从而避免之后依赖系统 python ，见后面。

~/opt/venv/e1/pyvenv.cfg 文件配置了 sys.path 的默认值，其不包括 /usr/lib/pythonX/dist-packages/ ，/usr/local/lib/pythonX.Y/dist-packages/ ， ~/.local/lib/pythonX.Y/site-packages/ 等目录，
仅包括 ~/opt/venv/e1/lib/pythonX.Y/site-packages 目录。

venv 仍使用 PYTHONPATH 环境变量。也可以动态修改 sys.path 来增加库的搜索目录。

新创建的 venv 最好升级一下 pip，因为初始状态的 pip 版本与操作系统自带的相同，可能比较老。

```
pip install --upgrade pip
```

对于 debian 系统上用系统 python 创建的 venv ，升级 pip 是必须的，因为初始状态下 venv 的 pip 链接到系统 pip ，使用中会有很多问题，如安装不上 whl 包，报错 "not a supported wheel on this platform" 或者 "not compatible with this Python" 。

可以用安装在一个主机中的不同 python 实例来创建 venv 。但用 conda 安装的 python 实例似乎不能用来创建 venv ，因为会创建错误的 python 命令符号链接。

要删除一个 venv 环境，删除其目录即可。

#### debian/ubuntu 上多版本 python 创建 venv
例如，ubuntu 24.04 上，默认的 python 3.12，ubuntu 22.04 上，默认的 python 3.10。可以加入 ubuntu 20.04 的源后安装 python3.8 或 python3.9 。
```
# 增加仓库。
echo "deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu focal main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu focal-updates main restricted universe multiverse" > /etc/apt/sources.list.d/ubuntu2004.list

apt update
apt install --no-install-recommends python3.9
```
但是，这样安装的 python3.9 无法使用 pip 和 venv，也无法顺利创建 venv[2.8]。可以这样处理：
```
# get-pip.py 依赖 distutils，但 python3-distutils 的依赖无法满足，因此直接下载和解压 deb 包。
apt download python3-distutils=3.8.10-0ubuntu1~20.04
sudo dpkg -x python3-distutils_3.8.10-0ubuntu1~20.04_all.deb /
# python3.9-venv 的依赖无法满足，因此直接下载和解压 deb 包。
apt download python3.9-venv
sudo dpkg -x python3.9-venv_3.9.5-3ubuntu0~20.04.1_amd64.deb /
# 创建 venv。必须加上 --without-pip 才能创建 venv ，否则会报错。此 venv 不包含 pip 。
python3.9 -m venv --without-pip ~/opt/venv/v39
# 为此 venv 安装 pip [2.9]。
wget https://bootstrap.pypa.io/get-pip.py
~/opt/venv/v39/bin/python get-pip.py
# 此 venv 的 pip 可用了。
# ~/opt/venv/v39/bin/pip install numpy
```

已有的 ubuntu 20.04 python3.9 在升级 ubuntu 22.04/24.04 后，venv （~/opt/venv/v39）中的 pip 也无法使用，出现如下问题`ModuleNotFoundError: No module named 'distutils.util'`，可以按之前的方法修复：
```
apt download python3-distutils=3.8.10-0ubuntu1~20.04
sudo dpkg -x python3-distutils_3.8.10-0ubuntu1~20.04_all.deb /

wget https://bootstrap.pypa.io/get-pip.py
~/opt/venv/v39/bin/python get-pip.py
```

根本原因可能是 debian/ubuntu 上 python3-distutils 只能装一个版本，就是发行版预设的版本。例如在 ubuntu 22.04（jammy）上配置了 ubuntu 20.04（focal）的仓库：

```
apt policy python3-distutils
python3-distutils:
  已安装：3.10.8-1~22.04
  候选： 3.10.8-1~22.04
  版本列表：
 *** 3.10.8-1~22.04 500
        500 https://mirrors.tuna.tsinghua.edu.cn/ubuntu jammy-updates/main amd64 Packages
        500 https://mirrors.tuna.tsinghua.edu.cn/ubuntu jammy-updates/main i386 Packages
        500 https://mirrors.tuna.tsinghua.edu.cn/ubuntu jammy-security/main amd64 Packages
        500 https://mirrors.tuna.tsinghua.edu.cn/ubuntu jammy-security/main i386 Packages
        100 /var/lib/dpkg/status
     3.10.4-0ubuntu1 500
        500 https://mirrors.tuna.tsinghua.edu.cn/ubuntu jammy/main amd64 Packages
        500 https://mirrors.tuna.tsinghua.edu.cn/ubuntu jammy/main i386 Packages
     3.8.10-0ubuntu1~20.04 500
        500 https://mirrors.tuna.tsinghua.edu.cn/ubuntu focal-updates/main amd64 Packages
        500 https://mirrors.tuna.tsinghua.edu.cn/ubuntu focal-updates/main i386 Packages
     3.8.2-1ubuntu1 500
        500 https://mirrors.tuna.tsinghua.edu.cn/ubuntu focal/main amd64 Packages
        500 https://mirrors.tuna.tsinghua.edu.cn/ubuntu focal/main i386 Packages
```

#### 移动、改名、复制 venv 环境（手动）

venv 的 bin, lib 目录下一些脚本将 venv 本身的路径写死了，所以不能随意移动、改名。要移动或改名一个 venv 环境，例如要将 `~/opt/venv/e1` 改名为 `~/opt/venv/e2`，可以这样做：

* 创建一个新的 venv `~/opt/venv/e2` 。
* 将 `~/opt/venv/e2/bin` 目录下的内容覆盖 `~/opt/venv/e1/bin` 的内容。
* 将 `~/opt/venv/e1/bin` 下的后期安装的 python 脚本打开编辑，将首行的 `#!.../opt/venv/e1/bin/python3` 改为 `#!.../opt/venv/e2/bin/python3` 。
* 将 `~/opt/venv/e1/lib/pythonX.Y/site-packages/setuptools.pth` 里的 `.../opt/venv/e1/lib/pythonX.Y/site-packages` 改为 `.../opt/venv/e2/lib/pythonX.Y/site-packages`
* 可视情况修正或删除 `~/opt/venv/e1/lib/python3.9/site-packages/easy-install.pth` 里的绝对路径。
* 删除 `~/opt/venv/e2`，将 `~/opt/venv/e1` 重命名为 `~/opt/venv/e2` 。

#### 移动、改名、复制 venv 环境（virtualenv-clone）

```
pip install virtualenv-clone

virtualenv-clone ~/opt/venv/v39-1 /mnt/venv/v39-2
```

virtualenv-clone 有一些未完善的地方：

* 命令提示符仍是旧名字。应将 `bin/activate` 中的 `PS1="(v39-1) ${PS1:-}"` 改为 `PS1="(v39-2) ${PS1:-}"`。

#### 复制而不是依赖系统 python 可执行文件
使用 `--copies` 参数，复制系统 python 可执行文件到 venv 中，而不是做符号链接，这样以后就不会依赖系统 python 了。
```
python -m venv --copies v311sa

ls -l v311sa/bin

-rwxr-xr-x 1 duanyao duanyao 6739864  6月21日 16:42 python
-rwxr-xr-x 1 duanyao duanyao 6739864  6月21日 16:42 python3
-rwxr-xr-x 1 duanyao duanyao 6739864  6月21日 16:42 python3.11
```
但应该注意到，3个 python 可执行文件（python python3 python3.11）是重复的，会多占用一些空间。

#### 变更 venv 指向的 python 实例

在大小版本号都不变的情况下，需要修改以下地方（venv 目录是 ~/opt/venv/e1）：

* ~/opt/venv/e1/bin/pythonX.Y : 符号链接，指向实际的 python 可执行程序，如 /usr/local/bin/python3.7 。
* ~/opt/venv/e1/pyvenv.cfg : 修改 home 变量，如果必要，如 /usr/local/bin 。version 也可以适当修改，如 3.7.6 。

在小版本号改变的情况下，一般不建议修改 venv 指向的实例，因为本地库链接的 python 都是指定小版本号的。

#### venv 中 python 的头文件

python 的头文件如 Python.h 并未链接到 venv 下的 include 目录下。
通常，应该使用 python 模块 sysconfig 来找到 include 目录：`sysconfig.get_path('include')` 。

在 cmake 中，可以用以下方法找到 python 的开发文件：

方法一：
CMakeList.txt 中：
```
find_package (Python3 COMPONENTS Interpreter Development)

# Python3_Development_FOUND

target_include_directories(${target_name} PRIVATE src/ ${Python3_INCLUDE_DIRS} ...)
target_link_libraries(${target_name} Python3_LIBRARIES ...)
```

方法二：

CMakeList.txt 中：

```
target_include_directories(${target_name} PRIVATE src/ ${PYTHON_INCLUDE_DIRS} ...)
target_link_libraries(${target_name} ${PYTHON_LIBRARIES} ...)
```

执行 cmake 命令：
```
cmake -D PYTHON_EXECUTABLE=python -D PYTHON_INCLUDE_DIRS=/usr/local/include/python3.7m
```

### virtualenv

用 pip 安装 [2.1]
sudo pip install virtualenv

或者 debian 上：
sudo apt-get install virtualenv
sudo apt-get install python3-virtualenv

使用 virtualenv
```
virtualenv <project_dir>
<project_dir>/bin/pip install xxx
```
如果是 python3：
```
virtualenv -p python3 <project_dir>
```
其它不变，python 和 pip 自动是 3 的。

## pipx
pipx 是 pip 的扩展，它自动为每个应用程序创建一个 venv 虚拟环境。
与 pip 相比，pipx 的适用范围主要是带命令行界面的应用程序，而不是单纯的库。
pipx 是完全用 python 实现的。

### 安装 pipx
在 debian/ubuntu 上安装：
```
sudo apt install pipx
```
这会安装到 `/usr/lib/python3/dist-packages/pipx` ，并可以通过 `/usr/bin/pipx` 运行。

也可以用以下命令安装/升级 pipx，仅限当前用户：
```
python3 -m pip install --user -U pipx
```

### 用 pipx 安装 python 包
用 pipx 安装 python 包 xxx ：
```
pipx install xxx
```
以普通用户执行以上命令时，将安装到 `~/.local/pipx/venvs/xxx/` 下（这是个 venv），如果有可执行脚本，则链接到到 `~/.local/bin` 下。
运行 `pipx ensurepath` 会将 `~/.local/bin` 加入到 PATH 环境变量中，也会把 `~/.local/bin` 添加到 `~/.bashrc` 或 `~/.zshrc` 中。
pipx 创建的多个 venv 之间可能共享一些完全相同的库，它们位于 `~/.local/pipx/shared/` 下面。

pipx 安装的包也来自 pip 仓库，使用 pip 的配置文件，如 ~/.pip/pip.conf 。

## python 多版本管理
### pyenv
[23.1]
pyenv 是一个 python 版本管理器，可以管理多个版本的 python。它是 shell 脚本实现的，本身不依赖 python。
pyenv 安装 python 是通过在本地编译源代码实现的，所以需要有编译环境。

#### 安装 pyenv
```
curl -fsSL https://pyenv.run | bash
```

### uv
[24.1]

#### 安装 uv
```
# 这个命令访问 https://astral.sh 可能被墙，可改用下面 pip/pipx 安装。
curl -LsSf https://astral.sh/uv/install.sh | sh

uv self update # 更新 uv 自己
```
安装位置是 `.local/bin/{uv,uvx}`，为 ELF 可执行程序，无其他依赖。

也可以通过 pip/pipx 安装：
```
pip install --user uv # 也是安装到 ~/.local/bin/{uv,uvx}，以及 ~/.local/lib/python3.x/site-packages/uv 
# 或者
pipx install uv # 安装到 ~/.local/pipx/venvs/uv/ ，链接到 ~/.local/bin/{uv,uvx}
```

可以给 `~/.bashrc` 添加一行 `export PATH="$PATH:$HOME/.local/bin"` ，如果 `$HOME/.local/bin` 还不在 PATH 中。

#### 使用 uv 安装 python
uv 可以直接安装二进制 cpython 和 pypy，版本比较齐全，包括最新的 alpha 版本。如果系统中自带了某些版本的 python，uv 也会自动检测并使用它们。
```
uv python list # 列出可安装/已安装的版本
uv python install 3.13.1+freethreaded 3.14
uv python install pypy@3.10
```

cpython 安装位置是`~/.local/share/uv/python/cpython-3.y.z-linux-x86_64-gnu` ，可以用 uv python list 来查看：
```
cpython-3.14.0a3-linux-x86_64-gnu                 <download available>
cpython-3.13.1+freethreaded-linux-x86_64-gnu      .local/share/uv/python/cpython-3.13.1+freethreaded-linux-x86_64-gnu/bin/python3.13t
```

uv 的 cpython 二进制文件来自此项目：https://github.com/astral-sh/python-build-standalone 。

#### 运行 uv run
* 可以直接运行： `.local/share/uv/python/cpython-3.13.1+freethreaded-linux-x86_64-gnu/bin/python`
* 运行默认 python : `uv run example.py` 或 `uv run python` 。
* 运行指定版本的 python : `uv run --python 3.13t python` 或 `uv run --python 3.14 python example.py` 。t 代表 freethreaded。版本号可以模糊匹配，例如 3.13t 可以匹配 3.13.1+freethreaded 。

#### 使用 uv venv 和 uv pip
例：
```
# 在当前目录创建名为 v12 的基于 python 3.12 的 venv 。如果 python 3.12 还没装会自动执行 `uv python install 3.12`。
# freethreaded 版本加上t后缀。
uv venv v12 --python 3.12 --seed
uv venv v13t --python 3.13t --seed

. v12/bin/activate # 激活 venv

# uv pip install 等效于 pip install numpy ，做了更多优化。也可以在非 uv 创建的 venv 中使用。
uv pip install numpy 
```


### 管理缓存、数据
```
uv cache clean # Remove cache entries.
uv cache prune # Remove outdated cache entries.
uv cache dir # Show the uv cache directory path.
uv tool dir # Show the uv tool directory path.
uv python dir # Show the uv installed Python versions path.

rm -Rf ~/.local/share/uv/

rm ~/.local/bin/uv ~/.local/bin/uvx
```

## 性能剖析/profiling

[12.1]

### cProfile
会拖慢被剖析程序。
```
python -m cProfile [-o output_file] [-s sort_order] (-m module | myscript.py)
```

```
# 运行程序记录数据：
# python -m cProfile -o profile_data.pyprof path/to/your/script arg1 arg2

# pip install pyprof2calltree
# 使用pyprof2calltree处理数据并自动调用KCacheGrind
pyprof2calltree -i profile_data.pyprof -k

# 生成 callgrind 格式的文件，然后用 kcachegrind 查看
pyprof2calltree -i profile_data.pyprof -o profile_data.callgrind
kcachegrind profile_data.callgrind
```

命令行模式下 cProfile 似乎只能剖析主进程。如果要进行子进程剖析，需要修改 python 代码：
```
import cProfile, sys
pr = cProfile.Profile()
pr.enable() # 开始剖析
YOUR MAIN FUNCTION
pr.disable() # 停止剖析

# 在进程退出的地方写文件：
# - for binary dump
pr.dump_stats('cpu_%d.prof' %sys.getpid())

# - for text dump
with open( 'cpu_%d.txt' %sys.getpid(), 'w') as output_file:
    sys.stdout = output_file
    pr.print_stats( sort='time' )
    sys.stdout = sys.__stdout__
```
要在程序退出的时候写文件，可以用 `atexit.register(func, *args, **kwargs)` 注册一个处理函数。

然后可视化：
```
pip install snakeviz
snakeviz -s -H 0.0.0.0 -p 8080 cpu_xxx.prof # 默认绑定 127.0.0.1:8080 ，只能被本地访问

Port 8080 in use, trying another.
snakeviz web server started on 0.0.0.0:8081; enter Ctrl-C to exit
http://0.0.0.0:8081/snakeviz/%2Fhome%2Frd%2Fproject%2Fmark-tool.git%2Fdoc%2Fr50_d1w1b32.pyprof # 用浏览器打开此链接
```

字段解释：
```
ncalls: Total number of calls to the function. If there are two numbers, that means the function recursed and the first is the total number of calls and the second is the number of primitive (non-recursive) calls.
tottime: Total time spent in the function, not including time spent in calls to sub-functions
percall: tottime divided by ncalls
cumtime: Cumulative time spent in this function and all sub-functions
percall: cumtime divided by ncalls
```

cProfile 并不能直接剖析多线程程序，只能剖其所在线程。一个可能的变通是，在每个线程中创建一个 cProfile 对象，专门剖析此线程。

### mtprof
mtprof 的 API 类似 cProfile，但是可以剖析多线程程序[12.5]。会拖慢被剖析程序。
mtprof 是纯 python 程序，底层实现仍为 cProfile。
mtprof 的结果混合了所有线程的数据，而非按线程分开统计。

安装：
```
pip install mtprof
```

编码式使用
```
from mtprof import Profile
# 0. 创建 Profile 对象
pr = Profile()
# 1. 开始剖析
pr.enable()
# 2. 运行被剖析的代码
func()

prof_path = 'xxx.prof'
# 3. 保存结果
pr.dump_stats(prof_path)
```

注意 mtprof 使用中的一些限制：
* 被剖析线程应在 Profile 对象创建之后创建，即在 1 ～ 3 的位置。
* 被剖析线程应在保存结果之前结束，即在 2 ～ 3 的位置。否则保存的结果中不包含仍活动的线程，并输出警告。

命令行使用：

```
mtperf.py [-h] [-o OUTFILE] [-s SORT] [-m] script_or_module

python -m mtprof -o OUTFILE to_be_profiled.py
```
命令行界面也与 cProfile 相似。

目前（2025.2） mtprof 与 python 3.13t 不兼容，即使启用 GIL ，会出异常：
```
  File "/home/duanyao/v13t/lib/python3.13t/site-packages/mtprof/__init__.py", line 92, in _run_thread
    prof.enable()
ValueError: Another profiling tool is already active
```

### Yappi
Yappi 可以剖析多线程、asyncio 或 gevent 程序[12.6]。会拖慢被剖析程序。
Yappi 安装时需要 C 编译器。

安装：
```
pip install yappi
```

使用：
```
import yappi
# 1. 启动剖析
yappi.start()

# 2. 运行被剖析的代码
func()

# 3. （可选）获得当前全部线程的信息，因为 yappi.get_thread_stats() 得到的信息不够有用。
# 应当在被剖析的线程结束前被调用。
thread_map = { thread.ident: {'native_id': thread.native_id, 'name': thread.name, 'id': thread.ident }
              for thread in threading.enumerate() }

yappi.stop()

threads = yappi.get_thread_stats()
for th in threads:
    st = yappi.get_func_stats(ctx_id=th.id)
    th_info = thread_map.get(th.tid)
    if th_info:
      prof_path = f'yappi_{os.getpid()}_{th_info['name']}_{th_info['native_id']}.prof'
    else:
      prof_path = f'yappi_{os.getpid()}_{th.name}_{th.tid}.prof'
    st.save(prof_path, type='pstat') 
```

注意事项：
* yappi.start() 应放在线程或线程池创建之前（除了主线程），否则可能产生混乱的结果。尽管线程池创建后并未立即创建线程，yappi.start() 还是需要在线程池创建前调用。这一点与其文档声称的不同（可以在任意时刻获得结果）。
* yappi.stop() 应放在线程和线程池关闭之后（除了主线程），否则可能产生混乱的结果。
* 线程名 `th.name` 并非真正的 python 线程名，一般是 'Thread' 或 'MainThread'（主线程）。
* 线程id `th.tid` 并非操作系统的线程id，而是 python 内部线程id，与 `threading.Thread:ident` 对应。
* 保存文件时需要指定格式 `type='pstat'`，其格式与 cProfile 相同。
* 目前（2025.2）yappi 与 python 3.13t 自由线程模式不兼容，会挂起无法结束运行，同时输出错误：
  ```
  [*]	[yappi-err]	Internal Error. [6]
  [*]	[yappi-err]	Internal Error. [15]
  ```
  启用 GIL 后可以正常工作。

### pyinstrument
https://github.com/joerick/pyinstrument
* 采样法，默认1000Hz。开销约 30%，低于 cProfile 的 84%。
* python+C 扩展编写。
* 命令行模式+代码模式。
* 记录程序消耗的时钟时间（wall-clock），而不是CPU时间。
* 可剖析 async/await 异步程序。
* 尚未支持多线程（有补丁等待合并 https://github.com/joerick/pyinstrument/pull/353 ）。
* 尚未支持多进程（https://github.com/joerick/pyinstrument/issues/31）

用法（命令行）：
```
pyinstrument -o profile.prof -r speedscope  --show-all xxx.py [arg] ...
```

用法（代码）：
```
import pyinstrument

# 默认 interval=0.001
with pyinstrument.profile(interval=0.0001):
    # code you want to profile

@pyinstrument.profile(interval=0.0001)
def my_function():
    # code you want to profile
```

底层 API
```
from pyinstrument import Profiler

profiler = Profiler(interval=0.0001, renderer='speedscope')
profiler.start()

# code you want to profile

profiler.stop()
with open('yyy.speedscope', 'wb') as f:
  profiler.print(file=f, show_all=True)
```

### py-spy
https://github.com/benfred/py-spy
* 采样法，低开销。默认100Hz，可调。
* 命令行模式。代码模式尚不清楚。
* Rust编写，支持 CPython 3.3-3.13（2025.2）
* 支持剖析本地扩展（--native）。
* 支持剖析子进程（--subprocesses）。
* 可剖析 GIL 活动。
* 可以避免采样时暂停目标进程（--nonblocking），进一步减少开销，不过这会影响剖析结果的准确性。

安装：
```
# 安装二进制版本。不区分 python 版本。建议用全局 pip 以用户模式运行，安装到 `~/.local/bin` 下。
# debian 系上用 --break-system-packages 强制执行。
pip install --user --break-system-packages py-spy
# 如果是 venv 中执行 pip install py-spy 则安装到了 `<venv>/bin` 下面。

# 从 rust 源码安装。linux 上依赖 libunwind-dev 包。
cargo install py-spy
```

用法：
```
# 记录剖析数据，生成 svg 文件。也可输出 speedscope 、chrometrace 等格式。
py-spy record -o profile.svg --pid 12345
# OR
py-spy record -o profile.svg -- python myprogram.py

py-spy record -s -t --format speedscope -o profile.speedscope -- python myprogram.py

# 实时显示剖析数据，包括热点函数、GIL开销、线程数等。
py-spy top --pid 12345
# OR
py-spy top -- python myprogram.py

# 显示各线程的当前调用栈。
py-spy dump --pid 12345
```

speedscope 文件的可视化查看器：https://www.speedscope.app/ （web app）。
或者安装本地版：`npm install -g speedscope`。

其他选项：
```
USAGE:
    py-spy record [OPTIONS] [python_program]...

ARGS:
    <python_program>...    commandline of a python program to run

OPTIONS:
    -p, --pid <pid>              PID of a running python program to spy on
        --full-filenames         Show full Python filenames, instead of shortening to show only the package part
    -o, --output <filename>      Output filename
    -f, --format <format>        Output file format [default: flamegraph] [possible values: flamegraph, raw, speedscope, chrometrace]
    -d, --duration <duration>    The number of seconds to sample for [default: unlimited]
    -r, --rate <rate>            The number of samples to collect per second [default: 100]
    -s, --subprocesses           Profile subprocesses of the original process
    -F, --function               Aggregate samples by function's first line number, instead of current line number
        --nolineno               Do not show line numbers
    -t, --threads                Show thread ids in the output
    -g, --gil                    Only include traces that are holding on to the GIL
    -i, --idle                   Include stack traces for idle threads
    -n, --native                 Collect stack traces from native extensions written in Cython, C or C++
        --nonblocking            Don't pause the python process when collecting samples. Setting this option will reduce the performance impact of sampling, but may lead to
                                 inaccurate results
```


### Scalene
a Python CPU+GPU+memory profiler with AI-powered optimization proposals
* 低开销，0～30%。
* python+C 扩展编写。
* 命令行模式、代码模式
* 行级别+函数级别剖析
* 可报告 GPU 时间（目前支持nvidia）
* 可剖析内存分配
* 支持多线程、多进程（2025.2，Windows/WSL 多进程有一些bug：https://github.com/plasma-umass/scalene/issues/846）
* 有 vscode 扩展
* 以 html 文件的形式输出结果

安装：
```
pip install scalene # 需要编译C++代码
```
如果使用 venv，应当安装 scalene 到每个 venv中。

用法（命令行）：
```
scalene --cpu --gpu --memory your_prog.py
或者
python -m scalene --cpu --gpu --memory your_prog.py
```

用法（代码）：
```
from scalene import scalene_profiler

# Turn profiling on
scalene_profiler.start()

# your code

# Turn profiling off
scalene_profiler.stop()
```
仅剖析个别函数：
```
@profile
def slow_function():
    import time
    time.sleep(3)
```

注意事项：
* 剖析多进程程序时，应确保子进程被正常关闭，否则剖析结果可能不完整。

### pprofile
https://github.com/vpelletier/pprofile
* 行级别+函数级别剖析
* 支持多线程（多进程尚不清楚）
* 采样法、插桩法可选
* 纯python，命令行模式（代码模式尚不清楚）

### Callgrind
参考 profiler_performance.md 中的 Callgrind 部分。
Callgrind 剖析的是本地代码部分。
Callgrind 会拖慢被剖析程序。

## ABI
参考 [7.1] 。

python 的 ABI 包括 python 的版本（大小版本号都包括，可以不写'.'号）和标志（flag）两部分，例如 35m；后缀也可以没有，例如 35。其中标志的含义是：


    --with-pydebug (flag: d)
    --with-pymalloc (flag: m)
    --with-wide-unicode (flag: u)

python 的本地库文件一般在文件名中包含 ABI 的描述，例如 resource.cpython-37m-x86_64-linux-gnu.so 和 libpython3.7m.so 。

不同标志的 python 和本地库不兼容。较高版本号的 python 可能兼容低版本号的本地库，也可能不兼容。

python3.2 之后 --with-pymalloc 默认是 yes。debian 的 python 和大部分 pip 库也是如此，有 m 标志。

python3.8 之后，python 的库文件名不再带 m 标志了，例如 /usr/lib/libpython3.9.so.1.0 。pymalloc 默认启用但可以在启动时被关闭[7.2]。

python3.3 以后去掉了 --with-wide-unicode 。

## 停止生成 pyc 文件
[13.1]
方法一：使用-B参数 即
```
python -B test.py
```
方法二：设置环境变量
```
export PYTHONDONTWRITEBYTECODE=1
```
方法三：在导入的地方设置如下
```
import sys
sys.dont_write_bytecode = True
```

## JIT 运行环境

### pypy

pypy 是另一个 python 运行环境的实现，与 CPython 相比，它支持 JIT ，速度更快。pypy 提供 linux 二进制包，解压即可使用 [8.1]。
pypy 支持的 python 语言版本一般落后于 CPython 。例如 CPython 到 python 3.9 时，pypy 仅支持 python 3.7 。

pypy 与 CPython 存在一些不兼容，特别是在本地接口方面[8.2]。一些重度依赖本地接口的程序可能无法在 pypy 上运行，不过这不是原理性的，主要是工程性的。

不兼容列表（ 2021.8 检查 ）

pytorch  https://github.com/pytorch/pytorch/issues/17835
mxnet https://github.com/apache/incubator-mxnet/issues/17097
tensorflow https://github.com/tensorflow/tensorflow/issues/252

兼容列表（一个较大的列表可见[8.3]）

scikit-learn
numpy
opencv-python 需要从源码编译  https://github.com/opencv/opencv-python/issues/195
pybind11  https://github.com/pybind/pybind11/pull/2146
cffi

### GraalVM Python Runtime

GraalVM 是一种多语言JIT/AOT运行环境。Python是其支持的一种语言，目前（2023.2）处于实验状态，支持 python3.8。
目前还不支持 Python C API ，只支持一些处理过的依赖本地代码的库，包括 NumPy 等。
纯 Python 代码可以比 CPython 3.8 快 5-6 倍。  

```
gu install python

graalpy -m venv my_new_venv
source my_new_venv/bin/activate

graalpy -m ginstall install --help # short help document including a comma-separated list of packages you can install
graalpy -m ginstall install pandas
```



GraalVM 可以将 Python 代码AOT编译为可执行程序，也可以在 JVM 上执行它。

### numba
[10.1-10.2]。
numba 是个 python 库，可以局部编译 python 代码为本地代码。主要优化数值计算代码，可以产生 CPU 的向量代码（X86、ARM等）和 GPU 代码（CUDA和ROCm）。
目前支持 Python 3.6-3.9 。

使用时，用装饰器标记要JIT编译的函数即可。

### Pyjion

Pyjion, a JIT extension for CPython that compiles your Python code into native CIL and executes it using the .NET CLR.
Pyjion 可以支持所有的 python C 扩展，不需要修改。Pyjion 是作为 CPython 的一个模块运行的 [9.1]。

先安装 .net 运行环境[9.2]：

```
wget https://packages.microsoft.com/config/debian/10/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
rm packages-microsoft-prod.deb
sudo apt-get update
sudo apt install dotnet-runtime-5.0
```

然后可在 venv 中安装 pyjion 模块： 

```
pip install pyjion
```

然后在 python 代码中启用 JIT ：

```
import pyjion
pyjion.enable()

def half(x):
  return x/2

half(2) # 1.0
pyjion.info(half)
# {'failed': False, 'compile_result': 1, 'compiled': True, 'pgc': 1, 'run_count': 1}

import pyjion.dis
pyjion.dis.dis_native(half)
```

### pyston

pyston 比 cpython 快 30%-50% ，但不如 pypy 快。pyston 是基于 LLVM 的实现。
进度落后 cpython 一个小版本。

目前与 cpython 并非 ABI 兼容。pip 安装本地模块时，如果可能，要从源码编译（自动进行）。

## 安装其它 python 版本/多版本 python 共存

### portable 二进制 linux 版

python 官方没有提供 portable 二进制 linux 版。
可以用 pyston 或 pypy 的二进制版代替。pyston, pypy 都与 cpython 的本地代码 ABI 不完全兼容，因此带有本地代码的 python 库无法完全兼容于 cpython。

### ubuntu/debian 多版本 python PPA

https://launchpad.net/%7Edeadsnakes/+archive/ubuntu/ppa
https://github.com/deadsnakes/

```
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt update
```

- Ubuntu 20.04 (focal) Python3.5 - Python3.7, Python3.9 - Python3.13
- Ubuntu 22.04 (jammy) Python3.7 - Python3.9, Python3.11 - Python3.13
- Ubuntu 24.04 (noble) Python3.7 - Python3.11, Python3.13
- Note: Python2.7 (focal, jammy), Python 3.8 (focal), Python 3.10 (jammy), Python3.12 (noble) are not provided by deadsnakes as upstream ubuntu provides those packages.

### 从源码构建二进制 linux 版


参考 python_build_linux.md 。在 debian 系统上，建议手动构建 deb 包。

## python 交互式控制台

### SyntaxError: multiple statements found while compiling a single statement
在较老版本的 python（如 3.6）交互式控制台中，粘贴多行 python 代码可能导致抛出异常：“SyntaxError: multiple statements found while compiling a single statement”。
在 bash 中的解决办法是禁用 bracketed-paste 功能[14.1]：
``` 
echo "set enable-bracketed-paste off" >> ~/.inputrc
```

## IDE 设置

### vs code
[6.1]

#### 设置调试用的 python 环境

要选择一个 python 用于调试，可以按下 Ctrl+Shift+P 打开命令面板，再输入 Python: Select Interpreter ，在下拉列表里选择一个 python 。新建的 venv
或者 conda 环境可能不能被列出，可以刷新一下窗口（输入 Reload Window ）再试试。

可以自动识别全局安装的 python，如 /user/bin/python3, /usr/local/bin/python3.7 。

可以自动识别 conda 环境里的 python。运行前会自动激活它。

可以自动识别当前工程目录下的名为 .venv 的 venv 环境里的 python。

不能自动识别非特定位置的 venv 环境里的 python。但可以设置 python.venvPath 指向一个包含多个 venv 环境的目录。方法：菜单 file -> preference -> user -> 搜索 python.venvPath，输入 `~/opt/venv/` 。

#### 调试 python 库的代码
需要在 .vscode/launch.json 中对项目设置：

"justMyCode": false

## sysconfig

sysconfig 是个 python 包，可以获得 python 的安装目录、include 和 lib 目录等信息。
例如 
```
sysconfig.get_config_var('prefix')
sysconfig.get_path('include')
sysconfig.get_config_var('LIBPL')
sysconfig.get_config_var('LIBS')
```

使用方法可参考 `${LIBPL}/python-config.py` 文件。

## bugs

### pyqt5 与 fcitx 输入法

pip3 安装的 PyQt5 包无法使用 fcitx 框架的输入法（包括搜狗拼音等），这是因为它缺少 /usr/local/lib/python3.X/dist-packages/PyQt5/Qt/plugins/platforminputcontexts/libfcitxplatforminputcontextplugin.so 这个文件。

apt 安装的 python3-pyqt5 要想使用 fcitx 框架的输入法，需要安装 fcitx-frontend-qt5 fcitx-libs-qt fcitx-libs-qt5 这几个包。fcitx-frontend-qt5 
包括了这个文件：

/usr/lib/x86_64-linux-gnu/qt5/plugins/platforminputcontexts/libfcitxplatforminputcontextplugin.so

但是，将这个文件复制到 /usr/local/lib/python3.X/dist-packages/PyQt5/Qt/plugins/platforminputcontexts/ 不见得有效，因为 pip3 安装的 PyQt5 和 apt 安装的 fcitx-frontend-qt5 版本未必能兼容。

所以，解决办法可能是删除 pip3 安装的 pyqt5 ，用 apt 安装 pyqt5 。如果一定要用 pip3 安装的 pyqt5 ，就要找到匹配版本的 libfcitxplatforminputcontextplugin.so 才行。

### pyqt5 与 opencv-python 的冲突

[20.1, 20.2]
opencv-python 4.x自带了一份 qt5 库，在 python 代码中激活 opencv 时，会设置 plugin 的搜索路径（位于 site-packages/cv2/qt/plugins/ ），它会优先于 pyqt5 的 plugin 的搜索路径（位于 site-packages/PyQt5/Qt5/plugins/ ）。如果再激活 pyqt5 ，可能会导入不兼容的 plugin ，导致崩溃。例如：

```
qt.qpa.plugin: Could not load the Qt platform plugin "xcb" in "/home/duanyao/opt/venv/v39/lib/python3.9/site-packages/cv2/qt/plugins" even though it was found.
```

反之，如果要使用 opencv 的 GUI，如 `cv2.imshow()` ，也可能受到 pyqt5 的干扰，导致崩溃。例如：

```
qt.qpa.plugin: Could not find the Qt platform plugin "xcb" in ""
This application failed to start because no Qt platform plugin could be initialized. Reinstalling the application may fix this problem.
```

要使用 pyqt5 的解决方法（任选其一即可）：

1. 安装不带有 GUI 功能的 opencv-python-headless 和 opencv-contrib-python-headless ，它们不会与 pyqt5 冲突。缺点是不能再使用 cv2.imshow 等方法。

2. 设置环境变量 QT_PLUGIN_PATH=$VIRTUAL_ENV/lib/python3.9/site-packages/PyQt5/Qt5/plugins/:$VIRTUAL_ENV/lib/python3.9/site-packages/cv2/qt/plugins ，让 PyQt5 的 plugins 目录排在 opencv-python 之前，再运行 python 代码。这样会优先加载前者的同名 plugin ，不设置时则默认加载后者的。注意，不能只写前者的 plugins 目录，否则无效。$VIRTUAL_ENV 是 python venv 设置的环境变量，即 venv 的目录。请根据实际情况调整版本号。

3. 在 python 代码中确保先激活 PyQt5 而不是 opencv-python 的 qt。具体做法是导入任何 opencv 包之前，先执行以下代码：
  ```
  import sys
  from PyQt5 import QtWidgets
  QtWidgets.QApplication(sys.argv)
  ```
  创建 QApplication 对象将触发加载 PyQt5 的 plugins 。
  
理论上 qt.conf 文件也可以指定 plugin 的搜索路径，但似乎 linux python venv 的 qt 无法加载 qt.conf 文件。

要使用 opencv GUI 的方法：

1. 设置环境变量 `export QT_PLUGIN_PATH=$VIRTUAL_ENV/lib/python3.9/site-packages/cv2/qt/plugins` 。请根据实际情况调整版本号。

在无 GUI 的环境中，使用 opencv 的 GUI，如 `cv2.imshow()`，也会出现类似的崩溃，并且不能通过以上方法解决，目前看只能避免使用这类 GUI 方法。

## setuptools 和 setup.py
https://www.geeksforgeeks.org/what-is-setup-py-in-python/
https://setuptools.pypa.io/en/latest/setuptools.html

```
from setuptools import setup
  
setup(
    name='my_package',
    version='0.1',
    description='A sample Python package',
    author='John Doe',
    author_email='jdoe@example.com',
    packages=['my_package'],
    install_requires=[
        'numpy',
        'pandas',
    ],
)
```


```
python setup.py sdist bdist_wheel
```

setup.cfg
distutils

```
python setup.py build develop
```

```
pip install -e .
```
## Python Eggs
http://peak.telecommunity.com/DevCenter/PythonEggs

## Easy Install
http://peak.telecommunity.com/DevCenter/EasyInstall
