## 参考资料

[1.1] pandas API reference
https://pandas.pydata.org/pandas-docs/stable/reference/index.html

[1.2] Pandas中文网、Pandas官方中文文档
http://www.pypandas.cn

[2.1] 《动手学深度学习》第二版预览版 
https://zh-v2.d2l.ai/

[2.2] 史上最全！用Pandas读取CSV，看这篇就够了
https://cloud.tencent.com/developer/article/1856554

[3.1] Pandas: Get the Row Number from a Dataframe
https://datagy.io/pandas-get-row-number/

[3.2] How to Select Rows by Index in a Pandas DataFrame
https://www.statology.org/pandas-select-rows-by-index/

[3.3] Pandas: How to Select Rows Based on Column Values
https://www.statology.org/pandas-select-rows-based-on-column-values/

## 安装

```
pip install pandas
```

## 简单的例子
[2.1]

```
import pandas as pd
data = pd.read_csv(data_file) # type: <class 'pandas.core.frame.DataFrame'>，等效与二维数组，第一轴为行。
inputs, outputs = data.iloc[:, 0:2], data.iloc[:, 2]  # 取出（复制）0-1 和第 2 列

print(inputs)

   NumRooms Alley
0       NaN  Pave
1       2.0   NaN
2       4.0   NaN
3       NaN   NaN

inputs = inputs.fillna(inputs.mean())  # 对 'NA' 或 'NaN' 单元填充为列平均值。
print(inputs)

   NumRooms Alley
0       3.0  Pave
1       2.0   NaN
2       4.0   NaN
3       3.0   NaN

inputs = pd.get_dummies(inputs, dummy_na=True)  # get_dummies 是pandas实现one hot encode的方式
print(inputs)

   NumRooms  Alley_Pave  Alley_nan
0       3.0           1          0
1       2.0           0          1
2       4.0           0          1
3       3.0           0          1

print(inputs.values)  # 类型是 <class 'numpy.ndarray'> / float64

array([[3., 1., 0.],
       [2., 0., 1.],
       [4., 0., 1.],
       [3., 0., 1.]])
```

## 输入输出

```
data = pd.read_csv(sep=' ', header=None)
```
header=None 表示没有表头。sep=' ' 表示分割符为空格（含多个空格）。

## 基本数据结构

Pandas 的主要数据结构是 Series（一维数据）与 DataFrame （二维数据）[1.2]。

DataFrame 的每一列有列名，不能重复。每一列有统一的数据类型，如 float64，int64，str 等，但不同的列可以有不同的数据类型。
DataFrame 的每一行有行名，不能重复。
行列名没有指定时，用索引号（0,1,2）来代替。
dataFrame.columns 返回列名的列表，类型为 Series[str] 或者 RangeIndex[int]。dataFrame.columns[i] 返回一个列名，类型为 str或int。
dataFrame.dtypes 返回各列的数据类型的列表，有 float64，int64 等。

dataFrame.describe() 可以返回各列的统计摘要。.describe() 也可以用于 Series 。

dataFrame.T 返回转置的 dataFrame 。为了维持每一列有统一的数据类型，可能会转换类型，如 int64 转为 float64。

```
from pandas import DataFrame,Series

# 一维数组，转化为单列表格
DataFrame([1,2,3], columns=['c'])
   c
0  1
1  2
2  3

# 二维数组，转化为多列表格（二维数组的第一轴是行）
DataFrame([[1,2,3],[4,5,6]], columns=['c1','c2','c3'])
   c1  c2  c3
0   1   2   3
1   4   5   6

# 一维数组，转化为序列
Series([1,2,3])
0    1
1    2
2    3

# 也可以用 numpy 代替 python 数组
na = df.to_numpy() # 转换为形状 (rowCount, colCount) 的 numpy
df = DataFrame(na, columns=['a','b','c',...]) # numpy 转为 DataFrame
```

## 访问数据、查找
[3.2]

len(dataFrame) 返回行数。
dataFrame.size 返回元素数（行数x列数）。
dataFrame.columns 返回列名的列表（Index 类型）。
在 dataFrame 上迭代，得到列名。
`'col_name' in dataFrame` 或者 `'col_name' in dataFrame.columns` 检查列是否存在。
`row_label in dataFrame.index` 检查行是否存在。
在 DataFrame.items() 上迭代，得到列，(column name, Series)。
在 DataFrame.iterrows() 上迭代，得到行，(index, data: Series)。index 是行标签或行号，data 是行数据。注意，元素的数据类型可能与列不符，例如整数变成浮点数。
在 DataFrame.itertuples(index=False) 上迭代，得到行，类型为 pandas.core.frame.Pandas，实为 namedtuple，可以用 data.<列名> 的方式访问。注意，如果参数 index=True(默认)，它会多出一个 data.Index 属性。

dataFrame['col1'] 或 dataFrame.col1 访问列 col1，类型为 Series 的视图。
dataFrame[['col1','col2'...]] 切片多个列，类型为 DataFrame 。列的顺序按输入的顺序来。

dataFrame[1:3] 切片[1:3]行，返回 DataFrame 类型的副本。

dataFrame.iloc[1:3] 切片[1:3]行，返回 DataFrame 类型的副本。
dataFrame.iloc[[4]] 切片第[4:5]行，返回 DataFrame 类型的副本。等效于 dataFrame[4:5]。
dataFrame.iloc[[2, 4]] 切片第[2, 4]行。
dataFrame.iloc[1] 访问[1]行，返回 Series 类型的副本。

dataFrame.loc['row_label'] 返回行标签为 'row_label' 的行的副本。

按索引查找行：

```
dataFrame.loc[idx1] # 返回 Series
dataFrame.loc[[idx1, idx2]] # 返回 DataFrame
```
.loc 的参数是行索引（index）， .iloc 的参数是行号。

按值查找某个元素：
```
dataFrame[ dataFrame['col_name'] == val ]
```
返回满足条件的行组成的一个新的 DataFrame 。
原理是，dataFrame['col_name'] == val 返回一个 Series[bool]，在满足 `dataFrame['col_name'] == val` 的行号处是 True，其它位置是 False。
dataFrame[ Series ] 则返回值为 True 的位置上的那些行组成的 DataFrame。

按多个值查找某个元素[3.3]：
```
df.loc[df['col1'].isin([value1, value2, value3, ...])]
```
复合判断[3.3]：
```
df.loc[(df['col1'] == value) & (df['col2'] < value)]
```


## 排序
```
dataFrame.sort_values('col_name')
```

## 增删
### 增加列
```
dataFrame['col_name'] = list_like
dataFrame['col_name'] = range(len(dataFrame))
```
list_like 可以是一维数据结构，如迭代器、Index、Series、list等。

根据行号增加列：
```
df.insert(1, 'id', range(len(df)))
```

### 修改列
用一个列表（迭代器或类似列表）给整列赋值：
```
df['col1']=[25,26,27,28]

```

## 转换格式/输入输出
### 与字典/列表互相转换

```
>>> import pandas as pd
>>> dataFrame = pd.DataFrame([[1,2,3],[4,5,6]], columns=['c1','c2','c3'])

>>> dataFrame
   c1  c2  c3
0   1   2   3
1   4   5   6

>>> dataFrame.to_dict(orient='records') #  [{column -> value}, … , {column -> value}]
[{'c1': 1, 'c2': 2, 'c3': 3}, {'c1': 4, 'c2': 5, 'c3': 6}]

>>> dataFrame.to_dict(orient='index') #  {index -> {column -> value}}
{0: {'c1': 1, 'c2': 2, 'c3': 3}, 1: {'c1': 4, 'c2': 5, 'c3': 6}}

>>> dataFrame.to_dict(orient='list')
{'c1': [1, 4], 'c2': [2, 5], 'c3': [3, 6]}

>>> dataFrame.to_dict(orient='tight') # pandas 1.4 以上
{'index': [0, 1], 'columns': ['c1', 'c2', 'c3'], 'data': [[1, 2, 3], [4, 5, 6]], 'index_names': [None], 'column_names': [None]}

>>> dataFrame.to_dict(orient='split')
{'index': [0, 1], 'columns': ['c1', 'c2', 'c3'], 'data': [[1, 2, 3], [4, 5, 6]]}

# 从字典输入。from_dict() 的 orient 参数的值 ‘columns’, ‘index’, ‘tight’，对应 to_dict() 的 orient 参数的值 'list', ‘index’, ‘tight’
>>> dataFrame2 = pd.DataFrame.from_dict({0: {'c1': 1, 'c2': 2, 'c3': 3}, 1: {'c1': 4, 'c2': 5, 'c3': 6}}, orient='index')
   c1  c2  c3
0   1   2   3
1   4   5   6

>>> dataFrame2 = pd.DataFrame.from_dict({'c1': [1, 4], 'c2': [2, 5], 'c3': [3, 6]}, orient='columns')

# 列表输入。输入格式可接受 to_dict(orient='index')
>>> pd.DataFrame.from_records([{'c1': 1, 'c2': 2, 'c3': 3}, {'c1': 4, 'c2': 5, 'c3': 6}])
   c1  c2  c3
0   1   2   3
1   4   5   6

# 如果元素是列表，则需要单独指定列名：
>>> pd.DataFrame.from_records([(3, 'a'), (2, 'b'), (1, 'c'), (0, 'd')], columns=['col_1', 'col_2'])
   col_1 col_2
0      3     a
1      2     b
2      1     c
3      0     d
```

### 与 json 的互相转换
orient 参数与数据结构与 `DataFrame.to_dict / DataFrame.from_dict` 类似。
```
pandas.DataFrame.to_json(path_or_buf=None, orient=None, date_format=None, double_precision=10, force_ascii=True, date_unit='ms', default_handler=None, lines=False, compression='infer', index=None, indent=None, storage_options=None, mode='w')

pandas.read_json(path_or_buf, *, orient=None, typ='frame', dtype=None, convert_axes=None, convert_dates=True, keep_default_dates=True, precise_float=False, date_unit=None, encoding=None, encoding_errors='strict', lines=False, chunksize=None, compression='infer', nrows=None, storage_options=None, dtype_backend=_NoDefault.no_default, engine='ujson')
```

### 读写 excel/csv


`dataFrame.to_csv(file_path, index=False)` # index

https://zhuanlan.zhihu.com/p/142972462

## 算术运算
DataFrame 之间可以进行四则运算，运算发生在同行同列（判断标准是行列的名字）的元素之间。如果存在非共同的行列，则先补足再运算。

## 绘图显示

### 直方图
```
df = pd.DataFrame({

    'length': [1.5, 0.5, 1.2, 0.9, 3],

    'width': [0.7, 0.2, 0.15, 0.2, 1.1]

    }, index=['pig', 'rabbit', 'duck', 'chicken', 'horse'])

hist = df.hist(bins=3)
```
pandas 调用 matplotlib 来显示直方图，但似乎显示窗口有问题。如果是这样，可以直接用 matplotlib 来显示直方图。

