[1.1] pngquant https://pngquant.org/

## pngquant

pngquant is a command-line utility and a library for lossy compression of PNG images.

安装：debian 包名 pngquant 。

```
pngquant --verbose --quality=70 image.png
```
100 = perfect, 70 = OK, 20 = awful
