## 参考资料

[1.1] excel做直方图怎么做，2010版本 https://jingyan.baidu.com/article/7082dc1c3f3c41e40a89bd9e.html

[1.2] wps怎么做频率分布直方图和频率分布表 https://www.wps.cn/learning/room/d/192449

## 直方图

### excel
[1.1]
设置好分段点，10,50,100,150,200,250,300,350，分别表示展现量在1-10,11-50,50-100，以此类推。
调出直方图按钮。文件-选项-加载项-转到，勾选分析工具库。2013版不需要进行设置，直接可以找到数据分析按钮。
这时候能够在数据工具栏中找到数据分析了，点击数据分析-选择“直方图”。
填写数据。输入区域，选择所有分段数据，接收区域选择展现次数所有数据。勾选标志，输出区域选择一个空列，勾选图标输出。我们就能看到一个简单的直方图。

### wps
[1.2]
1. 使用countif或者使用frequency函数统计次数；
2. 计算占比；
3. 做柱形图

### OpenOffice

Define the histogram's class interval
Calculate the frequencies in each class interval using the FREQUENCY function
Create the histogram

### SQL

1. 做一个“范围”（或叫做“接收”）表，包括每个范围的下限、上限、标题。可以用子查询或者临时表构造这个表。
2. 将输入数据表与范围表连接查询，按每个范围的下限和上限筛选、计数。

例如：

输入表 (score)

```
value
58
61
70
89
83
```

范围表 (range)

```
label	low	high
0-60	0	60
60-80	60	80
80-90	80	90
90-100	90	100
```

查询语句（用字面量表的形式）：

```
select 
    label, count(value) as count
from
(
    select "0-60" as label, 0 as low, 60 as high
    union all
    select "60-80", 60, 80
    union all
    select "80-90", 80, 90
    union all
    select "90-100", 90, 100
) as `range`
left join
(
    select 58 as value
    union all
    select 61
    union all
    select 70
    union all
    select 89
    union all
    select 83
) as score
on
    value >= low and value < high
group by
    label
```

输出表
```
label	count
0-60	1
60-80	2
80-90	2
90-100	0
```
