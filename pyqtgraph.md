## 参考资料

[2.1] 用Python串口实时显示数据并绘图 pyqtgraph （详细教程） https://cloud.tencent.com/developer/article/1809695

## 安装
依赖： Python 3.7+，PyQt5 或 PySide2 或 PyQt6；numpy；。
pyqtgraph 本身是纯 python 的。

```
pip install pyqtgraph
conda install pyqtgraph
```
当前版本 0.12.4 （2022.3）。

## 简单的例子

### python 交互模式下的例子

这些例子要在 python 交互模式下运行，否则执行完会立即退出，看不到窗口。

绘图：
```
import pyqtgraph as pg
import numpy as np
x = np.random.normal(size=1000)
y = np.random.normal(size=1000)
pg.plot(x, y, pen=None, symbol='o')  ## setting pen=None disables line drawing
```


pg.plot 返回 pyqtgraph.widgets.PlotWidget.PlotWidget 对象。

显示图像：

```
import pyqtgraph as pg
import cv2

imageData = cv2.imread('xxx.jpg')
pg.image(imageData)
```

pg.image 返回 pyqtgraph.imageview.ImageView.ImageView 对象。

### 独立应用模式的例子

[2.1]
```
import pyqtgraph as pg
import numpy as np

app = pg.mkQApp()#建立app
win = pg.GraphicsLayoutWidget() #pg.GraphicsWindow()#建立窗口
win.setWindowTitle('pyqtgraph逐点画波形图')
win.resize(1200, 768)#小窗口大小
win.show() 

def updatePlot():
  x = np.random.normal(size=1000)
  y = np.random.normal(size=1000)
  curve.setData(x, y)

p = win.addPlot()

x = np.random.normal(size=1000)
y = np.random.normal(size=1000)
curve = p.plot(x, y, pen=None, symbol='o')#绘制一个图形

timer = pg.QtCore.QTimer()
timer.timeout.connect(updatePlot)#定时调用plotData函数
timer.start(100)#多少ms调用一次

app.exec_()
```

## 图像

```
imv = pg.ImageView() # pyqtgraph.imageview.ImageView.ImageView
imv_v = imv.getView() # pyqtgraph.graphicsItems.ViewBox.ViewBox.ViewBox
imv.setImage(cv2.imread('xxx.jpg'))
```
