# protobuf
## 参考资料
## debian 版本
| debian 版本 | protobuf 版本 | protobuf deb包主版本 |
| --- | --- | --- |
| 10 buster   | 3.6.1   | 17 |
| 11 bullseye | 3.12.4  | 23 |
| 12 bookworm | 3.21.12 | 32 |

| ubuntu 版本 | protobuf 版本 | protobuf deb包主版本 |
| --- | --- | --- |
| 1804 bionic   | 3.0.0   | 10 |
| 2004 focal    | 3.6.1   | 17 |
| 2204 jammy    | 3.12.4  | 23 |
