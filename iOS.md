## 参考资料
[1.1] iOS-APP提交上架流程
https://www.cnblogs.com/BK-12345/p/5232633.html

[1.2] 2017年最新版本iOS申请证书与发布流程
https://www.jianshu.com/p/d1a93a689a14

[2.1] 关于Certificate、Provisioning Profile、App ID的介绍及其之间的关系
https://www.cnblogs.com/cywin888/p/3263027.html

[3.1]
https://stackoverflow.com/questions/38993527/cocoapods-failed-to-connect-to-github-to-update-the-cocoapods-specs-specs-repo

[3.2]
https://stackoverflow.com/questions/29741404/no-suitable-records-were-found-verify-your-bundle-identifier-is-correct

[3.3] 
https://stackoverflow.com/questions/42641806/check-and-remove-unsupported-architecture-x86-64-i386-in-ipa-archive

## 编译
### pod 安装依赖
有些项目使用 pods 来管理依赖，类似 java 的 maven。

初次：
  sudo gem install cocoapods

  在xcode工程目录下：
    pod setup
    pod install

以后更新：
  pod update

可能的错误：
[!] Failed to connect to GitHub to update the CocoaPods/Specs specs repo - Please check if you are offline, or that GitHub is down

解决办法[3.1]：
cd ~/.cocoapods/repos/master
git pull
  在xcode工程目录下：
  pod install

## iOS上架流程


1、创建APP身份证（App IDs）

2、申请iOS发布证书

3、申请iOS发布描述文件

4、上传ios证书编译打包IPA

5、在iTunes Connect创建App

6、Mac Application Loader <del>Windows</del>上传IPA到App Store

7、上传好IPA回到iTunes Connect填写APP信息并提交审核。
  

网站：https://developer.apple.com/

工具：Application loader

账号：略

## 打包和上传应用
### 总体流程

1、创建APP身份证（App IDs）

2、申请iOS发布证书

3、申请iOS发布描述文件

4、上传ios证书编译打包IPA

5、在iTunes Connect创建App，或创建App 的新版本

6、Mac Application Loader 上传IPA到App Store

7、上传好IPA回到iTunes Connect填写APP信息并提交审核。

### 打包
* xcode 左上角，设备类型选择 "Generic iOS Device"
* 菜单 product -> archive
* 菜单 window->oganizer，点击 "archives"，选中刚才编译出来的结果，有两个选择：
  * 点击"upload app strore" 直接上传到 app store。
  * 点击"export" 导出 .ipa 文件，可以分享给其它 iOS 设备供测试。
    选择目的：app store 或 developmnent，根据实际情况。
    这个过程中，可能会询问签名的方式，可以选自动。

### 用 application loader 上传
application loader 仅有 mac os 版，但可以在开发机以外的机器上使用。
“交付您的应用程序”->选取，选中 export 出来的 .ipa 文件。


### 提交新版本

### 测试 .ipa 文件
连接 iOS 设备到 PC，打开 iTunes，将 ipa 文件拖放到侧栏“设备”中的iOS 设备上，即可完成安装。

### 上传错误的解决

#### No suitable records were found. verify your bundle identifier is correct
[3.2]
可能的原因：
* bundle id 与 iTunes Connect 中的不符，有拼写错误。
* iTunes Connect 中尚未建立对应 bundle id 的 app。
* iTunes Connect 中的 app 没有新建一个版本，处于可接受上传的状态。
* application loader/xcode 没有用同一个账号登录。

#### RROR ITMS-90087: "Unsupported Architectures. The executable for xxx.framework contains unsupported architectures '[x86_64]'."
原因：有的库含有多种架构（x86、arm）的代码，在测试机、模拟器上都没问题，但上传到 app store 会报错。
解决办法：给 xcode 的构建过程添加 [3.3] 的脚本，去掉 x86 架构的代码。

#### ERROR ITMS-90475: "Invalid Bundle. Your app supports Multitasking on iPad, so you must include the UILaunchStoryboardName key in your bundle, 'com.mainbo.MobileTeaching’.
原因：缺少启动图像
解决：xcode 项目节点->targets->general->app icons and lanch images->launch screen file, 选一个，例如 lauchScreen.storyboard；提供相应的 icon。
解决：info.plist 中 UIRequiresFullScreen 改为 true。

### 新建一个版本
如果版本号变更了，点击“+版本或平台”，新建一个版本。
  版本号规范：https://stackoverflow.com/questions/21125159/which-ios-app-version-build-numbers-must-be-incremented-upon-app-store-release


