## 参考资料
[1.1] NVIDIA Driver Installation Quickstart Guide
  https://docs.nvidia.com/datacenter/tesla/tesla-installation-notes/index.html
  
[1.2] NVIDIA CUDA Installation Guide for Linux
  https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html

[1.3] cuda Debian 10 install
  https://developer.nvidia.com/cuda-downloads?target_os=Linux&target_arch=x86_64&Distribution=Debian&target_version=10&target_type=deb_network

[2.1] How to manage multiple versions of Cuda and cuDNN ?
https://notesbyair.github.io/blog/cs/2020-05-26-installing-multiple-versions-of-cuda-cudnn/

[3.1] https://jansora.com/topic/deepin/deepin-nvidia-drivers

[3.2] How to Install NVIDIA Drivers using Debian Repository  https://linoxide.com/debian/install-nvidia-driver-debian/

[4.1] CUDA Toolkit 9.2 Download https://developer.nvidia.com/cuda-92-download-archive?target_os=Linux&target_arch=x86_64&target_distro=Ubuntu&target_version=1604&target_type=debnetwork

[4.2] https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#package-manager-metas

[4.3] https://developer.nvidia.com/zh-cn/cuda-downloads?target_os=Linux&target_arch=x86_64&target_distro=Debian&target_version=10&target_type=debnetwork

[5.1] Matching CUDA arch and CUDA gencode for various NVIDIA architectures
https://arnon.dk/matching-sm-architectures-arch-and-gencode-for-various-nvidia-cards/

[6.1] https://anaconda.org/numba/cudatoolkit

[7.1] cudaGetDevice() failed. Status: CUDA driver version is insufficient for CUDA runtime version解决 https://www.machunjie.com/trouble/110.html

[7.2] Bug 1530828 - Cuda 9 fails to compile when using boost 
  https://bugzilla.redhat.com/show_bug.cgi?id=1530828

[7.3] Update for CUDA version macro changes.#175
  https://github.com/boostorg/config/pull/175/files#diff-672763afc774e7888a21e9b439e15f36L17
  
[7.4] nvidia-kernel-dkms: The DKMS build fails with incompatible pointer types.
  https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=972582

[8.1] Installing the NVIDIA Container Toolkit
  https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html

[8.2] NCCL Troubleshooting
https://docs.nvidia.com/deeplearning/nccl/user-guide/docs/troubleshooting.html#known-issues

[9.1] 11 GB of GPU RAM used, and no process listed by nvidia-smi
https://forums.developer.nvidia.com/t/11-gb-of-gpu-ram-used-and-no-process-listed-by-nvidia-smi/44459/2

[9.2] CUDA No process found but GPU memory occupied 
https://i-am-future.github.io/2023/05/06/CUDA-No-process-but-GPU-memory-occupied/

[10.1] linux nvidia-xrun : 高性能显卡切换方案 HappyTomCat
https://zhuanlan.zhihu.com/p/357288904

[10.2] Setting BUSID in xorg.conf
https://unix.stackexchange.com/questions/585910/setting-busid-in-xorg-conf

[11.1] NVIDIA/Tips and tricks
https://wiki.archlinux.org/title/NVIDIA/Tips_and_tricks

## 安装 nvidia 驱动

### 安装 deepin 仓库的 nvidia 驱动

如果是全新安装的 deepin 20.2 （2021.6），选择包括 nvidia 私有驱动即可。如果错过，安装 nvidia-driver 包。
注意，这样安装后，可能会缺少 libcuda1 nvidia-smi nvidia-kernel-dkms 这几个包，需要手动安装。

```
sudo aptitude install -t apricot libcuda1 nvidia-smi nvidia-kernel-dkms
```
### 安装 debian 系发行版主仓库的 nvidia 驱动

如果发行版仓库的 nvidia 驱动的版本可以满足需要，则优先使用此仓库。
入口的软件包是 nvidia-alternative 和 nvidia-kernel-dkms 和 nvidia-driver 和 libcuda1（ 390～430 不需要指定 nvidia-driver 和 libcuda1，440 需要。libcuda1 可能在多个仓库里都有提供，安装时注意一下小版本匹配 nvidia-driver ）。nvidia-smi 可以之后选装。nvidia-legacy-check 可选升级。
```
sudo aptitude install -t buster-backports nvidia-alternative nvidia-kernel-dkms nvidia-driver libcuda1
sudo aptitude install -t buster-backports nvidia-smi
```
nvidia-kernel-dkms 是内核模块，nvidia-alternative 和 nvidia-driver 是驱动本体。libcuda1 用于支持 CUDA ，可选。

安装完后检查一下 nvidia* 包的版本（ `apt search -n nvidia | grep 已安装 ; apt search -n nvidia -t buster-backports | grep 已安装` ），
看还有没有落在旧版本的，把它也升级一下。390->440 升级时可能落下 nvidia-egl-common 。

### 安装 debian backports 仓库的 nvidia 驱动
将 debian backports 加入deb源，安装时 apt 命令带上 `-t buster-backports` 参数，其它操作参考”安装 debian 系发行版主仓库的 nvidia 驱动“。

```
sudo aptitude install -t buster-backports nvidia-alternative nvidia-kernel-dkms nvidia-driver libcuda1
sudo aptitude install -t buster-backports nvidia-smi
```

### 安装 ubuntu 仓库的 nvidia 驱动（以 22.04 / 23.04为例）
安装命令：
```
apt search -n nvidia

apt install nvidia-driver-535
# 或者
apt install nvidia-driver-535-server
```
安装完后可能需要重启一次系统才可正常使用（用 nvidia-smi 验证）。

535 是版本号，可以根据 apt search 的结果调整。apt search 得到的相关包：

```
nvidia-driver-535
nvidia-driver-535-server
nvidia-driver-535-open
nvidia-headless-535-open
nvidia-kernel-common-535
nvidia-kernel-common-535-server
nvidia-kernel-source-535-server
nvidia-utils-535-server
nvidia-dkms-535-server
xserver-xorg-video-nvidia-535
xserver-xorg-video-nvidia-535-server
```
ubuntu 没有 nvidia-kernel-dkms 包，而是有 nvidia-dkms-xxx， nvidia-kernel-source-xxx 和 nvidia-kernel-common-xxx 包，xxx 是驱动版本号。

server 版的区别似乎主要是不包含 i386 支持库，i386 支持库主要用于 wine i386 程序。

nvidia-driver-535-open（535.86.05-0ubuntu0.23.04.1）目前似乎有问题，在 ubuntu 23.04 + 6.4/6.2 内核 + 4050 GPU 上都会造成死机，nvidia-driver-535 则没有这个问题。

### 安装 nvidia 的 debian 仓库的 nvidia 驱动

如果 debian 的 debian 仓库提供的版本已经满足要求，则不建议用此方法。

按照 “nvidia 的 debian 仓库安装 cuda” 的方法添加软件源。

```
sudo aptitude install nvidia-440 libcuda1-440 -s
```
440 是驱动的版本号。

### 安装 nvidia 的单体 nvidia 驱动

如果nvidia 的 debian 仓库或者 debian 的 debian 仓库提供的版本已经满足要求，则不建议用此方法。

这需要在文本环境里安装。
事先也需要卸载 debian nvidia 驱动。
参考[3]。

### 重新编译 nvidia-kernel-dkms 内核模块
先决条件：（1）安装了当前内核版本的头文件（linux-headers-* 包，注意可能有2个，一个带 generic 字样）。（2）安装了 gcc 编译器。

安装或升级 nvidia-kernel-dkms 包后，或者升级 linux 内核后，会自动重新编译内核模块。也可以手动触发编译：

```
sudo dpkg-reconfigure nvidia-kernel-dkms # debian
sudo dpkg-reconfigure nvidia-dkms-YYY # ubuntu
sudo dpkg-reconfigure nvidia-kernel-source-YYY # YYY 是版本号和其它后缀
```
或者
```
sudo dkms autoinstall
```

内核模块位于 `/lib/modules/XXX/updates/dkms/`

如果编译中出错，可查看日志：`/var/lib/dkms/nvidia-current/XXX/build/make.log` 或者 `/var/lib/dkms/nvidia/kernel-NNN/log//make.log`。
其中一个可能的错误是 gcc 版本不符合要求，安装编译内核所用的版本的 gcc，如果安装了多个版本，选择合适的版本：

```
sudo update-alternatives --config gcc
```

另一个可能的编译错误原因是内核版本与nvidia dkms版本不兼容，例如内核版本太新。这时可以安装更高或更低版本的nvidia驱动来试试。

安装驱动之后，重新加载驱动模块：

```
sudo rmmod nvidia_drm nvidia_modeset nvidia_uvm nvidia
sudo modprobe nvidia
```

### 检查 nvidia 驱动各个包的版本一致性

```
$ apt list --installed | grep nvidia

WARNING: apt does not have a stable CLI interface. Use with caution in scripts.

glx-alternative-nvidia/未知,now 1.2.0-1-1+eagle amd64 [已安装，自动]
libegl-nvidia0/未知,now 460.73.01-1~bpo10+1 amd64 [已安装，自动]
libgl1-nvidia-glvnd-glx/未知,buster-backports,now 460.73.01-1~bpo10+1 amd64 [已安装，自动]
libglx-nvidia0/未知,now 460.73.01-1~bpo10+1 amd64 [已安装，自动]
libnvidia-eglcore/未知,now 460.73.01-1~bpo10+1 amd64 [已安装，自动]
libnvidia-glcore/未知,now 460.73.01-1~bpo10+1 amd64 [已安装，自动]
libnvidia-ml1/未知,now 460.73.01-1~bpo10+1 amd64 [已安装，自动]
libnvidia-ptxjitcompiler1/未知,now 460.73.01-1~bpo10+1 amd64 [已安装，自动]
nvidia-alternative/未知,now 460.73.01-1~bpo10+1 amd64 [已安装，自动]
nvidia-driver/未知,now 460.73.01-1~bpo10+1 amd64 [已安装]
nvidia-driver-libs/未知,now 460.73.01-1~bpo10+1 amd64 [已安装，自动]
nvidia-egl-common/未知,now 460.73.01-1~bpo10+1 amd64 [已安装，自动]
nvidia-egl-icd/未知,buster-backports,now 460.73.01-1~bpo10+1 amd64 [已安装，自动]
nvidia-installer-cleanup/未知,stable,now 20151021+9 amd64 [已安装，自动]
nvidia-kernel-common/未知,stable,now 20151021+9 amd64 [已安装，自动]
nvidia-persistenced/未知,stable,now 418.56-1 amd64 [已安装，自动]
nvidia-support/未知,stable,now 20151021+9 amd64 [已安装，自动]
nvidia-vdpau-driver/未知,now 460.73.01-1~bpo10+1 amd64 [已安装，自动]
```
如果有版本跟不上 nvidia-driver 的，应当设法升级，例如上面的 nvidia-persistenced 。

## 安装 CUDA

### 安装 nvidia 的 ubuntu 20.04 / 18.04 仓库的 cuda （旧）

适用于 ubuntu 20.04 / 18.04 。

参考

https://developer.nvidia.com/cuda-downloads?target_os=Linux&target_arch=x86_64&Distribution=Ubuntu&target_version=20.04&target_type=deb_network

https://developer.nvidia.com/cuda-downloads?target_os=Linux&target_arch=x86_64&Distribution=Ubuntu&target_version=18.04&target_type=deb_network

目前（2021.11），ubuntu 20.04 仓库对应的是 cuda 11.x ， ubuntu 18.04 仓库对应的是 cuda 10.x 。ubuntu 20.04 其实可以同时加入两个仓库，从而安装两个大版本的 cuda 。

ubuntu 20.04 仓库：
```
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin
sudo cp cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/7fa2af80.pub

sudo add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/ /"
sudo apt-get update
sudo apt install cuda-toolkit-11-5
# sudo apt-get -y install cuda # cuda 包括 cuda-toolkit-* cuda-runtime-* 等部分。
```

ubuntu 18.04 仓库：
```
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-ubuntu1804.pin
sudo cp cuda-ubuntu1804.pin /etc/apt/preferences.d/cuda-repository-pin-600
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub

sudo add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/ /"
sudo apt-get update
sudo apt install cuda-toolkit-10-2
```

### 安装 nvidia 的 ubuntu 20.04 / 22.04 仓库的 cuda（新）
可以浏览 https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/ ，或者 https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64/ 找到最新的 cuda-keyring_xxx.deb，下载和安装它，这个包里有 apt 仓库地址和签名。

```
wget https://developer.download.nvidia.cn/compute/cuda/repos/ubuntu2004/x86_64/cuda-keyring_1.1-1_all.deb
sudo dpkg --force-confask -i ./cuda-keyring_1.1-1_all.deb
dpkg -L cuda-keyring
cat /etc/apt/sources.list.d/cuda-ubuntu2004-x86_64.list
deb [signed-by=/usr/share/keyrings/cuda-archive-keyring.gpg] https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/ /
```
注意：cuda-keyring 的 `/etc/apt/sources.list.d/cuda-ubuntu2004-x86_64.list` 和 `/etc/apt/preferences.d/cuda-repository-pin-600` 文件被标记为 "conffiles"（意为配置文件），卸载 cuda-keyring 时不会删除它们，如果需要，可手动删除。但如果你手动删除后，下次再安装 cuda-keyring 时默认不会安装这些文件，这时就要给 加上 `--force-confask` 或者 `--force-confnew` 来安装。

对于 PC或笔记本，最好还是用发行版的 driver 版本，因此需要降低 nvidia 仓库的优先级。
给 /etc/apt/preferences.d/cuda-repository-pin-600 改名：
```
mv /etc/apt/preferences.d/cuda-repository-pin-600 /etc/apt/preferences.d/cuda-repository-pin-600.bk
```
创建 `/etc/apt/preferences.d/nvidia.custom.pref`：
```
echo "# nvidia 仓库的 driver, cuda, cudnn, tensorrt. driver 版本一般比发行版的要新，对于 PC或笔记本，最好还是用发行版的 driver 版本。
Package: *
Pin: origin developer.download.nvidia.*
Pin-Priority: 400
" > /etc/apt/preferences.d/nvidia.custom.pref
```

然后可以安装 cuda-toolkit、cudnn、libnvinfer 等。
```
sudo apt update
sudo apt install cuda-toolkit-11-8
```
它含有仓库的最新签名和仓库定义。如果以前就存在包含 https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/ 的 /etc/apt/sources.list.d/xxx.list ，可以改名或删除。

### 安装 nvidia 的 debian 10 仓库的 cuda（旧）

```
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/debian10/x86_64/7fa2af80.pub
```
自行创建 /etc/apt/sources.list.d/cuda.list ，内容是 
```
deb [by-hash=no] https://developer.download.nvidia.com/compute/cuda/repos/debian10/x86_64/ /
```
注意 `[by-hash=no]`。可能是因为 nvidia 的仓库有bug，所以不能让 apt 启用 by-hash 功能，否则执行 `apt update` 会报错：

```
E: 无法下载 https://developer.download.nvidia.cn/compute/cuda/repos/debian10/x86_64/by-hash/SHA256/74133eebd35ebb07c284502e8980fdb2475f8a223104e80c60b1a35bcbfe4d34  404  Not Found [IP: 123.134.184.166 443]
E: 部分索引文件下载失败。如果忽略它们，那将转而使用旧的索引文件。
```
安装 cuda-toolkit：

```
sudo apt update
sudo aptitude install --without-recommends cuda-toolkit-10-0
```

可能的错误：

```
sudo apt update

W: GPG 错误：https://developer.download.nvidia.cn/compute/cuda/repos/debian10/x86_64  InRelease: 由于没有公钥，无法验证下列签名： NO_PUBKEY A4B469963BF863CC
E: 仓库 “https://developer.download.nvidia.com/compute/cuda/repos/debian10/x86_64  InRelease” 的签名不再生效。
```

这是因为nvidia提供的公钥过期了，可以删除旧公钥，用“安装 nvidia 的 debian 10/11 仓库的 cuda ”的方法安装：
```
sudo apt-key del 7fa2af80
```

### 安装 nvidia 的 debian 10/11 仓库的 cuda
参考[1.3]。
注意：nvidia 的 debian 10/11 仓库不包括 libnccl 和 tensorrt（libnvinfer），所以建议 debian 系也使用 nvidia 的 ubuntu 仓库。

debian 10 仓库里有 cuda 11.1~12.5，debian 11 仓库里有 cuda 11.5~12.5 （2024.6.25）。

删除旧公钥
```
sudo apt-key del 7fa2af80
```
删除旧的自建 `/etc/apt/sources.list.d/cuda.list`，如果存在。

对于 debian 10:
```
wget https://developer.download.nvidia.com/compute/cuda/repos/debian10/x86_64/cuda-keyring_1.0-1_all.deb
sudo dpkg -i cuda-keyring_1.0-1_all.deb
```

这个包会安装一个文件 `/etc/apt/sources.list.d/cuda-debian10-x86_64.list`
```
deb [signed-by=/usr/share/keyrings/cuda-archive-keyring.gpg] https://developer.download.nvidia.com/compute/cuda/repos/debian10/x86_64/ /
```

对于 debian 11：
```
wget https://developer.download.nvidia.com/compute/cuda/repos/debian11/x86_64/cuda-keyring_1.1-1_all.deb
sudo dpkg -i cuda-keyring_1.1-1_all.deb

dpkg -L cuda-keyring
/etc/apt/sources.list.d/cuda-debian11-x86_64.list
/usr/share/keyrings/cuda-archive-keyring.gpg

cat /etc/apt/sources.list.d/cuda-debian11-x86_64.list

deb [signed-by=/usr/share/keyrings/cuda-archive-keyring.gpg] https://developer.download.nvidia.com/compute/cuda/repos/debian11/x86_64/ /
```

对于 PC或笔记本，最好还是用发行版的 driver 版本，因此需要降低 nvidia 仓库的优先级。对于 debian 10/11:
创建 `/etc/apt/preferences.d/nvidia.custom.pref`：
```
# nvidia 仓库的 driver, cuda, cudnn, tensorrt. driver 版本一般比发行版的要新，对于 PC或笔记本，最好还是用发行版的 driver 版本。
Package: *
Pin: origin developer.download.nvidia.*
Pin-Priority: 400
```

### 安装 nvidia 的 ubuntu 16.04 仓库的 cuda *

从 [4.1] 下载 cuda-repo-ubuntu1604_9.2.148-1_amd64.deb ，安装。其实 9.2.148-1 这个版本号无关紧要，它包含了所有的 cuda 版本。ubuntu1604 是操作系统版本，最好与自己的 debian 版本大致符合。

它实际上安装了1个源：
```
$ cat /etc/apt/sources.list.d/cuda.list
deb http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64 /
```
也可以不安装 cuda-repo-ubuntu1604_9.2.148-1_amd64.deb ，而是自行创建 /etc/apt/sources.list.d/cuda.list 。

注意，后来服务器协议改成了 https，cuda.list 应为：

```
deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64 /
```

安装 cuda
```
sudo apt install dirmngr
sudo apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub
sudo apt-get update

sudo aptitude install --without-recommends cuda-toolkit-10-0

下列“新”软件包将被安装。
cuda-command-line-tools-10-0{a} cuda-compiler-10-0{a} cuda-cublas-10-0{a} cuda-cublas-dev-10-0{a} cuda-cudart-10-0{a} cuda-cudart-dev-10-0{a} cuda-cufft-10-0{a} 
  cuda-cufft-dev-10-0{a} cuda-cuobjdump-10-0{a} cuda-cupti-10-0{a} cuda-curand-10-0{a} cuda-curand-dev-10-0{a} cuda-cusolver-10-0{a} cuda-cusolver-dev-10-0{a} 
  cuda-cusparse-10-0{a} cuda-cusparse-dev-10-0{a} cuda-documentation-10-0{a} cuda-driver-dev-10-0{a} cuda-gdb-10-0{a} cuda-gpu-library-advisor-10-0{a} 
  cuda-libraries-dev-10-0{a} cuda-license-10-0{a} cuda-memcheck-10-0{a} cuda-misc-headers-10-0{a} cuda-npp-10-0{a} cuda-npp-dev-10-0{a} cuda-nsight-10-0{a} 
  cuda-nsight-compute-10-0{a} cuda-nvcc-10-0{a} cuda-nvdisasm-10-0{a} cuda-nvgraph-10-0{a} cuda-nvgraph-dev-10-0{a} cuda-nvjpeg-10-0{a} cuda-nvjpeg-dev-10-0{a} 
  cuda-nvml-dev-10-0{a} cuda-nvprof-10-0{a} cuda-nvprune-10-0{a} cuda-nvrtc-10-0{a} cuda-nvrtc-dev-10-0{a} cuda-nvtx-10-0{a} cuda-nvvp-10-0{a} cuda-samples-10-0{a} 
  cuda-toolkit-10-0 cuda-tools-10-0{a} cuda-visual-tools-10-0{a} freeglut3-dev{a} libglu1-mesa-dev{a} 
```

安装后，主要安装目录是 /usr/local/cuda-10.0 ，并且有符号链接 /usr/local/cuda 指向它。

nvcc 等命令行工具在 /usr/local/cuda-10.0/bin 目录下，可以将它或者 /usr/local/cuda/bin 加入 PATH .

### debian 的 debian 仓库的 cuda

入口的软件包是 nvidia-cuda-dev nvidia-cuda-toolkit libcupti-dev ， libcupti-dev 版本号与 nvidia-cuda-toolkit 一致，但并非硬性依赖。

```
sudo aptitude install --without-recommends -t buster-backports nvidia-cuda-dev nvidia-cuda-toolkit
sudo aptitude install --without-recommends -t buster-backports libcupti-dev
```

buster-backports 中 cuda 的版本是 9.1 (2020-0212).

安装后的包是

```
$ apt-rdepends --print-state --state-follow=none nvidia-cuda-dev
Reading package lists... Done
Building dependency tree       
Reading state information... Done
nvidia-cuda-dev
  Depends: libaccinj64-9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libcublas9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libcudart9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libcufft9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libcufftw9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libcuinj64-9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libcurand9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libcusolver9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libcusparse9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libnppc9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libnppial9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libnppicc9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libnppicom9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libnppidei9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libnppif9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libnppig9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libnppim9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libnppist9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libnppisu9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libnppitc9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libnpps9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libnvblas9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libnvgraph9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libnvrtc9.1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libnvtoolsext1 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libnvvm3 (= 9.1.85-8~bpo9+1) [Installed]
  Depends: libthrust-dev [Installed]

$ apt-rdepends --print-state --state-follow=none nvidia-cuda-toolkit
Reading package lists... Done
Building dependency tree        
Reading state information... Done
nvidia-cuda-toolkit
  Depends: clang (<< 1:5~) [NotInstalled]
  Depends: clang-3.8 [Installed]
  Depends: clang-3.9 [NotInstalled]
  Depends: clang-4.0 [NotInstalled]
  Depends: g++-4.8 [NotInstalled]
  Depends: g++-4.9 [Installed]
  Depends: g++-5 [NotInstalled]
  Depends: g++-6 [Installed]
  Depends: gcc-4.8 [NotInstalled]
  Depends: gcc-4.9 [Installed]
  Depends: gcc-5 [NotInstalled]
  Depends: gcc-6 [Installed]
  Depends: libc6 (>= 2.4) [Installed]
  Depends: libgcc1 (>= 1:3.0) [Installed]
  Depends: libnvvm3 (>= 7.0) [Installed]
  Depends: libstdc++6 (>= 4.1.1) [Installed]
  Depends: nvidia-cuda-dev (= 9.1.85-8~bpo9+1) [Installed]
  Depends: nvidia-opencl-dev (= 9.1.85-8~bpo9+1) [NotInstalled]
  Depends: nvidia-profiler (= 9.1.85-8~bpo9+1) [Installed]
  Depends: opencl-dev [NotInstalled]
```

安装位置是：/usr/lib/nvidia-cuda-toolkit/
命令工具是：/usr/lib/nvidia-cuda-toolkit/bin/nvcc

#### 卸载 cuda

```
sudo apt remove nvidia-cuda-dev nvidia-cuda-toolkit libcupti-dev
sudo apt autoremove
```

### conda/miniconda 安装 cuda

```
conda install cudatoolkit=9.0
conda install cudatoolkit=10.0
conda install -c numba cudatoolkit=9.1
```

注意，默认仓库中 cuda 的版本可能不齐全，例如缺少 9.1（但是有 9.0 和 9.2）。
numba 仓库版本是全的。

从 conda 里安装 tensorflow 或 pytorch 时，也会同时安装匹配 tensorflow 或 pytorch 的 cuda 包，包括 cudatoolkit, cudnn, cupti 等。

conda 里的 cuda 的版本是与 tensorflow 或 pytorch 的版本相关的，与主机环境里的 cuda 版本无关。

conda 里的 cudatoolkit 并不包括 nvcc 编译器。如果某个程序需要编译 cu 源文件，则会使用主机环境的 nvcc。
这就可能造成编译出来的 cuda 模块依赖的是主机环境的 cuda 版本，而其它部分依赖 conda 的 cuda 版本。
这可能会造成运行失败，一般是 `ImportError: xxx.so: undefined symbol: _YYY` ，其中 xxx.so 是编译好的 cuda 模块。
主机的 nvcc 和 conda 的 cudatoolkit 版本不同，不管哪个大哪个小都可能造成问题（比较到小版本，如 10.0）。

## 安装 CUDNN
### 包名和大小
目前（2024.6），cudnn 的大版本有 7, 8, 9 ，各个大版本可以共存。每个 cudnn 包还绑定具体的 cuda 小版本，绑定不同 cuda 大版本的 cudnn 无法同时安装。
例如：
```
libcudnn7-7.5.1.10-1+cuda10.0
libcudnn7-dev-7.5.1.10-1+cuda10.0
libcudnn8-8.3.1.22-1+cuda11.5
libcudnn8-dev-8.3.1.22-1+cuda11.5
```
而 cudnn9 的包名改变了
```
cudnn9-cuda-11-8
cudnn9-cuda-12-4
```
避免自动升级绑定具体的 cuda 小版本：
```
sudo apt-mark hold libcudnn8 libcudnn8-dev
```
安装大小：
libcudnn8-8.9.7.29-1+cuda11.8 ：441 MB 压缩/1092 MB 安装。

### nvidia 的 ubuntu 仓库

按照安装 cuda 的仓库的方法设置仓库，已经设置过就不用了。
安装：
```
apt list -a libcudnn8 libcudnn8-dev
sudo apt-get install libcudnn8=${cudnn_version}-1+${cuda_version} libcudnn8-dev=${cudnn_version}-1+${cuda_version}
```

其中：
${cudnn_version} 是 8.x
${cuda_version} 是 cuda10.x or cuda11.x

例如：
```
sudo apt install libcudnn8=8.3.1.22-1+cuda11.5 libcudnn8-dev=8.3.1.22-1+cuda11.5
sudo apt install libcudnn8=8.3.1.22-1+cuda10.2 libcudnn8-dev=8.3.1.22-1+cuda10.2
```
注意，绑定不同 cuda 大版本的 cudnn 无法同时安装。

### nvidia 的 debian 仓库安装 cudnn 

注：nvidia 的 debian 11 cuda 仓库应该是包含 cudnn 的，不用单独设置。debian 10 应设置如下：

创建 /etc/apt/sources.list.d/cudnn.list，内容是：

```
deb https://developer.download.nvidia.cn/compute/machine-learning/repos/ubuntu1604/x86_64 /
```
或者：
```
deb https://developer.download.nvidia.cn/compute/machine-learning/repos/ubuntu1804/x86_64 /
```
具体看你的 debian 系统版本。debian 10 可以用 ubuntu1804 的仓库。

安装：
```
sudo apt update
apt search -n cudnn
apt list -a libcudnn7  # 列出 cudnn7 的版本号，注意其依赖的版本号没有写入包依赖，所以要自己指定。
apt list -a libcudnn8  # 同上
sudo apt install libcudnn7=7.5.1.10-1+cuda10.0 libcudnn7-dev=7.5.1.10-1+cuda10.0   # 安装 libcudnn7 和 libcudnn7-dev，对应 cuda10.0
#sudo apt install libcudnn7=7.6.5.32-1+cuda10.0 libcudnn7-dev=7.6.5.32-1+cuda10.0
```
在 nvidia 的 debian 11 cuda 仓库中，没有 cudnn7，有 8 和 9 。

### 在 debian 10 上使用 nvidia ubuntu 仓库安装 cudnn

目前可以看到，nvidia cudnn 仓库支持的发行版不多：
https://developer.download.nvidia.cn/compute/machine-learning/repos/

```
rhel7/
rhel8/
ubuntu1404/
ubuntu1604/
ubuntu1804/
ubuntu2004/ 
```

删除自建的 `/etc/apt/sources.list.d/cudnn.list`，如果存在。

```
sudo apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64/7fa2af80.pub
wget https://developer.download.nvidia.cn/compute/machine-learning/repos/ubuntu1804/x86_64/nvidia-machine-learning-repo-ubuntu1804_1.0.0-1_amd64.deb
sudo dpkg -i nvidia-machine-learning-repo-ubuntu1804_1.0.0-1_amd64.deb
sudo apt update
```

```
apt list -a libcudnn8 libcudnn8-dev
sudo apt install libcudnn8=8.0.4.30-1+cuda11.1 libcudnn8-dev=8.0.4.30-1+cuda11.1
```

## 安装 NCCL （多卡运用必需）

### 简介
NVIDIA Collective Communications Library (NCCL)，用于多卡任务之间的通信，是多卡运用必需。

### 在 debian 系的系统中安装

在 debian 系的系统中，NCCL 在 cuda 或 cudnn 的仓库中。先参照 cuda/cudnn 的安装方法，配置仓库，然后安装 NCCL：
```
apt search -n nccl
apt list -a libnccl-dev
apt list -a libnccl2
sudo apt install libnccl-dev=2.5.6-1+cuda10.0 libnccl2=2.5.6-1+cuda10.0
```
NCCL 的版本号中的 cuda 版本号的二级版本号可以比实际安装的 cuda 低，但主版本号应该一致。

### docker 容器中运用多卡的 NCCL 相关问题

一个程序在容器内使用多 GPU 时，可能报错，如果报错与 nccl 有关，且指向不明，可以进一步用 `NCCL_DEBUG=WARN` 让 nccl 输出警告信息，例如 paddle：
```
NCCL_DEBUG=WARN python -c "import paddle; paddle.utils.run_check()"
```
出错信息可能是：
```
a4881a87de8e:9:59 [1] include/shm.h:28 NCCL WARN Call to posix_fallocate failed : No space left on device

a4881a87de8e:9:59 [1] include/shm.h:48 NCCL WARN Error while creating shared memory segment nccl-shm-recv-662d9f0ae52cdf1-1-0-1 (size 9637888)
```
这是因为 docker 默认的共享内存太小。可以加上 `--shm-size=1g --ulimit memlock=-1`：

```
docker run --gpus all --shm-size=1g --ulimit memlock=-1 -it --rm local/ubuntu2004-cuda112-cudnn8-dev-py39-cv-paddle242cu112 bash

NCCL_DEBUG=WARN python -c "import paddle; paddle.utils.run_check()"
```

## 安装 tensorrt（libnvinfer）

### 包名和大小

tensorrt 的包名是 `libnvinfer*`，目前（2024.6）有两个大版本8、10，可以共存：
```
libnvinfer8-8.6.1.6-1+cuda11.8
libnvinfer-plugin8-8.6.1.6-1+cuda11.8
libnvonnxparsers8-8.6.1.6-1+cuda11.8 # 可选，如果需要用 tensorrt 运行 onnx 模型。

libnvinfer10-10.1.0.27-1+cuda11.8
libnvinfer-plugin10-10.1.0.27-1+cuda11.8
libnvonnxparsers10-10.1.0.27-1+cuda11.8
```
而 dev 包的大版本不可共存：
```
libnvinfer-dev-{8.6.1.6-1+cuda11.8|10.1.0.27-1+cuda12.4}
libnvinfer-plugin-dev-{8.6.1.6-1+cuda11.8|10.1.0.27-1+cuda12.4}
```
其中， libnvinfer8 提供 libnvinfer.so.8， 而 libnvinfer-dev 提供 libnvinfer.so（指向 libnvinfer.so.8.x.x ）。
有些程序依赖 libnvinfer.so ，就需要安装 libnvinfer-dev 而不仅是 libnvinferX 。
类似的，libnvinfer-plugin-dev 提供 libnvinfer_plugin.so 

例如，PaddlePaddle 就依赖 libnvinfer.so 和 libnvinfer_plugin.so（见 /paddle/phi/backends/dynload/tensorrt.cc#L67 和 https://github.com/PaddlePaddle/Paddle/blob/release/2.4/paddle/fluid/platform/dynload/tensorrt.cc:60,71）

libnvinfer 版本号中的 cuda 版本号（上面的 +cuda11.8）可以比实际安装的 cuda 版本号略高，最好不要低，否则运行时可能找不到 libnvinfer.so 和 libnvinfer_plugin.so 。

避免被自动升级绑定的cuda版本号而导致不兼容
```
sudo apt-mark hold libnvinfer-plugin10 libnvinfer10 libnvonnxparsers10
```

安装大小：
libnvinfer10-10.1.0.27-1+cuda11.8: 662 MB 的归档，解压缩后 1602 MB。
libnvinfer-plugin 和 libnvonnxparsers 大小可忽略。

### （推荐）安装 tensorrt（用 nvidia cuda 仓库）

```
sudo apt install libnvinfer8=8.6.1.6-1+cuda11.8 libnvinfer-dev=8.6.1.6-1+cuda11.8 #libnvinfer-headers-dev=8.6.1.6-1+cuda11.8
sudo apt install libnvinfer-plugin8=8.6.1.6-1+cuda11.8 libnvinfer-plugin-dev=8.6.1.6-1+cuda11.8 #libnvinfer-headers-plugin-dev=8.6.1.6-1+cuda11.8
sudo apt install libnvonnxparsers8=8.6.1.6-1+cuda11.8 # 可选，如果需要用 tensorrt 运行 onnx 模型。
```

其中， libnvinfer8 提供 libnvinfer.so.8， 而 libnvinfer-dev 提供 libnvinfer.so（指向 libnvinfer.so.8.x.x ）。
有些程序依赖 libnvinfer.so ，就需要安装 libnvinfer-dev 而不仅是 libnvinferX 。
类似的，libnvinfer-plugin-dev 提供 libnvinfer_plugin.so 

例如，PaddlePaddle 就依赖 libnvinfer.so 和 libnvinfer_plugin.so（见 /paddle/phi/backends/dynload/tensorrt.cc#L67 和 https://github.com/PaddlePaddle/Paddle/blob/release/2.4/paddle/fluid/platform/dynload/tensorrt.cc:60,71）

libnvinfer 版本号中的 cuda 版本号（上面的 +cuda11.8）可以比实际安装的 cuda 版本号略高，最好不要低，否则运行时可能找不到 libnvinfer.so 和 libnvinfer_plugin.so 。

libnvinfer10 的安装：
```
sudo apt install libnvinfer10=10.1.0.27-1+cuda11.8
sudo apt install libnvinfer-plugin10=10.1.0.27-1+cuda11.8
sudo apt install libnvonnxparsers10=10.1.0.27-1+cuda11.8
# 避免被自动升级而导致不兼容
sudo apt-mark hold libnvinfer-plugin10 libnvinfer10 libnvonnxparsers10
```

### 安装 tensorrt（用 tensorrt local repo）

从 https://developer.nvidia.com/nvidia-tensorrt-download 下载 tensorrt sdk。下载前需要注册 nvidia developer 帐号。
之后选择要下载的版本，例如“TensorRT 8.6 GA for Ubuntu 20.04 and CUDA 11.0, 11.1, 11.2, 11.3, 11.4, 11.5, 11.6, 11.7 and 11.8 DEB local repo Package”。
其文件名 nv-tensorrt-local-repo-ubuntu2004-8.6.1-cuda-11.8_1.0-1_amd64.deb ，大约 1.4 GB。

安装 local-repo
```
os="ubuntuxx04"
tag="8.x.x-cuda-x.x"
sudo dpkg -i nv-tensorrt-local-repo-${os}-${tag}_1.0-1_amd64.deb
sudo cp /var/nv-tensorrt-local-repo-${os}-${tag}/*-keyring.gpg /usr/share/keyrings/
sudo apt-get update
```
安装 tensorrt 组件：
```
For full runtime

    sudo apt-get install tensorrt

For the lean runtime only, instead of tensorrt

    sudo apt-get install libnvinfer-lean8
    sudo apt-get install libnvinfer-vc-plugin8

For the lean runtime only

    sudo apt-get install libnvinfer-lean8

For the dispatch runtime only

    sudo apt-get install libnvinfer-dispatch8

For only running TensorRT C++ applications

    sudo apt-get install tensorrt-libs

For also building TensorRT C++ applications

    sudo apt-get install tensorrt-dev

For also building TensorRT C++ applications with lean only

    sudo apt-get install libnvinfer-lean-dev

For also building TensorRT C++ applications with dispatch only

    sudo apt-get install libnvinfer-dispatch-dev
```

### 最小化安装 cuda / cuda-toolkit

最小化安装 cuda 后，ld.so.cache 可能没有自动刷新，造成 cuda 的 so 文件无法加载。可以手动刷新一下：
```
sudo ldconfig -v
```
相关文件在
```
/etc/ld.so.conf.d/000_cuda.conf
/etc/ld.so.conf.d/989_cuda-11.conf
```

#### 安装 libcublas （cuda-toolkit 的部件）
包名和大小
版本号与 cuda-toolkit 一致，例如
```
libcublas-11-8
libcublas-12-5
libcublas-dev-11-8
libcublas-dev-12-5
libcublasmp0-cuda-11
libcublasmp0-cuda-12
libcublasmp0-dev-cuda-11
libcublasmp0-dev-cuda-12
```

安装大小：
libcublas-11-8-11.11.3.6-1: 248 MB 的归档，解压缩后 670 MB。

#### 安装 cudart （cuda-toolkit 的部件）
版本号与 cuda-toolkit 一致，例如
```
cuda-cudart-11-8/未知 11.8.89-1 amd64
cuda-cudart-12-5/未知 12.5.39-1 amd64
cuda-cudart-dev-11-8/未知 11.8.89-1 amd64
cuda-cudart-dev-12-5/未知 12.5.39-1 amd64
```
安装大小：
cuda-cudart-11-8-11.8.89-1: 165 kB 的归档。解压缩后 791 kB。

提供：
/usr/local/cuda-11.8/targets/x86_64-linux/lib/libOpenCL.so.1
/usr/local/cuda-11.8/targets/x86_64-linux/lib/libOpenCL.so.1.0
/usr/local/cuda-11.8/targets/x86_64-linux/lib/libcudart.so.11.0

#### 安装 libcurand （cuda-toolkit 的部件）
版本号与 cuda-toolkit 一致，例如
```
libcurand-11-8
libcurand-dev-11-8
```
提供
/usr/local/cuda-11.8/targets/x86_64-linux/lib/libcurand.so.10

libcurand-11-8_10.3.0.86-1: 42.2 MB 的归档，解压缩后 101 MB。

#### 安装 libcufft （cuda-toolkit 的部件）
版本号与 cuda-toolkit 一致，例如
libcufft-11-8
/usr/local/cuda-11.2/targets/x86_64-linux/lib/libcufft.so.10

94.2 MB 的归档，解压缩后 281 MB。

## 验证安装

```
cat /proc/driver/nvidia/version   #显示驱动版本

nvcc --version #nvcc版本
/usr/local/cuda/bin/nvcc --version  #nvcc版本，debian 仓库版本没有。
/usr/local/cuda-10.0/bin/nvcc --version #
```
nvidia-smi 命令的输出的左上角显示 CUDA 版本号。

nvidia-smi 可能会报错：
```
Failed to initialize NVML: Driver/library version mismatch
```

看内核日志：
```
journalctl -k -e

12月 02 20:05:30 gtrd kernel: NVRM: API mismatch: the client has the version 470.86, but
                               NVRM: this kernel module has the version 470.57.02.  Please
                               NVRM: make sure that this kernel module and all NVIDIA driver
                               NVRM: components have the same version.
```
这说明可能是升级了 NVIDIA driver，但还没重启系统，内核模块是旧版本；或者 NVIDIA driver 的版本低了，应安装一个更新的版本。


为了验证 cuDNN，可以跑 nvidia 提供的 cudnn-samples ：

```
git clone https://github.com/sbaktha/cudnn-samples.git sbaktha-cudnn-samples.git
cd sbaktha-cudnn-samples.git/cudnn_samples_v8/mnistCUDNN
make
./mnistCUDNN
```
显示 Test passed! 就表示测试成功。

v8 应该是表示 cudnn 8。

此外还有 cudnn_samples_v7/mnistCUDNN ，但在 RTX 30 系列显卡上编译不通过：

```
nvcc fatal   : Unsupported gpu architecture 'compute_30'
```

cudnn_samples_v7/samples_common.mk

SMS ?= 30 35 50 53 60 61 62 $(SMS_VOLTA)

## cuda 库的搜索路径

cuda 使用 /etc/ld.so.conf.d/*cuda*.conf 文件来定义额外的库搜索路径，例如 cuda 11 的：
```
/usr/local/cuda/targets/x86_64-linux/lib
/usr/local/cuda-11/targets/x86_64-linux/lib
```

## 版本兼容性，机器学习框架对 cuda 版本号的依赖

cuda 号称具有向后兼容性，即在旧版本 cuda 上编译的程序可以在新版 cuda 上运行。
但实际情况未必如此理想，有些机器学习框架并不能运行在比它要求的 cuda 版本更高的 cuda 上。

* pytorch: pip 安装包不依赖特定 cuda 版本。检查过 pytorch 1.9 和 cuda 11.3 。

* tensorflow: pip 安装包依赖特定 cuda 二级版本。例如 tensorflow-gpu-2.4.0 依赖 cuda 11.0 。如果 cuda 版本号更高或许可行。

* mxnet: pip 安装包依赖特定 cuda 二级版本。例如 mxnet-cu110-1.8.0.post0 依赖 cuda 11.0 ，高或低都不行。
  如果 cuda 版本较高，创建低版本的 .so 的符号链接并设置 LD_LIBRARY_PATH 也不一定可行（ mxnet-cu110-1.8.0.post0 不行）。


## cuda/cudnn 多版本切换
[2.1] 

```
root@gtrd:/home/rd# update-alternatives --install /usr/local/cuda-10 cuda-10 /usr/local/cuda-10.2 30
update-alternatives: 使用 /usr/local/cuda-10.2 来在自动模式中提供 /usr/local/cuda-10 (cuda-10)
root@gtrd:/home/rd# update-alternatives --install /usr/local/cuda cuda /usr/local/cuda-10 30
root@gtrd:/home/rd# update-alternatives --config cuda
有 2 个候选项可用于替换 cuda (提供 /usr/local/cuda)。

  选择       路径                优先级  状态
------------------------------------------------------------
* 0            /usr/local/cuda-11.5   115       自动模式
  1            /usr/local/cuda-10     30        手动模式
  2            /usr/local/cuda-11.5   115       手动模式

要维持当前值[*]请按<回车键>，或者键入选择的编号：1
update-alternatives: 使用 /usr/local/cuda-10 来在手动模式中提供 /usr/local/cuda (cuda)
root@gtrd:/home/rd# ls -l /usr/local/cuda
```

## cuda 工具

bandwidthTest
deviceQuery

## GPU + docker
宿主机需要安装：
* 显卡驱动，包名 nvidia-driver。
* cuda 驱动，包名 libcuda1 。版本号应当与 nvidia-driver 一致。无需安装 cuda-toolkit。
* docker 的 gpu 插件，包名 nvidia-container-toolkit 。安装方法见下面。
* docker 服务，包名 docker-ce。

容器内需要安装：
* cuda 本体，包名 cuda-toolkit
* cudnn
* nccl
* （可选）tensorrt，包名 libnvinfer 
* （可选）tensorrt 的 onnx 支持。包名 libnvonnxparsers 。
[8.1]
```
curl -fsSL https://nvidia.github.io/libnvidia-container/gpgkey | sudo gpg --dearmor -o /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg \
  && curl -s -L https://nvidia.github.io/libnvidia-container/stable/deb/nvidia-container-toolkit.list | \
    sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://#g' | \
    sudo tee /etc/apt/sources.list.d/nvidia-container-toolkit.list


sudo apt-get update
sudo apt-get install -y nvidia-container-toolkit

# The nvidia-ctk command modifies the /etc/docker/daemon.json file on the host. The file is updated so that Docker can use the NVIDIA Container Runtime.
sudo nvidia-ctk runtime configure --runtime=docker
sudo systemctl restart docker


# The nvidia-ctk command modifies the /etc/containerd/config.toml file on the host. The file is updated so that containerd can use the NVIDIA Container Runtime.
sudo nvidia-ctk runtime configure --runtime=containerd
sudo systemctl restart containerd

# The nvidia-ctk command modifies the /etc/crio/crio.conf file on the host. The file is updated so that CRI-O can use the NVIDIA Container Runtime.
sudo nvidia-ctk runtime configure --runtime=crio
sudo systemctl restart crio

# For Podman, NVIDIA recommends using CDI for accessing NVIDIA devices in containers.
# https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/cdi-support.html
```

之后，docker run 命令加上 --gpus all 参数即可使用 GPU 。如果要指定部分 GPU 被 docker 使用，可以写成 `docker run --gpus device=2,3`（序号从 0 开始），或者 

早前还有 nvidia-docker 和 nvidia-docker2 工具，是修改版的 docker，已经被这个 docker 插件 nvidia-container-toolkit 取代了。

## CPU 架构和 cuda 编译器 nvcc 的选项

```
Fermi†	Kepler†	Maxwell‡	Pascal	Volta	Turing	Ampere	Lovelace*	Hopper**
sm_20	sm_30	sm_50	sm_60	sm_70	sm_75	sm_80	sm_90?	sm_100c?
	sm_35	sm_52	sm_61	sm_72		sm_86		
	sm_37	sm_53	sm_62					

† Fermi and Kepler are deprecated from CUDA 9 and 11 onwards
‡ Maxwell is deprecated from CUDA 12 onwards
* Lovelace is the microarchitecture replacing Ampere (AD102)
** Hopper is NVIDIA’s rumored “tesla-next” series, with a 5nm process.
```

When you compile CUDA code, you should always compile only one ‘-arch‘ flag that matches your most used GPU cards. This will enable faster runtime, because code generation will occur during compilation.
If you only mention ‘-gencode‘, but omit the ‘-arch‘ flag, the GPU code generation will occur on the JIT compiler by the CUDA driver.

When you want to speed up CUDA compilation, you want to reduce the amount of irrelevant ‘-gencode‘ flags. However, sometimes you may wish to have better CUDA backwards compatibility by adding more comprehensive ‘-gencode‘ flags.

Pascal (CUDA 8 and later)

SM60 or SM_60, compute_60 –
Quadro GP100, Tesla P100, DGX-1 (Generic Pascal)
SM61 or SM_61, compute_61–
GTX 1080, GTX 1070, GTX 1060, GTX 1050, GTX 1030 (GP108), GT 1010 (GP108) Titan Xp, Tesla P40, Tesla P4, Discrete GPU on the NVIDIA Drive PX2
SM62 or SM_62, compute_62 – 
Integrated GPU on the NVIDIA Drive PX2, Tegra (Jetson) TX2 

Volta (CUDA 9 and later)

SM70 or SM_70, compute_70 –
DGX-1 with Volta, Tesla V100, GTX 1180 (GV104), Titan V, Quadro GV100
SM72 or SM_72, compute_72 –
Jetson AGX Xavier, Drive AGX Pegasus, Xavier NX 

Turing (CUDA 10 and later)

SM75 or SM_75, compute_75 –
GTX/RTX Turing – GTX 1660 Ti, RTX 2060, RTX 2070, RTX 2080, Titan RTX, Quadro RTX 4000, Quadro RTX 5000, Quadro RTX 6000, Quadro RTX 8000, Quadro T1000/T2000, Tesla T4 

Ampere (CUDA 11.1 and later)

SM80 or SM_80, compute_80 –
NVIDIA A100 (the name “Tesla” has been dropped – GA100), NVIDIA DGX-A100
SM86 or SM_86, compute_86 – (from CUDA 11.1 onwards)
Tesla GA10x cards, RTX Ampere – RTX 3080, GA102 – RTX 3090, RTX A2000, A3000, A4000, A5000, A6000, NVIDIA A40, GA106 – RTX 3060, GA104 – RTX 3070, GA107 – RTX 3050, Quadro A10, Quadro A16, Quadro A40, A2 Tensor Core GPU

“Devices of compute capability 8.6 have 2x more FP32 operations per cycle per SM than devices of compute capability 8.0. While a binary compiled for 8.0 will run as is on 8.6, it is recommended to compile explicitly for 8.6 to benefit from the increased FP32 throughput.“
https://docs.nvidia.com/cuda/ampere-tuning-guide/index.html#improved_fp32


Sample flags for generation on CUDA 11.0 for maximum compatibility with V100 and T4 Turing cards:

-arch=sm_52 \ 
-gencode=arch=compute_52,code=sm_52 \ 
-gencode=arch=compute_60,code=sm_60 \ 
-gencode=arch=compute_61,code=sm_61 \ 
-gencode=arch=compute_70,code=sm_70 \ 
-gencode=arch=compute_75,code=sm_75 \
-gencode=arch=compute_80,code=sm_80 \
-gencode=arch=compute_80,code=compute_80 

Sample flags for generation on CUDA 11.1 for best performance with RTX 3080 cards:

-arch=sm_80 \ 
-gencode=arch=compute_80,code=sm_80 \
-gencode=arch=compute_86,code=sm_86 \
-gencode=arch=compute_86,code=compute_86 

## 故障排除

### 重启显卡
当显卡出现不可恢复的 CUDA 问题（unknown error, 显存被不存在的进程占用）时，可以重启显卡。
前提：不要让 GUI（Xorg进程）运行在可能被重启的 nv 显卡上，否则重启显卡会导致GUI崩溃。具体方法见“显卡切换”一节。

创建 `~/bin/reset_nv.sh`，如下：
```
#!/bin/sh
echo "搜索当前占用nv显卡的进程：fuser -v /dev/nvidia0："
sudo fuser -v /dev/nvidia0
echo "杀死当前占用nv显卡的进程：fuser -k -SIGTERM|SIGKILL /dev/nvidia0："
sudo fuser -k -SIGTERM /dev/nvidia0
sleep 2
sudo fuser -k -SIGKILL /dev/nvidia0
echo "卸载 nv 内核模块：rmmod nvidia_drm nvidia_modeset nvidia_uvm nvidia："
sudo rmmod nvidia_drm nvidia_modeset nvidia_uvm nvidia
echo "加载 nv 内核模块：modprobe nvidia："
sudo modprobe nvidia
```
必要时执行上述文件。

### 显卡设备文件（ `/dev/nvidia0` ）不存在 
错误现象：
```
nvidia-smi
NVIDIA-SMI has failed because it couldn't communicate with the NVIDIA driver. Make sure that the latest NVIDIA driver is installed and running.

sudo fuser -v /dev/nvidia0
指定的文件名 /dev/nvidia0 不存在。

sudo modprobe nvidia
modprobe: FATAL: Module nvidia-current not found in directory /lib/modules/6.9.6-amd64-desktop-rolling
```

可能是因为 dkms 内核模块没有正确编译，手动触发编译：
```
sudo dkms autoinstall
dkms autoinstall on 6.9.6-amd64-desktop-rolling/x86_64 succeeded for nvidia-current
```

### 显存被不存在的进程占用

现象：nvtop/nvidia-smi 显示显存占用率高，但是没有进程占用显存，或者占用显存的进程id是不存在的（ps -elf 查不出此进程）。

检查：用 `sudo fuser -v /dev/nvidia0` 命令（如果有多个显卡，可能是 /dev/nvidia1, /dev/nvidia2 等）检查占用了此显卡的进程。这些进程可能不会被 nvtop/nvidia-smi  列出。

解决办法：
1. 如果 fuser 检查显示没有进程占用此显卡，可尝试重置显卡：`sudo nvidia-smi --gpu-reset -i 0` （0 为显卡序号）。如果无效，按“重启显卡”一节操作。
2. 如果 fuser 检查显示有进程占用此显卡，可先杀死那些进程：`sudo fuser -k -SIGKILL /dev/nvidia0`，看显存占用是否被释放。如果仍然没有释放，可再尝试重置显卡。

### nvidia-smi 输出错误 No devices were found 

如果以普通用户运行 nvidia-smi 出现 “No devices were found”，试着用 sudo 运行，可能是正常的。 
当root用户首次运行 nvidia-smi 成功后，普通用户就可以成功运行了；但重启机器后，仍然需要再用root用户首次运行 nvidia-smi。
这可能是因为 root用户首次运行 nvidia-smi创建了/dev/nv* 设备。机器重启后，有以下 /dev/nv* 设备：

```
/dev/nvidiactl  /dev/nvram
```
sudo nvidia-smi 之后则是：

```
/dev/nvidia0  /dev/nvidiactl  /dev/nvram

/dev/nvidia-caps:
nvidia-cap1  nvidia-cap2
```

这可能是因为 nvidia-modprobe 程序没有 suid ，无法以普通用户运行。详见“nvidia-modprobe 没有 suid”。

### docker 容器中的无法工作，但宿主机可以
宿主机挂起/恢复后，或者 dokcer 容器运行一段事件后，容器中的 GPU 可能无法找到，nvidia-smi 输出:
```
Failed to initialize NVML: Unknown Error
```
import paddle 可能报告：
```
W0401 11:16:10.695442 58360 init.cc:182] Compiled with WITH_GPU, but no GPU found in runtime.
/opt/venv/v39/lib/python3.9/site-packages/paddle/fluid/framework.py:634: UserWarning: You are using GPU version Paddle, but your CUDA device is not set properly. CPU device will be used by default.
```
`ls -l /dev/nv*` 的输出看似正常。
宿主机 `journalctl -u docker` 和 `journalctl -k` 无相关记录。

重启容器有可能解决问题。

如果在 docker 中使用 CUDA 时遇到 `CUDA unknown error` 或 `CUDA_ERROR_UNKNOWN` ，意味着宿主机也出问题了，可按照“重启显卡”一节处理。

另一个可能的解决方案是，升级/降级 nvidia-container-toolkit 的版本，因为某些版本出此问题的可能性更高。按照前面的方法设置 nvidia-container-toolkit 的仓库后，执行 `apt policy nvidia-container-toolkit` ，选择一个合适的版本来安装。

### nvidia-modprobe 没有 suid

/usr/bin/nvidia-modprobe 应该有 suid ，如：

```
ls -l /usr/bin/nvidia-modprobe
-rwsr-xr-x 1 root root 177720 4月   2 08:23 /usr/bin/nvidia-modprobe
```
否则一些 /dev/nv* 设备就无法自动创建，导致 cuda 程序和 nvidia-smi 无法以普通用户运行。
deepin 20.2.1-20.2.2 的 nvidia-modprobe 460.32.03-1 (deepin apricot 仓库) 就没有 suid 。

解决办法是安装其它仓库的版本，或者自己加上 suid 。

```
sudo chmod +s /usr/bin/nvidia-modprobe
```
### nvidia-smi 不显示 cuda 版本号

如果安装了 cuda-toolkit ，但没有安装 libcuda1 ，则 nvidia-smi 左上角显示 CUDA Version: N/A 。
安装与 nvidia-driver 版本匹配的 libcuda1 即可。

### nvcc 和 boost 的 bug

boost < 1.65 和 cuda >= 9.0 的 nvcc 组合存在兼容性问题。以下文件和编译命令可以重现：

```bb.cu
#include <boost/shared_ptr.hpp>
int a=1;
```
编译错误：
```
/usr/local/cuda/bin/nvcc bb.cu -c -o bb.o -ccbin /usr/bin/cc --std c++11

In file included from /usr/local/cuda/bin/../targets/x86_64-linux/include/cuda_runtime.h:120:0,
                 from <command-line>:0:
/usr/local/cuda/bin/../targets/x86_64-linux/include/crt/common_functions.h:74:24: error: token ""__CUDACC_VER__ is no longer supported.  Use __CUDACC_VER_MAJOR__, __CUDACC_VER_MINOR__, and __CUDACC_VER_BUILD__ instead."" is not valid in preprocessor expressions
 #define __CUDACC_VER__ "__CUDACC_VER__ is no longer supported.  Use __CUDACC_VER_MAJOR__, __CUDACC_VER_MINOR__, and __CUDACC_VER_BUILD__ instead."

```

这个问题是因为 Cuda 9.0 删除了宏 `__CUDACC_VER__`，但 boost 还在用，就导致了错误 [7.1, 7.2]。解决办法是修改 /usr/include/boost/config/compiler/nvcc.hpp，将 
```
#if !defined(__CUDACC_VER__) || (__CUDACC_VER__ < 70500)
```
改为
```
#if !defined(__CUDACC_VER_MAJOR__) || (__CUDACC_VER_MAJOR__ * 1000 + __CUDACC_VER_MINOR__ < 7005)
```

### cmake CUDA_cublas_device_LIBRARY 错误

```
cmake ..
make -j4

CMake Error: The following variables are used in this project, but they are set to NOTFOUND.
Please set them or make sure they are set and tested correctly in the CMake files:
CUDA_cublas_device_LIBRARY (ADVANCED)
```

这个错误是因为 cuda 10.0 需要 CMake >= 3.12.2 。建议下载 cmake 源码编译安装，参考 cmake.md 。

### cuda 的未知错误 / CUDA_ERROR_UNKNOWN / unknown error
onnxruntime （CUDAExecutionProvider）的表现：
```
CUDA failure 999: unknown error ; GPU=0 ; hostname=duanyao-lt-d ; file=/onnxruntime_src/onnxruntime/core/providers/cuda/cuda_execution_provider.cc ; line=280 ; expr=cudaSetDevice(info_.device_id);
```
tensorflow 的表现：
```
2021-06-29 11:44:46.710337: E tensorflow/stream_executor/cuda/cuda_driver.cc:328] failed call to cuInit: CUDA_ERROR_UNKNOWN: unknown error
```
pytorch 中的表现：

```
RuntimeError: cuda runtime error (999) : unknown error at /pytorch/aten/src/THC/THCGeneral.cpp:50
```
或者：
```
  File "/home/duanyao/opt/venv/v39/lib/python3.9/site-packages/torch/cuda/__init__.py", line 172, in _lazy_init
    torch._C._cuda_init()
RuntimeError: CUDA unknown error - this may be due to an incorrectly set up environment, e.g. changing env variable CUDA_VISIBLE_DEVICES after program start. Setting the available devices to be zero.
```
opencv 中的表现：

```
cuda_info.cpp:73: error: (-217:Gpu API call) invalid device ordinal in function 'setDevice'
```

排查方案：

* 如果是计算机运行一段时间后或待机唤醒后才出现的，则说明可能是驱动程序中的偶发错误。应退出任何可能使用 nvidia 显卡的应用程序，如 nvtop, nvidia-smi, nvidia-persistenced, Xorg 等。
  可按照“重启显卡”一节处理，或者按如下步骤手动处理。
  可以用 `sudo fuser -v /dev/nvidia0`（如果有多个显卡，可能是 /dev/nvidia1, /dev/nvidia2 等）查找占用的进程，这些进程可能不会被 nvtop/nvidia-smi 列出。用 `sudo fuser -k -SIGKILL /dev/nvidia0` 可以杀死它们。
  注意，有的进程可能需要通过 systemd 停止：
  ```
  sudo systemctl stop nvidia-persistenced
  ```
  然后再重载内核模块：
  ```
  sudo rmmod nvidia_drm nvidia_modeset nvidia_uvm nvidia
  sudo modprobe nvidia
  ```

  杀死 Xorg 可导致桌面退出。这时有两个选择：
  （1）可以停止其它相关进程后，尝试前述卸载和加载nvidia模块的操作，忽略rmmod的“Module nvidia is in use” 错误，这样有可能解决问题。
  （2）如果是有集显的计算机，可以将 xorg 配置为仅使用集显（参考“仅启用集成显卡”一节），这样以后就不必杀死 Xorg 而卸载 nvidia 模块。

* 如果是刚安装、升级 nvidia-driver, libcuda1, cuda, cudnn, libnccl2 等之后出现的，检查 (1) 以上包是否安装（2）cudnn, nccl 与 cuda 版本是否匹配。

* 检查 nvidia 内核模块是否加载了。用 `lsmod | grep nv` 命令，正常的输出类似下面：

    ```
    nvidia_uvm           1015808  0
    nvidia_drm             57344  0
    nvidia_modeset       1228800  1 nvidia_drm
    nvidia              34091008  5 nvidia_uvm,nvidia_modeset
    drm_kms_helper        241664  2 nvidia_drm,i915
    drm                   577536  21 drm_kms_helper,nvidia_drm,i915
    ```
  因为采用了别名，所以 modinfo 无法直接找到，真名是 nvidia-current, nvidia-current-drm, nvidia-current-uvm, nvidia-current-modeset 。
  如果不行，试试卸载后再加载：

    ```
    sudo rmmod nvidia_drm nvidia_modeset nvidia_uvm nvidia
    sudo modprobe nvidia
    ```
  
* 检查 `/dev/nv*` 设备是否完好。正常情况下应该至少有以下4个设备：`/dev/nvidia0 /dev/nvidia-uvm /dev/nvidiactl /dev/nvram` 。

  缺少 /dev/nvidia-uvm 设备时，tensorflow 和 pytorch 是无法工作的（/dev/nvidia-modeset 则可有可无）。正常来说这些设备是会按需自动创建的。
  目前知道的一个原因是nvidia-modprobe 没有 suid (详见 “nvidia-modprobe 没有 suid”）。

  如果修正 nvidia-modprobe 仍然无法解决，可使用这个脚本（需要 sudo，建议命名为 nv-fix.sh ）：

    ```
    #!/bin/bash

    /sbin/modprobe nvidia

    if [ "$?" -eq 0 ]; then
    # Count the number of NVIDIA controllers found.
    NVDEVS=`lspci | grep -i NVIDIA`
    N3D=`echo "$NVDEVS" | grep "3D controller" | wc -l`
    NVGA=`echo "$NVDEVS" | grep "VGA compatible controller" | wc -l`

    N=`expr $N3D + $NVGA - 1`
    for i in `seq 0 $N`; do
        mknod -m 666 /dev/nvidia$i c 195 $i
    done

    mknod -m 666 /dev/nvidiactl c 195 255

    else
    exit 1
    fi

    /sbin/modprobe nvidia-uvm

    if [ "$?" -eq 0 ]; then
    # Find out the major device number used by the nvidia-uvm driver
    D=`grep nvidia-uvm /proc/devices | awk '{print $1}'`

    mknod -m 666 /dev/nvidia-uvm c $D 0
    else
    exit 1
    fi
    ```

* 如果是升级 linux 内核或者或者升级 nvidia 驱动（nvidia-driver包）之后出现的，且重新 `sudo modprobe nvidia` 无效，则考虑重新编译 nvidia-kernel-dkms 内核模块，方法参考前面。

### CUDNN 与 cuda 版本不匹配

openpose 错误：

./build/examples/openpose/openpose.bin

```
F0517 17:43:17.845106 26251 cudnn_conv_layer.cpp:53] Check failed: status == CUDNN_STATUS_SUCCESS (1 vs. 0)  CUDNN_STATUS_NOT_INITIALIZED
```

tensorflow 错误: CuDNN library major and minor version needs to match

```
2020-05-17 19:40:37.338767: E tensorflow/stream_executor/cuda/cuda_dnn.cc:319] Loaded runtime CuDNN library: 7.5.1 but source was compiled with: 7.6.0.  CuDNN library major and minor version needs to match or have higher minor version in case of CuDNN 7.0 or later version.
```
这可能是因为 CUDNN 的版本不对。应当确保 CUDNN 与 CUDA 的 deb 版本匹配。

### Unable to determine the device handle for GPU
nvidia-smi 报错：
```
Unable to determine the device handle for GPU 0000:04:00.0: Unknown Error
```
lspci 和 /dev/nv* 正常:
```
lspci | grep -i nvi
04:00.0 VGA compatible controller: NVIDIA Corporation Device 2204 (rev a1)
04:00.1 Audio device: NVIDIA Corporation Device 1aef (rev a1)
09:00.0 VGA compatible controller: NVIDIA Corporation Device 2204 (rev a1)
09:00.1 Audio device: NVIDIA Corporation Device 1aef (rev a1)
rd@gtrd:~$ ls /dev/nv*
/dev/nvidia0  /dev/nvidiactl       /dev/nvidia-uvm        /dev/nvme0    /dev/nvme0n1p1  /dev/nvme0n1p3  /dev/nvme0n1p5
/dev/nvidia1  /dev/nvidia-modeset  /dev/nvidia-uvm-tools  /dev/nvme0n1  /dev/nvme0n1p2  /dev/nvme0n1p4  /dev/nvram

/dev/nvidia-caps:
nvidia-cap1  nvidia-cap2
```

### Error 803: system has unsupported display driver / cuda driver combination
概述：可能是因为显卡驱动（包名 nvidia-driver）与cuda驱动（包名 libcuda1）的版本号不一致，或者后者没有安装。
例如，在 docker 容器中可能出现此问题。宿主机安装了显卡驱动，但没装 libcuda1；容器里安装了 cuda-toolkit。

torch 的报错：
```
torch.cuda.is_available()
/opt/venv/v39/lib/python3.9/site-packages/torch/cuda/__init__.py:80: UserWarning: CUDA initialization: Unexpected error from cudaGetDeviceCount(). Did you run some cuda functions before calling NumCudaDevices() that might have already set an error? Error 803: system has unsupported display driver / cuda driver combination (Triggered internally at  ../c10/cuda/CUDAFunctions.cpp:112.)
  return torch._C._cuda_getDeviceCount() > 0
```
paddle 的报错：
```
paddle.utils.run_check()
Running verify PaddlePaddle program ... 
[2024-06-24 13:09:53,998] [ WARNING] install_check.py:71 - You are using GPU version PaddlePaddle, but there is no GPU detected on your machine. Maybe CUDA devices is not set properly.
 Original Error is (External) CUDA error(803), system has unsupported display driver / cuda driver combination. 
  [Hint: Please search for the error code(803) on website (https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__TYPES.html#group__CUDART__TYPES_1g3f51e3575c2178246db0a94a430e0038) to get Nvidia's official solution and advice about CUDA Error.] (at /paddle/paddle/phi/backends/gpu/cuda/cuda_info.cc:66)
```

nvidia-smi 的输出似乎是正常的：
```
nvidia-smi
Mon Jun 24 13:02:26 2024       
+---------------------------------------------------------------------------------------+
| NVIDIA-SMI 530.41.03              Driver Version: 530.41.03    CUDA Version: 11.2     |
|-----------------------------------------+----------------------+----------------------+
| GPU  Name                  Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf            Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|                                         |                      |               MIG M. |
|=========================================+======================+======================|
|   0  NVIDIA GeForce RTX 4050 L...    Off| 00000000:01:00.0 Off |                  N/A |
| N/A   50C    P0               N/A /  N/A|      5MiB /  6141MiB |      0%      Default |
|                                         |                      |                  N/A |
+-----------------------------------------+----------------------+----------------------+
                                                                                         
+---------------------------------------------------------------------------------------+
| Processes:                                                                            |
|  GPU   GI   CI        PID   Type   Process name                            GPU Memory |
|        ID   ID                                                             Usage      |
|=======================================================================================|
+---------------------------------------------------------------------------------------+
```

解决办法：这可能是因为宿主机缺少了 libcuda1 ，如果只安装驱动和 container-toolkit ，不装 cuda-tookit，可能会漏掉它。
```
apt policy libcuda1
sudo apt install --no-install-recommends libcuda1
```

## 频率、风扇、功耗控制

```
sudo nvidia-xconfig -a --cool-bits=28 --allow-empty-initial-configuration

#   cool-bits 的值的每一位都代表不同的含义，可以根据自己的需要组合，具体含义摘抄如下。
#   Coolbits 的值是所有位的组合成的二进制数对应的10进制

数值：
#   - 1 (bit0) 允许老的（Fermi核心）之前的显卡超频
#   - 2 (bit1) 当使用不同显存的GPU需要设置成SLI时，可以设置这一位
#   - 4 (bit2) 允许手动设置显卡风扇的转速
#   - 8 (bit3) 允许超频，驱动版本要比337.12新，架构要比Fermi新
#   - 16 (bit4) 允许超电压，驱动版本要比346.16新，架构要比Fermi新
# 这个命令会为你的每个显卡在Xorg.conf中添加一个Screen和Device，内容其实我们不需要了解
```

完成上述命令后，用Vim 打开/etc/X11/xorg.conf注释掉allow-empty-initial-configuration”所在行，再重启 xorg-server 即可。

随后，打开Nvidia X Server应用，Powermizer 下面会多出“editable performance levels” 区域，可以对核心和显存进行降频或超频。注意，每次修改频率增量后要回车才生效。

## 显卡切换（核显和独显切换）

### nvidia-prime

ubuntu 23.04 上，如果安装了 nvidia 驱动，则  nvidia-prime 可能会默认安装，并且设置为 on-demand 模式。要改为其它模式，可以：

prime-select nvidia|intel|on-demand|query
query 是查看当前模式。
intel 仅使用核显。nvidia 内核模块会被禁用，似乎也不可以手动加载。
nvidia 仅使用独显。

有些配置下 on-demand 模式会造成系统无法启动，就需要改为 intel 模式。

在 intel 模式，prime-select 会创建 /lib/modprobe.d/blacklist-nvidia.conf 和 /usr/lib/modprobe.d/blacklist-nvidia.conf 文件（两者内容相同），阻止 nvidia 内核模块加载。

```
# Do not modify
# This file was generated by nvidia-prime
blacklist nvidia
blacklist nvidia-drm
blacklist nvidia-modeset
alias nvidia off
alias nvidia-drm off
alias nvidia-modeset off
```

prime-offload 可以将一个程序在独显上运行。

### 手动修改 xorg.conf 
默认状态下，/etc/X11/xorg.conf 不存在，Xorg 进程可能会占用独显。
要手动修改 xorg.conf ，先要了解计算机上的显卡的信息，例如：
```
sudo lshw -C display
  *-display                 
       description: 3D controller
       product: AD107M [GeForce RTX 4050 Max-Q / Mobile]
       vendor: NVIDIA Corporation
       physical id: 0
       bus info: pci@0000:01:00.0
       configuration: depth=32 driver=nvidia latency=0 mode=1920x1080 visual=truecolor xres=1920 yres=1080
  *-display
       description: VGA compatible controller
       product: Phoenix1
       vendor: Advanced Micro Devices, Inc. [AMD/ATI]
       physical id: 0
       bus info: pci@0000:65:00.0
       configuration: depth=32 driver=amdgpu latency=0 resolution=2560,1440

lspci | grep "3D controller"
01:00.0 3D controller: NVIDIA Corporation AD107M [GeForce RTX 4050 Max-Q / Mobile] (rev a1)
lspci | grep "VGA compatible controller"
65:00.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Phoenix1 (rev c2)
```
其中的重要信息是驱动（driver=nvidia, driver=amdgpu）和总线（bus info）。

xorg.conf 中的 BusID 格式为 `BusID "PCI:<bus>:<device>:<function>"` [10.2]。lshw、lspci、xorg.conf 的PCI标识对照表如下：
```
lshw                 lspci        xorg.conf
pci@0000:01:00.0     01:00.0      PCI:1:0:0
pci@0000:65:00.0     65:00.0      PCI:65:0:0
```

#### 仅启用集成显卡
默认状态下，xorg 可能会占用独显，但我们想使用集显。


编辑 `~/xorg.ig.conf`，仅启用集成显卡：

```
# /etc/X11/xorg.conf
Section "ServerLayout"
        Identifier "layout"
        Screen 0 "ig"
        #Inactive "dg"
        #Option "AllowNVIDIAGPUScreens"
EndSection

Section "Device"
        Identifier "ig"
        Driver "amdgpu"
        #BusID "PCI:65:0:0" # pci@0000:65:00.0
EndSection

Section "Screen"
        Identifier "ig"
        Device "ig"
EndSection
```
`Section "Device"` 用来指定一块显卡，其中 Driver 和 BusID 一般无需同时指定，除非其中之一无法唯一决定一块显卡。

使配置文件生效：
```
sudo cp ~/xorg.ig.conf /etc/X11/xorg.conf
# 重启桌面和xorg。如果使用其它的 DM，相应修改。或者注销。
sudo systemctl restart lightdm.service
```

#### 集显和独显都启用，集显优先

/etc/X11/xorg.conf 如下：
```
# /etc/X11/xorg.conf
Section "ServerLayout"
        Identifier "layout"
        Screen 0 "ig"
        Inactive "dg"
        Option "AllowNVIDIAGPUScreens"
EndSection

Section "Device"
        Identifier "dg"
        Driver "nvidia"
EndSection

Section "Screen"
        Identifier "dg"
        Device "nvidia"
        #BusID "PCI:1:0:0" # pci@0000:01:00.0
EndSection

Section "Device"
        Identifier "ig"
        Driver "amdgpu"
        #BusID "PCI:65:0:0" # pci@0000:65:00.0
EndSection

Section "Screen"
        Identifier "ig"
        Device "ig"
EndSection
```
实际效果与默认（无 xorg.conf）的相同，xorg 进程仍会占用独显。

### 其他方案
bumblebee
bbswitch

### 其它
primus client-side GPU offloading for NVIDIA Optimus
primus-vk-nvidia NVIDIA Optimus support for Vulkan applications
deepin-nvidia-prime Tool to support Nvidia Optimus
bumblebee-nvidia NVIDIA Optimus support using the proprietary NVIDIA driver
bbswitch-source bbswitch-dkms Interface for toggling the power on NVIDIA Optimus video cards

https://bbs.deepin.org/post/192750
https://support.steampowered.com/kb_article.php?ref=6316-GJKC-7437

primusrun %command% 

primus 与 nvidia-driver-libs 不兼容，见：
https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=888020

nvidia-xrun https://github.com/Witko/nvidia-xrun

## 分析
$ find /usr -name "*nvidia*"

/usr/bin/nvidia-container-runtime-hook
/usr/bin/nvidia-smi
/usr/bin/nvidia-xconfig
/usr/bin/nvidia-container-cli
/usr/bin/nvidia-container-runtime
/usr/bin/nvidia-container-toolkit
/usr/bin/nvidia-persistenced
/usr/bin/nvidia-modprobe
/usr/bin/nvidia-settings
/usr/lib/nvidia
/usr/lib/nvidia/nvidia
/usr/lib/nvidia/current/nvidia-smi
/usr/lib/nvidia/current/nvidia.ids
/usr/lib/nvidia/current/nvidia-settings.desktop
/usr/lib/nvidia/current/nvidia_drv.so
/usr/lib/nvidia/current/nvidia-settings.1.gz
/usr/lib/nvidia/current/nvidia-smi.1.gz
/usr/lib/nvidia/current/nvidia-settings
/usr/lib/nvidia/check-for-mismatching-nvidia-module
/usr/lib/nvidia/nvidia_drv.so
/usr/lib/libnvidia-gtk3.so.390.48

/usr/lib/xorg/modules/drivers/nvidia_drv.so
/usr/lib/libnvidia-gtk2.so.390.48

/usr/lib/i386-linux-gnu/nvidia/current/
/usr/lib/i386-linux-gnu/libGLX_nvidia.so.0
/usr/lib/i386-linux-gnu/libnvidia-fatbinaryloader.so.390.67
/usr/lib/i386-linux-gnu/libnvidia-ml.so.1
/usr/lib/i386-linux-gnu/libnvidia-ptxjitcompiler.so.1
/usr/lib/i386-linux-gnu/libnvidia-tls.so.390.67
/usr/lib/i386-linux-gnu/libnvidia-glcore.so.390.67
/usr/lib/i386-linux-gnu/nvidia
/usr/lib/i386-linux-gnu/nvidia/current/libGLX_nvidia.so.390.67
/usr/lib/i386-linux-gnu/nvidia/current/libGLX_nvidia.so.0
/usr/lib/i386-linux-gnu/nvidia/current/libnvidia-ml.so
/usr/lib/i386-linux-gnu/nvidia/current/libnvidia-ml.so.390.67
/usr/lib/i386-linux-gnu/nvidia/current/libnvidia-ml.so.1
/usr/lib/i386-linux-gnu/nvidia/current/libnvidia-ptxjitcompiler.so.1
/usr/lib/i386-linux-gnu/nvidia/current/libnvidia-ptxjitcompiler.so.390.67

/usr/lib/x86_64-linux-gnu/stubs/libnvidia-ml.so
/usr/lib/x86_64-linux-gnu/nvidia
/usr/lib/x86_64-linux-gnu/nvidia/current/libGLX_nvidia.so.390.67
/usr/lib/x86_64-linux-gnu/nvidia/current/libvdpau_nvidia.so.1
/usr/lib/x86_64-linux-gnu/nvidia/current/libGLX_nvidia.so.0
/usr/lib/x86_64-linux-gnu/nvidia/current/libnvidia-ml.so
/usr/lib/x86_64-linux-gnu/nvidia/current/libvdpau_nvidia.so.390.67
/usr/lib/x86_64-linux-gnu/nvidia/current/libnvidia-ml.so.390.67
/usr/lib/x86_64-linux-gnu/nvidia/current/libnvidia-cfg.so.1
/usr/lib/x86_64-linux-gnu/nvidia/current/libnvidia-ml.so.1
/usr/lib/x86_64-linux-gnu/nvidia/current/libnvidia-ptxjitcompiler.so.1
/usr/lib/x86_64-linux-gnu/nvidia/current/libEGL_nvidia.so.0
/usr/lib/x86_64-linux-gnu/nvidia/current/libEGL_nvidia.so.390.67
/usr/lib/x86_64-linux-gnu/nvidia/current/libnvidia-cfg.so.390.67
/usr/lib/x86_64-linux-gnu/nvidia/current/libnvidia-ptxjitcompiler.so.390.67
/usr/lib/x86_64-linux-gnu/nvidia/libnvidia-cfg.so.1


$ find /lib -name "*nvidia*"
/lib/udev/nvidia_helper
/lib/udev/rules.d/60-nvidia-kernel-common.rules
/lib/firmware/nvidia
/lib/modules/4.15.0-29deepin-generic/kernel/drivers/net/ethernet/nvidia
/lib/modules/4.15.0-29deepin-generic/kernel/drivers/video/fbdev/nvidia
/lib/modules/4.15.0-29deepin-generic/kernel/drivers/video/fbdev/nvidia/nvidiafb.ko
/lib/modules/4.15.0-29deepin-generic/updates/dkms/nvidia-current-uvm.ko
/lib/modules/4.15.0-29deepin-generic/updates/dkms/nvidia-current.ko
/lib/modules/4.15.0-29deepin-generic/updates/dkms/nvidia-current-modeset.ko
/lib/modules/4.15.0-29deepin-generic/updates/dkms/nvidia-current-drm.ko
/lib/modules/4.15.0-21deepin-generic/kernel/drivers/net/ethernet/nvidia
/lib/modules/4.15.0-21deepin-generic/kernel/drivers/video/fbdev/nvidia
/lib/modules/4.15.0-21deepin-generic/kernel/drivers/video/fbdev/nvidia/nvidiafb.ko
/lib/modules/4.15.0-21deepin-generic/updates/dkms/nvidia-current-uvm.ko
/lib/modules/4.15.0-21deepin-generic/updates/dkms/nvidia-current.ko
/lib/modules/4.15.0-21deepin-generic/updates/dkms/nvidia-current-modeset.ko
/lib/modules/4.15.0-21deepin-generic/updates/dkms/nvidia-current-drm.ko

dpkg -S /lib/firmware/nvidia
firmware-misc-nonfree: /lib/firmware/nvidia

$ dpkg -S /usr/lib/libnvidia-gtk3.so.390.48
nvidia-settings: /usr/lib/libnvidia-gtk3.so.390.48

$ dpkg -S /usr/lib/nvidia/
nvidia-installer-cleanup, nvidia-smi:i386, glx-alternative-nvidia, xserver-xorg-video-nvidia, nvidia-alternative, nvidia-settings, nvidia-support: /usr/lib/nvidia

$ dpkg -S /usr/lib/nvidia/current/nvidia_drv.so
xserver-xorg-video-nvidia: /usr/lib/nvidia/current/nvidia_drv.so

$ dpkg -S /usr/lib/i386-linux-gnu/nvidia/current/libGLX_nvidia.so.0
libglx-nvidia0:i386: /usr/lib/i386-linux-gnu/nvidia/current/libGLX_nvidia.so.0

$ dpkg -S /usr/lib/x86_64-linux-gnu/nvidia/current/libGLX_nvidia.so.390.67
libglx-nvidia0:amd64: /usr/lib/x86_64-linux-gnu/nvidia/current/libGLX_nvidia.so.390.67

$ dpkg -S libnvidia-ml.so.390.67
libnvidia-ml1:i386: /usr/lib/i386-linux-gnu/nvidia/current/libnvidia-ml.so.390.67
libnvidia-ml1:amd64: /usr/lib/x86_64-linux-gnu/nvidia/current/libnvidia-ml.so.390.67

$ dpkg -S libnvidia-ptxjitcompiler.so.1
libnvidia-ptxjitcompiler1:amd64: /usr/lib/x86_64-linux-gnu/nvidia/current/libnvidia-ptxjitcompiler.so.1
libnvidia-ptxjitcompiler1:i386: /usr/lib/i386-linux-gnu/nvidia/current/libnvidia-ptxjitcompiler.so.1

$ dpkg -S libnvidia-tls.so.390.67
libnvidia-glcore:i386: /usr/lib/i386-linux-gnu/libnvidia-tls.so.390.67
libnvidia-glcore:amd64: /usr/lib/x86_64-linux-gnu/libnvidia-tls.so.390.67

$ dpkg -S libEGL_nvidia.so.390.67
libegl-nvidia0:amd64: /usr/lib/x86_64-linux-gnu/nvidia/current/libEGL_nvidia.so.390.67

-------------------------------------------------------
tensorflow 依赖的so

/usr/lib/x86_64-linux-gnu/libnvidia-fatbinaryloader.so.390.67  -> libnvidia-fatbinaryloader:amd64
/usr/lib/x86_64-linux-gnu/nvidia/current/libcuda.so.390.67     -> libcuda1:amd64

/usr/lib/x86_64-linux-gnu/libcurand.so.9.1.85
/usr/lib/x86_64-linux-gnu/libcufft.so.9.1.85
/usr/lib/x86_64-linux-gnu/libcudnn.so.7.4.2
/usr/lib/x86_64-linux-gnu/libcudart.so.9.1.85
/usr/lib/x86_64-linux-gnu/libcusolver.so.9.1.85
/usr/lib/x86_64-linux-gnu/libcublas.so.9.1.85

-------------------------------------------------------
依赖图：
libcuda1:amd64
  libnvidia-fatbinaryloader:amd64
    libnvidia-ptxjitcompiler1 [end]

$ apt-rdepends --print-state libcuda1:amd64

libcuda1:amd64
  Depends: libc6 (>= 2.2.5) [Installed]
  Depends: libnvidia-fatbinaryloader (= 390.87-2~bpo9+1) [Installed]
  Depends: nvidia-alternative (= 390.87-2~bpo9+1) [Installed]
  Depends: nvidia-support [Installed]
  PreDepends: nvidia-legacy-check (>= 343) [Installed]

libnvidia-fatbinaryloader
  Depends: libnvidia-ptxjitcompiler1 (= 390.87-2~bpo9+1) [Installed] #end

nvidia-alternative
  Depends: glx-alternative-nvidia (>= 0.8.4~) [Installed]
  PreDepends: nvidia-legacy-check (>= 343) [Installed]

glx-alternative-nvidia
  Depends: glx-alternative-mesa [Installed]
  Depends: glx-diversions (= 0.8.8~bpo9+1) [Installed]
  Depends: update-glx (= 0.8.8~bpo9+1) [Installed]

nvidia-legacy-check
  PreDepends: nvidia-installer-cleanup (>= 20151021) [Installed]

nvidia-support #end

-------------------------------------------------------
不影响 cuda：

bumblebee-nvidia nvidia-settings nvidia-xconfig xserver-xorg-video-nvidia nvidia-driver nvidia-driver-bin libnvidia-ml1 nvidia-vdpau-driver libegl-nvidia0 libnvidia-cfg1

  libegl-nvidia0:i386 libegl1-nvidia:i386 libgl1-nvidia-glx libgl1-nvidia-glx:i386 libgles-nvidia1
  libgles-nvidia1:i386 libgles-nvidia2 libgles-nvidia2:i386 libgles2:i386 libglx-nvidia0 libglx-nvidia0:i386
  libnvidia-cfg1:i386 libnvidia-egl-wayland1 libnvidia-egl-wayland1:i386 libnvidia-eglcore
  libnvidia-eglcore:i386 libnvidia-glcore libnvidia-glcore:i386 libvulkan1 libvulkan1:i386
  nvidia-driver-libs-nonglvnd:i386 nvidia-driver-libs-nonglvnd-i386:i386 nvidia-egl-wayland-common
  nvidia-egl-wayland-icd:i386 nvidia-nonglvnd-vulkan-common nvidia-nonglvnd-vulkan-icd
  nvidia-nonglvnd-vulkan-icd:i386

-------------------------------------------------------


-------------------------------------------------------
ldd /usr/lib/x86_64-linux-gnu/nvidia/current/libcuda.so.1
-------------------------------------------------------

$ apt search -t buster-backports -n nvidia | grep buster-backports

WARNING: apt does not have a stable CLI interface. Use with caution in scripts.

boinc-client-nvidia-cuda/buster-backports 7.16.1+dfsg-2~bpo10+1 amd64
libegl-nvidia0/buster-backports 430.64-4~bpo10+1 amd64 [可从该版本升级：390.67-3deepin]
libegl1-nvidia/buster-backports 430.64-4~bpo10+1 amd64
libgl1-nvidia-glvnd-glx/buster-backports 430.64-4~bpo10+1 amd64
libgles-nvidia1/buster-backports 430.64-4~bpo10+1 amd64
libgles-nvidia2/buster-backports 430.64-4~bpo10+1 amd64
libglx-nvidia0/buster-backports 430.64-4~bpo10+1 amd64 [可从该版本升级：390.67-3deepin]
libnvidia-cbl/buster-backports 430.64-4~bpo10+1 amd64
libnvidia-compiler/buster-backports 430.64-4~bpo10+1 amd64
libnvidia-eglcore/buster-backports 430.64-4~bpo10+1 amd64 [可从该版本升级：390.67-3deepin]
libnvidia-encode1/buster-backports 430.64-4~bpo10+1 amd64
libnvidia-fatbinaryloader/buster-backports 430.64-4~bpo10+1 amd64 [可从该版本升级：390.67-3deepin]
libnvidia-fbc1/buster-backports 430.64-4~bpo10+1 amd64
libnvidia-glcore/buster-backports 430.64-4~bpo10+1 amd64 [可从该版本升级：390.67-3deepin]
libnvidia-glvkspirv/buster-backports 430.64-4~bpo10+1 amd64
libnvidia-ifr1/buster-backports 430.64-4~bpo10+1 amd64
libnvidia-ml1/buster-backports 430.64-4~bpo10+1 amd64 [可从该版本升级：390.67-3deepin]
libnvidia-opticalflow1/buster-backports 430.64-4~bpo10+1 amd64
libnvidia-ptxjitcompiler1/buster-backports 430.64-4~bpo10+1 amd64 [可从该版本升级：390.67-3deepin]
libnvidia-rtcore/buster-backports 430.64-4~bpo10+1 amd64
nvidia-alternative/buster-backports 430.64-4~bpo10+1 amd64 [可从该版本升级：390.67-3deepin]
nvidia-driver/buster-backports 430.64-4~bpo10+1 amd64
nvidia-driver-libs/buster-backports 430.64-4~bpo10+1 amd64
nvidia-driver-libs-i386/buster-backports 430.64-4~bpo10+1 i386
nvidia-egl-common/buster-backports 430.64-4~bpo10+1 amd64 [可从该版本升级：390.67-3deepin]
nvidia-egl-icd/buster-backports 430.64-4~bpo10+1 amd64 [可从该版本升级：390.67-3deepin]
nvidia-libopencl1/buster-backports 430.64-4~bpo10+1 amd64
nvidia-nonglvnd-vulkan-common/buster-backports 430.64-4~bpo10+1 amd64
nvidia-opencl-common/buster-backports 430.64-4~bpo10+1 amd64
nvidia-opencl-icd/buster-backports 430.64-4~bpo10+1 amd64
nvidia-settings/buster-backports 430.64-1~bpo10+1 amd64 [可从该版本升级：390.48-2]
nvidia-smi/buster-backports 430.64-4~bpo10+1 amd64
nvidia-vdpau-driver/buster-backports 430.64-4~bpo10+1 amd64 [可从该版本升级：390.67-3deepin]
nvidia-vulkan-common/buster-backports 430.64-4~bpo10+1 amd64
nvidia-vulkan-icd/buster-backports 430.64-4~bpo10+1 amd64
xserver-xorg-video-nvidia/buster-backports 430.64-4~bpo10+1 amd64 [可从该版本升级：390.67-3deepin]


$ apt search -t panda -n nvidia | grep 已安装

WARNING: apt does not have a stable CLI interface. Use with caution in scripts.

glx-alternative-nvidia/panda,now 0.8.7 amd64 [已安装，自动]
libegl-nvidia0/panda,now 390.67-3deepin amd64 [已安装，自动]
libgl1-nvidia-glx/panda,now 390.67-3deepin amd64 [已安装，自动]
libglx-nvidia0/panda,now 390.67-3deepin amd64 [已安装，自动]
libnvidia-cfg1/panda,now 390.67-3deepin amd64 [已安装，自动]
libnvidia-container-tools/stretch,now 1.0.5-1 amd64 [已安装，自动]
libnvidia-container1/stretch,now 1.0.5-1 amd64 [已安装，自动]
libnvidia-eglcore/panda,now 390.67-3deepin amd64 [已安装，自动]
libnvidia-fatbinaryloader/panda,now 390.67-3deepin amd64 [已安装，自动]
libnvidia-glcore/panda,now 390.67-3deepin amd64 [已安装，自动]
libnvidia-ml1/panda,now 390.67-3deepin amd64 [已安装，自动]
libnvidia-ptxjitcompiler1/panda,now 390.67-3deepin amd64 [已安装，自动]
nvidia-alternative/panda,now 390.67-3deepin amd64 [已安装，自动]
nvidia-container-runtime/stretch,now 3.1.4-1 amd64 [已安装]
nvidia-container-toolkit/stretch,now 1.0.5-1 amd64 [已安装]
nvidia-cuda-dev/panda,now 9.1.85-4+b1 amd64 [已安装]
nvidia-cuda-doc/panda,panda,now 9.1.85-4 all [已安装，自动]
nvidia-cuda-gdb/panda,now 9.1.85-4+b1 amd64 [已安装，自动]
nvidia-cuda-toolkit/panda,now 9.1.85-4+b1 amd64 [已安装]
nvidia-egl-common/panda,now 390.67-3deepin amd64 [已安装，自动]
nvidia-egl-icd/panda,now 390.67-3deepin amd64 [已安装，自动]
nvidia-installer-cleanup/panda,now 20151021+8 amd64 [已安装，自动]
nvidia-kernel-common/panda,now 20151021+8 amd64 [已安装，自动]
nvidia-kernel-dkms/panda,now 390.67-3deepin amd64 [已安装，自动]
nvidia-kernel-support/panda,now 390.67-3deepin amd64 [已安装，自动]
nvidia-legacy-check/panda,now 390.67-3deepin amd64 [已安装，自动]
nvidia-modprobe/panda,now 390.25-1 amd64 [已安装，自动]
nvidia-persistenced/panda,now 390.25-1 amd64 [已安装，自动]
nvidia-profiler/panda,now 9.1.85-4+b1 amd64 [已安装，自动]
nvidia-settings/panda,now 390.48-2 amd64 [已安装]
nvidia-support/panda,now 20151021+8 amd64 [已安装，自动]
nvidia-vdpau-driver/panda,now 390.67-3deepin amd64 [已安装，自动]
nvidia-visual-profiler/panda,now 9.1.85-4+b1 amd64 [已安装，自动]
nvidia-xconfig/panda,now 390.25-1 amd64 [已安装]
xserver-xorg-video-nvidia/panda,now 390.67-3deepin amd64 [已安装，自动]


$ apt search -t lion -n nvidia | grep 已安装

WARNING: apt does not have a stable CLI interface. Use with caution in scripts.

bumblebee-nvidia/lion,now 3.2.1-14 amd64 [已安装]
glx-alternative-nvidia/lion,now 0.8.8~bpo9+1 amd64 [已安装，自动]
libegl-nvidia0/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
libegl1-nvidia/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
libgl1-nvidia-glx/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
libgles-nvidia1/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
libgles-nvidia2/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
libglx-nvidia0/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
libnvidia-cfg1/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
libnvidia-egl-wayland1/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
libnvidia-eglcore/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
libnvidia-fatbinaryloader/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
libnvidia-glcore/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
libnvidia-ml1/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
libnvidia-ptxjitcompiler1/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
nvidia-alternative/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
nvidia-cuda-dev/lion,now 8.0.44-4 amd64 [已安装]
nvidia-cuda-doc/lion,lion,now 8.0.44-4 all [已安装，自动]
nvidia-cuda-gdb/lion,now 8.0.44-4 amd64 [已安装，自动]
nvidia-cuda-toolkit/lion,now 8.0.44-4 amd64 [已安装]
nvidia-driver/lion,now 390.87-2~bpo9+1 amd64 [已安装]
nvidia-driver-bin/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
nvidia-driver-libs-nonglvnd/lion,now 390.87-2~bpo9+1 amd64 [已安装]
nvidia-driver-libs-nonglvnd-i386/lion,now 390.87-2~bpo9+1 i386 [已安装，自动]
nvidia-egl-wayland-common/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
nvidia-egl-wayland-icd/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
nvidia-installer-cleanup/lion,now 20151021+4 amd64 [已安装，自动]
nvidia-kernel-common/lion,now 20151021+4 amd64 [已安装，自动]
nvidia-kernel-dkms/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
nvidia-kernel-support/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
nvidia-legacy-check/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
nvidia-modprobe/lion,now 384.111-2~deb9u1 amd64 [已安装，自动]
nvidia-nonglvnd-vulkan-common/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
nvidia-nonglvnd-vulkan-icd/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
nvidia-persistenced/lion,now 384.111-1~deb9u1 amd64 [已安装，自动]
nvidia-profiler/lion,now 8.0.44-4 amd64 [已安装，自动]
nvidia-settings/lion,now 384.111-1~deb9u1 amd64 [已安装]
nvidia-support/lion,now 20151021+4 amd64 [已安装，自动]
nvidia-vdpau-driver/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
nvidia-visual-profiler/lion,now 8.0.44-4 amd64 [已安装，自动]
nvidia-xconfig/lion,now 384.111-1~deb9u1 amd64 [已安装]
xserver-xorg-video-nvidia/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]

最小化以后：
$ apt search -t lion -n nvidia | grep 已安装
glx-alternative-nvidia/lion,now 0.8.8~bpo9+1 amd64 [已安装，自动]
libnvidia-fatbinaryloader/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
libnvidia-ptxjitcompiler1/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
nvidia-alternative/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
nvidia-cuda-dev/lion,now 8.0.44-4 amd64 [已安装]
nvidia-cuda-doc/lion,lion,now 8.0.44-4 all [已安装，自动]
nvidia-cuda-gdb/lion,now 8.0.44-4 amd64 [已安装，自动]
nvidia-cuda-toolkit/lion,now 8.0.44-4 amd64 [已安装]
nvidia-installer-cleanup/lion,now 20151021+4 amd64 [已安装，自动]
nvidia-kernel-common/lion,now 20151021+4 amd64 [已安装，自动]
nvidia-kernel-dkms/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
nvidia-kernel-support/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
nvidia-legacy-check/lion,now 390.87-2~bpo9+1 amd64 [已安装，自动]
nvidia-modprobe/lion,now 384.111-2~deb9u1 amd64 [已安装，自动]
nvidia-profiler/lion,now 8.0.44-4 amd64 [已安装，自动]  NVIDIA Profiler for CUDA and OpenCL
nvidia-support/lion,now 20151021+4 amd64 [已安装，自动]
nvidia-visual-profiler/lion,now 8.0.44-4 amd64 [已安装，自动] NVIDIA Visual Profiler for CUDA and OpenCL


$ aptitude install -s -t buster-backports nvidia-smi
下列“新”软件包将被安装。         
  libegl-nvidia0:i386{a} libegl1:i386{a} libgl1-nvidia-glvnd-glx{a} libgl1-nvidia-glvnd-glx:i386{a} libgles-nvidia1{a} libgles-nvidia1:i386{a} libgles-nvidia2{a} libgles-nvidia2:i386{a} libgles1{ab} 
  libgles1:i386{ab} libgles2:i386{a} libnvidia-cbl{a} libnvidia-eglcore:i386{a} libnvidia-glvkspirv{a} libnvidia-glvkspirv:i386{a} libnvidia-rtcore{a} libopengl0:i386{a} libvulkan1:i386{a} nvidia-driver{a} 
  nvidia-driver-bin{a} nvidia-driver-libs{ab} nvidia-driver-libs:i386{ab} nvidia-driver-libs-i386:i386{a} nvidia-egl-icd:i386{a} nvidia-smi{b} nvidia-vulkan-common{ab} nvidia-vulkan-icd{a} 
  nvidia-vulkan-icd:i386{a} 
下列软件包将被“删除”：
  glx-alternative-mesa{u} glx-alternative-nvidia{u} glx-diversions{u} libnvidia-fatbinaryloader:i386{u} libnvidia-ml1:i386{u} libnvidia-ptxjitcompiler1:i386{u} update-glx{u} 
下列软件包将被升级：
  libegl-nvidia0 libglx-nvidia0 libglx-nvidia0:i386 libnvidia-cfg1 libnvidia-eglcore libnvidia-fatbinaryloader libnvidia-glcore libnvidia-glcore:i386 libnvidia-ml1 libnvidia-ptxjitcompiler1 libxnvctrl0 
  nvidia-alternative{b} nvidia-egl-icd nvidia-kernel-dkms nvidia-settings nvidia-vdpau-driver xserver-xorg-video-nvidia 
17 个软件包被升级，新安装 28 个，7 个将被删除， 同时 96 个将不升级。
需要获取 72.0 MB 的存档。解包后将要使用 93.9 MB。
下列软件包存在未满足的依赖关系：
 nvidia-vulkan-common : 冲突: libgl1-nvidia-glx but 390.67-3deepin is installed
 libgles1 : 依赖: libglvnd0 (= 1.1.0-1~bpo9+1) but 1.0.0+git20180308-2deepin is installed
            破坏: libgles2 (< 1.0.0+git20180308-5) but 1.0.0+git20180308-2deepin is installed
            破坏: libgles2:i386 (< 1.0.0+git20180308-5) but 1.0.0+git20180308-2deepin is to be installed
 libgles1:i386 : 依赖: libglvnd0:i386 (= 1.1.0-1~bpo9+1) but 1.0.0+git20180308-2deepin is installed
                 破坏: libgles2 (< 1.0.0+git20180308-5) but 1.0.0+git20180308-2deepin is installed
                 破坏: libgles2:i386 (< 1.0.0+git20180308-5) but 1.0.0+git20180308-2deepin is to be installed
 nvidia-smi : 冲突: nvidia-smi:i386 but 390.67-3deepin is installed and it is kept back
 nvidia-smi:i386 : 依赖: nvidia-alternative:i386 (= 390.67-3deepin) but it is not going to be installed
                   依赖: libnvidia-ml1:i386 (= 390.67-3deepin) but it is not going to be installed
                   冲突: nvidia-smi but 430.64-4~bpo10+1 is to be installed
 libgl1-nvidia-glx : 依赖: nvidia-alternative (= 390.67-3deepin) but 430.64-4~bpo10+1 is to be installed
                     依赖: libnvidia-glcore (= 390.67-3deepin) but 430.64-4~bpo10+1 is to be installed
                     冲突: libgl1-nvidia-glvnd-glx but 430.64-4~bpo10+1 is to be installed
                     冲突: libgl1-nvidia-glvnd-glx:i386 but 430.64-4~bpo10+1 is to be installed
 nvidia-kernel-support : 依赖: nvidia-alternative (= 390.67-3deepin) but 430.64-4~bpo10+1 is to be installed
 nvidia-driver-libs : 破坏: libgl1-nvidia-glx but 390.67-3deepin is installed
 nvidia-driver-libs:i386 : 破坏: libgl1-nvidia-glx but 390.67-3deepin is installed
 libcuda1 : 依赖: nvidia-alternative (= 390.67-3deepin) but 430.64-4~bpo10+1 is to be installed
            依赖: libnvidia-fatbinaryloader (= 390.67-3deepin) but 430.64-4~bpo10+1 is to be installed
 libcuda1:i386 : 依赖: nvidia-alternative:i386 (= 390.67-3deepin) but it is not going to be installed
                 依赖: libnvidia-fatbinaryloader:i386 (= 390.67-3deepin) but it is not going to be installed
 nvidia-alternative : 依赖: glx-alternative-nvidia (>= 0.9) but it is not going to be installed
打开：7937；关闭；8840；defer: 70; 冲突；73                                                                                                                                                             


$ aptitude install -s -t buster-backports nvidia-alternative libnvidia-ml1 nvidia-vdpau-driver libglx-nvidia0 nvidia-smi libcuda1
下列“新”软件包将被安装。
  nvidia-smi 
下列软件包将被“删除”：
  glx-alternative-mesa{u} glx-alternative-nvidia{u} glx-diversions{u} libnvidia-ml1:i386{u} nvidia-smi:i386{u} update-glx{u} 
下列软件包将被升级：
  libcuda1 libcuda1:i386 libglx-nvidia0 libglx-nvidia0:i386 libnvidia-cfg1 libnvidia-fatbinaryloader libnvidia-fatbinaryloader:i386 libnvidia-glcore libnvidia-glcore:i386 libnvidia-ml1 
  libnvidia-ptxjitcompiler1 libnvidia-ptxjitcompiler1:i386 nvidia-alternative{b} nvidia-kernel-dkms nvidia-vdpau-driver 
15 个软件包被升级，新安装 1 个，6 个将被删除， 同时 99 个将不升级。
需要获取 41.1 MB 的存档。解包后将要使用 8,147 kB。
下列软件包存在未满足的依赖关系：
 libegl-nvidia0 : 依赖: nvidia-alternative (= 390.67-3deepin) but 430.64-4~bpo10+1 is to be installed
 libgl1-nvidia-glx : 依赖: nvidia-alternative (= 390.67-3deepin) but 430.64-4~bpo10+1 is to be installed
                     依赖: libnvidia-glcore (= 390.67-3deepin) but 430.64-4~bpo10+1 is to be installed
 nvidia-kernel-support : 依赖: nvidia-alternative (= 390.67-3deepin) but 430.64-4~bpo10+1 is to be installed
 libnvcuvid1 : 依赖: libcuda1 (= 390.67-3deepin) but 430.64-4~bpo10+1 is to be installed
 xserver-xorg-video-nvidia : 依赖: libnvidia-glcore (= 390.67-3deepin) but 430.64-4~bpo10+1 is to be installed
                             依赖: nvidia-alternative (= 390.67-3deepin) but 430.64-4~bpo10+1 is to be installed
 nvidia-alternative : 依赖: glx-alternative-nvidia (>= 0.9) but it is not going to be installed
下列动作将解决这些依赖关系：

      删除 下列软件包：                                                                               
1)      libgl1-nvidia-glx [390.67-3deepin (now, panda)]                                               

      安装 下列软件包：                                                                               
2)      libgl1-nvidia-glvnd-glx [430.64-4~bpo10+1 (buster-backports)]                                 

      升级 下列软件包：                                                                               
3)      glx-alternative-mesa [0.8.7 (now, panda) -> 0.9.1~bpo9+1 (stretch-backports)]                 
4)      glx-alternative-nvidia [0.8.7 (now, panda) -> 0.9.1~bpo9+1 (stretch-backports)]               
5)      glx-diversions [0.8.7 (now, panda) -> 0.9.1~bpo9+1 (stretch-backports)]                       
6)      libegl-nvidia0 [390.67-3deepin (now, panda) -> 430.64-4~bpo10+1 (buster-backports)]           
7)      libnvcuvid1 [390.67-3deepin (now, panda) -> 430.64-4~bpo10+1 (buster-backports)]              
8)      libnvidia-eglcore [390.67-3deepin (now, panda) -> 430.64-4~bpo10+1 (buster-backports)]        
9)      nvidia-egl-icd [390.67-3deepin (now, panda) -> 430.64-4~bpo10+1 (buster-backports)]           
10)     nvidia-kernel-support [390.67-3deepin (now, panda) -> 430.64-4~bpo10+1 (buster-backports)]    
11)     update-glx [0.8.7 (now, panda) -> 0.9.1~bpo9+1 (stretch-backports)]                           
12)     xserver-xorg-video-nvidia [390.67-3deepin (now, panda) -> 430.64-4~bpo10+1 (buster-backports)]

      Leave the following dependencies unresolved:                                                    
13)     libgl1-nvidia-glx 推荐 nvidia-kernel-dkms (= 390.67-3deepin) | nvidia-kernel-390.67           
14)     xserver-xorg-video-nvidia 推荐 nvidia-kernel-dkms (= 390.67-3deepin) | nvidia-kernel-390.67 

--------------------------------
$ sudo aptitude install -s -t buster-backports nvidia-alternative
下列软件包将被“删除”：           
  glx-alternative-mesa{u} glx-alternative-nvidia{u} glx-diversions{u} update-glx{u} 
下列软件包将被升级：
  nvidia-alternative{b} 
1 个软件包被升级，新安装 0 个，4 个将被删除， 同时 80 个将不升级。
需要获取 207 kB 的存档。解包后将释放 135 kB。
下列软件包存在未满足的依赖关系：
 libnvidia-ml1:i386 : 依赖: nvidia-alternative:i386 (= 390.87-2~bpo9+1) but it is not going to be installed
 nvidia-smi:i386 : 依赖: nvidia-alternative:i386 (= 390.87-2~bpo9+1) but it is not going to be installed
 nvidia-kernel-support : 依赖: nvidia-alternative (= 390.87-2~bpo9+1) but 430.64-4~bpo10+1 is to be installed
 libcuda1 : 依赖: nvidia-alternative (= 390.87-2~bpo9+1) but 430.64-4~bpo10+1 is to be installed
 libcuda1:i386 : 依赖: nvidia-alternative:i386 (= 390.87-2~bpo9+1) but it is not going to be installed
 nvidia-alternative : 依赖: glx-alternative-nvidia (>= 0.9) but it is not going to be installed
下列动作将解决这些依赖关系：

      删除 下列软件包：                                                                                    
1)      libcuda1:i386 [390.87-2~bpo9+1 (lion, now)]                                                        
2)      libcuda1-i386:i386 [390.87-2~bpo9+1 (lion, now)]                                                   
3)      libnvidia-fatbinaryloader:i386 [390.87-2~bpo9+1 (lion, now)]                                       
4)      nvidia-smi:i386 [390.87-2~bpo9+1 (lion, now)]                                                      

      升级 下列软件包：                                                                                    
5)      glx-alternative-mesa [0.8.8~bpo9+1 (lion, now) -> 0.9.1~bpo9+1 (stretch-backports)]                
6)      glx-alternative-nvidia [0.8.8~bpo9+1 (lion, now) -> 0.9.1~bpo9+1 (stretch-backports)]              
7)      glx-diversions [0.8.8~bpo9+1 (lion, now) -> 0.9.1~bpo9+1 (stretch-backports)]                      
8)      libcuda1 [390.87-2~bpo9+1 (lion, now) -> 430.64-4~bpo10+1 (buster-backports)]                      
9)      libnvcuvid1 [390.87-2~bpo9+1 (lion, now) -> 430.64-4~bpo10+1 (buster-backports)]                   
10)     libnvidia-fatbinaryloader [390.87-2~bpo9+1 (lion, now) -> 430.64-4~bpo10+1 (buster-backports)]     
11)     libnvidia-ml1:i386 [390.87-2~bpo9+1 (lion, now) -> 430.64-4~bpo10+1 (buster-backports)]            
12)     libnvidia-ptxjitcompiler1 [390.87-2~bpo9+1 (lion, now) -> 430.64-4~bpo10+1 (buster-backports)]     
13)     libnvidia-ptxjitcompiler1:i386 [390.87-2~bpo9+1 (lion, now) -> 430.64-4~bpo10+1 (buster-backports)]
14)     nvidia-kernel-support [390.87-2~bpo9+1 (lion, now) -> 430.64-4~bpo10+1 (buster-backports)]         
15)     update-glx [0.8.8~bpo9+1 (lion, now) -> 0.9.1~bpo9+1 (stretch-backports)]   

