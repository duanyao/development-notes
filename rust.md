## 安装
```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
rustup update
```
默认安装位置为 `~/.cargo/bin`，包括 rustc rustup cargo 等工具 。rustup 安装时会自动将其加入 PATH（修改了 ~/.bashrc ）。
