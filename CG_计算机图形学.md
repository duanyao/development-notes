## 参考资料

Small lib for polygon offsetting(margin/padding).
  https://github.com/w8r/polygon-offset

Polygon Offsetting
  http://sseemayer.github.io/Py2D/documentation/features/offset.html

Polygonal skeletons (straight skeletons)
  http://scikit-geometry.github.io/scikit-geometry/skeleton.html

CGAL 5.5.2 - 2D Straight Skeleton and Polygon Offsetting 
  https://doc.cgal.org/latest/Straight_skeleton_2/index.html

An Algorithm for Inflating and Deflating Polygons
  https://www.baeldung.com/cs/polygons-inflating-deflating
