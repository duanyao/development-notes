## 参考资料
[1] How do I prevent mouse movement from waking up a suspended computer?
https://askubuntu.com/questions/252743/how-do-i-prevent-mouse-movement-from-waking-up-a-suspended-computer

[2] Make changes to `/proc/acpi/wakeup` permanent
https://unix.stackexchange.com/questions/417956/make-changes-to-proc-acpi-wakeup-permanent

[3] How does systemd-tmpfiles work?
https://unix.stackexchange.com/questions/61159/how-does-systemd-tmpfiles-work

[4] Power management/Suspend and hibernate
https://wiki.archlinux.org/index.php/Power_management/Suspend_and_hibernate#Suspend/hibernate_does_not_work,_or_does_not_work_consistently

[5] Apply the proc/acpi/wakeup settings permanently
https://askubuntu.com/questions/1146264/apply-the-proc-acpi-wakeup-settings-permanently

## 唤醒
当前可以唤醒计算机的方法，可以从 /proc/acpi/wakeup 文件看到，例如：

```
Device  S-state   Status   Sysfs node
PEGP      S4    *disabled
PEG1      S4    *disabled
PEGP      S4    *disabled
PEG2      S4    *disabled
PEGP      S4    *disabled
PS2K      S3    *enabled   pnp:00:04
                *disabled  serio:serio0
PS2M      S3    *disabled  pnp:00:05
XHC       S3    *enabled   pci:0000:00:14.0
HDAS      S4    *disabled  pci:0000:00:1f.3
RP01      S4    *enabled   pci:0000:00:1c.0
PXSX      S4    *disabled  pci:0000:01:00.0
RP02      S4    *disabled
PXSX      S4    *disabled
RP03      S4    *disabled
PXSX      S4    *disabled
RP04      S4    *disabled
PXSX      S4    *disabled
RP05      S4    *disabled  pci:0000:00:1c.4
PXSX      S4    *disabled  pci:0000:02:00.0
                *disabled  platform:rtsx_pci_sdmmc.0
RP06      S5    *enabled   pci:0000:00:1c.5
PXSX      S5    *disabled  pci:0000:03:00.0
RP07      S5    *disabled
PXSX      S5    *disabled
RP08      S4    *disabled
PXSX      S4    *disabled
RP09      S4    *disabled
PXSX      S4    *disabled
RP10      S4    *disabled
PXSX      S4    *disabled
RP11      S4    *disabled
PXSX      S4    *disabled
RP12      S4    *enabled   pci:0000:00:1d.0
PXSX      S4    *disabled  pci:0000:04:00.0
RP13      S4    *disabled
PXSX      S4    *disabled
RP14      S4    *disabled
PXSX      S4    *disabled
RP15      S4    *disabled
PXSX      S4    *disabled
RP16      S4    *disabled
PXSX      S4    *disabled
RP17      S4    *disabled
PXSX      S4    *disabled
RP18      S4    *disabled
PXSX      S4    *disabled
RP19      S4    *disabled
PXSX      S4    *disabled
RP20      S4    *disabled
PXSX      S4    *disabled
```

其中 S3 是待机，EHC1, EHC2, XHCI, XHC 是 USB 鼠标键盘，PWRB 是电源键，LID 是笔记本电脑屏幕翻折开关，TPAD 是触摸板，
PS2K 是键盘（似乎也包括笔记本内置键盘），PS2M 是鼠标（似乎也包括笔记本触摸板）。

命令 `echo XHC > /proc/acpi/wakeup` 或者 `sudo sh -c "echo XHC > /proc/acpi/wakeup"` 可以启用/停用唤醒设备。

/proc/acpi/wakeup 没有列出的设备可能属于不可配置的设备，总是采取默认行为。

### 笔记本 lid 与唤醒

笔记本 lid 关闭的情况下，仍有可能被唤醒，这可能是因为屏幕刚度不足，受压时触碰了键盘或触摸板，造成唤醒。
linux 似乎无法设置一种策略，让系统在 lid 关闭的情况下无法被唤醒，或者唤醒后又自动待机。
所以，要避免这种问题，似乎只能禁用键盘和触摸板的唤醒。

## 唤醒日志

唤醒日志如下面所示。目前看来不可能知道唤醒的具体设备。

```
4月 20 10:18:58 duanyao-laptop-c kernel: Freezing user space processes ... (elapsed 0.003 seconds) done.
4月 20 10:18:58 duanyao-laptop-c kernel: OOM killer disabled.
4月 20 10:18:58 duanyao-laptop-c kernel: Freezing remaining freezable tasks ... (elapsed 0.001 seconds) done.
4月 20 10:18:58 duanyao-laptop-c kernel: printk: Suspending console(s) (use no_console_suspend to debug)
4月 20 10:18:58 duanyao-laptop-c kernel: wlo1: deauthenticating from b0:73:5d:48:5c:00 by local choice (Reason: 3=DEAUTH_LEAVING)
4月 20 10:18:58 duanyao-laptop-c kernel: sd 0:0:0:0: [sda] Synchronizing SCSI cache
4月 20 10:18:58 duanyao-laptop-c kernel: sd 0:0:0:0: [sda] Stopping disk
4月 20 10:18:58 duanyao-laptop-c kernel: ACPI: EC: interrupt blocked
4月 20 10:18:58 duanyao-laptop-c kernel: ACPI: Preparing to enter system sleep state S3
4月 20 10:18:58 duanyao-laptop-c kernel: ACPI: EC: event blocked
4月 20 10:18:58 duanyao-laptop-c kernel: ACPI: EC: EC stopped
4月 20 10:18:58 duanyao-laptop-c kernel: PM: Saving platform NVS memory
4月 20 10:18:58 duanyao-laptop-c kernel: Disabling non-boot CPUs ...
4月 20 10:18:58 duanyao-laptop-c kernel: smpboot: CPU 1 is now offline
4月 20 10:18:58 duanyao-laptop-c kernel: smpboot: CPU 2 is now offline
4月 20 10:18:58 duanyao-laptop-c kernel: smpboot: CPU 3 is now offline
4月 20 10:18:58 duanyao-laptop-c kernel: ACPI: Low-level resume complete
4月 20 10:18:58 duanyao-laptop-c kernel: ACPI: EC: EC started
4月 20 10:18:58 duanyao-laptop-c kernel: PM: Restoring platform NVS memory
4月 20 10:18:58 duanyao-laptop-c kernel: Enabling non-boot CPUs ...
4月 20 10:18:58 duanyao-laptop-c kernel: x86: Booting SMP configuration:
4月 20 10:18:58 duanyao-laptop-c kernel: smpboot: Booting Node 0 Processor 1 APIC 0x2
4月 20 10:18:58 duanyao-laptop-c kernel: CPU1 is up
4月 20 10:18:58 duanyao-laptop-c kernel: smpboot: Booting Node 0 Processor 2 APIC 0x1
4月 20 10:18:58 duanyao-laptop-c kernel: CPU2 is up
4月 20 10:18:58 duanyao-laptop-c kernel: smpboot: Booting Node 0 Processor 3 APIC 0x3
4月 20 10:18:58 duanyao-laptop-c kernel: CPU3 is up
4月 20 10:18:58 duanyao-laptop-c kernel: ACPI: Waking up from system sleep state S3
4月 20 10:18:58 duanyao-laptop-c kernel: ACPI: EC: interrupt unblocked
4月 20 10:18:58 duanyao-laptop-c kernel: nvidia 0000:01:00.0: Enabling HDA controller
4月 20 10:18:58 duanyao-laptop-c kernel: ACPI: EC: event unblocked
4月 20 10:18:58 duanyao-laptop-c kernel: sd 0:0:0:0: [sda] Starting disk
4月 20 10:18:58 duanyao-laptop-c kernel: usb 1-3: reset high-speed USB device number 2 using xhci_hcd
4月 20 10:18:58 duanyao-laptop-c kernel: ata1: SATA link up 6.0 Gbps (SStatus 133 SControl 300)
4月 20 10:18:58 duanyao-laptop-c kernel: ata2: SATA link down (SStatus 4 SControl 300)
4月 20 10:18:58 duanyao-laptop-c kernel: ata3: SATA link down (SStatus 4 SControl 300)
4月 20 10:18:58 duanyao-laptop-c kernel: ata1.00: configured for UDMA/100
4月 20 10:18:58 duanyao-laptop-c kernel: ahci 0000:00:17.0: port does not support device sleep
4月 20 10:18:58 duanyao-laptop-c kernel: OOM killer enabled.
4月 20 10:18:58 duanyao-laptop-c kernel: Restarting tasks ... done.
4月 20 10:18:58 duanyao-laptop-c wpa_supplicant[780]: wlo1: CTRL-EVENT-DISCONNECTED bssid=b0:73:5d:48:5c:00 reason=3 locally_generated=1
4月 20 10:18:58 duanyao-laptop-c com.deepin.dde.daemon.Dock[8910]: <warning> audio_config.go:135: snd_ctl_open: 没有那个文件或目录
4月 20 10:18:58 duanyao-laptop-c wpa_supplicant[780]: dbus: wpa_dbus_property_changed: no property SessionLength in object /fi/w1/wpa_supplicant1/Interfaces/17
4月 20 10:18:58 duanyao-laptop-c wpa_supplicant[780]: wlo1: CTRL-EVENT-REGDOM-CHANGE init=DRIVER type=WORLD
4月 20 10:18:58 duanyao-laptop-c daemon/audio[26203]: audio_config.go:135: snd_ctl_open: 没有那个文件或目录
4月 20 10:18:58 duanyao-laptop-c NetworkManager[762]: <warn>  [1618885138.5850] sup-iface[0x13988b0,wlo1]: connection disconnected (reason -3)
4月 20 10:18:58 duanyao-laptop-c systemd[1]: Reloading Laptop Mode Tools.
4月 20 10:18:58 duanyao-laptop-c NetworkManager[762]: <info>  [1618885138.6393] device (wlo1): supplicant interface state: completed -> disconnected
4月 20 10:18:58 duanyao-laptop-c systemd-sleep[32231]: System resumed.
4月 20 10:18:58 duanyao-laptop-c NetworkManager[762]: <info>  [1618885138.6832] device (wlo1): supplicant interface state: disconnected -> scanning
4月 20 10:18:58 duanyao-laptop-c kernel: PM: suspend exit
4月 20 10:18:58 duanyao-laptop-c systemd[1]: Started Atop advanced performance monitor.
4月 20 10:18:58 duanyao-laptop-c systemd[1]: systemd-suspend.service: Succeeded.
4月 20 10:18:58 duanyao-laptop-c systemd[1]: Started Suspend.
4月 20 10:18:58 duanyao-laptop-c systemd[1]: Stopped target Sleep.
4月 20 10:18:58 duanyao-laptop-c systemd[1]: Reached target Suspend.
4月 20 10:18:58 duanyao-laptop-c systemd[1]: Stopped target Suspend.
4月 20 10:18:58 duanyao-laptop-c systemd-logind[1194]: Operation 'sleep' finished.
```
