
## 参考资料

[1.0] 综合资料

[1.1] docker 官方文档
https://docs.docker.com/

[1.2] hub docker 镜像库
https://hub.docker.com/

[1.3] Docker — 从入门到实践
https://docker_practice.gitee.io/zh-cn/
https://vuepress.mirror.docker-practice.com/
https://yeasy.gitbook.io/docker_practice/

[1.4] 详解 Docker！数据分析与开发 2023-08-07
https://mp.weixin.qq.com/s/DtefbjA8b_l1gYPVs-RHRg

[1.5] （过时，2019） Docker 教程
http://www.runoob.com/docker/docker-tutorial.html

[2.0] docker 的安装

[2.1] Debian 安装 Docker CE
https://docker_practice.gitee.io/zh-cn/install/debian.html

[2.2] 镜像加速器
https://docker_practice.gitee.io/zh-cn/install/mirror.html

[2.3] ustc Docker Hub 源使用帮助
https://mirrors.ustc.edu.cn/help/docker-ce.html

[3.0] 基本使用，容器和镜像管理

[3.6] docker run 如何让容器启动后不会自动停止
https://jerrymei.cn/docker-container-run-not-stop-automatically/

[3.8] Docker: Visualizing image hierarchy and container dependency using dockviz
https://fabianlee.org/2017/05/24/docker-visualizing-image-hierarchy-and-container-dependency-using-dockviz/
https://github.com/justone/dockviz

[4.0] 存储系统
[4.1] Where are Docker Images Stored? Docker Container Paths Explained
https://www.freecodecamp.org/news/where-are-docker-images-stored-docker-container-paths-explained/

[4.2] containerd Where are images stored?
https://github.com/containerd/containerd/discussions/5366

[4.3] About storage drivers
https://docs.docker.com/storage/storagedriver/

[4.4] Docker - How to analyze a container's disk usage?
https://stackoverflow.com/questions/26753087/docker-how-to-analyze-a-containers-disk-usage

[4.5] Use the BTRFS storage driver
https://docs.docker.com/storage/storagedriver/btrfs-driver/

[4.6] containerd image store with Docker Engine
https://docs.docker.com/storage/containerd/

[5.0] 网络
[5.1] Configure the daemon to use a proxy
https://docs.docker.com/config/daemon/proxy/

[6.0] 服务管理

[7.0] 日志系统

[8.0] 镜像构建

[8.5] How to see all output when executing `docker build`
https://devsolus.com/2023/03/17/how-to-see-all-output-when-executing-docker-build/


[8.6] Optimizing builds with cache management
https://docs.docker.com/build/cache/

[8.8] Cache storage backends
https://docs.docker.com/build/cache/backends/

[8.9] Garbage collection
https://docs.docker.com/build/cache/garbage-collection/

[8.10] docker builder prune
https://docs.docker.com/engine/reference/commandline/builder_prune/

[8.11] Base images （从头创建基础镜像）
https://docs.docker.com/build/building/base-images/

[8.12] Debootstrap
https://wiki.debian.org/Debootstrap

[9.0] 调试

[10.0] 排除故障
[10.1] Failed to pull image (unexpected commit digest)
https://github.com/containerd/containerd/issues/3974

[2.1] Dockfile 参考
https://docs.docker.com/engine/reference/builder/

[2.1] Systemd 232 导致 Docker container 启动问题
  https://havee.me/linux/2017-01/docker-container-start-problem.html
  
[2.2] docker: Error response from daemon: oci runtime error: apparmor failed to apply profile: no such file or directory
  https://github.com/moby/moby/issues/25488

[2.3] docker 中设置时区
  https://lengzzz.com/note/timezone-in-docker

[3.1] docker-wordpress Dockerfile
  https://github.com/jbfink/docker-wordpress/blob/master/Dockerfile


[6.1] 解决 UFW 和 Docker 的问题 https://github.com/chaifeng/ufw-docker

[7.1] docker-compose 下载
https://github.com/docker/compose/releases

[7.3] How to do docker-compose down without the config file that made the up?
https://serverfault.com/questions/990479/how-to-do-docker-compose-down-without-the-config-file-that-made-the-up

[7.4] How to restart a single container with docker-compose
https://stackoverflow.com/questions/31466428/how-to-restart-a-single-container-with-docker-compose

[8.1] [Docker]清理Docker占用的磁盘空间，迁移 /var/lib/docker 目录所解决的问题
https://blog.csdn.net/truelove12358/article/details/102949386

[9.1] How to get exact date for docker images?
https://stackoverflow.com/questions/32705176/how-to-get-exact-date-for-docker-images

[10.1] NFS Docker Volumes: How to Create and Use
https://phoenixnap.com/kb/nfs-docker-volumes

[10.2] Mount NFS Share Directly in docker-compose File
https://blog.stefandroid.com/2021/03/03/mount-nfs-share-in-docker-compose.html

[11.1] Docker Container GUI Display
https://leimao.github.io/blog/Docker-Container-GUI-Display/

[12.1] How to Clear Logs of Running Docker Containers
https://www.howtogeek.com/devops/how-to-clear-logs-of-running-docker-containers/

[12.2] Configure logging drivers
https://docs.docker.com/config/containers/logging/configure/

[13.1] 理解 docker 容器中的 uid 和 gid
https://www.cnblogs.com/sparkdev/p/9614164.html

[13.2] Setting the User in a Docker Container From the Host
https://www.baeldung.com/linux/docker-set-user-container-host

[14.1] Isolate containers with a user namespace
https://docs.docker.com/engine/security/userns-remap/

[15.1] Systemd 对垒 Docker 
https://linux.cn/article-7068-1.html
https://lwn.net/Articles/676831/ ***

[15.2] What is the scoop on running systemd in a container? **
https://developers.redhat.com/blog/2016/09/13/running-systemd-in-a-non-privileged-container/

[15.3] How to run systemd in a container 2019
https://developers.redhat.com/blog/2019/04/24/how-to-run-systemd-in-a-container#

[15.4] How to run systemd services in Arch Linux Docker container?
https://unix.stackexchange.com/questions/305340/how-to-run-systemd-services-in-arch-linux-docker-container/499585#499585

[15.5] systemd The Container Interface
https://systemd.io/CONTAINER_INTERFACE/、

[15.6] 在 Docker 中使用 Systemd (2021)
https://blog.csdn.net/kencaber/article/details/121980242

[15.7] Systemd fails to run in a docker container when using cgroupv2 (--cgroupns=private)
https://serverfault.com/questions/1053187/systemd-fails-to-run-in-a-docker-container-when-using-cgroupv2-cgroupns-priva/1054414#1054414

[16.1] Why strace doesn't work in Docker
https://jvns.ca/blog/2020/04/29/why-strace-doesnt-work-in-docker/

[17] 两种方式创建你自己的 Docker 基本映像 
https://linux.cn/article-5427-1.html

[18] 创建尽可能小的 Docker 容器 
https://linux.cn/article-5597-1-rel.html

[19] Docker 的镜像并不安全！ 
https://linux.cn/article-4702-1-rel.html

[20] 玩转Docker镜像
https://linux.cn/article-4432-1-rel.html

[21] 年终盘点：解读2016之容器篇——“已死”和“永生” 
https://yq.aliyun.com/articles/67178?spm=5176.100239.blogcont5849.40.6HRESz

[24] 从容器规范看Docker和Rocket **
https://linux.cn/article-4688-1.html

[25] Introducing dumb-init, an init system for Docker containers (以及一些关于容器技术的原理) ***
https://engineeringblog.yelp.com/2016/01/dumb-init-an-init-for-docker.html
https://github.com/Yelp/dumb-init

[26] Tini - A tiny but valid init for containers
https://github.com/krallin/tini

[27] Kubernetes 有状态集群服务部署与管理
https://mp.weixin.qq.com/s?__biz=MzI4NzE1NTYyMg==&mid=2651102805&idx=1&sn=a815ed83fde49b1b43db18fab2e10dea&chksm=f021ca9cc756438aedc84a41d03d25296bd0ce8103585b88da5a9d9544e610d6cd895725c16f#rd



[29] Docker官方将支持Kubernetes，容器编排大战宣告结束
http://www.infoq.com/cn/news/2017/10/Docker-Kubernetes-Swarm

[29] 关于docker的15个小tip
http://www.cnblogs.com/elnino/p/3899136.html

[41] Dockerfile to create a Docker container image for Squid proxy server 
https://github.com/sameersbn/docker-squid

[42] 使用Squid3搭建Docker镜像下载代理 
http://dockone.io/article/1380

## 原理
容器是一种没有独立操作系统内核的系统级虚拟机。它的用户空间进程、文件系统是与宿主机和其它容器隔离的，但内核空间与宿主机及其它容器共享。比起真正的系统级虚拟机，容器消耗较少的内存和硬盘资源，启动更迅速。但容器的隔离不如真虚拟机彻底，且无法使用与宿主机不同的操作系统内核。

docker 是容器的一种实现，主要在 linux 系统上流行，但在 windows 和 mac 上也可以运行（通过虚拟机或WSL 系统模拟 linux 内核）。

docker 利用 linux 内核的 namespaces 和 cgroups 机制来实现隔离和资源配额。

docker 底层的“容器运行时”其实有多种方案，早期使用  LXC，目前使用 Native 和 containerd。

与 docker 类似的竞争技术还有 podman 和 kubernetes （k8s） 等。

## 安装 docker for linux

### 版本
docker-ce 是社区版本，适用于个人或者小型团队。Ubuntu默认安装的是 docker-ce 。docker-ce 的版本号是 19.03 这样的，19 指 2019 年。
docker-ee 是docker的企业版，适用于企业级开发，同样也适用于开发、分发和运行商务级别的应用的IT 团队。

docker-io, docker-engine 是以前早期的版本，版本号是 1.*，默认centos7 安装的是docker-io，最新版是 1.13。

### （过时）apparmor 问题

对于某些早期 linux 系统（如 deepin 15.7 2018-9-27），可能需要删除 apparmor 才可以正常运行 docker。

`sudo apt remove apparmor`

否则，即使能启动容器，在容器内也会遇到 `Permission denied` 之类的错误，例如

```
# ping www.baidu.com
ping: socket: Permission denied
```

某些情况下，可以不删除 apparmor ，但需要在运行 docker 命令时加上 `--security-opt apparmor=unconfined` 参数。

### ustc deb 源 docker-ce
帮助文档：[2.3]。

debian:
```
curl -fsSL https://mirrors.ustc.edu.cn/docker-ce/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://mirrors.ustc.edu.cn/docker-ce/linux/debian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker-ce.list > /dev/null
```

如果是 debian 衍生版（如 deepin ），lsb_release 的输出可能无法匹配debian版本，第二步就需要手动指定版本：

```
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://mirrors.ustc.edu.cn/docker-ce/linux/debian bookworm stable" | sudo tee /etc/apt/sources.list.d/docker-ce.list > /dev/null
```
stretch, buster, bullseye, bookworm 分别是 debian 9~12。

```
sudo apt update
apt policy docker-ce
sudo apt-get install docker-ce=5:23.0.6-1~debian.10~buster
```

ununtu 20.04~23.10:
```
sudo apt install curl

curl -fsSL https://mirrors.ustc.edu.cn/docker-ce/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://mirrors.ustc.edu.cn/docker-ce/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker-ce.list > /dev/null
```
结果：
```
cat /etc/apt/sources.list.d/docker-ce.list
# 或者 20.04
deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://mirrors.ustc.edu.cn/docker-ce/linux/ubuntu focal stable
# 或者 22.04
deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://mirrors.ustc.edu.cn/docker-ce/linux/ubuntu jammy stable
# 或者 23.10
deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://mirrors.ustc.edu.cn/docker-ce/linux/ubuntu mantic stable
```
安装：
```
sudo apt update
sudo apt install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```
### aliyun centos yum 源 docker-ce

https://help.aliyun.com/document_detail/51853.html

```
wget -O /etc/yum.repos.d/docker-ce.repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

# 安装Alibaba Cloud Linux 2专用的yum源兼容插件。仅Alibaba Cloud Linux 2 需要
yum install yum-plugin-releasever-adapter --disablerepo=* --enablerepo=plus

yum search docker-ce
yum install docker-ce
systemctl start docker
```
### 设置镜像站点
[4.2]
在 /etc/docker/daemon.json 中写入如下内容（如果文件不存在请新建该文件）

```
{
    "registry-mirrors": [
        "http://f1361db2.m.daocloud.io", # 不可用
        "https://docker.mirrors.ustc.edu.cn", # 不可用
        "https://hub-mirror.c.163.com", # 不可用
        "https://mirror.ccs.tencentyun.com", # 不可用
        "https://as9e4cqq.mirror.aliyuncs.com", 
    ]
}
```
阿里云提供“镜像加速器”，仅限注册用户，可以在“控制台-容器镜像服务-镜像工具-镜像加速器”找到镜像加速器url，例如 `https://as9e4cqq.mirror.aliyuncs.com`。
华为云有类似的功能，在“镜像资源”---> “镜像中心”---> "镜像加速器"，其地址为 `XXX.mirror.swr.myhuaweicloud.com` 。

### 让普通用户可以使用 docker

```
sudo groupadd docker  # 可能已经在安装时自动创建
sudo usermod -aG docker $USER
```

### 启动用 nvidia GPU 插件
为了在 docker 容器内使用 GPU （--gpus 参数），需要安装 `nvidia-container-toolkit` 包。具体方法可参考 nvidia-linux.md 文档。

## 下载镜像

### 基本下载操作

注意：2024.7 以后从境内下载境外镜像时需要通过 http 代理（`*.docker.com` 暂不需要代理），设置方法见后面。
```
docker search debian

# 具体的版本（tag）可以在 https://hub.docker.com/ 上查看

docker pull debian:bookworm

bookworm: Pulling from library/debian
Digest: sha256:3f1d6c17773a45c97bd8f158d665c9709d7b29ed7917ac934086ad96f92e4510
Status: Downloaded newer image for debian:bookworm
docker.io/library/debian:bookworm
```

### 镜像的命名
镜像有两套命名体系，一是镜像id（image id），基于镜像散列值，是全局唯一的；二是镜像名，是 docker 用户以及 docker 仓库服务器维护的，并不唯一。
可以注意到，当 `docker pull` 请求下载的镜像名为 `debian:bookworm` 时， `docker pull` 输出中表示它实际采用的镜像名为 `docker.io/library/debian:bookworm` 。前者为简称，后者为全称。
一个完整镜像名的语法为：`[HOST[:PORT_NUMBER]/]PATH`，主机名是IP地址或域名，其中应至少有一个 `.` 以免与用户名混淆，主机名后可加上可选的端口号。上面的例子中是 `docker.io` 。
PATH 由 `/` 分割为多个部分，每个部分的语法是：由英文小写字母`a-z`、数字`0-9`、下划线`_`、减号`-`组成，只有字母和数字可出现在首位；PATH 中不可出现小数点`.`。
有的实现允许 PATH 中出现2个或更多 `/` ，但大部分实现（包括 docker hub）要求。好有一个 `/`，也就是说，PATH 部分被进一步规定为 `NAMESPACE/REPOSITORY:TAG` 结构，即 `[HOST[:PORT_NUMBER]/][NAMESPACE/]REPOSITORY[:TAG]` 。
NAMESPACE 是组织名或用户名，如例子中的 `library` ，REPOSITORY 则是一般理解的镜像名，如例子中的 `debian` 。
TAG 的语法是：由英文小写字母`a-z`、数字`0-9`、下划线`_`、减号`-`、小数点`.`组成，不能以减号`-`、小数点`.`开头。
HOST、 NAMESPACE、TAG 不指定时的默认值分别是 docker.io 、library 、latest。例如 `debian` 等效于 `docker.io/library/debian:latest` 。

在本地机器上命名一个镜像，并不要求它真的存在于其名字指定的仓库服务器上，所以可以随便命名，省略 HOST 部分。只有 `docker pull/push` 的时候才会试图访问镜像名指定的服务器。

镜像名的指向，或者说和镜像id的对应关系是可变的。例如，镜像仓库会更新镜像，然后将镜像名指向更新后的镜像id。

### 启用 containerd-snapshotter
默认的 docker pull 下载功能没有断点续传能力，任何一个 layer 下载失败，整个镜像都要从头重新下载。
containerd-snapshotter 的好处是有断点续传能力。
修改 /etc/docker/daemon.json :
``` 
{
  "max-concurrent-downloads": 1,
  "features": {
    "containerd-snapshotter": true
  }
}
```
重启 `sudo systemctl restart docker` 。
此时，docker 下载镜像是转交给 containerd 执行的，下载中的镜像不在 docker 数据目录中，而是在 containerd 的数据目录 （ /var/lib/containerd/io.containerd.content.v1.content/ ）中。
containerd-snapshotter 的缺点是
* 存在稳定性问题，可能出现 commit failed: unexpected commit digest 错误，处理方法详见后面。
* 并发下载数无法控制，`/etc/docker/daemon.json` 的 max-concurrent-downloads 无效，一般会有 8 个 layer 在同时下载。

### 用 ctr 下载
ctr 属于 containerd.io 包。
```
sudo ctr ns ls # 列出 namespace
sudo ctr -n <namespace> image pull <image_name> # 可以省略 namespace
```
ctr 只能用 root 用户运行。
尚不清楚如何让 ctr 用用户名和密码登录 docker 仓库。

### 用 podman 下载

```
podman pull --retry=6 image_spec
```
podman pull 与 docker pull 一样容易失败，但它可以指定重试的次数，默认是3次。

## 运行

### 交互式控制台模式
```
mkdir project
cd project
docker run --rm -it --gpus all --name xxx -v /sys/fs/cgroup:/sys/fs/cgroup:ro -v $PWD:/project -w /project daocloud.io/centos:7 bash

-i 交互式， -t 使用终端。如果要一次性运行一个程序则不需要带这两个参数。
-v $PWD:/project 挂载本地目录（project）到容器内的 /project 目录。
  注意：指定本地目录不能用相对路径，也不要拼写错误，docker 对于找不到的本地路径不会报错，而是挂载一个空目录。
--gpus all 让容器可以使用全部的 GPU。--gpus '"device=0,2"' 可以选择暴露给容器的 GPU。
-w /project 进入容器后，进入 /project 目录。
daocloud.io/centos:7 镜像名。其中 daocloud.io/centos 是仓库源（repository），7是标签（tag）。
bash 进入容器后要运行的程序，可以是全路径。
--name xxx 指定容器的名字，之后可以引用它。
--rm 如果容器已经存在则先删除它。默认不删除。
```

启动 docker 容器后，可以附加更多的 shell 上去：

```
docker exec -it --user root xxx bash
```

“xxx” 是 docker 容器的id的前几位，或者容器的名字，可以用 sudo docker ps 来查看。
`--user root` 表示用 root 用户来执行，可以改为其它用户。

### 后台服务模式

```
docker run --rm -d --gpus all --name xxx daocloud.io/centos:7 tail -f /dev/null
```
不使用 -i 或 -it 而是使用 --detach , -d，则让容器在后台运行。之后可以用 `docker exec -it container_id_or_name cmd args` 执行交互式命令。

### GPU 模式
```
docker run --gpus all --shm-size=1g --ulimit memlock=-1 -it --rm local/ubuntu2004-cuda112-cudnn8-dev-py39-cv-paddle242cu112 bash
```
--gpus all 让容器可以使用全部的 GPU。--gpus '"device=0,2"' 可以选择暴露给容器的 GPU。

`--shm-size=1g --ulimit memlock=-1` 放宽共享内存的限制，否则使用多个 GPU 的程序可能会失败。

### 以不同用户和组运行容器

默认情况下，docker run 和 exec 都是以容器内的 root 账户（uid和gid都是0）运行的。如果想要以其它账户运行，可以用 --user 参数：

```
docker run --rm --user 1505:1501 -it alpine id
# uid=1505 gid=1501 groups=1501

docker run --rm --user 1505:1501 -e HOME=/tmp -it alpine sh -c "echo HOME: ~"
# /tmp
```

`1505:1501` 分别是 uid 和 gid。它们可以已经存在于容器中（即已经定义在容器的 /etc/passwd 和 /etc/group），也可以不存在；甚至，这个uid和gid也可以不存在于宿主机。
在宿主机看来，容器中的进程的 uid 和 gid 也是 `1505:1501`。实际上，容器中的 uid 与宿主机的 uid 如果相同，它们就被操作系统内核认为是同一用户，具有相同的访问权限[13.1]。
如果容器和宿主有共享的目录，则容器内进程创建的文件的所有者也是 1505:1501，能够被宿主机看到。

如果 `--user` 指定的用户不存在于容器中，则此用户的家目录被设定为根目录 `/`，这可能造成一些问题，比如读写用户的文件。
此时可以用 `-e HOME=/tmp` 来指定此用户的家目录，`-e` 是指定环境变量。。

如果 `--user` 指定的用户存在于容器中，则根据 /etc/passwd 来确定用户名和家目录。

如果希望容器和宿主机共享用户信息，可以将宿主机的 /etc/passwd、/etc/group、/home 都挂载到容器上[13.2]，例如：
```
export UID=$(id -u)
export GID=$(id -g)

docker run --rm --user $UID:$GID -w "/home/$USER" -v /home:/home  -v /etc/group:/etc/group:ro -v /etc/passwd:/etc/passwd:ro -v /etc/shadow:/etc/shadow:ro alpine sh -c "id; echo HOME: \$HOME"
```

docker exec 命令同样可以指定任意 --user 参数，并且不受容器的 pid 1 进程的 uid 的影响。例如

```
docker run --rm --user 1505:1501 --name uid_test_1 -it alpine id
docker exec -it --user 0:0 uid_test_1 sh -c id
```

### 用户隔离，userns-remap

docker 可以把容器内的 root 用户映射为宿主机的普通用户，从而限制容器内的 root 用户影响宿主机的能力。这需要开启 userns-remap 功能[14.1]。
修改 `/etc/docker/daemon.json`：

```
{
  "userns-remap": "default"
}
```

此处 default 表示将容器内的 root 用户映射为宿主机的 dockremap 用户。如果希望使用其它宿主机用户，将 default 改为其它用户名。
这个修改是全局的，也就是，不同的容器不能使用不同的宿主机用户。

### GUI
可以启动 docker 容器里的GUI程序，但需要一些设置[11.1]。

```
$ xhost +
$ docker run -it --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix firefox:0.0.1
$ xhost -
```
`xhost +` 的作用是授予任意hostname访问X的权限。

### 出错
* no subsystem for mount
deepin 15.7， systemd 238-5, docker-engine 1.12.3-0~stretch
```
docker: Error response from daemon: invalid header field value "oci runtime error: container_linux.go:247: starting container process caused
\"process_linux.go:359: container init caused \\\"rootfs_linux.go:53: mounting \\\\\\\"cgroup\\\\\\\" to rootfs \\\\\\
\"/var/lib/docker/aufs/mnt/4cf1f94dc1ab68d4c082c5c3460f0fd4559bda311d5ebe6bec95d4f207f1f165\\\\\\\" at \\\\\\
\"/sys/fs/cgroup\\\\\\\" caused \\\\\\\"no subsystem for mount\\\\\\\"\\\"\"\n".
```

解决[2.1]：加上 `-v /sys/fs/cgroup:/sys/fs/cgroup:ro` 参数：
```
sudo docker run -v /sys/fs/cgroup:/sys/fs/cgroup:ro -it daocloud.io/centos:7 /bin/bash
```
按说这是 systemd 应该已经修复的问题？

* apparmor failed to apply profile

```
docker: Error response from daemon: invalid header field value "oci runtime error: container_linux.go:247: starting container process caused \"process_linux.go:359: container init caused \\\"apparmor failed to apply profile: no such file or directory\\\"\"\n".
```

解决[2.2]：

```
# 重装 apparmor（可选）
sudo rm -rf /etc/apparmor*
sudo apt install apparmor --reinstall

# 重启 apparmor 和 docker
sudo service apparmor restart
sudo service docker restart
```

## 从已有容器获取其启动时的命令行参数
可以使用 runlike 工具：https://github.com/lavie/runlike

```
pip install runlike

runlike <container_id_or_name>
```

注意 runlike 无法还原 --gpus 参数。

## DNS

docker 容器默认地复制主机的 /etc/resolv.conf 文件指定的 DNS。但是，如果主机的 DNS 是个内网地址，则 docker 容器就未必能访问。
这时就要另外指定 DNS：

命令行中指定：
```
docker run -it --dns 114.114.114.114 --dns 8.8.8.8 ubuntu:16.04
```
也可以在 /etc/default/docker 中指定。
```
DOCKER_OPTS="--dns 8.8.8.8 --dns 8.8.4.4"
```

也可以在 /etc/docker/daemon.json 中指定:
```
{                                                                          
    "dns": ["114.114.114.114", "8.8.8.8"]                                                                           
}     
```

## 防火墙

docker 和 ufw 防火墙有一定的冲突，docker 会修改 ufw 的设置方便管理，但也会带来安全隐患。
解决办法是 ufw-docker [6.1]。

## 镜像、容器操作
### 列出容器
```
sudo docker ps      # 列出运行的容器
sudo docker ps -a   # 列出所有的容器
``` 

### 运行状态

```
docker stats

CONTAINER ID   NAME                                                                   CPU %     MEM USAGE / LIMIT     MEM %     NET I/O           BLOCK I/O         PIDS
60e13fb32716   nuclio-local-storage-reader                                            0.00%     956KiB / 3.649GiB     0.02%     1.29kB / 0B       1.37MB / 0B       1
746f3ad127cc   nuclio-nuclio-openvino-dextr                                           2.39%     858.6MiB / 3.649GiB   22.98%    2.2kB / 0B        230MB / 0B        17
2b884e99f643   nuclio-nuclio-openvino-omz-intel-person-reidentification-retail-0300   2.47%     340.6MiB / 3.649GiB   9.11%     2.55kB / 0B       239MB / 0B        17
```

### 停止容器
```
# docker container stop [OPTIONS] CONTAINER [CONTAINER...]
docker stop -t 2 <container_id or container_name>
docker stop $(docker ps -a -q)     # 停止全部容器
```
docker stop 默认会等待较长时间等容器自行退出

### 删除容器
```
docker rm <container_id or container_name>
docker rm $(sudo docker ps -a -q)       # 删除全部容器
```

### 容器和宿主机之间传输/复制文件
```
docker cp <container_id or container_name>:/path/to/file /local/path
docker cp /local/path <container_id or container_name>:/path/to/file
docker cp -a /path/to/host/dir my_container:/path/to/container/dir
```
cp 可以复制文件或目录。可以让源是文件而目标是目录。-a 会保留所有属性，类似 `cp -a`。

### 列出本地镜像
```
sudo docker images
REPOSITORY           TAG                 IMAGE ID            CREATED             SIZE
daocloud.io/centos   7                   5182e96772bf        7 weeks ago         199.7 MB
learn/tutorial       latest              a7876479f1aa        5 years ago         128 MB
```
要显示绝对时间，可以用 --format，用 CreatedAt 代替 CreatedSince [9.1]。

```
docker images --format "table {{.Repository}}\t{{.Tag}}\t{{.ID}}\t{{.CreatedAt}}\t{{.Size}}"

REPOSITORY                                                    TAG            IMAGE ID       CREATED AT                      SIZE
<none>                                                        <none>         8f60cc344701   2021-12-08 21:20:03 +0800 CST   477MB
postgres                                                      10-alpine      71c418f0dbac   2021-11-30 13:39:04 +0800 CST   76.3MB
openvino/cvat_ui                                              latest         2b45ff0ccf48   2021-11-15 17:30:55 +0800 CST   49MB
openvino/cvat_server                                          latest         7720e30a355d   2021-11-15 17:26:52 +0800 CST   4.71GB
```

使用 reference 过滤器和格式化
```
docker image ls --filter=reference='*cmecloud.cn/*/*' --format "{{.Repository}}:{{.Tag}}"

cis-hub-huabei-3.cmecloud.cn/docker46io/cvat47ui:v2.16.1
cis-hub-huabei-3.cmecloud.cn/docker46io/cvat47server:v2.16.1
cis-hub-huabei-3.cmecloud.cn/docker46io/postgres:15-alpine
```
reference 过滤器匹配镜像名，要注意 `*` 可以匹配任意字符串，但除了 `/`。 

### 列出镜像历史/依赖关系
镜像历史：
```
docker image history <image_id/image_name>

IMAGE          CREATED        CREATED BY                                      SIZE      COMMENT
9fb7353dd6a6   4 months ago   /bin/sh -c #(nop)  ENTRYPOINT ["/usr/bin/sup…   0B        
<missing>      4 months ago   /bin/sh -c #(nop)  EXPOSE 8080                  0B        
<missing>      4 months ago   |2 CLAM_AV=yes INSTALL_SOURCES=yes /bin/sh -…   15.5MB    
<missing>      4 months ago   |2 CLAM_AV=yes INSTALL_SOURCES=yes /bin/sh -…   0B        
<missing>      4 months ago   /bin/sh -c #(nop) WORKDIR /home/django          0B        
<missing>      4 months ago   /bin/sh -c #(nop)  USER django                  0B    
```

用 dockviz 查看镜像之间的历史/依赖关系[3.8]：
```
alias dockviz="docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock nate/dockviz"
#或者
alias dockviz="docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock fabianlee/dockviz"
```
以 png 形式可视化：
```
sudo apt-get install graphviz

dockviz images -d -l | dot -Tpng -o dockviz.png
#或者
dockviz images --dot --only-labelled | dot -Tpng -o dockviz.png
```

以文本形式可视化：
```
dockviz images -t -l 
```

可视化容器：
```
dockviz containers -d | dot -Tpng -o containers.png
```

### 镜像的tag操作

给镜像名设置别名可以用 docker tag 命令：

```
docker tag source target
```
target 是要新的镜像名，source 是原镜像名。source 可以是镜像id 或镜像名。

### 本地提交对镜像的更改

对容器里文件系统进行的修改一般并不是持久的。如果退出容器后再次启动，则之前对文件系统的修改会丢失。要持久化修改，应该启动容器，修改，
用 sudo docker ps 显示容器 id，记下来，退出容器，然后执行命令：

```
sudo docker commit -m="install c++ dev tools" -a="duanyao" 841282edd792 duanyao/centos7:dev
```
841282edd792 是刚才记下的容器 id， duanyao/centos7:dev 是更改后创建的新镜像的名字， -a="duanyao" 指定作者的名字，-m="install c++ dev tools"指定提交消息。
注意，容器id每次运行时都会变化。

完成后，用 sudo docker images 查看，可看到多了一个镜像：

```
REPOSITORY           TAG                 IMAGE ID            CREATED             SIZE
duanyao/centos7      dev                 29da7c838702        5 minutes ago       526.7 MB
```

再次启动它：
sudo docker run -it -v /sys/fs/cgroup:/sys/fs/cgroup:ro -v $PWD:/project -w /project duanyao/centos7:dev /bin/bash

对运行中的容器执行 docker commit 也是可以的，但为了可靠起见，最好还是退出后执行。
可以在多次提交时使用完全相同的名字，这会覆盖之前的提交；但修改后的 image id 是会改变的。

### 远程下载/提交镜像

登录远程仓库：
```
docker login --username=成长轨迹教育 registry.cn-beijing.aliyuncs.com
```
会询问密码，通过后会将登录凭证存放在 `~/.docker/config.json` 文件中：

```
{
	"auths": {
		"registry.cn-beijing.aliyuncs.com": {
			"auth": "xxxx" # 登录凭证
		}
	},
    ...
}
```
以后 pull/push 就不会再询问登录密码了。


先用tag修改要提交的镜像名，指向要提交的镜像内容，然后用push提交：

```
docker tag duanyao/ai-centos8-v4:latest registry.cn-beijing.aliyuncs.com/aiparents/ai:v4
docker push registry.cn-beijing.aliyuncs.com/aiparents/ai:v4 
```

要提交的镜像名中必须包含可提交的仓库服务器。

### 删除镜像
```
sudo docker rmi <image_id>
sudo docker rmi -f <image_id>        # 忽略依赖此镜像的容器，但容器并不会被删除。
sudo docker rmi <image_tag>
```

删除镜像后，磁盘空间不一定会被立即释放。可以使用
```
docker system prune
```
来释放被删除镜像的磁盘空间。

如果有容器依赖于镜像，则镜像不能被删除，这时可以先删除容器：

```
sudo docker rm <container_id>
```

如果一个镜像id有多个标签，则也需要用 -f 才能删除。

删除悬空镜像（未配置任何 Tag （也就无法被引用）的镜像）。
```
docker system prune
```

### 当前目录 cwd

当前目录也是镜像的状态的一部分。如果启动时不用 `-w <dir>` 指定，则使用镜像规定的当前目录。
commit 的时候，主进程的最后的当前目录会被保存为镜像的当前目录。

### 导出导入镜像
https://www.digitalocean.com/community/questions/how-to-copy-a-docker-image-from-one-server-to-another-without-pushing-it-to-a-repository-first

https://docs.docker.com/engine/reference/commandline/save/

使用 save/load
```
docker save -o /home/sammy/your_image.tar your_image_name [your_image_name2 your_image_name3...]
# 或者
docker save your_image_name [your_image_name2 your_image_name3...] | gzip > your_image_name.tgz

# 或者保存全部镜像
docker save $(docker images --format '{{.Repository}}:{{.Tag}}') | gzip > allfilestoone.tgz

docker load -i your_image.tar
# 或者
docker load -i your_image_name.tgz
# 或者
docker load < your_image_name.tgz
```

使用 export/import
```
docker export -o your_image_name.tar CONTAINER_ID
# 或者
docker export CONTAINER_ID | gzip > your_image_name.tgz

docker import your_image_name.tar your_image_name:latest

# 或者
docker import your_image_name.tgz your_image_name:latest
```

将本地目录树变成镜像：

```
# - 表示从标准输入读
tar -c . | docker import - your_image_name:latest
```

save 用于导出镜像，镜像的各个层、修改历史、tag 等也都会被导出。
但依赖链上的镜像名不会自动被导出（尽管其数据已经被导出），如果需要依赖链上的镜像名，应全部显式列出。依赖链可以用 dockviz 工具查看。
load 用于导入 save 产生的镜像归档。多次导入同样的镜像并不是错误，也不会产生重复的镜像。
export 用于导出容器的文件系统，不包含层、修改历史、tag等，产生的tar文件就是简单的文件系统归档。注意，容器中挂载的卷里的文件不会被导出。
import 用于导入文件系统归档，产生一个新的镜像。

podman load 命令也可以导入 docker save 产生的镜像归档。

通过 ssh 远程导入镜像归档：

```
ssh usr@server cat /your_image_name.tgz | docker load
scp usr@server:/your_image_name.tgz /dev/stdout | docker load
curl sftp://usr@server/your_image_name.tgz | docker load
```
但远程导入对网络不稳定的容忍度不高，对于大型镜像，建议还是先下载归档文件，再本地导入。

## docker 的存储系统

### docker 的存储系统概述

docker 存储的数据按机制可以分两类：
* 分层存储。用于存储镜像的文件（只读），以及容器运行时创建/修改的文件。后者为本容器独占，且生命周期不超过本容器（随着容器销毁）。分层存储是为只读数据优化的，并使用层（layer）来减少存储容量，但读写性能往往并非最优。分层存储的实际位置是docker自动管理的，无法为每个容器单独指定。
* 数据卷（volumes）。用于存储容器运行时创建/修改的文件，以及与宿主机、其它机器、容器共享的文件。可以使用特殊存储介质（内存、网络），生命周期可以独立于本容器。可以直接暴露宿主机的文件系统（或者网络文件系统），可以获得原生的读写性能。

因此，对于性能、容量、生命周期、共享等有要求的可变数据，应使用数据卷来存储，否则两者皆可。

### 分层存储概述
[4.3]
注意此处所说的分层存储仅包括镜像和容器的存储方式，数据卷（volumes）是另一类相当不同的机制，将在单独描述。

分层存储的模式有多种（docker 称为 storage drivers），可以分两类：传统模式包括 overlayfs 和 btrfs 和 zfs，还有较新的 containerd-snapshotter 模式。目前默认采用传统模式，且根据当前的文件系统自动选择具体模式。

查看当前的镜像的存储模式：`docker info -f '{{ .DriverStatus }}'`，结果可能如下：
```
# ext4 文件系统，在其上构建 overlayfs 来存储镜像
[[Backing Filesystem extfs] [Supports d_type true] [Using metacopy false] [Native Overlay Diff true] [userxattr false]]

# btrfs 文件系统，利用其子卷功能来存储镜像
[[Btrfs ]]

# btrfs 文件系统，在其上构建 overlayfs 来存储镜像
[[Backing Filesystem btrfs] [Supports d_type true] [Using metacopy false] [Native Overlay Diff true] [userxattr false]]

# 启用 containerd-snapshotter 存储方式
[[driver-type io.containerd.snapshotter.v1]]
```

### 分层存储的位置
overlayfs 模式的默认存储位置是：`/var/lib/docker/overlay2/` ， btrfs 模式的默认存储位置是： `/var/lib/docker/image/btrfs/`  ，
containerd-snapshotter 模式的默认存储位置是：`/var/lib/containerd/io.containerd*` 。

查看某个镜像的存储目录：

```
docker inspect cis-hub-huabei-3.cmecloud.cn/docker46io/openpolicyagent47opa:0.63.0
        # overlayfs 的输出
        "GraphDriver": {
            "Data": {
                "LowerDir": "/var/lib/docker/overlay2/aecb8569572787058284c7043064d776246cbd9c99601769893a5e0da6d4d86f/diff",
                "MergedDir": "/var/lib/docker/overlay2/a159ac47c90167f2d8ae460526e9c75bfec1e8d96447f01d443e7b6cc8c358a4/merged",
                "UpperDir": "/var/lib/docker/overlay2/a159ac47c90167f2d8ae460526e9c75bfec1e8d96447f01d443e7b6cc8c358a4/diff",
                "WorkDir": "/var/lib/docker/overlay2/a159ac47c90167f2d8ae460526e9c75bfec1e8d96447f01d443e7b6cc8c358a4/work"
            },
            "Name": "overlay2"
        },
        
        # btrfs 的输出
        "GraphDriver": {
            "Data": null,
            "Name": "btrfs"
        },
```

containerd-snapshotter 模式的存储目录结构：
```
/var/lib/containerd/
  io.containerd.content.v1.content/ # 镜像相关数据
    ingest/
    blobs/
  io.containerd.snapshotter.v1.*/ # 镜像相关数据

/run/containerd
```

### btrfs 分层存储模式
[4.5]
btrfs 模式要求 docker 数据目录（默认 `/var/lib/docker/`）位于独立的 btrfs 格式的块设备上，而不是位于某个 btrfs 子卷上。否则，docker 仍将使用 overlayfs 模式。

### 切换分层存储模式
docker 的各种分层存储模式是对立的，数据是互相不可见的，一台宿主机上的 docker 服务只能同时运行在其中一种存储模式下。这时曾经在其它模式下创建的镜像、容器是不可见的，但切换存储模式后则可见。
要迁移镜像数据到其它存储模式，可以用 docker load|save 命令。

要切换 containerd-snapshotter 存储模式，可修改 /etc/docker/daemon.json 文件， `.features.containerd-snapshotter: true|false` 属性可以打开/关闭 containerd-snapshotter 存储方式（默认关闭）[4.6]。
```
{
  "features": {
    "containerd-snapshotter": true
  }
}
```
修改后要重启 docker 服务才能生效。

### 查看存储占用量

```
docker system df

TYPE            TOTAL     ACTIVE    SIZE      RECLAIMABLE
Images          21        10        18.86GB   12.54GB (66%)
Containers      11        9         8.153GB   124.8MB (1%)
Local Volumes   38        9         276.9GB   53.4MB (0%)
Build Cache     0         0         0B        0B
```

详细信息
```
docker system df -v
```

docker system df 对 Containers 可能不准，有些文件（如日志文件）的大小没有包括在内。建议用 du -d 2 -m /var/lib/docker/ 进一步确认。
例如， `/var/lib/docker/containers/<docker_id>/<docker_id>-json.log` 就可能变得很大（～6GB），但未被计入容器的大小。

### 数据卷（volumes）概述
docker 容器的数据卷（volumes）有多种类型
* 本地卷。最简单的一种卷，存储细节让 docker 来决定。实现为宿主机中普通目录，默认存储位置为 `/var/lib/docker/volumes` 。当容器停机时宿主机可以访问此文件系统，但运行时不建议访问。
* bind mount。即将宿主机文件或目录 bind mount 到容器中。容器和宿主机可以同时访问bind mount的目录，只要不存在冲突。
* NFS 卷。挂载一个网络文件系统。
* tmpfs。即 linux 的 tmpfs，以内存为存储介质，具有高性能，但数据可能丢失。

### 管理本地卷

```
docker volume ls
docker volume inspect my-vol
```
在 docker-comose 中，可以这样使用本地卷：

```
services:
  frontend:
    image: node:lts
    volumes:
      - myapp:/home/node/app # 挂载到容器内 /home/node/app 目录
volumes:
  myapp: # 新建一个本地卷
```

用已经存在的本地卷：

```
services:
  frontend:
    image: node:lts
    volumes:
      - myapp:/home/node/app
volumes:
  myapp:
    external: true # 用已经存在的本地卷
```

删除卷：

```
docker volume rm [OPTIONS] VOLUME [VOLUME...]

Options:
  -f, --force   Force the removal of one or more volumes
```

删除未使用卷（当前没有被任何容器使用的卷，注意里面仍可能有有价值的数据）：

```
docker volume prune
```

### 数据卷的自动数据复制功能
如果卷的挂载点不是个空目录，且被挂载的卷不是匿名卷（例如 `docker run -v local_dir:mount_point` 命令中被挂载的是匿名卷），则 docker 会自动将挂载点中的数据复制到卷中。
要禁用此功能，可以在 docker-compose 中设置 `volumes.nocopy: true` 。例如：
```
services:
  cvat_server:
    volumes:
      - type: volume
        source: cvat_data_data_nfs
        target: /home/django/data/data
        volume:
          nocopy: true
volumes:
  cvat_data_data_nfs:
    driver_opts:
      type: "nfs"
      o: "addr=1f49a4a93b-gyk72.cn-beijing.nas.aliyuncs.com,nolock,soft,rw"
      device: ":/cvat-1/volumes/cvat-1_cvat_data/_data/data/"
```

### volumes-from 和数据备份

```
docker run --volumes-from container1 image2 command
```
作用是将容器 container1 挂载的卷再次挂载到新启动的容器中，这样新的容器就可以看到容器 container1 的卷了。这可以用来备份/恢复容器的数据：

```
docker run -v /dbdata --name dbstore ubuntu /bin/bash
docker run --rm --volumes-from dbstore -v $(pwd):/backup ubuntu tar cvf /backup/backup.tar /dbdata
docker run --rm --volumes-from dbstore -v $(pwd):/backup ubuntu bash -c "cd /dbdata && tar xvf /backup/backup.tar --strip 1"
```

### 限制日志文件的大小，清理日志文件
[8.1] 

## 迁移 docker 数据目录

目录 /var/lib/docker 
[8.1]

1. 方法1：
配置文件为（此文件可能不存在）：
/etc/systemd/system/docker.service.d/devicemapper.conf

2. 方法2：
或者修改
/lib/systemd/system/docker.service

将 `ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock` 改为

`ExecStart=/usr/bin/dockerd -g /home/docker -H fd:// --containerd=/run/containerd/containerd.sock`

3. 方法3：
修改 /etc/docker/daemon.json，修改 "data-root" 属性：
{
  "data-root": "/media/data3/docker_root"
}

迁移 docker 的数据文件/目录时需要保留原来的权限和属性，否则 docker 容器运行很容易出问题。例如，如果镜像里 /tmp 目录的权限如果不是 drwxrwxrwt ，则普通用户将无法使用此目录。

docker compose -f /home/rd/project/cvat-v1.7.0/cvat17/docker-compose.yml restart
[+] Running 3/5
 ⠼ Container cvat        Restarting                                                                                                                                                                                          1.4s 
 ✔ Container traefik     Started                                                                                                                                                                                             1.4s 
 ⠼ Container cvat_db     Restarting                                                                                                                                                                                          1.4s 
 ✔ Container cvat_ui     Started                                                                                                                                                                                             1.4s 
 ✔ Container cvat_redis  Started                                                                                                                                                                                             1.1s 
Error response from daemon: Cannot restart container 28e90ee607fabde90bf0299cd92dc3d83c55b25cd7142252f5bb96a3ae09497e: error evaluating symlinks from mount source "/var/lib/docker/volumes/cvat17_cvat_logs/_data": lstat /var/lib/docker: no such file or directory

## docker-compose

docker-compose 有两个版本，V1 和 V2，前者是个单独的命令，docker-compose；后者是 docker 命令的一部分，即 docker compose。V2 大致上向后兼容 V1。到 2023.7，V1将不再支持。 

### docker-compose V1
从 [7.1] 下载 docker-compose 可执行程序到本地，改名为 docker-compose，加上可执行权限即可。

docker-compose up 和 docker-compose down 等命令都要求指定配置文件，或者默认配置文件位于当前目录下（docker-compose.yml），否则无法执行。

目前，docker-compose 无法告诉用户现在运行的 docker 容器是使用哪个（些）配置文件启动的，所以如果没记住配置文件的路径，就无法可靠地停止 docker-compose 启动的容器。
这是 docker-compose 的一个缺陷。

配置文件中使用 `restart: always` 可以让 docker-compose 启动的容器随着系统自动启动，也能在意外退出后自动重启，直到运行 docker-compose down 之后 [7.3]。注意，用 docker stop 停止的容器不会被自动重启，即使有 `restart: always`。

例子：

```
docker-compose -f docker-compose.yml -f components/serverless/docker-compose.serverless.yml up -d
docker-compose -f docker-compose.yml -f components/serverless/docker-compose.serverless.yml ps
docker-compose -f docker-compose.yml -f components/serverless/docker-compose.serverless.yml down
# 重启个别容器
docker-compose -f docker-compose.yml -f components/serverless/docker-compose.serverless.yml -f docker-compose.https.yml restart traefik
```

docker-compose restart 不会应用对配置文件（yml）的修改，尽管需要在命令行中指定配置文件[7.4]。 

-f 参数可以指定一个或多个配置文件，这些文件会被组合起来生效；如果多个配置文件中有相同的属性，则后面的配置文件的属性生效。

### docker compose V2
`docker compose ls` 显示当前运行的服务，例如

```
NAME                STATUS              CONFIG FILES
cvat-1              running(6)          docker-compose.yml,components/serverless/docker-compose.serverless.yml,docker-compose.https.yml
```

`docker compose -f <config_file> restart [SERVICE...]` 重启指定服务或所有服务。例如：`docker compose -f /home/rd/project/cvat-v1.7.0/cvat17/docker-compose.yml restart`。

### docker-compose 的自定义存储位置的本地卷
以cvat为例，新建一个自定义存储位置的本地卷的配置文件：

/home/rd/project/cvat.git/docker-compose.custom-storage.yml：
```
version: '3.3'

services:
  cvat:
    volumes:
      - /home/rd/project/cvat.git:/home/django/src
      - data2:/home/django/data2

volumes:
  data2:
    driver_opts:
      type: none
      device: /media/data2
      o: bind
```

在 /home/rd/project/cvat.git 下启动 cvat，追加自定义卷：
```
docker-compose -f docker-compose.yml -f docker-compose.custom-storage.yml up
```

检查自定义卷是否创建了：
```
docker volume ls
DRIVER    VOLUME NAME
local     cvatgit_cvat_data
local     cvatgit_cvat_db
local     cvatgit_cvat_keys
local     cvatgit_cvat_logs
local     cvatgit_data2

docker volume inspect cvatgit_data2
[
    {
        "CreatedAt": "2022-11-23T17:27:03+08:00",
        "Driver": "local",
        "Labels": {
            "com.docker.compose.project": "cvatgit",
            "com.docker.compose.version": "1.25.0",
            "com.docker.compose.volume": "data2"
        },
        "Mountpoint": "/var/lib/docker/volumes/cvatgit_data2/_data",
        "Name": "cvatgit_data2",
        "Options": {
            "device": "/media/data2",
            "o": "bind",
            "type": "none"
        },
        "Scope": "local"
    }
]

mount | grep cvat
/dev/sda1 on /var/lib/docker/volumes/cvatgit_data2/_data type ext4 (rw,relatime)

docker inspect cvat

            "Volumes": {
                "/home/django/data": {},
                "/home/django/data2": {},
                "/home/django/keys": {},
                "/home/django/logs": {},
                "/home/django/src": {}
            },
```

可见，docker-compose 将 /media/data2 mount-bind 到 /var/lib/docker/volumes/cvatgit_data2/_data 上，然后再挂载到 docker 容器里。

## 镜像的定义

镜像里包含的内容以及行为是通过一个名为 "Dockerfile" 的文件定义的，其格式是 docker 项目自定义的[2.1]。例子[3.1]：

```
FROM ubuntu:latest
MAINTAINER John Fink <john.fink@gmail.com>
RUN apt-get update # Fri Oct 24 13:09:23 EDT 2014
RUN apt-get -y upgrade
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install mysql-client mysql-server apache2 libapache2-mod-php5 pwgen python-setuptools vim-tiny php5-mysql  php5-ldap
RUN easy_install supervisor
ADD ./scripts/start.sh /start.sh
ADD ./scripts/foreground.sh /etc/apache2/foreground.sh
ADD ./configs/supervisord.conf /etc/supervisord.conf
ADD ./configs/000-default.conf /etc/apache2/sites-available/000-default.conf
RUN rm -rf /var/www/
ADD https://wordpress.org/latest.tar.gz /wordpress.tar.gz
RUN tar xvzf /wordpress.tar.gz 
RUN mv /wordpress /var/www/
RUN chown -R www-data:www-data /var/www/
RUN chmod 755 /start.sh
RUN chmod 755 /etc/apache2/foreground.sh
RUN mkdir /var/log/supervisor/
EXPOSE 80
CMD ["/bin/bash", "/start.sh"]
USER 1000:1000
WORKDIR /home
```

每一行是一条指令，第一个词是 Dockerfile 的命令[2.1]。

* FROM 指定基础镜像。空的基础镜像名为 scratch 。

* RUN

  在容器里执行一个命令行程序，例如用 apt/yum 安装软件。上面的例子是普通形式的参数，命令行的写法与 unix shell 语法相同。
  还可以用JSON数组形式对的参数，即 `RUN ["executable", "param1", "param2"]` ，这种形式允许参数中有空格，并且不会通过 shell 解释。 
  因为是 JSON 格式，因此里面的字符串只能用双引号，且特殊字符要按 JSON 规则转义。
  如果要执行多个命令，可以用 '&&' 连接。
  如果命令返回了非0值，则 RUN 步骤失败，构建中止。但应该注意到，有些命令在出现错误/警告的情况下仍然返回0值，RUN 步骤可能带着错误进入下一步，所以应注意命令的实际执行结果。
  必要时，应在构建时使用 `--progress plain` 参数，避免 docker build 隐藏 RUN 步骤的输出。

* ADD

  用来从本地复制文件和目录到容器里。如果有多于2个参数，则最后一个是容器里的路径，其余是本地的路径。与 RUN 同样，参数也可以写成 JSON 数组的形式。
  可以用通配符，例如`ADD hom*.txt /mydir/`。
  可以修改用户和组，例如 `ADD --chown=10:11 files* /somedir/` ，字符串形式的用户和组要在容器的 `/etc/passwd /etc/group` 文件里查找。
  
* COPY
  形式：
  ```
  COPY [--chown=<user>:<group>] [--chmod=<perms>] <src>... <dest>
  COPY [--chown=<user>:<group>] [--chmod=<perms>] ["<src>",... "<dest>"]
  ```
  例子：
  ```
  COPY --chown=55:mygroup files* /somedir/
  ```
  当  <src> 是目录时，其内容被复制，其本身不被复制。
  如果 <dest> 是目录，应该写成 '/' 结尾的形式，否则会被理解为普通文件。<dest> 及其上级目录不需要已经存在，会被自动创建。
  可以用 --from=xxx 来引用 `docker build --build-context xxx=yyy` 的 build context。
  可以用 --link 参数来让 COPY 指令不依赖于前面的层：
  ```
  FROM alpine
  COPY --link /foo /bar
  ```
  也就是说，如果 `COPY --link` 之前的层发生了变化，也不会导致 `COPY --link` 层重建。
  COPY 与 ADD 的区别：ADD 

* EXPOSE <port> [<port>/<protocol>...]

  开放的端口。仅起到文档的作用，真正开放端口要通过 `docker run -p` 参数。
  也可以指定一个范围：`EXPOSE 2000-3000`

* ENV
  环境变量。有多个变量时，可以写到一行或者多行。
  ```
  ENV MY_NAME="John Doe" MY_CAT=fluffy
  ENV MY_NAME="John Doe"
  ENV MY_CAT=fluffy
  ```

* CMD
  
  容器启动时默认执行的命令（及其参数）。
  CMD 的参数格式与 RUN 相同。
  一般来说，有 CMD 指令的镜像，运行时（docker run）不需要再指定要运行的命令及其参数；如果指定了，则会覆盖 CMD 的定义。

* ENTRYPOINT

  指定容器启动时执行的程序。
  ENTRYPOINT 的参数格式与 RUN 相同。
  一般来说，ENTRYPOINT 仅指定可执行程序，不指定参数，参数在执行 docker run 命令时传入。例如，当 ENTRYPOINT 是 `ls` ，`docker run image_name -la` 等效于执行了 `ls -la` 命令。
  除了用途上的差异，CMD 和 ENTRYPOINT 差别不大。一个 Dockerfile 至少要指定 CMD 或 ENTRYPOINT 之一。
  在运行时，可以用 `docker run --entrypoint <new_command>` 覆盖此参数；如果 new_command 是空串（""），则相当于取消 entrypoint，让后面的命令行部分生效。
  例如，当 ENTRYPOINT 是 `ls` ，`docker run --entrypoint "" image_name find /etc` 等效于执行了 `find /etc` 命令。

* USER
  指定构建和执行容器时的默认的用户。在构建期，影响此指令后的 RUN 指令；在执行期，影响 CMD, ENTRYPOINT 等指令。可以用数字id或名字的形式，例如 `USER 0:0` 或 `USER root:root`。
  目前 docker 无法单独指定构建和执行期间的用户，也无法为 RUN, CMD, ENTRYPOINT 等指令单独指定用户。所以，在多 dockerfile 文件构建时，最好在文件开头指定构建期用户，在结尾处指定在执行期用户。

要了解一个已有的镜像是如何定义的，可以在 hub.docker.com 上搜索查看对应的 Dockerfile 。

### build context
docker build 命令可以传入多个 --build-context 参数。
```
docker build --build-context mark-tool=../ --build-context model_clip=/media/data2/ai-model/CLIP --build-context dataset_tpc_t1=/tmp/teacher_pose_cls_test_1/ -t local/clip_cls_bench:1.0 -f clip_cls_bench.dockerfile .
```
然后在 dockerfile 中引用它们：
```
COPY --link --from=mark-tool .  /service/mark-tool/
COPY --link --from=model_clip .  /ai-model/CLIP/
COPY --link --from=dataset_tpc_t1 . /ai-dataset/teacher_pose_cls_test_1/
```

这些目录会在执行 dockerfile 前被传送到构建服务中。要注意的事项：
* build context 应该尽量不包含无关的大文件，否则传输耗时会很长。
* build context 中的符号链接会照原样传输，没有选项可以跟随链接再传输。所以如果其中有符号链接指向 build context 之外，会得到坏链接。
  解决办法是，先将 build context 以跟随链接的方式复制到临时目录中（如 `rsync -rtL src/ /tmp/dest/`），然后以此临时目录作为 build context，构建完镜像后删除。

## 时区设置
很多镜像的默认时区是 UTC，而systemd在容器中又不能用，连带 timedatectl 也不能用，所以要这样更改时区：

一般的 linux：
安装软件包 tzdata，如果没装。然后执行：

```
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime 
echo "Asia/Shanghai" > /etc/timezone
```

在 debian 系中：
```
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
dpkg-reconfigure -f noninteractive tzdata
```
效果似乎是一样的，dpkg-reconfigure 会帮我们修改 `/etc/timezone`。

UTC 时区的文件名是 `/usr/share/zoneinfo/Etc/UTC`。

## 从头创建基础镜像
### debian/ubuntu
参考 [8.11-8.12] 以及 apt_deb.md。
```
# 最新的 ubuntu 的 debootstrap & distro-info-data
wget https://mirrors.tuna.tsinghua.edu.cn/ubuntu/pool/main/d/distro-info-data/distro-info-data_0.62_all.deb
# 如果 distro-info-data 依赖更新版本的 distro-info，则 --ignore-depends 忽略它：
sudo dpkg --ignore-depends=distro-info -i distro-info-data_0.62_all.deb
wget https://mirrors.tuna.tsinghua.edu.cn/ubuntu/pool/main/d/debootstrap/debootstrap_1.0.136ubuntu1_all.deb
sudo dpkg --ignore-depends=distro-info -i debootstrap_1.0.136ubuntu1_all.deb

mkdir custom-sys-image
cd custom-sys-image

sudo su # fakeroot 不适用 debootstrap

# 制作 debian 12 (代号 bookworm)
export MY_CHROOT=${PWD}/debian12
export MY_CACHE=${PWD}/cache-debian
mkdir -p $MY_CHROOT $MY_CACHE
debootstrap --arch amd64 --cache-dir=${MY_CACHE} bookworm $MY_CHROOT https://mirrors.tuna.tsinghua.edu.cn/debian/ # http://deb.debian.org/debian/

# 制作 ubuntu 24.04 (代号 noble)
export MY_CHROOT=${PWD}/ub2404-image
export MY_CACHE=${PWD}/cache-ubuntu
mkdir -p $MY_CHROOT $MY_CACHE
debootstrap --arch=amd64 --cache-dir=${MY_CACHE} noble $MY_CHROOT https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ # http://archive.ubuntu.com/ubuntu/

mount sysfs $MY_CHROOT/sys -t sysfs
chroot $MY_CHROOT /bin/bash

# 增加仓库。默认的只有 "deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu noble main"
echo "deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu noble main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu noble-updates main restricted universe multiverse" > /etc/apt/sources.list
echo "deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu jammy main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu jammy-updates main restricted universe multiverse" > /etc/apt/sources.list.d/ubuntu2204.list
echo "deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu focal main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu focal-updates main restricted universe multiverse" > /etc/apt/sources.list.d/ubuntu2004.list

apt update
apt install --no-install-recommends curl wget

# 清除 apt 缓存
rm -Rf /var/cache/apt/* /var/lib/apt/lists/*

# 退出 chroot
exit
umount $MY_CHROOT/sys

# 生成 docker 镜像
sudo tar -C ${MY_CHROOT} -c . | docker import - local/ubuntu2404:2406
```



## 挂载 NFS

### docker
[10.1]
```
sudo apt install nfs-common
```

```
docker volume create --driver local \
  --opt type=nfs \
  --opt o=addr=[ip-address],rw \
  --opt device=:[path-to-directory] \
  [volume-name]
```

```
docker volume ls

docker volume inspect [volume-name]
```

```
docker run -d -it \
  --name [container-name] \
  --mount source=[volume-name],target=[mount-point]\
  [image-name]
```

### docker compose
[10.1]
```
version: "3.2"

services:
  [service-name]:
    image: [docker-image]
    ports:
      - "[port]:[port]"

    volumes:
      - type: volume
        source: [volume-name]
        target: /nfs
        volume:
          nocopy: true
volumes:
  [volume-name]:
    driver_opts:
      type: "nfs"
      o: "addr=[ip-address],nolock,soft,rw"
      device: ":[path-to-directory]"
```

或者 [10.2]:
```
version: "3"

services:
  jellyfin:
    image: jellyfin/jellyfin:latest
    container_name: jellyfin
    restart: unless-stopped
    ports:
      - 8096:8096
    volumes:
      - ./config:/config
      - ./cache:/cache
      - videos:/mnt/videos

volumes:
  videos:
    driver_opts:
      type: "nfs"
      o: "addr=192.168.1.4,nolock,ro,soft,nfsvers=4"
      device: ":/path/to/video-dir" # nfs 服务器上的路径，注意以 : 开头
```

## 网络

### 配置 docker 服务（daemon）使用 http 代理
此配置影响 docker pull/push 等功能。

方法1：修改 `/etc/docker/daemon.json`：
```
{
  "proxies": {
    "http-proxy": "http://proxy.example.com:3128",
    "https-proxy": "https://proxy.example.com:3129",
    "no-proxy": "*.test.example.com,.example.org,127.0.0.0/8"
  }
}
```
修改后重启 dockerd：
```
sudo systemctl restart docker
```
方法2：在 systemd 中配置 dockerd 的环境变量，例如 /etc/systemd/system/docker.service.d/docker-http-proxy.conf 
```
[Service]
Environment="HTTP_PROXY=http://proxy.example.com:3128"
Environment="HTTPS_PROXY=https://proxy.example.com:3129"
Environment="NO_PROXY=localhost,127.0.0.1,docker-registry.example.com,.corp"
```
文件名 `docker-http-proxy.conf` 可以随便起，但目录名是固定的。如果是采用 rootless mode，则目录名为 `~/.config/systemd/<user>/docker.service.d/` 。

修改后重启 dockerd：
```
sudo systemctl daemon-reload
sudo systemctl show --property=Environment docker # 验证环境变量已修改
sudo systemctl restart docker
```

### 容器的虚拟网络

在 linux 上，docker 会建立一个 docker0 的网卡，其默认地址是 172.17.0.1/16，容器可以通过 172.17.0.1 来访问主机。
docker 还会创建一个 br-xxxx 的网卡，地址是 172.nn.0.1/16 ，xxxx 和 nn 是随机的。各个容器的 IP 地址也会在 172.nn.0.1/16 范围内。
这可以通过 ifconfig 和 route -n 来确认。

但是，如果主机上有其它网卡的地址已经在 172.17.0.0/16 范围内，则 docker 会为 docker0 选择其它地址，如 172.18.0.1/16 。阿里云 ECS 的默认 IP 就在这个范围内。
这有时候会引起问题，因为有些应用写死了 172.17.0.1 的地址。这时候可以尝试更改其它网卡的地址和 docker0 的地址，直接用 ifconfig 改 docker0 的地址为 172.17.0.1 即可。

查看一个容器的 IP 地址：
```
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' <contrainer_id_or_name>
```

要查看所有容器的IP地址，可以：

```
docker inspect --format='{{.Name}} - {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps -aq)

/nuclio-local-storage-reader - 172.17.0.2
/pedantic_cori - 
/cvat_ui - 172.28.0.7
/cvat - 172.28.0.6
/traefik - 172.28.0.2
/cvat_db - 172.28.0.5
/nuclio - 172.28.0.4
/cvat_redis - 172.28.0.3
/amazing_swirles - 
/nuclio-nuclio-openvino-dextr - 
/nuclio-nuclio-openvino-omz-intel-person-reidentification-retail-0300 - 
/nuclio-nuclio-openvino-omz-public-faster_rcnn_inception_v2_coco -
```

要查看所有的 docker 网络（子网），可以先列出网络名：
```
docker network ls

NETWORK ID     NAME          DRIVER    SCOPE
dfe4196e5337   bridge        bridge    local
2e296a866118   cvat-1_cvat   bridge    local
ccbda7dfdf86   host          host      local
4e658f65e6c0   none          null      local
```
可以用：

```
docker network inspect bridge
docker network inspect cvat-1_cvat
```
进一步查看网络（子网）的特性。

### 服务端口映射

容器内的服务监听的端口可以被映射到宿主机上，例如：
```
docker run -p 8888:80  nginx:latest

docker run -P nginx:latest
```
-p 的全称是 --expose ，注意不要与 Dockerfile 中的 EXPOSE 混淆，后者并不真正映射端口。
表示此容器内的服务监听80端口，并且被映射到宿主机的8888端口，这样外界可以通过宿主机的8888端口访问容器内的服务。

具体的指定端口映射的格式[1.4]：

```
参数      说明

-p hostPort:containerPort    端口映射  -p 8080:80

-p ip/hostPort:containerPort 配置监听地址 -p 10.0.0.100/8080:80

-p ip::containerPort      随机分配端口 -p 10.0.0.100::80

-p hostPort:containerPort/udp          指定协议 -p 8080:80/udp

-p 81:80 –p 443:443          指定多个
```

### 抓包
如果容器映射了端口到宿主机，则可以用 tcpdump 在宿主机的映射的端口上抓包。
如果没有端口映射，则宿主机上似乎抓不到容器的包；这种情况下可以在容器内安装 tcpdump，在容器内抓包。

## 系统清理

```
docker system prune

WARNING! This will remove:
  - all stopped containers
  - all networks not used by at least one container
  - all dangling images
  - all dangling build cache

Are you sure you want to continue? [y/N] y
Deleted Networks:
cvatgit_cvat

Total reclaimed space: 0B
```

## 定时任务/cron
一个办法是，在 docker 中运行 cron。
How do we set up cron in a Docker container
https://techsparx.com/software-development/docker/damp/docker-cron.html

另一个办法是，利用 k8s 的 CronJob 机制，详见 k8s 的相关章节。

## 服务管理

### Systemd 和 Docker
根据 [15.1]，systemd 和 docker 还不能很好地一起工作。Docker 容器内的服务通常不能以 systemd 启动，导致很多镜像作者自行编写启动脚本。
根据 [15.2]，到 2016.9，systemd 可以和 docker 一起工作，容器不必以特权模式运行，但 journald 日志仍然无法输出到 host 中。

根据[15.3]，到 2019，systemd 和 docker 的配合度仍然不算好，但 docker 的替代品 podman 则与 systemd 配合良好。

根据[15.4]，到 2018，可以用以下方式在 docker 中运行 systemd：

```
docker run \
  --entrypoint=/usr/lib/systemd/systemd \
  --env container=docker \
  --mount type=bind,source=/sys/fs/cgroup,target=/sys/fs/cgroup \
  --mount type=bind,source=/sys/fs/fuse,target=/sys/fs/fuse \
  --mount type=tmpfs,destination=/tmp \
  --mount type=tmpfs,destination=/run \
  --mount type=tmpfs,destination=/run/lock \
    archlinux/base --log-level=info --unit=sysinit.target
```


* provide a container= variable, so systemd won't try to do number of things it usually does booting a hardware machine
* systemd actively uses cgroups, so bind mount /sys/fs/cgroup file system from a host
* bind mounting /sys/fs/fuse is not required but helps to avoid issues with fuse-dependent software
* systemd thinks that using tmpfs everywhere is a good approach, but running unprivileged makes it impossible for it to mount tmpfs where ever it wants, so pre-mount tmpfs to /tmp, /run and /run/lock
* as the last bit you need to specify sysinit.target as default unit to boot instead of multi-user.target or whatever, as you really do not want to start graphical things inside a container

根据 [15.5]，systemd 做出了很多修改，来适应在任意容器中运行，目前（2023）应该可以在任意符合要求的容器中运行。

2021年，[15.6-15.7] 给出了用 Dockerfile 定义的运行 systemd 的 debian 11 镜像。

### supervisor
supervisor 可以 在docker 容器里和运行，详见 supervisor.md 。
但它缺少 systemd 的一些特性，如定时任务，解决办法详见”定时任务一节“。

## 访问 shell
docker exec -it <实例名> bash

## 日志系统

### 查看日志
```
docker logs <实例名>
docker logs -f --tail=1 <实例名>
```

这输出的是容器主进程的标准输出。--tail=1 表示回溯一行。-f 表示跟随，即实时显示当前的日志。

默认情况下，

### 日志的大小限制和周转（log rotation）
docker 容器的日志有多种后端（称为 log driver）。其中默认的日志后端是 "json-file"，即每一行是一个json对象，默认的存储位置是 `/var/lib/docker/containers/<container_id>/<container_id>-json.log`。
默认情况下，docker 容器的日志无大小限制，也没有周转，会一直增大，这可能导致磁盘空间耗尽。而且，容器日志的大小不会计入 docker system df 的统计。

因此，应该设置日志的大小限制和周转。全局设置在 `/etc/docker/daemon.json` [12.1] :

```
{
    "log-opts": {
        "max-size": "8m", // 单个日志文件最大 8MB
        "max-file": "5"  // 为每个容器最多存储 5 个日志文件。
    }
}
```
修改 daemon.json 只对新建的容器起作用，不影响已存在的容器。 

也可以在容器启动时单独指定其日志的大小限制和周转：

```
docker run --name app \
    --log-driver json-file \
    --log-opts max-size=8M \
    --log-opts max-file=5 \
    app-image:latest
```

### 其它日志后端
见 [12.2]，包括 journald、syslog 等。

## 查看非标准日志文件
```
docker exec -it <实例名> tail -f <path_to_log>
```

tail -f 是将文件的尾部连续输出，适用于监视日志。

## docker build, Dockerfile, build cache 容器的构建

### build cache

可以在运行 RUN 的时候指定 cache 目录，用来缓存从网络上下载的文件。运行 npm、pip、apt 等软件包管理器的时候都可以运用这个技巧：
```
ARG HOME=/tmp/build

USER 1000:1000

# npm install 至少使用了 $HOME/.npm 和 $HOME/.config
RUN \
    --mount=type=cache,target=$HOME/,uid=1000,gid=1000 \
    cd /service/aiparents_ai/ && \
    npm --registry https://registry.npmmirror.com install && \
```
其中 `USER 1000:1000` 使得后面的 docker 命令（无论构建时还是运行时）都以 uid=1000,gid=1000 的身份运行。`RUN --mount=...,uid=1000,gid=1000` 使得挂载的缓存目录的所有者为 `uid=1000,gid=1000` 。
如果是以 root 身份执行 RUN，则可省略 `USER 1000:1000` 和 `uid=1000,gid=1000` 等部分。

### 清理缓存 build cache

```
docker builder prune # 清理未被引用的 build cache
docker builder prune --all # 清理所有未使用的 build cache
```
清理 build cache 将导致后续的 docker build 重新执行相关的阶段。

## 调试

容器化环境里如何方便的进行debug和测试？
https://www.zhihu.com/question/333652815

如何调试 Docker
https://docker-practice.github.io/zh-cn/appendix/debug.html

### ptrace
ptrace 是个系统调用，strace 和 gdb attach 等工具都用到它。默认状态下 docker 容器内不允许使用 ptrace，要启用：
```
docker run --cap-add=SYS_PTRACE  ...
```

### 在宿主机中调试
在宿主机中运行调试器，附加到容器中的进程是可行的。

## 故障

### docker pull 失败 commit failed: unexpected commit digest
docker pull 的输出：
```
failed commit on ref "layer-sha256:cb75bdb8950f47703515dbc2a082a45b1860e238d8b1ed10847f8253b084f462": commit failed: unexpected commit digest sha256:f93a938c6614bdc551eb41a1072eedc3b8d74c86b6f9ca998697bfe0d7c7dfda, expected sha256:cb75bdb8950f47703515dbc2a082a45b1860e238d8b1ed10847f8253b084f462: failed precondition
```
这实际上是 containerd 的失败。查看 containerd 的日志（journalctl -u containerd ）可以看到
```
level=error msg="(*service).Write failed" error="rpc error: code = FailedPrecondition desc = unexpected commit digest
```
重启 containerd 或 docker 服务都无法解决。删除 containerd 的镜像缓存 ( ingest ) 目录可以解决：
```
sudo su
rm -Rf /var/lib/containerd/io.containerd.content.v1.content/ingest/*
```
注意，containerd 的数据目录的位置是不固定的，其他基于 containerd 的容器工具（k3s）的目录位置可能不同。
