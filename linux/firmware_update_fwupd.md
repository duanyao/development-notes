## 参考资料

[1.1] How can I upgrade my device firmware from the command line?
https://askubuntu.com/questions/1394105/how-can-i-upgrade-my-device-firmware-from-the-command-line

[1.2] How to Update Firmware on Ubuntu using fwupd
https://linuxopsys.com/topics/update-firmware-on-ubuntu-using-fwupd

[1.3] https://fwupd.org/lvfs/docs/users

[1.4] How to Update Firmware on Ubuntu and Other Linux Distributions
https://itsfoss.com/update-firmware-ubuntu/

## GNOME Software 的图形界面固件更新
[1.3]
ubuntu 23.10 会自动检查固件更新，在图形界面弹出提示来更新。但目前存在一些缺点：下载过程没有进度指示，安装失败缺乏错误提示。

## fwupd 命令行界面固件更新
[1.1-1.4]
```
sudo apt install fwupd # ubuntu 23.10 是预装的。

sudo fwupdmgr refresh # 更新元数据。[1.2, 1.4]

fwupdmgr get-devices # 显示本机需要固件的设备。
fwupdmgr get-updates # 显示可用的更新
fwupdmgr update  # 更新固件
```
fwupdmgr 的优点是显示下载进度和预计时间，安装失败有明确提示。缺点是如果下载或安装失败后，重试要从头开始下载（下载很慢）。

fwupd 的固件更新来自 https://fwupd.org/ （Linux Vendor Firmware Service）。这个系统在大部分发行版中可用。
