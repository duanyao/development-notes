## 参考资料

[1] where are the default ulimit values set? (linux, centos)
https://serverfault.com/questions/356962/where-are-the-default-ulimit-values-set-linux-centos

[2] Node.js, "too many open files", and ulimit
https://www.imakewebsites.ca/posts/2018/03/06/node.js-too-many-open-files-and-ulimit/

[3] ubuntu 修改 ulimit
https://blog.csdn.net/u010953692/article/details/123287657

## 资源限制概述

limits 是 linux 进程的一组属性，用来限制一个进程可以使用的资源总量。
通过 /proc/<pid>/limits 可以查看，例如：

```
cat /proc/1/limits
Limit                     Soft Limit           Hard Limit           Units     
Max cpu time              unlimited            unlimited            seconds   
Max file size             unlimited            unlimited            bytes     
Max data size             unlimited            unlimited            bytes     
Max stack size            8388608              unlimited            bytes     
Max core file size        0                    unlimited            bytes     
Max resident set          unlimited            unlimited            bytes     
Max processes             31216                31216                processes 
Max open files            65536                65536                files     
Max locked memory         65536                65536                bytes     
Max address space         unlimited            unlimited            bytes     
Max file locks            unlimited            unlimited            locks     
Max pending signals       31216                31216                signals   
Max msgqueue size         819200               819200               bytes     
Max nice priority         0                    0                    
Max realtime priority     0                    0                    
Max realtime timeout      unlimited            unlimited            us
```

limits 的数值可以来自多个来源[1]：

* 来自 linux 内核。内核通常值设置 pid 1（即 init 进程，系统的第一个进程）的 limits，。
* 来自 systemd 。systemd 负责设置它管理的服务的 limits，详见 systemd 的文档。
* 来自继承。子进程默认继承父进程的 limits 。
* 调用 setrlimit(2) 来修改自身的 limits。向上修改需要特权，请参考 man 。
* 调用 prlimit(1) 来修改其它进程的 limits 。需要特权。
* 来自 PAM ，用户登录时被设置，配置文件 /etc/security/limits.conf 。注意，这不影响 systemd 和 cron 等不依赖登录用户的进程。

## ulimit 命令

```
ulimit -n
ulimit -n 100000
```
注意 ulimit 的设置仅对当前shell及其子进程有效，且不会持久。

可以在 `~/.bashrc` 中设置这个命令，则可确保本用户新启动的 bash 自动设置新的 ulimit ，但这对 GUI 程序无效。

## prlimit 命令
```
prlimit [options] [--resource[=limits] [--pid PID]

prlimit --pid 13134
   Display limit values for all current resources.

prlimit --pid 13134 --rss --nofile=1024:4095
              Display the limits of the RSS, and set the soft and hard limits for the number of open files to 1024
              and 4095, respectively. --nofile 不能简写成 -n 。
```
不带参数时，显示本进程（等效于当前shell）的限制。

## /etc/security/limits.conf
limits.conf 中不一定有明确配置，如果没有，会采用某些默认值。如果要设置 nofile ，可以这样写：
```
*        soft       nofile    530000
*        hard       nofile    530000
```
`*` 表示对所有用户生效。

limits.conf 的设置只对部分进程有影响，一般来说只影响登录的用户的会话下的进程，包括文本控制台、图形界面（图形界面下的终端和shell）、ssh远程执行的进程。
但不影响系统服务（如systemd及其子进程）。

/etc/security/limits.conf 修改后，对新的文本控制台、ssh登录会话立即起作用，但对已有的 GUI 会话不起作用，GUI下新开的终端也仍然使用旧的值。如果需要对GUI会话生效，得注销再重新登录；如果不想注销，可在 `~/.bashrc` 中追加 `ulimit -n xxxxx`，这将对新开的GUI终端生效。 

