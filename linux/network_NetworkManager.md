## 参考资料

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_networking/managing-the-default-gateway-setting_configuring-and-managing-networking

## 图形前端

### network-manager-gnome 

网络连接管理，包括 nm-applet （托盘区小程序）、nm-connection-editor （网络连接编辑器）

### nm-tray

LXQt 的nm前端。

## 命令行前端

### nmcli

修改默认网关：

nmcli connection modify example ipv4.gateway "192.0.2.1"
nmcli connection modify example ipv6.gateway "2001:db8:1::1"

交互模式：

nmcli connection edit example

显示所有网口的配置：
nmcli device show
