## 参考资料
[2.1] https://www.baeldung.com/linux/monitor-process-thread-count

[2.2] https://www.golinuxcloud.com/check-threads-per-process-count-processes/

[2.3] Is there a way to see details of all the threads that a process has in Linux?
https://unix.stackexchange.com/questions/892/is-there-a-way-to-see-details-of-all-the-threads-that-a-process-has-in-linux

[2.4] How to view threads of a process on Linux
https://www.xmodulo.com/view-threads-process-linux.html

[3.1] Is it possible to get `ps` to render thread names in wide mode?
https://superuser.com/questions/1782484/is-it-possible-to-get-ps-to-render-thread-names-in-wide-mode

[3.2] pthread_setname_np(3) — Linux manual page
https://man7.org/linux/man-pages/man3/pthread_getname_np.3.html

[4.1] linux进程、线程与cpu的亲和性（affinity）
https://www.cnblogs.com/wenqiang/p/6049978.html

[4.2] 线程/进程和核绑定（CPU亲和性）
https://blog.csdn.net/qq_38232598/article/details/114263105

## 线程名
Linux 中，线程可以有线程名，最多15字节长，应用程序可以设置[3.2]。线程名的长度是 pthread 库的限制[3.1, 3.2]。
可以用 ps、top、pstree、htop等工具查看线程名[2.4]，见下面。
有些语言的运行时（如python）可以设置线程名，并且长度不受以上限制，但这种线程名独立于真线程名，不能从进程外部查看。

## 查看进程的线程
### ps 工具
查看全部进程的线程：
```
ps -eLf
UID          PID    PPID     LWP  C NLWP STIME TTY          TIME CMD
root           1       0       1  0    1 12:00 ?        00:00:01 /sbin/init splash
root           2       0       2  0    1 12:00 ?        00:00:00 [kthreadd]

ps -eL
PID     LWP TTY          TIME CMD
1       1 ?        00:00:14 systemd
2       2 ?        00:00:00 kthreadd
3       3 ?        00:00:00 pool_workqueue_release
```

NLWP 是线程数（number of light weight processes）。
-L 用于显示线程信息。
-f 表示格式，CMD 为进程的命令行。不带-f时，CMD为线程名（对于Linux，线程名最多15字节）。

查看单个进程的线程数:
```
ps -o nlwp 1970
NLWP
   5

ps -o thcount 1970
THCNT
    5

watch -n 1 ps -o thcount 1970
Every 1,0s: ps -o thcount 1970                                          baeldung: Tue Mar 29 16:35:30 2022

THCNT
    5
```
nlwp 和 thcount 都是线程数的不同说法。
nlwp == number of light weight processes
thcount == thread count

查看单个进程的线程[2.3]：
```
ps -Lf -p 857328
UID          PID    PPID     LWP  C NLWP STIME TTY          TIME CMD
duanyao   857328  856739  857328  0    2 12:47 pts/3    00:00:01 /home/duanyao/opt/venv/v311/bin/python flask_test.py
duanyao   857328  856739  857329  0    2 12:47 pts/3    00:00:00 /home/duanyao/opt/venv/v311/bin/python flask_test.py
```

`-p <pid>` 用来限定进程号。LWP(light weight processes) 指线程id。-L 用于显示线程信息，-f 表示'full'格式。

或者：
```
ps -Tf -p 1062638
UID          PID    SPID    PPID  C STIME TTY          TIME CMD
duanyao  1062638 1062638 1062637  0 15:46 pts/3    00:00:00 /home/duanyao/opt/venv/v311/bin/python flask_test.py
duanyao  1062638 1062652 1062637  0 15:46 pts/3    00:00:00 /home/duanyao/opt/venv/v311/bin/python flask_test.py
```
`-T` 表示县城信息。SPID 指线程id。

### top
top, 按下 f ，用 d 选择 nTH 字段，按q返回。
要查看线程，加上 -H 参数，要查看特定进程的线程，再加上`-p <pid>`。
```
top -H -p 857328

 进程号 USER      PR  NI    VIRT    RES    SHR    %CPU  %MEM     TIME+ COMMAND                                                                      
 857328 duanyao   20   0  268660  31616  11008 S   0.0   0.1   0:08.84 /home/duanyao/opt/venv/v311/bin/python flask_test.py                         
 857329 duanyao   20   0  268660  31616  11008 S   0.0   0.1   0:00.37 /home/duanyao/opt/venv/v311/bin/python flask_test.py 
```
“进程号”实际上是线程id。

### htop
[2.4]
press <F2> to enter htop setup menu. Choose Display option under Setup column, and toggle on Three view and Show custom thread names options. Presss <F10> to exit the setup.

### pstree
```
pstree -apts [pid, user]
-a     Show command line arguments.  If the command line of a process is swapped out, that process is shown in parentheses.  -a implicitly disables compaction for processes but not threads.
-c     Disable compaction of identical subtrees.  By default, subtrees are compacted whenever possible.
-p     Show PIDs.  PIDs are shown as decimal numbers in parentheses after each process name.  -p implicitly disables compaction.
-s     Show parent processes of the specified process.
-t     Show full names for threads when available.
```

## CPU 亲和性
### 概述
CPU 亲和性，即线程/进程被允许在哪些 CPU 上运行。一般没有特别设置时，允许在所有 CPU 上运行。
在 linux 上，亲和性是线程而非进程的属性，虽然可以设置/查询进程的亲和性，但实际等效于访问其主线程的亲和性。

### 编程设置/查询
以下方法用来设置/查询线程的 CPU 亲和性 [4.1]。
```
#define _GNU_SOURCE             /* See feature_test_macros(7) */
#include <pthread.h>

int pthread_setaffinity_np(pthread_t thread, size_t cpusetsize,
                            const cpu_set_t *cpuset);
int pthread_getaffinity_np(pthread_t thread, size_t cpusetsize,
                            cpu_set_t *cpuset);

Compile and link with -pthread.
```
在 linux 的中，pthread_setaffinity_np/pthread_getaffinity_np 是基于 sched_setaffinity / sched_getaffinity 系统调用实现的。

```
#define _GNU_SOURCE             /* See feature_test_macros(7) */
#include <sched.h>

int sched_setaffinity(pid_t pid, size_t cpusetsize,
                        const cpu_set_t *mask);

int sched_getaffinity(pid_t pid, size_t cpusetsize,
                        cpu_set_t *mask);
```
尽管 sched_setaffinity/sched_getaffinity 的参数是 pid_t，但实际上作用于线程。

在 python 中，`os.{sched_getaffinity|sched_setaffinity}` 可以用来查询/设置线程的 CPU 亲和性。

### 命令行工具
[4.2]
```
# 查询进程/线程的亲和性，用掩码的形式：
taskset -p 789875
pid 789875's current affinity mask: ffff

# 用CPU 序号列表的形式：
taskset -cp 789875
pid 789875's current affinity list: 0-15
```