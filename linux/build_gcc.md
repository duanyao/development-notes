## 从普通源码编译 gcc
mkdir build
cd build

../configure -v --with-pkgversion=Deepin 10.2.0-5 --with-bugurl=file:///usr/share/doc/gcc-10/README.Bugs --enable-languages=c,ada,c++,go,brig,d,fortran,objc,obj-c++ --prefix=/usr --with-gcc-major-version-only --program-suffix=-10 --program-prefix=x86_64-linux-gnu- --enable-shared --enable-linker-build-id --libexecdir=/usr/lib --without-included-gettext --enable-threads=posix --libdir=/usr/lib --enable-nls --enable-clocale=gnu --enable-libstdcxx-debug --enable-libstdcxx-time=yes --with-default-libstdcxx-abi=new --enable-gnu-unique-object --disable-vtable-verify --enable-plugin --with-system-zlib --enable-libphobos-checking=release --with-target-system-zlib=auto --enable-objc-gc=auto --enable-multiarch --disable-werror --with-arch-32=i686 --with-abi=m64 --with-multilib-list=m32,m64,mx32 --enable-multilib --with-tune=generic --enable-offload-targets=nvptx-none=/home/duanyao/t-project/deb-src/gcc-10-10.2.0/debian/tmp-nvptx/usr,amdgcn-amdhsa=/home/duanyao/t-project/deb-src/gcc-10-10.2.0/debian/tmp-gcn/usr,hsa --without-cuda-driver --enable-checking=release --build=x86_64-linux-gnu --host=x86_64-linux-gnu --target=x86_64-linux-gnu

参考 /home/duanyao/t-project/deb-src/gcc-10-10.2.0/build/config.log，但是 --enable-languages 少了 m2。


## 自制 debian backport 包

/etc/apt/sources.list.d/debian-sid.list:

```
deb-src https://mirrors.tuna.tsinghua.edu.cn/debian/ sid main contrib non-free
```

```
sudo apt-get install packaging-dev debian-keyring devscripts equivs
mkdir deb-src && cd deb-src/
apt source gcc-10
cd gcc-10-10.2.0/
sudo mk-build-deps --install --remove
dpkg-buildpackage -us -uc
```
如果 mk-build-deps 出错，看一下有什么包没有安装，或者版本不满足，试着手动安装，如
sudo aptitude install -t apricot g++-multilib
然后重新运行 mk-build-deps

如果 dpkg-buildpackage 报告依赖问题，如：

```
dpkg-checkbuilddeps: error: Unmet build dependencies: libc6-dev (>= 2.30-1~) lib32gcc-s1 libx32gcc-s1 libzstd-dev systemtap-sdt-dev binutils:native (>= 2.35) binutils-hppa64-linux-gnu:native (>= 2.35) nvptx-tools llvm-10 lld-10 texinfo (>= 4.3) locales-all gnat-10:native g++-10:native libisl-dev (>= 0.20) libmpc-dev (>= 1.0) lib32z1-dev dejagnu chrpath libgc-dev doxygen (>= 1.7.2) docbook-xsl-ns
```
先尽量安装最高版本，如：

```
sudo aptitude install -s -t apricot libc6-dev libzstd-dev systemtap-sdt-dev binutils:native binutils-hppa64-linux-gnu:native nvptx-tools texinfo locales-all libisl-dev libmpc-dev lib32z1-dev dejagnu chrpath libgc-dev doxygen docbook-xsl-ns llvm-8 lld-8 g++-8 gnat
```

版本号仍然偏低的，修改 debian/control 的 Build-Depends ，改版本号。

仓库里不存在的，可以先删除，看看编译的结果。删除了这两项：

```
lib32gcc-s1 [amd64 ppc64 kfreebsd-amd64 mipsn32 mipsn32el mips64 mips64el mipsn32r6 mipsn32r6el mips64r6 mips64r6el s390x sparc64 x32]
libx32gcc-s1 [amd64 i386]
```
很奇怪，lib32gcc-s1 是 gcc-10 的源码编译生成的，但要编译 gcc-10 却反而依赖于前者。

再次执行 `dpkg-buildpackage -us -uc`，出现错误：

```
dpkg-source: error: cannot represent change to gcc-10-build-deps_10.2.0-5_amd64.deb: binary file contents changed
dpkg-source: error: add gcc-10-build-deps_10.2.0-5_amd64.deb in debian/source/include-binaries if you want to store the modified binary in the debian tarball
```
执行：
```
mv gcc-10-build-deps_10.2.0-5_amd64.deb ..
```

再次执行 `dpkg-buildpackage -us -uc`，出现错误：

```
LOGFILE END /home/duanyao/t-project/deb-src/gcc-10-10.2.0/build-gcn/amdgcn-amdhsa/libgcc/config.log
debian/rules2:1516: recipe for target 'stamps/05-build-gcn-stamp' failed
```

查看 build-gcn/amdgcn-amdhsa/libgcc/config.log ，发现

```
configure:3790: checking for suffix of object files
configure:3812: /home/duanyao/t-project/deb-src/gcc-10-10.2.0/build-gcn/./gcc/xgcc -B/home/duanyao/t-project/deb-src/gcc-10-10.2.0/build-gcn/./gcc/ -nostdinc -B/home/duanyao/t-project/deb-src/gcc-10-10.2.0/build-gcn/amdgcn-amdhsa/newlib/ -isystem /home/duanyao/t-project/deb-src/gcc-10-10.2.0/build-gcn/amdgcn-amdhsa/newlib/targ-include -isystem /home/duanyao/t-project/deb-src/gcc-10-10.2.0/src-gcn/newlib/libc/include -B/usr/amdgcn-amdhsa/bin/ -B/usr/amdgcn-amdhsa/lib/ -isystem /usr/amdgcn-amdhsa/include -isystem /usr/amdgcn-amdhsa/sys-include -isystem /home/duanyao/t-project/deb-src/gcc-10-10.2.0/build-gcn/sys-include    -c -g -O2  conftest.c >&5
/home/duanyao/t-project/deb-src/gcc-10-10.2.0/build-gcn/./gcc/as: 106: exec: -triple=amdgcn--amdhsa: not found
configure:3816: $? = 1
configure: failed program was:
| /* confdefs.h */
| #define PACKAGE_NAME "GNU C Runtime Library"
| #define PACKAGE_TARNAME "libgcc"
| #define PACKAGE_VERSION "1.0"
| #define PACKAGE_STRING "GNU C Runtime Library 1.0"
| #define PACKAGE_BUGREPORT ""
| #define PACKAGE_URL "http://www.gnu.org/software/libgcc/"
| /* end confdefs.h.  */
| 
| int
| main ()
| {
| 
|   ;
|   return 0;
| }
configure:3834: error: in `/home/duanyao/t-project/deb-src/gcc-10-10.2.0/build-gcn/amdgcn-amdhsa/libgcc':
configure:3836: error: cannot compute suffix of object files: cannot compile
```

再次执行 `dpkg-buildpackage -us -uc` ，错误：

```
dpkg-checkbuilddeps: error: Unmet build dependencies: lib32gcc-s1 llvm-9 lld-9 gnat-5:native g++-5:native
dpkg-buildpackage: warning: build dependencies/conflicts unsatisfied; aborting
dpkg-buildpackage: warning: (Use -d flag to override.)
```

再次修改 debian/control 的 Build-Depends ，删除 lib32gcc-s1，将 llvm-9 lld-9 gnat-5:native g++-5:native 的版本后缀都改为实际安装的版本（8）.

但是仍然无法解决前面的错误。

升级到 deepin V20（相当于debian 10），后，错误变为：

```
binutils (>= 2.35) required, found 2.31.1-3-1+deepin
make[1]: *** [debian/rules.conf:1380: check-versions] Error 1
```

在 debian/rules.conf 中，将
```
  else ifneq (,$(filter $(distrelease),buster focal))
    BINUTILSBDV = 2.34
  else
    BINUTILSBDV = 2.35
```

改为

```
  else ifneq (,$(filter $(distrelease),buster focal))
    BINUTILSBDV = 2.34
  else
    BINUTILSBDV = 2.31
```

使用命令
```
cp debian/control.new debian/control
dpkg-buildpackage -us -uc -nc
rm /home/duanyao/t-project/deb-src/gcc-10-10.2.0/build-gcn/amdgcn-amdhsa/libgcc/config.cache
```
就不会清除已经编译的部分。但错误依旧。

创建 /home/duanyao/t-project/deb-src/gcc-10-10.2.0/build-gcn/amdgcn-amdhsa/libgcc/conftest.c :

```
/* confdefs.h */
#define PACKAGE_NAME "GNU C Runtime Library"
#define PACKAGE_TARNAME "libgcc"
#define PACKAGE_VERSION "1.0"
#define PACKAGE_STRING "GNU C Runtime Library 1.0"
#define PACKAGE_BUGREPORT ""
#define PACKAGE_URL "http://www.gnu.org/software/libgcc/"
/* end confdefs.h.  */

int
main ()
{

  ;
  return 0;
}
```

执行命令
```
cd /home/duanyao/t-project/deb-src/gcc-10-10.2.0/build-gcn/amdgcn-amdhsa/libgcc/

/home/duanyao/t-project/deb-src/gcc-10-10.2.0/build-gcn/./gcc/xgcc -B/home/duanyao/t-project/deb-src/gcc-10-10.2.0/build-gcn/./gcc/ -nostdinc -B/home/duanyao/t-project/deb-src/gcc-10-10.2.0/build-gcn/amdgcn-amdhsa/newlib/ -isystem /home/duanyao/t-project/deb-src/gcc-10-10.2.0/build-gcn/amdgcn-amdhsa/newlib/targ-include -isystem /home/duanyao/t-project/deb-src/gcc-10-10.2.0/src-gcn/newlib/libc/include -B/usr/amdgcn-amdhsa/bin/ -B/usr/amdgcn-amdhsa/lib/ -isystem /usr/amdgcn-amdhsa/include -isystem /usr/amdgcn-amdhsa/sys-include -isystem /home/duanyao/t-project/deb-src/gcc-10-10.2.0/build-gcn/sys-include    -c -g -O2  conftest.c
```
输出
```
/home/duanyao/t-project/deb-src/gcc-10-10.2.0/build-gcn/./gcc/as: 106: exec: -triple=amdgcn--amdhsa: not found
```
并且没有产生 .o 文件。

### 尝试不编译 admgcn

```
cd ~/t-project/deb-src/gcc-10-10.2.0/
export DEB_BUILD_OPTIONS="nogcn nohsa"
#cp debian/control.new debian/control
dpkg-buildpackage --no-check-builddeps --no-sign --jobs=4

dpkg-buildpackage --no-check-builddeps --no-sign --jobs=4 --no-pre-clean

#-us -uc -nc
# -nc --no-pre-clean
# -d --no-check-builddeps
#-us or --unsigned-source
#-uc or --unsigned-changes
#rm /home/duanyao/t-project/deb-src/gcc-10-10.2.0/build-gcn/amdgcn-amdhsa/libgcc/config.cache
```
错误：
```
=== START stamps/04-configure-gcn-stamp ===
dh_testdir
rm -rf bin-gcn
mkdir bin-gcn
ln -sf /usr/lib/llvm-10/bin/llvm-ar bin-gcn/ar
ln -sf /usr/lib/llvm-10/bin/llvm-mc bin-gcn/as
ln -sf /usr/lib/llvm-10/bin/lld bin-gcn/ld
ln -sf /usr/lib/llvm-10/bin/llvm-nm bin-gcn/nm
ln -sf /usr/lib/llvm-10/bin/llvm-ranlib bin-gcn/ranlib
rm -f stamps/04-configure-gcn-stamp stamps/05-build-gcn-stamp
rm -rf src-gcn /home/duanyao/t-project/deb-src/gcc-10-10.2.0/build-gcn
cp -al src src-gcn
rm -rf src-gcn/newlib
cp -a /newlib src-gcn/newlib
cp: cannot stat '/newlib': No such file or directory
make[1]: *** [debian/rules2:1498: stamps/04-configure-gcn-stamp] Error 1
make[1]: Leaving directory '/home/duanyao/t-project/deb-src/gcc-10-10.2.0'
make: *** [debian/rules:40：build] 错误 2
dpkg-buildpackage: error: debian/rules build subprocess returned exit status 2
```

分析。04-configure-gcn-stamp 的出处：

```
~/t-project/deb-src/gcc-10-10.2.0/debian$ grep -Irn "04-configure-gcn-stamp" *
rules.defs:2107:configure_gcn_stamp	:= $(stampdir)/04-configure-gcn-stamp
```
rules.defs:2107:
```
configure_gcn_stamp	:= $(stampdir)/04-configure-gcn-stamp
build_gcn_stamp		:= $(stampdir)/05-build-gcn-stamp
install_gcn_stamp	:= $(stampdir)/06-install-gcn-stamp
```

分析使用 configure_gcn_stamp 的地方：

```
~/t-project/deb-src/gcc-10-10.2.0/debian$ grep -Irn configure_gcn_stamp *
rules2:1481:$(configure_gcn_stamp): $(build_stamp) \
rules2:1496:	rm -f $(configure_gcn_stamp) $(build_gcn_stamp)
rules2:1510:	touch $(configure_gcn_stamp)
rules2:1512:$(build_gcn_stamp): $(configure_gcn_stamp) \
rules.defs:2107:configure_gcn_stamp	:= $(stampdir)/04-configure-gcn-stamp
```
分析使用 build_gcn_stamp 的地方：
```
~/t-project/deb-src/gcc-10-10.2.0/debian$ grep -Irn build_gcn_stamp *
rules2:1496:	rm -f $(configure_gcn_stamp) $(build_gcn_stamp)
rules2:1512:$(build_gcn_stamp): $(configure_gcn_stamp) \
rules2:1544:	touch $(build_gcn_stamp)
rules2:1689:$(check_stamp): $(filter $(build_stamp) $(build_jit_stamp) $(build_hppa64_stamp) $(build_nvptx_stamp) $(build_gcn_stamp), $(build_dependencies)) \
rules2:2543:$(install_gcn_stamp): $(build_gcn_stamp)
rules.defs:2108:build_gcn_stamp		:= $(stampdir)/05-build-gcn-stamp
rules.defs:2150:  build_dependencies += $(build_gcn_stamp)
```

分析 rules.defs:2150:

```
ifeq ($(with_offload_gcn),yes)
  build_dependencies += $(build_gcn_stamp)
  install_dependencies += $(install_gcn_stamp)
endif
```

分析目标 build：
rules2:1203:
```
build: $(sort $(build_arch_dependencies) $(build_indep_dependencies))
```

显示 build_arch_dependencies 和 build_indep_dependencies 的内容：
```
debian/rules2:1203: ,,,,(1)build_arch_dependencies is stamps/05-build-stamp stamps/05-build-jit-stamp stamps/05-build-nvptx-stamp stamps/05-build-hppa64-stamp stamps/06-check-stamp, build_indep_dependencies is stamps/05-build-stamp stamps/05-build-jit-stamp stamps/05-build-nvptx-stamp stamps/05-build-hppa64-stamp stamps/06-check-stamp stamps/05-build-html-stamp stamps/05-build-doxygen-stamp stamps/05-build-gnatdoc-stamp
```

测试执行

```
~/t-project/deb-src/gcc-10-10.2.0$ make -f debian/rules2 stamps/06-check-stamp

debian/rules2:1487: === START stamps/04-configure-gcn-stamp ===
...
```

说明是 check-stamp 造成的问题。分析：

```
~/t-project/deb-src/gcc-10-10.2.0/debian$ grep -Irn "check-stamp" *
rules.defs:2094:check_stamp		:= $(stampdir)/06-check-stamp

~/t-project/deb-src/gcc-10-10.2.0/debian$ grep -Irn "check_stamp" *
rules:47:check: $(check_stamp)
rules:48:$(check_stamp): $(build_stamp)
rules2:1690:check: $(check_stamp)
rules2:1691:$(check_stamp): $(filter $(build_stamp) $(build_jit_stamp) $(build_hppa64_stamp) $(build_nvptx_stamp) $(build_gcn_stamp), $(build_dependencies)) \
rules2:1842:	touch $(check_stamp)
rules2:1844:$(check_inst_stamp): $(check_stamp)
rules.defs:2094:check_stamp		:= $(stampdir)/06-check-stamp
rules.defs:2129:      check_dependencies += $(check_stamp)
rules.defs:2141:      check_dependencies += $(check_stamp)
```
rules2:1690:

```
check: $(check_stamp)
$(check_stamp): $(filter $(build_stamp) $(build_jit_stamp) $(build_hppa64_stamp) $(build_nvptx_stamp) $(build_gcn_stamp), $(build_dependencies)) \
    $(if $(filter nvptx-none, $(offload_targets)), $(install_nvptx_stamp)) \
    $(if $(filter $(gcn_target_name), $(offload_targets)), $(install_gcn_stamp))
```

显示 gcn_target_name offload_targets: 

```
debian/rules2:1690: ,,,,gcn_target_name = amdgcn-amdhsa, offload_targets = nvptx-none amdgcn-amdhsa hsa
```

这就是问题所在了。

rules.defs 中存在这两处bug，忽略了 DEB_BUILD_OPTIONS ：

rules.defs:775:
```
ifneq (,$(filter $(DEB_TARGET_ARCH),$(gcn_archs)))  # gcn_archs := amd64
  offload_targets += $(gcn_target_name)
  with_offload_gcn := yes
endif
```
rules.defs:806:
```
hsa_archs := amd64
ifneq (,$(filter $(DEB_TARGET_ARCH),$(hsa_archs)))
  offload_targets += hsa
  with_offload_hsa := yes
endif
```

分别改为：
```
ifneq (,$(filter $(DEB_TARGET_ARCH),$(gcn_archs)))
  ifeq ($(findstring nogcn, $(DEB_BUILD_OPTIONS)),)
    offload_targets += $(gcn_target_name)
    with_offload_gcn := yes
  endif
endif
```

```
ifneq (,$(filter $(DEB_TARGET_ARCH),$(hsa_archs)))
  ifeq ($(findstring nohsa, $(DEB_BUILD_OPTIONS)),)
    offload_targets += hsa
    with_offload_hsa := yes
  endif
endif
```

继续执行 `dpkg-buildpackage --no-check-builddeps --no-sign --jobs=4 --no-pre-clean` 。

### 不编译测试

```
export DEB_BUILD_OPTIONS="nogcn nohsa nocheck"
```
继续执行 `dpkg-buildpackage --no-check-builddeps --no-sign --jobs=4 --no-pre-clean` 。

### dpkg-source: error: unwanted binary file

```
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: error: unwanted binary file: debian/liblsan0/usr/lib/x86_64-linux-gnu/liblsan.so.0
dpkg-source: error: unwanted binary file: debian/liblsan0/usr/lib/x86_64-linux-gnu/liblsan.so.0.0.0
dpkg-source: error: unwanted binary file: debian/libtsan0/usr/lib/x86_64-linux-gnu/libtsan.so.0
dpkg-source: error: unwanted binary file: debian/libtsan0/usr/lib/x86_64-linux-gnu/libtsan.so.0.0.0
dpkg-source: error: unwanted binary file: debian/gcc-10-offload-nvptx/usr/bin/x86_64-linux-gnu-accel-nvptx-none-gcc-10
dpkg-source: error: unwanted binary file: debian/gcc-10-offload-nvptx/usr/lib/gcc/x86_64-linux-gnu/10/accel/nvptx-none/collect2
dpkg-source: error: unwanted binary file: debian/gcc-10-offload-nvptx/usr/lib/gcc/x86_64-linux-gnu/10/accel/nvptx-none/lto1
dpkg-source: error: unwanted binary file: debian/gcc-10-offload-nvptx/usr/lib/gcc/x86_64-linux-gnu/10/accel/nvptx-none/lto-wrapper
dpkg-source: error: unwanted binary file: debian/gcc-10-offload-nvptx/usr/lib/gcc/x86_64-linux-gnu/10/accel/nvptx-none/mkoffload
dpkg-source: error: unwanted binary file: debian/libgomp-plugin-nvptx1/usr/lib/x86_64-linux-gnu/libgomp-plugin-nvptx.so.1
dpkg-source: error: unwanted binary file: debian/libgomp-plugin-nvptx1/usr/lib/x86_64-linux-gnu/libgomp-plugin-nvptx.so.1.0.0
dpkg-source: error: detected 11 unwanted binary files (add them in debian/source/include-binaries to allow their inclusion).
dpkg-buildpackage: error: dpkg-source -b . subprocess returned exit status 255
```

rm -rf debian/libtsan0/usr/lib/x86_64-linux-gnu/* debian/liblsan0/usr/lib/x86_64-linux-gnu/* debian/gcc-10-offload-nvptx/usr/bin/* debian/gcc-10-offload-nvptx/usr/lib/gcc/x86_64-linux-gnu/* debian/libgomp-plugin-nvptx1/usr/lib/x86_64-linux-gnu/*

### 调试 control 文件的生成

执行 `make -f debian/rules control` 。

这会重新生成 debian/control, debian/substvars.local, debian/rules.parameters 等文件，指定了自身的版本和依赖的版本。

### 修改生成的 deb 包的版本号

修改 debian/changelog 的第一行的版本号，例如
```
gcc-10 (10.2.0-5+redep1) unstable; urgency=medium
```
用 dpkg-parsechangelog 命令可以查看效果。


### 修改版本号依赖关系

见 debian/gcc-10-source/usr/src/gcc-10/debian/control

gcc:Version 定义的地方：

rules.conf:1083:
```
		echo 'libgcc:Version=$(DEB_GCC_VERSION)'; \
		echo 'gcc:Version=$(DEB_GCC_VERSION)'; \
		echo 'gcc:EpochVersion=$(DEB_EVERSION)'; \
		echo 'gcc:SoftVersion=$(DEB_GCC_SOFT_VERSION)'; \
		echo 'gdc:Version=$(DEB_GDC_VERSION)'; \
		echo 'gm2:Version=$(DEB_GM2_VERSION)'; \
		echo 'gnat:Version=$(DEB_GNAT_VERSION)'; \
		echo 'gnat:SoftVersion=$(DEB_GNAT_SOFT_VERSION)'; \
```
这些变量被写入 debian/substvars.local 文件。


将 rules.conf:1083-1084 改为:
```
		echo 'libgcc:Version=$(DEB_GCC_SOFT_VERSION)'; \
		echo 'gcc:Version=$(DEB_GCC_SOFT_VERSION)'; \
		echo 'gcc:EpochVersion=$(DEB_EVERSION)'; \
		echo 'gcc:SoftVersion=$(DEB_GCC_SOFT_VERSION)'; \
		echo 'gdc:Version=$(DEB_GCC_SOFT_VERSION)'; \
		echo 'gm2:Version=$(DEB_GCC_SOFT_VERSION)'; \
		echo 'gnat:Version=$(DEB_GNAT_SOFT_VERSION)'; \
		echo 'gnat:SoftVersion=$(DEB_GNAT_SOFT_VERSION)'; \
```

### 编译 i386 版本

```
export DEB_BUILD_OPTIONS="nogcn nohsa nocheck"

dpkg-buildpackage --no-check-builddeps --no-sign --host-arch i386 --jobs=4
dpkg-buildpackage --no-check-builddeps --no-sign --host-arch i386 --jobs=4 --no-pre-clean
```

dpkg-buildpackage 有两个关于CPU架构的选项。

--host-arch 指编译出来的 deb 包可运行的机器的CPU架构
--target-arch 指编译出来的 deb 包产生的代码可运行的机器的CPU架构。仅当编译出来的 deb 包是个编译器时有意义。


dpkg-buildpackage 可能至少要运行两次，一次不带 --no-pre-clean ，一次带 --no-pre-clean ，因为只跑一次可能出错。

出错：

```
configure: exit 0
LOGFILE END /home/duanyao/t-project/deb-src/gcc-10-10.2.0/build/libcpp/config.log
make[1]: *** [debian/rules2:1234: stamps/05-build-stamp] Error 1
make[1]: Leaving directory '/home/duanyao/t-project/deb-src/gcc-10-10.2.0'
make: *** [debian/rules:40：build] 错误 2
dpkg-buildpackage: error: debian/rules build subprocess returned exit status 2
```
从输出和日志看不出来原因。

### 单独编译 gcc-n-base

可能是通过
debian/rules.d/binary-base.mk

### 安装 gcc-10 的 deb 包

```
sudo aptitude install gcc-10
```
出错：
```
下列“新”软件包将被安装。         
  cpp-10{a} gcc-10{b} gcc-10-base{a} libasan6{a} libgcc-10-dev{ab} libgcc-s1{ab} 
0 个软件包被升级，新安装 6 个，0 个将被删除， 同时 75 个将不升级。
需要获取 0 B/26.8 MB 的存档。解包后将要使用 103 MB。
下列软件包存在未满足的依赖关系：
 libgcc-10-dev : 依赖: libgomp1 (>= 10.2.0-5) 但是 8.3.0.3-3+rebuild 已安装
                 依赖: libitm1 (>= 10.2.0-5) 但是 8.3.0.3-3+rebuild 已安装
                 依赖: libatomic1 (>= 10.2.0-5) 但是 8.3.0.3-3+rebuild 已安装
                 依赖: liblsan0 (>= 10.2.0-5) 但是 8.3.0.3-3+rebuild 已安装
                 依赖: libtsan0 (>= 10.2.0-5) 但是 8.3.0.3-3+rebuild 已安装
                 依赖: libubsan1 (>= 10.2.0-5) 但是 8.3.0.3-3+rebuild 已安装
                 依赖: libquadmath0 (>= 10.2.0-5) 但是 8.3.0.3-3+rebuild 已安装
 libgcc-s1 : 破坏: cryptsetup-initramfs (< 2:2.2.2-3~) 但是 2:2.1.0-5+deb10u2 已安装
 gcc-10 : 依赖: libcc1-0 (>= 10.2.0-5) 但是 8.3.0.3-3+rebuild 已安装
下列动作将解决这些依赖关系：

     保持 下列软件包于其当前版本：
1)     gcc-10 [未安装的]          
2)     libasan6 [未安装的]        
3)     libgcc-10-dev [未安装的]   
4)     libgcc-s1 [未安装的]       

```

libgcc-s1 破坏 cryptsetup-initramfs (< 2:2.2.2-3~) 的原因是： https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=950551

似乎只能升级 cryptsetup-initramfs 才能解决。删除 cryptsetup-initramfs 会有问题，因为 dde 和 overlayroot 的一些组件依赖它。
debian sid 的 cryptsetup-initramfs 当前版本是(2.3.3-2 2020-08-19)，似乎可以升级。

```
aptitude install -s cryptsetup-initramfs -t sid
```

升级完 cryptsetup-initramfs 即可安装 gcc-10，sid 或者自己编译的都可以装。

### 安装 g++-10

```
$ sudo aptitude install g++-10
[sudo] duanyao 的密码：
下列“新”软件包将被安装。         
  g++-10 libstdc++-10-dev{ab} 
0 个软件包被升级，新安装 2 个，0 个将被删除， 同时 109 个将不升级。
需要获取 10.4 MB 的存档。解包后将要使用 48.7 MB。
下列软件包存在未满足的依赖关系：
 libstdc++-10-dev : 依赖: libstdc++6 (>= 10.2.0-5) 但是 8.3.0.3-3+rebuild 已安装
                    依赖: libc6-dev (>= 2.30-1~) 但是 2.28.8-2+rebuild 已安装
```

libc6-dev 对整个系统影响大，无法简单地升级，因此要修改 libstdc++-10-dev 的依赖版本。

control:8:  libc6.1-dev (>= 2.30-1~) [alpha ia64] | libc0.3-dev (>= 2.30-1~) [hurd-i386] | libc0.1-dev (>= 2.30-1~) [kfreebsd-i386 kfreebsd-amd64] | libc6-dev (>= 2.30-1~), libc6-dev (>= 2.13-31) [armel armhf]

rules.conf:364:  LIBC_BUILD_DEP = libc6.1-dev (>= $(libc_dev_ver)) [alpha ia64] | libc0.3-dev (>= $(libc_dev_ver)) [hurd-i386] | libc0.1-dev (>= $(libc_dev_ver)) [kfreebsd-i386 kfreebsd-amd64] | libc6-dev (>= $(libc_dev_ver))

gcc-10-offload-nvptx/DEBIAN/control:7:Depends: gcc-10-base (= 10.2.0-5), gcc-10 (= 10.2.0-5), libc6-dev (>= 2.30-1~), nvptx-tools, libgomp-plugin-nvptx1 (>= 10.2.0-5), libc6 (>= 2.14), libgmp10 (>= 2:5.0.1~), libmpc3, libmpfr6 (>= 3.1.3), libzstd1 (>= 1.3.2), zlib1g (>= 1:1.1.4)
lib32gcc-10-dev/DEBIAN/control:8:Recommends: libc6-dev (>= 2.30-1~)


rules.conf:1094:		echo 'dep:libcdev=$(LIBC_DEV_DEP)'; \

rules.conf:358:
LIBC_DEP := $(LIBC_DEP)$(LS)$(AQ)
LIBC_DEV_DEP := $(LIBC_DEV_DEP)$(LS)$(AQ) (>= $(libc_dev_ver))

rules.conf:258:
libc_ver := 2.11
libc_dev_ver := $(libc_ver)
ifeq ($(with_multiarch_lib),yes)
  ifeq ($(derivative),Debian)
    libc_dev_ver := 2.30-1~
  else
    libc_dev_ver := 2.13-0ubuntu6
  endif
endif

$(warning ,,,, libc_dev_ver = $(libc_dev_ver), derivative = $(derivative))

将 libc_dev_ver := 2.30-1~ 改为 libc_dev_ver := 2.27 。

$ sudo aptitude install -s g++-10 -t ""
下列“新”软件包将被安装。         
  g++-10{b} libstdc++-10-dev{ab} 
0 个软件包被升级，新安装 2 个，0 个将被删除， 同时 83 个将不升级。
需要获取 0 B/10.4 MB 的存档。解包后将要使用 48.3 MB。
下列软件包存在未满足的依赖关系：
 libstdc++-10-dev : 依赖: gcc-10-base (= 10.2.0-5+redep1) 但是 10.2.0-5 已安装
                    依赖: libgcc-10-dev (= 10.2.0-5+redep1) 但是 10.2.0-5 已安装
                    依赖: libstdc++6 (>= 10.2.0-5+redep1) 但是 8.3.0.3-3+rebuild 已安装
 g++-10 : 依赖: gcc-10-base (= 10.2.0-5+redep1) 但是 10.2.0-5 已安装
          依赖: gcc-10 (= 10.2.0-5+redep1) 但是 10.2.0-5 已安装

$ sudo aptitude install gcc-10-base=10.2.0-5+redep1
下列软件包将被升级：             
  gcc-10-base{b} 
1 个软件包被升级，新安装 0 个，0 个将被删除， 同时 92 个将不升级。
需要获取 0 B/198 kB 的存档。解包后将要使用 0 B。
下列软件包存在未满足的依赖关系：
 gcc-10-base : 破坏: gcc-10-base:i386 (!= 10.2.0-5+redep1) 但是 10.2.0-5 已安装
 gcc-10-base:i386 : 破坏: gcc-10-base (!= 10.2.0-5) 但是 10.2.0-5+redep1 将被安装

gcc-10-base:i386=10.2.0-5+redep1 是不存在的（未能编译出来），但其它一些程序依赖 gcc-10-base:i386=10.2.0-5 ，所以 gcc-10-base=10.2.0-5+redep1 也就无法安装。
 
## 分析 debian 目录的 amdgcn

```
bian/
duanyao@duanyao-laptop-c:~/t-project/deb-src/gcc-10-10.2.0/debian$ grep -Irn amdgcn *
control:1806:Package: gcc-10-offload-amdgcn
control:1810:  libgomp-plugin-amdgcn1 (>= ${gcc:Version}), 
control:1818:Package: libgomp-plugin-amdgcn1
control.m4:6119:ifenabled(`olgcn',`
control.m4:6120:Package: gcc`'PV-offload-amdgcn
control.m4:6121:Architecture: gcn_archs
control.m4:6126:  libgomp-plugin-amdgcn`'GOMP_SO (>= ${gcc:Version}),
control.m4:6135:ifenabled(`gompgcn',`
control.m4:6136:Package: libgomp-plugin-amdgcn`'GOMP_SO
control.m4:6137:Architecture: gcn_archs
control.m4:6144:')`'dnl gompgcn
control.m4:6145:')`'dnl olgcn

rules2:750:  ifneq (,$(filter $(gcn_target_name), $(offload_targets)))
rules2:751:	offload_opt += $(gcn_target_name)=$(CURDIR)/$(d)-gcn/$(PF)
rules2:1461:	--target=$(gcn_target_name) \
rules2:1471:	--with-build-time-tools=$(CURDIR)/bin-gcn
rules2:1475:CONFARGS_GCN += --program-prefix=$(gcn_target_name)-
rules2:1481:$(configure_gcn_stamp): $(build_stamp) \
rules2:1488:	rm -rf bin-gcn
...

rules.conf:460:ifeq ($(with_offload_gcn),yes)
rules.conf:461:  OFFLOAD_BUILD_DEP += llvm-$(gcn_tools_llvm_version) [$(gcn_archs)], lld-$(gcn_tools_llvm_version) [$(gcn_archs)],
rules.conf:894:ifeq ($(with_offload_gcn),yes)
rules.conf:895:  addons += olgcn gompgcn
rules.conf:1047:	  -Dgcn_archs="$(gcn_archs)" \
rules.conf:1050:	  -DLLVM_VER=$(gcn_tools_llvm_version) \
rules.d/binary-gcn.mk:1:ifeq ($(with_offload_gcn),yes)
rules.d/binary-gcn.mk:2:  arch_binaries := $(arch_binaries) gcn
rules.d/binary-gcn.mk:8:p_gcn	= gcc$(pkg_ver)-offload-amdgcn
rules.d/binary-gcn.mk:11:p_pl_gcn = libgomp-plugin-amdgcn1
rules.d/binary-gcn.mk:24:#	$(PF)/amdgcn-none
rules.d/binary-gcn.mk:52:	: # FIXME: split out into an amdgcn-tools package?

rules.d/binary-snapshot.mk:26:ifeq ($(with_offload_gcn),yes)
rules.d/binary-snapshot.mk:27:  snapshot_depends += llvm-$(gcn_tools_llvm_version), lld-$(gcn_tools_llvm_version),
rules.d/binary-snapshot.mk:35:    $(if $(filter yes, $(with_offload_gcn)), $(install_gcn_stamp))

rules.defs:10:builddir_gcn	= $(CURDIR)/build-gcn
rules.defs:765:gcn_archs := amd64
rules.defs:766:gcn_target_name = amdgcn-amdhsa
rules.defs:769:  gcn_tools_llvm_version = 9
rules.defs:771:  gcn_tools_llvm_version = 10
rules.defs:774:ifneq (,$(filter $(DEB_TARGET_ARCH),$(gcn_archs)))
rules.defs:775:  offload_targets += $(gcn_target_name)
rules.defs:776:  with_offload_gcn := yes
rules.defs:780:  with_offload_gcn :=
rules.defs:783:  with_offload_gcn := disabled for cross builds
rules.defs:788:    with_offload_gcn := disabled for $(PKGSOURCE) builds
rules.defs:791:ifneq ($(findstring nogcn, $(DEB_BUILD_OPTIONS)),)
rules.defs:792:  with_offload_gcn := disabled by DEB_BUILD_OPTIONS
rules.defs:794:with_offload_gcn := $(call envfilt, gcn, , , $(with_offload_gcn))
rules.defs:795:#with_offload_gcn := not yet built for GCC 10
rules.defs:2049:ifeq ($(with_offload_gcn),yes)
```

rules.defs:791:
```
ifneq ($(findstring nogcn, $(DEB_BUILD_OPTIONS)),)
  with_offload_gcn := disabled by DEB_BUILD_OPTIONS
endif

ifneq ($(findstring nohsa, $(DEB_BUILD_OPTIONS)),)
  with_offload_hsa := disabled by DEB_BUILD_OPTIONS
endif
```

### 分析 binutils 依赖

```
rules.conf:255:#BINUTILS_BUILD_DEP += , binutils-gold (>= $(BINUTILSV)) [$(gold_archs)]
rules.conf:1088:		echo 'binutils:Version=$(BINUTILSV)'; \
rules.conf:1114:	echo 'dep:gold=binutils-gold (>= $(BINUTILSV))' \
rules.conf:1384:	v=$$(dpkg-query -l binutils binutils-multiarch 2>/dev/null | awk '/^.i/ {print $$3;exit}'); \
rules.conf:1386:	  echo "binutils (>= $(BINUTILSBDV)) required, found $$v"; \
rules.d/binary-snapshot.mk:21:  snapshot_depends = binutils-hppa64,
rules.defs:344:  # FIXME: newer binutils needed?
rules.defs:1434:# armel with binutils 2.20.51 only
rules.defs:1578:      binutils_hppa64 := binutils-hppa64
rules.defs:1583:      binutils_hppa64 := binutils-hppa64-linux-gnu
substvars.local:9:binutils:Version=2.31.1
substvars.local:29:dep:gold=binutils-gold (>= 2.31.1)
```
