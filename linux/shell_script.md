## 参考
[1] Linux - Shell Script
http://www.ams.sunysb.edu/~wli/AMS595/linux_shell_script.html

[2] The Beginner’s Guide to Shell Scripting: The Basics
http://www.howtogeek.com/67469/the-beginners-guide-to-shell-scripting-the-basics/

[3] What does $@ mean in a shell script?
http://stackoverflow.com/questions/9994295/what-does-mean-in-a-shell-script

[4] Shell Variables you should know (including $* and $@)
http://teaching.idallen.com/dat2330/04f/notes/shell_variables.txt

[5] 第十三章、学习 Shell Scripts
http://vbird.dic.ksu.edu.tw/linux_basic/0340bashshell-scripts_4.php

[6] Unix shell script find out which directory the script file resides?
https://stackoverflow.com/questions/242538/unix-shell-script-find-out-which-directory-the-script-file-resides

[7] How do you convert an entire directory with ffmpeg?
https://stackoverflow.com/questions/5784661/how-do-you-convert-an-entire-directory-with-ffmpeg

[8] How to Evaluate Arithmetic Expressions in Bash
https://www.baeldung.com/linux/bash-arithmetic-expressions

[9] How to Run Bash Commands in Parallel [4 Ways With Examples]
https://linuxsimply.com/bash-scripting-tutorial/basics/executing/run-commands-in-parallel/

[10] Linux下的并行神器—— GNU parallel
https://www.jianshu.com/p/cc54a72616a1

[11] What happens to /usr/bin/parallel if I install the moreutils on top of the parallel package?
https://askubuntu.com/questions/1191516/what-happens-to-usr-bin-parallel-if-i-install-the-moreutils-on-top-of-the-paral

[12] How to Redirect stderr to stdout in Bash
https://linuxize.com/post/bash-redirect-stderr-stdout/

[13] Variable Substitution in Bash [Replace Character & Substring]
https://linuxsimply.com/bash-scripting-tutorial/variables/usage/variable-substitution/

[14] 8 Methods to Split String in Bash [With Examples]
https://linuxsimply.com/bash-scripting-tutorial/string/split-string/

## 标准输入输出和重定向和管道

### 标准错误重定向到标准输出
```
# shell 通用
command > file 2>&1
# bash 适用
command &> file
```

## 特殊变量

$TERM:
    Set during login to contain the Unix name of the type of your
    terminal.  Common values are "vt100", "xterm", "ansi", and "linux".
    When in doubt about the type of terminal you are on, set TERM to
    "vt100".  Don't use vi or vim without having a correct $TERM set.

$HOME:
    Set during login to be the absolute path to your home directory.
    The shell alias "~" is a synonym for $HOME if used at the start of
    a pathname, e.g. ~/foo is the same as $HOME/foo
    (Note: ~userid at the start of a pathname is expanded by the shell
    to be the home directory of the userid "userid", e.g. ~idallen .)

$PATH:
    Set during login to be a colon-separated list of directories in
    which the shell will look when it tries to find an executable file
    that matches a command name.  $PATH is not used if the command name
    you type already contains any slashes.  The "which" command looks
    for a command name in your $PATH.

$SHELL:
    Set during login to be the pathname of the Unix Shell you are
    assigned in the password file (/etc/passwd).  (This may or may not
    be the shell you are currently running, since you are free to start
    other shells once you have logged in.

$USER:
    Set during login to be your account name, e.g. abcd0001.
    Some systems set $LOGNAME instead of $USER.

$$:
    The process ID of the current shell.  This is often used when
    creating unique temporary file names, e.g.:  tmp=/tmp/tempfile$$

$#:
    The count of command line arguments given to the currently
    executing shell (usually used inside a shell script).

$0:
    The name of the currently executing shell script.

$?:
    The return status of the most recently executed command.

$1, $2, ...:
    The individual command line arguments given to the currently
    executing shell script.

$*:
    All the command-line arguments given to the currently executing
    shell script.  The arguments are each separated by one blank.

$@:
    All the command-line arguments given to the currently executing
    shell script.  When this variable is used inside double quotes, it
    becomes a list of all the command-line arguments each individually
    double-quoted - this is the only time that a double-quoted string
    generates more than one argument.  See the next section for more
    detail on how this works.

$* all command line arguments
$@ is all of the parameters passed to the script. For instance, if you call ./someScript.sh foo bar then $@ will be equal to foo bar.

wrapper "one two    three" four five "six seven"

"$@": wrappedProgram "one two    three" four five "six seven"
"$*": wrappedProgram "one two    three four five six seven"
$*:   wrappedProgram one two three four five six seven

注意，"command-line arguments" 均不包含 shell script 本身以及以前的部分。例如，`sh foo.sh ab cd` 的 `$*` 只包括 "ab cd" 。

## 多行变量
多行变量应该用双引号引起来：
```
VAR="This displays with \
extra spaces."
echo ${VAR}
```

## 调用其它脚本文件
调用其它脚本文件是在当前脚本上下文中执行的，不会创建子进程，其它脚本文件修改/创建的变量会影响当前脚本。
```
. foo.sh        # sh 标准语法
source foo.sh   # bash 语法，source 为内部命令
```

## if .... then

if [ 条件判断式 ]; then
	当条件判断式成立时，可以进行的命令工作内容；
fi   <==将 if 反过来写，就成为 fi 啦！结束 if 之意！

或者

if [ 条件判断式 ]
then
   do something
fi

或者

if 命令
then
   do something
fi

如果我有多个条件要判别时， 除了 sh06.sh 那个案例所写的，也就是『将多个条件写入一个中括号内的情况』之外， 我还可以有多个中括号来隔开喔！而括号与括号之间，则以 && 或 || 来隔开，他们的意义是：

    && 代表 AND ；
    || 代表 or ；

所以，在使用中括号的判断式中， && 及 || 就与命令下达的状态不同了。举例来说， sh06.sh 里面的判断式可以这样修改：

    [ "$yn" == "Y" -o "$yn" == "y" ]
    上式可替换为
    [ "$yn" == "Y" ] || [ "$yn" == "y" ]

多个条件判断 (if ... elif ... elif ... else) 分多种不同情况运行
if [ 条件判断式一 ]; then
	当条件判断式一成立时，可以进行的命令工作内容；
elif [ 条件判断式二 ]; then
	当条件判断式二成立时，可以进行的命令工作内容；
else
	当条件判断式一与二均不成立时，可以进行的命令工作内容；
fi

## 循环

```
for value in {1..5}  #bash，包含上限5
#for i in $(seq 1 5) #sh，包含上限5
#for value in 1 2 3 4 5  
do  
     echo "Now value is $value..."  
done 

while [ "$yn" != "yes" -a "$yn" != "YES" ] 
do 
    read -p "Please input yes/YES to stop : " yn
done
```

死循环和退出循环：
```
while [ 1 ]
do
  #...
  if [ $? -eq 0 ]
  then
    break
  fi
done
```

读取文本文件，按行分割为元素（空行会被忽略），然后迭代：
```
for variable in $(cat <filename>); do
<commands to be executed>
done
```

将循环写到一行里：
```
for v in a b c; do echo "item $v"; done
```

## 比较

整数比较

-eq       等于,如:if [ "$a" -eq "$b" ]
-ne       不等于,如:if [ "$a" -ne "$b" ]
-gt       大于,如:if [ "$a" -gt "$b" ]
-ge       大于等于,如:if [ "$a" -ge "$b" ]
-lt       小于,如:if [ "$a" -lt "$b" ]
-le       小于等于,如:if [ "$a" -le "$b" ]
<       小于(bash, 需要双括号),如:(("$a" < "$b"))
<=       小于等于(需要双括号),如:(("$a" <= "$b"))
>       大于(需要双括号),如:(("$a" > "$b"))
>=       大于等于(需要双括号),如:(("$a" >= "$b"))

字符串比较
=       等于,如:if [ "$a" = "$b" ]
==       等于,如:if [ "$a" == "$b" ],与=等价
       注意:==的功能在[[]]和[]中的行为是不同的,如下:
       1 [[ $a == z* ]]    # 如果$a以"z"开头(模式匹配)那么将为true
       2 [[ $a == "z*" ]] # 如果$a等于z*(字符匹配),那么结果为true
       3
       4 [ $a == z* ]      # File globbing 和word splitting将会发生
       5 [ "$a" == "z*" ] # 如果$a等于z*(字符匹配),那么结果为true
       一点解释,关于File globbing是一种关于文件的速记法,比如"*.c"就是,再如~也是.
       但是file globbing并不是严格的正则表达式,虽然绝大多数情况下结构比较像.
!=       不等于,如:if [ "$a" != "$b" ]
       这个操作符将在[[]]结构中使用模式匹配.
<       小于,在ASCII字母顺序下.如:
       if [[ "$a" < "$b" ]]
       if [ "$a" \< "$b" ]
       注意:在[]结构中"<"需要被转义.
>       大于,在ASCII字母顺序下.如:
       if [[ "$a" > "$b" ]]
       if [ "$a" \> "$b" ]
       注意:在[]结构中">"需要被转义.
       具体参考Example 26-11来查看这个操作符应用的例子.
-z       字符串为"null".就是长度为0.
-n       字符串不为"null"
       注意:
       使用-n在[]结构中测试必须要用""把变量引起来.使用一个未被""的字符串来使用! -z
       或者就是未用""引用的字符串本身,放到[]结构中。虽然一般情况下可
       以工作,但这是不安全的.习惯于使用""来测试字符串是一种好习惯.

## 列表
```
ROBOTS=('192.168.0.74' '192.168.0.85')
size=${#ROBOTS[*]}
for ((i=0;i<$size;i++))
do
ROBOT_DEPLOY=${ROBOTS[$i]}
done

CHECKING_ROBOTS=(${ROBOTS[@]})
while [ ${#CHECKING_ROBOTS[@]} -ne 0 ]
do
    echo "===将要自检以下目标 ${CHECKING_ROBOTS[@]}==="
    FAILED_ROBOTS=()
    for ROBOT_DEPLOY in ${CHECKING_ROBOTS[@]}
    do
        echo "===docker 服务自检开始 ${ROBOT_DEPLOY}==="
        curl --no-progress-meter -m 10 -I "http://${ROBOT_DEPLOY}:9105/metrics"  | grep HTTP

        if [ $? -eq 0 ]
        then
            echo "===docker 服务自检正常 ${ROBOT_DEPLOY}==="
        else
            echo "===docker 服务自检失败，将重启后重试 ${ROBOT_DEPLOY}==="
            FAILED_ROBOTS=(${FAILED_ROBOTS[@]} ${ROBOT_DEPLOY})
            ssh root@$ROBOT_DEPLOY docker stop -t 1 ${container_name}
            ssh root@$ROBOT_DEPLOY docker start ${container_name}
        fi
    done
    CHECKING_ROBOTS=(${FAILED_ROBOTS[@]})
    # 模型服务初始化约需要35秒，等待其完成。
    sleep 45
done
```

## 算术
[8]
shell 中可以调用 expr 或者 bc 命令来计算数学表达式。expr 只能计算整数和布尔值，bc 可以计算浮点数，还支持函数、控制流，实际上是一种脚本语言。

```
$ expr 2 + 3
5
$ expr 2 \< 3
1
$ expr substr Baeldung 1 4
Bael

$ echo "scale=2;4/3" | bc
1.33

$ echo "for(i=1; i<=10; i++) {if (i % 2 == 0) i;}" | bc
2
4
6
8
10

$ echo "scale=4;sqrt(10)" | bc
3.1622
```

## function 功能

什么是『函数 (function)』功能啊？简单的说，其实， 函数可以在 shell script 当中做出一个类似自订运行命令的东西，最大的功能是， 可以简化我们很多的程序码～举例来说，上面的 sh12.sh 当中，每个输入结果 one, two, three 其实输出的内容都一样啊～那么我就可以使用 function 来简化了！ function 的语法是这样的：

function fname() {
	程序段
}

在 shell script 当中的 function 的配置一定要在程序的最前面。

函数内，参数可通过 $1, $2 等访问，但是函数名后面并没有参数表。

## 路径操作
basename: 得到路径的文件名部分。
dirname: 得到路径的目录部分，不含结尾的 '/'。

求出脚本所在的目录[6]：

```
# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
echo $SCRIPTPATH
```
## 字符串
### 空串
使用空串：
```
# 以下 var 都是空串
var=
var=''

#传递给命令行程序
a_command --arg $var
a_command --arg "$var"
```

测试字符串空/非空
[ -z "$var" ] # 空
[ -n "$var" ] # 非空


### 字符串替换

子字符串替换/删除[13]
```
new_string="${original/old_substring/new_substring}"
result="${original_string/$substring_to_remove/}
```

模式替换
```
f=foo.mp4
echo ${f%%.*}.mkv # foo.mkv
```

### 字符串分割
[14]
read 是内部命令，可以用来分割字符串，写入指定的列表变量。默认的分割符是空白符，也可以用变量 IFS 来指定。
```
sentence="a 1 b"
read -a w_list <<< "$sentence"
echo ${w_list[0]} # a
echo ${w_list[1]} # 1

for w in ${w_list[@]}; do echo $w; done # a 1 b

sentence="a,1,b"
IFS=',' read -a w_list <<< "$sentence"
echo ${w_list[0]} # a
echo ${w_list[1]} # 1

for w in ${w_list[@]}; do echo $w; done # a 1 b
```

## 版本号
bash:

echo $BASH_VERSION  # 5.0.3(1)-release
echo "${BASH_VERSINFO[*]: 0:3}" # 5 0 3
echo "${BASH_VERSINFO[0]}"  # 5

注意，BASH_VERSION 是可以被脚本修改的，但没什么办法复原。

## 例子：批量转换格式和扩展名
[7]
```
old_extension=$1
new_extension=$2

for i in *."$old_extension";
  do ffmpeg -i "$i" "${i%.*}.$new_extension";
done
```

```
for f in *.avi; do ffmpeg -i "${f}" "${f%%.*}.mkv"; done
```

## 查询进程
### pid, ppid
当前进程（shell 进程）的pid：
```
echo $$
9186
```
获得 ppid：
```
ps -o ppid= $$ # 注意 `=` 后的空格。
   9175
```
获得整个 ppid 链条（注意最后一项总是 pstree 本身，应该忽略）：
```
pstree -s -p $$
systemd(1)───systemd(3762)───deepin-terminal(9175)───bash(9186)───pstree(14864)
```

## 子进程和并行执行
### 1. 使用后台进程符号 `&` 和 wait 命令
shell 中给命令结尾加上 `&` 可以让其在后台并行执行，用 `wait` 命令可以等待所有后台子进程结束。例如：
```
# 并行发送多个请求。
for n in 1 2 3 4 5 6 7 8 9 10
do
curl -i -X POST  "http://example.org/do?name=${n}" --data-binary "@../test/data/${n}.jpg" &
done
# 等待所有子进程完成
wait
```
优点：
1. 简单，无外部依赖。

存在的问题：
1. 不便于指定并发度。
2. 各个后台任务的标准输出可能混杂在一起。

### 2. 使用 xargs 程序

### 3. 使用 parallel 程序
注意 parallel 程序有两个，且参数和使用方式并不兼容：
* GNU parallel . 属于软件包 parallel ，功能比较复杂和完善。
* moreutils parallel . 属于软件包 moreutils ，功能较简单。
如果同时安装两者，会有冲突。ubuntu 上预装了 moreutils parallel ，如果再安装 GNU parallel ，则前者会被重命名为 /usr/bin/parallel.moreutils ，后者是 /usr/bin/parallel [11]。
