## 参考资料

[1.0] 多网卡负载均衡

[1.1] How does a socket know which network interface controller to use?
https://stackoverflow.com/questions/4297356/how-does-a-socket-know-which-network-interface-controller-to-use

[1.2] How to use different network interfaces for different processes?
https://superuser.com/questions/241178/how-to-use-different-network-interfaces-for-different-processes

[1.3] How to set the preferred network interface in linux
https://serverfault.com/questions/123553/how-to-set-the-preferred-network-interface-in-linux

[1.4] Binding applications to a specific IP
http://daniel-lange.com/archives/53-Binding-applications-to-a-specific-IP.html

[2.0] 链路聚合

[2.1] Link aggregation
https://en.wikipedia.org/wiki/Link_aggregation

[2.2] Linux Ethernet Bonding Driver HOWTO
https://www.kernel.org/doc/Documentation/networking/bonding.txt

[2.3] Linux bonding driver 
https://wiki.linuxfoundation.org/networking/bonding

[3.0] macvlan 网卡虚拟化

[3.1] 网卡虚拟化技术 macvlan 详解
https://blog.csdn.net/weixin_30764883/article/details/97172521

[3.2] Linux 虚拟网卡技术：Macvlan
https://cloud.tencent.com/developer/article/1495440

[4.0] ipvlan 网卡虚拟化
[4.1] linux 网络虚拟化： ipvlan
https://www.cnblogs.com/lovezbs/p/14609276.html

[4.2] 虚拟网络技术：ipvlan
https://blog.csdn.net/lsz137105/article/details/100596277

[5.0] 多网关负载均衡

[5.1] Can a PC have more than one Default Gateway?
https://serverfault.com/questions/347032/can-a-pc-have-more-than-one-default-gateway

[5.2] How does Linux choose between multiple default gateways?
https://unix.stackexchange.com/questions/337332/how-does-linux-choose-between-multiple-default-gateways

[5.3] Chapter 19. Managing the default gateway setting
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_networking/managing-the-default-gateway-setting_configuring-and-managing-networking

[5.4] Multiple default gateways can cause connectivity problems
https://learn.microsoft.com/en-us/troubleshoot/windows-server/networking/connectivity-issues-multiple-default-gateways

## 多网卡负载均衡

### 当前的本地路由配置

[1.3]

操作系统自动选择本地路由的依据是路由表、目标地址、优先级(Metric，数字越小优先级越高)。

获得本地路由表：
```
ip route list
default via 172.17.191.253 dev eth0 proto dhcp metric 100 
default via 172.17.191.253 dev eth1 proto dhcp metric 101 
172.17.176.0/20 dev eth0 proto kernel scope link src 172.17.178.27 metric 100 
172.17.176.0/20 dev eth1 proto kernel scope link src 172.17.191.219 metric 101
```

获得当前访问某个外部地址时使用的本地接口和网关：

```
ip route get 8.8.8.8

8.8.8.8 via 172.17.191.253 dev eth0 src 172.17.178.27 uid 0 
    cache 
```

### linux socket API

程序发起出站连接时，可以先调用 bind 再调用 connect ，bind 时指定本地 IP 地址，这样就选定了本地网卡。
如果省略 bind 调用，直接调用 connect ，操作系统将隐式调用 bind ，自动选择一个本地 IP 地址。
操作系统为隐式 bind 选择本地 IP 地址的依据一般是路由表条目的 metric 的大小，但如果多个 IP 的 metric 相等，可能会随机选择一个或固定选择一个。

### 多网卡路由 metric 周期性调整/随机化
为了让多网卡的负载变得均衡，可以用程序周期性地调整它们的 metric 的相对大小，使得新创建的 socket（隐式 bind）有机会使用任何一张网卡，或使用当前负载较低的网卡。
目前尚不知道有这样的程序。

### LD_PRELOAD 拦截 socket API

[1.3-1.4]
```
BIND_ADDR="ip_of_ethX" LD_PRELOAD=./bind.so twinkle
```

### network namespaces
参考 network_namespace.md 。

在多网卡机器上，将不同的网卡分配到不同的 network namespace 中，并在不同的 network namespace 中启动进程，则每个进程只能看到本 NNS 中的网卡。

需要注意的点：
* 一个网卡只能属于一个 NNS 。主机的默认链接外网的网卡一般不要移入 NNS ，否则会断网。这对云主机尤其重要。
* 移入 NNS 的网卡需要重新配置 IP 地址、默认网关等参数。

例子：

```
#create netns
ip netns add myNamespace
#link iface to netns
ip link set eth0 netns myNamespace
#set ip address in namespace
ip netns exec myNamespace ifconfig eth0 192.168.0.10/24 up
#set loopback (may be needed by process run in this namespace)
ip netns exec myNamespace ifconfig lo 127.0.0.1/8 up
#set route in namespace
ip netns exec myNamespace route add default gw 192.168.0.1
#force firefox to run inside namespace (using eth0 as outgoing interface and the route)
ip netns exec myNamespace firefox
```

## 单网卡多网关负载均衡
### 问题描述
客户机通过一张网卡一个IP地址（假定 192.168.8.125）连接到一个局域网（假定 192.168.8.0/24）中。此局域网中有2个网关（192.168.8.1，192.168.8.2），都可以与外网连接。问：如何让客户机与外网之间的流量均衡地通过这两个网关？

### Windows 系统

Windows 系统允许为一个网络连接设置多个默认网关。如果这些网关有不同的 metrics，则选择 metrics 较低的；如果这些网关 metrics 相同，则选择任意一个。如果 Windows 检测到某个默认网关不再可用了，则不再选择它。

因此，Windows 系统上要实现单网卡多网关负载均衡，可以设置多个默认网关，并周期性地调整 metrics 。

### Linux 系统

Linux 系统中，为一个网络连接可以设置最多一个默认网关。

因此，Linux 系统上要实现单网卡多网关负载均衡，需要周期性地修改默认网关。

## 链路聚合

[2.1 - 2.3]
链路聚合是将多个网卡或网络连接视为一个连接，提供合并的带宽或者提高可靠性。
聚合一般发生在第二层（数据链路层），但也可能发生在一、三层（物理层或网络层）。
但网络层的聚合还没有找到实例。

## 网卡虚拟化 macvlan

macvlan 这种技术能将 一块物理网卡虚拟成多块虚拟网卡 ，相当于物理网卡施展了 多重影分身之术 ，由一个变多个。

macvlan 可以用在有线网卡上，但一般不能用在无线网卡（WIFI）上，因为 802.11 协议不允许一个客户端拥有多个 mac 地址。

## IPvlan

和 macvlan 类似，都是从一个主机接口虚拟出多个虚拟网络接口，ipvlan虚拟出的子接口共有物理接口的mac地址，但可配置不同的ip地址。

由于所有的虚拟接口共享同个mac地址，因此有些地方需要注意：当使用DHCP协议分配ip时，一般会用mac地址作为机器的标识，因此需要配置唯一的ClientID字段作为机器的标识， DHCP server配置ip时需使用该字段作为机器标识，而不是使用mac地址。
