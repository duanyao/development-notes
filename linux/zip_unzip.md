## 参考资料

[1] zip命令分卷压缩和解压缩
https://www.cnblogs.com/haiyang21/p/11608406.html

## 分卷压缩/解压

[1]
创建分卷压缩文件

```
zip -s 100m -r folder.zip folder/

    -s: 创建分卷的大小
    -r: 循环压缩文件夹下面的内容
```
切分已有的文件：

```
zip existing.zip --out new.zip -s 50m
```

执行晚之后回创建下面一系列文件：

new.zip
new.z01
new.z02
new.z03

解压分卷文件

```
# 将分卷文件合并成一个单独的文件
zip -s 0 split.zip --out single.zip
# 解压文件
unzip single.zip
```

注意，有的资料说可以用 cat 命令将分卷的 zip 文件合并后再解压，实际上未必可行，可能无法解压。

`zip -s 0` 的用法在 zip 的帮助中没有说明，不知有何出处。
