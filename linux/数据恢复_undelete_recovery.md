## extundelete

```
mount -o remount,ro /dev/nvme0n1p4 # 将要恢复的分区改为只读状态
extundelete /dev/nvme0n1p4 --restore-directory rd/project/ -o /media/data2/restore/b # 恢复特定目录。-o 指定输出目录，应当在不同分区上，最好在不同的物理盘上。
extundelete /dev/nvme0n1p4 --restore-all -o /media/data2/restore/b # 恢复所有可能的文件目录
```
## ext4magic

```
mount -o remount,ro /dev/nvme0n1p4 # 将要恢复的分区改为只读状态

sync # 刷新写缓存

debugfs -R "dump <8> /media/data2/restore/JOURNAL.copy" /dev/nvme0n1p4 # 将要恢复的分区的 ext4 日志导出到文件 JOURNAL.copy ，应当在不同分区上，最好在不同的物理盘上。

mkdir /media/data2/restore/a

ext4magic /dev/nvme0n1p4 -m -d /media/data2/restore/a -j /media/data2/restore/JOURNAL.copy  # 恢复分区上被删除的所有文件。-j 指定刚才导出的 ext4 日志。-d 指定输出目录，应当在不同分区上，最好在不同的物理盘上。
#ext4magic /dev/nvme0n1p4 -M -d /media/data2/restore/a -j /media/data2/restore/JOURNAL.copy # 恢复分区上的所有文件（包括没有被删除的）
```

输出：

```
--------        /media/data2/restore/a/MAGIC-3/text/plain/0035941164.txt#
Hardlink Database : 9 entries
         1      /media/data2/restore/a/rd/ai-dataset-e/touch_2-5b17-19n_1_m5_test/images
         1      /media/data2/restore/a/rd/ai-dataset-e/cls_trt_17_m5_test/images
         1      /media/data2/restore/a/rd/ai-dataset-e/cls_touch_2b3b4b5b_1_m5_test/images
         1      /media/data2/restore/a/rd/ai-dataset-e/cls_touch_17-19n_1_m5_test/images
         1      /media/data2/restore/a/rd/ai-dataset-e/trt_16n17n18n19_m5_test/images
         1      /media/data2/restore/a/rd/ai-dataset-e/trt_19_m5_test/images
         1      /media/data2/restore/a/rd/ai-dataset-e/trt_18_m5_test/images
         1      /media/data2/restore/a/rd/ai-dataset-e/cls_touch_old_1_m5_test/images
         1      /media/data2/restore/a/rd/ai-dataset-e/touch_7b_2_m8_test/images
```

查看恢复结果：
```
root@gtrd:~# cd /media/data2/restore/a
root@gtrd:/media/data2/restore/a# ls -l
总用量 12
drwx------  6 root root 4096 4月  23 19:21 MAGIC-2
drwx------  9 root root 4096 4月  23 19:25 MAGIC-3
drwx------ 32 root root 4096 4月  23 21:24 rd
```
一般的文件
