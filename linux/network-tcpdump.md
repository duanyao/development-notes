[1] A tcpdump Tutorial and Primer with Examples
https://danielmiessler.com/study/tcpdump/#gs.RmlDTJ4

## 安装
包名 tcpdump 。

## Options
[1]
    -w capture_file 写入 pcap 文件
    -12078	16459	58734	58735	H-200	1342934	408270	http://bkds.oss-cn-hangzhou-internal.aliyuncs.com/docList/temp/f_1_0?sn=1467	Error: length mismatch; aborted by serverr capture_file 读 pcap 文件
    -i any : Listen on all interfaces just to see if you’re seeing any traffic.
    -i eth0 : Listen on the eth0 interface.
    -D : Show the list of available interfaces
    -n : Don’t resolve hostnames.
    -nn : Don’t resolve hostnames or port names.
    -q : Be less verbose (more quiet) with your output.
    -t : Give human-readable timestamp output.
    -tttt : Give maximally human-readable timestamp output.
    -X : Show the packet’s contents in both hex and ASCII.
    -XX : Same as -X, but also shows the ethernet header.
    -v, -vv, -vvv : Increase the amount of packet information you get back.
    -c : Only get x number of packets and then stop.
    -s : Define the snaplength (size) of the capture in bytes. Use -s0 to get everything, unless you are intentionally capturing less.
    -S : Print absolute sequence numbers.
    -e : Get the ethernet header as well.
    -q : Show less protocol information.
    -E : Decrypt IPSEC traffic by providing an encryption key.

[ The default snaplength as of tcpdump 4.0 has changed from 68 bytes to 96 bytes. While this will give you more of a packet to see, it still won’t get everything. Use -s 1514 or -s 0 to get full coverage ]

## 例子
Just see what’s going on, by looking at all interfaces.

  tcpdump -i any
  tcpdump -i eth0
  
  tcpdump -nnvXSs 0 -c1 icmp
  tcpdump host 1.2.3.4
  tcpdump src 2.3.4.5
  tcpdump dst 3.4.5.6
  
  tcpdump port 3389
  tcpdump src port 1025

  tcpdump port 80 -w capture_file
  tcpdump -r capture_file
  
  tcpdump -i any -s 150 host bkds.oss-cn-hangzhou-internal.aliyuncs.com -w bkds-internal.pcap
  tcpdump -i any -s 150 host bkds.oss.aliyuncs.com -w bkds-outer.pcap

  tcpdump src 10.0.2.4 and (dst port 3389 or 22)
  
## 捕获到文件
