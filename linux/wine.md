## 参考
[1.2] 2007-[翻译]Wine完全使用指南——从基本到高级
  http://forum.ubuntu.com.cn/viewtopic.php?f=121&t=72933

[1.5] http://linux-wiki.cn/wiki/zh-hans/Wine的中文显示与字体设置

[2.1] https://wiki.winehq.org/Debug_Channels

[2.2] 
https://askubuntu.com/questions/1103240/signature-verification-error-for-wine-index-files-failed-to-download-changin

[3.1]
https://wiki.winehq.org/Winetricks

[4.1]
http://vineyardproject.org/

[5.1]
https://lutris.net/

[5.2]
https://github.com/lutris/docs

[6.1] How To Find All Installed Fonts From Commandline In Linux
https://ostechnix.com/find-installed-fonts-commandline-linux/

[6.2] 让 wine 中文程序不依赖 simsun
https://forum.ubuntu.org.cn/viewtopic.php?t=37791

[6.3] Wine的中文显示与字体设置
http://linux-wiki.cn/wiki/Wine%E7%9A%84%E4%B8%AD%E6%96%87%E6%98%BE%E7%A4%BA%E4%B8%8E%E5%AD%97%E4%BD%93%E8%AE%BE%E7%BD%AE

[6.4] [PATCH] tools/wine.inf.in: Added UTF8 bom header. (resend)
https://www.winehq.org/pipermail/wine-devel/2012-November/097756.html

[7.1] copy/paste still sometimes fails in X11 with mutter 3.36.2
https://gitlab.gnome.org/GNOME/mutter/-/issues/1268

[7.2] [X11] copy/paste (clipboard) with LibreOffice is broken in Ubuntu 19.10 & 20.04 (see comment 93 and 176) 
https://bugs.launchpad.net/ubuntu/+source/mutter/+bug/1852183

[7.3] Wine on Ubuntu - Copy to clipboard needs 2 CTRL-Cs
https://askubuntu.com/questions/1200002/wine-on-ubuntu-copy-to-clipboard-needs-2-ctrl-cs

[10.1] MikuMikuDance
  https://appdb.winehq.org/objectManager.php?sClass=application&iId=13443

## 安装
### 本体
[1.5]

sudo dpkg --add-architecture i386

/etc/apt/wine.list:

deb https://dl.winehq.org/wine-builds/debian/ stretch main

wget -nc https://dl.winehq.org/wine-builds/Release.key
sudo apt-key add Release.key

wget -nc https://dl.winehq.org/wine-builds/winehq.key
sudo apt-key add winehq.key

sudo apt update

稳定分支 	

sudo apt install --install-recommends winehq-stable

开发分支 	

sudo apt install --install-recommends winehq-devel

ln -s /opt/wine-staging/bin/wine64 ~/bin/
ln -s /opt/wine-staging/bin/wine ~/bin/
ln -s /opt/wine-staging/bin/winecfg ~/bin/

## 创建和使用新的 prefix，运行64位程序

```
env WINEPREFIX=~/opt/wine64 wine64 xxx.exe
```

## 中文字体问题

### 验证 linux 自带的中文字体

使用 fc-list 命令列出系统里的中文字体：
[6.1]
```
fc-list :lang=zh-cn
```
`:lang=zh-cn` 表示过滤出支持简体中文的字体。如果不限于简体，可改为 `:lang=zh`。冒号（:）与 `lang=zh-cn` 之间不要有空格，否则不起作用。

结果大致如下：

```
/usr/share/fonts/opentype/noto/NotoSansCJK-Bold.ttc: Noto Sans CJK JP,Noto Sans CJK JP Bold:style=Bold,Regular
/usr/share/fonts/opentype/noto/NotoSerifCJK-Bold.ttc: Noto Serif CJK SC:style=Bold
/usr/share/fonts/opentype/noto/NotoSerifCJK-Bold.ttc: Noto Serif CJK TC:style=Bold
/usr/share/fonts/opentype/noto/NotoSerifCJK-Bold.ttc: Noto Serif CJK JP:style=Bold
/usr/share/fonts/opentype/noto/NotoSerifCJK-Bold.ttc: Noto Serif CJK KR:style=Bold
/usr/share/fonts/opentype/noto/NotoSansCJK-Bold.ttc: Noto Sans Mono CJK KR,Noto Sans Mono CJK KR Bold:style=Bold,Regular
/usr/share/fonts/truetype/wqy/wqy-microhei.ttc: 文泉驿微米黑,文泉驛微米黑,WenQuanYi Micro Hei:style=Regular
/usr/share/fonts/opentype/noto/NotoSansCJK-Bold.ttc: Noto Sans Mono CJK JP,Noto Sans Mono CJK JP Bold:style=Bold,Regular
/usr/share/fonts/opentype/noto/NotoSansCJK-Regular.ttc: Noto Sans CJK JP,Noto Sans CJK JP Regular:style=Regular
/usr/share/fonts/truetype/wqy/wqy-zenhei.ttc: 文泉驿正黑,文泉驛正黑,WenQuanYi Zen Hei:style=Regular
/usr/share/fonts/truetype/wqy/wqy-zenhei.ttc: 文泉驿点阵正黑,文泉驛點陣正黑,WenQuanYi Zen Hei Sharp:style=Regular
```

### 验证 wine 自带的程序能否使用中文字体

启动 wine 自带的 winecfg、notepad 等程序，看中文是否显示正常:
```
wine winecfg
wine notepad
```
winecfg “显示” 页面“屏幕分辨率”中可能有中文显示为方框。这在“修改 wine 的字体取代方案”的 Tahoma 字体后可以解决。
notepad 中输入中文后，可能显示为方框。这时可以打开“编辑-字体”，选择一个用 `fc-list :lang=zh-cn` 查出来的中文字体，看显示是否正常，一般来说可以正常显示。

### 验证第三方程序能否使用中文字体

用 wine 启动你希望验证的第三方程序，看能否显示中文。有的程序可能需要在“修改 wine 的字体取代方案”后重装才能正确显示中文。

### 修改 wine 的字体取代方案
```
wine regedit
```
查看 `HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion\FontLink\SystemLink` 的内容，初始内容大致如下：

```
"Lucida Sans Unicode"=
    SIMSUN.TTC,SimSun
    MINGLIU.TTC,PMingLiu
    MSGOTHIC.TTC,MS UI Gothic
    BATANG.TTC,Batang

"Microsoft Sans Serif"=
    SIMSUN.TTC,SimSun
    MINGLIU.TTC,PMingLiu
    MSGOTHIC.TTC,MS UI Gothic
    BATANG.TTC,Batang

"Tahoma"=
    SIMSUN.TTC,SimSun
    MINGLIU.TTC,PMingLiu
    MSGOTHIC.TTC,MS UI Gothic
    BATANG.TTC,Batang
```

每一项改为 `wqy-microhei.ttc` (不要写成 `wqy-microhei.ttc,WenQuanYi Micro Hei`，不起作用) [6.3]，或者其它 fc-list 列出的中文字体。
同时，添加若干项类型为 REG_Multi_SZ，如

```
Arial
Arial Black
Lucida Sans Unicode
Microsoft Sans Serif
Microsoft Yahei
MS Sans Serif
NSimSun
SimHei
SimSun
Tahoma Bold
微软雅黑
```
等，并且把默认值也设置，以求尽可能大的覆盖范围（windows应用使用了不在上述范围内的字体名，就仍然会显示不出汉字，而是显示方框）。

其它可能需要替换的字体名：

```
+HKCU,Software\Wine\Fonts\Replacements,"SimSun",,"WenQuanYi Micro Hei"
+HKCU,Software\Wine\Fonts\Replacements,"宋体",,"WenQuanYi Micro Hei"
+HKCU,Software\Wine\Fonts\Replacements,"PMingLiU",,"WenQuanYi Micro Hei"
+HKCU,Software\Wine\Fonts\Replacements,"新細明體",,"WenQuanYi Micro Hei"
+HKCU,Software\Wine\Fonts\Replacements,"MS Gothic",,"Ume Gothic"
+HKCU,Software\Wine\Fonts\Replacements,"MS PGothic",,"Ume P Gothic"
+HKCU,Software\Wine\Fonts\Replacements,"ＭＳＰゴシック",,"Ume P Gothic"
+HKCU,Software\Wine\Fonts\Replacements,"MS UI Gothic",,"Ume UI Gothic"
+HKCU,Software\Wine\Fonts\Replacements,"MS Mincho",,"Ume Mincho"
+HKCU,Software\Wine\Fonts\Replacements,"MS PMincho",,"Ume P Mincho"
+HKCU,Software\Wine\Fonts\Replacements,"Batang",,"UnBatang"
+HKCU,Software\Wine\Fonts\Replacements,"Dotum",,"UnDotum"
+HKCU,Software\Wine\Fonts\Replacements,"Gulim",,"UnDotum"
+HKCU,Software\Wine\Fonts\Replacements,"Arial Unicode MS",,"Droid Sans Fallback"
```

有些资料说修改 `HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion\FontSubstitutes` 以及 `HKEY_CURRENT_USER\Software\\Wine\\Fonts\\Replacements` 也可能起作用[6.2, 6.3]。

## 显示比例问题

使用高分辨率屏幕时，wine程序的显示比例会显得过小，且不遵从宿主机的显示比例设置。
可以启动 winecfg（命令 winecfg），点击“显示”，将“屏幕分辨率”调高（默认96dpi）。这不会立即生效，要将wine程序关闭再打开，才能看到效果，包括 winecfg 本身。

## wine 辅助工具

### winetricks
安装：

```
sudo apt install zenity cabextract # zenity 是 GUI 部分

wget  https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
chmod +x winetricks 
sudo mv ./winetricks /usr/bin/  # debian 系统自带的 winetricks 很老，但有些包又依赖它，所以要替换掉
```

运行：
```
winetricks -f ie8  # -f 是强制安装，可用于更新 winetricks/wine 后的重新安装
```

缓存目录：

~/.cache/winetricks

winetricks 要尽量用最新版，否则可能跟不上 wine 的更新，出现兼容性问题。

### Wine运行器
https://gitee.com/gfdgd-xi/deep-wine-runner
https://gfdgd-xi.github.io

包名 spark-deepin-wine-runner

### playonliux

wget http://deb.playonlinux.com/playonlinux_stretch.list
sudo cp playonlinux_stretch.list /etc/apt/sources.list.d/
rm playonlinux_stretch.list
wget -q "http://deb.playonlinux.com/public.gpg"
sudo apt-key add public.gpg
rm public.gpg

sudo apt-get install playonlinux

### Lutris

```
echo "deb http://download.opensuse.org/repositories/home:/strycore/Debian_10/ ./" | sudo tee /etc/apt/sources.list.d/lutris.list
wget -q https://download.opensuse.org/repositories/home:/strycore/Debian_10/Release.key -O- | sudo apt-key add -
sudo apt-get update
sudo apt-get install lutris
```

如果因为 python3-evdev 依赖的 python3 版本不对而无法安装，则忽略版本依赖安装：

```
apt download python3-evdev
sudo dpkg --ignore-depends=python3 -i python3-evdev_0.8.1+dfsg-1_amd64.deb
```

修改 `/var/lib/dpkg/status` 的 python3-evdev 条目，使其符合现状，然后重新运行 `sudo apt-get install lutris` 。

sudo apt install mesa-vulkan-drivers mesa-vulkan-drivers:i386

### q4wine

### Vineyard
```
sudo sh -c 'add-apt-repository ppa:cybolic/vineyard-testing'
sudo apt update
sudo apt install vineyard
```
如果第一步出现 gpg: key 150BA781A5FF2BFC ... gpg: no valid OpenPGP data found 错误，则从别处下载：

```
sudo apt-key adv --keyserver hkp://pool.sks-keyservers.net:80 --recv-keys 150BA781A5FF2BFC
```

在开始菜单中的图标名为“windows applications”，输入 wine 或者 vine 也能搜索到。

从源码安装：

git clone https://github.com/Cybolic/vineyard.git
cd vineyard
./vineyard-preferences

Vineyard 创建的 prefix 在 /home/duanyao/.local/share/wineprefixes 下。可以用符号链接：

```
duanyao@duanyao-laptop-c:~$ mkdir -p /home/duanyao/.local/share/wineprefixes
duanyao@duanyao-laptop-c:~$ cd /home/duanyao/.local/share/wineprefixes
duanyao@duanyao-laptop-c:~/.local/share/wineprefixes$ ln -s ../../../opt/wine32_1 wine32_1
duanyao@duanyao-laptop-c:~/.local/share/wineprefixes$ ln -s ../../../opt/wine64_1 wine64_1
```

Vineyard 创建的快捷方式在 `<wineprefix>vineyard/applications` 下。目前无法通过 Vineyard 界面删除快捷方式，可以直接从这个目录删除。

## 剪贴板问题

从 wine 程序到 X 程序的双向复制粘贴会随机失败，如粘贴内容是上上次复制的，或者 wine 程序和 X 程序好像有两套独立的剪贴板，各自粘贴的内容不同。
这个问题不一定是跟 wine 相关，因为 X 程序之间、wine 程序之间的复制粘贴，也会随机失败，只是 wine 程序参与时概率似乎更高。
这个问题在 ubuntu 20.04 上出现过，被认为是 mutter 的问题，已经修复。
在 deepin 20.8 ～ 20.9 上也出现，原因不明。概率较高的程序为：regedit（wine）、钉钉（wine）、WPS、钉钉。

## 安装其它 windows 组件

### .net framework
不要直接用 .net framework 的安装包，装上也很可能不能用。要用 winetricks:

```
winetricks dotnet40
```

dotnet40 安装的实际上是 .net 4 client profile 。

4.5.2 (dotnet452) 似乎装上也不能用。

装完后，运行 .net 的方法与运行普通 exe 的方法并无不同。

### IE
Vineyard 可以安装 IE 6-7

winetricks 可安装 ie8，只适用于 32 位 prefix，命令：

```
winetricks -f ie8
```
如果下载出错，建议翻墙后，设置 http_proxy 和 https_proxy 环境变量后再尝试。

winetricks 需要升级到最新，否则可能遇到错误。

ie8 可以运行，但是很多网页无法显示，比如 baidu.com ，或者无法点击，如 bing.com，或者导致崩溃，如 http://www.html5test.com。
https 地址似乎都不能显示，而是白屏。
ie的菜单、右键菜单也不显示。

### directx
wine 自己有 directx 的实现，但也可以安装微软的来替代它：

winetricks d3dx9

## 调试输出
[11]
在命令行里执行：

  WINEDEBUG=warn wine XXX.exe
或者
  WINEDEBUG=warn+all wine XXX.exe
  
  WINEDEBUG=trace+font wine XXX.exe
  
  env WINEDEBUG=+font wine XXX.exe

看看有什么错误。这可能并不是很有用，因为产生的日志量太大，很难分析。

WINEDEBUG=[class][+/-]channel[,[class2][+/-]channel2]

class is optional and can be one of the following: err, warn, fixme, or trace. If class is not specified, all debugging messages for the specified channel are turned on. Each channel will print messages about a particular component of Wine. The following character can be either + or - to switch the specified channel on or off respectively. If there is no class part before it, a leading + can be omitted. Note that __spaces are not allowed any where in the string__. 


## 特定程序的调整

### MikuMikuDance
* 问题：单窗口模式下，部分界面呈现黑色，无法看到
  绕开：进入多窗口模式：显示-副窗口。

