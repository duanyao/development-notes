## 资料
[1.1] What are apt-get Command Options in Linux a Guide for Beginners
  https://www.cyberpratibha.com/apt-get-command-in-linux/

[1.5] What is /etc/apt/preferences ?
  https://difyel.com/linux/etc/apt/preferences/
  
[2.1] 从 apt 升级中排除/保留/阻止特定软件包的三种方法
  https://linux.cn/article-11992-1.html

[3.1] How To Show A History Of Installed, Upgraded Or Removed Packages In Debian, Ubuntu or Linux Mint [dpkg]
https://www.linuxuprising.com/2019/01/how-to-show-history-of-installed.html

[1] How to display package dependencies
https://blog.sleeplessbeastie.eu/2018/07/02/how-to-display-package-dependencies/

[2] How to Fix Unmet Dependencies Error on Ubuntu
https://appuals.com/fix-unmet-dependencies-error-ubuntu/

[3] Fix Missing GPG Key Apt Repository Errors (NO_PUBKEY)
https://www.linuxuprising.com/2019/06/fix-missing-gpg-key-apt-repository.html

[4] debian软件源source.list文件格式说明 
https://blog.zscself.com/posts/3af88213/

[5] debian Simple Backport Creation
https://wiki.debian.org/SimpleBackportCreation

[10.1] Debian Version Numbers
https://readme.phys.ethz.ch/documentation/debian_version_numbers/

[10.2] How to compare Debian package versions?
https://stackoverflow.com/questions/4957514/how-to-compare-debian-package-versions

[11.1] Easily unpack DEB, edit postinst, and repack DEB
https://unix.stackexchange.com/questions/138188/easily-unpack-deb-edit-postinst-and-repack-deb

[11.2] HowTo: See Contents of A .DEB Debian / Ubuntu Package File
https://www.cyberciti.biz/faq/view-contents-of-deb-file/

[11.3] How to get information about deb package archive?
https://unix.stackexchange.com/questions/187221/how-to-get-information-about-deb-package-archive

[11.4] Inspecting and extracting Debian package contents
https://blog.packagecloud.io/eng/2015/10/13/inspect-extract-contents-debian-packages/

[12.1] Debian 打包入门 https://linux.cn/article-9878-1.html

[12.2] 5. Control files and their fields
  https://www.debian.org/doc/debian-policy/ch-controlfields.html#package-interrelationship-fields-depends-pre-depends-recommends-suggests-breaks-conflicts-provides-replaces-enhances
  
[12.3] 7. Declaring relationships between packages
  https://www.debian.org/doc/debian-policy/ch-relationships.html

[12.4] Chapter 15. Creating a Debian Package https://debian-handbook.info/browse/stable/debian-packaging.html

[13.1] 转换rpm包为deb包的简单方法 https://m.linuxidc.com/Linux/2007-05/4250.htm

[14.1] How to create a .deb file from installed package?
https://askubuntu.com/questions/592551/how-to-create-a-deb-file-from-installed-package

[14.2] dpkg-repack - put an unpacked .deb file back together
https://manpages.ubuntu.com/manpages/lunar/en/man1/dpkg-repack.1.html

[15.1] Everything you need to know about conffiles: configuration files managed by dpkg
https://raphaelhertzog.com/2010/09/21/debian-conffile-configuration-file-managed-by-dpkg/

[16.1] How to display reverse package dependencies
https://sleeplessbeastie.eu/2018/09/26/how-to-display-reverse-package-dependencies/

[16.2] How to list dependent packages (reverse dependencies)?
https://askubuntu.com/questions/128524/how-to-list-dependent-packages-reverse-dependencies

## 升级

```
sudo apt update     # 更新仓库元数据
sudo apt upgrade    # 升级所有可升级的包。-y: 不再询问确认
sudo apt dist-upgrade # 升级发行版
```

## 列出

列出标记为手动安装的：

```
apt-mark showmanual | sort -u

apt list --manual-installed
apt list --manual-installed | sed 's/\// /' | awk '{print $1 "=" $3}' 
aptitude search '~i!~M'
```

标记为手动安装，包括用户手动安装的，加上系统预装的包。

列出用户手动安装的：
```
zcat /var/log/apt/history.log.*.gz  2>/dev/null | cat - /var/log/apt/history.log | grep -Po '^Commandline: apt(?:-get)? install (?!.*--reinstall)\K.*'
```
缺点：不能找出以 /usr/bin/apt 开头的命令。

或者：
```
zgrep -h ' install ' /var/log/dpkg.log* | sort | awk '{print $4}'
```

列出用户手动安装的，假设预装的包的列表位于 /var/log/installer/initial-status.gz  （早期 ubuntu 是这样，23.04 不适用），可以：
```
comm -23 <(apt-mark showmanual | sort -u) <(gzip -dc /var/log/installer/initial-status.gz | sed -n 's/^Package: //p' | sort -u)
```

列出系统预装的包：
```
zcat /var/log/apt/history.log.*.gz  2>/dev/null | cat - /var/log/apt/history.log | grep -Po '^Commandline: apt(?:-get)? install (?!.*--reinstall)\K.*' | sort -u > apt-by-user.1.list

apt-mark showmanual | sort -u > apt-manual.1.list

comm -23 <(cat apt-manual.1.list) <(cat apt-by-user.1.list) > apt-manual-pre-ubuntu2304.list
```

## 仓库源配置文件
以下文件配置仓库源
```
/etc/apt/sources.list
/etc/apt/sources.list.d/*.list
```
sources.list.d 下的文件如果不是以 `.list` 结尾，则会被忽略。非操作系统自带的源一般可以写到这里。
一些 deb 软件包也会把自己的源配置文件放到这里，以便以后能自动更新。

## 源的格式和仓库 url 的格式

源的格式为
```
deb [by-hash=force] http://packages.deepin.com/deepin lion main contrib non-free
```
则仓库的布局为：

```
http://packages.deepin.com/deepin
    dists/
        lion/
            InRelease
            Release
            Release.gpg
            main/
                Contents-amd64.bz2
                binary-amd64/
                  Packages.gz
                  Release 
            contrib/
            non-free/
```

源的格式为

```
deb http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64 /
```
则仓库的布局为：

```
http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/
    Packages.gz
    Release 
    Release.gpg
```
## 仓库优先级的配置
在以下文件配置
/etc/apt/preferences
/etc/apt/preferences.d/* # 除了.bk

建议将软件包的和自定义的优先级都放到后者里。

## 配置文件的控制：conffiles
[15.1]
如果一个 deb 包含有配置文件（安装到 /etc/ 下），deb 包可以将它们标记为“配置文件”（在 DEBIAN/conffiles 文件中），则卸载、升级、重装的行为都会不同：
* 卸载 deb 包时一般不会删除配置文件。
* 如果用户手动删除了配置文件，重装/升级 deb 包时一般不会再次安装它们。
* 如果用户手动修改了配置文件，重装/升级 deb 包时会询问如何处理（保留手动修改的、替换为deb 包里的）。

相关命令行参数：
```
dpkg --status bash

Conffiles:
 /etc/bash.bashrc 89269e1298235f1b12b4c16e4065ad0d
 /etc/skel/.bash_logout 22bfb8c1dd94b5f3813a2b25da67463f
 /etc/skel/.bashrc ee35a240758f374832e809ae0ea4883a
 /etc/skel/.profile f4e81ade7d6f9fb342541152d08e7a97
```

dpkg -i 的参数

--force-confold: do not modify the current configuration file, the new version is installed with a .dpkg-dist suffix. With this option alone, even configuration files that you have not modified are left untouched. You need to combine it with --force-confdef to let dpkg overwrite configuration files that you have not modified.ls
--force-confnew: always install the new version of the configuration file, the current version is kept in a file with the .dpkg-old suffix.
--force-confdef: ask dpkg to decide alone when it can and prompt otherwise. This is the default behavior of dpkg and this option is mainly useful in combination with --force-confold.
--force-confmiss: ask dpkg to install the configuration file if it’s currently missing (for example because you have removed the file by mistake).
--force-confask: 总是询问。

apt 的参数：
```
sudo apt-get -o Dpkg::Options::="--force-confmiss" install --reinstall dde-session
sudo apt-get -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" dist-upgrade
```

## 同时加入多个平行的源
以 deepin 为例，lion 和 panda 分别是操作系统的稳定和不稳定源。
修改 /etc/apt/sources.list ，增加“panda”一行:

```
deb [by-hash=force] http://packages.deepin.com/deepin lion main contrib non-free   # 稳定版
deb [by-hash=force] http://packages.deepin.com/deepin panda main contrib non-free  # 不稳定版
```

修改 /etc/apt/preferences 或者增加  /etc/apt/preferences.d/xxx.pref 文件（参考[1.5]），增加
```
# based on debian unstable
Package: *
Pin: release panda
Pin-Priority: 100

# based on debian stable
Package: *
Pin: release lion
Pin-Priority: 500

# nvidia cuda
Package: *
Pin: origin developer.download.nvidia.*
Pin-Priority: 400
```

origin 用来指定仓库的 url 中的 hostname 部分，可以用通配符。`origin ""` 表示本地仓库。

### debian/ubuntu 的平行源

```
Debian 8 "Jessie" -3487
Debian 9 "Stretch" -2704
Debian 10 "Buster" -1955
Debian 11 "Bullseye" -1185
Debian 12 "Bookworm" -520
Debian 13 "Trixie" (unknown)
Debian 14 "Forky" (unknown)
Debian  "Sid" (unknown)

Ubuntu 18.04 LTS "Bionic Beaver" -2391
Ubuntu 20.04 LTS "Focal Fossa" -1663
Ubuntu 22.04 LTS "Jammy Jellyfish" -935
Ubuntu 24.04 LTS "Noble Numbat" -200
```

```
echo "deb https://mirrors.tuna.tsinghua.edu.cn/debian/ buster main contrib non-free
" | sudo tee /etc/apt/sources.list.d/debian-10.list

echo "Package: *
Pin: release buster
Pin-Priority: 99
" | sudo tee /etc/apt/preferences.d/debian-10.pref

# 注意 debian 9 已经停止支持，此源已经不存在
echo "deb http://deb.debian.org/debian/ stretch main contrib non-free
" | sudo tee /etc/apt/sources.list.d/debian-9.list

echo "Package: *
Pin: release stretch
Pin-Priority: 90
" | sudo tee /etc/apt/preferences.d/debian-9.pref

# 以下为 ubuntu 的源
echo "deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic main universe multiverse restricted
" | sudo tee /etc/apt/sources.list.d/ubuntu-1804.list

echo "Package: *
Pin: release bionic
Pin-Priority: 95
" | sudo tee /etc/apt/preferences.d/ubuntu-1804.pref

echo "deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal main universe multiverse restricted
" | sudo tee /etc/apt/sources.list.d/ubuntu-2004.list

echo "Package: *
Pin: release focal
Pin-Priority: 115
" | sudo tee /etc/apt/preferences.d/ubuntu-2004.pref
```

要在非 ubuntu 的系统上使用 ubuntu 源，需要先安装 keyring 包：

```
curl -fsSL -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:127.0) Gecko/20100101 Firefox/127.0' https://mirrors.tuna.tsinghua.edu.cn/ubuntu/pool/main/u/ubuntu-keyring/ubuntu-keyring_2023.11.28.1_all.deb -o ~/ubuntu-keyring.deb
sudo dpkg -i ~/ubuntu-keyring.deb

sudo cp /usr/share/keyrings/ubuntu-archive-keyring.gpg /etc/apt/trusted.gpg.d/
sudo cp /usr/share/keyrings/ubuntu-master-keyring.gpg /etc/apt/trusted.gpg.d/
```

/usr/share/keyrings/ubuntu-{archive|master}-keyring.gpg 是 ubuntu-keyring 的一部分，但需要复制到 /etc/apt/trusted.gpg.d/ 目录中才对 apt 生效。
ubuntu-keyring 的最新版本的 url 可能之后有更新，可以在其目录中浏览来确定。

curl命令的选项解释：

-f 或 -fail：服务器返回失败的HTTP状态码时，不输出HTML错误页面，直接退出。
-s 或 --silent：静默模式，不显示进度条和错误信息。
-S 或 --show-error：当curl因为HTTP传输而失败时，显示错误。
-L 或 --location：如果服务器报告该页面已永久移动到新位置（HTTP 301/302响应），则让curl重新发起请求到新的位置。

-H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:127.0) Gecko/20100101 Firefox/127.0'：这个选项用于设置HTTP请求的头部信息，这里特别设置了User-Agent，模拟了一个Firefox浏览器的请求。有些服务器会根据User-Agent返回不同的内容，例如，某些网站可能会阻止非浏览器的请求。


## 从特定源安装、安装特定版本
```
sudo apt install -t panda gnome-keyring
#或者
sudo apt install gnome-keyring=3.28.2-1
```

`-t panda` 指定了搜索的源。
注意，后一种形式中，因为没有指定源，apt 只会试图从优先级最高的源中获取依赖的包，所以可能出现依赖无法满足的问题。

## 列出一个包所有的版本
```
apt list -a libcudnn7

libcudnn7/未知 7.3.0.29-1+cuda10.0 amd64
libcudnn7/未知 7.3.0.29-1+cuda9.0 amd64
libcudnn7/未知 7.2.1.38-1+cuda9.2 amd64
libcudnn7/未知 7.2.1.38-1+cuda9.0 amd64
libcudnn7/未知,now 7.2.1.38-1+cuda8.0 amd64 [已安装，可升级至：7.6.5.32-1+cuda10.2]
```

## 显示包的信息
```
apt show libhdf5-serial-dev

Package: libhdf5-serial-dev
Version: 1.10.0-patch1+docs-3+deb9u1
Priority: optional
Section: libdevel
Source: hdf5
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Installed-Size: 37.9 kB
Depends: libhdf5-dev
Homepage: http://hdfgroup.org/HDF5/
Tag: devel::lang:c, devel::library, field::physics, implemented-in::c,
 role::devel-lib, role::documentation, use::storing
Download-Size: 27.7 kB
APT-Manual-Installed: yes
APT-Sources: http://packages.deepin.com/deepin lion/main amd64 Packages
Description: transitional dummy package
 This package is a transitionnal package from libhdf5-serial-dev to
 libhdf5-dev. It can safely be removed.
```

## 简单形式的源
```
deb http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64 /
```
存在文件 https://developer.download.nvidia.cn/compute/machine-learning/repos/ubuntu1604/x86_64/Release

## 搜索文件所属的包
```
dpkg -S filename # 已经安装的包

apt-file update
apt-file search filename # 未安装的包
```

## 列出包含有的文件
```
dpkg -L packagename # 已安装的包
apt-file list packagename # 未安装的包
dpkg-deb -c package.deb # 本地deb文件
```

## 验证一个包安装后是否有文件被修改
```
dpkg --verify openssh-server # 或者 dpkg -V
```

## 搜索包名和描述
```
apt search xxx    #在包名或描述中搜索
apt search -n xxx #在包名中搜索
```

搜索的关键词可以是正则表达式。

apt search 总是返回已安装和未安装的包。如果只需要已安装的：

```
apt search -n xxx | grep '已安装'
apt list --installed | grep xxx
```

## 更改 apt 的输出的语言
默认语言与 locale 一致。要临时更改，可以设置 LC_ALL (设置 LANG 对 apt-get 无效):

```
export LC_ALL=C  # 英语输出
unset LC_ALL     # 退回默认语言
```

或者
```
LC_ALL=C sudo apt ...
```

## 查看递归依赖
[16.1]
```
sudo apt-get install apt-rdepends
apt-rdepends --print-state --state-follow=none tmux
```
## 查看反向依赖
[16.2]
```
apt rdepends tmux
apt rdepends --installed # 仅列出已经安装的
```

`apt-cache rdepends` 也有类似的效果，但 `apt rdepends` 输出更详细，包含版本号。

[16.1]
```
sudo apt-get install apt-rdepends
apt-rdepends --reverse --print-state --state-follow=none tmux
```


## 虚假的依赖错误
apt install 可能因为一些虚假的依赖错误（实际上没有错误）而无法安装特定软件包。这时可以试试 aptitude：

```
$ sudo apt-get install -t panda --no-install-recommends --dry-run nautilus
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
有一些软件包无法被安装。如果您用的是 unstable 发行版，这也许是
因为系统无法达到您要求的状态造成的。该版本中可能会有一些您需要的软件
包尚未被创建或是它们已被从新到(Incoming)目录移出。
下列信息可能会对解决问题有所帮助：

下列软件包有未满足的依赖关系：
 libkf5coreaddons5 : 破坏: libkf5auth5 (< 5.46) 但是 5.28.0-2 正要被安装
                     破坏: libkf5globalaccel-bin (< 5.46) 但是 5.28.0.1-1+comsta 正要被安装
 libkf5crash5 : 破坏: libkf5globalaccel-bin (< 5.46) 但是 5.28.0.1-1+comsta 正要被安装
E: 错误，pkgProblemResolver::Resolve 发生故障，这可能是有软件包被要求保持现状的缘故。


sudo aptitude install -t panda -s nautilus

下列“新”软件包将被安装。         
  bubblewrap{a} libgnome-desktop-3-17{a} libtracker-sparql-2.0-0{a} libunistring2{a} 
下列软件包将被“删除”：
  libgnome-autoar-common{u} 
下列软件包将被升级：
  gnome-desktop3-data gsettings-desktop-schemas libglib2.0-0 libgnome-autoar-0-0 libnautilus-extension1a libsqlite3-0 nautilus nautilus-data 
8 个软件包被升级，新安装 4 个，1 个将被删除， 同时 1532 个将不升级。
需要获取 8,533 kB 的存档。解包后将要使用 4,451 kB。
下列软件包存在未满足的依赖关系：
 libglib2.0-bin : 依赖: libglib2.0-0 (= 2.50.3-2) but 2.56.1-2 is to be installed
 libglib2.0-dev : 依赖: libglib2.0-0 (= 2.50.3-2) but 2.56.1-2 is to be installed
下列动作将解决这些依赖关系：

      删除 下列软件包：                                          
1)      libatk-bridge2.0-dev [2.22.0-2 (lion, now)]              
2)      libatk1.0-dev [2.22.0-1 (lion, now)]                     
3)      libatspi2.0-dev [2.22.0-6+deb9u1 (lion, now)]            
4)      libavahi-glib-dev [0.6.32-2 (lion, now)]                 
5)      libcairo2-dev [1.14.8-1 (lion, now)]                     
6)      libgck-1-dev [3.28.0-2deepin (now, panda)]               
7)      libgcr-3-dev [3.28.0-2deepin (now, panda)]               
8)      libgdk-pixbuf2.0-dev [2.36.5-2+deb9u2 (lion, now)]       
9)      libglib2.0-dev [2.50.3-2 (lion, now)]                    
10)     libgtk-3-dev [3.22.11-1deepin (lion, now)]               
11)     libharfbuzz-dev [1.4.2-1 (lion, now)]                    
12)     libimobiledevice-dev [1.2.0+dfsg-3.1 (lion, now)]        
13)     libpango1.0-dev [1.40.5-1 (lion, now)]                   
14)     libsecret-1-dev [0.18.5-3.1 (lion, now)]                 
15)     libsoup2.4-dev [2.56.0-2+deb9u2 (lion, now)]             
16)     libspice-client-gtk-3.0-dev [0.33-3.3+deb9u1 (lion, now)]
17)     libwebkitgtk-3.0-dev [2.4.11-3 (lion, now, panda)]       
18)     valac [0.40.4-1 (now, panda)]                            

      升级 下列软件包：                                          
19)     libglib2.0-bin [2.50.3-2 (lion, now) -> 2.56.1-2 (panda)]

      Leave the following dependencies unresolved:               
20)     dde 推荐 libimobiledevice-dev  
```
## recommends 和 suggests
apt 默认会安装 Recommend，但不会安装 Suggest 。检查默认配置：

```
$ apt-config dump | grep Recommend
APT::Install-Recommends "true";
$ apt-config dump | grep Suggest
APT::Install-Suggests "0";
```

用命令
```
apt-get --no-install-recommends <package-name>
apt-get --install-recommends <package-name>
```
可以避免安装或安装 recommends 。
要改变默认行为，创建文件 `/etc/apt/apt.conf.d/zz01-norecommend`，内容
```
APT::Install-Recommends "0";
APT::Install-Suggests "0";
```

注意，`/etc/apt/apt.conf.d/` 里面可能有优先级更高的文件，会覆盖优先级低的文件（按文件名升序）。

## apt 仓库签名
列出已经安装的仓库签名：

```
apt-key list

Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).
/etc/apt/trusted.gpg
--------------------
pub   rsa4096 2017-02-22 [SCEA]
      9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88
uid             [ 未知 ] Docker Release (CE deb) <docker@docker.com>
sub   rsa4096 2017-02-22 [S]

/etc/apt/trusted.gpg.d/debian-archive-bullseye-automatic.gpg
------------------------------------------------------------
pub   rsa4096 2021-01-17 [SC] [有效至：2029-01-15]
      1F89 983E 0081 FDE0 18F3  CC96 73A4 F27B 8DD4 7936
uid             [ 未知 ] Debian Archive Automatic Signing Key (11/bullseye) <ftpmaster@debian.org>
sub   rsa4096 2021-01-17 [S] [有效至：2029-01-15]

/etc/apt/trusted.gpg.d/debian-archive-bullseye-security-automatic.gpg
---------------------------------------------------------------------
pub   rsa4096 2021-01-17 [SC] [有效至：2029-01-15]
      AC53 0D52 0F2F 3269 F5E9  8313 A484 4904 4AAD 5C5D
uid             [ 未知 ] Debian Security Archive Automatic Signing Key (11/bullseye) <ftpmaster@debian.org>
sub   rsa4096 2021-01-17 [S] [有效至：2029-01-15]
```

将签名存储在 /etc/apt/trusted.gpg 是被抛弃的做法，一般应该存储在 /etc/apt/trusted.gpg.d/ 中。此位置存储的gpg文件是全局的，也就是说任何软件源都可以使用，无需用 signed-by 显式列出。

另有一种习惯是将签名存储在 /usr/share/keyrings/ 目录下，但这无法被 apt-key list 列出，且在 apt 源中需要用`signed-by`显式列出。
例如，`/etc/apt/sources.list.d/nvidia-container-toolkit.list` 的内容为：
```
deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://nvidia.github.io/libnvidia-container/stable/deb/$(ARCH) /
```

可以将 /etc/apt/trusted.gpg 中的签名删除：
```
sudo apt-key --keyring /etc/apt/trusted.gpg del 0EBFCD88
```
0EBFCD88 是 `apt-key list` 列出的公钥指纹的最后8位。

从网络上下载和安装签名的例子：
```
curl -fsSL https://mirrors.ustc.edu.cn/docker-ce/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```

### debian-archive-keyring
这个包包含所有 Debian 仓库的 GPG 签名。不过，不同的发行版可能对此包做修改，例如 ubuntu 22.04 的 debian-archive-keyring 去掉了 /etc/apt/trusted.gpg.d/ 中的 gpg 文件，只保留了 /usr/share/keyrings/ 中的 gpg 文件。这意味着无法全局使用该签名，可以这样补救：
```
cp /usr/share/keyrings/debian-archive*.gpg /etc/apt/trusted.gpg.d/
```

## 无交互模式
Stop interactive prompts from apt-get
https://unix.stackexchange.com/questions/314279/stop-interactive-prompts-from-apt-get
```
DEBIAN_FRONTEND=noninteractive apt-get -y install ...
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install ...
```

## alternative 机制
update-java-alternatives --list
sudo update-alternatives --config java
sudo  update-alternatives --config javac

## 缓存

deb 包缓存： `/var/cache/apt/`
仓库元数据缓存： `/var/lib/apt/lists/`

清除缓存：
```
sudo apt clean
sudo apt purge
```

## by-hash
sources.list 的例子：

```
deb [by-hash=force] http://packages.deepin.com/deepin lion main contrib non-free
deb [by-hash=yes] http://packages.deepin.com/deepin panda main contrib non-free
deb [by-hash=no] https://developer.download.nvidia.com/compute/cuda/repos/debian10/x86_64/ /
```
by-hash 启用后，apt 客户端根据文件的 hash 而不是文件名拼凑请求的 url ，这可以避免重命名或者修改包内容但没有重命名的问题。仓库会指出它是否支持 by-hash 。[by-hash=...] 配置中，force 强制启用，yes（默认）则在仓库支持时启用，no 强制不启用。有的仓库的 by-hash 机制有bug，导致 apt update 失败，这时可以指定 [by-hash=no] 。

## 一些源服务器
http://mirrors.163.com/
https://mirrors.ustc.edu.cn/

### ubuntu
http://mirrors.aliyun.com/ubuntu/

### debian
https://mirrors.tuna.tsinghua.edu.cn/debian/
https://mirrors.tuna.tsinghua.edu.cn/help/debian/

source.list 的写法：

sid
```
deb https://mirrors.tuna.tsinghua.edu.cn/debian/ sid main contrib non-free
```
testing

```
deb https://mirrors.tuna.tsinghua.edu.cn/debian/ testing main contrib non-free
# deb-src https://mirrors.tuna.tsinghua.edu.cn/debian/ testing main contrib non-free
deb https://mirrors.tuna.tsinghua.edu.cn/debian/ testing-updates main contrib non-free
# deb-src https://mirrors.tuna.tsinghua.edu.cn/debian/ testing-updates main contrib non-free
deb https://mirrors.tuna.tsinghua.edu.cn/debian/ testing-backports main contrib non-free
# deb-src https://mirrors.tuna.tsinghua.edu.cn/debian/ testing-backports main contrib non-free
deb https://mirrors.tuna.tsinghua.edu.cn/debian-security testing-security main contrib non-free
# deb-src https://mirrors.tuna.tsinghua.edu.cn/debian-security testing-security main contrib non-free
```

## 忽略依赖错误（冲突、破坏等），强行安装、卸载

### 方法（1）：dpkg
卸载：
```
sudo dpkg -r --force-depends packagename
```
安装：
```
apt download foo  # 下载到当前目录
# 或略对于bar的依赖
dpkg --ignore-depends=bar -i foo_1.2.3_amd64.deb
# 或略所有的依赖、冲突、破坏错误
dpkg --force-depends --force-breaks --force-conflicts -i foo_1.2.3_amd64.deb
```
局限性：之后的 apt 会一直报错，直到将有问题的包 foo 卸载。

### 方法（2）：直接解压
```
apt download foo=1.2.3
sudo dpkg -x foo_1.2.3_amd64.deb /
```
局限性：无法用 apt 卸载，需要手动删除文件。

### 方法（3）
打开 `/var/lib/dpkg/status` 文件，搜索 foo 的描述，修改其中的 `Depends:` 行，令其符合系统的现状，从而使 apt 等工具不再报错。

## aptitude

一些与apt等效的选项

aptitude                apt
--with-recommends      --install-recommends
--without-recommends   --no-install-recommends
-s                     --dry-run

## 阻止特定软件包的升级
有三种命名可以“盯住”软件包的版本[2.1]：

    apt-mark 命令
    dpkg 命令
    aptitude 命令

sudo apt-mark hold nano
apt-mark showhold
sudo apt-mark unhold nano

sudo aptitude hold python3
sudo aptitude unhold python3

## NO_PUBKEY xxx 错误
apt update 等命令运行时，可能遇到错误：
由于没有公钥，无法验证下列签名： NO_PUBKEY 76F1A20FF987672F

这时可以从某个 keyserver 获得公钥：

```
sudo apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 76F1A20FF987672F
```
有时候遇到错误会导致公钥导入失败：

```
gpg: invalid key resource URL '/etc/apt/trusted.gpg.d/home:erigas:cpupower-gui.gpg'
gpg: 密钥区块资源‘(null)’：一般错误
```

这可能是因为文件名 “home:erigas:cpupower-gui.gpg” 有特殊字符，改名或删除即可。

如果错误来自特定发行版的仓库，可以安装其公钥包：
```
# debian
sudo apt install debian-archive-keyring
```

## 自动与手动安装

重新将一个包设置为自动/手动安装：
```
sudo apt-mark auto libfcitx-gclient1
sudo apt-mark manual libfcitx-gclient1
apt-mark showmanual
sudo apt-mark minimize-manual - Mark all dependencies of meta packages as automatically installed.
```
## 在 debian 上使用 PPA

```
sudo apt install software-properties-common

sudo add-apt-repository ppa:intel-opencl/intel-opencl
Traceback (most recent call last):
  File "/usr/bin/add-apt-repository", line 95, in <module>
    sp = SoftwareProperties(options=options)
  File "/usr/lib/python3/dist-packages/softwareproperties/SoftwareProperties.py", line 109, in __init__
    self.reload_sourceslist()
  File "/usr/lib/python3/dist-packages/softwareproperties/SoftwareProperties.py", line 599, in reload_sourceslist
    self.distro.get_sources(self.sourceslist)    
  File "/usr/lib/python3/dist-packages/aptsources/distro.py", line 93, in get_sources
    (self.id, self.codename))
aptsources.distro.NoDistroTemplateException: Error: could not find a distribution template for Deepin/stable
```

### add-apt-repository 错误：gpg: no valid OpenPGP data found

添加 ppa 时可能遇到错误“gpg: no valid OpenPGP data found”，这往往是公钥文件下载错误。

```
$ sudo sh -c 'add-apt-repository ppa:cybolic/vineyard-testing'

 The latest version of Vineyard.
 More info: https://launchpad.net/~cybolic/+archive/ubuntu/vineyard-testing
Press [ENTER] to continue or ctrl-c to cancel adding it

gpg: keybox '/tmp/tmpzr4htebx/pubring.gpg' created
gpg: /tmp/tmpzr4htebx/trustdb.gpg: trustdb created
gpg: key 150BA781A5FF2BFC: public key "Launchpad gnome-wine" imported
gpg: Total number processed: 1
gpg:               imported: 1
gpg: no valid OpenPGP data found.
```

之后执行 apt update 也会出错：

```
$ sudo apt update
错误:10 http://ppa.launchpad.net/cybolic/vineyard-testing/ubuntu xenial InRelease
  由于没有公钥，无法验证下列签名： NO_PUBKEY 150BA781A5FF2BFC
正在读取软件包列表... 完成    
W: GPG 错误：http://ppa.launchpad.net/cybolic/vineyard-testing/ubuntu xenial InRelease: 由于没有公钥，无法验证下列签名： NO_PUBKEY 150BA781A5FF2BFC
E: 仓库 “http://ppa.launchpad.net/cybolic/vineyard-testing/ubuntu xenial InRelease” 没有数字签名。
N: 无法安全地用该源进行更新，所以默认禁用该源。
N: 参见 apt-secure(8) 手册以了解仓库创建和用户配置方面的细节。
```

修复的方法是从别处下载公钥文件（150BA781A5FF2BFC 应该与前面报错的公钥id相同）：

```
$ sudo apt-key adv --keyserver hkp://pool.sks-keyservers.net:80 --recv-keys 150BA781A5FF2BFC

Executing: /tmp/apt-key-gpghome.BdxzErT6mI/gpg.1.sh --keyserver hkp://pool.sks-keyservers.net:80 --recv-keys 150BA781A5FF2BFC
gpg: key 150BA781A5FF2BFC: public key "Launchpad gnome-wine" imported
gpg: Total number processed: 1
gpg:               imported: 1

```

然后再执行 apt update 。

## 将 rpm 转换为 deb 包

[13.1]

```
sudo apt install alien fakeroot
fakeroot
alien xxx.rpm
exit
```
将生成  xxx.deb 。或者将fakeroot和alien放在一句里：
```
fakeroot alien xxx.rpm
```

## 从已经安装的包重建 deb 文件
dpkg-repack 可以从已安装的包中重建 deb 文件[14.1]。
```
sudo apt-get install dpkg-repack
dpkg-repack gparted
fakeroot -u dpkg-repack gparted
```
dpkg-repack 产生的 deb 文件放在当前目录下。
以普通用户运行 dpkg-repack 一般就够了，除非包中的一些文件对当前用户不可读，或者属于其它非 root 用户，这时可以用 fakeroot -u dpkg-repack 来运行[14.1~14.2]。

## 编译 deb 源码包 / 自制 debian backport 包
/etc/apt/sources.list.d/debian-sid.list:

```
deb-src https://mirrors.tuna.tsinghua.edu.cn/debian/ sid main contrib non-free
```

```
sudo apt-get install packaging-dev debian-keyring devscripts equivs
mkdir deb-src && cd deb-src/
apt source gcc-10
cd gcc-10-10.2.0/
sudo mk-build-deps --install --remove # 或者 sudo aptitude build-dep gcc-10
dpkg-buildpackage -us -uc
```
如果 mk-build-deps 出错，看一下有什么包没有安装，或者版本不满足，试着手动安装，如
sudo aptitude install -t apricot g++-multilib
然后重新运行 mk-build-deps

如果 mk-build-deps 有依赖问题，可以尝试 aptitude build-dep <package name> 。

如果 dpkg-buildpackage 报告依赖问题，如：

```
dpkg-checkbuilddeps: error: Unmet build dependencies: libc6-dev (>= 2.30-1~) lib32gcc-s1 libx32gcc-s1 libzstd-dev systemtap-sdt-dev binutils:native (>= 2.35) binutils-hppa64-linux-gnu:native (>= 2.35) nvptx-tools llvm-10 lld-10 texinfo (>= 4.3) locales-all gnat-10:native g++-10:native libisl-dev (>= 0.20) libmpc-dev (>= 1.0) lib32z1-dev dejagnu chrpath libgc-dev doxygen (>= 1.7.2) docbook-xsl-ns
```
先尽量安装最高版本，如：

```
sudo aptitude install -s -t apricot libc6-dev libzstd-dev systemtap-sdt-dev binutils:native binutils-hppa64-linux-gnu:native nvptx-tools texinfo locales-all libisl-dev libmpc-dev lib32z1-dev dejagnu chrpath libgc-dev doxygen docbook-xsl-ns llvm-8 lld-8 g++-8 gnat
```

版本号仍然偏低的，修改 debian/control 的 Build-Depends ，改版本号。

仓库里不存在的，可以先删除，看看编译的结果。

### DEB_BUILD_OPTIONS

deb 包编译时的选项一般通过环境变量 DEB_BUILD_OPTIONS 来设置，一般是传递到 configure 等的选项。DEB_BUILD_OPTIONS 是空格分割的多个选项，每个选项可以是一个词，如 noopt 或者键值对，如 a=b 。

不同的项目中 DEB_BUILD_OPTIONS 支持的选项也不同。debian 没有提供列出 DEB_BUILD_OPTIONS 支持的选项的工具，可以在 debian/ 目录下搜索：

```
grep -Irn DEB_BUILD_OPTIONS *
```
选项的含义要看相应的源码。但也有一些约定俗成的选项：

nocheck: 不跑测试
nobech: 不跑性能测试
nopgo: 不做 profile guided optimizition

### 删除 xxx-build-deps_nnn.deb 文件

这个文件是 mk-build-deps 生成的。但是当运行 dpkg-buildpackage 时，可能会把它误当成源码包的一部分，引发错误，例如

```
~/t-project/deb-src/python3.9-3.9.0~rc1$ dpkg-buildpackage --no-check-builddeps --no-sign --jobs=4

dpkg-source: error: cannot represent change to python3.9-build-deps_3.9.0~rc1-1_amd64.deb: binary file contents changed
dpkg-source: error: add python3.9-build-deps_3.9.0~rc1-1_amd64.deb in debian/source/include-binaries if you want to store the modified binary in the debian tarball
dpkg-source: error: unrepresentable changes to source
dpkg-buildpackage: error: dpkg-source -b . subprocess returned exit status 2
```

这时应该删除 xxx-build-deps_nnn.deb 文件再继续。

## 从本地目录安装 deb 及其依赖

https://askubuntu.com/questions/40011/how-to-let-dpkg-i-install-dependencies-for-me
sudo apt install ./foo-1.2.3.deb

## 自建 deb 仓库

https://askubuntu.com/questions/170348/how-to-create-a-local-apt-repository
https://help.ubuntu.com/community/Repositories/Personal
https://www.linux.com/topic/desktop/create-your-own-local-apt-repository-avoid-dependency-hell/

在含有 deb 文件的目录下运行：

cd ~/t-project/deb-src/out
dpkg-scanpackages . /dev/null | gzip -c > Packages.gz
sudo dedit /etc/apt/sources.list.d/deb-src.list

deb [trusted=yes] file:///home/duanyao/t-project/deb-src/out ./

apt update
apt install xxx

如果不写 [trusted=yes]，可能遇到错误：

E: 仓库 “file:/home/duanyao/t-project/deb-src/out ./ Release” 没有 Release 文件。
N: 无法安全地用该源进行更新，所以默认禁用该源。

英语：

E: The repository 'xxx' does not have a Release file.
N: Updating from such a repository can't be done securely, and is therefore disabled by default.

## deepin 升级，从 15.11 到 V20

/etc/apt/sources.list <<< "deb [by-hash=force] https://community-packages.deepin.com/deepin/ apricot main contrib non-free"

/etc/apt/sources.list.d/appstore.list <<< "deb https://community-store-packages.deepin.com/appstore eagle appstore"

sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 1C30362C0A53D5BB

sudo apt update
sudo apt dist-upgrade

## 判断发行版 dpkg-vendor

例如，`dpkg-vendor --derives-from Debian ; echo $?` ，如果是 Debian 的衍生版，退出码是0，否则是1（用 $? 查看）。

## deb 包的版本号

deb 包的版本号的完整例子：`2:3.6.19-1~bpo70+1+b1`

* 2：第一个冒号(:)前是纪元（epoch），是自增的自然数，这是为了重置版本号。可选。
* 3.6.19：纪元之后，最后一个减号之前，是上游版本号。格式必须是小数点(.)分隔的自然数。
* 1~bpo70+1+b1：最后一个减号之后，是 debian 修订号。这里面不能再有减号。可选。
  * 1：原始 debian 修订号，自然数。对打包进行修改后就自增。具体什么算修改？
  * ~bpo70：向后移植版。bpo = backported, 70 = to Debian 7 aka Wheezy 。
  * +1 = Backport revision, increments if only the backport packaging has changed between two backport package releases.
  * +b1 = First binary rebuild without source changes

比较版本号的时候，按纪元、上游版本号、debian 修订号的顺序比较，前面的比出结果就不用再比后面的了。
可以用以下命令比较版本号：

```
dpkg --compare-versions 11a lt 100a ; echo $?
```
参数语法是 <version1> lt|gt|eq <version2> 。返回值：0 表示真，1 表示假。

## 查看 .deb 文件的信息
```
dpkg-deb -c <foo.deb> # 列出文件
dpkg-deb -I <foo.deb> # 显示元数据
```
## 下载 deb ，不安装
例子：
```
aptitude download lightdm
aptitude download lightdm=1.21.6-1
apt download libssl1.1
```

下载到当前目录。注意， -t 参数无效，如果要下载特定仓库的版本，只能指定版本号。

## 显示将要下载的 deb 包的链接 url

```
apt download --print-uris libssl1.1
'https://mirrors.tuna.tsinghua.edu.cn/ubuntu/pool/main/o/openssl/libssl1.1_1.1.1f-1ubuntu2.19_amd64.deb' libssl1.1_1.1.1f-1ubuntu2.19_amd64.deb 1321244 SHA512:8c033d73b2ee6cb5e7a33096501189d953fc4881f4864fb24d127ac70d79208fe0ed99691c0e0927d5b8099b4347516e7b2a5949a76f2d6a52f9454bea71c6fb
```
注意，如果当前目录下已经有下载好的 deb 包，则 apt download 不会工作；可以先删除或改名。

## 解压和重新压缩 deb

```
fakeroot bash
mkdir tmp
dpkg-deb -R original.deb tmp
# edit DEBIAN/postinst
dpkg-deb -b tmp fixed.deb
exit
```

fakeroot 的作用是：让普通用户可以创建具有 root 身份的压缩文档中的文件。

## proxy
因为 apt 的大部分操作需要 sudo ，而 sudo 不会传递原用户的环境变量，所以让 apt 使用 proxy 需要特殊处理，详见 sudo.md 。

## 日志/安装历史
deb 的安装日志在 /var/log/dpkg.log 。无论以何种前端安装的 deb 包，最终都会调用 dpkg，所以这里的记录是最全面的[3.1]。
```
grep xxxx /var/log/dpkg.log
```
此外 /var/log/apt/history.log 包括 apt/apt-get 命令安装的 deb 包，但不包括其它前端安装的。

## 自制系统镜像（根文件系统）
参考 linux/create_os_image_debootstrap.md 。
