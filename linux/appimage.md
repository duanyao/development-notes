## 参考资料

[1] AppImageKit https://github.com/AppImage/AppImageKit

## 解压后运行
[1]
appimage 的直接运行依赖于用 fuse 挂载文件系统，但有些环境如docker/k8s容器中不允许这样做，这时就需要解压后运行了。

foo.appimage --appimage-extract
解压到 ./squashfs-root 目录，一般可执行文件在 ./squashfs-root/usr/bin/nvtop

foo.appimage --appimage-extract-and-run
解压并运行。

env APPIMAGE_EXTRACT_AND_RUN=1 foo.appimage
解压并运行。
