## 参考资料
[2] What's the difference between a normal user and a system user?
https://unix.stackexchange.com/questions/80277/whats-the-difference-between-a-normal-user-and-a-system-user

[3] How can I create a non-login user?
https://superuser.com/questions/77617/how-can-i-create-a-non-login-user

[5] Effect of entries in /etc/securetty
https://unix.stackexchange.com/questions/41840/effect-of-entries-in-etc-securetty

[6] 4.4.2. Disallowing Root Access
https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/4/html/Security_Guide/s2-wstation-privileges-noroot.html

[7] Why can I create Users with the same UID?
https://askubuntu.com/questions/427107/why-can-i-create-users-with-the-same-uid

[8] What are the side effects of having several UNIX users share one UID?
https://unix.stackexchange.com/questions/124968/what-are-the-side-effects-of-having-several-unix-users-share-one-uid

[9] What are correct permissions for /tmp ? I unintentionally set it all public recursively
https://unix.stackexchange.com/questions/71622/what-are-correct-permissions-for-tmp-i-unintentionally-set-it-all-public-recu

## 基本概念
用户和组一般是以名字（字符串）来表达，在 /etc/passwd, /etc/group, /etc/shadow, /home/username 中也是如此。
但是在文件系统中，用户和组是以 uid 和 gid（数字）来记载。
这样，两者就有发生不匹配的可能性，例如：

* 将存储设备从一个系统移动到另一个系统：两个系统中同名用户/组的id未必相同，非同名用户/组的id未必不同。
* 重装系统并重建账号：同名用户/组的id与上次安装未必相同。
* 重装一个系统服务软件包，该包在安装时创建专用账号：用户/组的id与上次安装未必相同。

这种不匹配可能造成访问文件系统时出现问题。
建议：在创建用户/组时指定id，而不是让工具随意选择id。

## 多用户共享同样的uid
unix/linux 允许多个用户名具有相同的uid。
这反映在 /etc/passwd 中，两行不同的用户名具有相同的uid。
在某些层面上，这些用户是相同的：他们创建的文件、进程具有相同的uid，被认为是属于同一用户的。
在另一些些层面上，这些用户是不同的：在 /etc/passwd 中，他们可以设定不同的家目录（HOME）、主组和主组id、登录shell、登录密码。
unix/linux 的基本系统可以正常处理多个用户名具有相同的uid的情形，ssh和nfs应该也没问题；GUI系统能否正确处理还未可知。

## 用户管理文件
### /etc/passwd

例子：
root:x:0:0:root:/root:/bin/ash
adm:x:3:4:adm:/var/adm:/sbin/nologin
sshd:x:22:22:sshd:/dev/null:/sbin/nologin

### /etc/shadow
例子：
root:::0:::::
sshd:!::0:::::

### /etc/group
例子：

sys:x:3:root,bin,adm
adm:x:4:root,adm,daemon
squid:x:31:squid

### /etc/login.defs
定义了 uid/gid 的范围，例如

```
#
# Min/max values for automatic uid selection in useradd
#
UID_MIN			 1000
UID_MAX			60000
# System accounts
#SYS_UID_MIN		  100
#SYS_UID_MAX		  999

#
# Min/max values for automatic gid selection in groupadd
#
GID_MIN			 1000
GID_MAX			60000
# System accounts
#SYS_GID_MIN		  100
#SYS_GID_MAX		  999
```

密码过期时间：
```
#       PASS_MAX_DAYS   Maximum number of days a password may be used.
#       PASS_MIN_DAYS   Minimum number of days allowed between password changes.
#       PASS_WARN_AGE   Number of days warning given before a password expires.
#
PASS_MAX_DAYS   90
PASS_MIN_DAYS   0
PASS_WARN_AGE   7
```

## 用户管理工具
### 显示用户账户信息
* id
id [OPTION]... [USERNAME]

输出示例
uid=1000(duanyao) gid=1000(duanyao) 组=1000(duanyao),4(adm),7(lp),24(cdrom),25(floppy),27(sudo),29(audio),44(video),46(plugdev),100(users),102(netdev),108(lpadmin),109(scanner)

uid=0(root) gid=0(root) 组=0(root)

### adduser - 创建一个新用户或更新默认新用户信息

用法：adduser [选项] 登录名

  -g, --gid GROUP		新账户主组的名称或 ID
  -G, --groups GROUPS	新账户的附加组列表
  -u, --uid UID			新账户的用户 ID
  -U, --user-group		创建与用户同名的组


adduser [--uid uid] [--system] [--group] [--no-create-home] username
adduser [-u uid] [-h /] [-H] [-S] [-G groupname] username #busybox版

不加选项时，adduser 创建主目录，并创建与用户名同名的用户组。

[--system] 和 [-S] 创建系统账户。与 --system 同时使用时，--group 创建同名组（busybox版无此功能）。
-h 指定主目录，-H 和 --no-create-home 不创建主目录。
注意仅有 [--system] 和 [-S] 时，还是会创建家目录的。

系统账户是用来专门运行系统服务的账户，没有密码，不能登录，没有主目录。

将用户追加到一个组里，原有的组不变：
adduser username groupname

### useradd - 创建一个新用户或更新默认新用户信息

useradd -rU username
useradd -U username -s /bin/bash

-u UID
           用户id。不指定时自动分配。搭配-o选项可以使用已经存在的uid。

-g, --gidGROUP
           用户初始登陆组的组名或号码。组名必须已经存在。组号码必须指代已经存在的组。

-G, --groupsGROUP1[,GROUP2,...[,GROUPN]]]
           用户还属于的附加组列表。每个组都用逗号隔开，没有中间的空格。

-U, --user-group
           创建一个和用户同名的组，并将用户添加到组中。这一般是默认行为。

-r, --system
           创建一个系统账户。
           
-s, --shell SHELL
           设置登录shell，例如 /bin/bash。默认值由 /etc/default/useradd 里的 SHELL 变量指定，一般是 /bin/sh 。一般用户还是应该指定 /bin/bash。
           
-o, --non-unique
           allows the creation of an account with an already existing UID.

           This option is only valid in combination with the -u option.

默认情况下，useradd 不创建主目录，所以不适合一般用户使用。

### usermod
usermod [选项] 登录

  -g, --gid GROUP               强制使用 GROUP 为新主组
  -G, --groups GROUPS           新的附加组列表 GROUPS
  -a, --append GROUP            将用户追加至上边 -G 中提到的附加组中，
                                并不从其它组中删除此用户
增加用户到一个组（注销后生效）：                 
usermod -a -G groupname username # 注意 usermod 的帮助在此处是误导的，-a 不能带参数，必须以 -G 指定要添加的组。


### addgroup
addgroup [--gid ID] [--system] groupname
addgroup [-g gid] [-S] [USER] groupname # busybox 版

--system 或 -S 创建系统组。
[USER] 参数会将该用户加入组。

### groupadd

用法：groupadd [选项] 组

选项:
  -f, --force		如果组已经存在则成功退出
			并且如果 GID 已经存在则取消 -g
  -g, --gid GID                 为新组使用 GID
  -r, --system                  创建一个系统账户

### 刷新对用户组的修改，不注销

如果将用户新增到一个组，例如：

```
sudo usermod -aG docker $USER
```
可以用以下方法刷新当前shell：
```
exec newgrp docker # 将主组切换为新的组
exec newgrp - # 将主组切换为真正的主组。
```

### deluser, userdel
userdel srv-uploadProxy

### delgroup, groupdel
groupdel srv-uploadProxy

## 系统用户/系统组
参考[2,3]和 man adduser。

系统用户指专门用来运行系统服务的用户。其特点是：
（1）专户专用，一个用户只用于一个系统服务。
（2）不可登录（也没有密码）。
（3）在/home下没有家目录。
（4）通常其用户组也是唯一的，与用户名同名。
（5）没有shell（在/etc/passwd中指定为/bin/false或/sbin/nologin）。

系统组与普通组并没有实质区别，仅仅是有个约定的gid范围。例如系统组小于 500，普通组大于 500。
但不同的发行版约定的范围不尽相同，gid的范围对程序和系统也没有实质影响。
一个稳定而能避免id冲突的方法是：如果是网络服务，用服务的端口号作为uid/gid。

可以用以下方法创建系统用户：
useradd -M username  # -M 表示不创建主目录
usermod -L username # -L 表示锁定用户，不允许口令登录（详见 /etc/shadow）

或者更简单地：
useradd --system username
这还会创建同名的组。

如果是 adduser 则是 
adduser --system username
adduser --system --group username # 同时创建同名组
如果不指定 uid，则自动从 FIRST_SYSTEM_UID - LAST_SYSTEM_UID 范围选择。

busybox：
adduser -h / -H -S -u uid -G username username
busybox 版不会自动创建同名组。

## 登录控制

### 登录 shell
/etc/passwd 中的登录 shell 如果被设置为 /sbin/nologin 或者 /bin/false 等非 shell 程序，则无法通过终端登录[6]。
通常将系统服务的用户设置成这样，例如：
squid:x:31:31:Squid:/var/cache/squid:/sbin/nologin

这不影响 sudo、ftp 等，而 ssh、sftp 确实会受影响。

root 用户仍然可以通过 su -s /bin/sh username 来切换到没有 shell 的用户并启动 shell。

### /etc/shadow
详细的解释见 man shadow。

第二个字段是加密的密码。
这个字段如果是"!"或者"*"开头，则这个用户的口令被禁用，等同于无法登录，例如
squid:!::0:::::

这个似乎不影响非口令登录，例如 ssh 的密钥登录？

通常，系统用户（）

### 限制/禁止root用户登录
/etc/securetty 列出了允许 root 登录的终端，例如：
console
tty1
tty2
tty3
tty4

/etc/securetty 主要影响本地 root 登录：物理终端和虚拟终端，但是不影响 ssh root 登录[5]。
一个空的 /etc/securetty 文件会阻止大部分本地登录（单用户模式和 su/sudo 等是例外）。
要控制 root 通过 ssh 登录，请修改 sshd 的配置文件（参考 ssh.txt）。

要阻止用户进入单用户模式，需要在 bootloader 上设置。grub 的情形请参考 grub.cfg。

## 以其它用户身份运行程序
su -s /bin/sh -c "command line" username

  -s 指定用来执行 "command line" 的 shell。对于系统用户，必须指定 shell，因为其默认 shell 不可用。
  -c 指定执行的命令行。等效于用 sh -c "command line" 来执行。


## umask, fmask, dmask
用户创建的文件/目录的默认权限由 umask 来决定。
umask 是 shell 的一种属性，可以由 umask 命令来改变。umask 命令做出的改变是临时的，要长期有效，可以在 ~/.profile /etc/profile 等文件中执行 umask 命令。
mount 命令和 /etc/fstab 文件中也可以指定一系列挂载选项（umask, fmask, dmask, uid, gid），来指定一个文件系统的默认文件权限和默认用户/组。

What Is umask in Linux, and How Do You Use It?
https://www.howtogeek.com/812961/umask-linux/

fstab mount options for umask, fmask, dmask for ntfs with noexec
https://unix.stackexchange.com/questions/396904/fstab-mount-options-for-umask-fmask-dmask-for-ntfs-with-noexec
https://askubuntu.com/questions/429848/dmask-and-fmask-mount-options

## sticky bits
setuid, setgid, setsid

## 多用户共享目录
### 设置目录权限，允许所有用户读写
一个例子是 `/tmp`：

```
ls -la /tmp
总用量 55920
drwxrwxrwt  1 root                root                    6486 8月   4 15:15 .
```

/tmp 的权限是 rwx,o+t 或者 1777 。例如 `chmod 1777 /tmp` 或 `chmod 1777 /tmp a=rwx,o+t`；
`+t` 是指 sticky bit，`o` 是指 owner，`o+t` 是指仅允许所有者从 `/tmp` 中删除文件[9]。
1777 权限仅需要设置在 /tmp 上，不应该设置到其内的文件和目录上。

### 设置目录权限，允许组用户读写
https://askubuntu.com/a/313332

### 使用 bindfs，强行覆盖目录/文件所有者和权限
https://bindfs.org/

局限性： 
inotify 无效。
这会让一些自动编译工具、IDE无法监视文件改变。可能的解决方案： FUSE fsnotify 。
