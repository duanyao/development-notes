## 参考资料

[1] Sudo https://wiki.archlinux.org/index.php/Sudo

[2] https://stackoverflow.com/questions/8633461/how-to-keep-environment-variables-when-using-sudo

[3] https://serverfault.com/questions/38114/why-does-sudo-command-take-long-to-execute

[4] https://askubuntu.com/questions/322514/terminal-command-with-sudo-takes-a-long-time

## 配置文件
[1]
配置文件是 `/etc/sudoers` 和 `/etc/sudoers.d/*` ，但不应该直接编辑它们，因为如果有语法错误，可能让你再也无法使用 sudo 。安全地编辑方法是：

```
sudo EDITOR=dedit visudo
sudo EDITOR=dedit visudo /etc/sudoers.d/XXX
```
它在生效前会检查语法。visudo 一般默认使用 vi 编辑器，但可以用 EDITOR 变量指定。

## sudo 编辑文件
[1]
sudo -e 的作用是：以当前用户的身份运行编辑器，编辑到临时文件，当编辑器退出时，以 root 身份复制覆盖原文件。编辑器可以用环境变量 SUDO_EDITOR 或者 EDITOR 指定。

```
SUDO_EDITOR="kate -b" sudo -e /etc/fstab
EDITOR="kate -b" sudo -e /etc/fstab
```
kate参数-b的意思是让命令行阻塞直到被编辑的文件关闭。这是因为 kate 默认是单实例模式。

可以将 SUDO_EDITOR 和 EDITOR 放到 `~/.xprofile` 里面。

```
export EDITOR="kate -b"
export SUDO_EDITOR="kate -b"
```
## 环境变量

默认状态下，sudo 只保持很少的当前用户的环境变量到下级命令中。使用命令 sudo env 可以检查 sudo 会保持哪些环境变量[2]。

可以使用
```
sudo name1=value1 name2=value2... command ...
```
的语法传递环境变量name1、name2。而
```
name1=value1 name2=value2... sudo command ...
```
则一般会忽略name1、name2。

不做特殊设置也会保持的特殊环境变量有：LANGUAGE、LANG、LC_ALL、EDITOR、SUDO_EDITOR 等。
要增加特殊环境变量，可以在 sudo 的配置文件中写 `Defaults env_keep +=...`[1]：

编辑 `/etc/sudoers.d/02_keep_env` ：
```
sudo EDITOR=dedit visudo /etc/sudoers.d/02_keep_env
```
内容是
```
Defaults env_keep += "ftp_proxy http_proxy https_proxy no_proxy"
```

效果即时生效。这样就保持了 proxy 相关的环境变量。

## sudo 缓慢的问题
例如 sudo ls （本地目录）可能经过数秒才能出结果，如果需要输入密码，则数秒后才显示“请输入密码”。

这个问题往往与网络有关，当机器已经联网但网速很慢时，sudo 会很慢，如果断网或者网络很快则sudo会很快。
在网上搜索可知，这是 sudo 的一个特性（缺陷），它执行时似乎要通过 DNS 查询本主机的 IP 地址，因此速度依赖网络。解决办法是把本主机名加到 /etc/hosts 的回环地址后面（默认只有 localhost 等）[3]，例如
```
127.0.0.1 localhost duanyao-lt-d
::1     localhost ip6-localhost ip6-loopback duanyao-lt-d
```
可以解决这个问题。但是这样做也可能有副作用，有些应用要求本主机名不绑定到回环地址。

远程 ssh 登录环境中执行 sudo 也可能查询 DNS，导致缓慢[3]。

更多可能性参考[3,4]。
