## 参考资料
minikube start
https://minikube.sigs.k8s.io/docs/start/

## 概述
minikube 是个单机环境下的 k8s 系统，管理机构和容器都在一台宿主机上，也只能在一台宿主机上。


## 安装（linux + docker）

minikube 默认的后端是 docker，即 minikube 自身以及 k8s 容器都以 docker 容器的形式运行于本机上。minikube 还支持其它形式的后端，如 KVM2 虚拟机、hyper-v 虚拟机（用于 windows 平台）等。

安装 minikube 的本地 deb 包：
```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb
```
设置 proxy，例如：
```
export http_proxy=http://192.168.49.1:23128;export https_proxy=http://192.168.49.1:23128;export NO_PROXY=localhost,192.168.49.1,10.0.0.0/8,172.16.0.0/12,192.168.0.0/16 
```
这是因为 minikube 安装期间要访问的部分网站可能存在访问困难的问题。安装之后再执行 minikube start 等命令，则一般不需要再设置 http(s)_proxy 。
NO_PROXY 是需要的，避免通过 proxy 访问 minikube 本身（minikube 容器的IP地址是 192.168.49.2，宿主机的 IP 是 192.168.49.1）。

注意，如果你的 proxy 是在 minikube 的宿主机上，不要把 http_proxy 的 IP 地址写成 127.0.0.1，而是要写成宿主机 IP 192.168.49.1，这是因为 minikube 的后端 docker 容器也可能要使用代理（例如下载 k8s addon），写成 127.0.0.1 是无法访问的，得明确写成宿主机 IP 。

初次启动（将下载 minikube 的 docker 镜像）：
```
minikube start
```
执行此命名不需要 root 或 sudo。

成功后输出大致如下：

```
😄  Debian 10.10 上的 minikube v1.30.1
✨  根据现有的配置文件使用 docker 驱动程序
❗  docker is currently using the btrfs storage driver, consider switching to overlay2 for better performance
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
🔄  Restarting existing docker container for "minikube" ...
🌐  找到的网络选项：
    ▪ NO_PROXY=localhost,192.168.49.1,10.0.0.0/8,172.16.0.0/12,192.168.0.0/16
    ▪ http_proxy=http://192.168.49.1:23128
    ▪ https_proxy=http://192.168.49.1:23128
🐳  正在 Docker 23.0.2 中准备 Kubernetes v1.26.3…
    ▪ env NO_PROXY=localhost,192.168.49.1,10.0.0.0/8,172.16.0.0/12,192.168.0.0/16
    ▪ env HTTP_PROXY=http://192.168.49.1:23128
    ▪ env HTTPS_PROXY=http://192.168.49.1:23128
    ▪ kubelet.localStorageCapacityIsolation=false
🔗  Configuring bridge CNI (Container Networking Interface) ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
    ▪ Using image registry.k8s.io/metrics-server/metrics-server:v0.6.3
🌟  Enabled addons: storage-provisioner, default-storageclass, metrics-server
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

这下载了 kicbase/stable 镜像，然后在本地启动了一个名为 minikube 的 docker 容器，可以查看：

```
docker images

REPOSITORY                                                                                            TAG          IMAGE ID       CREATED         SIZE
kicbase/stable                                                                                        v0.0.39      67a4b1138d2d   2 months ago    1.05GB

docker ps

CONTAINER ID        IMAGE                    COMMAND                  CREATED             STATUS              PORTS                                                                                                                                  NAMES
254e92b175f2        kicbase/stable:v0.0.39   "/usr/local/bin/entr…"   3 weeks ago         Up 19 minutes       127.0.0.1:32772->22/tcp, 127.0.0.1:32771->2376/tcp, 127.0.0.1:32770->5000/tcp, 127.0.0.1:32769->8443/tcp, 127.0.0.1:32768->32443/tcp   minikube
```

启用扩展 metrics-server：
```
minikube addons enable metrics-server

💡  metrics-server is an addon maintained by Kubernetes. For any concerns contact minikube on GitHub.
You can view the list of minikube maintainers at: https://github.com/kubernetes/minikube/blob/master/OWNERS
    ▪ Using image registry.k8s.io/metrics-server/metrics-server:v0.6.3
🌟  启动 'metrics-server' 插件
```

下载 addons 的镜像时，也需要 http(s)_proxy 。

minikube / k8s 的 addons 的版本号可能会随时变化 此外， metrics-server

启动控制面板服务：
```
minikube dashboard

minikube dashboard
🤔  正在验证 dashboard 运行情况 ...
🚀  Launching proxy ...
🤔  正在验证 proxy 运行状况 ...
🎉  Opening http://127.0.0.1:42435/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/ in your default browser...
```

这会在控制台中启动一个web服务器，并引导浏览器打开 `http://127.0.0.1:42435/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/`。

初次启动 minikube start 后，它会修改 kubectl 的配置文件 `~/.kube/config` ，增加对本地 minikube cluster 的配置，其默认服务器地址为 `https://192.168.49.2:8443`。


minikube 的配置文件、镜像缓存等位于 `~/.minikube/` 。

## 其它基本操作

暂停/恢复：
```
minikube pause
minikube unpause
```

暂停后，minikube 的 docker 容器仍在运行，但 kubectl 已经无法连接。

启动/关闭：

```
minikube start
minikube stop
```
登录到 minikube 后端的 docker 容器，用 ssh 方式。
```
minikube ssh
```

其它
```
minikube config set memory 9001
Browse the catalog of easily installed Kubernetes services:

minikube addons list
Create a second cluster running an older Kubernetes release:

minikube start -p aged --kubernetes-version=v1.16.1
Delete all of the minikube clusters:

minikube delete --all # 也会清除 `~/.kube/config` 中的相关记录。
```
 
## 使用 kubectl

minikube 自带一个 kubectl ，可以用 `minikube kubectl ...` 调用。同时，k8s 的 kubectl 也可以与 minikube 集群交互。
例子：
```
kubectl get po -A

minikube kubectl -- get po -A
```

输出类似：

```
E0612 16:42:31.013687  958138 memcache.go:287] couldn't get resource list for metrics.k8s.io/v1beta1: the server is currently unable to handle the request
E0612 16:42:31.025100  958138 memcache.go:121] couldn't get resource list for metrics.k8s.io/v1beta1: the server is currently unable to handle the request
E0612 16:42:31.028920  958138 memcache.go:121] couldn't get resource list for metrics.k8s.io/v1beta1: the server is currently unable to handle the request
E0612 16:42:31.045244  958138 memcache.go:121] couldn't get resource list for metrics.k8s.io/v1beta1: the server is currently unable to handle the request
NAMESPACE              NAME                                        READY   STATUS             RESTARTS       AGE
kube-system            coredns-787d4945fb-8wtpr                    1/1     Running            3 (16d ago)    22d
kube-system            etcd-minikube                               1/1     Running            1 (16d ago)    22d
kube-system            kube-apiserver-minikube                     1/1     Running            1 (16d ago)    22d
kube-system            kube-controller-manager-minikube            1/1     Running            1 (16d ago)    22d
kube-system            kube-proxy-jxwcm                            1/1     Running            1 (16d ago)    22d
kube-system            kube-scheduler-minikube                     1/1     Running            1 (16d ago)    22d
kube-system            metrics-server-6588d95b98-jw4rn             0/1     ImagePullBackOff   0              71m
kube-system            storage-provisioner                         1/1     Running            11 (86m ago)   22d
kubernetes-dashboard   dashboard-metrics-scraper-5c6664855-6xms4   1/1     Running            4 (16d ago)    22d
kubernetes-dashboard   kubernetes-dashboard-55c4cbbc7c-vhtcn       1/1     Running            9 (85m ago)    22d
```
