## 安装
### ubuntu 旧版本安装新版 sshfs
```
apt install sshfs=3.7.3-1.1 fuse3=3.14.0-4 libfuse3-3=3.14.0-4
```

查看版本：
```
apt policy fuse3
fuse3:
  Installed: 3.14.0-4
  Candidate: 3.14.0-4
  Version table:
 *** 3.14.0-4 150
        150 https://mirrors.tuna.tsinghua.edu.cn/debian bookworm/main amd64 Packages
        100 /var/lib/dpkg/status
     3.10.5-1build1 500
        500 http://mirrors-internal.cmecloud.cn/ubuntu jammy/main amd64 Packages

apt policy sshfs
sshfs:
  Installed: 3.7.3-1.1
  Candidate: 3.7.3-1.1
  Version table:
 *** 3.7.3-1.1 150
        150 https://mirrors.tuna.tsinghua.edu.cn/debian bookworm/main amd64 Packages
        100 /var/lib/dpkg/status
     3.7.1+repack-2 500
        500 http://mirrors-internal.cmecloud.cn/ubuntu jammy/universe amd64 Packages
```

### 从源码构建
```
sudo apt install ninja-build meson libfuse3-dev libglib2.0-dev python3-docutils
```
python3-docutils 可选，用于生成 manpage。

## 用法

### 配置 fuse
修改 /etc/fuse.conf ，取消 user_allow_other 的注释，这样允许在挂载时使用 allow_root 和 allow_other 选项。

```
user_allow_other
```

### 用命令行挂载
```
sshfs [user@]host:[dir] mountpoint [-o option[,option...]]
```
例子：
```
sshfs -f -o debug -o max_conns=4 -o reconnect -o direct_io -o no_readahead -o allow_other -o follow_symlinks -o noatime -o Ciphers=chacha20-poly1305@openssh.com -o Compression=no -o ServerAliveInterval=3 -o ConnectTimeout=3 ydnfs01:/safe_share /media/sshfs/safe_share
```

-f 启用 FUSE 的 foreground 模式。否则，自动进入后台运行。
--debug 文档有误，似乎是启用 libfuse 的调试输出，但不会启用 sshfs 本身的调试输出。
-o debug 启用 sshfs 的调试输出。
-o reconnect 连接意外断开后，自动重连服务器。默认不自动重连。如果 sshfs 检测到连接意外断开，如果不自动重连，则 sshfs 卸载此挂载点并退出；如果自动重连，则 sshfs 不退出并无限尝试重新连接，挂载点继续处于挂载状态，但在连接成功前，客户进程对挂载点的访问会立即返回错误。自动重连的时间间隔似乎无法设置，看起来很短。
-o max_conns=4 允许每个挂载点使用 max_conns 个 ssh 连接，默认1。由于 openssh 对每个连接只能使用一个 CPU 核心，单连接时加密/解密可能成为性能瓶颈，而多连接可以利用多核CPU，提高吞吐量。
-o no_readahead 禁用预取。
Compression, ServerAliveInterval, ConnectTimeout 等都是 ssh 的参数，也可以在 ~/.ssh/config 中设置。
ydnfs01 是 ssh 服务器名，可以在 ~/.ssh/config 中设置。
-o Ciphers=chacha20-poly1305@openssh.com -o Compression=no是ssh客户端的设置，选择更节省CPU的算法，一般也是ssh的默认设置。
-o ServerAliveInterval=3 -o ConnectTimeout=3 设置 ssh 连接超时和保持连接的间隔，单位是秒。

sshfs 没有类似 nfs 的 timeo 选项，即“操作超时”。ServerAliveInterval 可以部分起到类似作用。

命令行挂载时，采用本用户的 ssh 客户端的登录流程，例如查询 ~/.ssh/config, ~/.ssh/id_*, ~/.ssh/known_hosts, 等 。

如果用户不是 root，挂载点 /media/sshfs/safe_share 需要属于此用户，且可写。

对于每个挂载点，会存在一个 sshfs 进程，以及一个或多个 ssh 客户端进程（最多 max_conns 个）。
要卸载 sshfs 挂载点，可以结束相关 sshfs 进程（用 SIGTERM 或 SIGINT）,它会自动卸载挂载点。

### 用 fstab 挂载
命令行挂载方式中的所有 -o 参数，都可以写成 /etc/fstab 中的挂载选项，例如：

```
ydnfs01:/safe_share /media/sshfs/safe_share fuse.sshfs x-systemd.automount,_netdev,max_conns=4,reconnect,direct_io,no_readahead,allow_other, follow_symlinks,noatime,Compression=no,ServerAliveInterval=3,ConnectTimeout=3,IdentityFile=/home/user/.ssh/id_rsa 0 0
```

注意一些额外的选项：
- x-systemd.automount 启用 systemd 的按需（on-demand）挂载，即在首次访问挂载点时挂载。无需此功能也可以不写。
- _netdev 指示这是个网络设备。这是必需的，否则可能有 "No such device" 错误。
- IdentityFile=/home/user/.ssh/id_rsa 指定私钥文件。如果私钥文件是 /root/id_xxx ，则可以省略。

fstab 挂载是以 root 用户进行的，因此 ssh 登录流程默认查询 /root/.ssh/config, /root/.ssh/id_*, /root/.ssh/known_hosts 等文件。
此外，fstab 挂载是静默执行的，没有机会与用户交互，所以要确保：
* 服务器必需可以用公钥登录，且私钥文件没有密码保护。
* 服务器必需已经加入了用户的主机列表（.ssh/known_hosts）。

### 用 systemd --user 挂载
这种方式更适合非 root 用户，可以在登录后自动挂载，ssh 私钥可以有密码。

创建 `$HOME/.config/systemd/user/sshfs-ydnfs01.service` 文件：
```
[Unit]
Description=sshfs mount ydnfs01:/ on $HOME/media/ydnfs01
After=network.target

[Service]
Type=simple
ExecStart=sshfs -f-o max_conns=3 -o reconnect -o direct_io -o no_readahead -o allow_other -o follow_symlinks -o noatime -o Compression=no -o ServerAliveInterval=5 -o ConnectTimeout=5 ydnfs01:/ media/ydnfs01
WorkingDirectory=%h
Restart=no
#Restart=always
#RestartSec=3.0

[Install]
WantedBy=default.target
```
要点：
* `Type=simple` 结合 `sshfs -f` ，让 sshfs 进程的输出进入日志，可以用 `journalctl --user sshfs-ydnfs01` 查看。
* `WorkingDirectory=%h` 指定工作目录为当前用户的家目录，而挂载点写成相对家目录的路径（`media/ydnfs01`），避免不可移植 。
* 因为已经设置了 `sshfs -o reconnect` ，所以 `Restart=always` 不是必需的，但也可以使用。
* 因为 systemd 也是静默执行的，服务器必需可以用此用户的公钥登录。私钥可以有密码保护，只要在登录时被解锁，这可以通过某种 keyring 实现。

```
systemctl --user start sshfs-ydnfs01.service
```

## 源码分析

### 连接处理循环
在一个单独的线程中执行。
```
static void *process_requests(void *data_)
{
	(void) data_;
	struct conn *conn = data_;

	while (1) {
		if (process_one_request(conn) == -1)
			break;
	}

	pthread_mutex_lock(&sshfs.lock);
	conn->processing_thread_started = 0;
	close_conn(conn);
	g_hash_table_foreach_remove(sshfs.reqtab, (GHRFunc) clean_req, conn);
	conn->connver = ++sshfs.connvers;
	sshfs.outstanding_len = 0;
	pthread_cond_broadcast(&sshfs.outstanding_cond);
	pthread_mutex_unlock(&sshfs.lock);

	if (!sshfs.reconnect) {
		/* harakiri */
		kill(getpid(), SIGTERM);
	}
	return NULL;
}
```

### 启动连接处理线程
```
static int start_processing_thread(struct conn *conn)
```
调用链：
```
# 初始化时创建一个线程（和一个连接）
sshfs_init()
  start_processing_thread()

# 发送 SFTP 请求时按需启动一个线程（和一个连接），也可能复用已有线程（和一个连接）
sftp_request_send()
  start_processing_thread()
```