## 参考资料

[1.2] https://wiki.archlinux.org/index.php/Btrfs_

[1.3] https://linuxhint.com/create-mount-btrfs-subvolumes/

[1.4] 使用 Btrfs 快照进行增量备份 | Linux 中国
  https://zhuanlan.zhihu.com/p/259534619
  
[1.5] BTRFS: How big are my snapshots?
  http://dustymabe.com/2013/09/22/btrfs-how-big-are-my-snapshots/

[2.1] https://btrfs.wiki.kernel.org/index.php/Compression

[3.1] How to use “btrfs scrub” command to manage scrubbing on Btrfs file systems
  https://www.thegeekdiary.com/how-to-use-btrfs-scrub-command-to-manage-scrubbing-on-btrfs-file-systems/
  
[3.2] How to monitor BTRFS filesystem raid for errors?
   https://superuser.com/questions/789303/how-to-monitor-btrfs-filesystem-raid-for-errors

[4.1] Btrfsck
  https://btrfs.wiki.kernel.org/index.php/Btrfsck

[4.2] https://roov.org/2017/11/btrfs-repair/

[4.3] https://www.man7.org/linux/man-pages/man8/btrfs-restore.8.html

[4.4] https://unix.stackexchange.com/questions/598625/btrfs-has-invalid-csums-scrubbing-aborts-with-i-o-errors-but-ssd-seems-fine

[4.5] How to recover a BTRFS partition
   https://ownyourbits.com/2019/03/03/how-to-recover-a-btrfs-partition/


## 创建

mkfs.btrfs -L SYSB /dev/sda8

## subvolume

btrfs subvolume 的操作几乎都需要在已经挂载的文件系统上进行，用目录名来指代 subvolume 。

```
sudo mkdir -p /media/TOP
sudo mount -t btrfs /dev/sda8 /media/TOP

sudo btrfs subvolume create /media/TOP/@root-deepin-1
sudo btrfs subvolume create /media/TOP/@home

sudo btrfs subvolume list -p /media/TOP

ID 256 gen 7 parent 5 top level 5 path @root-deepin-1
ID 257 gen 8 parent 5 top level 5 path @home

btrfs subvolume delete /media/TOP/@root-deepin-1
```

删除目录的操作，如 rm -r, rmdir 也可以删除 subvolume 。

在子卷间移动/复制文件：

```
cp -a --reflink=always /home/<whatever> /home/@home/
```
--reflink=always 会导致共享底层存储空间。

## 配额 quota，子卷占用的空间
[1.5]
启用配额：

```
sudo btrfs quota enable /media/TOP
```

启用配额后，可以看到子卷和快照占用的空间。
```
sudo btrfs qgroup show /media/TOP

qgroupid         rfer         excl 
--------         ----         ---- 
0/5          16.00KiB     16.00KiB 
0/256        13.62GiB     10.42GiB 
0/257        10.66GiB     10.52GiB 
0/264         3.45GiB    257.98MiB 
0/265       158.15MiB     12.56MiB 
0/266        16.00KiB     16.00KiB 
0/290        16.00KiB     16.00KiB 
```

第一列是 subvolume id ，第二列是子卷占用的空间，第三列是子卷单独占用的空间（有些文件是在不同子卷/快照中共享空间的）。

## snapshot

snapshot 也是 subvolume ，它与另一个 subvolume 以“写时复制”模式共享内容。snapshot 可以是可写（默认）或只读模式。
语法是 

btrfs subvolume snapshot -r <source_path> <dest_path>

-r 表示只读。
source_path 和 dest_path 都是目录。

例如：
```
sudo btrfs subvolume snapshot -r /media/TOP/@root-deepin-1 /media/TOP/@root-deepin-1=2021-06-17=1
```

对于根卷，也可以创建快照。
```
sudo btrfs subvolume snapshot -r /media/TOP/ /media/TOP/@=2021-06-17=1
```

对只读快照建立一个可写快照，就可以修改了。

## 增量备份 send / receive

snapshot 只能位于同一文件系统上，而 send / receive 可以从一个 btrfs 备份数据到另一个 btrfs ，并可以实现增量备份 。

## mount 参数
在 fstab 中

```
LABEL=SYSB        /media/SYSB/top           btrfs      rw,relatime,noauto,ssd                                                         0  1
LABEL=SYSB        /                         btrfs      rw,relatime,compress=zlib,ssd,space_cache,commit=120,subvol=@root-deepin-1     0  1
LABEL=SYSB        /home                     btrfs      rw,relatime,compress=zlib,ssd,space_cache,commit=120,subvol=/@home             0  1
```

根卷的挂载与一般文件系统并无不同（第一行）；子卷的挂载需要用 subvol=... 参数来指定是哪个子卷。子卷的写法可以有开头的 '/' 也可以没有。子卷的挂载并不以根卷的挂载为前提。
compress=zlib 用来指定压缩方式为 zlib，还可以是 lzo 和 zstd，如果只写 compress 则采用默认方式（zlib），如果指定的压缩方式不被支持，则不会报错，回退到无压缩。
ssd 指定对固态硬盘优化，实际上可以省略，btrfs 可以自动检测到固态硬盘。

## 内核参数

如果根分区或/root分区位于 btrfs 的子卷，则除了 root 参数，还需要加上 `rootflags=subvol=/path/to/subvolume` 内核参数。例如在 grub.cfg 中：

```
menuentry 'deepin SYSB' {
  search --label --set=root SYSB
  linux /@root-deepin-1/vmlinuz root=LABEL=SYSB rootflags=subvol=@root-deepin-1
  initrd /@root-deepin-1/initrd.img
}
```

## grub
grub 通过 btrfs 模块来支持 btrfs 。

```
insmod btrfs
```
如果根分区或/root分区位于 btrfs 的子卷，grub.cfg 中，直接通过 /path/to/subvolume 来引用子卷中的内核与 initrd 文件即可。参考"内核参数"的例子。

注意，只有较新的 grub (2.0.6+) 才支持 btrfs 的 zstd 压缩。

## 压缩
[2.1]

可以用 compsize 工具查看一个目录下文件的压缩效果：

```
$ sudo compsize -x /var
Processed 17872 files, 9688 regular extents (10297 refs), 14886 inline.
Type       Perc     Disk Usage   Uncompressed Referenced  
TOTAL       61%     1023M         1.6G         1.6G       
none       100%      764M         764M         714M       
zlib        28%      258M         893M         967M
```

-x 参数避免跨越文件系统（否则 compsize 可能报错）

compsize 位于 btrfs-compsize 包。

## 工具
```
sudo btrfs device scan
```

```
btrfs filesystem show

Label: 'SYSB'  uuid: a21675c9-b895-42de-bdb0-1087c63adfa7
        Total devices 1 FS bytes used 82.83GiB
        devid    1 size 137.28GiB used 137.28GiB path /dev/sda8
```

列出子卷：
```
sudo btrfs subvol list /home
```

显示使用量：
```
sudo btrfs filesystem usage /
```

注意 btrfs filesystem|subvolume 的参数是安装点下的任意目录，而不是块设备。用哪个目录做参数是没有区别的，显示的都是整个文件系统的信息。


## 文件系统错误计数
btrfs device stats 显示文件系统经历的IO错误的数量：

````
sudo btrfs device stats /

[/dev/sda8].write_io_errs    0
[/dev/sda8].read_io_errs     0
[/dev/sda8].flush_io_errs    0
[/dev/sda8].corruption_errs  0
[/dev/sda8].generation_errs  0
```

## 文件系统校验/scrub

btrfs scrub 检查元数据和文件内容的校验和，如果有备份，会尝试恢复。与 fscheck 不同，它不能修复文件系统结构错误。

```
$ sudo btrfs scrub start /dev/sda8  # or btrfs scrub start /

scrub started on /dev/sda8, fsid c0480a4d-a637-4da8-bdaf-dc15690a8fb3 (pid=32393)

$ sudo btrfs scrub status /dev/sda8

UUID:             c0480a4d-a637-4da8-bdaf-dc15690a8fb3
Scrub started:    Thu Jun 17 17:10:32 2021
Status:           finished
Duration:         0:00:07
Total to scrub:   3.62GiB
Rate:             528.87MiB/s
Error summary:    no errors found
```

btrfs scrub 默认在后台运行，btrfs scrub status 可以显示最近一次运行的结果。

也可以通过 systemd 单元 btrfs-scrub@.timer 和 btrfs-scrub@.service 来运行它 [1.2] 。


## 反碎片 defragment

mount option autodefrag

btrfs filesystem defragment -r /

## 崩溃恢复/btrfs check

btrfs check 类似其它文件系统的 fscheck 。

btrfs check 目前（2021.6.16）有一定的危险性，可能造成更严重的破坏和数据丢失，所以操作前应先备份数据：
（1）所以如果文件系统还能挂载，以只读模式挂载（-o ro,usebackuproot），并备份数据。
（2）尝试用 mount –o recovery 挂载，并备份数据。
（3）如果不能挂载，用 btrfs restore 设法读出数据。

从崩溃的 btrfs 上可能只能读出部分数据，越新的数据越有可能丢失。

btrfs check 应该在非挂载状态运行。

--repair, --init-csum-tree, --init-extent-tree 都会对文件系统做修改，并且潜在的破坏性依次增大。除非数据已经不重要，或者别无他法，不要使用 --init-extent-tree 选项。
没有以上选项时，btrfs check 只检查，不修改。

sudo btrfs check /dev/sda8

sudo btrfs check --repair /dev/sda8

sudo btrfs check --force --repair --tree-root 154313998336 --super 0 /dev/sda8

--tree-root 是指定根块，来自 btrfs-find-root 。

sudo btrfs check --init-csum-tree /dev/sda8

sudo btrfs check --init-extent-tree /dev/sda8

## 崩溃恢复 mount recovery

```
mount –o recovery /dev/sda /btrfs
```
这个模式会挂载一个最近的正确的 B-tree 

## 从崩溃的文件系统查找可能的根/btrfs-find-root

sudo btrfs-find-root -a /dev/sda8

输出类似这样：
```
Superblock thinks the generation is 1973532
Superblock thinks the level is 1
Well block 153949863936(gen: 1973532 level: 1) seems good, and it matches superblock
Well block 154313998336(gen: 1973531 level: 1) seems good, but generation/level doesn't match, want gen: 1973532 level: 1
Well block 154315079680(gen: 1973530 level: 1) seems good, but generation/level doesn't match, want gen: 1973532 level: 1
Well block 154315128832(gen: 1962714 level: 1) seems good, but generation/level doesn't match, want gen: 1973532 level: 1
```
从前到后（按 gen 递减）的顺序尝试“Well block”，作为参数 -t 传递。

## 从崩溃的文件系统读出文件/ btrfs restore

根据 btrfs-find-root 找到的“Well block”，作为参数 -t 传递：

sudo btrfs restore /dev/sda8 /home/duanyao/backup/btrfs-c/ -vimSxo -t 153949863936 --path-regex "(@home(|/.*))$"

## winbtrfs
