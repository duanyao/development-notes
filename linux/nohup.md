
## 用法
2. To run a command in the foreground:

Runs the command in the foreground and redirects the command output to the nohup.out file if any redirecting filename is not mentioned.

$ nohup bash geekfile.sh

To redirect the output to the output.txt file:

$ nohup bash geekfile.sh > output.txt

It can be brought back to the foreground with the fg command.

$ nohup bash geekfile.sh &
fg

注意事项：
* 如果 nohup 运行在前台，那么防护效果很弱。ssh 会话中，如果遇到客户端崩溃、客户机待机，nohup的进程还是可能收到 SIGINT, SIGTERM, or SIGHUP 等信号而退出。
* 如果 使用 frp + ssh 会话，那么 nohup 没有防护效果，即使 nohup 运行在后台。这种情况下可使用 tmux 。
