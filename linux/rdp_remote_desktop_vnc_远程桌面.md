## 参考资料
[1.1] 11 Best Tools to Access Remote Linux Desktop
  https://www.tecmint.com/best-remote-linux-desktop-sharing-software/

[2.1] xrdp
  http://xrdp.org/
  
[2.2] Debian Bug report logs - #856436 xrdp: client is not connecting when security_layer=tls
  https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=856436

[4.1] SPICE
  https://www.spice-space.org/
[4.2] x11spice
  https://gitlab.freedesktop.org/spice/x11spice
[4.3] spice_redhat_summit_2009
  https://www.spice-space.org/static/docs/spice_redhat_summit_2009.pdf
  
[5.1] Xpra multi-platform screen and application forwarding system "screen for X11" 
  http://xpra.org/
  
[5.2] Xpra - X持久化远程应用
  https://cloud-atlas.readthedocs.io/zh_CN/latest/linux/desktop/xpra.html
  
[6.1] ubuntu 使用tigervnc-server实现远程桌面访问
  https://www.cnblogs.com/liyuanhong/articles/15487147.html
  
## RustDesk

## ToDesk

## X window over ssh

ssh -X 

可行，但运行起来很卡，不具有实用性。

## VNC



### tightvnc

sudo apt-get install tightvncserver

以普通用户身份运行 vncserver ：

```
You will require a password to access your desktops.

Password: 
Verify:   
Would you like to enter a view-only password (y/n)? n

New 'X' desktop is duanyao-laptop:1

Creating default startup script /home/duanyao/.vnc/xstartup
Starting applications specified in /home/duanyao/.vnc/xstartup
Log file is /home/duanyao/.vnc/duanyao-laptop:1.log
```

```
vncserver :1  # 服务器端，通过 ssh 执行。服务器端本地登录或者不登录GUI。

vncviewer duanyao-laptop.local:1  #客户端
```

但是，vncviewer 显示灰色的屏幕，不可用。

## team viewer

## RDP

### xrdp
服务器端。

sudo apt install xrdp
systemd 服务：

  xrdp-sesman.service                                                                         loaded active running   xrdp session manager                                                         
  xrdp.service
  loaded active running   xrdp daemon 

xrdp 在 debian 系中有个 bug：/etc/xrdp/key.pem 无法被 xrdp 进程访问，所以客户端会无法连接。xrdp.service 的日志：

```
1月 04 20:33:37 duanyao-laptop xrdp[11907]: (11907)(140055446546240)[ERROR] Cannot read private key file /etc/xrdp/key.pem: Permission denied
```

解决办法是将 xrdp 用户（xrdp 进程采用的用户）加入 ssl-cert 组：

```
sudo adduser xrdp ssl-cert
sudo systemctl restart xrdp
```

### rdesktop 客户端

```
rdesktop -u user_name remote_host_address
```

### 问题
* 目前（2021.1.4） deepin 20.1 上 remmina rdp 无法连接 xrdp。
* rdesktop 可以连接 xrdp ，但除了输入法以外都是黑屏。

## SPICE
SPICE 是一种网络协议和软件，主要用于远程或本地虚拟机的控制，但也可能用于物理机的控制。
支持的虚拟机类型主要是 QEMU/KVM 。

x11spice 是一种 SPICE 服务器，可以控制当前 x11 会话[4.2]。

xserver-xspice

## xpra
服务器端和客户端都安装xpra：

sudo apt install xpra

启动和控制一个远程应用：
```
xpra start ssh://SERVERUSERNAME@SERVERHOSTNAME/ --start-child=nautilus
xpra attach ssh:serverhostname
```

控制当前会话
```
xpra shadow ssh://user_name@SERVERHOST  # 控制当前会话，自动检测要连接的屏幕.自动检测可能连接到意外的 xserver 上。
xpra shadow ssh://user_name@SERVERHOST/DISPLAY  # 控制其它屏幕的会话, DISPLAY 是 0,1,2 等。
```

xrpa 可以缩放屏幕：alt+shift+{'+'|'-'|'*'}

命令参数（通过 man xpra 查看）：

--encoding={jpeg, png, png/P, png/L, webp, rgb, vp8, vp9, h264 and h265}
  默认为自动选择。最好选 vp8，h264总是出现画面模糊，即使设置 quality=lossless 。

--bandwidth-limit={1Mbps, 500K}
  默认为

--video-scaling=on|off|SCALING
  How  much  automatic  video downscaling should be used, from 1 (rarely) to 100 (aggressively), 0 to disable.

--quality=VALUE
  This  option  sets a fixed image compression quality for lossy encodings (jpeg, webp, h264/h265 and vp8/vp9). Values range from 1 (lowest quality, high compression - generally unusable) to 100  (highest  quality,  low  compression).
  100 是 lossless（无损）

例子：
xpra shadow ssh://duanyao@duanyao-laptop.local/0 --encoding=vp8 --bandwidth-limit=15Mbps --quality=100 --debug=INFO

配置文件：

```
/etc/xpra/conf.d/*
/etc/xpra/xorg-uinput.conf
/etc/xpra/xorg.conf
/etc/xpra/xpra.conf

~/.xpra/xpra.conf  # 覆盖 /etc/xpra/ 下的同名参数
```
配置文件的语法是 `option=value` ，选项与命令行参数相同，去掉前导的 `--` 即可。

注意，在托盘菜单中修改的参数不会保存，必须手动修改 `~/.xpra/xpra.conf` 。

日志文件（仅服务器端有）：
```
/run/user/1000/xpra/:0.log
```

## remmina
remmina 是多协议远程桌面客户端。要使用rdp，还应该安装 remmina-plugin-rdp ，要使用 spice，则安装 remmina-plugin-spice 。
