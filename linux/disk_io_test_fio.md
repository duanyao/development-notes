## 参考资料

[2.1] linux: how to simulate hard disk latency? I want to increase iowait value without using a cpu power
https://serverfault.com/questions/523509/linux-how-to-simulate-hard-disk-latency-i-want-to-increase-iowait-value-withou

[2.2] Device-Mapper's "delay" target delays reads and/or writes
https://www.kernel.org/doc/html/latest/admin-guide/device-mapper/delay.html
https://www.kernel.org/doc/Documentation/device-mapper/delay.txt

[3.1] blktrace - generate traces of the i/o traffic on block devices
https://linux.die.net/man/8/blktrace

[4.1] dm-flakey
https://www.kernel.org/doc/html/latest/admin-guide/device-mapper/dm-flakey.html


## fio

### 例子

顺序读600G（同步）：

```
fio -filename=disk_test/d1.raw -direct=1 -iodepth 1 -thread -rw=read -ioengine=psync -bs=16k -size=20G -numjobs=30 -runtime=1000 -group_reporting -name=test-read
```
-size 是指每个 job 的 io 量，-numjobs 是 job 数量，因此总 io 量是两者的乘积，即 600GiB 。
每个 job 会用一个线程跑，因此总共 30 个线程。-name 是本次测试的名字，可以随便起。

输出
```
...
Run status group 0 (all jobs):
   READ: bw=874MiB/s (916MB/s), 874MiB/s-874MiB/s (916MB/s-916MB/s), io=600GiB (644GB), run=702961-702961msec

Disk stats (read/write):
  nvme0n1: ios=39318186/867, merge=0/348, ticks=20983126/446, in_queue=20983675, util=100.00%
```

顺序读140G，异步：

```
fio -filename=disk_test/d1.raw -direct=1 -iodepth 64 -thread -rw=read -ioengine=libaio -bs=16k -size=20G -numjobs=7 -runtime=1000 -group_reporting -name=test-read

fio -filename=disk_test/d1.raw -direct=1 -iodepth 64 -thread -rw=read -ioengine=libaio -bs=1M -size=60G -numjobs=10 -runtime=1000 -group_reporting -name=test-read
```

随机读140G/600G，异步：
```
fio -filename=disk_test/d1.raw -direct=1 -iodepth 64 -thread -rw=randread -ioengine=libaio -bs=16k -size=20G -numjobs=7 -runtime=1000 -group_reporting -name=test-read

fio -filename=disk_test/d1.raw -direct=1 -iodepth 64 -thread -rw=randread -ioengine=libaio -bs=16k -size=60G -numjobs=10 -runtime=1000 -group_reporting -name=test-read
```

顺序写140G，异步：

```
fio --filename=disk_test/d1.raw -iodepth=64 -ioengine=libaio -direct=1 -rw=write -bs=1m -size=20g -numjobs=7 -runtime=1000 -group_reporting -name=test-write
```

随机写140G/280G，异步：

```
fio --filename=disk_test/d1.raw -iodepth=64 -ioengine=libaio -direct=1 -rw=randwrite -bs=1m -size=20g -numjobs=7 -runtime=1000 -group_reporting -name=test-write

fio --filename=disk_test/d1.raw -iodepth=64 -ioengine=libaio -direct=1 -rw=randwrite -bs=1m -size=40g -numjobs=7 -runtime=1000 -group_reporting -name=test-write
```

小批量写入：

```
fio --filename=disk_test/d1.raw -iodepth=64 -ioengine=libaio -direct=1 -rw=write -bs=1m -size=1g -numjobs=5 -runtime=1000 -group_reporting -name=test-write

fio --filename=disk_test/d1.raw -iodepth=64 -ioengine=libaio -direct=1 -rw=randwrite -bs=1m -size=1g -numjobs=5 -runtime=1000 -group_reporting -name=test-write
```

## m.2 nvme 延长线故障测试

随机写140G/280G，异步：

```
fio --filename=disk_test/d1.raw -iodepth=64 -ioengine=libaio -direct=1 -rw=randwrite -bs=1m -size=20g -numjobs=7 -runtime=1000 -group_reporting -name=test-write

fio --filename=disk_test/d1.raw -iodepth=64 -ioengine=libaio -direct=1 -rw=randwrite -bs=1m -size=40g -numjobs=7 -runtime=1000 -group_reporting -name=test-write
```
输出：
```
fio-3.16
Starting 7 processes
fio: io_u error on file d1.raw: Input/output error: write offset=6506414080, buflen=1048576

Message from syslogd@gtrd at Jan  4 14:39:39 ...
 kernel:[ 3247.411167] EXT4-fs (nvme0n1p4): failed to convert unwritten extents to written extents -- potential data loss!  (inode 22675690, error -30)
fio: pid=6049, err=5/file:io_u.c:1787, func=io_u error, error=Input/output error
fio: io_u error on file d1.raw: Input/output error: write offset=6231687168, buflen=1048576
fio: pid=6053, err=5/file:io_u.c:1787, func=io_u error, error=Input/output error
fio: io_u error on file d1.raw: Input/output error: write offset=6700400640, buflen=1048576
fio: pid=6054, err=5/file:io_u.c:1787, func=io_u error, error=Input/output error
fio: io_u error on file d1.raw: Input/output error: write offset=7559184384, buflen=1048576
fio: pid=6050, err=5/file:io_u.c:1787, func=io_u error, error=Input/output error
fio: io_u error on file d1.raw: Input/output error: write offset=7041187840, buflen=1048576
fio: pid=6052, err=5/file:io_u.c:1787, func=io_u error, error=Input/output error
fio: io_u error on file d1.raw: Input/output error: write offset=6242172928, buflen=1048576
fio: io_u error on file d1.raw: Input/output error: write offset=6479151104, buflen=1048576
fio: pid=6051, err=5/file:io_u.c:1787, func=io_u error, error=Input/output error
fio: pid=6048, err=5/file:io_u.c:1787, func=io_u error, error=Input/output error
Connection to gtrd.local closed by remote host.
Connection to gtrd.local closed.
```

开机后直接测试，M.2_1，PCI 1.0/3.0，写入进度 17.9-18.2% （约25GiB）硬盘就会出错，可重复。

开机后直接测试，M.2_2，PCI 3.0，写入进度 100% + 60% （约25GiB）硬盘就会出错，可重复。

开机后直接测试，M.2_2，PCI 1.0，写入280 GiB 的 100%（约280 GiB）没有报错。改为 PCI 3.0，继续写入280 GiB的 48%，报错。

如果先测读后测写，则写出错可能提前，如先读 600G，再写 140G，则写出错进度 11.2% 。

出错后短时间内（1-5min），硬盘可能无法启动，甚至BIOS可能找不到启动项（虽然nvme设置中硬盘仍可见），或者在BIOS中“高级-nvme设置-运行设备自检”会卡死。关机后等待几分钟可解决，但等待时间似乎有较大随机性。

错误日志（需要把 /var/log 挂载到另一个硬盘上，才能保存日志）：

```
1月 05 13:38:02 gtrd kernel: nvme nvme0: controller is down; will reset: CSTS=0xffffffff, PCI_STATUS=0x10
1月 05 13:38:02 gtrd kernel: nvme 0000:03:00.0: enabling device (0000 -> 0002)
1月 05 13:38:02 gtrd kernel: nvme nvme0: Removing after probe failure status: -19
1月 05 13:38:02 gtrd kernel: blk_update_request: I/O error, dev nvme0n1, sector 1124030464 op 0x1:(WRITE) flags 0xc800 phys_seg 3 prio class 0
1月 05 13:38:02 gtrd kernel: blk_update_request: I/O error, dev nvme0n1, sector 1155052544 op 0x1:(WRITE) flags 0x8800 phys_seg 3 prio class 0
1月 05 13:38:02 gtrd kernel: blk_update_request: I/O error, dev nvme0n1, sector 1097178112 op 0x1:(WRITE) flags 0x8800 phys_seg 4 prio class 0
1月 05 13:38:02 gtrd kernel: nvme0n1: detected capacity change from 2000409264 to 0
1月 05 13:38:02 gtrd kernel: blk_update_request: I/O error, dev nvme0n1, sector 1499234976 op 0x1:(WRITE) flags 0x103000 phys_seg 1 prio class 0
1月 05 13:38:02 gtrd kernel: blk_update_request: I/O error, dev nvme0n1, sector 1141394432 op 0x1:(WRITE) flags 0x8800 phys_seg 3 prio class 0
1月 05 13:38:02 gtrd kernel: blk_update_request: I/O error, dev nvme0n1, sector 1143260160 op 0x1:(WRITE) flags 0x8800 phys_seg 4 prio class 0
1月 05 13:38:02 gtrd kernel: blk_update_request: I/O error, dev nvme0n1, sector 1139475456 op 0x1:(WRITE) flags 0x8800 phys_seg 3 prio class 0
1月 05 13:38:02 gtrd kernel: blk_update_request: I/O error, dev nvme0n1, sector 1103060992 op 0x1:(WRITE) flags 0xc800 phys_seg 3 prio class 0
1月 05 13:38:02 gtrd kernel: blk_update_request: I/O error, dev nvme0n1, sector 1097177088 op 0x1:(WRITE) flags 0xc800 phys_seg 3 prio class 0
1月 05 13:38:02 gtrd kernel: Buffer I/O error on dev nvme0n1p4, logical block 101187924, lost async page write
1月 05 13:38:02 gtrd kernel: blk_update_request: I/O error, dev nvme0n1, sector 1141393408 op 0x1:(WRITE) flags 0xc800 phys_seg 3 prio class 0
1月 05 13:38:02 gtrd kernel: Buffer I/O error on dev nvme0n1p4, logical block 90702098, lost async page write
1月 05 13:38:02 gtrd kernel: Aborting journal on device nvme0n1p4-8.
1月 05 13:38:02 gtrd kernel: Buffer I/O error on dev nvme0n1p4, logical block 81821696, lost sync page write
1月 05 13:38:02 gtrd kernel: JBD2: Error -5 detected when updating journal superblock for nvme0n1p4-8.
1月 05 13:38:02 gtrd systemd[1]: Stopped target Local File Systems.
1月 05 13:38:02 gtrd systemd[1]: Unmounting /boot/efi...
1月 05 13:38:02 gtrd pulseaudio[1268]: X connection to :0 broken (explicit kill or server shutdown).
1月 05 13:38:02 gtrd systemd[1]: Stopping Session 1 of user rd.
1月 05 13:38:02 gtrd systemd[1]: Stopping Session 3 of user rd.
1月 05 13:38:02 gtrd kernel: JBD2: Error while async write back metadata bh 90702098.
1月 05 13:38:02 gtrd kernel: JBD2: Error while async write back metadata bh 101187924.
1月 05 13:38:02 gtrd kernel: EXT4-fs error (device nvme0n1p4): __ext4_find_entry:1524: inode #22675457: comm bash: reading directory lblock 0
1月 05 13:38:02 gtrd kernel: Buffer I/O error on dev nvme0n1p4, logical block 0, lost sync page write
1月 05 13:38:02 gtrd kernel: EXT4-fs (nvme0n1p4): I/O error while writing superblock
1月 05 13:38:02 gtrd kernel: EXT4-fs error (device nvme0n1p4): __ext4_find_entry:1524: inode #22675457: comm bash: reading directory lblock 0
1月 05 13:38:02 gtrd kernel: Buffer I/O error on dev nvme0n1p4, logical block 0, lost sync page write
1月 05 13:38:02 gtrd kernel: EXT4-fs (nvme0n1p4): I/O error while writing superblock
1月 05 13:38:02 gtrd kernel: EXT4-fs error (device nvme0n1p4): __ext4_find_entry:1524: inode #22675457: comm bash: reading directory lblock 0
1月 05 13:38:02 gtrd kernel: Buffer I/O error on dev nvme0n1p4, logical block 0, lost sync page write
1月 05 13:38:02 gtrd kernel: EXT4-fs (nvme0n1p4): I/O error while writing superblock

1月 05 13:38:03 gtrd systemd[1]: home.mount: Unit is bound to inactive unit dev-disk-by\x2duuid-b7505d46\x2d0515\x2d4757\x2d8598\x2d4af274f5f3a3.device. Stopping, too.
1月 05 13:38:03 gtrd systemd[1]: home.mount: Unit is bound to inactive unit dev-disk-by\x2duuid-b7505d46\x2d0515\x2d4757\x2d8598\x2d4af274f5f3a3.device. Stopping, too.
1月 05 13:38:03 gtrd systemd[1]: home.mount: Unit is bound to inactive unit dev-disk-by\x2duuid-b7505d46\x2d0515\x2d4757\x2d8598\x2d4af274f5f3a3.device. Stopping, too.
1月 05 13:38:03 gtrd systemd[1]: home.mount: Unit is bound to inactive unit dev-disk-by\x2duuid-b7505d46\x2d0515\x2d4757\x2d8598\x2d4af274f5f3a3.device. Stopping, too.
1月 05 13:38:03 gtrd systemd[1]: home.mount: Unit is bound to inactive unit dev-disk-by\x2duuid-b7505d46\x2d0515\x2d4757\x2d8598\x2d4af274f5f3a3.device. Stopping, too.
1月 05 13:38:02 gtrd systemd[1]: Stopping Session 4 of user rd.
1月 05 13:38:03 gtrd gvfsd[1290]: A connection to the bus can't be made
1月 05 13:38:03 gtrd tracker-miner-fs[1270]: Received signal:15->'已终止'
1月 05 13:38:03 gtrd tracker-miner-fs[1270]: OK
1月 05 13:38:03 gtrd gnome-session[1637]: gnome-session-binary[1637]: WARNING: GsmSessionSave: Failed to create directory /home/rd/.config/gnome-session/saved-session:  >
1月 05 13:38:03 gtrd gnome-session[1637]: gnome-session-binary[1637]: WARNING: GsmSessionSave: could not create directory for saved session: /home/rd/.config/gnome-sessi>
1月 05 13:38:03 gtrd gnome-session[1637]: gnome-session-binary[1637]: WARNING: GsmSessionSave: cannot create saved session directory
1月 05 13:38:03 gtrd gnome-terminal-server[3162]: Failed to load cookie file from cookie: 输入/输出错误

1月 05 13:38:02 gtrd sshd[2658]: pam_unix(sshd:session): session closed for user rd
1月 05 13:38:03 gtrd at-spi-bus-launcher[1696]: X connection to :0 broken (explicit kill or server shutdown).
1月 05 13:38:02 gtrd unknown[1651]: gnome-shell: Fatal IO error 11 (资源暂时不可用) on X server :0.
1月 05 13:38:02 gtrd sshd[2657]: pam_unix(sshd:session): session closed for user rd
1月 05 13:38:02 gtrd udisksd[998]: Error statting /dev/nvme0n1p3: No such file or directory
1月 05 13:38:02 gtrd ntfs-3g[769]: Unmounting /dev/nvme0n1p1 ()
1月 05 13:38:02 gtrd udisksd[998]: Error statting /dev/nvme0n1p3: No such file or directory
1月 05 13:38:02 gtrd ntfs-3g[769]: Failed to sync device /dev/nvme0n1p1: 输入/输出错误
1月 05 13:38:02 gtrd udisksd[998]: Error statting /dev/nvme0n1p2\040(deleted): No such file or directory
1月 05 13:38:02 gtrd ntfs-3g[769]: Failed to close volume /dev/nvme0n1p1: 输入/输出错误
1月 05 13:38:02 gtrd systemd[1233]: boot-efi.mount: Succeeded.
1月 05 13:38:02 gtrd dbus-daemon[1280]: [session uid=1000 pid=1280] Activating via systemd: service name='org.freedesktop.Tracker1' unit='tracker-store.service' requeste>
1月 05 13:38:02 gtrd systemd[1233]: Stopped target GNOME X11 Session (session: ubuntu).
1月 05 13:38:02 gtrd polkitd(authority=local)[974]: Unregistered Authentication Agent for unix-session:1 (system bus name :1.50, object path /org/freedesktop/PolicyKit1/>
1月 05 13:38:02 gtrd systemd[1233]: Stopped target Current graphical user session.
1月 05 13:38:02 gtrd dbus-daemon[1280]: [session uid=1000 pid=1280] Successfully activated service 'org.freedesktop.Tracker1'
1月 05 13:38:02 gtrd systemd[1233]: unicast-local-avahi.path: Succeeded.
1月 05 13:38:02 gtrd gnome-session-binary[1637]: WARNING: GsmSessionSave: Failed to create directory /home/rd/.config/gnome-session/saved-session: 输入/输出错误
1月 05 13:38:03 gtrd pulseaudio[3495]: W: [pulseaudio] core-util.c: Failed to open configuration file '/home/rd/.config/pulse//daemon.conf': 输入/输出错误
1月 05 13:38:03 gtrd pulseaudio[3495]: W: [pulseaudio] daemon-conf.c: 打开配置文件失败：输入/输出错误
1月 05 13:38:02 gtrd systemd[1]: boot-efi.mount: Succeeded.
1月 05 13:38:02 gtrd gnome-session-binary[1637]: WARNING: GsmSessionSave: could not create directory for saved session: /home/rd/.config/gnome-session/saved-session
1月 05 13:38:02 gtrd systemd[1]: Unmounted /boot/efi.
1月 05 13:38:02 gtrd gnome-session-binary[1637]: WARNING: GsmSessionSave: cannot create saved session directory
1月 05 13:38:02 gtrd systemd[1233]: Stopped Path trigger for Avahi .local domain notifications.
1月 05 13:38:02 gtrd gdm3[1095]: Freeing conversation 'gdm-autologin' with active job
1月 05 13:38:02 gtrd systemd[1233]: update-notifier-crash.path: Succeeded.
1月 05 13:38:02 gtrd dbus-daemon[3505]: [session uid=1000 pid=3505] AppArmor D-Bus mediation is enabled
1月 05 13:38:02 gtrd systemd[1233]: Stopped Path trigger for Apport crash notifications.
1月 05 13:38:02 gtrd dbus-daemon[3505]: Cannot setup inotify for '/home/rd/.local/share/dbus-1/services'; error 'Input/output error'
1月 05 13:38:02 gtrd systemd[1233]: update-notifier-release.path: Succeeded.
1月 05 13:38:02 gtrd gdm-launch-environment][3462]: pam_unix(gdm-launch-environment:session): session opened for user gdm by (uid=0)
1月 05 13:38:02 gtrd systemd-logind[994]: Session 1 logged out. Waiting for processes to exit.
```

CSTS=0xffffffff, PCI_STATUS=0x10 可能的含义是（ https://serverfault.com/a/1067053 ）：


The line kernel: [158430.895045] nvme nvme1: controller is down; will reset: CSTS=0xffffffff, PCI_STATUS=0x10 means that the NVMe disk controller was not responding and was reset by the NVMe driver to recover communication with the device.

Such issues can be caused by:

    malfunctioning hardware
    spurious power (ie: bad PSU)
    too aggressive PCIe Active State Power Management (ASPM)

Putting aside bad hardware, you can try disabling ASPM with the kernel boot command line pcie_aspm=off

Same as OP, I was under the impression that the error arose because of high I/O, but it seems like it was rather the hardware going into lower power and performance mode after some time.

So if you come across this issue, take a look at your BIOS Power settings and turn the knobs and maybe this problem will go away for you too.

类似问题：

Cache pool BTRFS missing device Samsung 980
https://forums.unraid.net/topic/116886-cache-pool-btrfs-missing-device-samsung-980/

Proxmox just died with: nvme nvme0: controller is down; will reset: CSTS=0xffffffff, PCI_STATUS=0x10
https://forum.proxmox.com/threads/proxmox-just-died-with-nvme-nvme0-controller-is-down-will-reset-csts-0xffffffff-pci_status-0x10.88604/

Samsung PM951 NVMe sudden controller death
https://bugzilla.kernel.org/show_bug.cgi?id=195039

### 可能的解决办法

软件：关闭 Autonomous Power State Transition（APST）

adding the option nvme_core.default_ps_max_latency_us=0 to the Grub Linux launch command
nvme get-feature -f 0x0c -H /dev/nvme0 测试APST是否关闭。

  
