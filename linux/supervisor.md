## 参考资料
[1] http://supervisord.org/

[2] 使用 Supervisor 来管理进程
  http://wiki.jikexueyuan.com/project/docker-technology-and-combat/supervisor.html

[3] Docker+supervisor+tomcat+nginx+php-fpm配置与注意事项  
  https://segmentfault.com/a/1190000005026434

[4] Docker, Supervisord and logging - how to consolidate logs in docker logs?
  https://stackoverflow.com/questions/18683810/docker-supervisord-and-logging-how-to-consolidate-logs-in-docker-logs

## 安装
### python pip

```
pip install supervisor
```
可以全局安装（root用户），也可安装到 venv 。

有 python，但没有 pip:
```
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py
pip install supervisor
```

### redhat 系 rpm
centos 8 上 yum 可安装。

### debian 系 deb
包名 supervisor 。
在 ubuntu 上，supervisor deb安装后会自启动（通过systemd）。

### 从源码安装

```
python setup.py install
```

## 配置文件
supervisord 启动时可指定配置文件路径：

```
supervisord -c /etc/supervisor/supervisord.conf
```

如果不指定配置文件路径，supervisord 先在当前目录下寻找 supervisord.conf，如果没有，则寻找 `/etc/supervisord.conf`

配置文件样例：
```
[supervisord]
nodaemon=true
environment=LANG=en-US.UTF-8,LD_LIBRARY_PATH="/opt/intel/openvino/opencv/lib"

# supervisord 自身的事件，以及被管理的程序的状态转换事件，被写入此日志。默认值为 $CWD/supervisord.log 。logfile=syslog 写入 syslog 。
logfile=/var/log/supervisord.log
# 达到 logfile_maxbytes 开始轮转
logfile_maxbytes=10000000
# 轮转达到 logfile_backups 开始删除旧日志文件
logfile_backups=10

# supervisord 自身的日志也可以输出到标准输出：
# logfile=/dev/stdout
# 如果输出到 stdout（logfile=/dev/stdout），应当将 logfile_maxbytes 设置为 0
# logfile_maxbytes=0

# 配置 supervisorctl 命令行工具
[supervisorctl]
# 默认值 http://localhost:9001 。要与 [unix_http_server] 或者 [inet_http_server] 对应。
serverurl = unix:///tmp/supervisor.sock

# supervisord 可启动一个 unix_http_server ，与 supervisorctl 命令行工具 交互。
[unix_http_server]
file=/tmp/supervisor.sock
chmod=0700

# 一般的使用方式下，照抄此 rpcinterface:supervisor 段即可。
[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface

[program:aa]
command=/usr/bin/node /usr/local/lib/AA.js
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0

[program:bb]
environment=LANG=zh-CN.UTF-8  # 覆盖 LANG=en-US.UTF-8
command=/usr/bin/node /usr/local/lib/BB.js

[program:cc]
environment=object_detection_config=/service/pedestrian_detection/object_detection_config.json
# `%(ENV_XXX)s` 用来展开环境变量 XXX。
command=gunicorn -w %(ENV_WORKER_COUNT)s -b 0.0.0.0:9100 --timeout 120 object_detection_service:app1 --pythonpath /service/object_detection/
```

在容器里面，supervisord 的 nodaemon=true 是必要的。也可以在启动命令中加上 `-n, --nodaemon`，效果是一样的。

k8s 容器的启动命令写 `["/usr/bin/supervisord"]` 即可。
```
containers:
        - command:
            - supervisord
            # 指定配置文件的路径。本来这个路径是默认的，但 centos 8 自带的 supervisord 4.2.2 似乎不认
            - -c
            - /etc/supervisor/supervisord.conf
          env:
            - name: aiparents_config
              value: production
            - name: LANG
              value: zh-CN.UTF-8
```

## 管理工具

命令行工具 supervisorctl 可以控制 supervisord 管理的进程。

```
supervisorctl -s unix:///tmp/supervisor.sock # 显示各个服务的状态，并进入交互模式
supervisorctl -s unix:///tmp/supervisor.sock status # 显示各个服务的状态
supervisorctl -s unix:///tmp/supervisor.sock restart faceProcess # 重启服务
supervisorctl -s unix:///tmp/supervisor.sock restart all # 重启全部服务
supervisorctl -s unix:///tmp/supervisor.sock stop faceProcess # 停止服务
supervisorctl -s unix:///tmp/supervisor.sock update # 如果修改了 supervisord.conf ，重新加载配置
```

`-s unix:///tmp/supervisor.sock` 指定与通信的渠道。如果不指定，默认是 `unix:///run/supervisor/supervisor.sock` 。
通信的渠道可以在 supervisord.conf 中配置。

其它命令：

```
add    exit      open  reload  restart   start   tail   
avail  fg        pid   remove  shutdown  status  update 
clear  maintail  quit  reread  signal    stop    version
```

tail 是读日志，但还没有成功。

## 环境变量

supervisord 会把自己的进程继承的环境变量传递给它管理的进程。因此也可以在 k8s 配置文件中指定全局环境变量。

在配置文件 [supervisord] 段中的 environment 属性指定的环境变量会传递给所有被管理的进程，并可以被 [program:xx] 段中的 environment 属性指定的环境变量覆盖。

supervisord 启动被管理进程时不运行 shell 配置文件，所以其中指定的环境变量没有任何效果。

## 日志
如果不做日志相关设置，supervisor 接收被管理进程的标准输出，然后丢弃。supervisor 自身的日志则输出到标准输出。

如果设置 `[supervisord] childlogdir=/var/log/supervisor` ，则被管理进程的标准输出转发到这个目录下的文件，自动命名。

通过配置属性 stdout_logfile, stderr_logfile 可以指定将被管理进程的标准输出转发到的文件。
stdout_logfile/stderr_logfile 还可以有一些特殊值：

/dev/stdout 和 /dev/stderr : 转发到 supervisord 的标准输出，这需要 supervisord 本身以非 daemon 模式运行。
syslog : 转发到 syslog。

如果转发到普通文件，则还应该设置 stdout_logfile_maxbytes/ stderr_logfile_maxbytes，限制文件大小。

对于容器中的supervisor，建议对每个程序设置

```
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
```
不过，这就是单纯的转发，并不能区分哪一行是从哪个服务发出来的。
