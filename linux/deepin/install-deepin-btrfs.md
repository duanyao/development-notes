## 步骤

### 初始安装

用官方安装盘，安装到目标分区（/dev/sda8）。安装过程中，目标分区格式化为 ext4，单一分区模式。
deepin 20.2.1 安装后大小约为 6.5GB 。

### 备份目标分区的文件

重启进入另一个 linux 系统。挂载目标分区（/dev/sda8 -> /media/duanyao/cff2e9bc-dc5f-4fc1-ae40-2319a56218da/ ）。
准备一个备份目录（ ~/backup/deepin-0617-init/ ），剩余空间应足够，文件系统为 posix 兼容。
执行复制：
sudo rsync -aAHXxvh /media/duanyao/cff2e9bc-dc5f-4fc1-ae40-2319a56218da/ ~/backup/deepin-0617-init/

参数的含义见 rsync.md 。

### 制作 btrfs 文件系统
sudo mkdir -p /media/TOP
sudo umount /dev/sda8
sudo mkfs.btrfs -f -L SYSB /dev/sda8
sudo mount -t btrfs /dev/sda8 /media/TOP
sudo btrfs subvolume create /media/TOP/@root-deepin-1
sudo btrfs subvolume create /media/TOP/@home
sudo btrfs subvolume list -p /media/TOP

ID 256 gen 9 parent 5 top level 5 path @root-deepin-1
ID 257 gen 7 parent 5 top level 5 path @home

sudo umount /dev/sda8

### 挂载和还原目标分区

sudo mkdir -p /media/ROOT/
sudo mount /dev/sda8 -o subvol=/@root-deepin-1,relatime,compress=zlib /media/ROOT/
sudo mkdir -p /media/ROOT/home
sudo mount /dev/sda8 -o subvol=/@home,relatime,compress=zlib /media/ROOT/home

sudo rsync -aAHXxvh ~/backup/deepin-0617-init/ /media/ROOT/

### 建立快照

sudo btrfs subvolume snapshot -r /media/TOP/@root-deepin-1 /media/TOP/@root-deepin-1=2021-06-17=1
sudo btrfs subvolume snapshot -r /media/TOP/@home /media/TOP/@home=2021-06-17=1
sudo btrfs subvolume snapshot -r /media/TOP/ /media/TOP/@=2021-06-17=1

### 恢复备份的配置文件

sudo rsync -a /home/duanyao/backup/btrfs-c/@root-deepin-1/etc/fstab /media/ROOT/etc/

sudo rsync -a /home/duanyao/backup/btrfs-c/@root-deepin-1/etc/apt/sources.list /media/ROOT/etc/apt/
sudo rsync -a /home/duanyao/backup/btrfs-c/@root-deepin-1/etc/apt/preferences /media/ROOT/etc/apt/
sudo rsync -a /home/duanyao/backup/btrfs-c/@root-deepin-1/etc/apt/sources.list.d/debian-10-backports.list /media/ROOT/etc/apt/sources.list.d/
sudo rsync -a /home/duanyao/backup/btrfs-c/@root-deepin-1/etc/apt/sources.list.d/debian-11-backports.list /media/ROOT/etc/apt/sources.list.d/
sudo rsync -av /home/duanyao/backup/btrfs-c/@root-deepin-1/etc/apt/sources.list.d/debian-sid-backports.list /media/ROOT/etc/apt/sources.list.d/
sudo rsync -a /home/duanyao/backup/btrfs-c/@root-deepin-1/etc/apt/sources.list.d/docker-ce-ustc.list /media/ROOT/etc/apt/sources.list.d/
sudo rsync -a /home/duanyao/backup/btrfs-c/@root-deepin-1/etc/apt/sources.list.d/wine.list /media/ROOT/etc/apt/sources.list.d/

### 恢复部分用户文件(1) 
deepin-terminal .ssh

sudo rsync -a /home/duanyao/.config/deepin/deepin-terminal/  /media/ROOT/home/duanyao/.config/deepin/deepin-terminal/
sudo rsync -a /home/duanyao/.ssh/  /media/ROOT/home/duanyao/.ssh/

### 修正 grub 
#### 修正 grub (1)
```
sudo mount /dev/sda8 -o subvol=/@root-deepin-1,relatime,compress=zlib /media/ROOT/
sudo mount /dev/sda8 -o subvol=/@home,relatime,compress=zlib /media/ROOT/home
sudo mount --bind /dev /media/ROOT/dev
sudo mount --bind /proc /media/ROOT/proc
sudo mount --bind /sys /media/ROOT/sys
sudo chroot /media/ROOT/

# 在 chroot 环境：
mount /dev/sda1 /boot/efi
grub-install --target=x86_64-efi
backend_syslog.go:45: <info> syslog is not available: Unix syslog delivery error
```
似乎卡在这一步不动了，可能已经执行了部分操作。

#### 修正 grub（2）

修改 ESP 分区的 /EFI/deepin/grub.cfg ，原来是：

```
search.fs_uuid 8fc0da8b-1dfe-4ab3-a7e9-a0f714e1d341 root hd0,gpt4 
set prefix=($root)'/boot/grub'
configfile $prefix/grub.cfg
```
改为：

```
search.fs_uuid c0480a4d-a637-4da8-bdaf-dc15690a8fb3 root hd0,gpt8 
set prefix=($root)'/@root-deepin-1/boot/grub'
configfile $prefix/grub.cfg
```

失败了，efi grub 没有加载这个配置文件，而是加载了 /boot/grub/grub.cfg 。这与 deepin live cd efi grub 的行为类似。

#### 修正 grub (3)

修改 ESP 分区的 /boot/efi/boot/grub/grub.cfg :

```
search.fs_uuid c0480a4d-a637-4da8-bdaf-dc15690a8fb3 root hd0,gpt8
linux /@root-deepin-1/boot/vmlinuz-5.11.13-amd64-desktop root=LABEL=SYSB rootflags=subvol=@root-deepin-1
initrd /@root-deepin-1/boot/initrd.img-5.11.13-amd64-desktop
```
失败了，自动进入 grub shell ，原因不明。

#### 修正 grub (4)
修改 ESP 分区的 /boot/efi/boot/grub/grub.cfg :
```
set timeout=3

insmod part_gpt
insmod ext2
insmod efi_gop
insmod efi_uga
insmod video_bochs
insmod video_cirrus

insmod loopback
insmod part_msdos
insmod fat
insmod ntfs
insmod ntfscomp
insmod btrfs

#...

menuentry 'deepin SYSB2' {
  search --label --set=root SYSB
  linux /@root-deepin-1/boot/vmlinuz-5.11.13-amd64-desktop root=LABEL=SYSB rootflags=subvol=@root-deepin-1
  initrd /@root-deepin-1/boot/initrd.img-5.11.13-amd64-desktop
}

```

成功了。

### 恢复部分用户文件(2)

cd /media/duanyao/SYSX/home/duanyao

sudo rsync -av .mozilla/firefox/pndbdsj6.default-1415537172132 .mozilla/firefox/profiles.ini  ~/.mozilla/firefox/

mkdir -p ~/.kube/
sudo rsync -av .kube/config ~/.kube/


sudo rsync -av bin/ ~/bin/
sudo rsync -av project/proxy_pac_cn ~/project/


### 恢复部分用户文件(3)

cd /media/duanyao/SYSX/home/duanyao/backup/btrfs-c/@home/duanyao

sudo rsync -av bin/ ~/bin/
sudo rsync -av .pip/ ~/.pip/
sudo rsync -av .config/gtk-3.0/bookmarks ~/.config/gtk-3.0/bookmarks
sudo rsync -av .docker/ ~/.docker/

### 恢复部分配置文件
sudo rsync -av /media/duanyao/SYSX/home/duanyao/backup/btrfs-c/@root-deepin-1/etc/modprobe.d/nobeep.conf /etc/modprobe.d/
sudo rsync -av /media/duanyao/SYSX/home/duanyao/backup/btrfs-c/@root-deepin-1/etc/apt/sources.list.d/debian-sid.list.save /etc/apt/sources.list.d/debian-sid.list
sudo rsync -av /media/duanyao/SYSX/home/duanyao/backup/btrfs-c/@root-deepin-1/etc/apt/sources.list.d/cuda.list /etc/apt/sources.list.d/cuda.list
sudo rsync -av /media/duanyao/SYSX/home/duanyao/backup/btrfs-c/@root-deepin-1/etc/apt/sources.list.d/cudnn.list /etc/apt/sources.list.d/cudnn.list

sudo apt update
sudo apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 7EA0A9C3F273FCD8 76F1A20FF987672F F60F4B3D7FA2AF80 F60F4B3D7FA2AF80


sudo rsync -av /media/duanyao/SYSX/etc/systemd/user/ /etc/systemd/user/

### 安装软件
sudo apt install autossh tcpdump wireshark tmux avahi-daemon traceroute mtr network-manager-gnome
sudo apt install atop lsof psensor dconf-editor cgroup-tools earlyoom gnome-disk-utility
sudo apt install manpages-dev tortoisehg git git-gui git-svn subversion build-essential automake libtool cmake gdb golang-go meld  strace moreutils time dh-make
sudo apt install apt-file cpupower-gui timidity

sudo apt install nautilus=3.26.3.1-1 nautilus-data=3.26.3.1-1 libnautilus-extension1a=3.26.3.1-1 nautilus-open-terminal
#sudo apt install unzip=6.0-31deepin
sudo apt-mark hold nautilus nautilus-data libnautilus-extension1a unzip

sudo apt install kate wxhexeditor

sudo aptitude install --without-recommends cuda-toolkit-11-3
sudo aptitude install --without-recommends libcudnn8 libcudnn8-dev

sudo apt install btrfs-compsize

#### node.js

curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
nvm install v15
npm config set registry https://registry.npm.taobao.org

#### java

curl -s "https://get.sdkman.io" | bash
source "/home/duanyao/.sdkman/bin/sdkman-init.sh"
sdk install java 11.0.11.fx-zulu

#### python
参照 python.md 中 “从源码手工生成 debian control 文件” 。
sudo apt install fakeroot dh-make build-essential zlib1g-dev libbz2-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev libsqlite3-dev tcl-dev tk-dev

dpkg-buildpackage --no-sign --jobs=4 --no-pre-clean

sudo dpkg -i ../python3.9-all-in-one_3.9.2-1_amd64.deb

#### cuda + cudnn
补充安装 libcuda 和 nvidia-smi 。

```
sudo aptitude install -t libcuda nvidia-smi
```
都采用 nvidia 的 debian/ubuntu 仓库安装 cuda-toolkit 和 libcudnn 。

```
sudo aptitude install --without-recommends cuda-toolkit-11-3
sudo aptitude install --without-recommends libcudnn8 libcudnn8-dev
sudo aptitude install --without-recommends libnccl-dev=2.8.3-1+cuda11.0 libnccl2=2.8.3-1+cuda11.0
```

## 更新 grub
deepin 20.2.1 - 20.2.2 (2021.06.02-2021.06.30) 的 grub-install 存在一个bug：没有正确地安装 grub.cfg 和 grub.efi 文件。

deepin 的 grub-install 向 esp 分区的 /EFI/deepin/ 目录安装了以下文件：

```
BOOTX64.CSV  fbx64.efi  grub.cfg  grubx64.efi  mmx64.efi  shimx64.efi
```
系统启动时，BIOS 加载 /EFI/deepin/shimx64.efi ，/EFI/deepin/shimx64.efi 加载 /EFI/deepin/grub.efi ， /EFI/deepin/grub.efi 执行 /EFI/UOS/grub.cfg 。
可见，缺失了 /EFI/deepin/grub.efi 和 /EFI/UOS/grub.cfg 两个文件，这造成了系统无法启动。

修正的方法是，将 /EFI/deepin/grubx64.efi 复制为 /EFI/deepin/grub.efi，将 /EFI/deepin/grub.cfg 复制为 /EFI/UOS/grub.cfg。

因此，修正 grub-install 的方法是：

```
sudo mount /boot/efi

sudo grub-install --debug

grub-mkimage --directory '/usr/lib/grub/x86_64-efi' --prefix '' --output '/boot/grub/x86_64-efi/grub.efi'  --dtb '' --sbat '' --format 'x86_64-efi' --compression 'auto'  --config '/boot/grub/x86_64-efi/load.cfg' 'btrfs' 'part_gpt' 'search_fs_uuid'

grub-install：信息： copying `/usr/lib/shim/shimx64.efi.signed' -> `/boot/efi/EFI/deepin/shimx64.efi'.
grub-install：信息： copying `/usr/lib/grub/x86_64-efi-signed/grubx64.efi.signed' -> `/boot/efi/EFI/deepin/grubx64.efi'.
grub-install：信息： copying `/usr/lib/shim/mmx64.efi.signed' -> `/boot/efi/EFI/deepin/mmx64.efi'.
grub-install：信息： copying `/usr/lib/shim/fbx64.efi.signed' -> `/boot/efi/EFI/deepin/fbx64.efi'.
grub-install：信息： copying `/usr/lib/shim/BOOTX64.CSV' -> `/boot/efi/EFI/deepin/BOOTX64.CSV'.
grub-install：信息： copying `/boot/grub/x86_64-efi/load.cfg' -> `/boot/efi/EFI/deepin/grub.cfg'.
grub-install：信息： Registering with EFI: distributor = `deepin', path = `\EFI\deepin\shimx64.efi', ESP at hostdisk//dev/sda,gpt1.

sudo update-grub

正在生成 grub 配置文件 ...
找到主题：/boot/grub/themes/deepin-fallback/theme.txt
Found background image: /boot/grub/themes/deepin-fallback/background.jpg
找到 Linux 镜像：/boot/vmlinuz-5.12.9-amd64-desktop
找到 initrd 镜像：/boot/initrd.img-5.12.9-amd64-desktop
找到 Linux 镜像：/boot/vmlinuz-5.11.13-amd64-desktop
找到 initrd 镜像：/boot/initrd.img-5.11.13-amd64-desktop
找到 Windows Boot Manager 位于 /dev/sda1@/EFI/Microsoft/Boot/bootmgfw.efi
Adding boot menu entry for EFI firmware configuration
完成
```

修正操作：

```
sudo cp /boot/efi/EFI/deepin/grubx64.efi /boot/efi/EFI/deepin/grub.efi
sudo mkdir -p /boot/efi/EFI/UOS
sudo cp /boot/efi/EFI/deepin/grub.cfg /boot/efi/EFI/UOS/grub.cfg
```

注意，虽然grub-install 输出 `copying '/boot/grub/x86_64-efi/load.cfg' -> '/boot/efi/EFI/deepin/grub.cfg'` ，但这两个文件并不一样，所以要将 /boot/efi/EFI/deepin/grub.cfg 复制到 /boot/efi/EFI/UOS/grub.cfg 。

另一个方法是，使用 /boot/grub/x86_64-efi/grub.efi 。此文件内嵌 grub.cfg ，取自 /boot/grub/x86_64-efi/load.cfg ，内容是：

```
search.fs_uuid c0480a4d-a637-4da8-bdaf-dc15690a8fb3 root hd0,gpt8 
set prefix=($root)'/@root-deepin-1/boot/grub'
```

这样实际上加载了配置文件 `(hd0,gpt8)/@root-deepin-1/boot/grub/grub.cfg` 。
