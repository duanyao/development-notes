## 步骤

### 初始安装

采用硬盘安装。

光盘镜像放在：
/media/duanyao/Windows/deepin-desktop-community-23-Beta3-amd64.iso

其grub配置文件（ `/boot/grub.cfg` ）可以作为参考：


```
if loadfont /boot/grub/unicode.pf2 ; then
	set gfxmode=auto
	insmod efi_gop
	insmod efi_uga
	insmod gfxterm
	terminal_output gfxterm
fi

set menu_color_normal=white/black
set menu_color_highlight=black/light-gray
set timeout=5
background_image /boot/grub/background.png
menuentry "Install Deepin 23 with kernel 6.1 desktop" {
	set gfxpayload=keep
	linux	/live/vmlinuz-6.1 boot=live union=overlay livecd-installer locales=zh_CN.UTF-8 console=tty splash --
	initrd	/live/initrd-6.1.lz
}
menuentry "Install Deepin 23 with kernel 6.1 desktop (Safe graphics,Please use this mode to install if your graphics card is not working properly.)" {
	set gfxpayload=keep
	linux	/live/vmlinuz-6.1 boot=live union=overlay livecd-installer locales=zh_CN.UTF-8 console=tty splash nomodeset --
	initrd	/live/initrd-6.1.lz
}
```

查看 EFI 启动项：
```
sudo efibootmgr -v
BootCurrent: 0007
Timeout: 0 seconds
BootOrder: 0007,0000,0004,0003,0006,0001,0002,0005
Boot0000* ubuntu        HD(1,GPT,2e4346d5-d26a-4d49-863b-0ab9c9c2a562,0x800,0x82000)/File(\EFI\ubuntu\shimx64.efi)
Boot0001* IPV4 Network - Realtek PCIe GBE Family Controller     PciRoot(0x0)/Pci(0x1,0x3)/Pci(0x0,0x0)/MAC(bc0ff367d246,0)/IPv4(0.0.0.00.0.0.0,0,0)N.....YM....R,Y.....ISPH
Boot0002* IPV6 Network - Realtek PCIe GBE Family Controller     PciRoot(0x0)/Pci(0x1,0x3)/Pci(0x0,0x0)/MAC(bc0ff367d246,0)/IPv6([::]:<->[::]:,0,0)N.....YM....R,Y.....ISPH
Boot0003  USB:          PciRoot(0x0)/Pci(0x8,0x1)/Pci(0x0,0x3)N.....YM....R,Y.....ISPH
Boot0004* Windows Boot Manager  HD(1,GPT,2e4346d5-d26a-4d49-863b-0ab9c9c2a562,0x800,0x82000)/File(\EFI\Microsoft\Boot\bootmgfw.efi)WINDOWS.........x...B.C.D.O.B.J.E.C.T.=.{.9.d.e.a.8.6.2.c.-.5.c.d.d.-.4.e.7.0.-.a.c.c.1.-.f.3.2.b.3.4.4.d.4.7.9.5.}...e....................ISPH
Boot0005* Linux Firmware Updater        HD(1,GPT,2e4346d5-d26a-4d49-863b-0ab9c9c2a562,0x800,0x14a000)/File(\EFI\ubuntu\fwupdx64.efi)
Boot0006* custom-generic-grub   HD(1,GPT,2e4346d5-d26a-4d49-863b-0ab9c9c2a562,0x800,0x82000)/File(\EFI\custom-generic-grub\custom-generic-grub.efi)....ISPH
```
其中，custom-generic-grub 是我们的自定义 grub EFI 镜象（制作方法见 `ubunut2304-grub_custom-btrfs.md`），
目前挂载于 /boot/efi/EFI/custom-generic-grub/ ，目录内容如下：
```
/boot/efi/EFI/custom-generic-grub/
	custom-generic-grub.efi
	grub.cfg
	deepin23-live.grub.cfg
```

custom-generic-grub.efi 的配置文件为 `<esp>/EFI/custom-generic-grub/grub.cfg`。在其中加入一项 menuentry，跳到为 $prefix/deepin23-live.grub.cfg ：



/boot/efi/EFI/custom-generic-grub/grub.cfg
```
set timeout=3
set default=0
insmod part_gpt
insmod ext2
insmod btrfs
# https://www.linuxbabe.com/desktop-linux/boot-from-iso-files-using-grub2-boot-loader
insmod ntfs
insmod iso9660
insmod loopback
insmod efi_gop
insmod efi_uga
insmod font
insmod video_bochs
insmod video_cirrus

menuentry "jump ./ubuntu23-live.grub.cfg" {
  configfile $prefix/ubuntu23-live.grub.cfg
}

menuentry "jump ./deepin23-live.grub.cfg" {
  configfile $prefix/deepin23-live.grub.cfg
}

```

deepin23-live.grub.cfg 的内容是：
```
set isofile="/deepin-desktop-community-23-Beta3-amd64.iso"
search --file --set=root $isofile
loopback loop1 $isofile
set timeout=5
menuentry "Install Deepin 23 with kernel 6.1 desktop" {
    linux       (loop1)/live/vmlinuz-6.1 findiso=$isofile boot=live union=overlay livecd-installer locales=zh_CN.UTF-8 console=tty splash --
    initrd      (loop1)/live/initrd-6.1.lz
}

menuentry "Install Deepin 23 with kernel 6.1 desktop  (Safe graphics,Please use this mode to install if your graphics card is not working properly.)" {
    linux       (loop1)/live/vmlinuz-6.1 findiso=$isofile boot=live union=overlay livecd-installer locales=zh_CN.UTF-8 console=tty splash nomodeset --
    initrd      (loop1)/live/initrd-6.1.lz
}
```

重启系统，在 BIOS 阶段按 F9 调出 EFI 菜单，选择 custom-generic-grub，再选择 "jump ./deepin23-live.grub.cfg"，再选择 "Install Deepin 23 with kernel 6.1 desktop"，开始引导 Deepin 23 live cd 。安装到目标分区（/dev/nvme0n1p6）。安装过程中，目标分区格式化为 ext4，单一分区模式。
deepin 23b3 安装后大小约为 14GB 。

### 复制文件
确认当前挂载：
```
mount | grep /dev/nvme
/dev/nvme0n1p7 on / type btrfs (rw,relatime,ssd,discard=async,space_cache=v2,subvolid=5,subvol=/)
/dev/nvme0n1p7 on /var/snap/firefox/common/host-hunspell type btrfs (ro,noexec,noatime,ssd,discard=async,space_cache=v2,subvolid=5,subvol=/)
/dev/nvme0n1p3 on /media/windows type fuseblk (rw,relatime,user_id=0,group_id=0,allow_other,blksize=4096)
/dev/nvme0n1p1 on /boot/efi type vfat (rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,errors=remount-ro)
/dev/nvme0n1p6 on /media/duanyao/Roota type ext4 (rw,nosuid,nodev,relatime,errors=remount-ro,uhelper=udisks2)
```

```
cd /

sudo btrfs subvolume create subvol/root-deepin-1

sudo mkdir -p /media/root-deepin-1/
sudo mount /dev/nvme0n1p7 -o subvol=subvol/root-deepin-1,relatime,compress=zlib /media/root-deepin-1/
```

执行复制：
```
sudo rsync -aAHXxvh /media/duanyao/Roota/ /media/root-deepin-1/
```
参数的含义见 rsync.md 。

## fstab

```
cp /subvol/root-ubuntu-1/etc/fstab ~/custom-generic-grub/fstab.root-deepin-1
```

修改 ~/custom-generic-grub/fstab.root-deepin-1 内容如下：

```
LABEL=sys1 /                         btrfs rw,relatime,compress=zlib,ssd,commit=120,subvol=subvol/root-deepin-1         0 1
LABEL=sys1 /home                     btrfs rw,relatime,compress=zlib,ssd,commit=120,subvol=subvol/home                  0 1
LABEL=sys1 /var/lib/docker           btrfs rw,relatime,compress=zlib,ssd,commit=120,subvol=subvol/docker                0 1
LABEL=sys1 /var/lib/docker/volumes   btrfs rw,relatime,compress=zlib,ssd,commit=120,subvol=subvol/docker-vol            0 1

LABEL=sys1 /media/top                btrfs rw,relatime,compress=zlib,ssd,commit=120,subvol=/                            0 1
```

部署 fstab:
```
sudo cp ~/custom-generic-grub/fstab.root-deepin-1 /subvol/root-deepin-1/etc/fstab
```

## 添加 grub 配置


```
cp /boot/efi/EFI/custom-generic-grub/grub.cfg ~/custom-generic-grub/grub.cfg
cp /boot/efi/EFI/custom-generic-grub/root-ubuntu-1.grub.cfg ~/custom-generic-grub/root-deepin-1.grub.cfg

sudo cat /subvol/root-deepin-1/boot/grub/grub.cfg

xdg-open ~/custom-generic-grub/root-deepin-1.grub.cfg
```

根据从 /subvol/root-deepin-1/boot/grub/grub.cfg 中找到 vmlinuz 和 initrd 的文件名，修改 ~/custom-generic-grub/root-deepin-1.grub.cfg 如下：
```
search --no-floppy --fs-uuid --set=root e7889cac-3f57-4e3c-89c4-3b68ee1706de
linux	/subvol/root-deepin-1/boot/vmlinuz-6.1.32-amd64-desktop-hwe root=UUID=e7889cac-3f57-4e3c-89c4-3b68ee1706de rootflags=subvol=subvol/root-deepin-1 ro  quiet splash
initrd	/subvol/root-deepin-1/boot/initrd.img-6.1.32-amd64-desktop-hwe
```
再修改 ~/custom-generic-grub/grub.cfg ，增加 menuentry 跳转到 root-deepin-1.grub.cfg：

```
menuentry "jump ./root-deepin-1.grub.cfg" {
  configfile $prefix/root-deepin-1.grub.cfg
}
```
下次启动就进入这一项。

更新 grub 配置
```
sudo cp ~/custom-generic-grub/grub.cfg /boot/efi/EFI/custom-generic-grub/grub.cfg 
sudo cp ~/custom-generic-grub/root-deepin-1.grub.cfg /boot/efi/EFI/custom-generic-grub/root-deepin-1.grub.cfg
```

## 修复 grub
修复 grub 包括 grub-install 和  update-grub 两步。

```
sudo LC_ALL=C grub-install --debug

grub-install: info: copying `/usr/lib/shim/shimx64.efi.signed' -> `/boot/efi/EFI/deepin/shimx64.efi'.
grub-install: info: copying `/usr/lib/grub/x86_64-efi-signed/grubx64.efi.signed' -> `/boot/efi/EFI/deepin/grubx64.efi'.
grub-install: info: copying `/usr/lib/shim/mmx64.efi.signed' -> `/boot/efi/EFI/deepin/mmx64.efi'.
grub-install: info: copying `/usr/lib/shim/fbx64.efi.signed' -> `/boot/efi/EFI/deepin/fbx64.efi'.
grub-install: info: copying `/usr/lib/shim/BOOTX64.CSV' -> `/boot/efi/EFI/deepin/BOOTX64.CSV'.
grub-install: info: copying `/boot/grub/x86_64-efi/load.cfg' -> `/boot/efi/EFI/deepin/grub.cfg'.
grub-install: info: Registering with EFI: distributor = `deepin', path = `\EFI\deepin\shimx64.efi', ESP at hostdisk//dev/nvme0n1,gpt1.
grub-install: info: executing modprobe efivars 2>/dev/null.
grub-install: info: setting EFI variable Boot0007.
grub-install: info: skipping unnecessary update of EFI variable Boot0007.
grub-install: info: setting EFI variable BootOrder.
Installation finished. No error reported.
```

生成了 /boot/efi/EFI/deepin/grub.cfg :
```
search.fs_uuid e7889cac-3f57-4e3c-89c4-3b68ee1706de root 
set prefix=($root)'/subvol/root-deepin-1/boot/grub'
configfile $prefix/grub.cfg
```

再执行 update-grub：
```
sudo update-grub

ound theme: /boot/grub/themes/deepin/theme.txt
Found background image: /boot/grub/themes/deepin/background.jpg
Found linux image: /boot/vmlinuz-6.1.32-amd64-desktop-hwe
Found initrd image: /boot/initrd.img-6.1.32-amd64-desktop-hwe
Warning: os-prober will be executed to detect other bootable partitions.
Its output will be used to detect bootable binaries on them and create new boot entries.
Found Windows Boot Manager on /dev/nvme0n1p1@/EFI/Microsoft/Boot/bootmgfw.efi
Found Deepin 23 (23) on /dev/nvme0n1p6
Adding boot menu entry for UEFI Firmware Settings ...
done
```
这修改了 /boot/grub/grub.cfg：
```
        linux   /subvol/root-deepin-1/boot/vmlinuz-6.1.32-amd64-desktop-hwe root=UUID=e7889cac-3f57-4e3c-89c4-3b68ee1706de ro rootflags=subvol=subvol/root-deepin-1  splash quiet  DEEPIN_GFXMODE=$DEEPIN_GFXMODE
        initrd  /subvol/root-deepin-1/boot/initrd.img-6.1.32-amd64-desktop-hwe
```

当前 EFI 启动项：
```
efibootmgr -v
BootCurrent: 0006
Timeout: 0 seconds
BootOrder: 0007,0000,0004,0003,0006,0001,0002,0005
Boot0000* ubuntu        HD(1,GPT,2e4346d5-d26a-4d49-863b-0ab9c9c2a562,0x800,0x14a000)/File(\EFI\ubuntu\shimx64.efi)
Boot0001* IPV4 Network - Realtek PCIe GBE Family Controller     PciRoot(0x0)/Pci(0x1,0x3)/Pci(0x0,0x0)/MAC(bc0ff367d246,0)/IPv4(0.0.0.00.0.0.0,0,0)N.....YM....R,Y.....ISPH
Boot0002* IPV6 Network - Realtek PCIe GBE Family Controller     PciRoot(0x0)/Pci(0x1,0x3)/Pci(0x0,0x0)/MAC(bc0ff367d246,0)/IPv6([::]:<->[::]:,0,0)N.....YM....R,Y.....ISPH
Boot0003  USB:          PciRoot(0x0)/Pci(0x8,0x1)/Pci(0x0,0x3)N.....YM....R,Y.....ISPH
Boot0004* Windows Boot Manager  HD(1,GPT,2e4346d5-d26a-4d49-863b-0ab9c9c2a562,0x800,0x82000)/File(\EFI\Microsoft\Boot\bootmgfw.efi)WINDOWS.........x...B.C.D.O.B.J.E.C.T.=.{.9.d.e.a.8.6.2.c.-.5.c.d.d.-.4.e.7.0.-.a.c.c.1.-.f.3.2.b.3.4.4.d.4.7.9.5.}...e....................ISPH
Boot0005* Linux Firmware Updater        HD(1,GPT,2e4346d5-d26a-4d49-863b-0ab9c9c2a562,0x800,0x14a000)/File(\EFI\ubuntu\fwupdx64.efi)
Boot0006* custom-generic-grub   HD(1,GPT,2e4346d5-d26a-4d49-863b-0ab9c9c2a562,0x800,0x82000)/File(\EFI\custom-generic-grub\custom-generic-grub.efi)....ISPH
Boot0007* deepin        HD(1,GPT,2e4346d5-d26a-4d49-863b-0ab9c9c2a562,0x800,0x14a000)/File(\EFI\deepin\shimx64.efi)
```

## 迁移配置
### /etc/hosts
sudo cp /home/duanyao/doc.personal/development/linux/custom_config/hosts /etc/


## 快照/子卷备份

```
sudo btrfs subvolume snapshot -r /media/top/subvol/root-deepin-1 /media/top/snapshot/root-deepin-1=2024-2-18=1
sudo btrfs subvolume snapshot -r /media/top/subvol/home /media/top/snapshot/home=2024-2-18=1
```

## 安装软件
### 附加镜像源
```
cd ~/doc.personal/development/linux/apt_source/

sudo cp debian-11.list debian-12.list  /etc/apt/sources.list.d/
sudo cp deepin23-debian.pref  /etc/apt/preferences.d/

sudo apt install debian-archive-keyring
sudo apt update
```

debian-11.list
```
# /etc/apt/sources.list.d/
# 默认注释了源码镜像以提高 apt update 速度，如有需要可自行取消注释
deb https://mirrors.tuna.tsinghua.edu.cn/debian/ bullseye main contrib non-free
deb-src https://mirrors.tuna.tsinghua.edu.cn/debian/ bullseye main contrib non-free

deb https://mirrors.tuna.tsinghua.edu.cn/debian/ bullseye-updates main contrib non-free
deb-src https://mirrors.tuna.tsinghua.edu.cn/debian/ bullseye-updates main contrib non-free

deb https://mirrors.tuna.tsinghua.edu.cn/debian/ bullseye-backports main contrib non-free
deb-src https://mirrors.tuna.tsinghua.edu.cn/debian/ bullseye-backports main contrib non-free

#deb https://security.debian.org/debian-security bullseye-security main contrib non-free
#deb-src https://security.debian.org/debian-security bullseye-security main contrib non-free
```

debian-12.list
```
# /etc/apt/sources.list.d/
# 默认注释了源码镜像以提高 apt update 速度，如有需要可自行取消注释
deb https://mirrors.tuna.tsinghua.edu.cn/debian/ bookworm main contrib non-free non-free-firmware
deb-src https://mirrors.tuna.tsinghua.edu.cn/debian/ bookworm main contrib non-free non-free-firmware

deb https://mirrors.tuna.tsinghua.edu.cn/debian/ bookworm-updates main contrib non-free non-free-firmware
deb-src https://mirrors.tuna.tsinghua.edu.cn/debian/ bookworm-updates main contrib non-free non-free-firmware

deb https://mirrors.tuna.tsinghua.edu.cn/debian/ bookworm-backports main contrib non-free non-free-firmware
deb-src https://mirrors.tuna.tsinghua.edu.cn/debian/ bookworm-backports main contrib non-free non-free-firmware

#deb https://security.debian.org/debian-security bookworm-security main contrib non-free non-free-firmware
#deb-src https://security.debian.org/debian-security bookworm-security main contrib non-free non-free-firmware
```

deepin23-debian.pref
```
# /etc/apt/preferences.d/
# deepin 23
Package: *
Pin: release beige
Pin-Priority: 500

# debian 11
Package: *
Pin: release bullseye
Pin-Priority: 100

# debian 12
Package: *
Pin: release bookworm
Pin-Priority: 150
```

### 安装附加软件
```
sudo apt install manpages-dev  git git-gui build-essential automake libtool cmake gdb golang-go meld traceroute strace time dh-make

sudo apt install kate ffmpeg vlc wireshark curl tmux avahi-daemon lsof cgroup-tools timidity  powertop apt-file

deepin 仓库缺少 moreutils autossh iptux tcpdump atop psensor dconf-editor earlyoom cpu-x cpufrequtils mcomix cpupower-gui wxhexeditor btrfs-compsize nautilus ，但可以从 debian 仓库安装。

deepin 和 debian  仓库缺少 cpu-g CopyQ 。totem 和 nautilus 的依赖有冲突（ libxkbcommon0 ）。

# nautilus 来自 debian 12, libxkbregistry0 来自 deepin 23b3
sudo apt install -t bookworm nautilus libxkbregistry0=1.6.0-1
sudo apt install -t bookworm --no-install-recommends nautilus-admin nautilus-extension-gnome-terminal
sudo apt install -t bookworm gnome-system-monitor gnome-power-manager

# 较新的内核。默认内核为 6.1 （2024.2.19）
sudo apt install linux-image-6.6.7-amd64-desktop-hwe linux-headers-6.6.7-amd64-desktop-hwe

# 安装其它版本的 python 。系统默认为 3.11，附带 3.12，bullseye(debian 11)附带 3.9。
sudo apt install python3.9 python3.9-dev
sudo apt install python3.12 python3.12-dev

# systemd-swap-gui
sudo apt install /home/duanyao/software/systemd-swap-gui0.1.1.deb

sudo apt install com.alibabainc.dingtalk # 7.1.0.31017, 2024.2
```

解决 gnome-power-manager 不出现在菜单的问题：
```
cp /usr/share/applications/org.gnome.PowerStats.desktop ~/.local/share/applications/org.gnome.PowerStats.desktop
xdg-open ~/.local/share/applications/org.gnome.PowerStats.desktop
```
删除或注释 `OnlyShowIn=GNOME;Unity;` 。

解决 dingtalk 无法运行的问题：./com.alibabainc.dingtalk: symbol lookup error: /lib/x86_64-linux-gnu/libcairo.so.2: undefined symbol: FT_Get_Color_Glyph_Layer

```
cp /opt/apps/com.alibabainc.dingtalk/entries/applications/com.alibabainc.dingtalk.desktop  ~/.local/share/applications/com.alibabainc.dingtalk.desktop
xdg-open ~/.local/share/applications/com.alibabainc.dingtalk.desktop
# 将 `Exec=/opt/apps/com.alibabainc.dingtalk/files/Elevator.sh %u` 改为 `Exec=env LD_PRELOAD=/lib/x86_64-linux-gnu/libfreetype.so.6 /opt/apps/com.alibabainc.dingtalk/files/Elevator.sh %u`。
sudo cp ~/.local/share/applications/com.alibabainc.dingtalk.desktop /opt/apps/com.alibabainc.dingtalk/entries/applications/com.alibabainc.dingtalk.desktop
```

### 安装 docker-ce
```
curl -fsSL https://mirrors.ustc.edu.cn/docker-ce/linux/debian/gpg | sudo apt-key add -

cd ~/doc.personal/development/linux/apt_source/
sudo cp docker-ce-debian12-ustc.list /etc/apt/sources.list.d/
sudo apt-get install docker-ce

sudo systemctl enable --now docker
sudo usermod -aG docker $USER
```
docker-ce-debian12-ustc.list: 
```
# /etc/apt/sources.list.d/
deb [arch=amd64] https://mirrors.ustc.edu.cn/docker-ce/linux/debian bookworm stable
```
