
##  问题描述
点击 dde-dock 左侧的 dde-laucher 按钮，菜单没有弹出。dde-laucher 进程不存在。

## 初步分析

查看日志：

```
journalctl --user -f
=============================================================================================
10月 04 16:59:21 duanyao-laptop-a dbus-daemon[17141]: [session uid=1000 pid=17141] Activating service name='com.deepin.dde.Launcher' requested by ':1.31' (uid=1000 pid=17310 comm="/usr/bin/dde-dock -r ")
10月 04 16:59:21 duanyao-laptop-a startdde[17127]: startmanager.go:711: current appId is dde-launcher
10月 04 16:59:21 duanyao-laptop-a com.deepin.dde.Launcher[17141]: method return time=1633337961.042931 sender=:1.5 -> destination=:1.119 serial=182 reply_serial=2
10月 04 16:59:21 duanyao-laptop-a com.deepin.dde.Launcher[17141]:    boolean true
10月 04 16:59:21 duanyao-laptop-a dbus-daemon[17141]: [session uid=1000 pid=17141] Successfully activated service 'com.deepin.dde.Launcher'
10月 04 16:59:21 duanyao-laptop-a x-event-monitor[17235]: manager.go:130: <nil>
10月 04 16:59:21 duanyao-laptop-a startdde[17127]: startmanager.go:732: [/bin/sh -c export GIO_LAUNCHED_DESKTOP_FILE_PID=$$;exec /usr/bin/cgexec -g memory,freezer,blkio:11@dde/uiapps/26 /usr/bin/dde-launcher]: signal: segmentation fault
10月 04 16:59:21 duanyao-laptop-a daemon/dock[17235]: dock_manager_entries.go:177: entry of window 85983242 is nil
10月 04 16:59:21 duanyao-laptop-a daemon/dock[17235]: dock_manager_entries.go:177: entry of window 20973213 is nil
10月 04 16:59:21 duanyao-laptop-a daemon/dock[17235]: dock_manager_xevent.go:120: window info of 85983242 is nil
10月 04 16:59:22 duanyao-laptop-a daemon/dock[17235]: dock_manager_xevent.go:209: mapNotifyEvent after 2s, call identifyWindow, win: 37748760
10月 04 16:59:22 duanyao-laptop-a daemon/dock[17235]: dock_manager_xevent.go:209: mapNotifyEvent after 2s, call identifyWindow, win: 37748760
10月 04 16:59:23 duanyao-laptop-a daemon/dock[17235]: dock_manager_xevent.go:209: mapNotifyEvent after 2s, call identifyWindow, win: 20973213

================================================================================================
```
在命令行启动 dde-launcher，也是立即崩溃：

```
$ dde-launcher -s
2021-10-04, 17:06:20.846 [Warning] [  ] qUncompress: Z_DATA_ERROR: Input data is corrupted
2021-10-04, 17:06:20.846 [Warning] [dsvgrenderer.cpp     Dtk::Gui::DSvgRenderer::load        291] DSvgRenderer::load: rsvg_handle_fill_with_data: assertion `data_len != 0' failed
段错误
```

查看 strace 
```
strace dde-launcher -s

================================================================================================
openat(AT_FDCWD, "/home/duanyao/.cache/deepin/icons/5126d8cd44dba65ac78f17a9d0297f92a399b898.png", O_RDONLY|O_CLOEXEC) = 38
openat(AT_FDCWD, "/usr/share/icons/bloom/apps/64/deepin-system-monitor.svg", O_RDONLY|O_CLOEXEC) = 37
read(37, "<svg xmlns=\"http://www.w3.org/20"..., 16384) = 8376
read(37, "", 8008)                      = 0
read(37, "", 16384)                     = 0
read(37, "", 16384)                     = 0
statx(37, "", AT_STATX_SYNC_AS_STAT|AT_EMPTY_PATH, STATX_ALL, {stx_mask=STATX_ALL|0x1000, stx_attributes=0, stx_mode=S_IFREG|0644, stx_size=8376, ...}) = 0
malloc(): unsorted double linked list corrupted
+++ killed by SIGABRT +++
已放弃
```
## 下载源码编译：
```
cd ~/project/deb-src/
apt source dde-launcher
sudo apt-get install packaging-dev debian-keyring devscripts equivs
sudo mk-build-deps --install --remove
cd dde-launcher-5.4.40
dpkg-buildpackage -us -uc
debian/dde-launcher/usr/bin/dde-launcher -s
./obj-x86_64-linux-gnu/dde-launcher -s
```

cd ./obj-x86_64-linux-gnu/
make

## 

dconf-editor:
```
com.deepin.dde.launcher fullscreen true
com.deepin.dde.launcher display-mode 'free'
```
category 模式就不会崩溃。

```
$ gdb --args dde-launcher -s
GNU gdb (Uos 8.2.1.1-1+security) 8.2.1
Copyright (C) 2018 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
Type "show copying" and "show warranty" for details.
This GDB was configured as "x86_64-linux-gnu".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
    <http://www.gnu.org/software/gdb/documentation/>.

For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from dde-launcher...done.
(gdb) r
Starting program: /home/duanyao/bin/dde-launcher -s
[Thread debugging using libthread_db enabled]
Using host libthread_db library "/lib/x86_64-linux-gnu/libthread_db.so.1".
[New Thread 0x7ffff2cdf700 (LWP 19692)]
[New Thread 0x7ffff24de700 (LWP 19693)]
[New Thread 0x7ffff1ca6700 (LWP 19694)]
[New Thread 0x7ffff0a6d700 (LWP 19695)]
[New Thread 0x7fffe3fff700 (LWP 19696)]
[New Thread 0x7fffe37fe700 (LWP 19697)]
[New Thread 0x7fffe2ffd700 (LWP 19698)]
[New Thread 0x7fffe080d700 (LWP 19699)]
[New Thread 0x7fffcdaa2700 (LWP 19700)]
[New Thread 0x7fffcd2a1700 (LWP 19701)]
>>>>LauncherSys::showLauncher(): 
[New Thread 0x7fffc2750700 (LWP 19702)]
[New Thread 0x7fffc1f4f700 (LWP 19703)]
[New Thread 0x7fffc174e700 (LWP 19704)]
[New Thread 0x7fffc0f4d700 (LWP 19705)]
>>>>LauncherSys::onVisibleChanged:now=1
2021-10-04, 23:24:19.567 [Warning] [  ] qUncompress: Z_DATA_ERROR: Input data is corrupted
2021-10-04, 23:24:19.567 [Warning] [dsvgrenderer.cpp     Dtk::Gui::DSvgRenderer::load        291] DSvgRenderer::load: rsvg_handle_fill_with_data: assertion `data_len != 0' failed
free(): invalid pointer

Thread 9 "IconFreshThread" received signal SIGABRT, Aborted.
[Switching to Thread 0x7fffe080d700 (LWP 19699)]
__GI_raise (sig=sig@entry=6) at ../sysdeps/unix/sysv/linux/raise.c:50
50      ../sysdeps/unix/sysv/linux/raise.c: 没有那个文件或目录.
(gdb) bt
#0  0x00007ffff5d418eb in __GI_raise (sig=sig@entry=6) at ../sysdeps/unix/sysv/linux/raise.c:50
#1  0x00007ffff5d2c535 in __GI_abort () at abort.c:79
#2  0x00007ffff5d83638 in __libc_message (action=action@entry=do_abort, fmt=fmt@entry=0x7ffff5e8e2a0 "%s\n") at ../sysdeps/posix/libc_fatal.c:181
#3  0x00007ffff5d89d4a in malloc_printerr (str=str@entry=0x7ffff5e8c44e "free(): invalid pointer") at malloc.c:5341
#4  0x00007ffff5d8b55c in _int_free (av=<optimized out>, p=<optimized out>, have_lock=<optimized out>) at malloc.c:4165
#5  0x00007fffe2609f9f in QSvgIconEngine::read(QDataStream&) () at /usr/lib/x86_64-linux-gnu/qt5/plugins/iconengines/based-dtk/libdsvgicon.so
#6  0x00007ffff693f920 in operator>>(QDataStream&, QIcon&) () at /lib/x86_64-linux-gnu/libQt5Gui.so.5
#7  0x00007ffff0dfc8b7 in  () at /lib/x86_64-linux-gnu/libQt5XdgIconLoader.so.3
#8  0x00007fffcfff9646 in XdgIconProxyEngine::followColorPixmap(ScalableEntry*, QSize const&, QIcon::Mode, QIcon::State) ()
    at /usr/lib/x86_64-linux-gnu/qt5/plugins/iconengines/based-dtk/libxdgicon.so
#9  0x00007fffcfffaacf in XdgIconProxyEngine::pixmapByEntry(QIconLoaderEngineEntry*, QSize const&, QIcon::Mode, QIcon::State) ()
    at /usr/lib/x86_64-linux-gnu/qt5/plugins/iconengines/based-dtk/libxdgicon.so
#10 0x00007fffcfffacb1 in XdgIconProxyEngine::pixmap(QSize const&, QIcon::Mode, QIcon::State) ()
    at /usr/lib/x86_64-linux-gnu/qt5/plugins/iconengines/based-dtk/libxdgicon.so
#11 0x00007ffff693dcfc in QIcon::pixmap(QWindow*, QSize const&, QIcon::Mode, QIcon::State) const () at /lib/x86_64-linux-gnu/libQt5Gui.so.5
#12 0x00007ffff693de9e in QIcon::pixmap(QSize const&, QIcon::Mode, QIcon::State) const () at /lib/x86_64-linux-gnu/libQt5Gui.so.5
#13 0x0000000000467643 in getThemeIcon(QPixmap&, ItemInfo const&, int, bool) (pixmap=..., itemInfo=..., size=size@entry=36, reObtain=<optimized out>)
    at /usr/include/x86_64-linux-gnu/qt5/QtCore/qsize.h:125
#14 0x00000000004876e0 in IconFreshThread::createPixmap(ItemInfo const&, int) (this=0x6ad650, itemInfo=..., size=36)
    at /usr/include/c++/8/bits/atomic_base.h:390
#15 0x000000000048796b in IconFreshThread::run() (this=0x6ad650)
    at /home/duanyao/project/deb-src/dde-launcher-5.4.40/src/model/iconfreshthread.cpp:181
#16 0x00007ffff62b99a8 in  () at /lib/x86_64-linux-gnu/libQt5Core.so.5
#17 0x00007ffff599ffa3 in start_thread (arg=<optimized out>) at pthread_create.c:486
#18 0x00007ffff5e0360f in clone () at ../sysdeps/unix/sysv/linux/x86_64/clone.S:95
(gdb) info threads
  Id   Target Id                                           Frame 
  1    Thread 0x7ffff2db9e40 (LWP 19688) "dde-launcher"    0x00007ffff55f8b40 in ?? () from /lib/x86_64-linux-gnu/libz.so.1
  2    Thread 0x7ffff2cdf700 (LWP 19692) "gmain"           0x00007ffff5df8959 in __GI___poll (fds=0x5f2ae0, nfds=1, timeout=-1)
    at ../sysdeps/unix/sysv/linux/poll.c:29
  3    Thread 0x7ffff24de700 (LWP 19693) "gdbus"           0x00007ffff5df8959 in __GI___poll (fds=0x605020, nfds=2, timeout=-1)
    at ../sysdeps/unix/sysv/linux/poll.c:29
  4    Thread 0x7ffff1ca6700 (LWP 19694) "dconf worker"    0x00007ffff5df8959 in __GI___poll (fds=0x60cc40, nfds=1, timeout=-1)
    at ../sysdeps/unix/sysv/linux/poll.c:29
  5    Thread 0x7ffff0a6d700 (LWP 19695) "QXcbEventQueue"  0x00007ffff5df8959 in __GI___poll (fds=0x7ffff0a6cb28, nfds=1, timeout=-1)
    at ../sysdeps/unix/sysv/linux/poll.c:29
  6    Thread 0x7fffe3fff700 (LWP 19696) "QDBusConnection" 0x00007ffff5df8959 in __GI___poll (fds=0x7fffd802ba80, nfds=6, timeout=-1)
    at ../sysdeps/unix/sysv/linux/poll.c:29
  7    Thread 0x7fffe37fe700 (LWP 19697) "Thread (pooled)" futex_reltimed_wait_cancelable (private=0, reltime=0x7fffe37fdad0, expected=0, 
    futex_word=0x6dca04) at ../sysdeps/unix/sysv/linux/futex-internal.h:142
  8    Thread 0x7fffe2ffd700 (LWP 19698) "Thread (pooled)" futex_reltimed_wait_cancelable (private=0, reltime=0x7fffe2ffcad0, expected=0, 
    futex_word=0x6dd2b4) at ../sysdeps/unix/sysv/linux/futex-internal.h:142
* 9    Thread 0x7fffe080d700 (LWP 19699) "IconFreshThread" __GI_raise (sig=sig@entry=6) at ../sysdeps/unix/sysv/linux/raise.c:50
  10   Thread 0x7fffcdaa2700 (LWP 19700) "Thread (pooled)" futex_reltimed_wait_cancelable (private=0, reltime=0x7fffcdaa1ad0, expected=0, 
    futex_word=0x7fffc808ae34) at ../sysdeps/unix/sysv/linux/futex-internal.h:142
  11   Thread 0x7fffcd2a1700 (LWP 19701) "Thread (pooled)" futex_reltimed_wait_cancelable (private=0, reltime=0x7fffcd2a0ad0, expected=0, 
    futex_word=0x7fffc80a1194) at ../sysdeps/unix/sysv/linux/futex-internal.h:142
  12   Thread 0x7fffc2750700 (LWP 19702) "dde-lau:disk$0"  futex_wait_cancelable (private=0, expected=0, futex_word=0x7db758)
    at ../sysdeps/unix/sysv/linux/futex-internal.h:88
  13   Thread 0x7fffc1f4f700 (LWP 19703) "dde-lau:disk$1"  futex_wait_cancelable (private=0, expected=0, futex_word=0x7db758)
    at ../sysdeps/unix/sysv/linux/futex-internal.h:88
  14   Thread 0x7fffc174e700 (LWP 19704) "dde-lau:disk$2"  futex_wait_cancelable (private=0, expected=0, futex_word=0x7db758)
    at ../sysdeps/unix/sysv/linux/futex-internal.h:88
  15   Thread 0x7fffc0f4d700 (LWP 19705) "dde-lau:disk$3"  futex_wait_cancelable (private=0, expected=0, futex_word=0x7db758)
    at ../sysdeps/unix/sysv/linux/futex-internal.h:88
```

```
duanyao@duanyao-laptop-a:~$ /usr/bin/dde-launcher -s
2021-10-04, 23:55:53.032 [Warning] [  ] qUncompress: Z_DATA_ERROR: Input data is corrupted
2021-10-04, 23:55:53.032 [Warning] [dsvgrenderer.cpp     Dtk::Gui::DSvgRenderer::load        291] DSvgRenderer::load: rsvg_handle_fill_with_data: assertion `data_len != 0' failed
malloc(): unsorted double linked list corrupted
已放弃
duanyao@duanyao-laptop-a:~$ /usr/bin/dde-launcher -s
^C
duanyao@duanyao-laptop-a:~$ /usr/bin/dde-launcher -s
2021-10-04, 23:56:06.049 [Warning] [  ] qUncompress: Z_DATA_ERROR: Input data is corrupted
2021-10-04, 23:56:06.050 [Warning] [dsvgrenderer.cpp     Dtk::Gui::DSvgRenderer::load        291] DSvgRenderer::load: rsvg_handle_fill_with_data: assertion `data_len != 0' failed
malloc(): unsorted double linked list corrupted
已放弃
duanyao@duanyao-laptop-a:~$ /usr/bin/dde-launcher -s
free(): invalid pointer
已放弃
duanyao@duanyao-laptop-a:~$ /usr/bin/dde-launcher -s
段错误
duanyao@duanyao-laptop-a:~$ /usr/bin/dde-launcher -s
2021-10-04, 23:56:17.477 [Warning] [  ] qUncompress: Z_DATA_ERROR: Input data is corrupted
2021-10-04, 23:56:17.477 [Warning] [dsvgrenderer.cpp     Dtk::Gui::DSvgRenderer::load        291] DSvgRenderer::load: rsvg_handle_fill_with_data: assertion `data_len != 0' failed
段错误
duanyao@duanyao-laptop-a:~$ /usr/bin/dde-launcher -s
段错误
duanyao@duanyao-laptop-a:~$ /usr/bin/dde-launcher -s
2021-10-04, 23:56:23.206 [Warning] [  ] qUncompress: Z_DATA_ERROR: Input data is corrupted
2021-10-04, 23:56:23.206 [Warning] [dsvgrenderer.cpp     Dtk::Gui::DSvgRenderer::load        291] DSvgRenderer::load: rsvg_handle_fill_with_data: assertion `data_len != 0' failed
段错误
duanyao@duanyao-laptop-a:~$ /usr/bin/dde-launcher -s
2021-10-04, 23:56:30.555 [Warning] [  ] qUncompress: Z_DATA_ERROR: Input data is corrupted
2021-10-04, 23:56:30.555 [Warning] [dsvgrenderer.cpp     Dtk::Gui::DSvgRenderer::load        291] DSvgRenderer::load: rsvg_handle_fill_with_data: assertion `data_len != 0' failed
段错误
duanyao@duanyao-laptop-a:~$ /usr/bin/dde-launcher -s
段错误
duanyao@duanyao-laptop-a:~$ /usr/bin/dde-launcher -s
2021-10-04, 23:56:36.754 [Warning] [  ] qUncompress: Z_DATA_ERROR: Input data is corrupted
2021-10-04, 23:56:36.755 [Warning] [dsvgrenderer.cpp     Dtk::Gui::DSvgRenderer::load        291] DSvgRenderer::load: rsvg_handle_fill_with_data: assertion `data_len != 0' failed
段错误
```

dconf-editor:
```
com.deepin.dde.launcher fullscreen false
com.deepin.dde.launcher display-mode 'free'
```
```
duanyao@duanyao-laptop-a:~$ /usr/bin/dde-launcher -s
2021-10-05, 00:00:59.714 [Warning] [  ] Length 2 runs past end of data
^C
duanyao@duanyao-laptop-a:~$ /usr/bin/dde-launcher -s
2021-10-05, 00:01:13.389 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:13.395 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:13.395 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:13.395 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:13.395 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:13.396 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:13.397 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:13.397 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:13.397 [Warning] [  ] Length 2 runs past end of data
^C
duanyao@duanyao-laptop-a:~$ /usr/bin/dde-launcher -s
^C
duanyao@duanyao-laptop-a:~$ /usr/bin/dde-launcher -s
2021-10-05, 00:01:27.672 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:27.673 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:27.673 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:27.675 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:27.675 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:27.675 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:27.676 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:27.676 [Warning] [  ] Length 2 runs past end of data
^C
duanyao@duanyao-laptop-a:~$ /usr/bin/dde-launcher -s
2021-10-05, 00:01:31.784 [Warning] [  ] Length 2 runs past end of data
^C
duanyao@duanyao-laptop-a:~$ /usr/bin/dde-launcher -s
2021-10-05, 00:01:53.857 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:53.862 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:53.863 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:53.863 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:53.863 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:53.864 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:53.865 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:53.865 [Warning] [  ] Length 2 runs past end of data
2021-10-05, 00:01:53.865 [Warning] [  ] Length 2 runs past end of data
```

## valgrind 2

```
com.deepin.dde.launcher fullscreen false
com.deepin.dde.launcher display-mode 'free'
```

```
valgrind --tool=memcheck --trace-children=yes /usr/bin/dde-launcher -s

duanyao@duanyao-laptop-a:~$ valgrind --tool=memcheck --trace-children=yes /usr/bin/dde-launcher -s
==7906== Memcheck, a memory error detector
==7906== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==7906== Using Valgrind-3.14.0 and LibVEX; rerun with -h for copyright info
==7906== Command: /usr/bin/dde-launcher -s
==7906== 
==7906== Conditional jump or move depends on uninitialised value(s)
==7906==    at 0x4FC38C7: Dtk::Widget::DBlurEffectWidgetPrivate::getMaskColorAlpha() const (in /usr/lib/x86_64-linux-gnu/libdtkwidget.so.5.5.3)
==7906==    by 0x4FC3AA7: Dtk::Widget::DBlurEffectWidgetPrivate::setMaskColor(QColor const&) (in /usr/lib/x86_64-linux-gnu/libdtkwidget.so.5.5.3)
==7906==    by 0x4FC3DF2: Dtk::Widget::DBlurEffectWidget::setMaskAlpha(unsigned char) (in /usr/lib/x86_64-linux-gnu/libdtkwidget.so.5.5.3)
==7906==    by 0x4A9E33: ??? (in /usr/bin/dde-launcher)
==7906==    by 0x468A26: ??? (in /usr/bin/dde-launcher)
==7906==    by 0x468ED4: ??? (in /usr/bin/dde-launcher)
==7906==    by 0x43EDDB: ??? (in /usr/bin/dde-launcher)
==7906==    by 0x696A09A: (below main) (libc-start.c:308)
==7906== 
==7906== Conditional jump or move depends on uninitialised value(s)
==7906==    at 0x5D19AC7: QColor::setAlpha(int) (in /usr/lib/x86_64-linux-gnu/libQt5Gui.so.5.15.1)
==7906==    by 0x4FC3A9D: Dtk::Widget::DBlurEffectWidgetPrivate::setMaskColor(QColor const&) (in /usr/lib/x86_64-linux-gnu/libdtkwidget.so.5.5.3)
==7906==    by 0x4FC3DF2: Dtk::Widget::DBlurEffectWidget::setMaskAlpha(unsigned char) (in /usr/lib/x86_64-linux-gnu/libdtkwidget.so.5.5.3)
==7906==    by 0x4A9E33: ??? (in /usr/bin/dde-launcher)
==7906==    by 0x468A26: ??? (in /usr/bin/dde-launcher)
==7906==    by 0x468ED4: ??? (in /usr/bin/dde-launcher)
==7906==    by 0x43EDDB: ??? (in /usr/bin/dde-launcher)
==7906==    by 0x696A09A: (below main) (libc-start.c:308)
==7906== 
==7906== Conditional jump or move depends on uninitialised value(s)
==7906==    at 0x4B5A3A2: __Appearance::onPropertyChanged(QString const&, QVariant const&) (in /usr/lib/x86_64-linux-gnu/libdframeworkdbus.so.2.0.0)
==7906==    by 0x637271B: ??? (in /usr/lib/x86_64-linux-gnu/libQt5Core.so.5.15.1)
==7906==    by 0x4C8C829: DBusExtendedAbstractInterface::propertyChanged(QString const&, QVariant const&) (in /usr/lib/x86_64-linux-gnu/libdframeworkdbus.so.2.0.0)
==7906==    by 0x4AD16D3: DBusExtendedAbstractInterface::onAsyncPropertyFinished(QDBusPendingCallWatcher*) (in /usr/lib/x86_64-linux-gnu/libdframeworkdbus.so.2.0.0)
==7906==    by 0x4C8CBAB: ??? (in /usr/lib/x86_64-linux-gnu/libdframeworkdbus.so.2.0.0)
==7906==    by 0x637274C: ??? (in /usr/lib/x86_64-linux-gnu/libQt5Core.so.5.15.1)
==7906==    by 0x530161E: QDBusPendingCallWatcher::finished(QDBusPendingCallWatcher*) (in /usr/lib/x86_64-linux-gnu/libQt5DBus.so.5.15.1)
==7906==    by 0x530171F: ??? (in /usr/lib/x86_64-linux-gnu/libQt5DBus.so.5.15.1)
==7906==    by 0x636A465: QObject::event(QEvent*) (in /usr/lib/x86_64-linux-gnu/libQt5Core.so.5.15.1)
==7906==    by 0x54EF82D: QApplicationPrivate::notify_helper(QObject*, QEvent*) (in /usr/lib/x86_64-linux-gnu/libQt5Widgets.so.5.15.1)
==7906==    by 0x54F6557: QApplication::notify(QObject*, QEvent*) (in /usr/lib/x86_64-linux-gnu/libQt5Widgets.so.5.15.1)
==7906==    by 0x4F8FEB5: Dtk::Widget::DApplication::notify(QObject*, QEvent*) (in /usr/lib/x86_64-linux-gnu/libdtkwidget.so.5.5.3)
==7906== 
==7906== Conditional jump or move depends on uninitialised value(s)
==7906==    at 0x4B5A3A4: __Appearance::onPropertyChanged(QString const&, QVariant const&) (in /usr/lib/x86_64-linux-gnu/libdframeworkdbus.so.2.0.0)
==7906==    by 0x637271B: ??? (in /usr/lib/x86_64-linux-gnu/libQt5Core.so.5.15.1)
==7906==    by 0x4C8C829: DBusExtendedAbstractInterface::propertyChanged(QString const&, QVariant const&) (in /usr/lib/x86_64-linux-gnu/libdframeworkdbus.so.2.0.0)
==7906==    by 0x4AD16D3: DBusExtendedAbstractInterface::onAsyncPropertyFinished(QDBusPendingCallWatcher*) (in /usr/lib/x86_64-linux-gnu/libdframeworkdbus.so.2.0.0)
==7906==    by 0x4C8CBAB: ??? (in /usr/lib/x86_64-linux-gnu/libdframeworkdbus.so.2.0.0)
==7906==    by 0x637274C: ??? (in /usr/lib/x86_64-linux-gnu/libQt5Core.so.5.15.1)
==7906==    by 0x530161E: QDBusPendingCallWatcher::finished(QDBusPendingCallWatcher*) (in /usr/lib/x86_64-linux-gnu/libQt5DBus.so.5.15.1)
==7906==    by 0x530171F: ??? (in /usr/lib/x86_64-linux-gnu/libQt5DBus.so.5.15.1)
==7906==    by 0x636A465: QObject::event(QEvent*) (in /usr/lib/x86_64-linux-gnu/libQt5Core.so.5.15.1)
==7906==    by 0x54EF82D: QApplicationPrivate::notify_helper(QObject*, QEvent*) (in /usr/lib/x86_64-linux-gnu/libQt5Widgets.so.5.15.1)
==7906==    by 0x54F6557: QApplication::notify(QObject*, QEvent*) (in /usr/lib/x86_64-linux-gnu/libQt5Widgets.so.5.15.1)
==7906==    by 0x4F8FEB5: Dtk::Widget::DApplication::notify(QObject*, QEvent*) (in /usr/lib/x86_64-linux-gnu/libdtkwidget.so.5.5.3)
==7906== 
==7906== Conditional jump or move depends on uninitialised value(s)
==7906==    at 0x4FC3DE4: Dtk::Widget::DBlurEffectWidget::setMaskAlpha(unsigned char) (in /usr/lib/x86_64-linux-gnu/libdtkwidget.so.5.5.3)
==7906==    by 0x637271B: ??? (in /usr/lib/x86_64-linux-gnu/libQt5Core.so.5.15.1)
==7906==    by 0x4C9DD42: __Appearance::OpacityChanged(double) const (in /usr/lib/x86_64-linux-gnu/libdframeworkdbus.so.2.0.0)
==7906==    by 0x4B5A3B6: __Appearance::onPropertyChanged(QString const&, QVariant const&) (in /usr/lib/x86_64-linux-gnu/libdframeworkdbus.so.2.0.0)
==7906==    by 0x637271B: ??? (in /usr/lib/x86_64-linux-gnu/libQt5Core.so.5.15.1)
==7906==    by 0x4C8C829: DBusExtendedAbstractInterface::propertyChanged(QString const&, QVariant const&) (in /usr/lib/x86_64-linux-gnu/libdframeworkdbus.so.2.0.0)
==7906==    by 0x4AD16D3: DBusExtendedAbstractInterface::onAsyncPropertyFinished(QDBusPendingCallWatcher*) (in /usr/lib/x86_64-linux-gnu/libdframeworkdbus.so.2.0.0)
==7906==    by 0x4C8CBAB: ??? (in /usr/lib/x86_64-linux-gnu/libdframeworkdbus.so.2.0.0)
==7906==    by 0x637274C: ??? (in /usr/lib/x86_64-linux-gnu/libQt5Core.so.5.15.1)
==7906==    by 0x530161E: QDBusPendingCallWatcher::finished(QDBusPendingCallWatcher*) (in /usr/lib/x86_64-linux-gnu/libQt5DBus.so.5.15.1)
==7906==    by 0x530171F: ??? (in /usr/lib/x86_64-linux-gnu/libQt5DBus.so.5.15.1)
==7906==    by 0x636A465: QObject::event(QEvent*) (in /usr/lib/x86_64-linux-gnu/libQt5Core.so.5.15.1)
==7906== 
```

## 
杀死 dde-lock 和 dde-lowpower 以后，dde-launcher 无法启动了：
```
duanyao@duanyao-laptop-a:~/project/deb-src/dde-launcher-5.4.40$ debian/dde-launcher/usr/bin/dde-launcher -s
2021-10-04, 21:18:09.532 [Info   ] [  ] session locked, can not show launcher
2021-10-04, 21:18:25.611 [Info   ] [  ] session locked, can not show launcher
2021-10-04, 21:18:26.307 [Info   ] [  ] session locked, can not show launcher
2021-10-04, 21:18:26.522 [Info   ] [  ] session locked, can not show launcher
2021-10-04, 21:18:26.732 [Info   ] [  ] session locked, can not show launcher
```