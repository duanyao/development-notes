## 参考资料
[1] overlayfs Documentation
https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/Documentation/filesystems/overlayfs.txt

[2] Linux Filesystem Overlay - what is workdir used for? (OverlayFS)
https://unix.stackexchange.com/questions/324515/linux-filesystem-overlay-what-is-workdir-used-for-overlayfs

[3] How can I overlayfs the root filesystem on linux?
http://stackoverflow.com/questions/41119656/how-can-i-overlayfs-the-root-filesystem-on-linux

[4] Protecting the Root Filesystem on Ubuntu with Overlayroot 
https://spin.atomicobject.com/2015/03/10/protecting-ubuntu-root-filesystem/

[5] overlayroot
https://github.com/chesty/overlayroot

## 概述

overlayfs 是 linux 的一种“合并”式文件系统。它由以下部分组成[1]：

* 下位目录（lowerdir），在 overlayfs 中是只读的。可以有1到多个。
* 上位目录（upperdir），在 overlayfs 中是读写的。可以有0到1个。
* 挂载点目录，即 overlayfs 的挂载位置。只能是一个。可以是下位目录或上位目录中的一个，或者是另外的目录。
* 工作目录（workdir），在 overlayfs 中是读写的。必须与上位目录在同一原始文件系统上（从而允许原子的文件移动）。初始状态下，工作目录应该是空的。
  它用于处理上位目录中的修改事务，不会长期存储数据[2]。
  如果没有上位目录，则不需要工作目录，整个 overlayfs将是只读的。

整个 overlayfs 的数据由下位目录和上位目录合并而成[1]：
* 读取一个路径时：
  * 如果该路径在下位和上位中都是目录，则两个目录合并输出。同名的对象递归处理。
  * 如果该路径在下位和上位中不都是目录，则选取上位目录的对象。
* 以可写方式打开一个普通文件时，如果该文件仅存在于下位目录，则先复制到上位目录中再打开。
  打开特殊文件则有特殊的行为，写入命名管道或者设备文件时，并不会导致该文件被复制到上位目录；
  即使整个 overlayfs 是只读的，也可以写入命名管道或者设备文件，只是修改时间不会更新。
* 新建文件/目录时，在上位目录中新建。
* 删除文件/目录时，如果它在下位目录中存在，则在上位目录中产生一个同名的“标记文件”（具有特殊的扩展属性），使其在挂载点目录中不可见。
* 重命名文件时，如果只存在于下位目录，相当于复制+删除。
* 重命名目录，有两种模式，（1）复制+删除，这是默认的模式。（2）重定向，让上位目录中的新目录有一个特殊的扩展属性，指向原来的下位目录。上位目录中的旧目录则标记为已删除。

命令语法：
mount -t overlay overlay -o lowerdir=./low,upperdir=./high,workdir=./work ./merge
mount -t overlay overlay -o lowerdir=/lower1:/lower2:/lower3 /merged

fstab 语法：

overlay /merge overlay lowerdir=/low,upperdir=/high,workdir=/work 0 0

overlay /merged overlay noauto,x-systemd.automount,lowerdir=/lower,upperdir=/upper,workdir=/work 0 0

似乎 overlay 的挂载也需要 root 权限，但似乎曾经用非 root 权限挂载过？

### remount rw/ro 问题
overlay 的下层目录如果是个普通文件系统，则不能通过 mount -o remount,rw 来改变读写属性。
overlayroot 利用这个特性防止下层文件系统被修改[4,5]。


## 内核模块
kernel/fs/overlayfs/overlay.ko
无其它依赖。

## 用 overlayfs 作为根文件系统
这必须在启动过程的早期做。大概的原理是[3]：

* 假定根分区和可写分区分别是 /dev/sda1 和 /dev/sda2。
* 在 initramfs 的 /init 进程中，先挂载根分区和可写分区，如 /mnt/ro_rootfs 和 /mnt/rw_rootfs，
  然后组合成一个 overlayfs，如 /mnt/overlayfs。
* bind mount /proc /dev /sys 到 /mnt/overlayfs/ 下的对应目录。
* chroot 到 /mnt/overlayfs，执行 /init （实际上是 /mnt/overlayfs/init）。

可见，这需要修改 initramfs。单纯修改根分区的 /etc/fstab 是不起作用的。

ubuntu、deepin 等通过软件包 overlayroot 可以直接支持 overlayfs 作为根文件系统[4,5]。
配置文件是 /etc/overlayroot.conf ，通过内核参数 overlayroot=disabled 可以禁用。

