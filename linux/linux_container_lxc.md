## 参考资料

### 1. 入门、基本操作
[1.1] lxc getting-started
https://linuxcontainers.org/lxc/getting-started/

[1.2] Ubuntu 服务器指南 » 虚拟化 » LXC
https://help.ubuntu.com/lts/serverguide/lxc.html

### 问题和解决
[2.1] LXC 2.1 has been released
https://discuss.linuxcontainers.org/t/lxc-2-1-has-been-released/487

[2.2] lxc-create hangs and finally fails
https://askubuntu.com/questions/544597/lxc-create-hangs-and-finally-fails

### 配置和高级操作
[4.1] LXC linux container
https://www.stgraber.org/2013/12/20/lxc-1-0-blog-post-series/

[4.2] lxc-usernet
https://linuxcontainers.org/lxc/manpages/man5/lxc-usernet.5.html

[4.3] lxc.container.conf
https://linuxcontainers.org/lxc/manpages/man5/lxc.container.conf.5.html

[4.4] LXC Security
https://linuxcontainers.org/lxc/security/

### 相关专题
[5.1] Seccomp
https://en.wikipedia.org/wiki/Seccomp

[6.1] subuid
http://man7.org/linux/man-pages/man5/subuid.5.html

[7.1] cgmanager
https://linuxcontainers.org/cgmanager/introduction/

[7.2] LXC container won't start after systemd upgrade
https://github.com/lxc/lxc/issues/1554

[7.3] Cgroup namespace
http://hustcat.github.io/cgroup-namespace/

[7.4] CGroup Namespaces
https://lwn.net/Articles/618873/

[8.1] overview of Linux user namespaces
http://man7.org/linux/man-pages/man7/user_namespaces.7.html

[8.2] How to enable user_namespaces in the kernel? (For unprivileged `unshare`.)
https://unix.stackexchange.com/questions/303213/how-to-enable-user-namespaces-in-the-kernel-for-unprivileged-unshare

## 简介
依赖的技术：

cgmanager[7.1]
  包 cgmanager。这是 LXC 项目提供的用于管理 cgroup 的工具，不过在支持 user namespace 的系统上（4.6+内核）已经不建议使用了。详见 cgmanager 和 cgroup namespace 和 lxcfs 一节。
  
newuidmap and newgidmap
  debian 包 uidmap，alpine 包 shadow-uidmap

Linux kernel >= 3.12

可以选的：
libcap (to allow for capability drops)
libapparmor (to set a different apparmor profile for the container)
libselinux (to set a different selinux context for the container)
libseccomp (to set a seccomp policy for the container) 【seccomp 是对进程可使用的系统调用的限制措施】
libgnutls (for various checksumming)
liblua (for the LUA binding)
python3-dev (for the python3 binding)

## 安装
```
sudo apt install lxc lxcfs
```

## 非特权容器（unprivileged container）

非特权容器将容器内的uid 0（root）映射到容器外的 nobody 用户，因此不会有容器逃逸的安全问题[4.4]。
非特权容器的安全不依赖于 SELinux, AppArmor, Seccomp, capabilities, 但有了更好[4.4]。

非特权容器的 root 用户不能做以下事情：

    mounting most of filesystems
    creating device nodes
    any operation against a uid/gid outside of the mapped set

这样，一般的 linux 发行版都不能作为非特权容器运行，需要专门的模板镜像。

操作步骤：

* 检查 subuid subgid
    以下文件里应该配置了允许你的账号创建若干子用户、组[6.1]：

    /etc/subuid
    <your username>:<初始subuid>:<subuid总数>
    /etc/subgid
    <your username>:<初始subgid>:<subgid总数>
    
    初始subuid和初始subgid应该超过了/etc/login.defs 中定义的 UID_MIN 和 GID_MIN，所以一般不会与普通用户混淆。
    
* /etc/lxc/lxc-usernet
    [4.2]
    修改/新建 /etc/lxc/lxc-usernet：
    <your username> veth lxcbr0 10
    语法是：
    user type bridge number 
    type 目前只能是 veth。lxc 的 bridge 总是 lxcbr0 ，虽然名字原则上是随意的。

* 建立 ~/.config/lxc/default.conf
  从 /etc/lxc/default.conf 复制作为起点。语法参考[4.3]（注意有些旧写法，例如 net->network，idmap->id_map）。

```
# 网络类型。
#  none: 容器共享主机的网口，这样容器对网口的修改也会影响主机
#  empty: 只创建 loopback
#  veth: 
#  lxc:
#  vlan:
#  macvlan:
lxc.network.type = empty
# uid 映射，语法：u|g 容器内第一个uid、gid 主机的第一个uid、gid 映射的总数（请参考/etc/subuid|subgid）
lxc.id_map = u 0 951968 65536
lxc.id_map = g 0 951968 65536
```

* 创建 unprivileged container

$ lxc-create -t download -n my-container
这样会显示一个可以下载的发行版的列表，选择一个以后将下载镜像。
下载和创建运行环境完成后，可以启动容器：
  lxc-start -n my-container -d
其他操作见“基本容器操作”一节。

如果出现以下错误，需要启用非特权 user namespaces，详见“user namespaces”一节。
```
unshare: Operation not permitted
read pipe: Permission denied
lxc-create: lxccontainer.c: do_create_container_dir: 983 Failed to chown container dir
lxc-create: tools/lxc_create.c: main: 318 Error creating container my-container
```

如果出现以下错误：
```
Setting up the GPG keyring
ERROR: Unable to fetch GPG key from keyserver.
lxc-create: lxccontainer.c: create_run_template: 1295 container creation template for my-container failed
lxc-create: tools/lxc_create.c: main: 318 Error creating container my-container
```

可能的原因是keyserver域名 _pgpkey-http._tcp.pool.sks-keyservers.net 无法解析（具体问题需要抓包分析）。
指定另一个 keyserver 可能有用[2.2]：
lxc-create -t download -n my-container -- --keyserver hkp://p80.pool.sks-keyservers.net:80

## 基本容器操作
下载、创建：
lxc-create -t download -n my-container
lxc-create -t download -n my-container -- --keyserver hkp://p80.pool.sks-keyservers.net:80
lxc-create -t download -n my-container -- --dist ubuntu --release xenial --arch amd64 --keyserver hkp://p80.pool.sks-keyservers.net:80

启动：
lxc-start -d -n my-container
lxc-start -F -n my-container
-d 的意思是作为 daemon 运行，这也是默认行为。
-F 的意思是前台运行，并连接到控制台。

显示状态：
lxc-info -n my-container

列出容器：
lxc-ls -f

连接到控制台：
lxc-attach -n my-container

停止：
lxc-stop -n my-container

销毁:
lxc-destroy -n my-container

## unprivileged containers as root
这是指以 root 用户启动的非特权容器。
步骤：
* 在 /etc/subuid 和 /etc/subgid 中为 root 用户指定 subuid、subgid 的范围（默认不存在）。
* 在 /etc/lxc/default.conf 中指定 lxc.id_map 。

## privileged containers
如果系统支持非特权容器，则不应再使用特权容器[4.4]。
特权容器中的 uid 0 是映射到主机的 uid 0 的，所以是不安全的。
对于容器逃逸的防护主要是靠 MAC (apparmor, selinux), seccomp, capabilities，namespaces。

sudo lxc-create -t download -n privileged-container

## 容器的特点
### 进程关系
非特权容器的进程树类似这样（host 视角）：

lxc-start   <host启动容器的用户>
  init      <容器内的 root 用户映射到 host 的uid，/etc/subuid 指定>
    getty   <容器内的 root 用户映射到 host 的uid，/etc/subuid 指定>
    
lxc-attach  <host启动容器的用户>
  ash       <容器内的 root 用户映射到 host 的uid，/etc/subuid 指定> # 根据发行版也可能是其它 shell。
  
### 目录结构

非特权容器的文件位置在[1.2]：
~/.local/share/lxc/
特权容器在[1.2]:
/var/lib/lxc/

容器运行时目录下的内容
<容器id>/
  config   # 配置文件，初始内容是从 ~/.config/lxc/default.conf 得到的。
  <容器id>.log # 日志
  rootfs/  # 根文件系统，所有者是容器的 uid 0 映射到主机的 uid。
    bin/
    dev/


### /proc/<pid>/cgroup
29072 是容器内 pid 0 的主机 pid。

```
$ cat /proc/29072/cgroup 
10:perf_event:/
9:pids:/user.slice/user-1000.slice/session-16.scope
8:devices:/user.slice
7:memory:/user/duanyao/0/lxc/my-container
6:net_cls,net_prio:/
5:blkio:/user.slice
4:freezer:/user/duanyao/0/lxc/my-container
3:cpu,cpuacct:/user.slice
2:cpuset:/
1:name=systemd:/user.slice/user-1000.slice/session-16.scope/lxc/my-container
0::/user.slice/user-1000.slice/session-16.scope
```
以上，“0::”开头的目录都是相对于/sys/fs/cgroup/unified/
而，*:memory: ，*:blkio: 等开头的是相对 /sys/fs/memory/ 等的（cgroup v1）。



```
$ cat /proc/29072/uid_map 
         0     951968      65536
$ cat /proc/29072/gid_map 
         0     951968      65536
```
    
## hybrid cgroup layout
采用 cgroup v2 的系统一般采用 hybrid cgroup layout，之前的称为 unified cgroup layout。
识别特征是挂载于 /sys/fs/cgroup/unified 的 cgroupv2 文件系统。
使用 systemd 233+ 默认采用 hybrid cgroup layout，包括 deepin 15.5。
LXC 2.1+ 开始支持 hybrid cgroup layout [2.1]，cgmanager 不支持 hybrid cgroup layout。
在 hybrid cgroup layout 的系统上运行 LXC 2.0-，也是可行的；如果报错，可能的解法是增加内核参数 systemd.unified_cgroup_hierarchy=false [7.2]。

## cgmanager 和 cgroup namespace 和 lxcfs
cgmanager 这是 LXC 项目提供的用于管理 cgroup 的工具，不过已经不建议使用了[7.1]。
替代技术是 lxcfs 和 cgroup namespace[7.3]。

有了cgroup namespace后，每个namespace中的进程都有自己cgroupns root和cgroup filesystem视图。

## user namespaces
让非特权用户可以使用 user namespaces（以及 unshare 命令）[8.2]：

  sysctl -w kernel.unprivileged_userns_clone=1
然后可以
  unshare --user --map-root-user --mount-proc --pid --fork -Cm bash
root用户只需要

  unshare -Cm bash

