## 参考资料
[1] https://www.thegeekdiary.com/how-to-find-which-rpm-package-provides-a-specific-file-or-library-in-rhel-centos/
[2] https://www.tecmint.com/view-yum-history-to-find-packages-info/
[3] https://www.golinuxcloud.com/yum-install-specific-version/
[4] https://serverfault.com/questions/372481/yum-simulate-install

## 技巧

### yum-utils
yum-utils 含有一些必要但是没有预装的工具。

```
yum install yum-utils
```
### 列出仓库

yum repolist
yum repolist all

仓库定义文件在 /etc/yum.repos.d/*.repo 。如果扩展名不是 .repo ，会被忽略。

### 启用和停用一个仓库
```
yum-config-manager --enable repository
yum-config-manager --disable repository
```
yum-config-manager 是 yum-utils 的一部分，但没有预装。

### 查看包信息

yum info nodejs
yum list intel-openvino*
yum list installed intel-openvino*
yum grouplist

### 安装常用工具

```
yum install centos-release-scl
yum install epel-release
yum install net-tools
```

### 更新元数据

```
yum clean all && yum makecache
```

### 清除缓存

```
yum clean all
dnf clean all
```

### 安装本地或网络 rpm 包
```
yum install /path/to/foo.rpm
yum install https://gosspublic.alicdn.com/ossfs/ossfs_1.80.6_centos8.0_x86_64.rpm
rpm -i /path/to/foo.rpm
```

本地 rpm 不满足依赖关系时，yum 会尝试下载依赖的 rpm 包，rpm 命令则不会。

### 根据文件名搜索包
[1]
```
rpm -q --whatprovides [file name]
rpm -qf [file name] # 全路径或相对路径
yum whatprovides libstdc++
yum whatprovides *bin/ls
```
### 列出包内文件

```
rpm -ql package-name
rpm -qlp package.rpm 
```

### 模拟执行

```
yum install --assumeno <package> 
yum remove --assumeno <package>
yum check-update
yum check-update <package> 
```
### 安装/删除日志

日志文件 /var/log/yum.log
查看命令历史 yum history

### 
yum clean all
yum makecache

cd /etc/yum.repos.d/
for i in `ls`;do sed -i 's/mirrors.cloud.aliyuncs.com/mirrors.aliyun.com/g' $i;done
for i in `ls`;do sed -i 's/$releasever/$releasever-stream/g' $i;done
