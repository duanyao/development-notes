## 参考资料
[1] fio - Flexible I/O tester rev. 3.36
https://fio.readthedocs.io/en/latest/fio_doc.html

[2] 在 Linux 中使用 Fio 来测评硬盘性能 
https://blog.csdn.net/qq_34556414/article/details/121453449


## 测试案例
```
# 创建被测试文件
dd if=/dev/zero of=/media/safe_share/tf2 bs=1G count=200
214748364800 bytes (215 GB, 200 GiB) copied, 745.116 s, 288 MB/s

sysctl vm.drop_caches=1 # 清理缓存，并非必须，因为fio 设置了--direct=1 会无视缓存。

fio -filename=/media/safe_share/tf2 --direct=1 --iodepth 64 --thread --rw=randrw --randrepeat=0 --ioengine=libaio --bs=16k --fsync=1 --size=200G --io_size=1G --numjobs=10 --runtime=1000 --group_reporting --name=rw16k

   read: IOPS=4432, BW=69.2MiB/s (72.6MB/s)(5120MiB/73941msec)
   write: IOPS=4431, BW=69.2MiB/s (72.6MB/s)(5120MiB/73941msec); 0 zone resets
   READ: bw=69.2MiB/s (72.6MB/s), 69.2MiB/s-69.2MiB/s (72.6MB/s-72.6MB/s), io=5120MiB (5369MB), run=73941-73941msec
  WRITE: bw=69.2MiB/s (72.6MB/s), 69.2MiB/s-69.2MiB/s (72.6MB/s-72.6MB/s), io=5120MiB (5368MB), run=73941-73941msec

sysctl vm.drop_caches=1

fio -filename=/media/safe_share/tf2 --direct=1 --iodepth 64 --thread --rw=randrw --randrepeat=0 --ioengine=libaio --bs=1M --fsync=1 --size=5G --io_size=1G --numjobs=10 --runtime=1000 --group_reporting --name=rw1m-5G

  read: IOPS=348, BW=349MiB/s (366MB/s)(5027MiB/14421msec)
  write: IOPS=361, BW=361MiB/s (379MB/s)(5213MiB/14421msec); 0 zone resets
   READ: bw=349MiB/s (366MB/s), 349MiB/s-349MiB/s (366MB/s-366MB/s), io=5027MiB (5271MB), run=14421-14421msec
  WRITE: bw=361MiB/s (379MB/s), 361MiB/s-361MiB/s (379MB/s-379MB/s), io=5213MiB (5466MB), run=14421-14421msec
```

--filename：被测试文件的路径。最好远大于物理内存，以避免受益于操作系统的缓存机制。
--bs=1M：测试的块大小。读写一块计量为一个IOP。
--rw=randrw : 测试随机读写。
--randrepeat=0|1 : 不采用|采用重复随机模式，默认1。在重复随机模式下，如果重复执行相同的fio命令，则每次的访问模式是相同的。这意味着，如果总的测试数据量不是远大于物理内存，则后续的重复执行可能受益于操作系统的缓存机制。在非重复随机模式下，如果重复执行相同的fio命令，则每次的访问模式都不同。
--size=5G：被测试的文件被访问的范围。可以小于被测试的文件的大小，这意味着只有文件的一部分被读写。
--numjobs=10：测试的并发任务数。如果指定 --thread，则用10个线程来实现。
--io_size=1G：每个并发任务的测试数据量。总测试数据量为 numjobs * io_size 。
--group_reporting：汇总所有并发任务的测试结果。
--ioengine=libaio：使用异步I/O。
--direct=0|1：不使用|使用直接I/O，默认0。1 对应 open() 的 O_DIRECT 选项，减少或消除操作系统 cache 的影响。注意有的操作系统（OpenBSD）和文件系统（tmpfs, ZFS）不支持 direct=1。
--runtime=1000：测试的最长持续时间，秒。

输出结果中，IOPS表示每秒的读写次数。读写一块（--bs）计量为一个IOP。BW为带宽。
