## 指标的种类

### 计数器 Counter
单调增加的整数，可描述请求数量等。
### 仪表盘 Gauge
可增减的数值，描述内存使用量等。

### 摘要 Summary
一个事件的属性值，例如一次请求的耗时等。

### 直方图 Histogram

## fuse 文件系统监控 fuse-ebpf-exporter

### 获取源码
```
git clone https://github.com/syseleven/fuse-ebpf-exporter fuse-ebpf-exporter.git

# 或者：

git clone https://gitee.com/duanyao/fuse-ebpf-exporter fuse-ebpf-exporter.git
```

### 源码补丁
此补丁已经包含在 https://gitee.com/duanyao/fuse-ebpf-exporter 中。

修改 prometheus-ebpf-exporter.py：
* 缩进错误。将 `\t` 替换为 8 个空格。
* 作用域问题。`CustomCollector::collect()` 中增加 `global b`。
* 初始化顺序错误：`REGISTRY.register(CustomCollector())` 移动到 `b = BPF(text=bpf_text)` 之后。
* 同名结构体定义问题。同名定义在 `/usr/src/linux-headers-xxx/include/linux/kref.h`，二进制上是兼容的，但变量名不同。可以采取以下方法之一修改：
  1. 将 `struct kref` 改名为 `struct kref2`。适用于 linux 5.15|6.6 。
  2. 或者注释掉 `struct kref { atomic_t refcount; };` 适用于 linux 6.6 不适用于 5.15。
* python 2 到 3 的修改（可以执行 `pip install 2to3; prometheus-ebpf-exporter.py` 来获得提示。）：
  * xrange。`for usec_log2 in xrange(0, INFINITY_BUCKET) ]`。
  * 删除 `from __future__ import print_function` 。
* 将 `parser.add_argument("--minms", action="store", default="10"` 修改为 `parser.add_argument("--minms", action="store", default=10`。


### 安装依赖
注意需要用全局 python3 来运行 fuse-ebpf-exporter，其依赖也应优先安装 apt 版本而不是 pip 版本。
```
sudo apt install python3-bpfcc python3-prometheus-client
```

### 运行测试
启动服务：
```
sudo python3 prometheus-ebpf-exporter.py --events
# prometheus-ebpf-exporter.py --listen-addr localhost --listen-port 9500  --xfs --ext4 --events --minms 10 --printhistograms
```

默认端口 9500。

在另一个终端执行一些 sshfs 上的 IO 操作：
```
dd if=/dev/zero of=/home/duanyao/media/ydnfs01/safe_share/tf103 bs=10M count=1

cat /home/duanyao/media/ydnfs01/safe_share/tf103 > /dev/null
```

prometheus-ebpf-exporter.py 的控制台输出：
```
INFO:__main__:Tracing FUSE operations
INFO:__main__:TIME     COMM           PID    T      BYTES   OFF_KB   LAT(ms) FILENAME
INFO:__main__:17:01:20 b'dd'          1006587 open       0        0  243.83 b'tf103'
INFO:__main__:17:02:11 b'dd'          1006587 write 38797312        0 51080.04 b'tf103'
INFO:__main__:17:02:41 b'cat'         1008293 open       0        0  103.31 b'tf103'
INFO:__main__:17:02:43 b'cat'         1008293 read  131072        0 1844.19 b'tf103'
```

http 输出：
```
# HELP ebpf_fuse_req_latency_ms Latency of various FUSE operations
# TYPE ebpf_fuse_req_latency_ms histogram
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.001",operation="read"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.002",operation="read"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.004",operation="read"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.008",operation="read"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.016",operation="read"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.032",operation="read"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.064",operation="read"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.128",operation="read"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.256",operation="read"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.512",operation="read"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="1.024",operation="read"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="2.048",operation="read"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="4.096",operation="read"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="8.192",operation="read"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="16.384",operation="read"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="32.768",operation="read"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="65.536",operation="read"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="131.072",operation="read"} 15.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="262.144",operation="read"} 267.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="524.288",operation="read"} 289.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="1048.576",operation="read"} 292.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="2097.152",operation="read"} 297.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="4194.304",operation="read"} 297.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="8388.608",operation="read"} 297.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="16777.216",operation="read"} 297.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="33554.432",operation="read"} 297.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="67108.864",operation="read"} 297.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="134217.728",operation="read"} 297.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="268435.456",operation="read"} 297.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="536870.912",operation="read"} 297.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="1073741.824",operation="read"} 297.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="+Inf",operation="read"} 297.0
ebpf_fuse_req_latency_ms_count{filename="rest",operation="read"} 297.0
ebpf_fuse_req_latency_ms_sum{filename="rest",operation="read"} 63585.44
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.001",operation="open"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.002",operation="open"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.004",operation="open"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.008",operation="open"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.016",operation="open"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.032",operation="open"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.064",operation="open"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.128",operation="open"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.256",operation="open"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.512",operation="open"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="1.024",operation="open"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="2.048",operation="open"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="4.096",operation="open"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="8.192",operation="open"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="16.384",operation="open"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="32.768",operation="open"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="65.536",operation="open"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="131.072",operation="open"} 1.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="262.144",operation="open"} 2.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="524.288",operation="open"} 2.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="1048.576",operation="open"} 2.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="2097.152",operation="open"} 2.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="4194.304",operation="open"} 2.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="8388.608",operation="open"} 2.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="16777.216",operation="open"} 2.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="33554.432",operation="open"} 2.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="67108.864",operation="open"} 2.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="134217.728",operation="open"} 2.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="268435.456",operation="open"} 2.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="536870.912",operation="open"} 2.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="1073741.824",operation="open"} 2.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="+Inf",operation="open"} 2.0
ebpf_fuse_req_latency_ms_count{filename="rest",operation="open"} 2.0
ebpf_fuse_req_latency_ms_sum{filename="rest",operation="open"} 347.137
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.001",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.002",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.004",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.008",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.016",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.032",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.064",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.128",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.256",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="0.512",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="1.024",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="2.048",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="4.096",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="8.192",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="16.384",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="32.768",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="65.536",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="131.072",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="262.144",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="524.288",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="1048.576",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="2097.152",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="4194.304",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="8388.608",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="16777.216",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="33554.432",operation="write"} 0.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="67108.864",operation="write"} 1.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="134217.728",operation="write"} 1.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="268435.456",operation="write"} 1.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="536870.912",operation="write"} 1.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="1073741.824",operation="write"} 1.0
ebpf_fuse_req_latency_ms_bucket{filename="rest",le="+Inf",operation="write"} 1.0
ebpf_fuse_req_latency_ms_count{filename="rest",operation="write"} 1.0
ebpf_fuse_req_latency_ms_sum{filename="rest",operation="write"} 51080.038
# HELP ebpf_fuse_req_sync_outstanding The number of sync I/O requests that did not complete after being registered
# TYPE ebpf_fuse_req_sync_outstanding gauge
ebpf_fuse_req_sync_outstanding 0.0
# HELP ebpf_fuse_req_async_outstanding The number of async I/O requests that did not complete after being registered
# TYPE ebpf_fuse_req_async_outstanding gauge
ebpf_fuse_req_async_outstanding 0.0
```