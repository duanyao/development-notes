清理：

```
rm -Rf ~/.cache/thumbnails/*
rm -Rf ~/.cache/pip
rm -Rf ~/.cache/paddle

sudo apt clean # /var/cache/apt
```

查询：
```
du -m -d 1 ~/Documents/

7245    Documents/WeChat Files
0       Documents/NVIDIA Nsight Systems
0       Documents/Oray
124     Documents/IT_hardware
7368    Documents/

du -m -d 3 ~/Documents/"WeChat Files"
464     ~/Documents/WeChat Files/wxid_492cv8ogibt722/FileStorage/CustomEmotion
4459    ~/Documents/WeChat Files/wxid_492cv8ogibt722/FileStorage/Image
517     ~/Documents/WeChat Files/wxid_492cv8ogibt722/FileStorage/File
664     ~/Documents/WeChat Files/wxid_492cv8ogibt722/FileStorage/Video

du -m -d 1 ~/.cache
.cache
174     ./.cache/deepin
212     ./.cache/google-chrome
258     ./.cache/jedi  python 自动补全
10      ./.cache/mesa_shader_cache
309     ./.cache/mozilla
39      ./.cache/parso
2399    ./.cache/thumbnails

300     ./.cache/winetricks
2838    ./.cache/pip

.config
.deepinwine
.local
.npm
.nvm
  .nvm/.cache/bin
  .nvm/.cache/src
9084    ./opt
10897   ./project
17934   ./software
24251   ./t-project


$ sudo du -m -d 1 /var
30      /var/backups
678     /var/cache
131911  /var/lib
0       /var/local
1363    /var/log
1       /var/mail
0       /var/opt
1       /var/spool
22      /var/tmp
1       /var/www
26827   /var/xdroid
160828  /var

$ sudo du -m -d 1 /var/cache/
1       /var/cache/appearance
209     /var/cache/apt
6       /var/cache/cracklib
13      /var/cache/cups
8       /var/cache/debconf
30      /var/cache/deepin
1       /var/cache/dictionaries-common
0       /var/cache/flashplugin-nonfree
7       /var/cache/fontconfig
12      /var/cache/image-blur
1       /var/cache/ldconfig
1       /var/cache/lightdm
3       /var/cache/man
2       /var/cache/powertop
1       /var/cache/samba
3       /var/cache/snapd
0       /var/cache/apache2
0       /var/cache/icecc
0       /var/cache/fonts
0       /var/cache/private
389     /var/cache/pbuilder
1       /var/cache/atop.d
678     /var/cache/

$ sudo du -m -d 1 /var/lib

1592    /var/lib/apt
1669    /var/lib/lastore
266155  /var/lib/docker
269708  /var/lib

$ sudo du -m -d 1 /var/lib/docker

sudo du -d 2 -m /data/uengine/

1       /data/uengine/apk
1926    /data/uengine/data/rootfs
0       /data/uengine/data/cache
600     /data/uengine/data/data
1       /data/uengine/data/containers
1       /data/uengine/data/state
61      /data/uengine/data/logs
0       /data/uengine/data/devices
2586    /data/uengine/data

3       /data/uengine/安卓数据文件
2589    /data/uengine/
```
