[7.1] fuse-ext2
  https://github.com/alperakcan/fuse-ext2
[7.2] fuse-ext4-rs 
  https://github.com/AshyIsMe/fuse-ext4-rs

[10.1] libeatmydata
  https://packages.debian.org/buster/eatmydata
  https://launchpad.net/libeatmydata
  
[11.1] ext2fsd
  https://sourceforge.net/projects/ext2fsd/

[11.2] Ext3Fsd
  https://github.com/matt-wu/Ext3Fsd/

[11.3] Ext4Fsd
  https://github.com/bobranten/Ext4Fsd
[11.4]
  https://www.accum.se/~bosse/ext2fsd/0.70/Ext2Fsd-0.70b3w10-setup.exe
  
[12.1] ExtFS for Windows
  https://china.paragon-software.com/home-windows/extfs-for-windows-eshop
  
[13.1] wsl2-mount
  https://learn.microsoft.com/zh-cn/windows/wsl/wsl2-mount-disk

## 关闭 fsync
关闭 fsync 可以在多个层面上进行：
（1）应用层。应用程序代码中不调用 fsync 。例如 debian 的包管理器 dpkg 有选项可以不在安装时频繁调用 fsync。
（2）C 标准库层。用 LD_PRELOAD 库替换掉 fsync 函数。例子是 eatmydata [10.1]。
（3）文件系统层。让文件系统驱动忽略掉 fsync 调用。还不清楚有没有这样的例子。

## 用户空间文件系统 fuse

### ext4 fuse
Fuse-ext2 is an EXT2/EXT3/EXT4 filesystem for FUSE, and is built to work with osxfuse. [7.1]
写入操作仍然不安全。

Fuse ext4 in Rust[7.2] 。

## windows 的 linux 文件系统驱动

### ext2fsd
ext2fsd 0.69 ～ 0.70 在 windows 10 上仍然有严重 bug，要么只能读不能写[11.1]，要么会导致系统破坏（需要重置）[11.4]。

### ExtFS for Windows
商业软件，在 windows 10 上是稳定的。试用版过了试用期会降速到 5MB/s（注：经过测试，试用版一开始就限速 5MB/s），正式版国内可用信用卡购买，120元。

### wsl2-mount
在 WSL 2 中可以装载 Linux 磁盘。但似乎只能挂载整个硬盘，而非一个分区 [13.1]。
