## 参考资料
[8] write(2) - Linux man page
https://linux.die.net/man/2/write

[9] Linux Programmer's Manual PIPE(7)
http://man7.org/linux/man-pages/man7/pipe.7.html

[10] Introduction to Named Pipes
http://www.linuxjournal.com/article/2156

[11] What are guarantees for concurrent writes into a named pipe?
  https://unix.stackexchange.com/questions/68146/what-are-guarantees-for-concurrent-writes-into-a-named-pipe

[12] Named Pipes (FIFOs) on Unix with multiple readers
http://stackoverflow.com/questions/1634580/named-pipes-fifos-on-unix-with-multiple-readers

[13] How does SIGPIPE affect writer in named pipe?
http://stackoverflow.com/questions/24664233/how-does-sigpipe-affect-writer-in-named-pipe

[14] Consistent EPIPE error writing to stdout when pipe has disconnected (SIGPIPE related?)
https://github.com/nodejs/node-v0.x-archive/issues/3211

[15] Why does SIGPIPE exist?
http://stackoverflow.com/questions/8369506/why-does-sigpipe-exist

[16] http://www.cnblogs.com/mickole/p/3192909.html
linux系统编程之管道（三）：命令管道（FIFO)

## 管道的缓冲区

管道本身能暂存一定量的数据，其上限称为管道的容量（capacity），默认从4KB（早期linux）到64KB。
容量可以通过 fcntl(2) F_GETPIPE_SZ 和 F_SETPIPE_SZ 来查询和设置。允许设置的最大值为
/proc/sys/fs/pipe-max-size （或者 sysctl fs.pipe-max-size） ，默认 1048576 （1MiB） [9]。

读取缓冲已空的管道，或写入缓冲已满的管道，一般会阻塞，除非采用非阻塞IO[9]。

## 并发读写
一个管道允许有多个读者和多个写者。

* 如果有多个读者，则管道中的同一片数据只会被其中一个读者读到[12]。
* 如果有多个写者，则它们写入的数据会混合地进入管道。
  如果一次 write() 的数据长度不超过 PIPE_BUF （见 limits.h，在linux上是4KiB）则保证是原子的（不会与其它并发 write()调用的数据交叠）[9, 11]。

## 命名管道(named pipe)
参考[13]。

创建：
mkfifo <file name>

一个命名管道允许有0到多个读者和0到多个写者。有多个读写者时，管道仍然只有一条。

* 如果没有写者，读者以阻塞模式打开管道时，则被阻塞在 open() 上；以非阻塞模式打开则成功；
  read() 时，无论是否阻塞模式，都返回 0 （表示EOF）；可以不断重试，直到写者出现。
* 如果没有读者，写者以阻塞模式打开管道时，则被阻塞在 open() 上；以非阻塞模式打开，失败，得到 ENXIO 6 No such device or address；
  可以先以阻塞模式打开管道，然后用 fcntl 改为非阻塞模式。

* 多个读者写者的情形见“管道上的错误”和“并发读写”。

## 非命名管道/匿名管道(unnamed pipe)

```
#define _GNU_SOURCE             /* See feature_test_macros(7) */
#include <fcntl.h>              /* Obtain O_* constant definitions */
#include <unistd.h>

int pipefd[2];
pipe(pipefd);
```

pipe() 是 posix 标准函数，创建一个非命名管道，pipefd[0] 为读端，pipefd[1] 为写端。

通常，pipe() 与 fork() 搭配使用，在父子进程之间通信。
特别的，通过 dup2() 使 pipe 的一端取代子进程的标准输入/输出文件，就实现了父子进程之间通过标准输入/输出进行通信。

## 管道上的错误

* 如果写端的进程提前退出或关闭管道了，则：
  读端继续读取（例如 read(2)），会得到 end-of-file（EOF）[9]；
  如果读端不关闭 fd，用 select() 方法，则会连续不断地被唤醒，但如果写端重新启动并写入管道则可恢复正常；
  如果读端关闭 fd 再重新打开，用 select() 方法，则不会被唤醒，直到写端重新启动并写入管道。
  因此，对于读端得到 EOF 的正确处理方式是关闭 fd 再重新打开。
  参考 input_output_c.txt 。

* 如果读端的进程提前退出或主动关闭管道，写端继续写入就会引发 SIGPIPE 信号[8,9,15]。
  SIGPIPE 默认会导致进程终止。如果捕获了 SIGPIPE，则管道上的 write() 调用会收到 EPIPE 错误码，这时可以相应处理[8]。
  用 aprogram | head 可以模拟读端的进程提前退出的情形。

  顺带一提，node.js 在这种情况下默认捕获 SIGPIPE，并在 process.stdout|stderr 上产生 error 事件（EPIPE），如果不处理这个 error 会造成进程终止[14]。

  另外，SIGPIPE 不止在管道上，也会在 socket 上出现。

* 注意无论读者在写者之后还是之前打开命名管道，只要在写者打开命名管道后的任何时候读者数量从大于0变成0，写者还是会在下一次写入时收到SIGPIPE/EPIPE错误。

* 如果有多个读者，则其中一个退出/关闭并不会导致写者收到 SIGPIPE/EPIPE，直到最后一个读者退出才会。这点在命名管道上观察到了，不清楚普通管道。

* 一个管道的写端一旦收到 EPIPE 错误，就不可能再恢复可写（将被自动关闭）。对于命名管道，如果读端崩溃、写端收到 EPIPE 错误、即使读端再次打开命名管道，则写端也不能恢复写入。
  解决的办法有：
  * 读端采用多进程，一个可靠的进程打开命名管道但不真的去读，其它不可靠的进程打开命名管道真的去读，从而维持至少一个读者的状态[13]。
  * 让写端也崩溃，然后立即重启写端和读端进程。

注意以上错误在使用控制台作为标准流时都不会发生。要让程序能正确处理重定向到管道，就要处理以上情形。
