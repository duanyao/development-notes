# 使用 debootstrap 创建 debian 系 linux 操作系统的根文件系统和 docker 镜像
## 参考资料

[1] Debootstrap
  https://wiki.debian.org/Debootstrap

[2] How to use the command debootstrap (with examples)
  https://commandmasters.com/commands/debootstrap-linux/

[3] building base-images for docker
  https://docs.docker.com/build/building/base-images/

[4] EmDebianCrossDebootstrap 
  https://wiki.debian.org/EmDebian/CrossDebootstrap

[5] How to fix "dpkg: error processing package systemd (--configure)
  https://askubuntu.com/questions/1244976/how-to-fix-dpkg-error-processing-package-systemd-configure

[6] "Error processing package" - apt upgrade cannot run due to package configuration errors
  https://askubuntu.com/questions/1171668/error-processing-package-apt-upgrade-cannot-run-due-to-package-configuration

## 概述
debian 系（包括ubuntu等衍生版）操作系统的 debootstrap 工具可以用来制作系统镜像（根文件系统），其中包括的包是可以选择的，也有一些预设清单。注意，某个发行版默认apt仓库中的 debootstrap 一般只能制作宿主系统当前版本和以前版本的系统镜像；不过，debian衍生版的 debootstrap 一般都可以制作 debian 的系统镜像。通过交叉安装其它发行版/版本的 debootstrap ，也可以制作其它发行版或比宿主系统更新的版本的系统镜像。

## 安装 debootstrap
```
sudo apt install debootstrap
```
此命令一般会同时安装 distro-info 和 distro-info-data，如果没有，可手动安装。

使用以下命令，可列出已知的发行版和版本、代号、发布日，也就是 debootstrap 可以创建的版本：
```
distro-info -afy # 默认的，一般与本机相同
debian-distro-info -afy # 列出 debian 的版本。
ubuntu-distro-info -afy # 列出 ubuntu 的版本。
```

宿主系统中的 debootstrap（以及 distro-info 和 distro-info-data） 一般只包含当前发行版的当前版本和以前版本的数据。

## 交叉安装其它发行版/版本的 debootstrap

如果要制作其它发行版或比宿主系统更新的版本的系统镜像，可以参照以下例子更新 debootstrap 和 distro-info-data。

假设宿主系统为 ubuntu20.04，我们要制作 ubuntu24.04 和 debian12 的镜像。找一个镜像仓库（例如 `https://mirrors.tuna.tsinghua.edu.cn/ubuntu/pool/main/` ），用浏览器打开并查找到 debootstrap 和 distro-info-data 包的链接并下载安装。

```
# 最新的 ubuntu 的 debootstrap & distro-info-data
wget https://mirrors.tuna.tsinghua.edu.cn/ubuntu/pool/main/d/distro-info-data/distro-info-data_0.62_all.deb

sudo dpkg -i distro-info-data_0.62_all.deb
wget https://mirrors.tuna.tsinghua.edu.cn/ubuntu/pool/main/d/debootstrap/debootstrap_1.0.136ubuntu1_all.deb
sudo dpkg -i debootstrap_1.0.136ubuntu1_all.deb

# 最新的 debian 的 debootstrap & distro-info-data
wget https://mirrors.tuna.tsinghua.edu.cn/debian/pool/main/d/distro-info-data/distro-info-data_0.62_all.deb
sudo dpkg -i distro-info-data_0.62_all.deb
wget https://mirrors.tuna.tsinghua.edu.cn/debian/pool/main/d/debootstrap/debootstrap_1.0.136_all.deb
sudo dpkg -i debootstrap_1.0.136_all.deb
```

如果有依赖错误，则 -x 直接解压：
```
sudo dpkg -x debootstrap_1.0.136ubuntu1_all.deb /
sudo dpkg -x distro-info-data_0.62_all.deb /
```

## 使用方法
```
# debootstrap --arch <CPU指令集> --cache-dir=<apt缓存目录> --include=<额外安装的deb包> --exclude=<避免安装的deb包> --variant=minbase|buildd|fakechroot --components=<仓库的部分> <发行版版本代码> <输出镜像目录> <发行版仓库地址>
sudo debootstrap --arch amd64 --cache-dir=$PWD/cache-debian --include=ca-certificates,curl --exclude=systemd --variant=minbase --components=main,contrib,non-free buster ./debian10 https://mirrors.tuna.tsinghua.edu.cn/debian/
```
* debootstrap 必须以 root 身份运行。
* 默认生成的是最小镜像（minbase），且不包含内核。用 --variant 可以指定其他预设镜像类型：buildd 包含 C/C++ 编译工具。
* 用 --include 和 --exclude 可以增减软件包，多个包用逗号隔开。--exclude 列出的包不一定不被安装，因为有的包是被 debootstrap 强制包含的，如 systemd；有的包则被其它包依赖，需要把依赖它的包也写在 --exclude 后面。
* ca-certificates 是 apt 客户端支持 https 访问必须的，如果仓库地址是 https 则需要它，否则报告“Certificate verification failed” 错误。各个发行版的最小镜像一般都包含 ca-certificates ，但也有个别例外，如 debian 11，这时就需要放到 --include 中。
* `<发行版仓库地址>` 将用于 debootstrap 下载软件包，并且也会写入镜像的 `/etc/apt/source.list` 文件。
* --cache-dir 必须是绝对路径。
* 目标目录（`./debian10`）应当不存在或者是属于 root 的空目录。非空目录虽然不妨碍运行，但是可能出错。不属于 root 的目标目录也会导致错误，见下面“systemd 安装错误”。
* 默认的 `<仓库的部分>` 只有 main，可以用 --components 增加更多的仓库的部分，这些也会写入镜像的 `/etc/apt/source.list` 文件。可用的部分，debian 一般是 main,contrib,non-free，而 ubuntu 一般是 main,restricted,universe,multiverse，可以参考 `https://mirrors.tuna.tsinghua.edu.cn/help/debian/` 和 `https://mirrors.tuna.tsinghua.edu.cn/help/ubuntu/`。

## 可能的错误

### systemd 安装错误
debootstrap 命令：
```
mkdir -p debian11-arm64
sudo debootstrap --arch arm64 --cache-dir=/home/duanyao/custom-sys-image/cache-debian-arm64 --include=libc6,ca-certificates,curl --components=main,contrib,non-free,restricted,universe,multiverse bullseye /home/duanyao/custom-sys-image/debian11-arm64 https://mirrors.tuna.tsinghua.edu.cn/debian/
```
debootstrap 输出的结尾：
```
W: Failure trying to run: chroot "/home/duanyao/custom-sys-image/debian11-arm64" dpkg --force-overwrite --force-confold --skip-same-version --install /var/cache/apt/archives/dmsetup_2%3a1.02.175-2.1_arm64.deb /var/cache/apt/archives/libapparmor1_2.13.6-10_arm64.deb /var/cache/apt/archives/libargon2-1_0~20171227-0.2_arm64.deb /var/cache/apt/archives/libcap2_1%3a2.44-1_arm64.deb /var/cache/apt/archives/libcryptsetup12_2%3a2.3.7-1+deb11u1_arm64.deb /var/cache/apt/archives/libdevmapper1.02.1_2%3a1.02.175-2.1_arm64.deb /var/cache/apt/archives/libip4tc2_1.8.7-1_arm64.deb /var/cache/apt/archives/libjson-c5_0.15-2+deb11u1_arm64.deb /var/cache/apt/archives/libkmod2_28-1_arm64.deb /var/cache/apt/archives/systemd_247.3-7+deb11u5_arm64.deb
W: See /home/duanyao/custom-sys-image/debian11-arm64/debootstrap/debootstrap.log for details (possibly the package systemd is at fault)
```

在 debian11-arm64/debootstrap/debootstrap.log 的结尾：
```
Setting up systemd (247.3-7+deb11u5) ...
Created symlink /etc/systemd/system/getty.target.wants/getty@tty1.service -> /lib/systemd/system/getty@.service.
Created symlink /etc/systemd/system/multi-user.target.wants/remote-fs.target -> /lib/systemd/system/remote-fs.target.
Created symlink /etc/systemd/system/sysinit.target.wants/systemd-pstore.service -> /lib/systemd/system/systemd-pstore.service.
Initializing machine ID from random generator.
Detected unsafe path transition / -> /var during canonicalization of /var/log/journal.
Detected unsafe path transition / -> /var during canonicalization of /var/log/journal.
Detected unsafe path transition / -> /var during canonicalization of /var/log/journal.
dpkg: error processing package systemd (--install):
 installed systemd package post-installation script subprocess returned error exit status 73
Processing triggers for libc-bin (2.31-13+deb11u11) ...
Errors were encountered while processing:
 systemd
```

日志中没说明错误的原因。根据[5]和本地测试，此错误的诱因是：目标目录（此处为 `./debian11-arm64`）在调用 debootstrap 前就存在，且所有者不是root。

debootstrap 命令改为这样即可：
```
mkdir -p debian11-arm64
chown root:root debian11-arm64
sudo debootstrap --arch arm64 --cache-dir=/home/duanyao/custom-sys-image/cache-debian-arm64 --include=libc6,ca-certificates,curl --components=main,contrib,non-free,restricted,universe,multiverse bullseye /home/duanyao/custom-sys-image/debian11-arm64 https://mirrors.tuna.tsinghua.edu.cn/debian/
```

## 交叉构建
交叉构建是指创建不同于宿主机 CPU 架构的系统镜像，例如在 AMD64 环境下构建 ARM64 的镜像。
这需要用到 binfmt 和 qemu 的相关工具[4]，允许在宿主环境中无缝运行其它 CPU 架构的程序，也支持以 chroot 和 docker 方式运行其它 CPU 架构的程序。
```
apt install binfmt-support
apt install qemu-user-static=1:7.2+dfsg-7+deb12u7
```

目前（2024-10），qemu-user-static-8.1+ 会造成 arm64 的 /sbin/ldconfig 运行出错（segmentation fault），进而导致 debootstrap / qemu-debootstrap 出错，所以要安装 7.2，它在 debian 11/12 仓库中。

然后按正常流程运行 debootstrap 即可。有的资料说应该使用 qemu-debootstrap （来自 qemu-user-static 包）代替 debootstrap ，但较新的 debootstrap 已经合并了 qemu-debootstrap 的功能。

## 将镜像目录制作成 docker 镜像
参考[3]。
```
sudo rm -Rf ./debian10/var/lib/apt/lists ./debian10/var/cache/apt/archives # 删除 apt 缓存，减少镜像体积
sudo tar -C ./debian10 -c . | docker import - local/debian10:base2410
```

## 制作镜像的脚本
创建 `build_debian_image.sh`：
```
#!/bin/bash
# 用法：
#   IMG_ARCH=<cpu架构> build_debian_image.sh <发行版名> <发行版版本号> <发行版版本代码> <发行版仓库地址> <docker 镜像的版本号（tag）> [要额外安装的deb包（逗号分割）]
# 举例：
#   IMG_ARCH=arm64 build_debian_image.sh debian 10 buster https://mirrors.tuna.tsinghua.edu.cn/debian/ base2410
#   # 默认 amd64
#   build_debian_image.sh ubuntu 2404 noble https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ base2410
#   # debootstrap debian 11 默认没有安装 ca-certificates，这会造成 https 的仓库无法正常工作，因此额外安装它 
#   build_debian_image.sh debian 11 bullseye https://mirrors.tuna.tsinghua.edu.cn/debian/ base2410 ca-certificates,curl
# 其中 <发行版版本代码> 必须正确，可以通过 `debian-distro-info -afy` 和 `ubuntu-distro-info -afy` 查询；
# <发行版名> 和 <发行版版本号> 和 <docker 镜像的版本号（tag）> 均可自定义，被用来命名 docker 镜像。
# 镜像目录为 ./<发行版名><发行版版本号>-<cpu架构> ，这个目录最好事先不存在或为空。

ARCH=${IMG_ARCH:-amd64} # amd64|arm64
OS_TYPE=$1 # debian | ubuntu
OS_VERSION=$2 # debian==10-13 | ubuntu==2004-2404
OS_CODE_NAME=$3 # debian==buster(10),bullseye(11),bookworm(12),trixie(13) | ubuntu==focal(2004),jammy(2204),noble(2404)
REPO_URL=$4 # e.g. debian== https://mirrors.tuna.tsinghua.edu.cn/debian/ | http://deb.debian.org/debian/ ; ubuntu== https://mirrors.tuna.tsinghua.edu.cn/ubuntu/
IMG_VERSION=$5 # docker 镜像的版本号（tag），如 base2410
PKGS=$6

MY_CHROOT=${PWD}/${OS_TYPE}${OS_VERSION}-${ARCH}
MY_CACHE=${PWD}/cache-${OS_TYPE}-${ARCH}

mkdir -p ${MY_CACHE}
sudo mkdir -p ${MY_CHROOT}
# 如果 ${MY_CHROOT} 不属于 root，会导致安装 systemd 时出错，且 systemd 无法用 --exclude 排除，因此要确保 ${MY_CHROOT} 属于 root 。
sudo chown root:root ${MY_CHROOT}

# --components debian 有 contrib,non-free，ubuntu 有 restricted,universe,multiverse ，全写上并无不良作用。
echo "running debootstrap --arch ${ARCH} --cache-dir=${MY_CACHE} --include=libc6,${PKGS} --components=main,contrib,non-free,restricted,universe,multiverse ${OS_CODE_NAME} ${MY_CHROOT} ${REPO_URL}"
sudo debootstrap --arch ${ARCH} --cache-dir=${MY_CACHE} --include=libc6,${PKGS} --components=main,contrib,non-free,restricted,universe,multiverse ${OS_CODE_NAME} ${MY_CHROOT} ${REPO_URL}
echo "done debootstrap"

echo "cleaning cache" 
sudo rm -Rf ${MY_CHROOT}/var/lib/apt/lists ${MY_CHROOT}/var/cache/apt/archives

IMG_SPEC=local/${OS_TYPE}${OS_VERSION}-${ARCH}:${IMG_VERSION}
echo "creating docker image ${IMG_SPEC}"
sudo tar -C ${MY_CHROOT} -c . | docker import - ${IMG_SPEC}

echo "done"
```

用法示例1：
```
build_debian_image.sh debian 13 trixie https://mirrors.tuna.tsinghua.edu.cn/debian/ base2410 curl
```
执行完成后，得到以下结果：
* `./debian13-amd64` 目录，包含 debian13 的最小根文件系统（加上 curl 包）。
* `./cache-debian-amd64` 目录，debian 的 .deb 缓存。
* docker 镜像 `local/debian13-amd64:base2410` ，导入了 `./debian13-amd64` 目录。

用法示例2：
```
IMG_ARCH=arm64 build_debian_image.sh debian 12 bookworm https://mirrors.tuna.tsinghua.edu.cn/debian/ base2410 ca-certificates,curl
```
得到 arm 架构的镜像。
