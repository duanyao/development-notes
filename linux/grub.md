## 参考资料
[1] GNU GRUB manual
https://www.gnu.org/software/grub/manual/

[2] BIOS installation
https://www.gnu.org/software/grub/manual/html_node/BIOS-installation.html#BIOS-installation

[3] UEFI Booting
https://help.ubuntu.com/community/UEFIBooting

[4] UEFI
https://help.ubuntu.com/community/UEFI

[5] archlinux GRUB 
https://wiki.archlinux.org/index.php/GRUB
https://wiki.archlinux.org/index.php/GRUB_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)#.E5.AE.89.E8.A3.85_2 (简体中文)

[6] Linux on UEFI: A Quick Installation Guide
http://www.rodsbooks.com/linux-uefi/

[7] GRUB2 Modules
http://blog.fpmurphy.com/2010/06/grub2-modules.html#sthash.5sCVD1tI.dpbs

[8] 带EFI支持的GRUB2安装全记录
https://www.douban.com/note/210077866/?type=like

[9] 4.2.2. Boot Loader Passwords
https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/4/html/Security_Guide/s2-wstation-bootloader.html

[10] 6.3 Multi-boot manual config
https://www.gnu.org/software/grub/manual/html_node/Multi_002dboot-manual-config.html

[11] How does grub2 find its configuration file in EFI boot?
https://unix.stackexchange.com/questions/252189/how-does-grub2-find-its-configuration-file-in-efi-boot

[12] 简单使用grub shell
https://blog.csdn.net/qq_38232378/article/details/94868309

[13] How to Rescue a Non-booting GRUB 2 on Linux
https://www.linux.com/training-tutorials/how-rescue-non-booting-grub-2-linux/

[14] Set default kernel in GRUB
https://unix.stackexchange.com/questions/198003/set-default-kernel-in-grub

[15] Cannot set EFI variable Boot0000. No space left on device
https://unix.stackexchange.com/questions/627548/cannot-set-efi-variable-boot0000-no-space-left-on-device

[21] (windows) Boot Process and BCDEdit
https://technet.microsoft.com/en-us/library/ee221031(v=ws.10).aspx

[31] UEFI+GPT环境下安装 Deepin引导分区和引导文件研究
https://www.shanyemangfu.com/deepin-boot-uefi.html

[32] Deepin修复UEFI的grub2引导不加载菜单
https://bbs.deepin.org/forum.php?mod=viewthread&tid=133379

[33] deepin 硬盘安装的方法（15.10.1 iso 可用） 
https://bbs.deepin.org/forum.php?mod=viewthread&tid=179215&extra=

[34] How to Boot ISO Files From GRUB2 Boot Loader
https://www.linuxbabe.com/desktop-linux/boot-from-iso-files-using-grub2-boot-loader

## 概述
### 可执行程序结构
一般至少包括可执行程序、配置文件（grub.cfg）、环境变量文件（grubenv，用来存储持久的可变数据）。

* X86 MBR 分区表硬盘的情形
  * MBR 和 VBR 记录
  * /boot/grub/
           grub.cfg
           grubenv
           *.img     # 可执行文件

可执行文件当中，core.img 可以位于 MBR 之后、第一个分区之前的未分配空间内，这是推荐的方式。将grub的可执行文件放在文件系统中是比较脆弱的：
如果它的物理地址被移动了，MBR/VBR 就无法加载它了[2]。有些文件系统及工具会移动文件的物理地址。

* X86 EFI + GPT 分区表硬盘的情形
在 EFI 系统分区（ESP，EFI System Partition）上：
    /EFI/
      grub/
          grubx64.efi  # 可执行文件，EFI 应用
      BOOT/
          bootx64.efi  # 默认 EFI 程序，/efi/grub/grubx64.efi 的复制品。可选。
    /grub/
       grub.cfg    # grub 配置文件
       grubenv     # grub 环境变量文件
       x86_64_efi/ # grub 的模块和可执行文件
       locale/
       fonts/      # grub 字体

制作 ESP 分区时，如果用 gdisk，分区类型应设置为 ‘0xEF00’，GUID 应为 21686148-6449-6e6f-744e656564454649。
注意，grub-install 命令是根据上述类型来选择安装位置的，所以要注意不要设置到错误的分区上。

## grub shell 和调试

grub 程序本可以作为命令行解释器交互式运行，其语法类似 shell script，称为 grub shell。grub shell 会显示一个 “grub>”提示符。
grub 程序也可以自动执行配置文件（通常命名为 grub.cfg），通常会显示一个菜单。菜单出现后，按下c即可进入 grub shell。

在 grub shell 中，用 `echo $var` 命令可以显示环境变量 var 的值，执行 set 命令（无参数）可以列出所有的环境变量。

grub 程序一般是根据 prefix 环境变量来确定 grub.cfg 的位置的，而 prefix 的初始值内嵌在 grub 程序中[11]，这可以通过16进制编辑器看到，或者用 strings 命令查看。
例如，如果 prefix 是 (hd0,1)/grub ，则认为配置文件在 (hd0,1)/grub/grub.cfg 。如果 ($prefix)/grub/grub.cfg 不存在或者出现严重错误，grub 一般不显示错误信息，而是进入 grub shell，这时应该
查看 prefix 的值，这可以用来诊断找不到 grub.cfg 而进入 grub shell 的错误[11]。

为了确认 grub 程序中内嵌的 prefix 环境变量的值，可以将 esp 分区中的所有名叫的 grub.cfg 文件移走或改名，重启电脑，选择一个 grub 程序执行，如果进入 grub shell ，就输入 `echo $prefix` 来查看。
这是因为 grub 程序可能加载了 esp 分区中的一个 grub.cfg 文件，而这个文件修改了 prefix 环境变量的值，从而扰乱了我们的观察。

其他常用操作[12]:

启用显示分页：
grub> set pager=1

列出分区：
grub> ls
(hd0) (hd0,gpt3) (hd0,gpt2) (hd0,gpt1) (hd1) (hd1,gpt2) (hd1,gpt1)

列出分区下的文件：
ls (hd0,1)/

显示文件内容：

grub> cat (hd0,1)/etc/issue

设置当前分区：
grub> set root=(hd0,gpt1)

列出当前分区下的某个目录的文件：
grub> ls /
efi/

执行另一个 efi 程序：
grub> chainloader /efi/linuxmint/grubx64.efi

加载模块：
grub> insmod video

关机和重启：
grub> halt
grub> reboot

启动 linux：

grub> linux /boot/vmlinuz-3.13.0-29-generic root=/dev/sda1
grub> initrd /boot/initrd.img-3.13.0-29-generic
grub> boot

在 grub shell 中，使用 `configfile (hd0,1)/path/to/grub.cfg` 可以执行一个配置文件。省略分区部分（(hd0,1)）时，默认为当前分区

在一个配置文件里也可以用 configfile 语句来跳转到另一个配置文件。很多发行版在 ESP 分区下的 grub.cfg 文件就只是简单地跳转到根分区下的 /boot/grub/grub.cfg 文件，后者才是真正定义启动菜单的地方。
例如：

```
search.fs_uuid a21675c9-b895-42de-bdb0-1087c63adfa7 root hd0,gpt8 
set prefix=($root)'/@root-deepin-1/boot/grub'
configfile $prefix/grub.cfg
```

如果进入了 grub rescue shell，应当先加载 normal 模块，执行 normal 命令，再加载 linux 模块，方可引导 linux 内核。

grub rescue> set root=(hd0,1)
grub rescue> insmod normal
grub rescue> normal
grub rescue> insmod linux

## grub.cfg 配置文件 grub.cfg
### grub.cfg 总体结构

通常应包含全局部分和 menuentry 部分。全局部分立即执行，menuentry 部分要等选中菜单项时才执行。
例子：

-------------------------------------------------
set timeout=10
set default=0
insmod part_gpt
insmod ext2
insmod efi_gop
insmod efi_uga
insmod font
insmod video_bochs
insmod video_cirrus

if loadfont ${prefix}/fonts/unicode.pf2
then
    insmod gfxterm
    set gfxmode=640x480
    set gfxpayload=text
    terminal_output gfxterm
fi
 
menuentry "Alpine Linux at root0" {
 fakebios
 fix_video
 search --label --set=root root0
 linux /boot/vmlinuz-virtgrsec root=LABEL=root0 modules=sd-mod,usb-storage,ext4
 initrd /boot/initramfs-virtgrsec
}
-------------------------------------------------

### 语句

grub.cfg 可以执行的语句与 grub shell 中的语句基本相同，这就像 unix shell script 与 unix shell 的关系。
与 unix shell script 类似，grub.cfg 一条语句如果出错，并不会导致整个脚本停止执行，相反，下一个语句继续执行。
这有时候会带来意想不到的效果，例如，`search.fs_uuid a21675c9-b895-42de-bdb0-1087c63adfa7 root` 如果找不到对应的文件系统，则环境变量 root 保持前值，再执行下一句。

grub 执行的语句大概有这么几类：

* 设置环境变量
设置
  set timeout=10
引用环境变量时则是 ${prefix}。
删除
  unset timeout
环境变量是大小写敏感的。

grub 中有一些内置环境变量，有些是影响 grub 的行为，如 root, timeout, default，有些是提供运行期信息，如 prefix。
也可以自定义环境变量。

详细解说：
  * prefix 自身的安装位置，如 (hd1,pgt1)/grub。
  * root 根分区。例如 hd1,pgt2。linux, initrd 等指令中的文件路径，如果没有指出根分区，则采用这个值。初始状态下，root 被设定为grub的安装分区。
  * default 默认选中的菜单项，从0开始。
  * fallback 如果 default 启动失败，则启动这一项。值域与 default 相同。
  * timeout 显示菜单项的超时。

* 环境变量文件
加载：
  load_env [-f file]
保存：
  save_env [-f file] var… # 多个变量名用空格分开。
不指定文件时，从默认位置加载，如 /boot/grub/grubenv 。默认位置取决于具体的grub安装位置。


* 加载 grub 模块
如
insmod part_gpt # gpt
insmod ext2     # ext2,3,4 文件系统
insmod fat      # fat 文件系统

模块列表见 [7]。

The current version of GRUB2 (v1.98) has 159 modules:

acpi.mod          date.mod                     gcry_sha1.mod       loopback.mod         pbkdf2.mod          terminfo.mod
affs.mod          datetime.mod                 gcry_sha256.mod     lsmmap.mod           pci.mod             test.mod
afs_be.mod        dm_nv.mod                    gcry_sha512.mod     ls.mod               play.mod            tga.mod
afs.mod           drivemap.mod                 gcry_tiger.mod      lspci.mod            png.mod             trig.mod
aout.mod          echo.mod                     gcry_twofish.mod    lvm.mod              probe.mod           true.mod
ata.mod           efiemu.mod                   gcry_whirlpool.mod  mdraid.mod           pxecmd.mod          udf.mod
ata_pthru.mod     elf.mod                      gettext.mod         memdisk.mod          pxe.mod             ufs1.mod
at_keyboard.mod   example_functional_test.mod  gfxmenu.mod         memrw.mod            raid5rec.mod        ufs2.mod
befs_be.mod       ext2.mod                     gfxterm.mod         minicmd.mod          raid6rec.mod        uhci.mod
befs.mod          extcmd.mod                   gptsync.mod         minix.mod            raid.mod            usb_keyboard.mod
biosdisk.mod      fat.mod                      gzio.mod            mmap.mod             read.mod            usb.mod
bitmap.mod        font.mod                     halt.mod            msdospart.mod        reboot.mod          usbms.mod
bitmap_scale.mod  fshelp.mod                   handler.mod         multiboot2.mod       reiserfs.mod        usbtest.mod
blocklist.mod     functional_test.mod          hashsum.mod         multiboot.mod        relocator.mod       vbeinfo.mod
boot.mod          gcry_arcfour.mod             hdparm.mod          normal.mod           scsi.mod            vbe.mod
bsd.mod           gcry_blowfish.mod            hello.mod           ntfscomp.mod         search_fs_file.mod  vbetest.mod
bufio.mod         gcry_camellia.mod            help.mod            ntfs.mod             search_fs_uuid.mod  vga.mod
cat.mod           gcry_cast5.mod               hexdump.mod         ohci.mod             search_label.mod    vga_text.mod
chain.mod         gcry_crc.mod                 hfs.mod             part_acorn.mod       search.mod          video_fb.mod
charset.mod       gcry_des.mod                 hfsplus.mod         part_amiga.mod       serial.mod          video.mod
cmp.mod           gcry_md4.mod                 iso9660.mod         part_apple.mod       setjmp.mod          videotest.mod
configfile.mod    gcry_md5.mod                 jfs.mod             part_gpt.mod         setpci.mod          xfs.mod
cpio.mod          gcry_rfc2268.mod             jpeg.mod            part_msdos.mod       sfs.mod             xnu.mod
cpuid.mod         gcry_rijndael.mod            keystatus.mod       part_sun.mod         sh.mod              xnu_uuid.mod
crc.mod           gcry_rmd160.mod              linux16.mod         parttool.mod         sleep.mod
crypto.mod        gcry_seed.mod                linux.mod           password.mod         tar.mod
datehook.mod      gcry_serpent.mod             loadenv.mod         password_pbkdf2.mod  terminal.mod


Some of the more noteworthy modules are:

    ls, cat, echo, cmp: Provide similar functionality to their Unix command couterparts.
    ext2, iso9660, reiserfs, xfs: Provide support for filesystems of the same name.
    part_sun, part_gpt, part_msdos: Provide support for various partition schemes.
    linux: Loader for Linux images
    vga, tga, vbe, png,jpeg: Provide support for graphics and background images.

The commands to load (insmod) or unload (rmmod) a module into GRUB2 have the same names as in GNU/Linux.

When a module is loaded, or a linked-in module is initialized, the module registers one or more commands, and can also register variables, parsers, and drivers. A GRUB2 command such as hexdump requires a mapping from a name to a specific module function The file /boot/grub/command.lst contains the mappings from the command name to the module that contains the function (and code to implement that command). Here is part of the command.lst file:

cat: minicmd
chainloader: chain
clear: minicmd
cmp: cmp
.: configfile
configfile: configfile
*cpuid: cpuid
crc: crc
date: date
*drivemap: drivemap
dump: minicmd
*echo: echo
hexdump: hexdump


loadenv 提供 load_env 和 save_env 命令 

目前我们在 grub efi 镜像中包含的模块：

loadenv part_gpt part_msdos loopback btrfs fat ext2 iso9660 ntfs ntfscomp scsi normal configfile chain multiboot search search_label search_fs_uuid search_fs_file boot efi_gop efi_uga linux echo cpio cat cpio hexdump ls date reboot minicmd video_bochs video_cirrus gfxterm font


* OS 内核加载指令
如
```
linux /boot/vmlinuz-virtgrsec root=LABEL=root0 modules=sd-mod,usb-storage,ext4
initrd /boot/initramfs-virtgrsec
```
linux 指令后面跟着 linux 内核的路径，再后面的参数照原样传给 linux 内核。
initrd 指令后面跟着 initramfs 的路径。必须在 linux 指令之后。

* 设置根分区
如
```
set root=(hd0,1)
root (hd0,1)
```
第一个硬盘的第一个分区，注意硬盘从0开始计数，分区从1开始计数，这与linux内核的设备命名不同。
不论是IDE还是SATA接口，硬盘名都是hdN；但如果是GPT分区表，也可以写作 "gptN"，例如：
```
set root=(hd0,gpt1)
```

* 分区查找
如
```
search --label --set=root root0
search --fs-uuid --set=root xxxxxx-xxxxx-xxxxxxx
search --file --no-floppy --set=root /vmlinuz
```
--label 是指查找方法是卷标（root0）。
--fs-uuid 是指查找方法是文件系统的 uuid。
--file 是指查找方法是存在特定文件（/vmlinuz）。
--set=root 是指将查找到的结果写入环境变量 root。默认是输出到控制台。
--no-floppy 是指不要查找软盘。

search 的输出格式是 "盘号,分区号"，例如 "hd1,gpt1"。

* 导入其它配置文件 configfile 
格式：configfile file 
引入另一个配置文件。如果配置文件中有菜单项，则显示出来。

* 终端模式
terminal_output <type>
不带参数的 terminal_output 列出可用的终端类型。

终端类型有：图形模式（gfxterm），字符模式（console），serial 等。

字符模式：
```
terminal_output console
```

图形模式：
```
insmod efi_gop
insmod efi_uga
insmod font
insmod video_bochs
insmod video_cirrus

if loadfont ${prefix}/fonts/unicode.pf2
then
    insmod gfxterm
    set gfxmode=640x480
    set gfxpayload=text
    terminal_output gfxterm
fi
```
* 执行引导
boot
或者在 grub 界面按 ctl+x 或者 f10。  
menuentry 中并不需要显式的加入这个指令，用户选中菜单项后，最终会执行这个指令。

* chainloader
chainloader 可以加载 MBR/VBR （分区引导记录），也可以加载一个 EFI 程序。模块：chain。

chainloader (hd0,2)/efi/Microsoft/Boot/bootmgfw.efi # 加载 windows efi 程序
chainloader (hd0,2) +1 # 加载 hd0,2 的 VBR
chainloader +1  # 加载 VBR，应该已经设定了 root 变量

加载 VBR 时，"+1"的意思是位于第2个扇区。

* 单用户模式或文本模式

single
  单用户模式，一般要与 init=/bin/sh 或者 init=/bin/bash 联用；如果有 splash 参数也得去掉，否则出错（/bin/sh: 0: Can't open splash）；ro 参数如果有也去掉。
overlayroot=disabled
  停用 overlayroot 只读根分区功能[54]。
3 或 2
  进入文本界面模式，3、2是运行级别。一般不用写 init=...

文件系统相关的参数见 filesystem_mount.txt。

### 加载 Windows
有两种方式：chainloader（又分为 EFI 和 VBR） 和 ntldr。

ntldr 的方式[10]：

insmod part_msdos
insmod ntfs
insmod ntldr
ntldr (hd0,2)/bootmgr

bootmgr 是 Windows 安装分区上的文件，是引导管理器。注意只有 MBR 模式的 grub 有 ntldr 模块，EFI 模式的是没有的。

使用 chainloader 和 windows EFI 时，bootmgfw.efi 应当位于 ESP 分区的 /efi/Microsoft/Boot/ 目录。
其实这个文件也存放在 windows 的安装分区下的 WINDOWS\BOOT\EFI\bootmgfw.efi，以及 window 恢复分区的/Recovery/WindowsRE/Winre.wim 镜像里面。同目录下还存在[21]：

bootmgr.efi

bootmgfw.efi

memtest.efi

BCD

BCD.Backup.001

以及语言文件，如 en_US 等。
如果是非英文系统，可能还需要 Fonts 目录下的字体文件，例如 Fonts/chs_boot.ttf

其中 BCD 文件也是必需的，如有必要可以备份。BCD 文件可以用 BCDEdit.exe 编辑。

* 加载磁盘/光盘镜像文件
例如 loopback loop (hd0,2)/iso/my.iso

“loop”是任意指定的名字。然后可以这样引用镜像上的文件：
(loop)/live/vmlinuz

模块：loopback

* 设定分页
只在交互模式有用，在显示较长的输出时分页。
set pager=1

* 类似 unix 环境的命令
显示字符串 echo string
列出目录 ls (hd1,gpt1)

### 分区的命名规则和路径格式
对于硬盘：
  hdN,M
N和M都是整数，分别是盘编号和分区编号。分区编号从1开始，盘编号则不确定，可能是0或1开始。
对于 GPT 分区，还可以是
  hdN,gptM
这与前面形式是等效的。

grub 的绝对路径需要有分区部分，例如
  ls (hd1,gpt1)/boot/
注意分区部分要用圆括号包围。
但是，如果不是作为路径的一部分，则分区不要有圆括号，例如 root 环境变量直接写成 hd1,gpt1 就好。
search 命令返回的分区名也是不带圆括号的。

### 引导 live cd
例子：
```
menuentry 'deepin install' {
	search --label --set=root ROOT
	linux /vmlinuz boot=live union=overlay username=user quiet live-config  findiso=/deepin-15.4.iso  locales=zh_CN.UTF-8
	initrd /initrd.lz
}

menuentry 'deepin install 2' {
	search --label --set=root ROOT
	set isofile="/deepin-15.4.iso"
	loopback loop $isofile
	linux (loop)/live/vmlinuz.efi  boot=live union=overlay username=user quiet  live-config noprompt noeject findiso=$isofile locales=zh_CN.UTF-8
	initrd (loop)/live/initrd.lz
}

menuentry 'deepin install' {
    set isofile="/deepin-15.10.1-amd64.iso"
    #search --label --set=root DATA
    search --file $isofile --set=root
    loopback loop $isofile
    linux (loop)/live/vmlinuz.efi  boot=live union=overlay username=user quiet  live-config noprompt noeject findiso=$isofile locales=zh_CN.UTF-8
    initrd (loop)/live/initrd.lz
}
```
前者需要把linux内核（vmlinuz）与 initrd（initrd.lz）解压到硬盘上，后者则不需要，利用 loopback 设备。

根据 iso 文件的名字搜索分区，以及 Failsafe 模式。

```
menuentry "Deepin Linux ISO (live)" --class deepin --class gnu-linux --class gnu --class os{
	echo 'booting...'
	search --file /deepin.iso --set=root
	set isofile=/deepin.iso
	loopback loop $isofile
	
	if [ 'pc' == $grub_platform ] ; then
		linux (loop)/live/vmlinuz findiso=$isofile boot=live components union=overlay locales=zh_CN.UTF-8
		initrd (loop)/live/initrd.lz
	else
		linux (loop)/live/vmlinuz findiso=$isofile boot=live components union=overlay locales=zh_CN.UTF-8
		initrd (loop)/live/initrd.lz
	fi
}

menuentry "Deepin Linux ISO (Failsafe)" --class deepin --class gnu-linux --class gnu --class os{
	echo 'booting...'
	search --file /deepin.iso --set=root
	set isofile=/deepin.iso
	loopback loop $isofile
	
	if [ 'pc' == $grub_platform ] ; then
		linux (loop)/live/vmlinuz  findiso=$isofile boot=live components memtest noapic noapm nodma nomce nolapic nomodeset nosmp nosplash vga=normal union=overlay locales=zh_CN.UTF-8
		initrd (loop)/live/initrd.lz
	else
	    linux (loop)/live/vmlinuz.efi findiso=$isofile boot=live components memtest noapic noapm nodma nomce nolapic nomodeset nosmp nosplash vga=normal union=overlay locales=zh_CN.UTF-8
	    initrd (loop)/live/initrd.lz
	fi
}
```

ubuntu
```
menuentry "ubuntu ISO (live)" {
	echo 'booting...'
	set isofile=/ubuntu.iso
	search --file $isofile --set=root
	loopback loop $isofile
	
	linux (loop)/casper/vmlinuz.efi boot=casper iso-scan/filename=$isofile ro quiet splash
  initrd (loop)/casper/initrd.lz
}
```

### 引导 btrfs 上的系统

```
menuentry 'deepin SYSB' {
  search --label --set=root SYSB
  linux /@root-deepin-1/vmlinuz root=LABEL=SYSB rootflags=subvol=@root-deepin-1
  initrd /@root-deepin-1/initrd.img
}
```

rootflags=subvol=... 用来指定作为 linux 根分区的 subvolumn。linux 和 initrd 指令后的参数要相对于 btrfs 的根卷。

## grub 用户界面/交互

### 隐藏/反隐藏启动菜单
如果启动菜单是隐藏的，在启动时按esc键，可显示启动菜单。

## 用户态工具
### grub-mkconfig 用来生成 grub.cfg
sudo grub-mkconfig 

### grub-editenv 用来修改环境变量文件。
grub-editenv [OPTION...] FILENAME COMMAND 
set [NAME=VALUE ...]       Set variables.
unset [NAME ...]           Delete variables.
list                       List the current variables.

注意 grub-editenv set 和 create 采取新建-重命名的模式操作，保证崩溃不会造成文件破坏。
list 会显示所有的变量，没有办法只显示一个变量。

grub-editenv 依赖 device-mapper-libs。

### grub-install

安装 boot loader 可执行文件。grub-install 实际上分别执行了 grub-mkimage 和 grub-setup。
参考[5]。

对于 MBR/VBR 模式：
  sudo grub-install --boot-directory=/mnt/boot /dev/fd0

--boot-directory 表示grub可执行文件的安装位置，参数 /dev/fd0 表示 MBR/VBR 所在的设备。
如果省略 --boot-directory，则默认是 /boot。

对于 EFI，需要先挂载 ESP 分区。不指定挂载目录时，grub-install 假定其挂载到 /boot/efi (很多linux发行版默认如此)。执行：

```
sudo LC_ALL=C grub-install --debug
```

如果 grub-install 报错 `grub-install: error: cannot find EFI directory.` 则表示它找不到 ESP 分区的挂载位置。
这可能是因为 ESP 分区没有挂载，或者挂载到了非标准位置，后者可用 --efi-directory 指定。

grub-install 更新哪些文件是因发行版而不同的，例如 deepin 更新的是 /boot/efi/EFI/deepin/ 目录。
此外，grub-install 可能还将它的可选模块都安装到 /boot/grub/x86_64-efi/ 目录下。

也可以指定目录结构：

  sudo grub-install --target=x86_64-efi --efi-directory=/esp --bootloader-id=grub --boot-directory=/esp --debug
  
假定ESP分区挂载在/esp。--efi-directory 表示ESP分区的挂载点。--bootloader-id 表示 grub 在 EFI 中的名字。
不指定 --boot-directory 时，GRUB 自身的主目录将位于 /boot/grub/，指定 /esp 后则是 /esp/grub/。

有的发行版的 grub-install 还会修改 BIOS 中的 EFI 启动项，添加本系统的条目。grub-install --debug 的输出会显示它修改了什么。
例如 deepin 20.2.2 :
```
grub-install：信息： Registering with EFI: distributor = `deepin', path = `\EFI\deepin\shimx64.efi', ESP at hostdisk//dev/sda,gpt1.
```

要分析 grub-install 做了什么，为什么出错，可以用 --debug 参数，并加上 LC_ALL=C 变成英文输出：

```
sudo LC_ALL=C grub-install --debug
```

grub-install 修改 BIOS 中的 EFI 启动项可能出错：

```
grub-install: info: setting EFI variable Boot0002.
grub-install: warning: Cannot set EFI variable Boot0002.
grub-install: warning: efivarfs_set_variable: writing to fd 6 failed: No space left on device.
grub-install: warning: _efi_set_variable_mode: ops->set_variable() failed: No space left on device.
grub-install: error: failed to register the EFI boot entry: No space left on device.
```

看看有没有 /sys/firmware/efi/efivars/dump-* 文件，有的话删掉，重启系统再试。参考[15]。

### grub-mkimage

grub-mkimage 用来制作 grub 可执行文件。这是 grub-install 要执行的一步。
在交叉编译的情况下，不能执行 grub-install，就需要单独执行 grub-mkimage。

对于 EFI [8,3]，例一：
```
grub-mkimage -d /usr/lib/grub/x86_64-efi/ -O x86_64-efi -p /grub -o /media/esp/EFI/boot/bootx64.efi loadenv part_gpt part_msdos loopback btrfs fat ext2 ntfs ntfscomp scsi normal configfile chain multiboot search search_label search_fs_uuid search_fs_file boot efi_gop efi_uga linux echo cpio cat cpio hexdump ls date reboot minicmd video_bochs video_cirrus gfxterm font
```
例二：
```
grub-mkimage --directory '/usr/lib/grub/x86_64-efi' --prefix '' --output '/boot/grub/x86_64-efi/grub.efi'  --dtb '' --sbat '' --format 'x86_64-efi' --compression 'auto'  --config '/boot/grub/x86_64-efi/load.cfg' 'btrfs' 'part_gpt' 'search_fs_uuid'
```
-p 是 prefix，是 grub 运行时主目录，存放配置文件、模块、locale 的目录，位于 ESP 上。
-o 是输出的映像文件。
-d 是输入模块的存放位置。
-c --config 是内嵌配置文件，起到 grub.cfg 的作用。

其它参数都是要放到映像里的模块。

### grub-mkconfig
见 update-grub

### update-grub

update-grub 命令扫描本机硬盘，将一个或多个操作系统添加到 grub.cfg 文件中。

实际等效于：
grub-mkconfig -o /boot/grub/grub.cfg

其配置文件为 /etc/default/grub 和 /etc/grub.d/xxx （例如 /etc/grub.d/40_custom），在这里可以设置默认启动项等。

由于自动生成的 grub 菜单比较复杂，还存在二级菜单，设置默认启动项也比较复杂。建议设置

```
GRUB_SAVEDEFAULT=true
GRUB_DEFAULT=saved
```
让 grub 记住上次的选择。

## 设置 EFI 启动项

在 EFI 机器上，grub-install 只会将相关文件安装到硬盘上，但不一定会修改 BIOS 中的 EFI 启动项。如果 BIOS 本身没有提供修改 EFI 启动项的功能，则可以使用 efibootmgr 程序来实现。

查看已有的启动项及其内容：
```
$ efibootmgr -v

BootCurrent: 0002
Timeout: 5 seconds
BootOrder: 0001,3001,0000,0002,2001,2002,2004
Boot0000* deepin	HD(1,GPT,98dbaa74-7d2a-4cbb-918f-f276fa6220d4,0x800,0x82000)/File(\EFI\deepin\shimx64.efi)
Boot0001* Windows Boot Manager	HD(1,GPT,98dbaa74-7d2a-4cbb-918f-f276fa6220d4,0x800,0x82000)/File(\EFI\Microsoft\Boot\bootmgfw.efi)WINDOWS.........x...B.C.D.O.B.J.E.C.T.=.{.9.d.e.a.8.6.2.c.-.5.c.d.d.-.4.e.7.0.-.a.c.c.1.-.f.3.2.b.3.4.4.d.4.7.9.5.}...e................
Boot0002* ubuntu	HD(1,GPT,98dbaa74-7d2a-4cbb-918f-f276fa6220d4,0x800,0x82000)/File(\EFI\ubuntu\grubx64.efi)RC
Boot2001* EFI USB Device	RC
Boot3001* Internal Hard Disk or Solid State Disk	RC
```

其中 deepin、 Windows Boot Manager、ubuntu 都是显示的启动项的名字。HD(1,GPT,98dbaa74-7d2a-4cbb-918f-f276fa6220d4,0x800,0x82000) 是指 ESP 分区，`\EFI\deepin\shimx64.efi` 是 EFI 程序的路径。

要修改，参考 [3]：

```
sudo modprobe efivars

cd <grub2_compiled_source_dir>/grub-core
../grub-probe --target=device /boot/efi/efi/grub/grub.efi

# Assuming the output the grub-probe to be /dev/sda1
sudo efibootmgr --create --gpt --disk /dev/sda --part 1 --write-signature --label "GRUB2" --loader "\\EFI\\grub\\grub.efi"

另一个办法是将 grub 的可执行程序复制为 EFI 的默认可执行程序（假定 ESP 分区挂载于 esp/）：
# mkdir esp/EFI/boot
# cp esp/EFI/grub/grubx64.efi esp/EFI/boot/bootx64.efi
```

## 环境变量

Grub 也有环境变量（Environment Variables）的概念，它是持久的，Grub bootloader 自身（load_env/save_env）或用户空间程序（grub-editenv）都可以修改它。
它存储在普通文件里（通常是 /boot/grub/grubenv），但限制是不能在特殊的分区上，例如LVM，RAID，ZFS，USB等。

Grub 有个“回退（fallback）”机制，可以在一个启动条目失败的情况下去启动另一个。“失败”的定义是（1）grub不能启动内核（2）内核发生panic。

## linux grub secure boot
ubuntu 是这样实现 secure boot 的：

ESP 分区下 EFI\ubuntu\ 目录下有 shimx64.efi 和 grubx64.efi 两个文件，启动时 BIOS 引导 shimx64.efi，shimx64.efi 再启动 grubx64.efi 。
grubx64.efi 是 efi 版 grub ，而 shimx64.efi 起个过渡作用。shimx64.efi 有微软的数字签名，所以可以被几乎全部 secure boot 主板接受。


## 常见问题
### 过小的 fat32 ESP 分区
ESP 分区可以采用 FAT16 或 FAT32 格式。应注意，FAT32 分区的最小尺寸是 32MiB（mkfs.vfat 可以创建小于这个尺寸的分区，
linux也能挂载，但fsck.vfat 会给出警告，EFI也不一定能识别）。

### grub Error: no suitable video mode found. Booting in blind mode 或者黑屏
可能的原因有多种[3]：
* EFI 固件的 bug
  有时候给内核加上 nomodeset 可以解决。参考[6] "Fixing blank displays"。
  其它可能尝试的内核参数：noefi  video=efifb
  尝试 grub 指令 fakebios，fix_video
* 控制台模式设置不当
  参考[5]
  尝试过 terminal_output console 和 terminal_output gfxterm
* 缺少字体文件
  参考[5]
* 内核不支持 EFI
  这个在物理机的内核上不常见。但 alpine linux 的虚拟机内核上似乎是没有 EFI 支持的，不过还不能确定是否是这个原因。

## 使用 /etc/grub.d/40_custom 和 update-grub

可以在 /etc/grub.d/40_custom 增加自定义 grub 条目，然后运行 update-grub 来更新 /boot/grub/grub.cfg 。

注意 40_custom 是个可执行文件，如果不小心去掉可执行标记，则 update-grub 会安静地忽略它。

## 安装到引导记录：grub-install
GRUB2
  http://linux-wiki.cn/wiki/Grub2%E9%85%8D%E7%BD%AE
  
   grub-install 

   1 安装grub到设备中，如果需要会创建/boot/grub目录

   2 他会把*.mod、*.lst、*.img从 /usr/lib/grub/i386-pc/ 目录复制到/boot/grub目录下，他会覆盖已有文件

   3 然后会调用grub_probe扫描计算机并收集磁盘和分区信息

   4 接着调用grub_mkimage构建一个新的new.img

  5 最后调用grub_setup把grub的boot.img写入MBR中，把core.img写进设备的第一个扇区。

   注意grub_install并不运行grub_mkconfig
	
grub-install -v

     显示版本号

?
1
	
grub-install /dev/sda
  
  update-grub

     update-grub主要是在每次配置文件有更新后生成新的grub.cfg，其实update-grub是调用grub-mkconfig，在系统中还有一个update-grub2，发现他是调用update-grub

     grub-mkconfig

     调用grub-mkdevicemap和grub-probe生成grub.cfg

How to chroot Ubuntu using Live CD to fix GRUB rescue prompt
  http://karuppuswamy.com/wordpress/2010/06/02/how-to-chroot-to-ubuntu-using-live-cd-to-fix-grub-rescue-prompt/

    $ sudo mount /dev/sdax /mnt/myroot (where sdax is your root partition)

Let’s say you have mounted root partition at /media/xx..xx.

Step-4: $ Run the terminal through Applications -> Accessories -> Terminal.

Step-5: Run the following commands to export the pesudo file system of Live CD to your would be root file system soon.

    $ sudo mount --bind /dev /media/xx..xx/dev

    $ sudo mount --bind /proc /media/xx..xx/proc

    $ sudo mount --bind /sys /media/xx..xx/sys

Step-6: Changing the root file system of live system to your hard disk installed root file system.

    $ sudo chroot /media/xx..xx

Step-7: Installing GRUB Boot record in Master Boot record of your hard disk. My hard disk is sda. Replace your hard disk device node in the following command.

    $ sudo grub-install /dev/sda
    

## 安全
为了防止用户在启动时进入单用户模式，或者修改启动参数，就需要启动 grub 密码[9]。

## 包
grub-efi
  grub 的 efi 版本。如果安装时磁盘是 MBR 的，则可能不会自动安装。
grub-common
grub2-common
grub-pc
grub-pc-bin
grub-themes-deepin

## grub 引导 windows 时花屏（screen distort, mess up）的问题

## deepin linux lion 的 EFI grub 的特殊问题

deepin linux lion (15.9.2+ 全新安装）安装在 EFI 机器的硬盘时，ESP 分区上的结构是[31, 32]：

```
/EFI/
  Boot/
    bootx64.efi
    grubx64.efi
    grub.cfg
  deepin/
    shimx64.efi
    grubx64.efi
    grub.cfg
  ubuntu/
    grubx64.efi
    grub.cfg
```

其中，3个 grub.cfg 相同，3 个 grubx64.efi 相同，而 bootx64.efi 和 shimx64.efi 相同。

deepin linux 安装器还在 BIOS 里创建了 deepin 的 EFI 启动项，指向 /EFI/deepin/shimx64.efi 。

/EFI/deepin/shimx64.efi 执行后，又执行了同目录下的 /EFI/deepin/grubx64.efi ，而后者则加载ESP 分区上的 /EFI/ubuntu/grub.cfg 。
`/EFI/ubuntu/` 是作为 prefix 环境变量写死在 grubx64.efi 中的，可以通过 strings 命令查看，或16进制编辑器查看。

这就意味着，不论执行其中哪个 .efi 文件，3个 grub.cfg 中只有 /EFI/ubuntu/grub.cfg 起作用。

这样做的原因可能是，deepin 直接使用了 ubuntu 的 efi 文件，因为它的 efi 文件有数字签名，可以被启用了 secure boot 功能的主板接受。
但因为 efi 文件里写死了 grub.cfg 的路径，这样只好将 grub.cfg 放到 /EFI/ubuntu/ 目录下。

然而，deepin 的 grub-install 程序并没有相应修改，所以它只会更新 /EFI/deepin/ 下的 3 个文件 ，不会碰 /EFI/ubuntu/grub.cfg 。

这样，如果你的系统根分区变更了，grub-install 并不会起作用。如果你不小心删除了 /EFI/ubuntu/grub.cfg ，那么系统也会无法启动，而且 grub-install 也无法修复。
这两种情况下，你都需要自行将 /EFI/deepin/grub.cfg 复制到 /EFI/ubuntu/ 下面。

## deepin linux panda 的 EFI grub (从 15.9.1- 全新安装，逐步升级)

deepin linux panda (从 15.9.1- 全新安装，逐步升级) 安装在 EFI 机器的硬盘时，ESP 分区上的结构是：

```
/EFI/
  Boot/
    bootx64.efi
  grub/
    grubx64.efi
```

这两个 efi 文件是相同的。它们写死了 prefix 环境变量为 `(,gpt3)/boot/grub`，其中 (,gpt3) 是 deepin linux panda 的根分区（/boot没有独立分区）。
这样，grub 程序直接就加载了根分区的 `(,gpt3)/boot/grub/grub.cfg` 配置文件。当然，这种 grub 无法独立引导其它分区的系统了。

## deepin 20.2.1 - 20.2.2 的 EFI grub 的特殊问题

deepin 20.2.2 的 EFI 引导过程大致是这样的：BIOS 加载 ESP 分区上的 /EFI/deepin/shimx64.efi 文件，/EFI/deepin/shimx64.efi 再加载 /EFI/deepin/grub.efi ， /EFI/deepin/grub.efi 再执行 /EFI/UOS/grub.cfg 。（至于为啥是 UOS ，可能是需要用它的数字签名的 grub.efi 。更早的 deepin 版本还用过 /EFI/ubuntu/grub.cfg 。）

而 deepin 20.2.2 的 grub-install 工具只在 ESP 分区的 /EFI/deepin/ 目录安装了以下文件：

  BOOTX64.CSV  fbx64.efi  grub.cfg  grubx64.efi  mmx64.efi  shimx64.efi

可见，缺失了 /EFI/deepin/grub.efi 和 /EFI/UOS/grub.cfg 这两个文件，这造成了 deepin 系统无法启动。

修正的方法是，将 ESP 分区上的 /EFI/deepin/grubx64.efi 复制为 /EFI/deepin/grub.efi，将 /EFI/deepin/grub.cfg 复制为 /EFI/UOS/grub.cfg 。

grub-install 属于 grub2-common 包，版本是 2.04.1-17 ，apricot 仓库。

## deepin linux 的安装光盘的 EFI grub

安装光盘的 EFI grub （ /EFI/BOOT/grubx64.efi ）的 prefix 是 `/boot/grub/` ，即它会寻找与 grubx64.efi 同分区的 /boot/grub/grub.cfg 文件。
另外它自身带的模块不够多，需要从同分区的 /boot/grub/x86_64-efi 目录加载模块。

因此这个 grubx64.efi 可以作为万能 grub 程序，放到硬盘 ESP 分区或 U 盘上，来引导任意系统。

## grub 引导硬盘上的 iso 文件（硬盘安装）

参考[33-34]。

```
insmod part_gpt
# 加载 iso 文件所在分区的文件系统模块。ext2 支持 ext2,3,4。
insmod ext2 
insmod btrfs
insmod ntfs

# 搜索和挂载 iso 文件。loop1 可以随便取名。
set isofile="/deepin-desktop-community-23-Beta-amd64.iso"
search --file --set=root $isofile
loopback loop1 $isofile

# linux 内核和 initdrd 文件的路径（/live/vmlinuz-6.1，/live/initrd-6.1.lz）和启动参数，大多可以从 iso 文件里的  /boot/grub/grub.cfg （或者 loopback.cfg, kernels.cfg 等）获得。
# 但其中负责指定 iso 文件的参数，如  `findiso=$isofile` ，是因发行版和版本而不同的
linux	(loop1)/live/vmlinuz-6.1 findiso=$isofile boot=live union=overlay livecd-installer locales=zh_CN.UTF-8 console=tty splash --
initrd	(loop1)/live/initrd-6.1.lz
```
根据[34]，各个发行版的 linux 内核指令如下：
ubuntu2004:
```
linux (loop)/casper/vmlinuz boot=casper iso-scan/filename=$isofile quiet noeject noprompt splash
```
debian10:
```
linux (loop)/live/vmlinuz-4.19.0-14-amd64 boot=live findiso=$isofile
```
archlinux 2021.03.01
```
linux (loop)/arch/boot/x86_64/vmlinuz-linux archisolabel=ARCH_202103 img_dev=/dev/sda5 img_loop=$isofile earlymodules=loop
```
rhel8:
```
linux (loop)/isolinux/vmlinuz noeject inst.stage2=hd:/dev/sda5:$isofile
```
fedora33, openSUSE 15.2:
```
linux (loop)/isolinux/vmlinuz root=live:CDLABEL=Fedora-WS-Live-33-1-2 rd.live.image verbose iso-scan/filename=$isofile
```

根据笔者的研究：
ubuntu2304:
```
linux	(loop)/casper/vmlinuz layerfs-path=minimal.standard.live.squashfs --- iso-scan/filename=${isofile} quiet splash
```
