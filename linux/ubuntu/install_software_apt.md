
## apt 源镜像

阿里云的：
```
deb http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse
```

阿里云的镜像并不稳定，经常无法连接。

https://mirrors.tuna.tsinghua.edu.cn/help/ubuntu/

```
# 默认注释了源码镜像以提高 apt update 速度，如有需要可自行取消注释
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-updates main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-updates main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-backports main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-backports main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-security main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-security main restricted universe multiverse

# 预发布软件源，不建议启用
# deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-proposed main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-proposed main restricted universe multiverse
```

网易开源镜像站

http://mirrors.163.com/.help/ubuntu.html

```
deb http://mirrors.163.com/ubuntu/ focal main restricted universe multiverse
deb http://mirrors.163.com/ubuntu/ focal-security main restricted universe multiverse
deb http://mirrors.163.com/ubuntu/ focal-updates main restricted universe multiverse
deb http://mirrors.163.com/ubuntu/ focal-proposed main restricted universe multiverse
deb http://mirrors.163.com/ubuntu/ focal-backports main restricted universe multiverse
deb-src http://mirrors.163.com/ubuntu/ focal main restricted universe multiverse
deb-src http://mirrors.163.com/ubuntu/ focal-security main restricted universe multiverse
deb-src http://mirrors.163.com/ubuntu/ focal-updates main restricted universe multiverse
deb-src http://mirrors.163.com/ubuntu/ focal-proposed main restricted universe multiverse
deb-src http://mirrors.163.com/ubuntu/ focal-backports main restricted universe multiverse
```

中科大开源镜像站
https://mirrors.ustc.edu.cn/help/ubuntu.html

```
# 默认注释了源码仓库，如有需要可自行取消注释
deb https://mirrors.ustc.edu.cn/ubuntu/ focal main restricted universe multiverse
# deb-src https://mirrors.ustc.edu.cn/ubuntu/ focal main restricted universe multiverse

deb https://mirrors.ustc.edu.cn/ubuntu/ focal-security main restricted universe multiverse
# deb-src https://mirrors.ustc.edu.cn/ubuntu/ focal-security main restricted universe multiverse

deb https://mirrors.ustc.edu.cn/ubuntu/ focal-updates main restricted universe multiverse
# deb-src https://mirrors.ustc.edu.cn/ubuntu/ focal-updates main restricted universe multiverse

deb https://mirrors.ustc.edu.cn/ubuntu/ focal-backports main restricted universe multiverse
# deb-src https://mirrors.ustc.edu.cn/ubuntu/ focal-backports main restricted universe multiverse

# 预发布软件源，不建议启用
# deb https://mirrors.ustc.edu.cn/ubuntu/ focal-proposed main restricted universe multiverse
# deb-src https://mirrors.ustc.edu.cn/ubuntu/ focal-proposed main restricted universe multiverse
```

## 新内核
要安装比仓库里更新的内核，可以在这里找：
https://kernel.ubuntu.com/~kernel-ppa/mainline/

一般需要下载 `linux-image*.deb`， `linux-headers*`，例如：

```
linux-headers-6.4.12-060412-generic_6.4.12-060412.202308231633_amd64.deb 
linux-headers-6.4.12-060412_6.4.12-060412.202308231633_all.deb
linux-image-unsigned-6.4.12-060412-generic_6.4.12-060412.202308231633_amd64.deb
linux-modules-6.4.12-060412-generic_6.4.12-060412.202308231633_amd64.deb
```

注意 linux-headers 包可能有两个，其中一个多出 `generic` 字样，两者都需要安装。

## 发行版升级
https://sypalo.com/how-to-upgrade-ubuntu#:~:text=How%20to%20upgrade%20Ubuntu%20to%2023.10%20and%20kernel,packages%20list%20sudo%20apt-get%20update%20Upgrade%20packages%20

Change default distro from your current

    20.04 - focal
    20.10 - groovy
    21.04 - hirsute
    21.10 - impish
    22.04 - jammy
    22.10 - kinetic
    23.04 - lunar
    23.10 - mantic (development branch)

in the example below, we are upgrading from Ubuntu 22.04 (jammy) to 23.10 (mantic)
```
sudo cp /etc/apt/sources.list /etc/apt/sources.list.bk-2004
# sudo sed -i 's/jammy/mantic/g' /etc/apt/sources.list
sudo sed -i 's/focal/jammy/g' /etc/apt/sources.list
Update packages list
sudo apt-get update
Upgrade packages
# sudo apt-get upgrade
Run full upgrade
sudo apt-get dist-upgrade
```

## 22.04 上加入 24.04 的源

```
sudo su

echo "deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu noble main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu noble-updates main restricted universe multiverse
" > /etc/apt/sources.list.d/ub2404.list

echo "Package: *
Pin: release noble
Pin-Priority: 100
" > /etc/apt/preferences.d/ub2404.pref
```

