## 参考资料

[1.1] How can I compile, install and run the tools inside kernel/tools?
https://unix.stackexchange.com/questions/92957/how-can-i-compile-install-and-run-the-tools-inside-kernel-tools

[1.2] Linux Perf 性能分析工具及火焰图浅析
https://zhuanlan.zhihu.com/p/54276509

[1.3] perf: Linux profiling with performance counters
https://perf.wiki.kernel.org/index.php/Main_Page

[1.4] Linux kernel profiling with perf
https://perf.wiki.kernel.org/index.php/Tutorial

[1.5] How to monitor the full range of CPU performance events
http://www.bnikolic.co.uk/blog/hpc-prof-events.html

[1.6] Some troubles with perf and measuring flops 
https://linux-perf-users.vger.kernel.narkive.com/s7GIb114/some-troubles-with-perf-and-measuring-flops

[1.7] 
https://www.man7.org/linux/man-pages/man1/perf-stat.1.html

[1.8] perf Examples
https://www.brendangregg.com/perf.html

[2.1] How to easily measure Floating Point Operations Per Second (FLOPS)
http://www.bnikolic.co.uk/blog/hpc-howto-measure-flops.html

[3.1] Performance Application Programming Interface
http://icl.cs.utk.edu/papi/index.html

[3.2] Counting FLOPS and other CPU counters in Python
http://www.bnikolic.co.uk/blog/python/flops/2019/09/27/python-counting-events.html

[3.3] PyPAPI is a Python binding for the PAPI (Performance Application Programming Interface) library.
https://pypi.org/project/python_papi/

[3.4] Counting Floating Point Operations on Intel Sandy Bridge and Ivy Bridge
https://bitbucket.org/icl/papi/wiki/PAPI-Flops.md

[3.5] Downloading and Installing PAPI
https://bitbucket.org/icl/papi/wiki/Downloading-and-Installing-PAPI.md

[4.1] vtune-profiler
https://www.intel.com/content/www/us/en/developer/tools/oneapi/vtune-profiler.html#gs.3o97v1

[4.2] FLOPS for my application using intel vtune Amplifier
https://stackoverflow.com/questions/30337662/flop-measurement

[5.1] hardware events that can be monitored for the CPU
https://perfmon-events.intel.com/
https://perfmon-events.intel.com/skylake_server.html

## 概述

Intel CPU 有多个“perf counters”，可以统计各种指令的执行次数，这可以用来研究程序或CPU的性能。相关硬件被称为PMU (Performance Monitoring Units)。

不同型号的Intel CPU支持的perf counters种类不同，可以在 [5.1] 查看。

例如 Xeon Skylake 支持以下浮点运算事件：
FP_ARITH_INST_RETIRED.128B_PACKED_DOUBLE
FP_ARITH_INST_RETIRED.128B_PACKED_SINGLE
FP_ARITH_INST_RETIRED.256B_PACKED_DOUBLE
FP_ARITH_INST_RETIRED.256B_PACKED_SINGLE
FP_ARITH_INST_RETIRED.512B_PACKED_DOUBLE
FP_ARITH_INST_RETIRED.512B_PACKED_SINGLE
FP_ARITH_INST_RETIRED.SCALAR_DOUBLE
FP_ARITH_INST_RETIRED.SCALAR_SINGLE

## 安装 perf (apt, debian, ubuntu)
以 ubuntu 20.04 为例：
```
sudo apt install linux-tools-common
sudo apt install linux-tools-5.13.0-40-generic
```
linux-tools-common 提供 /usr/bin/perf 命令，具体实现在 linux-tools-5.13.0-40-generic 。
linux-tools-5.13.0-40-generic 的版本部分要根据系统的内核版本号来定。

## 安装 perf (yum, centos, redhat)

```
yum install perf
```

## 安装 perf（从源码编译）

deepin 20 没有提供 Perf 的二进制版本，需要从 linux 内核的源码编译。注意内核的源码版本要与你使用的内核一致。

```
$ sudo apt install flex bison

$ cd ~/project/deb-build/linux-kernel

$ uname -a
Linux duanyao-laptop-c 5.12.18-amd64-desktop #1 SMP Tue Jul 20 15:26:54 CST 2021 x86_64 GNU/Linux

$ apt source linux-image-5.12.18-amd64-desktop

$ ls
linux-5.12.18-amd64-desktop-5.12.18-amd64-desktop
linux-5.12.18-amd64-desktop_5.12.18-amd64-desktop-1.diff.gz
linux-5.12.18-amd64-desktop_5.12.18-amd64-desktop-1.dsc
linux-5.12.18-amd64-desktop_5.12.18-amd64-desktop.orig.tar.gz

$ cd linux-5.12.18-amd64-desktop-5.12.18-amd64-desktop/tools/

$ make perf prefix=/usr/local/
$ make perf_install # 或者 sudo make perf_install
```

prefix 其实没啥用。不论是否以 root 运行 make perf_install，它会安装 perf 和 trace 程序到 `~/bin/perf` 。当然，可以手动复制到 `/usr/bin` 。

## 安装 perf（从源码编译，yum，rpm）

```
mkdir ~/rpm-build
cd rpm-build
yumdownloader --source kernel
ls

^ kernel-4.18.0-348.7.1.el8_5.src.rpm

rpm -ivh ./kernel-4.18.0-348.7.1.el8_5.src.rpm

ls -l ~/rpmbuild/SOURCES/

^ -rw-rw-r-- 1 root root 116978156 12月 22 21:05 linux-4.18.0-348.7.1.el8_5.tar.xz

yum-builddep kernel

^ 没有匹配的软件包可以安装： 'dwarves'
^ 没有匹配的软件包可以安装： 'libbabeltrace-devel
^ 没有匹配的软件包可以安装： 'opencsd-devel >= 1.0.0-2'
^ 没有匹配的软件包可以安装： 'libbpf-devel'

tar -xf ~/rpmbuild/SOURCES/linux-4.18.0-348.7.1.el8_5.tar.xz

ls -l

^ drwxrwxr-x 25 root root      4096 12月  9 2021 linux-4.18.0-348.7.1.el8_5

cd linux-4.18.0-348.7.1.el8_5/tools

yum install bison flex

make perf prefix=/usr/local/

make perf_install

ls -l ~/bin/

^ -rwxr-xr-x 2 root root 18617248 6月  15 18:47 perf
^ -rwxr-xr-x 2 root root 18617248 6月  15 18:47 trace
```

在阿里云 ECS Xeon Platinum 8163 上，这样编译出来的 perf 与 yum 安装的 perf 类似，也不支持向量浮点事件。

## 可查看的项目

列出可查看的项目
perf list 

```
  task-clock                                         [Software event]
  cpu-clock                                          [Software event]
  cpu-cycles OR cycles                               [Hardware event]
  instructions                                       
  duration_time                                      [Tool event]

floating point:
  fp_arith_inst_retired.128b_packed_double          
       [Number of SSE/AVX computational 128-bit packed double precision
        floating-point instructions retired; some instructions will count
        twice as noted below. Each count represents 2 computation operations,
        one for each element. Applies to SSE* and AVX* packed double precision
        floating-point instructions: ADD SUB HADD HSUB SUBADD MUL DIV MIN MAX
        SQRT DPP FM(N)ADD/SUB. DPP and FM(N)ADD/SUB instructions count twice
        as they perform 2 calculations per element]
  fp_arith_inst_retired.128b_packed_single          
       [Number of SSE/AVX computational 128-bit packed single precision
        floating-point instructions retired; some instructions will count
        twice as noted below. Each count represents 4 computation operations,
        one for each element. Applies to SSE* and AVX* packed single precision
        floating-point instructions: ADD SUB HADD HSUB SUBADD MUL DIV MIN MAX
        SQRT RSQRT RCP DPP FM(N)ADD/SUB. DPP and FM(N)ADD/SUB instructions
        count twice as they perform 2 calculations per element]
  fp_arith_inst_retired.256b_packed_double          
       [Number of SSE/AVX computational 256-bit packed double precision
        floating-point instructions retired; some instructions will count
        twice as noted below. Each count represents 4 computation operations,
        one for each element. Applies to SSE* and AVX* packed double precision
        floating-point instructions: ADD SUB HADD HSUB SUBADD MUL DIV MIN MAX
        SQRT FM(N)ADD/SUB. FM(N)ADD/SUB instructions count twice as they
        perform 2 calculations per element]
  fp_arith_inst_retired.256b_packed_single          
       [Number of SSE/AVX computational 256-bit packed single precision
        floating-point instructions retired; some instructions will count
        twice as noted below. Each count represents 8 computation operations,
        one for each element. Applies to SSE* and AVX* packed single precision
        floating-point instructions: ADD SUB HADD HSUB SUBADD MUL DIV MIN MAX
        SQRT RSQRT RCP DPP FM(N)ADD/SUB. DPP and FM(N)ADD/SUB instructions
        count twice as they perform 2 calculations per element]
  fp_arith_inst_retired.scalar_double               
       [Number of SSE/AVX computational scalar double precision floating-point
        instructions retired; some instructions will count twice as noted
        below. Each count represents 1 computational operation. Applies to
        SSE* and AVX* scalar double precision floating-point instructions: ADD
        SUB MUL DIV MIN MAX SQRT FM(N)ADD/SUB. FM(N)ADD/SUB instructions count
        twice as they perform 2 calculations per element]
  fp_arith_inst_retired.scalar_single               
       [Number of SSE/AVX computational scalar single precision floating-point
        instructions retired; some instructions will count twice as noted
        below. Each count represents 1 computational operation. Applies to
        SSE* and AVX* scalar single precision floating-point instructions: ADD
        SUB MUL DIV MIN MAX SQRT RSQRT RCP FM(N)ADD/SUB. FM(N)ADD/SUB
        instructions count twice as they perform 2 calculations per element]
  fp_assist.any                                     
       [Cycles with any input/output SSE or FP assist]
```

duration_time: 程序运行的世界时间（wall clock），单位是纳秒。
task-clock, cpu-clock：程序运行的 CPU 时间，单位是毫秒。对于多核CPU上运行多线程程序，CPU 时间会大于世界时间。

查看程序浮点性能的选项：
```
clock,cycles,instructions,fp_arith_inst_retired.scalar_single,fp_arith_inst_retired.128b_packed_single,fp_arith_inst_retired.256b_packed_single,fp_arith_inst_retired.scalar_double,fp_arith_inst_retired.128b_packed_double,fp_arith_inst_retired.256b_packed_double 
```

## 选项

```
-B, --big-num
--no-big-num
perf config stat.big-num=false

-C, --cpu=0,1 --cpu=0-2

-a, --all-cpus
           system-wide collection from all CPUs (default if no target is
           specified)

--per-thread
           Aggregate counts per monitored threads, when monitoring
           threads (-t option) or processes (-p option).

-i, --no-inherit
           child tasks do not inherit counters. 默认状态下，perf stat 统计

-p, --pid=<pid>
           stat events on existing process id (comma separated list)

-t, --tid=<tid>
           stat events on existing thread id (comma separated list)
           
-x SEP, --field-separator SEP
           print counts using a CSV-style output to make it easy to
           import directly into spreadsheets. Columns are separated by
           the string specified in SEP.
```

--per-thread -p 可以联用，这对于分析一个进程的各个线程的特征很有用。

## 权限问题

```
Error:
Access to performance monitoring and observability operations is limited.
Consider adjusting /proc/sys/kernel/perf_event_paranoid setting to open
access to performance monitoring and observability operations for processes
without CAP_PERFMON, CAP_SYS_PTRACE or CAP_SYS_ADMIN Linux capability.
More information can be found at 'Perf events and tool security' document:
https://www.kernel.org/doc/html/latest/admin-guide/perf-security.html
perf_event_paranoid setting is 2:
  -1: Allow use of (almost) all events by all users
      Ignore mlock limit after perf_event_mlock_kb without CAP_IPC_LOCK
>= 0: Disallow raw and ftrace function tracepoint access
>= 1: Disallow CPU event access
>= 2: Disallow kernel profiling
To make the adjusted perf_event_paranoid setting permanent preserve it
in /etc/sysctl.conf (e.g. kernel.perf_event_paranoid = <setting>)
```

修改：
```
sudo sysctl kernel.perf_event_paranoid=-1
```
要永久生效，建议 `kernel.perf_event_paranoid=-1` 加入 /etc/sysctl.d/zz-custom.conf 文件。

## 用法举例

### 默认统计

```
$ perf stat  dd if=/dev/zero of=/dev/null count=100000
记录了100000+0 的读入
记录了100000+0 的写出
51200000 bytes (51 MB, 49 MiB) copied, 0.130591 s, 392 MB/s

 Performance counter stats for 'dd if=/dev/zero of=/dev/null count=100000':

            130.69 msec task-clock                #    0.993 CPUs utilized          
                 6      context-switches          #    0.046 K/sec                  
                 2      cpu-migrations            #    0.015 K/sec                  
                75      page-faults               #    0.574 K/sec                  
       349,537,843      cycles                    #    2.675 GHz                      (74.03%)
       193,947,565      instructions              #    0.55  insn per cycle           (75.78%)
        40,243,753      branches                  #  307.938 M/sec                    (75.57%)
           622,156      branch-misses             #    1.55% of all branches          (74.62%)

       0.131578240 seconds time elapsed

       0.079574000 seconds user
       0.051723000 seconds sys
```
等效的做法：
```
$ perf stat -e task-clock,cycles,instructions  dd if=/dev/zero of=/dev/null count=100000
记录了100000+0 的读入
记录了100000+0 的写出
51200000 bytes (51 MB, 49 MiB) copied, 0.131276 s, 390 MB/s

 Performance counter stats for 'dd if=/dev/zero of=/dev/null count=100000':

            131.58 msec task-clock                #    0.994 CPUs utilized          
       352,683,484      cycles                    #    2.680 GHz                    
       193,080,887      instructions              #    0.55  insn per cycle         

       0.132310526 seconds time elapsed

       0.104009000 seconds user
       0.028002000 seconds sys
```
注意，需要指定 task-clock，cycles 之后才能计算出 xxx GHz ；指定 cycles，才能计算出 xxx insn per cycle 

### 某进程的浮点性能统计

总体：
```
perf stat -e task-clock,cycles,instructions,fp_arith_inst_retired.scalar_single,fp_arith_inst_retired.128b_packed_single,fp_arith_inst_retired.256b_packed_single,fp_arith_inst_retired.scalar_double,fp_arith_inst_retired.128b_packed_double,fp_arith_inst_retired.256b_packed_double --no-big-num -p nnn
```
按线程：
```
perf stat -e task-clock,cycles,instructions,fp_arith_inst_retired.scalar_single,fp_arith_inst_retired.128b_packed_single,fp_arith_inst_retired.256b_packed_single,fp_arith_inst_retired.scalar_double,fp_arith_inst_retired.128b_packed_double,fp_arith_inst_retired.256b_packed_double --no-big-num --per-thread -p nnn
```

输出形如：
```
 Performance counter stats for process id '13104':

          81032.91 msec task-clock                #    2.311 CPUs utilized          
      214415823290      cycles                    #    2.646 GHz                      (37.50%)
      309933860034      instructions              #    1.45  insn per cycle           (37.49%)
          83286948      fp_arith_inst_retired.scalar_single #    1.028 M/sec                    (37.54%)
                 0      fp_arith_inst_retired.128b_packed_single #    0.000 K/sec                    (37.45%)
      337647724109      fp_arith_inst_retired.256b_packed_single # 4166.798 M/sec                    (37.51%)
          17218078      fp_arith_inst_retired.scalar_double #    0.212 M/sec                    (37.54%)
            230527      fp_arith_inst_retired.128b_packed_double #    0.003 M/sec                    (37.60%)
                 0      fp_arith_inst_retired.256b_packed_double #    0.000 K/sec                    (37.41%)

      35.059715903 seconds time elapsed
```

使用自定义 CSV 分隔符，产生机器可读的输出：

```
perf stat -e task-clock,cycles,instructions,fp_arith_inst_retired.scalar_single,fp_arith_inst_retired.128b_packed_single,fp_arith_inst_retired.256b_packed_single,fp_arith_inst_retired.scalar_double,fp_arith_inst_retired.128b_packed_double,fp_arith_inst_retired.256b_packed_double --no-big-num -x ";" -p 10394

58251.58;msec;task-clock;58251581640;100.00;2.078;CPUs utilized
153680669207;;cycles;21961839437;37.45;2.638;GHz
211518056507;;instructions;21924922401;37.39;1.38;insn per cycle
63504923;;fp_arith_inst_retired.scalar_single;21908960393;37.36;1.090;M/sec
0;;fp_arith_inst_retired.128b_packed_single;21860633363;37.28;0.000;K/sec
229845840891;;fp_arith_inst_retired.256b_packed_single;21959354369;37.45;3945.744;M/sec
10884131;;fp_arith_inst_retired.scalar_double;22048752524;37.62;0.187;M/sec
225779;;fp_arith_inst_retired.128b_packed_double;22224333674;37.91;0.004;M/sec
0;;fp_arith_inst_retired.256b_packed_double;22045474767;37.60;0.000;K/sec
```
字段解释：

task-clock 行： （1）时长，单位毫秒：58251.58（2）左边的单位：msec（3）对左边的说明：task-clock（4）时长，单位纳秒：58251581640（5）不明，总是 100.00（6）CPU占用率，2.078（7）对左边的说明。
cycles 行：（1）CPU周期：153680669207（2）空白（3）对左边的说明：cycles（4）不明：21961839437（5）不明：37.45（6）CPU频率：2.638（7）左边的单位GHz。
instructions行：（1）指令数？：211518056507（2）空白（3）对左边的说明：instructions（4）不明：21924922401（5）不明：37.39（6）每周期指令数（IPC）：1.38（7）对左边的说明：insn per cycle
fp_arith_inst_retired.scalar_single行：（1）指令数：63504923（2）空白（3）对左边的说明：fp_arith_inst_retired.scalar_single（4）不明：21908960393（5）不明：37.36（6）指令出现频率：1.090 M/s（7）左边的单位：M/sec

### 全系统、按线程、核心统计
```
perf stat -a --per-thread -e instructions:u
^C
 Performance counter stats for 'system wide':

 gvfs-udisks2-vo-29808         1,228,453,912      instructions:u                                              
  winedevice.exe-28681           472,238,675      instructions:u                                              
         systemd-29230           169,104,530      instructions:u                                              
     firefox-bin-27520           103,169,540      instructions:u                                              
 deepin-terminal-27577           102,115,414      instructions:u                                              
        kwin_x11-29488            81,182,028      instructions:u                                              
     gvfsd-trash-24444            65,360,022      instructions:u                                              
 huayupy-daemon--30323            65,308,095      instructions:u                                              
     Web Content-12645            55,723,054      instructions:u                                              
 deepin-deepinid-30345            32,698,880      instructions:u
 
$ perf stat -a --per-core -e instructions:u^C
 Performance counter stats for 'system wide':

S0-D0-C0           2      4,882,163,527      instructions:u                                              
S0-D0-C1           2      4,627,917,489      instructions:u                                              

       5.143119743 seconds time elapsed
```

## perf top

实时监视。但结果看不懂。

-p 参数指定监视的进程。
-K 忽略内核线程。

## 用 libpfm4 来查看 CPU 支持的检测项目

```
git clone git://perfmon2.git.sourceforge.net/gitroot/perfmon2/libpfm4 libpfm4.git
cd libpfm4.git
make
./examples/check_events | grep SSE
./examples/showevtinfo > showevtinfo.txt

cp ./examples/showevtinfo ./examples/check_events ~/bin/ # 可选
```
遗憾的是，在我的机器上的输出与 [1.5-1.6] 不符，没有 FP_COMP_OPS_EXE 或者 SSE 的相关内容。
但是在 showevtinfo 的输出中 showevtinfo.txt 可以看到

但是可以看到向量浮点运算相关的信息：

i5-6200U （属于 Skylake 架构）
```
#-----------------------------
IDX	 : 419430469
PMU name : skl (Intel Skylake)
Name     : FP_ARITH
Equiv	 : FP_ARITH_INST_RETIRED
Flags    : None
Desc     : Floating-point instructions retired
Code     : 0xc7
Umask-00 : 0x01 : PMU : [SCALAR_DOUBLE] : None : Number of scalar double precision floating-point arithmetic instructions (multiply by 1 to get flops)
Umask-01 : 0x02 : PMU : [SCALAR_SINGLE] : None : Number of scalar single precision floating-point arithmetic instructions (multiply by 1 to get flops)
Umask-02 : 0x04 : PMU : [128B_PACKED_DOUBLE] : None : Number of scalar 128-bit packed double precision floating-point arithmetic instructions (multiply by 2 to get flops)
Umask-03 : 0x08 : PMU : [128B_PACKED_SINGLE] : None : Number of scalar 128-bit packed single precision floating-point arithmetic instructions (multiply by 4 to get flops)
Umask-04 : 0x10 : PMU : [256B_PACKED_DOUBLE] : None : Number of scalar 256-bit packed double precision floating-point arithmetic instructions (multiply by 4 to get flops)
Umask-05 : 0x20 : PMU : [256B_PACKED_SINGLE] : None : Number of scalar 256-bit packed single precision floating-point arithmetic instructions (multiply by 8 to get flops)
Umask-06 : 0x40 : PMU : [512B_PACKED_DOUBLE] : None : Number of scalar 512-bit packed double precision floating-point arithmetic instructions (multiply by 8 to get flops)
Umask-07 : 0x80 : PMU : [512B_PACKED_SINGLE] : None : Number of scalar 512-bit packed single precision floating-point arithmetic instructions (multiply by 16 to get flops)
Modif-00 : 0x00 : PMU : [k] : monitor at priv level 0 (boolean)
Modif-01 : 0x01 : PMU : [u] : monitor at priv level 1, 2, 3 (boolean)
Modif-02 : 0x02 : PMU : [e] : edge level (may require counter-mask >= 1) (boolean)
Modif-03 : 0x03 : PMU : [i] : invert (boolean)
Modif-04 : 0x04 : PMU : [c] : counter-mask in range [0-255] (integer)
Modif-05 : 0x05 : PMU : [t] : measure any thread (boolean)
Modif-06 : 0x07 : PMU : [intx] : monitor only inside transactional memory region (boolean)
Modif-07 : 0x08 : PMU : [intxcp] : do not count occurrences inside aborted transactional memory region (boolean)
#-----------------------------
```

Xeon Platinum 8163（属于 Skylake-SP 架构） 
```
#-----------------------------
IDX	 : 668991557
PMU name : skx (Intel Skylake X)
Name     : FP_ARITH
Equiv	 : FP_ARITH_INST_RETIRED
Flags    : None
Desc     : Floating-point instructions retired
Code     : 0xc7
Umask-00 : 0x01 : PMU : [SCALAR_DOUBLE] : None : Number of scalar double precision floating-point arithmetic instructions (multiply by 1 to get flops)
Umask-01 : 0x02 : PMU : [SCALAR_SINGLE] : None : Number of scalar single precision floating-point arithmetic instructions (multiply by 1 to get flops)
Umask-02 : 0x04 : PMU : [128B_PACKED_DOUBLE] : None : Number of scalar 128-bit packed double precision floating-point arithmetic instructions (multiply by 2 to get flops)
Umask-03 : 0x08 : PMU : [128B_PACKED_SINGLE] : None : Number of scalar 128-bit packed single precision floating-point arithmetic instructions (multiply by 4 to get flops)
Umask-04 : 0x10 : PMU : [256B_PACKED_DOUBLE] : None : Number of scalar 256-bit packed double precision floating-point arithmetic instructions (multiply by 4 to get flops)
Umask-05 : 0x20 : PMU : [256B_PACKED_SINGLE] : None : Number of scalar 256-bit packed single precision floating-point arithmetic instructions (multiply by 8 to get flops)
Umask-06 : 0x40 : PMU : [512B_PACKED_DOUBLE] : None : Number of scalar 512-bit packed double precision floating-point arithmetic instructions (multiply by 8 to get flops)
Umask-07 : 0x80 : PMU : [512B_PACKED_SINGLE] : None : Number of scalar 512-bit packed single precision floating-point arithmetic instructions (multiply by 16 to get flops)
Modif-00 : 0x00 : PMU : [k] : monitor at priv level 0 (boolean)
Modif-01 : 0x01 : PMU : [u] : monitor at priv level 1, 2, 3 (boolean)
Modif-02 : 0x02 : PMU : [e] : edge level (may require counter-mask >= 1) (boolean)
Modif-03 : 0x03 : PMU : [i] : invert (boolean)
Modif-04 : 0x04 : PMU : [c] : counter-mask in range [0-255] (integer)
Modif-05 : 0x05 : PMU : [t] : measure any thread (boolean)
Modif-06 : 0x07 : PMU : [intx] : monitor only inside transactional memory region (boolean)
Modif-07 : 0x08 : PMU : [intxcp] : do not count occurrences inside aborted transactional memory region (boolean)
```

不过，向量整数运算没有看到。

用 check_events 查看结果：

```
./examples/check_events FP_ARITH:SCALAR_SINGLE # 或者 ./examples/check_events FP_ARITH:0x02 或者 ./examples/check_events FP_ARITH:SCALAR_SINGLE
Requested Event: FP_ARITH:SCALAR_SINGLE
Actual    Event: skl::FP_ARITH_INST_RETIRED:SCALAR_SINGLE:k=1:u=1:e=0:i=0:c=0:t=0:intx=0:intxcp=0
PMU            : Intel Skylake
IDX            : 419430470
Codes          : 0x5302c7
```

再查看单精度操作的 Codes， 结果如下

```
FP_ARITH:SCALAR_SINGLE      0x5302c7
FP_ARITH:128B_PACKED_SINGLE 0x5308c7
FP_ARITH:256B_PACKED_SINGLE 0x5320c7
FP_ARITH:512B_PACKED_SINGLE 0x5380c7
```

查看双精度的：

```
./examples/check_events FP_ARITH:SCALAR_DOUBLE FP_ARITH:128B_PACKED_DOUBLE FP_ARITH:256B_PACKED_DOUBLE FP_ARITH:512B_PACKED_DOUBLE

FP_ARITH:SCALAR_DOUBLE       0x5301c7
FP_ARITH:128B_PACKED_DOUBLE  0x5304c7
FP_ARITH:256B_PACKED_DOUBLE  0x5310c7
FP_ARITH:512B_PACKED_DOUBLE  0x5340c7
```

以上这些 Codes，对于 Skylake 和 Skylake-SP 是一样的。

接下来，将 Codes 应用于 perf stat 的 -e 参数，可以测量一个程序的浮点操作数量，即运行 `perf stat -e r5302c7 -e r5308c7 -e r5320c7 -e r5380c7 -e r5301c7 -e r5304c7 -e r5310c7 -e r5340c7 program_to_messure` 。

但是，showevtinfo 和 check_events 能查到的事件，perf stat 却不一定支持。例子就是阿里云 ECS 的 Xeon Platinum 8163 ，它显示：

```
 Performance counter stats for 'python3':

   <not supported>      r5302c7                                                     
   <not supported>      r5308c7                                                     
   <not supported>      r5320c7                                                     
   <not supported>      r5380c7                                                     
   <not supported>      r5301c7                                                     
   <not supported>      r5304c7                                                     
   <not supported>      r5310c7                                                     
   <not supported>      r5340c7                                                     

       3.813385834 seconds time elapsed

       0.017914000 seconds user
       0.003850000 seconds sys
```

## 浮点性能测量

```
$ perf stat -e r5302c7 -e r5308c7 -e r5320c7 -e r5380c7 -e r5301c7 -e r5304c7 -e r5310c7 -e r5340c7 python demos/instance_segmentation_demo/python/instance_segmentation_demo.py -m intel/instance-segmentation-security-1040/FP32/instance-segmentation-security-1040.xml --labels data/dataset_classes/coco_80cl.txt -i /home/duanyao/project/annotation/1625185842605/1.jpg
[ INFO ] Creating Inference Engine...
[ INFO ] Loading network
[ INFO ] Loading IR to the plugin...
[ INFO ] Starting inference...
To close the application, press 'CTRL+C' here or switch to the output window and press ESC key
python: 已终止

 Performance counter stats for 'python demos/instance_segmentation_demo/python/instance_segmentation_demo.py -m intel/instance-segmentation-security-1040/FP32/instance-segmentation-security-1040.xml --labels data/dataset_classes/coco_80cl.txt -i /home/duanyao/project/annotation/1625185842605/1.jpg':

        30,353,064      r5302c7:u                                                     (38.82%)
           407,618      r5308c7:u                                                     (39.30%)
     3,498,708,183      r5320c7:u                                                     (39.00%)
                 0      r5380c7:u                                                     (38.20%)
           207,027      r5301c7:u                                                     (37.66%)
                 5      r5304c7:u                                                     (36.91%)
                21      r5310c7:u                                                     (36.98%)
                31      r5340c7:u                                                     (37.61%)

      14.252401555 seconds time elapsed

       2.858037000 seconds user
       0.806678000 seconds sys
```
可见 r5320c7（ FP_ARITH:256B_PACKED_SINGLE ）占绝大部分浮点操作，计算其flops：3498708183*8=27989665464 ~ 28Gflops。
文档给出的 instance-segmentation-security-1040 计算量为 29.334 GFlops，基本符合。

但 FP16-INT8 模型的数据有较大的偏离，说明可能有一些整数运算没有被统计：

```
perf stat -e r5302c7 -e r5308c7 -e r5320c7 -e r5380c7 -e r5301c7 -e r5304c7 -e r5310c7 -e r5340c7 python demos/instance_segmentation_demo/python/instance_segmentation_demo.py -m intel/instance-segmentation-security-1040/FP16-INT8/instance-segmentation-security-1040.xml --labels data/dataset_classes/coco_80cl.txt -i ~/project/annotation/1625185842605/1.jpg

        43,510,297      r5302c7:u                                                     (37.34%)
         2,730,040      r5308c7:u                                                     (38.10%)
       458,800,216      r5320c7:u                                                     (37.80%)
                60      r5380c7:u                                                     (37.87%)
           402,893      r5301c7:u                                                     (38.12%)
               380      r5304c7:u                                                     (38.36%)
                29      r5310c7:u                                                     (37.70%)
                 5      r5340c7:u                                                     (37.82%)
```

根据 openvino 文档，INT8 操作是通过 SSE4.2 ~ AVX* 指令集进行的：

https://github.com/openvinotoolkit/openvino/blob/master/docs/IE_DG/Int8Inference.md

不知道是不是因为 libpfm4 不支持这类指令集。


```
perf stat -e task-clock,cycles,instructions,fp_arith_inst_retired.scalar_single,fp_arith_inst_retired.128b_packed_single,fp_arith_inst_retired.256b_packed_single,fp_arith_inst_retired.scalar_double,fp_arith_inst_retired.128b_packed_double,fp_arith_inst_retired.256b_packed_double python demos/instance_segmentation_demo/python/instance_segmentation_demo.py -m intel/instance-segmentation-security-1040/FP16-INT8/instance-segmentation-security-1040.xml --labels data/dataset_classes/coco_80cl.txt -i ~/project/annotation/1625185842605/1.jpg
```

## PAPI
[3.5]
```
git clone https://bitbucket.org/icl/papi.git
cd papi.git/
cd src/
./configure --prefix=$PWD/install
make && make install
cd install/bin
./papi_avail
```

## python-papi
[3.1-3.3]
```
pip install python-papi
```
python-papi 使用 PAPI 为底层，理论上与 perf 使用相同的数据源。

示例程序：

```
from pypapi import papi_high
from pypapi import events as papi_events

# Starts some counters
papi_high.start_counters([
    papi_events.PAPI_TOT_CYC,
    papi_events.PAPI_FP_INS,
    papi_events.PAPI_FP_OPS,
    papi_events.PAPI_SP_OPS,
    papi_events.PAPI_DP_OPS,
    papi_events.PAPI_VEC_SP,
    papi_events.PAPI_VEC_DP,
])

# Reads values from counters and reset them
results = papi_high.read_counters()  # -> [int, int]

# Reads values from counters and stop them
results = papi_high.stop_counters()  # -> [int, int]
```
events 类型的含义：

```
PAPI_FP_INS 	SSE_SCALAR_DOUBLE + SSE_FP_SCALAR_SINGLE
PAPI_FP_OPS 	same as above
PAPI_SP_OPS 	FP_COMP_OPS_EXE:SSE_FP_SCALAR_SINGLE + 4*(FP_COMP_OPS_EXE:SSE_PACKED_SINGLE) + 8*(SIMD_FP_256:PACKED_SINGLE)
PAPI_DP_OPS 	FP_COMP_OPS_EXE:SSE_SCALAR_DOUBLE + 2*(FP_COMP_OPS_EXE:SSE_FP_PACKED_DOUBLE) + 4*(SIMD_FP_256:PACKED_DOUBLE)
PAPI_VEC_SP 	4*(FP_COMP_OPS_EXE:SSE_PACKED_SINGLE) + 8*(SIMD_FP_256:PACKED_SINGLE)
PAPI_VEC_DP 	2*(FP_COMP_OPS_EXE:SSE_FP_PACKED_DOUBLE) + 4*(SIMD_FP_256:PACKED_DOUBLE)
```
可见，并不能分别统计 SSE(128) 和 AVX(256) 的操作数。

python-papi 存在一个问题：不同的CPU（操作系统）支持的事件是不一样的，如果指定了不支持的事件，会产生错误:
```
pypapi.exceptions.PapiNoEventError: Event does not exist. (PAPI_ENOEVNT) 
```
PAPI 支持的事件类型应该通过 papi_avail 程序来查看。

支持的事件，也可能发生错误：
```
pypapi.exceptions.PapiConflictError: Event exists, but cannot be counted due to counter resource limitations. (PAPI_ECNFLCT)
```

## 整数性能测量

## 问题

根据2013 年的信息[1.6]，当时的 intel 处理器的浮点操作计数器不可靠。

某些 intel CPU 型号不支持浮点事件[4.2]

某些 intel CPU 型号+操作系统的组合不支持向量浮点指令的计数。如阿里云 ECS + Xeon Platinum 8163 + centos 8 。

某些 AMD CPU 不支持向量浮点指令的计数。

## vtune
