## 参考资料
[1] How to Install gcc 5.3 with yum on CentOS 7.2?
https://stackoverflow.com/questions/36327805/how-to-install-gcc-5-3-with-yum-on-centos-7-2

[2] 用yum将安装包及其依赖包下载到本地的方法
https://blog.csdn.net/GX_1_11_real/article/details/80694556

[3] cmake: symbol lookup error: cmake: undefined symbol: archive_write_add_filter_zstd 两种解决方法
https://blog.csdn.net/chenyulancn/article/details/118540210

[4] Install FFMpeg on CentOS 8 with YUM or DNF
https://www.binarycomputer.solutions/install-ffmpeg-centos-8-yum-dnf/

[5] https://johnvansickle.com/ffmpeg/

[6] http://trac.ffmpeg.org/ticket/9309

[7] Failed to set locale, defaulting to C https://forums.centos.org/viewtopic.php?f=54&t=72323&sid=3feeb72abd7098be702593573214d6f2&start=10

## 软件仓库和安装

### 安装 gcc/g++ 等 C/C++ 开发工具

yum install gcc-c++ # centos 8 默认 gcc-c++-8.4, centos 7 默认 gcc-c++-4.8, 
yum install make    # centos 8 默认 4.2
yum install cmake   #  centos 8 默认 3.18, centos 7 默认 2.8.  centos 7 可平行安装包 cmake3 (3.17)，命令名也是 cmake3
yum install libarchive # centos 8 cmake 3.18 依赖这个

### 安装较新的 gcc
[1]
因为 centos 7 自带的 gcc-c++ 版本很老（4.8），所以需要安装7.0以上的。方法是使用 scl 仓库：
```
yum install centos-release-scl
yum search devtoolset
```

可以看到所有支持的gcc版本，目前最新的是7。安装 gcc-c++ 7：
```
yum install devtoolset-7-gcc-c++
scl enable devtoolset-7 bash
```

### 安装 python
centos 8 可以安装以下版本之一：

```
python3  # 默认为 python36
python36
python38
python39
```

### 安装 ffmpeg

centos 8 [4]：

（1）安装 rpm fusion 仓库，见相关章节。
（2）安装 SDL2。这个没有在相关仓库中，但可以单独安装 rpm 包（yum和dnf二选一，如果404,在rpmfind.net上重新搜索）：

```
yum install http://rpmfind.net/linux/centos/8-stream/PowerTools/x86_64/os/Packages/SDL2-2.0.10-2.el8.x86_64.rpm
dnf install http://rpmfind.net/linux/centos/8-stream/PowerTools/x86_64/os/Packages/SDL2-2.0.10-2.el8.x86_64.rpm
```
 如果链接失效，可以在 rpmfind.net 上搜索 SDL2 。

 (3) `yum install ffmpeg` 或者 `dnf install ffmpeg` 。这也会同时安装 ffprobe 。
 
 
注意，ffmpeg 静态编译版本[5] 2019.7-2021.8 的版本在 centos 8 上工作有问题，读 rtsp 等网络数据时会崩溃 [6]。

### epel 仓库
企业版 Linux 附加软件包(以下简称 EPEL)是一个由特别兴趣小组创建、维护并管理的，针对红帽企业版 Linux(RHEL)及其衍生发行版(比如 CentOS、Scientific Linux、Oracle Enterprise Linux)的一个高质量附加软件包项目。

yum -y install epel-release

epel安装完成后，更新源：

yum clean all && yum makecache

### rpm fusion 仓库
[4]
两者二选一即可。

yum 方法
```
yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm # 安装过 epel-release 可省略
yum install https://download1.rpmfusion.org/free/el/rpmfusion-free-release-8.noarch.rpm https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-8.noarch.rpm
```

dnf 方法
```
dnf -y install https://download.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
dnf config-manager --enable PowerTools
dnf install --nogpgcheck https://download1.rpmfusion.org/free/el/rpmfusion-free-release-8.noarch.rpm https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-8.noarch.rpm
```

### yum-config-manager
来自 yum-utils。用法：

yum-config-manager --disable mysql80-community
yum-config-manager --enable mysql57-community

### yum-plugin-downloadonly
yum install yum-plugin-downloadonly
yum install --downloadonly mysql-community-server

## docker

```
yum install net-tools bind-utils
```

### 阿里云 centos 8 镜像仓库地址修复
参考：阿里云 Centos8 yum 镜像 404 https://blog.csdn.net/fan63182830/article/details/122808633

```
cd /etc/yum.repos.d/
for i in `ls`;do sed -i 's/mirrors.cloud.aliyuncs.com/mirrors.aliyun.com/g' $i;done
for i in `ls`;do sed -i 's/$releasever/$releasever-stream/g' $i;done

yum clean all
yum makecache
```

但是，其它附加仓库仍然可能导致 404 错误，。例如
```

Invalid configuration value: failovermethod=priority in /etc/yum.repos.d/CentOS-Linux-epel.repo; 配置：ID 为 "failovermethod" 的 OptionBinding 不存在
Repository epel is listed more than once in the configuration
Invalid configuration value: failovermethod=priority in /etc/yum.repos.d/nodesource-el8.repo; 配置：ID 为 "failovermethod" 的 OptionBinding 不存在
Invalid configuration value: failovermethod=priority in /etc/yum.repos.d/nodesource-el8.repo; 配置：ID 为 "failovermethod" 的 OptionBinding 不存在
Docker CE Stable - x86_64                                                                                                                 5.3 kB/s | 2.3 kB     00:00    
Errors during downloading metadata for repository 'docker-ce-stable':
  - Status code: 404 for https://mirrors.aliyun.com/docker-ce/linux/centos/8-stream/x86_64/stable/repodata/repomd.xml (IP: 42.81.213.231)
错误：为仓库 'docker-ce-stable' 下载元数据失败 : Cannot download repomd.xml: Cannot download repodata/repomd.xml: All mirrors were tried

Invalid configuration value: failovermethod=priority in /etc/yum.repos.d/nodesource-el8.repo; 配置：ID 为 "failovermethod" 的 OptionBinding 不存在

Errors during downloading metadata for repository 'epel':
  - Status code: 404 for https://mirrors.fedoraproject.org/metalink?repo=epel-8-stream&arch=x86_64&infra=stock&content=centos (IP: 13.233.183.170)
```

解决办法：可以更新或删除出错的 .repo 文件，再重试：
```
wget -O /etc/yum.repos.d/docker-ce.repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
rm /etc/yum.repos.d/nodesource-el8.repo
rm /etc/yum.repos.d/CentOS-Linux-epel.repo
rm -f /etc/yum.repos.d/epel*.repo

yum clean all
yum makecache
```


### CentOS Linux 8 - AppStream 

参考： https://blog.csdn.net/qq21497936/article/details/122862858

```
# yum install tcpdump
Invalid configuration value: failovermethod=priority in /etc/yum.repos.d/nodesource-el8.repo; Configuration: OptionBinding with id "failovermethod" does not exist
Invalid configuration value: failovermethod=priority in /etc/yum.repos.d/nodesource-el8.repo; Configuration: OptionBinding with id "failovermethod" does not exist
CentOS Linux 8 - AppStream                                                                                                                 17  B/s |  38  B     00:02    
Error: Failed to download metadata for repo 'appstream': Cannot prepare internal mirrorlist: No URLs in mirrorlist
```

```
cd /etc/yum.repos.d
mkdir /project/centos8_yum.bk
mv *.repo /project/centos8_yum.bk/
curl https://mirrors.aliyun.com/repo/Centos-vault-8.5.2111.repo -o /etc/yum.repos.d/CentOS-Base.repo
```

### 阿里云 centos 8 封禁 3128 端口？

阿里云 ECS 的 centos 8 上的 squid 的默认端口 3128 似乎被封堵了（即使从 127.0.0.1 访问），改成其它端口，如 5432 才可以访问。

## locale 问题
```
locale

locale: Cannot set LC_CTYPE to default locale: No such file or directory
locale: Cannot set LC_MESSAGES to default locale: No such file or directory
locale: Cannot set LC_ALL to default locale: No such file or directory
LANG=zh_CN.UTF-8
LC_CTYPE="zh_CN.UTF-8"
LC_NUMERIC="zh_CN.UTF-8"
LC_TIME="zh_CN.UTF-8"
LC_COLLATE="zh_CN.UTF-8"
LC_MONETARY="zh_CN.UTF-8"
LC_MESSAGES="zh_CN.UTF-8"
LC_PAPER="zh_CN.UTF-8"
LC_NAME="zh_CN.UTF-8"
LC_ADDRESS="zh_CN.UTF-8"
LC_TELEPHONE="zh_CN.UTF-8"
LC_MEASUREMENT="zh_CN.UTF-8"
LC_IDENTIFICATION="zh_CN.UTF-8"
LC_ALL=

locale -a

C
C.utf8
POSIX
en_AG
en_AU
en_AU.utf8
en_BW
en_BW.utf8
... # 没有 zh_CN.UTF-8
```

这说明系统没有安装 zh_CN.UTF-8 的 locale ，但是用户强行设置了 locale 为 zh_CN.UTF-8 ，这可能是因为从 ssh 客户端登录，而 ssh 客户端的 locale 是 zh_CN.UTF-8，故而登录时将 ssh 会话的 locale 也设置为 zh_CN.UTF-8 了。

要在系统中安装新 locale[7]：
```
#  yum search glibc-langpack | grep zh
glibc-langpack-zh.x86_64 : Locale data for zh

yum install glibc-langpack-zh.x86_64
```
装完即可解决。
