## 参考资料
[1.1] fontconfig-user
https://www.freedesktop.org/software/fontconfig/fontconfig-user.html

[1.2] Font_configuration
https://wiki.archlinux.org/title/Font_configuration

## 配置文件
用户级[1.2]：
```
$HOME/.config/fontconfig/
  fonts.conf
  conf.d/
```
全局[1.2]：
```
/etc/fonts/
  fonts.conf
  conf.avail
  conf.d/
    *.conf
```
## 查询当前的设置
[1.2]
```
fc-match --verbose Sans
fc-match --verbose "Ubuntu Regular"

FC_DEBUG=1 fc-match "Ubuntu Regular"
```
FC_DEBUG 的值[1.1]：

```
  Name         Value    Meaning
  ---------------------------------------------------------
  MATCH            1    Brief information about font matching
  MATCHV           2    Extensive font matching information
  EDIT             4    Monitor match/test/edit execution
  FONTSET          8    Track loading of font information at startup
  CACHE           16    Watch cache files being written
  CACHEV          32    Extensive cache file writing information
  PARSE           64    (no longer in use)
  SCAN           128    Watch font files being scanned to build caches
  SCANV          256    Verbose font file scanning information
  MEMORY         512    Monitor fontconfig memory usage
  CONFIG        1024    Monitor which config files are loaded
  LANGSET       2048    Dump char sets used to construct lang values
  MATCH2        4096    Display font-matching transformation in patterns
```

## 列出字体

使用 fc-list 命令列出系统里的中文字体：
[6.1]
```
fc-list :lang=zh-cn
```
`:lang=zh-cn` 表示过滤出支持简体中文的字体。如果不限于简体，可改为 `:lang=zh`。冒号（:）与 `lang=zh-cn` 之间不要有空格，否则不起作用。

结果大致如下：

```
/usr/share/fonts/opentype/noto/NotoSansCJK-Bold.ttc: Noto Sans CJK JP,Noto Sans CJK JP Bold:style=Bold,Regular
/usr/share/fonts/opentype/noto/NotoSerifCJK-Bold.ttc: Noto Serif CJK SC:style=Bold
/usr/share/fonts/opentype/noto/NotoSerifCJK-Bold.ttc: Noto Serif CJK TC:style=Bold
/usr/share/fonts/opentype/noto/NotoSerifCJK-Bold.ttc: Noto Serif CJK JP:style=Bold
/usr/share/fonts/opentype/noto/NotoSerifCJK-Bold.ttc: Noto Serif CJK KR:style=Bold
/usr/share/fonts/opentype/noto/NotoSansCJK-Bold.ttc: Noto Sans Mono CJK KR,Noto Sans Mono CJK KR Bold:style=Bold,Regular
/usr/share/fonts/truetype/wqy/wqy-microhei.ttc: 文泉驿微米黑,文泉驛微米黑,WenQuanYi Micro Hei:style=Regular
/usr/share/fonts/opentype/noto/NotoSansCJK-Bold.ttc: Noto Sans Mono CJK JP,Noto Sans Mono CJK JP Bold:style=Bold,Regular
/usr/share/fonts/opentype/noto/NotoSansCJK-Regular.ttc: Noto Sans CJK JP,Noto Sans CJK JP Regular:style=Regular
/usr/share/fonts/truetype/wqy/wqy-zenhei.ttc: 文泉驿正黑,文泉驛正黑,WenQuanYi Zen Hei:style=Regular
/usr/share/fonts/truetype/wqy/wqy-zenhei.ttc: 文泉驿点阵正黑,文泉驛點陣正黑,WenQuanYi Zen Hei Sharp:style=Regular
```

