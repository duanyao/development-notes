## 参考资料

Autofs
https://help.ubuntu.com/community/Autofs

autofs - how it works
https://www.kernel.org/doc/html/latest/filesystems/autofs.html

Mount NFS filesystems with autofs
https://www.redhat.com/sysadmin/mount-nfs-filesystems-autofs
