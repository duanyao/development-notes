## 参考资料
[1.1] How to Use the ss Command on Linux
https://www.howtogeek.com/681468/how-to-use-the-ss-command-on-linux/

[1.2] ss command: Display Linux TCP / UDP Network/Socket Information
https://www.cyberciti.biz/tips/linux-investigate-sockets-network-connections.html

[1.3] man ss - another utility to investigate sockets
https://manpages.debian.org/buster/iproute2/ss.8.en.html

[2.1] Checking the Number of Open HTTP Connections With Netstat
https://www.baeldung.com/linux/netstat-count-open-http-connections


## ss
ss (包名 iproute2)，是 netstat 的一个现代版本。

字段解释[1.1]：

Netid: The type of socket. In our example, we have "u_str," a Unix stream, a "udp," and "icmp6," an IP version 6 ICMP socket. You can find more descriptions of Linux socket types in the Linux man pages.
State: The state the socket is in.
Recv-Q: The number of received packets.
Send-Q: The number of sent packets.
Local Address:Port: The local address and port (or equivalent values for Unix sockets).
Peer Address:Port: The remote address and port (or equivalent values for Unix sockets).

状态（State）的解释[1.1]：

LISTEN: Server-side only. The socket is waiting for a connection request.
SYN-SENT: Client-side only. This socket has made a connection request and is waiting to see if it's accepted.
SYN-RECEIVED: Server-side only. This socket is waiting for a connection acknowledgment after accepting a connection request.
ESTABLISHED: Server and clients. A working connection has been established between the server and the client, allowing data to be transferred between the two.
FIN-WAIT-1: Server and clients. This socket is awaiting a connection termination request from the remote socket, or an acknowledgment of a connection termination request that was previously sent from this socket.
FIN-WAIT-2: Server and clients. This socket is awaiting a connection termination request from the remote socket.
CLOSE-WAIT: Server and client. This socket is awaiting a connection termination request from the local user.
CLOSING: Server and clients. This socket is awaiting a connection termination request acknowledgment from the remote socket.
LAST-ACK: Server and client. This socket is awaiting an acknowledgment of the connection termination request it sent to the remote socket.
TIME-WAIT: Server and clients. This socket sent an acknowledgment to the remote socket to let it know it received the remote socket's termination request. It's now waiting to make sure that acknowledgment was received.
CLOSED: There is no connection, so the socket has been terminated

根据 socket 类型选择：
```
ss -a -t -4
```
-a: client socket 和 server socket 。默认只显示 client socket 。
-l: (listening) server socket，即在监听端口的 socket。
-t: TCP 协议。
-u: UDP 协议。
-x: unix socket 。
-w: raw socket 。
-4: IPv4 。
-6: IPv6 。

根据 socket 状态选择：
```
ss -r state established
ss -r state listening
```
状态选择词为 [1.2]：
established
syn-sent
syn-recv
fin-wait-1
fin-wait-2
time-wait
closed
close-wait
last-ack
listen
closing
all : All of the above states
connected : All the states except for listen and closed
synchronized : All the connected states except for syn-sent
bucket : Show states, which are maintained as minisockets, i.e. time-wait and syn-recv.
big : Opposite to bucket state.

地址、端口名称：ss 默认会适用一些文字名，例如 localhost。用 `-r` 参数可以用数字形式显示地址、端口。

根据端口号选择：
```
ss -a  '( dport = :22 or sport = :22 )'
ss \( sport = :http or sport = :https \)
```
使用单引号或者 '\' 来传递 '('。

dport 目标端口，sport 源端口。

根据地址选择：
```
ss -a dst 192.168.4.25
ss -a src 127.0.0.1

ss dst 192.168.1.5:http
ss dst 192.168.1.5:443
```
dst：目标地址，src 源地址。

显示相关进程(-p)：
```
sudo ss -t -p
```

汇总的连接数据（ -s: summary）：
```
ss -s

Total: 12
TCP:   31 (estab 3, closed 23, orphaned 0, timewait 0)

Transport Total     IP        IPv6
RAW       0         0         0        
UDP       2         2         0        
TCP       8         1         7        
INET      10        3         7        
FRAG      0         0         0
```

显示计时器（timer, -o）:

```
ss -o -t
State                     Recv-Q                Send-Q                                 Local Address:Port                                     Peer Address:Port                      Process                                       
ESTAB                     0                     0                                          127.0.0.1:50196                                       127.0.0.1:38571                                                                   
ESTAB                     0                     0                                       192.168.8.97:39784                                   42.236.86.218:https                      timer:(keepalive,36sec,0)                    
ESTAB                     0                     0                                       192.168.8.97:33796                                   36.138.103.14:ssh                        timer:(keepalive,44min,0)
```

计时器的解释：
```
    Show timer information. For tcp protocol, the output format is:

timer:(<timer_name>,<expire_time>,<retrans>)

<timer_name>
    the name of the timer, there are five kind of timer names:

on: means one of these timers: tcp retrans timer, tcp early retrans timer and tail loss probe timer

keepalive: tcp keep alive timer

timewait: timewait stage timer

persist: zero window probe timer

unknown: none of the above timers

<expire_time>
    how long time the timer will expire
<retrans>
    how many times the retran occurs
```


显示扩展信息（ extended, -e ）：
```
ss  -t -e
State                            Recv-Q                       Send-Q                                             Local Address:Port                                                  Peer Address:Port                             Process                                                                                                                                                                                                                                                                                  
ESTAB                            0                            0                                                   192.168.8.97:33796                                                36.138.103.14:ssh                               timer:(keepalive,41min,0) uid:1000 ino:11421692 sk:786 cgroup:/user.slice/user-1000.slice/user@1000.service/app.slice/app-DDE-deepin\x2dterminal@2ed06c8d559340b8bf90f464032b654c.service <-> 
```

## netstat

netstat (包名 net-tools)。

```
netstat -npt

Active Internet connections (w/o servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 127.0.0.1:6379          127.0.0.1:55828         ESTABLISHED 3432/./redis-server 
tcp        0      1 192.168.0.24:22         142.93.184.4:51140      LAST_ACK    -                   
tcp        0      0 192.168.0.24:22         124.64.23.139:52002     ESTABLISHED 1278106/sshd: root@ 
tcp        0      0 192.168.0.24:59282      192.168.0.26:22         ESTABLISHED 1284462/ssh
tcp        0      0 172.17.0.2:9100         192.168.0.32:52060      TIME_WAIT   -                   
tcp        0      0 172.17.0.2:59866        192.168.0.32:80         TIME_WAIT   -
```

字段解释[2.1]：

Proto: This tells us the protocol of the connection. Usually, the protocol is either TCP or UDP. However, there are other sockets that we use in the network stack.
Recv-Q: This indicates the bytes in the queue for that socket, which it needs to read.
Send-Q: The count of bytes not acknowledged by the remote host. If both the Recv-Q and Send-Q are at 0, this means that the applications on both sides of the connection and the network between them are okay.
Local Address: The address and the port number of the local end of the socket.
Foreign Address: The address and the port number of the remote end of the socket.
State: The state of the local socket. There are several states for a socket, including:
    ESTABLISHED: A working connection has been established between the two endpoints, allowing data to be transferred.
    SYN-SENT: This socket has made a connection request and is waiting for the remote host to accept.
    CLOSING: The socket is waiting for a termination connection request acknowledgment from the remote connection.
    TIME_WAIT (timewait)：正在关闭的连接。具体来说，本机已经发送了关闭的包，等待对端回复确认关闭的包。

根据 socket 类型选择：
-a （全部），也包括在监听的 server socket。
-l （listen），在监听的 server socket。
-t: TCP 协议。
-u: UDP 协议。
-x: unix socket 。
-w: raw socket 。
-4: IPv4 。
-6: IPv6 。

数字形式显示地址、端口：`-n` 参数可以用。不使用这个参数，可能导致 netstat 很慢。

## docker 容器中使用 netstat 和 ss

注意，netstat 和 ss 显示的连接信息不包括容器的（docker），即使容器映射端口到宿主机，且连接来自宿主机之外。
要显示容器相关的连接，要在容器内执行 netstat 和 ss，例如：`docker exec e5db9a01d4a8 netstat |grep ESTABLISHED`。
