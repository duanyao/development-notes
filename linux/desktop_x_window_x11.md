[1] X Application Software Engineering: Debugging X Input Events
  http://www.rahul.net/kenton/events.html

[2] How to trace the X window events being generated on Linux?
  https://superuser.com/questions/490606/how-to-trace-the-x-window-events-being-generated-on-linux

[3] xwininfo - window information utility for X 
  http://www.xfree86.org/current/xwininfo.1.html

[4] xev - print contents of X events
  https://www.x.org/releases/X11R7.7/doc/man/man1/xev.1.xhtml

[5] A Brief intro to X11 Programming
  http://math.msu.su/%7Evvb/2course/Borisenko/CppProjects/GWindow/xintro.html#compile
  
[7] evdev
  https://en.wikipedia.org/wiki/Evdev
  
[8] Linux input ecosystem
  https://www.joeshaw.org/linux-input-ecosystem/

[9] Peter Hutterer X input hacker.
  http://who-t.blogspot.com/
  
[10] evtest is dead. long live evemu 
  http://who-t.blogspot.com/2013/11/evtest-is-dead-long-live-evemu.html
  
[11] Xmon X protocol monitor
  https://sourceforge.net/p/xmon/code/
  
[12] xnee http://savannah.gnu.org/projects/xnee/

[13] Testing Applications with Xnee
  https://www.linuxjournal.com/article/6660
  
[14] Introduction to GNU Xnee
  https://xnee.wordpress.com/
  
[15] Xnee Manual
  https://xnee.files.wordpress.com/2012/10/xnee1.pdf

[5.2] Xvfb & Docker - cannot open display
https://stackoverflow.com/questions/32151043/xvfb-docker-cannot-open-display

[5.3] How To Run Your Tests Headlessly with Xvfb
http://elementalselenium.com/tips/38-headless

[6.1] How can I enable Ctrl+Alt+Backspace?
https://askubuntu.com/questions/10622/how-can-i-enable-ctrlaltbackspace

## 事件流  
evdev (short for 'event device') is a generic input event interface in the Linux kernel and FreeBSD.[1] It generalizes raw input events from device drivers and makes them available through character devices in the /dev/input/ directory. [7] 

For Weston/Wayland, the stack would look like this:

    kernel → libevdev → libinput → Wayland compositor → Wayland client

Since version 1.16 the xorg-xserver obtained support for libinput:

    kernel → libevdev → libinput → xf86-input-libinput → X server → X client

## 调试技术

### 查看输入设备事件：evtest 和 evemu

evtest is a tool that prints out a human-readable description of an evdev kernel device and its events. evemu does the same, but it also records the events in a format that can be parsed and re-played easily on another machine, making it possible to reproduce bugs easily. [10]

两者分别包含在 evtest 和 evemu-tools 包里。

用法：

```
$> sudo evemu-record
Available devices:
/dev/input/event0: Lid Switch
/dev/input/event1: Sleep Button
/dev/input/event2: Power Button
/dev/input/event3: AT Translated Set 2 keyboard
/dev/input/event4: SynPS/2 Synaptics TouchPad
/dev/input/event5: PIXART USB OPTICAL MOUSE
/dev/input/event6: Microsoft Microsoft® Digital Media Keyboard
/dev/input/event7: Video Bus
/dev/input/event8: TPPS/2 IBM TrackPoint
/dev/input/event9: Wacom ISDv4 E6 Pen
/dev/input/event10: Wacom ISDv4 E6 Finger
/dev/input/event11: Microsoft Microsoft® Digital Media Keyboard
/dev/input/event12: Integrated Camera
/dev/input/event13: ThinkPad Extra Buttons
/dev/input/event14: HDA Intel PCH HDMI/DP,pcm=8
/dev/input/event15: HDA Intel PCH HDMI/DP,pcm=7
/dev/input/event16: HDA Intel PCH HDMI/DP,pcm=3
Select the device event number [0-16]:
```

选择一个输入设备后，就可以显示其产生的所有输入事件。

### xnee
GNU Xnee is a suite of programs that can record, replay and distribute
user actions under the X11 environment.

sudo apt install xnee gnee

### 查询窗口信息
在终端模拟器执行命令 xwininfo [3]，会提示用鼠标点击一个窗口，点击之后，它就会显示窗口信息，如：

```
xwininfo: Window id: 0x4e00003 "XWININFO(1) manual page - Mozilla Firefox"

  Absolute upper-left X:  0
  Absolute upper-left Y:  50
  Relative upper-left X:  0
  Relative upper-left Y:  0
  Width: 1920
  Height: 976
  Depth: 24
  Visual: 0x100
  Visual Class: TrueColor
  Border width: 0
  Class: InputOutput
  Colormap: 0x4e00002 (not installed)
  Bit Gravity State: NorthWestGravity
  Window Gravity State: NorthWestGravity
  Backing Store State: NotUseful
  Save Under State: no
  Map State: IsViewable
  Override Redirect State: no
  Corners:  +0+50  -0+50  -0-54  +0-54
  -geometry 1920x976+0+0

```

其中 Window id 是窗口id，可以传递给 xev 的 id 参数。

### xev
xev [4] 包含在 x11-utils 包里面。
xev 可能会漏掉部分事件。

运行（其中 id 参数可以用 xwininfo 获得）：
```
xev -id 0x4e00003
```

鼠标按下、移动、释放：

```
ButtonRelease event, serial 40, synthetic NO, window 0xb800001,
    root 0x159, subw 0x0, time 300847279, (94,87), root:(95,987),
    state 0x110, button 1, same_screen YES

MotionNotify event, serial 40, synthetic NO, window 0xb800001,
    root 0x159, subw 0x0, time 300848143, (95,86), root:(96,986),
    state 0x10, is_hint 0, same_screen YES

MotionNotify event, serial 40, synthetic NO, window 0xb800001,
    root 0x159, subw 0x0, time 300848163, (95,86), root:(96,986),
    state 0x10, is_hint 0, same_screen YES

```

不过，xev 对很多程序，如 kate、firefox 都不能捕捉到全部的鼠标事件。

### xtrace
在包 xtrace 里。



### xscope
pseudo-server event watchers

### xmon
pseudo-server event watchers

代码在 [11]。

```
sudo apt install cvs 

sudo apt install xutils-dev libxaw7-dev
```

在源码目录下：
```
xmkmf  # 生成 Makefile, xutils-dev 中
make 
```

## 高分辨率显示器，分数缩放，多显示器

https://wiki.archlinux.org/index.php/HiDPI

https://ricostacruz.com/til/fractional-scaling-on-xorg-linux

xrandr examples
http://jouyouyun.github.io/Blog/xrandr-examples/

## 无头（headless） X 服务器
[5.2, 5.3]
如果有需要在没有物理图形设备的机器上运行图形界面程序，可以使用无头（headless） X 服务器。典型的是 Xvfb 。

```
Xvfb :99 &
env DISPLAY=:99 firefox
```
Xvfb 是 Xorg 自带的无头X服务器，它启动后会一直运行，所以可以放到后台去。
启动其它 GUI 程序时，应确保 DISPLAY 环境变量设置为启动 Xvfb 时指定的参数 - display port。

另一个办法是用 xvfb-run:
```
xvfb-run firefox
```
这也会同时启动 Xvfb 和 firefox。

如果在同一操作系统中运行多个 Xvfb 实例，则 display port 不能重复，否则会冲突。xvfb-run 会自动选择端口。

## 启用 Ctrl+Alt+Backspace 来重启 X 。

编辑 /etc/default/keyboard 增加一行 `XKBPTIONS="terminate:ctrl_alt_bksp"` 。
或者运行 sudo dpkg-reconfigure keyboard-configuration ，在其中选择启用 Ctrl+Alt+Backspace 。
两者是等效的。

gnome 的设置是（也可以用 dconf 编辑）：

```
gsettings set org.gnome.desktop.input-sources xkb-options "['terminate:ctrl_alt_bksp']"
```
deepin 的设置（疑似）：
```
gsettings set com.deepin.wrap.gnome.desktop.input-sources xkb-options "['terminate:ctrl_alt_bksp']"
```

但是，以上方法在 deepin 上都不生效。
