## 参考资料
[1] https://wiki.winehq.org/MIDI
[2] Timidity https://wiki.archlinux.org/index.php/Timidity#Daemon_mode
[3] 如何以nobody用户执行命令？ http://www.cnblogs.com/xiaoming279/p/6115167.html
[4] Programming and Using Linux Sound - in depth - MIDI ALSA  https://blog.csdn.net/zgyrelax/article/details/17386145
[5] http://linux-audio.com/TiMidity-howto.html
[6] Ted's Linux MIDI Guide http://www.tedfelix.com/linux/linux-midi.html

## 背景
很多 linux 发行版默认并没有配置好 midi 功能，这样一些依赖 midi 的程序，例如 windows 游戏，就无法正常发声了[1]。
因此，首先需要安装某个软件 midi 合成器，例如 timidity 。

## timidity
安装：
```
sudo apt install timidity
```
运行：用当前桌面用户执行：

```
timidity -iA -B2,8 -Os -EFreverb=0
```
或者
```
timidity -Os -iA
```

用 `aconnect -o` 检查，如果成功，输出

```
client 14: 'Midi Through' [type=kernel]
    0 'Midi Through Port-0'
client 128: 'TiMidity' [type=user,pid=6174]
    0 'TiMidity port 0 '
    1 'TiMidity port 1 '
    2 'TiMidity port 2 '
    3 'TiMidity port 3 '

```
在运行 timidity 之前，则只会有 

```
client 14: 'Midi Through' [type=kernel]
    0 'Midi Through Port-0'
```
这个设备是不会发声的。

14 和 128 都是用来标识输出设备的。

用 aplaymidi 来测试是否能播放：

```
aplaymidi filename.mid --port 128:0
#或者
aplaymidi -p128:0 filename.mid
```

`128:0` 对应 `client 128 ... port 0`，改为 `128:1` 也可以，但是 `128:2` 和 `128:3` 就不一定有声音了。

在运行 timidity 的时候，运行使用 midi 的程序（包括 wine 的程序），应该能发声。

### timidity 服务化
timidity 自带一个系统服务，`/etc/init.d/timidity`，但是在 deepin 上似乎有些 bug：

```
sudo journalctl -u timidity
3月 16 16:46:12 duanyao-laptop systemd[1]: Starting LSB: start and stop timidity...
3月 16 16:46:34 duanyao-laptop timidity[1964]: Starting TiMidity++ ALSA midi emulation....
3月 16 16:46:34 duanyao-laptop systemd[1]: Started LSB: start and stop timidity.
3月 16 16:46:36 duanyao-laptop pulseaudio[6195]: [autospawn] core-util.c: Home directory not accessible: 权限不够
3月 16 16:46:36 duanyao-laptop pulseaudio[6195]: [autospawn] lock-autospawn.c: 不能访问自动执行锁。
3月 16 16:46:36 duanyao-laptop pulseaudio[6195]: [pulseaudio] main.c: Failed to acquire autospawn lock
3月 16 22:29:49 duanyao-laptop systemd[1]: Stopping LSB: start and stop timidity...
3月 16 22:29:49 duanyao-laptop timidity[15765]: Stopping TiMidity++ ALSA midi emulation....
3月 16 22:29:49 duanyao-laptop systemd[1]: Stopped LSB: start and stop timidity.

```
虽然 aplaymidi 可以播放，但一些 wine 的 windows 程序不行。原因可能在于 timidity 系统服务是用 timidity:timidity(123:134) 这个特殊用户运行的，而这会导致一些与 pulseaudio 相关的错误：

```
sudo chroot --userspec=123:134 / /usr/bin/timidity -Os -iA
No protocol specified
xcb_connection_has_error() returned true
Home directory not accessible: Permission denied
W: [pulseaudio] core-util.c: Failed to open configuration file '/root/.config/pulse//daemon.conf': 权限不够
W: [pulseaudio] daemon-conf.c: 打开配置文件失败：权限不够
Requested buffer size 32768, fragment size 8192
ALSA pcm 'default' set buffer size 30104, period size 3760 bytes
TiMidity starting in ALSA server mode
Opening sequencer port: 128:0 128:1 128:2 128:3
ALSA lib pcm_dmix.c:1099:(snd_pcm_dmix_open) unable to open slave
Can't open pcm device 'default'.
Couldn't open ALSA pcm device (`s')
```

所以我们禁用自带的服务，创建一个用户服务：

```
sudo systemctl disable --now timidity
timidity.service is not a native service, redirecting to systemd-sysv-install.
Executing: /lib/systemd/systemd-sysv-install disable timidity
insserv: warning: current start runlevel(s) (empty) of script `timidity' overrides LSB defaults (2 3 4 5).
insserv: warning: current stop runlevel(s) (0 1 2 3 4 5 6) of script `timidity' overrides LSB defaults (0 1 6).
```

创建文件 /etc/systemd/user/timidity.service

```
[Unit]
Description=TiMidity Daemon
After=sound.target

[Service]
ExecStart=/usr/bin/timidity -iA -Os
Restart=always

[Install]
WantedBy=default.target
```

启动、设为自启动：
```
systemctl --user enable --now timidity.service
Created symlink /home/duanyao/.config/systemd/user/default.target.wants/timidity.service → /etc/systemd/user/timidity.service.
```

查看日志：

```
$ journalctl --user -u timidity
-- Logs begin at Thu 2019-02-28 10:27:11 CST, end at Sat 2019-03-16 23:29:24 CST. --
3月 16 23:23:09 duanyao-laptop systemd[7384]: Started TiMidity Daemon.
3月 16 23:23:09 duanyao-laptop timidity[22828]: Requested buffer size 32768, fragment size 8192
3月 16 23:23:09 duanyao-laptop timidity[22828]: ALSA pcm 'default' set buffer size 32768, period size 8192 bytes
```

## 错误

### /etc/timidity/fluidr3_gm.cfg
```
$ systemctl --user status timidity

10月 19 10:43:08 duanyao-laptop-c systemd[3159]: Started TiMidity Daemon.
10月 19 10:43:08 duanyao-laptop-c timidity[32215]: /etc/timidity/fluidr3_gm.cfg: No such file or directory
10月 19 10:43:08 duanyao-laptop-c timidity[32215]: timidity: Error reading configuration file.
10月 19 10:43:08 duanyao-laptop-c timidity[32215]: Please check /etc/timidity/timidity.cfg
10月 19 10:43:08 duanyao-laptop-c systemd[3159]: timidity.service: Main process exited, code=exited, status=1/FAILURE
10月 19 10:43:08 duanyao-laptop-c systemd[3159]: timidity.service: Failed with result 'exit-code'.

$ ls /etc/timidity/
fluidr3_gs.cfg  freepats.cfg  timidity.cfg
```
fluidr3_gs.cfg  freepats.cfg 都属于 fluid-soundfont-gs 包，fluidr3_gm.cfg 则属于 fluid-soundfont-gm，所以解决办法是安装后者：
```
sudo apt install fluid-soundfont-gm
```
