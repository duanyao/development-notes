## 参考资料

[1.0] network namespaces
[1.1] man-pages network namespaces
https://www.man7.org/linux/man-pages/man7/network_namespaces.7.html

[1.2] Network Namespace
https://zhaohuabing.com/post/2020-03-12-linux-network-virtualization/

[1.3] network namespace 的基本概念和用法
https://www.cnblogs.com/bakari/p/10443484.html

[1.4] Linux ip netns 命令
https://www.cnblogs.com/sparkdev/p/9253409.html

[2.1] linux namespace, How to connect internet in network namespace?
https://unix.stackexchange.com/questions/156847/linux-namespace-how-to-connect-internet-in-network-namespace

## network namespaces 基本概念
[1.1-1.3]
Network namespaces provide isolation of the system resources
associated with networking: network devices, IPv4 and IPv6
protocol stacks, IP routing tables, firewall rules, the /proc/net
directory (which is a symbolic link to /proc/PID/net), the
/sys/class/net directory, various files under /proc/sys/net, port
numbers (sockets), and so on. 

Network namespace 在逻辑上是网络堆栈的一个副本，它有自己的路由、防火墙规则和网络设备。

一个网卡，不论是物理的还是虚拟的，只能属于一个 NNS 。 

为了沟通两个 NNS ，可以使用 veth pair ，这是互相连通的一对虚拟网卡，每个网卡分别位于一个 NNS 中。

为了沟通两个以上的 NNS ，可以使用 bridge ，这相当于虚拟交换机。将一对 veth pair 的一个连接在 bridge 上，另一端加入一个 NNS 。
bridge 是工作在二层上的，它和它直接连接的 veth 都不需要 IP 地址[1.2]。

为了从一个 NNS 中访问外网，可以有几种方法[2.1]：
* NAT
* IP 路由
* 以太网桥

## 操作命令
[1.4]
```
#create netns
ip netns add myNamespace
#link iface to netns
ip link set eth0 netns myNamespace
#set ip address in namespace
ip netns exec myNamespace ifconfig eth0 192.168.0.10/24 up
#set loopback (may be needed by process run in this namespace)
ip netns exec myNamespace ifconfig lo 127.0.0.1/8 up
#set route in namespace
ip netns exec myNamespace route add default gw 192.168.0.1
#force firefox to run inside namespace (using eth0 as outgoing interface and the route)
ip netns exec myNamespace firefox
```

```
[root@recorder-3 ~]# ip netns add nns1
[root@recorder-3 ~]# ip netns list
nns1 (id: 0)
[root@recorder-3 ~]# ls /var/run/netns
nns1
[root@recorder-3 ~]# ip link set eth1 netns nns1
[root@recorder-3 ~]# ip netns exec nns1 ifconfig eth1 172.17.191.219/20 up
[root@recorder-3 ~]# ip netns exec nns1 ifconfig lo 127.0.0.1/8 up
[root@recorder-3 ~]# ip netns exec nns1 route add default gw 172.17.191.253
[root@recorder-3 ~]# ip netns exec nns1 route -n
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         172.17.191.253  0.0.0.0         UG    0      0        0 eth1
172.17.176.0    0.0.0.0         255.255.240.0   U     0      0        0 eth1

[root@recorder-3 ~]# ip netns exec nns1 ip route list
default via 172.17.191.253 dev eth1 
172.17.176.0/20 dev eth1 proto kernel scope link src 172.17.191.219

[root@recorder-3 ~]# ip netns exec nns1 ip route get 8.8.8.8
8.8.8.8 via 172.17.191.253 dev eth1 src 172.17.191.219 uid 0 
    cache 
[root@recorder-3 ~]# ip netns exec nns1 ping 114.114.114.114
PING 114.114.114.114 (114.114.114.114) 56(84) bytes of data.
64 bytes from 114.114.114.114: icmp_seq=1 ttl=88 time=30.2 ms

[root@recorder-3 ~]# ip netns exec nns1 ifconfig
eth1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 172.17.191.219  netmask 255.255.240.0  broadcast 172.17.191.255
        inet6 fe80::216:3eff:fe32:8a52  prefixlen 64  scopeid 0x20<link>

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0

[root@recorder-3 ~]# route -n
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         172.17.191.253  0.0.0.0         UG    100    0        0 eth0
172.17.176.0    0.0.0.0         255.255.240.0   U     100    0        0 eth0

[root@recorder-3 ~]# ifconfig
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 172.17.178.27  netmask 255.255.240.0  broadcast 172.17.191.255
lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
```

删除命名的 network namespace

`ip [-all] netns del [ NAME ]` 命令删除指定名称的 network namespace。如果指定了 -all 选项，则尝试删除所有的 network namespace。

删除 nns 时，如果有进程还在使用它，则直到进程退出继续有效。

查看进程的 network namespace

ip netns identify [PID] 命令用来查看进程的 network namespace

查看 network namespace 中进程的 PID

ip netns pids NAME 命令用来查看指定的 network namespace 中的进程的 PID。
