https://stackoverflow.com/questions/47838800/etc-lsb-release-vs-etc-os-release

The /etc/lsb-release file is a file that some, but not all, Linux distributions put there for older programs to use. The "lsb" refers to the Linux Standard Base, a project working to define a common set of standards for any Linux distribution to follow, including things like filesystem layout. However, that file, /etc/lsb-release, isn't part of the standard. It's an extra thing that some distributions use, but not all.

The /etc/os-release file is the standard, however. Any distribution based on systemd, including Red Hat Enterprise Linux, CentOS, Fedora, Gentoo, Debian, Mint, Ubuntu, and many others, is required to have that file. Distributions that don't use systemd may also have the file.

If you need a reliable way of detecting what distribution you're running on, your best bet will be to read the /etc/os-release file. If it's not there you can try running the program called lsb_release. But just ignore the /etc/lsb-release file.

You can read more about os-release here and here. And just for fun, take a look at all the different files that Linux distributions used to use!

/etc/os-release -> ../usr/lib/os-release

os-release 例子：

```
NAME="Ubuntu"
VERSION="16.04.6 LTS (Xenial Xerus)"
ID=ubuntu
ID_LIKE=debian
PRETTY_NAME="Ubuntu 16.04.6 LTS"
VERSION_ID="16.04"
HOME_URL="http://www.ubuntu.com/"
SUPPORT_URL="http://help.ubuntu.com/"
BUG_REPORT_URL="http://bugs.launchpad.net/ubuntu/"
VERSION_CODENAME=xenial
UBUNTU_CODENAME=xenial
```

变更、复原：

```
sudo ln -sf /etc/os-release-ubuntu-1604 /etc/os-release
sudo ln -sf /usr/lib/os-release /etc/os-release
```

/etc/lsb-release 例子：
```
DISTRIB_ID=Ubuntu
DISTRIB_RELEASE=16.04
DISTRIB_CODENAME=xenial
DISTRIB_DESCRIPTION="Ubuntu 16.04.6 LTS"
```

```
sudo mv /etc/lsb-release /etc/lsb-release-origin
sudo dedit /etc/lsb-release-ubuntu-1604
```

变更、复原：
```
sudo ln -sf /etc/lsb-release-ubuntu-1604 /etc/lsb-release
sudo ln -sf etc/lsb-release-origin /etc/lsb-release
```
