## 参考资料

[2.1] Tmux 使用教程
https://www.ruanyifeng.com/blog/2019/10/tmux.html

[2.2] tmux命令_Linux tmux命令：一个窗口操作多个会话
http://c.biancheng.net/linux/tmux.html

[3.1] tmux in practice: scrollback buffer
  https://www.freecodecamp.org/news/tmux-in-practice-scrollback-buffer-47d5ffa71c93/
  
[3.2] How to scroll back in Tmux
  https://www.runrails.com/tmux/scrolling-in-tmux/

  
## 入门操作
tmux # 创建新窗口+会话

按下Ctrl+d或者显式输入exit命令，就可以退出 Tmux 窗口，同时也终结会话（但其中的nohup进程不会终结，会变成 pid 1 的子进程）。

tmux attach -t 0 # 打开已经存在的窗口。可以简写 tmux at -t 0

tmux kill-window -t :1
tmux list-sessions
tmux list-windows
tmux new -s <session-name> # 新建一个指定名称的会话
Ctrl+b d 或者 tmux detach # 退出当前 Tmux 窗口，但是会话和里面的进程仍然在后台运行。

tmux new-window [-n window-name] [-t target-window]
select-window [-lnpT] [-t target-window]

## 窗口和会话
一般来说，窗口和会话是一一关联的。但是，也可以将其分离，让会话在没有窗口的情况下运行。

## 快捷键

Tmux 窗口有大量的快捷键。所有快捷键都要通过前缀键唤起。默认的前缀键是Ctrl+b，即先按下Ctrl+b，快捷键才会生效。
例如，在 Tmux 窗口中，先按下Ctrl+b，再按下?，就会显示帮助信息。

## 滚动

Ctrl-b then [ to enter copy mode, use Down/Up arrows or PageDown and PageUp keys, q or Enter to exit copy mode [3.1].

## 拆分窗格
```
# 划分上下两个窗格
$ tmux split-window

# 划分左右两个窗格
$ tmux split-window -h
```
在窗格之间移动焦点：`ctrl+b; <方向键>`，注意，无法连续按方向键移动，要按一次 `ctrl+b` 再按一次方向键。
