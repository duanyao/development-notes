## 参考资料

[2] Chapter 24: Process Creation - The Linux Programming Interface
  http://man7.org/tlpi/download/TLPI-24-Process_Creation.pdf

[3] Process identifier
  https://en.wikipedia.org/wiki/Process_identifier

[4] How to change the kernel max PID number?
https://unix.stackexchange.com/questions/162104/how-to-change-the-kernel-max-pid-number
[4.2] What is the maximum value of the Process ID?
https://unix.stackexchange.com/questions/16883/what-is-the-maximum-value-of-the-process-id

[5] Reduce PID_MAX Safely
https://serverfault.com/questions/648287/reduce-pid-max-safely

[7] Unix signal
https://en.wikipedia.org/wiki/Unix_signal

[8] 关于SIGPIPE信号
http://blog.sina.com.cn/s/blog_502d765f0100kopn.html

[9] Linux Programmer's Manual PIPE(7)
http://man7.org/linux/man-pages/man7/pipe.7.html

[10] write(2) - Linux man page
https://linux.die.net/man/2/write

[11] Which process has PID 0?
  https://unix.stackexchange.com/questions/83322/which-process-has-pid-0

[13] Standard streams
  https://en.wikipedia.org/wiki/Standard_streams

[14] The difference between stdout and STDOUT_FILENO in LINUX C
http://stackoverflow.com/questions/12902627/the-difference-between-stdout-and-stdout-fileno-in-linux-c

[15] Capture the output of a child process in C
Capture the output of a child process in C

[16] What are the default stdin and stdout of a child process?
https://unix.stackexchange.com/questions/278105/what-are-the-default-stdin-and-stdout-of-a-child-process

[17] How to redirect the output back to the screen after freopen(“out.txt”, “a”, stdout)
http://stackoverflow.com/questions/1908687/how-to-redirect-the-output-back-to-the-screen-after-freopenout-txt-a-stdo

[18]
https://superuser.com/questions/539920/cant-kill-a-sleeping-process

[19] TASK_KILLABLE
https://lwn.net/Articles/288056/

[21] Linux - Threads and Process
http://stackoverflow.com/questions/9305992/linux-threads-and-process

[22] Threads and fork(): think twice before mixing them.
http://www.linuxprogrammingblog.com/threads-and-fork-think-twice-before-using-them

[23] What does the FD_CLOEXEC fcntl() flag do?
http://stackoverflow.com/questions/6125068/what-does-the-fd-cloexec-fcntl-flag-do

[30] Child Parent Relationship and Inheritance in C
http://stackoverflow.com/questions/5021296/child-parent-relationship-and-inheritance-in-c

[31] Orphan_process
https://en.wikipedia.org/wiki/Orphan_process

[32] Daemon
https://en.wikipedia.org/wiki/Daemon_(computing)

[33] What is a “subreaper” process?
https://unix.stackexchange.com/questions/250153/what-is-a-subreaper-process

[34] prctl: add PR_{SET,GET}_CHILD_SUBREAPER to allow simple process supervision
https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=ebec18a6d3aa1e7d84aab16225e87fd25170ec2b

[35] Process group
https://en.wikipedia.org/wiki/Process_group

[36] How do I run a Unix process in the background?
https://kb.iu.edu/d/afnz

[37] Creating a Daemon Process in C Language with an Example Program
http://www.thegeekstuff.com/2012/02/c-daemon-process/

[38] How to make child process die after parent exits?
http://stackoverflow.com/questions/284325/how-to-make-child-process-die-after-parent-exits

[39] unstoppable high cpu zombie processes
https://github.com/moby/moby/issues/19166

[40] Zombie process using 100% CPU
https://superuser.com/questions/388565/zombie-process-using-100-cpu

[41] Signal Handling - glibc
https://www.gnu.org/software/libc/manual/html_node/Signal-Handling.html

[42] 24.4.1 Signal Handlers that Return
https://www.gnu.org/software/libc/manual/html_node/Handler-Returns.html

[43] 24.4.2 Handlers That Terminate the Process
https://www.gnu.org/software/libc/manual/html_node/Termination-in-Handler.html

[44] 24.4.6 Signal Handling and Nonreentrant Functions
https://www.gnu.org/software/libc/manual/html_node/Nonreentrancy.html

[45] 24.7 Blocking Signals
https://www.gnu.org/software/libc/manual/html_node/Blocking-Signals.html

[46] Reentrancy (computing)
https://en.wikipedia.org/wiki/Reentrancy_(computing)

[47] C signal handling
https://en.wikipedia.org/wiki/C_signal_handling

[48] Unix signal
https://en.wikipedia.org/wiki/Unix_signal

[49]
https://unix.stackexchange.com/questions/85364/how-can-i-check-what-signals-a-process-is-listening-to

[50]
https://unix.stackexchange.com/questions/37915/why-do-i-get-error-255-when-returning-1

[51] capabilities(7) - Linux man page
https://linux.die.net/man/7/capabilities

[52] change /proc/PID/environ after process start
https://unix.stackexchange.com/questions/302948/change-proc-pid-environ-after-process-start

[53] prctl
https://man7.org/linux/man-pages/man2/prctl.2.html

[54] [1/2] c/r: prctl: Add ability to set new mm_struct::exe_file 
https://lore.kernel.org/patchwork/patch/292515/

[55] Checkpoint/Restore Of Unprivileged Processes Sent In For Linux 5.9
https://www.phoronix.com/scan.php?page=news_item&px=Linux-5.9-Checkpoint-Restore

[56] Features/Checkpoint Restore
https://fedoraproject.org/wiki/Features/Checkpoint_Restore

[57] How to join a thread that is hanging on blocking IO?
https://stackoverflow.com/questions/203754/how-to-join-a-thread-that-is-hanging-on-blocking-io

[58] signalfd(2) — Linux manual page
https://man7.org/linux/man-pages/man2/signalfd.2.html

[59] How to view threads of a process on Linux
https://www.xmodulo.com/view-threads-process-linux.html

[60] 宋宝华：可以杀死的深度睡眠TASK_KILLABLE状态(最透彻一篇)
https://cloud.tencent.com/developer/article/1612051

## PID 进程ID
一个系统中所有的进程的ID（包括活动的和僵尸）都是唯一的。

PID的取值范围是非负整数。其最大值并不统一，在 32 位 linux 上一般为 2^15 = 32768，在 64 位 linux 上一般为 2^22 = 4194304 （但一般仍配置为32768），可参考 man 5 proc。
这个最大值在 linux 上可以通过 /proc/sys/kernel/pid_max 或者 sysctl kernel.pid_max 来查询或设置。
kernel.pid_max 的最大允许值为 2^30 ~ 10^9 ，但具体得看内核编译时的设置，许多系统（如 debian 10 64bit）上为 2^22 [4, 4.2] 。

如果一个系统的进程数超过 PID 的最大值，将无法继续新建进程。但是，动态地减小 pid_max，对于已存在的 PID 大于 pid_max 的进程并没有影响。

要获得本进程的 PID，可调用 getppid() 系统调用；在 shell 中则可访问 $$ 变量[3]。

## PID 的分配
已经退出的进程的 PID 可以被重复利用，但不是立即的，大部分 posix 系统会尽量推迟重复利用，以避免新进程被误认为旧进程（例如，先ps，再kill）。
例如linux尽量递增地为新进程分配 PID，直到触及 pid_max 时，会从 0 开始寻找空闲的 PID。

## 特殊 PID
PID 0 和 1 是特殊的，不会分配给普通进程。前者是内核中的调度器（swapper or sched），其实不是普通进程；后者是 init 进程。
init 进程的 PID 是 1 并非技术上的原因，而是因为有很多的程序依赖于这一点，就成了事实标准 [3,11]。

linux中还有一些内核对象以“进程”的形式暴露出来，但没有固定的 PID，例如 kthreadd 等[11]。

## 父/子进程和孤儿进程、僵尸进程

所有的进程都有父进程，即创建该进程的进程，除了 init 进程。
ps afx 可以显示进程树。

父进程如果早于子进程退出，则子进程成为孤儿进程。对孤儿进程的处理，操作系统有不同的做法[31]：

* 内核会自动将孤儿进程的父进程设置为 init 进程（但此时仍然叫孤儿进程）。这是传统 posix 的做法，叫做 re-parenting 或者 adoption。
  有时候，会故意让进程成为孤儿，进而成为 init 的子进程，例如服务进程（daemon）。
* 立即终止孤儿进程。特别地，如果子进程是由一个 shell 进程启动的，则 shell 终止时，子进程也会终止。
* 让除了init之外的某个进程成为父进程。现代linux支持这种做法，这样的父进程叫做 "subreaper"[33]。
  subreaper 的主要用途是实现 systemd 这样的服务管理程序[33,34]。

子进程结束时（调用了 exit()系统调用），并非立即消失，而是先变成僵尸进程（ps 查看会显示为 defunct，状态为 Z，即 zombie），直到父进程查看其退出码（通过 wait/waitpid 系统调用）后才会消失（此时其 PID 才可重用）。这个让僵尸进程消失的操作称为收割（reap）。init 进程总是会正确地收割其子进程。

如果父进程没有正确实现，只是创建子进程而不去查看其退出码，就会产生长期存在的僵尸进程；如果这样的父进程运行时间较长，且反复创建子进程，则可能产生大量的僵尸进程，耗尽系统资源（主要是指PID，而内存等其它资源已经被回收）。
对此没什么好办法，kill 对僵尸进程无效，只能杀死有问题的父进程，让 init 接管进而收割僵尸进程。

init 进程收割僵尸进程是周期性的，即可能不会立即收割。如果要立即收割，可以给 init 进程发送 SIGCHLD 信号：`sudo kill -SIGCHLD 1` 。

僵尸进程不会再消耗 CPU 和内存资源（除了PID）。但由于内核的某些 bug，僵尸进程却可能继续消耗 CPU，而且无法被 init 收割。例如[39,40]，以及在我的机器（4.15.0-30deepin-generic x86_64 内核）上发现的一例：

```
ps -el|grep Z
F S   UID   PID  PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD
0 Z  1000 22703     1 52  80   0 -     0 -      ?        11:27:24 python <defunct>
```

## fork 和子进程继承的资源
在 fork() 之后，子进程会从父进程中继承一些资源。

### 打开的文件描述符[2, 30]

文件描述符关联的文件位置指针、打开属性（open或fcntl设置的）在两个进程间也是共享的，一边修改了另一边也能看到[2]，要注意争用问题。

值得注意的是标准输入/输出/错误这3个文件也是继承的。
这意味着父子进程共享这3个文件，如果并发访问，会有奇怪的事情发生。这通常是不好的。

如果希望让父进程和子进程通过stdio交互，就需要[15,16]
  * 在 fork() 前，在父进程中创建一对 pipe
  * fork()
  * 子进程关闭文件描述符 STDOUT_FILENO，然后重新打开它，并连接到那个 pipe 上（也被继承了），通过 dup2() 。
  * 父进程在那个 pipe 上读取子进程的 stdout。

[15]给出了代码样例。

如果希望简单地丢弃子进程的stdio，可以

        freopen("/dev/null", "r", stdin); // stdin 是 FILE *
        freopen("/dev/null", "w", stdout);
        freopen("/dev/null", "w", stderr);

通过把 /dev/null 换成普通文件，也可以将标准输出重定向到文件。

如果父进程中打开的文件设置了 FD_CLOEXEC 属性（通过 open() 或者 fcntl(fd, F_SETFD, FD_CLOEXEC) ），
则当子进程调用 exec*() 后，这样的文件描述符会被自动关闭 [23]。这是避免不必要的文件描述符共享的措施。

### exec*() 与信号 signal 处理器

如果子进程执行了 exec*()，所有的信号处理器都会被设置为默认行为。

## 其它创建进程的方法

POSIX 还定义了 posix_spawn() 函数，大致上相当于 fork()+exec() [2]。
除了方便，posix_spawn() 对硬件的要求较低，不要求系统具有 MMU，而 fork 要求。

vfork() 是一种比 fork() 更快的创建子进程的方法[2]。
* 子进程不会复制父进程的内存，而是共享父进程的内存。
* 子进程在 vfork() 返回之后不能从调用 vfork() 的函数中返回，必须以调用 exec*() 或 _exit() 为终结。
* 当子进程位于 vfork() 返回之后、exec*() 或 _exit() 调用之前时，父进程被操作系统挂起，以免出现争用。
  当然，因为共享内存，子进程在这期间对内存的修改对父进程都是可见的（最好不要这样做）。
* 子进程会复制父进程的文件描述符。
一般不建议使用 vfork()，因为它的未定义行为较多，已经被UNIX标准废弃。

## 了解父进程/子进程的退出

父进程要得知子进程的退出，可以：
* wait/waitpid

* 接收 SIGCHLD 信号
  当一个子进程终止、中断、或从中断返回的时候发出。

子进程要得知父进程的退出，可以：
* 轮询父进程 PID 的变化[38]。
  有人建议检查父进程是否变成了1，但这不够可靠，因为linux存在 subreaper 机制，收养的父进程不一定是 init；有的操作系统 init 的 PID 也不是 1。
  注意最好在 fork 之前保存父进程的 PID，否则父进程如果在子进程首次检测 PID 前就退出，那就没效果了。

* 要求操作系统在父进程退出时发送信号[38]。
  例如在子进程中调用 prctl(PR_SET_PDEATHSIG, SIGHUP); SIGHUP 也可以换成 SIGTERM，如果只是需要让子进程随着父进程退出。
  注意最好在 fork 之前保存父进程的 PID，在 prctl 之前检查父进程是否已经退出。

两边通用的方法：
* 检测管道上的错误。写端写入数据，再检测 SIGPIPE 信号或者 EPIPE 错误，向对端关闭的管道/socket写数据可能触发 [8]。
  注意，SIGPIPE 的默认行为是终止程序。详见“标准流” 和 pipe.txt 。
  读端可检测 read(2) 是否返回 end-of-file (EOF)。

* 打开一对 Unix domain sockets，检测连接是否失效（POLLHUP）[38]。

## 进程的退出码 exit code / exit status

POSIX 进程的退出码限制在 0-255 范围内，即8位无符号数。

在 C 程序中，exit(n) 或者 main() 函数的返回值的低八位变成无符号数就是退出码。这就是说超范围的状态码是无意义的。

wait/waitpid 可以得到子进程的退出码（通过 WEXITSTATUS(status) 宏）。

shell 中可以通过 `echo $?` 查看上一个退出的命令的退出码。

如果进程是因为未处理的信号而退出，则没有退出码，而 wait/waitpid 可以得到导致进程退出的信号（通过 WTERMSIG(wstatus) 宏）。

如果进程是因为未处理的信号而退出，`echo $?` 仍然会返回一个数字，其值减去 128 是导致退出的信号的值，例如 137 代表 SIGKILL（SIGKILL=9，可用 kill -l 查阅）。
这也意味着，在 POSIX 系统上，为了兼容 shell 环境，退出码最好不要超过 128，以免混淆。

顺便说一句，Windows 进程的退出码具有 32 位的范围。

## 会话 session

## 服务进程（daemon）
posix 系统中，服务进程一般都是 init 的子进程。这可以通过两种方式做到[32]：
* 由 init 直接创建。
* 由其它进程创建，但立即 fork 并退出，这样刚 fork 出来的服务进程就成了 init 的子进程。
  这个技巧叫做 double-forking。更详细地说[37]：

    Create a normal process (Parent process)
    Create a child process from within the above parent process
    The process hierarchy at this stage looks like :  TERMINAL -> PARENT PROCESS -> CHILD PROCESS
    Terminate the the parent process.
    The child process now becomes orphan and is taken over by the init process.
    Call setsid() function to run the process in new session and have a new group.
    After the above step we can say that now this process becomes a daemon process without having a controlling terminal.
    Change the working directory of the daemon process to root and close stdin, stdout and stderr file descriptors.
    Let the main logic of daemon process run.


在 linux 上 double-forking 的一个实现例子见[37]。

在 shell 中，通过给命令后面加上 &，可以让进程在后台运行[36]。
根据[2]，shell与其启动的子进程共享作为标准流的终端。如果子进程是前台的，shell等待（wait()）子进程退出，在此之前不会读写控制台；
如果子进程是后台的，则shell和子进程可以并行地写控制台（但子进程应该不能读？）。

## 标准流
### 标准流的文件描述符

一般的进程都拥有标准输入、输出和错误这3个文件，并自动处于打开状态，统称标准流（Standard stream）[13]。

这3个文件描述符的值是固定的，分别是 STDIN_FILENO(0), STDOUT_FILENO(1), STDERR_FILENO(2)，定义在 unistd.h 中。
此外，libc 的 stdio.h 中还定义了 stdin, stdout, stderr 这3个 FILE* 型的常量，与上述文件描述符指向同样的打开的文件，因此有
STDOUT_FILENO == fileno(stdout) [14]。

fprintf(stdout, "x=%d\n", x) 与 printf("x=%d\n", x) 的行为相同。

### 标准流的重定向和管道

标准流对应的真实文件并不总是不变的。从shell中启动程序时是连接到终端上（/dev/tty），但也可以被重定向到管道、命名管道或者普通文件，或者 /dev/null 这样的特殊文件。

重定向标准流可以通过 dup2(), freopen() 等函数完成，详情请查看man。这些函数在不改变文件描述符（以及FILE结构）的值的情况下，改变其对应的打开的文件。

例如：
        freopen("/dev/null", "w", stdout);

通过把 /dev/null 换成普通文件，也可以将标准输出重定向到普通文件。

重定向标准流后，原来的对应的文件会被关闭，通常无法恢复。如果想恢复，应该事先用 dup() 复制标准流的文件描述符，事后用 dup2() 恢复[17]。

### 标准流上的错误

如果标准流连接到终端，则几乎不可能出错，也不可能遇到 EOF，但重定向以后则不然。

* stdout/stderr 重定向到管道，如果读端的进程提前退出或主动关闭管道，继续写入就会引发 SIGPIPE 信号[9,10]。
  SIGPIPE 默认会导致进程终止。如果捕获了 SIGPIPE，则管道上的 write() 调用会收到 EPIPE 错误码，这时可以相应处理[10]。
  用 aprogram | cat 可以模拟读端的进程提前退出的情形，或者也可以用 mkfifo 。

  不过，如果写端的 stdout 被缓冲了，则 SIGPIPE/EPIPE 可能不会立即发生，例如，printf 输出到控制台时，遇到换行就清空缓冲，但如果输出到文件/管道，就会有更大的缓冲区。
  fflush(stdout) 可以立即清空缓冲。

  顺带一提，node.js 在这种情况下默认捕获 SIGPIPE，并在 process.stdout|stderr 上产生 error 事件（EPIPE），如果不处理这个 error 会造成进程终止。

* 标准流重定向到文件，那么一般的文件写入错误都可能发生。

* stdin 重定向到管道，如果写端关闭了，则继续读取会得到 EOF[9]。

### 管道的缓冲区

管道本身能暂存一定量的数据，其上限称为管道的容量（capacity），默认从4KB（早期linux）到64KB。
容量可以通过 fcntl(2) F_GETPIPE_SZ 和 F_SETPIPE_SZ 来查询和设置。允许设置的最大值为
/proc/sys/fs/pipe-max-size （或者 sysctl fs.pipe-max-size） ，默认 1048576 （1MiB） [9]。

读取缓冲已空的管道，或写入缓冲已满的管道，一般会阻塞，除非采用非阻塞IO[9]。

## 当前目录
要查看其它进程的当前目录，可以通过 /proc/<pid>/cwd，这是个指向当前目录的符号链接。

注意，当前目录与打开的文件一样，都会阻止 umount 一个目录。

## 信号 signal

### 信号的并发性

信号机制发明的时候还没有多线程程序，所以一直以来信号多数是与单线程程序结合使用，信号与多线程程序很多时候并不能结合的很好。

不过，信号实际上让单线程程序有了一些“并发性”，并且会遇到一些并发性的问题。

除了信号处理器以外的正常执行流程我们称为主控制流，并拥有一个调用栈。
进程收到信号时，操作系统暂停主控制流，然后调用信号处理器函数，这形成了一个新的控制流（我们称为信号控制流），以及一个新的相应的调用栈。
如果一个信号控制流执行期间，又收到另一个信号，那么可能暂停这个信号控制流，并形成和执行另一个信号控制流。

当然，因为是单线程程序，并不是真正的并发，两个控制流不会并行或交替执行：信号控制流执行完毕后，主控制流才有机会恢复执行，
因此，控制流之间的同步是不必要也不可能的。

不同控制流仍然是共享整个进程的内存的。特别地，全局变量和堆是共享的，但调用栈是互相独立的。

注意，信号到来时，当前执行的控制流可能暂停在任意地方，比如两个CPU指令之间，这就可能导致一些类似多线程的并发性的问题。
例如，如果主控制流和信号控制流都会读写同一全局变量，那么就可能是不安全的——术语叫做“不可重入（Nonreentrant）”[43]。
malloc(), free(), printf() 等一大批库函数都是不可重入的，不能在主控制流和信号控制流中同时使用（仅在一个控制流中使用是可以的）[43]。
仅使用栈内存的函数当然是可重入的，因为不同控制流的调用栈是互相独立的。

可重入和线程安全之间没有必然关系，一个函数可能只拥有其中之一[46]。

要在不同控制流中安全地访问一个全局变量，则操作必须是原子的，这可以通过将其声明为 volatile sig_atomic_t 类型来实现。
当然原子操作只是必要条件，不是充分条件。

很多函数会修改 errno 这个全局变量（多线程程序中实际上是线程局部的），原则上它们是不可重入的。但如果仅仅是 errno 的问题，
是可以补救的：在信号控制流中，调用可能修改 errno 的函数前，先保存当前 errno 的值，调用完之后再恢复[43]。

为了避免不可重入的问题，最好尽量简化信号处理器的工作，例如“修改变量-返回”。

linux 2.6.22 引入了 signalfd() ，提供了除了信号处理器以外的另一种监听信号的方式，这种方式不需要在信号控制流中执行客户代码，可以避免信号控制流的一些问题。signalfd() 将信号与一个打开的文件描述符关联，客户代码可以用非阻塞的方式（如 select()）监听这个文件描述符，这就等于监听了信号。当然，如果同时有其它 IO 操作，也得是用非阻塞的方式，否则阻塞期间就无法接收到信号了。[57, 58]。

### 修改变量-返回

信号处理器可以修改一个全局变量并返回，并在主控制流中周期性的检测这个变量的变化。例如[42]：

```
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

/* This flag controls termination of the main loop. */
volatile sig_atomic_t keep_going = 1;

/* The signal handler just clears the flag and re-enables itself. */
void
catch_alarm (int sig)
{
  keep_going = 0;
  signal (sig, catch_alarm);
}

void
do_stuff (void)
{
  puts ("Doing stuff while waiting for alarm....");
}

int
main (void)
{
  /* Establish a handler for SIGALRM signals. */
  signal (SIGALRM, catch_alarm);

  /* Set an alarm to go off in a little while. */
  alarm (2);

  /* Check the flag once in a while to see when to quit. */
  while (keep_going)
    do_stuff ();

  return EXIT_SUCCESS;
}
```

这个方式通常用来实现程序的正常退出。

注意，如果主控制流采用阻塞式IO并且有可能无限期阻塞，则修改变量-返回可能并不适用，因为主控制流如果无限期阻塞，就没有机会检查该变量。
这时，或许应考虑改为非阻塞IO，并采用 signalfd(2) 来监听信号（可选，仅限linux）。[57,58]。

### 终止在信号处理器中[43]

```
volatile sig_atomic_t fatal_error_in_progress = 0;

void
fatal_error_signal (int sig)
{

  /* Since this handler is established for more than one kind of signal, 
     it might still get invoked recursively by delivery of some other kind
     of signal.  Use a static variable to keep track of that. */
  if (fatal_error_in_progress)
    raise (sig);
  fatal_error_in_progress = 1;


  /* Now do the clean up actions:
     - reset terminal modes
     - kill child processes
     - remove lock files */
  …


  /* Now reraise the signal.  We reactivate the signal’s
     default handling, which is to terminate the process.
     We could just call exit or abort,
     but reraising the signal sets the return status
     from the process correctly. */
  signal (sig, SIG_DFL);
  raise (sig);
}
```

### 信号的投递（deliver）和阻塞（block）和忽略（ignore）
详见[45]。
进程可以要求操作系统暂时阻塞某种信号的投递，将它们缓存起来；当解除阻塞后，会继续投递这些信号。
这样做可以避免打断某些敏感操作。某些信号，如 SIGKILL 不能被进程阻塞。

同类信号是自动相互阻塞的，即只有上一个信号的处理器执行完毕，才会投递下一个同类信号。

要查看进程为什么处于 sleep 状态，可以用 `strace -p <pid>` 来查看它处于什么系统调用。

要查看进程捕获、阻塞、忽略什么信号，可以查看 `/proc/pid/status` 文件，其中

blocked (SigBlk), ignored (SigIgn), caught (SigCgt) 都是 16 进制，是掩码，转为2进制后，每一位1的位数代表一个信号值（用 kill -l 查看）[49]。

...
SigBlk: 0000000000000000
SigIgn: fffffffe57f0d8fc
SigCgt: 00000000280b2603
...

### 内核阻塞信号和无法杀死的进程：uninterruptible sleep

操作系统本身也会暂时阻塞信号的投递，这一般发生在进程执行系统调用时，如果内核判断其中某些操作不可中断，则会暂时阻塞所有信号的投递，包括 SIGKILL [18]。
这种状态的进程是在 S 或 D 状态（用 ps 查看，S=sleep，D=uninterruptible sleep），或者叫做 uninterruptible sleep 状态。

读写文件或网络，尤其是网络文件系统属于这种情况，例如NFS、gvfs-sftp。
如果这样的系统调用持续较长时间，则进程在此期间不可能被杀死。

这可以说是 Linux/Unix 的一个设计缺陷。Linux 内核 2.6.25 引入了一种新的 sleep 状态：`TASK_KILLABLE`，这时内核会阻塞所有的“非致命信号”，但对于致命信号（可导致进程终止的），则不阻塞[19， 60]。
使用 TASK_KILLABLE 的主要地方是内核模块。如 NFS 客户端，在实现 `read()/write()` 等 IO 函数时设置客户进程为 `TASK_KILLABLE` 状态，这就允许访问 NFS 挂载点的进程在等待 IO 时被杀死（尚不清楚哪些版本的 linux 的 nfs 客户端实现了这个特性）[60]。又如，用户态代码进入 sleep 时可以改用新的 API，例如 `wait_event_killable()`，也会进入 `TASK_KILLABLE` 状态[19]。

解决的办法：
(1) 强制终止相关的网络连接，可以用cutter或 killcx 工具 [18]。
(2) 如果终止网络连接也无效，并且是 FUSE 文件系统（linux 用户空间文件系统），可以杀死相关的服务进程，例如 gvfsd-sftp 和 gvfsd ，然后 umount 相关文件系统。

### 常见信号
定义在 signal.h (或 csignal 在 C++)。
C 语言定义的信号[46]：

    SIGABRT - "abort", abnormal termination.
    SIGFPE - floating point exception.
    SIGILL - "illegal", invalid instruction.
    SIGINT - "interrupt", interactive attention request sent to the program.
    SIGSEGV - "segmentation violation", invalid memory access.
    SIGTERM - "terminate", termination request sent to the program.

UNIX 定义了更多信号[47]，其中一些是：

SIGHUP
  when its controlling terminal is closed. Many daemons will reload their configuration files and reopen
  their logfiles instead of exiting.

SIGPIPE
  when it attempts to write to a pipe without a process connected to the other end.

SIGQUIT
  by its controlling terminal when the user requests that the process quit and perform a core dump.

SIGSTOP
  The SIGSTOP signal instructs the operating system to stop a process for later resumption.
SIGCONT
  恢复 SIGSTOP 挂起的进程。

SIGKILL
  The SIGKILL signal is sent to a process to cause it to terminate immediately (kill). 

以上很多信号的默认行为都是退出（除了SIGSTOP）。行为良好的程序可能会捕获 SIGINT, SIGTERM, SIGHUP, SIGPIPE，SIGQUIT, 做一些清理工作再退出，或者不退出。
SIGSTOP 和 SIGKILL 不能被捕获或忽略。SIGABRT, SIGILL, SIGSEGV 可能没有捕获的价值。

此外 kill -l 命令可以列出信号及其对应的数字。

## 限制进程使用的资源
参考 man getrlimit。

       #include <sys/time.h>
       #include <sys/resource.h>

       int getrlimit(int resource, struct rlimit *rlim);
       int setrlimit(int resource, const struct rlimit *rlim);
       int prlimit(pid_t   pid,   int   resource,  const  struct  rlimit *new_limit, struct rlimit *old_limit);

           struct rlimit {
               rlim_t rlim_cur;  /* Soft limit */
               rlim_t rlim_max;  /* Hard limit (ceiling for rlim_cur) */
           };

prlimit 是linux特有的。

这些限制都会从父进程继承。shell 命令 ulimit 可以设置自身的这些限制，并被其子进程继承。
非特权进程只能设置 Soft limit，且不能超过 Hard limit，且只能减小，不能增加。
特权进程（有CAP_SYS_RESOURCE）则可以随意设置。部分资源类型：

RLIMIT_AS
              The maximum size of the process's  virtual  memory  (address
              space)  in  bytes.

RLIMIT_NOFILE
              Specifies a value one greater than the maximum file descrip‐
              tor  number  that  can  be opened by this process. 

RLIMIT_RSS
              Specifies the limit (in pages) of the process's resident set
              (the number of virtual pages resident in RAM).   This  limit
              has  effect  only  in Linux 2.4.x, x < 30, and there affects
              only calls to madvise(2) specifying MADV_WILLNEED.

## linux capabilities
linux 可以将进程的特权进行细分，而不是只有 root 用户和非 root 用户之别 [51]。

## 查看 linux 线程信息

从 linux 的 `/proc` 文件系统中可以查看线程的信息，在 `/proc/<pid>/task/<tid>/` 目录下，这个目录的内容与 `/proc/<pid>/` 基本对应。
同时，`/proc/<tid>/` 目录也存在，内容与 `/proc/<pid>/task/<tid>/` 相同，但 `/proc/<tid>/` 目录在 `/proc` 下是隐藏的，ls 看不到。

`ps -T -p <pid>` 命令可以查看指定进程 id 的线程信息[59]，例如：

```
$ ps -T -p 31590
  PID  SPID TTY          TIME CMD
31590 31590 ?        00:00:16 dde-lock
31590 31604 ?        00:00:00 QXcbEventQueue
31590 31605 ?        00:00:03 QDBusConnection
31590 31606 ?        00:00:00 gmain
31590 31607 ?        00:00:00 dconf worker
31590 31608 ?        00:00:00 gdbus
31590 31700 ?        00:00:00 KeyboardMonitor
31590 31735 ?        00:00:00 dde-loc:disk$0
31590 31736 ?        00:00:00 dde-loc:disk$1
```
SPID 是线程 id。CMD 是进程或线程名，来自 `/proc/<tid>/comm` [59]。

`top -H` 命令可以以线程为单位查看线程占用的资源。`top -H -p <pid>` 可查看特定进程的线程占用的资源[59]。

## 线程和 TID

在 linux 上，从内核的角度和从用户态的角度看，PID和TID的概念是不一样的。

从内核看，每个线程有一个独特的 PID 和可能相同的 TGID（意思是线程组ID），前者对应用户态的 TID，后者对应用户态的 PID[21]。

系统调用 gettid(2) 获得用户态的 TID，而 getpid() 获得用户态的 PID。

用户态的 TID 和 PID 在同一个空间，即不会有一个进程的某个 tid 与另一个进程的某个 tid 重复的情况。一个进程的第一个线程的 tid 与其 pid 相同。

## 线程和fork

在多线程的进程中调用 fork()，子进程中将只有一个线程，就是父进程中调用 fork 的那个线程的副本。当然，父进程的整个内存空间还是被复制了的。

如果在子进程中不再试图以多线程的方式去工作（如访问lock），而是调用 exec() 等函数，则不会有问题，否则，会出现很多奇怪的问题。

## 进程的名字

以下文件从不同角度反映了进程的名字：

```
/proc/<pid>/cmdline 文本，进程的命令行，含所有参数，用 `\0` 分割。
/proc/<pid>/comm    文本，进程的名字。注意这个文件对进程本身可写（也仅对进程本身可写），进程本身可以修改进程的名字，stat 和 status 会同步，但 cmdline 不变。
/proc/<pid>/exe     符号链接，指向启动进程的可执行文件。如果进程的入口是脚本，则一般指向执行脚本的解释器文件。如果解释器是符号链接，exe 指向最终的可执行文件。
/proc/<pid>/stat    文本，第二字段括号内为进程的名字，总是与 comm 相同。此文件只读。
/proc/<pid>/status  文本，第一行 Name 后为进程的名字，总是与 comm 相同。此文件只读。
```

决定进程的名字的算法（部分是我的测试和推测）：

* 内核收到 exec* 系统调用，判断第一个参数代表的文件是可执行程序还是 '#!' 开头的脚本文件。
  * 如果是可执行程序，则 /proc/<pid>/comm 设置为 exec* 的第一个参数的文件名部分，根据 exec* 的所有参数设置 /proc/<pid>/cmdline ，根据可执行程序的实际路径设置 /proc/<pid>/exe 。
    stat 和 status 跟随 comm 。如果 exec* 的第一个参数是相对路径，/proc/<pid>/cmdline 也会原样保留。
  * 如果是脚本，则 /proc/<pid>/comm 设置方法同上，而 /proc/<pid>/cmdline 设置为"解释器的路径"加上exec* 的所有参数，根据解释器的实际路径设置 /proc/<pid>/exe 。stat 和 status 跟随 comm 。
* 如果一个进程执行了 exec* 系统调用多次，则上述算法也会执行多次，这时我们就只能看到最近一次调用后的结果。
* 一个进程可以随时修改自己的 /proc/<pid>/comm ，以修改文件的方式，因此它并不总是反映刚启动时的进程名，不一定与 /proc/<pid>/cmdline 一致。
* 如果有 CAP_SYS_RESOURCE 特权，一个进程可以随时修改自己的 /proc/<pid>/cmdline 和 /proc/<pid>/exe，但不能以修改文件的方式（是只读的），方法是用 prctl() 系统调用修改进程的特定内存指针 [52,53]。
  修改 /proc/<pid>/exe 的目的主要是支持进程的 Checkpoint/Restore 功能，即保存进程的状态到磁盘上并在需要时恢复[54-56]。


例如有 python 脚本 /usr/bin/blueman-applet ，其第一行为

```
#! /usr/bin/python3
```

而 /usr/bin/python3 是指向 /usr/bin/python3.7 的符号链接。

从命令行执行 blueman-applet 或者 /usr/bin/blueman-applet ，则

```
/proc/<pid>/cmdline = "/usr/bin/python3\0/usr/bin/blueman-applet\0"
/proc/<pid>/comm = "blueman-applet"
/proc/<pid>/exe = /usr/bin/python3.7 的符号链接
```

但如果执行 `python3 /usr/bin/blueman-applet` ，则会变为

```
/proc/<pid>/cmdline = "python3\0/usr/bin/blueman-applet\0"
/proc/<pid>/comm = "python3"
/proc/<pid>/exe = /usr/bin/python3.7 的符号链接
```

不过，实际上 blueman-applet 会自己修改 /proc/<pid>/comm 为 blueman-applet 。

如果一个进程是 wine 启动的 windows exe程序，则 cmdline 是 exe 文件的全路径加参数，但却是 windows 路径格式。/proc/<pid>/comm 是去掉 '.exe' 的程序文件名，但如果太长会被截断。/proc/<pid>/exe 指向 /opt/wine-devel/bin/wine64-preloader 。wine 可能是对 /proc/<pid>/comm 和 /proc/<pid>/cmdline 都进行了修改。

## 进程相关的命令行工具
### ps
```
ps -el | less
F S   UID     PID    PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD
4 S     0       1       0  0  80   0 - 42441 -      ?        00:01:24 systemd
1 S     0       2       0  0  80   0 -     0 -      ?        00:00:00 kthreadd
1 I     0       3       2  0  60 -20 -     0 -      ?        00:00:00 rcu_gp

ps aux | less
USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root           1  0.0  0.0 169764 13908 ?        Ss   4月24   1:24 /sbin/init splash
root           2  0.0  0.0      0     0 ?        S    4月24   0:00 [kthreadd]
rd       3393354  0.0  0.0 132696 45872 ?        Sl   10:20   0:00 /home/rd/opt/venv/v39-1/bin/python3.9 /home/rd/.vscode-server/extensions/ms-python.black-formatter-2024.2.0/bundled/tool/lsp_server.py --stdio
rd       3393463  1.4  1.6 1850112 1086132 ?     Sl   10:20   0:33 /home/rd/.vscode-server/cli/servers/Stable-b58957e67ee1e712cebf466b995adf4c5307b2bd/server/node /home/rd/.vscode-server/extensions/ms-python.vscode-pylance-2024.4.106/dist/server.bundle.js --cancellationReceive=file:0b118c551510054baaf1a2f19ea66540ce1508a8c1 --node-ipc --clientProcessId=3393133
```

### pstree
```
pstree -apts [pid, user]
-a     Show command line arguments.  If the command line of a process is swapped out, that process is shown in parentheses.  -a implicitly disables compaction for processes but not threads.
-c     Disable compaction of identical subtrees.  By default, subtrees are compacted whenever possible.
-p     Show PIDs.  PIDs are shown as decimal numbers in parentheses after each process name.  -p implicitly disables compaction.
-s     Show parent processes of the specified process.
-t     Show full names for threads when available.
```
### killall

