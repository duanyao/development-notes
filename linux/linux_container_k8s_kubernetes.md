# Kubernetes

## 参考资料

[2.1] Install and Set Up kubectl
  https://kubernetes.io/docs/tasks/tools/install-kubectl/

[2.2] StatefulSet Basics
  https://kubernetes.io/docs/tutorials/stateful-application/basic-stateful-set/
  
[2.3] kubectl for Docker Users
  https://kubernetes.io/docs/reference/kubectl/docker-cli-to-kubectl/

[2.4] kubectl Cheat Sheet
  https://kubernetes.io/docs/reference/kubectl/cheatsheet/

[2.5] Installing snap on CentOS
  https://docs.snapcraft.io/installing-snap-on-centos/10020

[3.1] Get a Shell to a Running Container
  https://kubernetes.io/docs/tasks/debug-application-cluster/get-shell-running-container/

[3.2] How can I keep a container running on Kubernetes?
  https://stackoverflow.com/questions/31870222/how-can-i-keep-a-container-running-on-kubernetes

[3.4] kubectl.sh exec /bin/bash hangs after a while
  https://github.com/kubernetes/kubernetes/issues/9180

[3.5] kubernetes 权威指南学习第一天 简单了解k8s
  https://www.jianshu.com/p/f3be468e26a4

[4.1] aliyun 创建镜像
  https://help.aliyun.com/document_detail/86783.html?spm=a2c4g.11186623.6.632.3bb935afPJvbIU

[4.2] 多可用区创建实例
  https://help.aliyun.com/document_detail/157290.html

[4.3] 地域和可用区
  https://help.aliyun.com/document_detail/89155.htm?spm=a2c4g.11186623.2.8.2e3577f0YdHZVh#topic-1860084

[5.1] kunbernetes-基于NFS的存储
  https://www.kubernetes.org.cn/4022.html

[6.1] YAML快速入门
  https://www.jianshu.com/p/97222440cd08

[7.1] 阿里云开启共享带宽新时代【二】：负载均衡，NAT网关，ECS都支持共享带宽啦
  https://developer.aliyun.com/article/231601

[7.2] 阿里云共享带宽(后付费)
  https://common-buy.aliyun.com/?spm=5176.11182197.0.0.19d34882mirabG&commodityCode=cbwp#/buy

[8.1] Set MAC addresses of pods deterministically, allow prefixing
  https://github.com/kubernetes/kubernetes/issues/2289

[8.2] Is it possible to set the mac address of a pod/container?
  https://discuss.kubernetes.io/t/is-it-possible-to-set-the-mac-address-of-a-pod-container/11602/6

[8.3] openshift Using a MAC address pool for virtual machines 
  https://docs.openshift.com/container-platform/4.5/virt/virtual_machines/vm_networking/virt-using-mac-address-pool-for-vms.html
  
[8.4] Static IP Addresses and MAC Addresses for Pods
  https://support.huaweicloud.com/intl/en-us/usermanual-kunpengcpfs/kunpengkubeovn_06_0019.html
  
[9.1] CronJob
  https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/
  
[10.1] Kubernetes ImagePullBackOff error: what you need to know
  https://www.tutorialworks.com/kubernetes-imagepullbackoff/
  
[10.2] Pull an Image from a Private Registry
  https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
  
[10.3] k8s的imagePullSecrets如何生成及使用
  https://www.cnblogs.com/xiao987334176/p/11434326.html
  
[11.1] 自己动手在阿里云部署 K8S 集群
  https://juejin.cn/post/7033822420280541192
  
[11.2] 从零开始搭建K8s集群
  https://cloud.tencent.com/developer/article/1633267
  
[11.3] metrics-server
  https://github.com/kubernetes-sigs/metrics-server/
  
[11.4] Kubernetes Metrics Server | How to deploy k8s metrics server and use it for monitoring
  https://signoz.io/blog/kubernetes-metrics-server
  
[11.5] kube-state-metrics
  https://github.com/kubernetes/kube-state-metrics
  
[11.6] kube-prometheus
  https://github.com/prometheus-operator/kube-prometheus/

[11.7] Kubernetes K8S之kube-prometheus概述与部署
  https://cloud.tencent.com/developer/article/1780158
  
[12.1] Kubernetes Events: In-Depth Guide & Examples
  https://www.containiq.com/post/kubernetes-events

[13.1] Google Container Registry Mirror(Google Container Registry镜像加速)  
https://github.com/anjia0532/gcr.io_mirror

[13.2] K8S(kubernetes)镜像源
https://www.cnblogs.com/Leo_wl/p/15775077.html#_label2

## 简介

kubernetes 是从集群的层级管理linux容器的系统。被管理的linux容器可以是docker容器。

节点（node）是指传统服务器，集群（cluster）是指节点的集群，容器（pod）则运行在节点上。除了容器运行的节点，还有一个管理节点（master），用来管理所有的容器。

部署（deployment, statefulset）是一组容器组成的单位，进行统一管理。

镜像（image）是一个 linux 系统的文件树加上 k8s 的配置文件，前者包括可执行程序、库和配置文件等，但不包括内核。容器就基于镜像来运行。

kubernetes 的客户端命令行工具是 kubectl，它可以连接到管理节点，并管理所有的容器。

阿里云还提供一种新的 kubernetes serverless 产品，就是对用户屏蔽了节点这一层，用户只要管理部署和容器即可，节点由阿里云自动管理。

## kubectl
kubectl 可以代替 docker 命令行工具。

### 安装
* 直接下载安装
  ```
  curl -L  https://dl.k8s.io/release/stable.txt # 查看最新稳定版本
  curl -LO https://dl.k8s.io/release/v1.26.3/bin/linux/amd64/kubectl
  install -m 0755 kubectl $HOME/bin/kubectl-1.26.3
  rm kubectl
  ln -sf kubectl26 $HOME/bin/kubectl
  # sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
  ```
  kubectl 的版本号要与服务器端匹配，可相差一个小版本号，所以有时候需要安装多个版本。

* apt 安装（不建议，版本太老）
  ```
  apt install kubernetes-client
  ```
* snapd 安装
  ```
  sudo apt install snapd
  sudo snap install kubectl --classic
  sudo ln -s /snap/bin/kubectl /usr/local/bin/kubectl     # 如果 snap 把程序装到了 /snap/bin/ 下，不在 PATH 中，那么需要链接一下，或者将 /snap/bin/ 加入 PATH。
  ```
  
  centos 上安装 snapd 的方法见[2.5]。不过要注意 docker 容器里 snapd 无法运行，因为依赖 systemd。可以在主机中安装 snapd 版 kubectl，然后复制过去。文件是 /snap/kubectl/current/kubectl 。

### 版本
kubectl version

这个命令会显示 kubectl 客户端版本和连接的服务器端的版本，两者最好一致，或者客户端版本高于服务器端版本。apt版本如果落后，可以装 snap 版本。

### 配置
默认配置文件：~/.kube/config ，默认不存在。在这里配置如何连接到管理节点。

阿里云 kubernetes serverless 管理页面（容器服务kubernetes-集群-连接信息）上提供了此文件的内容，复制粘贴即可。

注意其中的 clusters[].cluster.server 字段（集群管理服务器）可能是内网地址，例如 https://172.17.112.25:6443 ，如果要从外网访问，可设置 ssh 隧道，例如
autossh -p22 -CNg -L 6443:172.17.112.25:6443 root@test.ai-parents.cn 。

* KUBECONFIG 环境变量
* kubeconfig 参数，例如 `kubectl --kubeconfig=path/to/config get pods`

### 控制服务器

## 查询状态

```
kubectl get pods
kubectl get services
kubectl get deployment
kubectl get statefulset
kubectl get nodes
kubectl get namespaces
kubectl describe pod <pod_name>
kubectl get events
```

对于阿里云的 kubernetes serverless 模式，get nodes 会失败。

## 查询资源占用情况
```
kubectl top pod
```

输出类似
```
NAME           CPU(cores)   MEMORY(bytes)   
ai-process-0   2158m        90Mi            
ai-process-1   1318m        86Mi 
```

## 创建部署（容器）

kubectl run my-nginx --image=nginx --replicas=3 --port=80
上述命令会创建一个部署，名叫 my-nginx，旗下包括3个nginx容器，每个容器都会暴露它的80端口。

可以用以下命令查看：
```
kubectl get deployment
kubectl get pods
kubectl get pods -l run=my-nginx # 只列出属于部署“my-nginx”的容器。
```

deployment 其实仅仅是指无状态的部署，k8s 另有一种有状态的部署，叫做 statefulset[2.2]：
```
kubectl get statefulset
```

如果容器的配置比较复杂，那么就应该写成一个配置文件，参考"附录1：阿里云上 StatefulSet yaml 样例，挂载 nfs"，然后执行这个配置文件：

```
kubectl apply -f xxx.yaml
```

## 停止、重启部署（容器）
```
kubectl delete deployment my-nginx  # 无状态的部署
kubectl delete statefulset my-nginx # 有状态的部署
```
注意，虽然可以单独停止一个容器，但 k8s 会立即重新创建一个容器来补充：
```
kubectl delete pod my-nginx-xxx
```
delete pod 之后重建的容器，并不会重新读取当初 `kubectl apply -f xxx.yaml` 时使用的配置文件，因此对配置文件的修改不会生效。

如果修改了配置文件后想要使其生效，可以重新执行`kubectl apply -f xxx.yaml`，或者 `delete deployment|statefulset` 之后再执行 `kubectl apply -f xxx.yaml`。

如果更新了镜像文件，想要在重启时应用新镜像，则应该保证镜像名有变化（yaml 中的 `image:`），否则仍可能使用老镜像，可能是因为k8s缓存了老镜像，并且是根据镜像名而不是其摘要来决定更新镜像。

## 创建可以从外网访问的服务

kubectl run my-nginx --image=nginx --replicas=3 --port=80

kubectl expose deployment my-nginx --port=80 --type=LoadBalancer
创建出一个类型为LoadBalancer的service。在阿里云上，这会创建一个负载均衡器（单独计费）。

通过查看服务的命令，可以看到这个服务的情况和创建出的公网IP。

```
$ kubectl get services # services 可简写为 svc
NAME       CLUSTER-IP     EXTERNAL-IP     PORT(S)        AGE
my-nginx   172.21.3.221   39.107.148.97   80:31633/TCP   10s
```

接下来用浏览器访问 http://39.107.148.97 即可。

如果用阿里云的网页控制台创建，可以在创建应用（应用似乎就对应pod）时就创建关联的服务（service），选择“负载均衡”类型；或者先创建应用，再创建服务。

## 让集群和容器可以访问外网

在阿里云等云服务上，kubernetes 集群的节点一般没有自己的外网IP，所以它如果要访问外网，就要通过一个 NAT 服务器了。
NAT 不是必选项，但如果没有，连拉取官方镜像都会失败，容器也就无法启动，所以这是个需要注意的坑。
阿里云上，NAT 服务器不归 kubernetes 管辖，且对其单独收费。

阿里云上，为kubernetes 集群创建NAT的方法有两个：
（1）创建集群时选择自动创建虚拟专网和NAT，这会同时自动创建一个虚拟专网、一个虚拟交换机、一个NAT、一个安全组、一个外网IP（EIP），NAT被配置为连通虚拟交换机和外网IP。
（2）创建集群时选择已有的虚拟专网。这会自动创建一个安全组（无法加入已有安全组）。如果可能，会利用已有虚拟交换机。然后自行创建一个 NAT，一个外网IP（EIP），并将两者关联。
   然后把NAT配置为连通所在虚拟专网（或虚拟交换机）和外网IP（创建SNAT条目->选择 k8s 集群所在的的虚拟专网（或虚拟交换机），选择NAT自己的外网IP）。

由于一个 k8s 集群可以使用一个虚拟专网下的多个虚拟交换机（新建的容器被随机分配到不同的交换机上），所以NAT的配置应尽量使用连通虚拟专网的模式。
   
SNAT 的功能是从内网访问外网；DNAT 则是让外网访问内网。

目前，SNAT 可以使用单个或多个公网 IP 地址。使用单个IP时，如果采用按流量收费，则带宽限制在200Mbps。
使用多个公网 IP 地址需要采用“共享带宽”[7.1,7.2]，不能按流量收费，价格比较高。

## 在阿里云上，让集群和容器可以访问其它云服务器 ECS
* 创建k8s集群时选择已有的、ECS 所在的虚拟专网。
* 设置 ECS 的安全组，增加k8s集群所在的安全组。不做这一步时，从 k8s 可以 ping 到 ECS，但进行 tcp 连接时会没有响应。

注意 ECS 可以加入多个安全组，但 k8s 集群只能有一个安全组，而且是自动创建的，创建时无法加入已有安全组。

已有的 k8s 集群所在的的虚拟专网和安全组，可以从“集群信息-所有集群-选择集群-集群资源” 下了解。

## 在已有容器中再执行程序

### 执行单个命令

```
kubectl exec pod-name -- command args
```
在不至于引起误会时，`--`可以省略。

### 在一组容器中同时执行一个命令

可以使用 kcdo 脚本（ https://gist.github.com/mnadel/4ddcc0ba07d398b35dad89b4dfdc8308 ）：

```
kcdo -s pod-name -- command args
```
例如：
```
kcdo -s video-process-v3  -- hostname
###
### kcdo {pods: 2, command: "hostname"}
###
###
### kcdo {pod: "1/2", name: "video-process-v3-0"}
###
video-process-v3-0
###
### kcdo {pod: "2/2", name: "video-process-v3-1"}
###
video-process-v3-1
```
-s 后面是应用的名字， -- 后面是要在容器内执行的命令及其参数。

### 在已有容器中运行一个 shell
[3.1]
```
kubectl get pods
NAME                          READY     STATUS    RESTARTS   AGE
my-nginx-4-8676fdf6cc-bw6rd   1/1       Running   0          13m

kubectl exec -it my-nginx-4-8676fdf6cc-bw6rd -- bash
```

不过，远程的 kubectl shell 连接会在短时间内超时并挂起，这应该是个 bug。

## 显示日志
容器的日志：
```
kubectl logs --tail=10 -f <pod-id>   # 实时日志
kubectl logs --tail=1000 <pod-id>    # 最近100行日志
```
显示事件（events）[12.1]：这包括容器尚未运行前的事件，如拉取镜像失败 ImagePullBackOff，以及非 pod 的事件。--all-namespaces 可以显示所有的 namespace，默认只显示 default namespace。
```
kubectl get events --all-namespaces | grep myapp
```
## 运行操作系统类镜像
[3.2]
这类镜像没有指定一个长时间运行的服务（例如nginx），所以如果直接运行，就会立即退出，并报告一个"Back-off restarting failed container" 错误。

解决方法是启动容器时，启动一个长时间运行的进程，例如 `sh -c "tail -f /dev/null"` 。

```
kubectl run my-centos --image=centos --replicas=1 -- sh -c "tail -f /dev/null"
kubectl get pods
NAME                          READY     STATUS    RESTARTS   AGE
my-centos-658bd8df7c-s9mqx    1/1       Running   0          34m
```

在配置文件或者阿里云上，可以指定容器执行的命令 `["sh", "-c", "tail -f /dev/null"]`。

或者在 k8s 配置文件中写

```
spec:
  template:
    spec:
      containers:
        - command:
            - tail
            - -f
            - /dev/null
```

附加shell到已有的容器实例中：

```
kubectl exec -it my-centos-658bd8df7c-s9mqx -- /bin/sh
```

`--` 之后是要在容器中执行的命令和参数，之前是 kubectl 本身的参数。

注意，容器版的操作系统可能精简掉了很多软件，需要手动安装。例如 centos 就没有 ifconfig（属于 net-tools 包）。
centos 可以用 yum whatprovides filename 来查询某个程序所属的包名。

## 容器的主机名
容器里可能没有 hostname 命令，可以用以下命令查看 hostname：

```
cat /proc/sys/kernel/hostname
```
容器的主机名与容器名相同，命名规律是：无状态容器为"部署名-随机后缀"，有状态容器为"部署名-从0开始的整数"。
但这个规律不绝对，有的云服务商有其它的命名规律，例如阿里云的有状态容器在 2019-07-24 以后改为"viking-集群id-部署名-从0开始的整数"。

目前没有找到指定容器的主机名模式的方法。

## 容器的 IP 地址
使用命令
kubectl get pod -o wide
可以得到每个容器的内网IP。

在阿里云上，IP 地址是自动分配的。

v1 中，IP地址位于集群的虚拟交换机的 IP 地址范围内。

V2 中，没有专门的虚拟交换机。容器的IP地址范围可以根据“集群信息”中的“API Server 内网连接端点“推测出来，例如内网连接端点是 https://172.17.112.25:6443 ，则容器地址范围是 172.17.112.0/20 。
或者IP范围可以通过容器的内网IP推测出来。

如果要从容器内访问 RDS 数据库，将集群的虚拟交换机的 IP 地址范围列入 RDS 的白名单即可。

IP 地址可以限制分配范围。

## 容器的 MAC 地址

[8.1-8.4]
目前 k8s 不能直接指定MAC地址或限定分配范围，一般是随机分配的。

k8s 的解决方案有：
* 特殊网络插件/驱动[8.1-8.2]。还没有看到成功案例。
* 容器启动后，在容器内部用 ip 工具修改 MAC 地址。这需要设置特权[8.2]。

阿里云 ASK （serverless k8s）不能限定MAC（2022.3.1）。
华为云 k8s 可以指定 MAC [8.4]。
redhat openshift 可以使用 mac 地址池 [8.3]。

## 自制镜像

可以将本地的 docker 镜像提供给 k8s 集群使用。本地 docker 镜像的制作方法请参考 docker 的相关文档。

阿里云提供了一个镜像仓库服务，可以上传本地的 docker 镜像，并在其 k8s 系统中使用[4.1]。方法是：容器服务-市场-镜像-容器镜像服务控制台。

开通镜像服务后，会提示生成 docker 登录密码，然后用这个密码进行 docker 登录：

sudo docker login --username=<阿里云账号> registry.cn-beijing.aliyuncs.com  #登录
sudo docker images  # 查看本地镜像

REPOSITORY           TAG                 IMAGE ID            CREATED             SIZE
duanyao/centos7      dev                 e5c881d7c443        3 weeks ago         939.8 MB
daocloud.io/centos   7                   5182e96772bf        7 months ago        199.7 MB

sudo docker tag duanyao/centos7:dev registry.cn-beijing.aliyuncs.com/aiparents/ai:v1 # e5c881d7c443 是要上传的本地 docker 的镜像 Id，v1 是版本号，随便写。
sudo docker push registry.cn-beijing.aliyuncs.com/aiparents/ai:v1             # 上传镜像

在阿里云创建容器的时候，镜像仓库的地址可以写 registry-vpc.cn-beijing.aliyuncs.com ，这样不会走外网流量。

## 镜像的相关故障

k8s 常见的镜像的相关故障是’ImagePullBackOff‘，表明镜像无法拉取[10.1]。其原因可能有多种：

1. 网络故障。拉取镜像是从 node 发起，到镜像仓库，需要考虑这一段的网络问题。
2. 镜像的url或版本号写错，或仓库里不存在此镜像或版本号。
3. 镜像仓库需要登录，但是帐号密码未提供或不正确。
4. 镜像仓库限流。

举个例子，使用一个不存在的镜像的url来启动一个app：
```
kubectl run myapp --image=myimage/myimage:latest
```
查看pod，显示 ImagePullBackOff 状态：

```
kubectl get pods --all-namespaces

NAMESPACE     NAME                                READY   STATUS             RESTARTS       AGE
default       myapp-5856487548-npf6n              0/1     ImagePullBackOff   0              118m
```

查看 events，获得尽可能详细的信息：

```
kubectl get events --all-namespaces | grep myapp

29m         Normal    Scheduled           pod/myapp-5856487548-npf6n    Successfully assigned default/myapp-5856487548-npf6n to recorder-3
28m         Normal    Pulling             pod/myapp-5856487548-npf6n    Pulling image "myimage/myimage:latest"
28m         Warning   Failed              pod/myapp-5856487548-npf6n    Failed to pull image "myimage/myimage:latest": rpc error: code = Unknown desc = Error response from daemon: pull access denied for myimage/myimage, repository does not exist or may require 'docker login': denied: requested access to the resource is denied
28m         Warning   Failed              pod/myapp-5856487548-npf6n    Error: ErrImagePull
4m48s       Normal    BackOff             pod/myapp-5856487548-npf6n    Back-off pulling image "myimage/myimage:latest"
28m         Warning   Failed              pod/myapp-5856487548-npf6n    Error: ImagePullBackOff
29m         Normal    SuccessfulCreate    replicaset/myapp-5856487548   Created pod: myapp-5856487548-npf6n
29m         Normal    ScalingReplicaSet   deployment/myapp              Scaled up replica set myapp-5856487548 to 1
```

可以看出，events 中报告说，“仓库不存在或需要登录”，这说明 k8s 的错误报告不能区分以原因2和3。

注意，有的资料说可以用 `kubectl describe pod` 命令来查看 events，但客户端 v1.14.0 服务器 v1.23.6 的组合并不能查看到 events，还是需要用 `kubectl get events` 命令。

## k8s 系统的镜像仓库的访问问题
[13.1-13.2]

一些 k8s 系统的镜像（如metrics server）存放在 gcr.io （Google Container Registry）网站上，但这个网站从境内访问有问题，因此会造成 ImagePullBackOff 之类的问题。
让 k8s 拉镜像时使用代理服务器是一个可能的办法，但尚不清楚如何配置。
目前比较现实的方法是[13.1]，有人持续将 gcr 上的镜像同步到 docker hub 上，因此将 k8s 配置文件中的 gcr 镜像的url改为在docker hub上的副本的url即可。
[13.2]列举了一些其它方法和gcr的镜像，但基本上已经不可用。


## 挂载 NFS 存储卷
这需要在配置文件中配置 nfs 的挂载参数：

```
spec:
  #...
  template:
    spec:
      #...
      containers:
          #...
          volumeMounts:
            - name: redis-persistent-storage
              mountPath: /data
      #...
      volumes:
        - name: k8s-data-1
          nfs:
            path: /k8s-nfs/redis/data
            server: 192.168.8.150
            readonly: true
```
volumeMounts 表示容器内的挂载点，可以有多个。volumes 表示存储设备的定义，例如 nfs，可以有多个。

volumeMounts[i].name 用来用于 volumes[j].name 匹配。

volumeMounts[i].mountPath 是容器内的挂载点。不需要是已经存在的目录，如果不存在，会自动创建。

nfs 的属性 
* server 是 nfs 服务器的地址
* path 是 nfs 的导出目录。
* readonly 只读

此处无法任意 nfs 的挂载参数；要设置更多的 nfs 挂载参数，可以使用 `persistentVolumeClaim` ，见后面的章节。

## 挂载本地存储卷（emptyDir）

```
spec:
  #...
  template:
    spec:
      #...
      containers:
          #...
          volumeMounts:
            - mountPath: /var/data1
              name: volume-1554280748218
      #...
      volumes:
        - emptyDir: {}
          name: volume-1554280748218
```
volumeMounts[i] 表示容器内的挂载点，与 nfs 没什么区别。
volumes[i] 中指定 `emptyDir: {}` 表示一个本地存储卷。

本地存储卷的用处不多，因为 k8s 容器自身的文件系统默认都是可写的。k8s pod销毁时，对 k8s 容器自身的的文件系统以及本地存储卷的修改都会消失。
本地存储卷的用途有：
* 当 k8s 容器（甚至node）意外崩溃时，本地存储卷里的数据一般不会丢失，但对 k8s 容器的自身的文件系统的修改则会丢失。可以利用这一点在容器意外崩溃后恢复运行。
* 可以设置 `emptyDir.medium` 为 "Memory"，这样将产生一个基于 tmpfs 的卷，使用内存为介质，可以获得极快的性能，但 node 崩溃后数据会消失（容器崩溃仍然可保留）。
* 让同属于一个 pod 的多个容器共享文件。

## 阿里云 ASK 中定义 nfs persistent volume，指定 mountOptions

为了更详细地指定 nfs 卷的挂载参数，需要使用 persistentVolumeClaim 的挂载方式。下面以阿里云 ASK 为例来说明，如何指定 nfs 的版本和缓存参数（vers, noac）。

在阿里云控制台ASK页面，打开集群详情，左边栏点击存储-存储卷，新建，自定义名字，类型为 NAS，存储驱动为 CSI（flexVolume为过时的驱动）。
挂载点选择已有的 NFS 服务器的域名，子目录（/）和版本（3）保持默认。

点击“查看yaml”，在spec下面，与volumeMode平级，增加或修改mountOptions属性如下：
```
  mountOptions:
    - hard
    - vers=3
    - noac
```
noac 是指不要缓存文件系统元数据，这可以缓解不同 NFS 客户机之间的可见性问题。

左边栏点击存储-存储声明，创建，自定义名字，“存储类型声明”选NAS，“分配模式”选“已有存储卷”，已有存储卷选刚才创建的。

最后，在容器定义的 yaml 文件中，引用上述存储声明：

```
    spec:
      containers:
      - name: nginx
        ...
        volumeMounts:
          - name: pvc-nas
            mountPath: "/data"
      volumes:
        - name: pvc-nas
          persistentVolumeClaim:
            claimName: pvc-nas
```

claimName 的值为上述存储声明的名字。

## 通过数值指定CPU和内存规格

阿里云为例：

[[250m, 4000m], [0.5G,4G]]: [2, 4G]
[[2000m, 8000m], [3G,8G]]: [2, 4G]
[[3000m, 8000m], [3G,8G]]: [4, 4G]
[[2500m,3000m], [3G,3G]]: [4, 8G]

不过，虚拟 CPU 下限超过 8000m 会有问题，可能一直无法创建成功。

目前看来，CPU资源上限没什么作用，阿里云会把 CPU 消耗限制在下限附近。

容器内部可以通过环境变量来得知容器上设置的资源限制。这需要在 yaml 中的 env 属性中配置：

```
          env:
            - name: memory_size
              valueFrom:
                resourceFieldRef:
                  resource: requests.memory
            - name: cpu_size
              valueFrom:
                resourceFieldRef:
                  resource: requests.cpu
```

阿里云上，通过数值指定CPU和内存规格似乎是过时的方式，它能分配到的实际机型是比较老旧的，似乎总是 Intel(R) Xeon(R) Platinum 8163 CPU @ 2.50GHz 。不过，通过指定ECS规格创建ECI Pod要昂贵很多。
ecs.g5 系列实例采用 Intel Xeon(Skylake) Platinum 8163 / Intel Xeon(Cascade Lake) Platinum 8269CY 。g6/g6e 采用 8269CY。g7 采用 Intel Xeon(Ice Lake) Platinum 8369B 。

## 通过指定ECS规格创建ECI Pod

ECI指定规格完全参考ECS规格定义。ECI单价与对应规格的ECS价格保持一致，按秒计费。参考类型：

GPU计算型实例规格族gn6i、gn6v、gn5i、gn5（不支持本地存储）
突发性能实例规格族t6、t5
密集计算型（1:1）实例规格族ic5
高主频计算型（1:2）实例规格族hfc6、hfc5

GPU型和突发性能是我们比较有兴趣的类型。

但目前，通过指定ECS规格创建ECI Pod比 要昂贵很多。

使用方法：

```
spec:
  template:
    metadata:
      annotations:
        k8s.aliyun.com/eci-use-specs : "ecs.c5.large"  #根据需要替换ECS规格
        
           limits:
             nvidia.com/gpu: '1'
```

## 阿里云 GPU 型实例

使用GPU实例 https://help.aliyun.com/document_detail/165054.html
https://help.aliyun.com/document_detail/114581.html

使用阿里云Serverless Kubernetes（ASK）和弹性容器实例（ECI），快速完成基于GPU的TensorFlow训练任务 https://help.aliyun.com/document_detail/145515.html

在Pod定义中增加annotations: k8s.aliyun.com/eci-use-specs。
在Pod的metadata中添加指定规格的annotations。
在Container的resources中声明GPU资源。

```
apiVersion: v1
kind: Pod
metadata:
  name: tensorflow
  annotations:
    k8s.aliyun.com/eci-use-specs: "ecs.gn6i-c4g1.xlarge"  # 指定GPU规格创建ECI实例
    k8s.aliyun.com/eci-image-cache: "true"             # 开启镜像缓存自动匹配
spec:
  containers:
  - name: tensorflow
    image: registry-vpc.cn-beijing.aliyuncs.com/eci/tensorflow:1.0 # 训练任务的镜像
    command:
      - "sh"
      - "-c"
      - "python models/tutorials/image/imagenet/classify_image.py" # 触发训练任务的脚本
    resources:
      limits:
        nvidia.com/gpu: "1"   # 容器所需的GPU个数
    volumeMounts:
    - name: nfs-pv
      mountPath: /tmp/imagenet
  volumes:   
  - name: nfs-pv   #训练结果持久化，保存到NAS
    flexVolume:
        driver:  alicloud/nas
        fsType: nfs
        options:
          server: 16cde4****-ijv**.cn-beijing.nas.aliyuncs.com     #NAS文件系统挂载点
          path: /         #挂载目录
  restartPolicy: OnFailure
```

其中 `eci-use-specs: ecs.gn5i-c4g1.xlarge` 指定了 GPU 型实例，具体含义是：

GPU计算型实例规格族gn7i（NVIDIA A10），例如：ecs.gn7i-c8g1.2xlarge。

GPU计算型实例规格族gn7（NVIDIA A100），例如：ecs.gn7-c12g1.3xlarge。

GPU计算型实例规格族gn6v（NVIDIA V100)，例如：ecs.gn6v-c8g1.2xlarge。

GPU计算型实例规格族gn6e（NVIDIA V100），例如：ecs.gn6e-c12g1.3xlarge。

GPU计算型实例规格族gn6i（NVIDIA T4)，例如：ecs.gn6i-c4g1.xlarge。

GPU虚拟化型实例规格族vgn6i-vws（NVIDIA T4)，例如：ecs.vgn6i-m4-vws.xlarge、ecs.vgn6i-m8-vws.2xlarge。

GPU计算型实例规格族gn5i（NVIDIA P4)，例如：ecs.gn5i-c2g1.large。

GPU计算型实例规格族gn5（NVIDIA P100)，例如：ecs.gn5-c4g1.xlarge。

如何制作GPU镜像则不太明了。

根据上述文档，似乎可以使用 https://hub.docker.com/r/nvidia/cuda 镜像，且由于驱动版本的限制，最高可使用 cuda 11.2 。
docker pull nvidia/cuda:11.2.2-base-ubi8
docker pull nvidia/cuda:11.2.2-runtime-ubi8
docker pull nvidia/cuda:11.2.2-devel-ubi8
docker pull nvidia/cuda:11.2.2-cudnn8-runtime-ubi8
docker pull nvidia/cuda:11.2.2-devel-ubuntu20.04
docker pull nvidia/cuda:11.2.2-runtime-ubuntu20.04

可能的官方镜像：

registry-vpc.cn-beijing.aliyuncs.com/eci/tensorflow:1.0
alinux-optimized-tensorflow-registry.cn-hangzhou.cr.aliyuncs.com/tensorflow/tensorflow2

## 阿里云多可用区创建实例
[4.2] 启动大量实例进行Job任务处理时，可能会遇到可用区对应规格实例库存不足或者指定的交换机IP耗尽等特殊情况，从而导致实例创建失败，影响业务。此时，您可以采用指定多可用区的方式来创建实例，提高实例创建的成功率。

前提条件

已在要使用的专有网络VPC下创建多个不同可用区的交换机。

对于已有的 k8s 集群，可以这样来编辑配置：

```
kubectl edit cm -n kube-system eci-profile
```
这会打开配置文件：
```
apiVersion: v1
data:
  kube-proxy: "true"
  privatezone: "false"
  quota-cpu: "192000"
  quota-memory: 640Ti
  quota-pods: "3000"
  region: cn-beijing
  resourcegroup: ""
  securitygroup: sg-2zee3q29pob3r2nxlawg
  vpc: vpc-2zenwbhq3lnisym6eg0s9
  vswitch: vsw-2zedyxem1w6iax7dsjooh,vsw-2ze3obgz6isguf1qk8x5o,vsw-2zedufrzcrfsxgl01id22,vsw-2zelhy3bv4d8sd876paq0
kind: ConfigMap
metadata:
  creationTimestamp: "2020-07-08T11:26:39Z"
  name: eci-profile
  namespace: kube-system
  resourceVersion: "66893926"
  selfLink: /api/v1/namespaces/kube-system/configmaps/eci-profile
  uid: c930f314-f264-41fc-a4c7-f8b0ca002d1f

```
在 vswitch 字段增加交换机的id。所有的交换机都要位于同文件的 vpc（虚拟专网）字段内。

## 自建k8s系统

k8s 的宿主机分为master（1台或少数几台）和node（通常很多台）。

master 和 node 上需要安装：docker(docker-ce)、kubelet、kubeadm、kubectl。如果容器需要挂载 nfs，则 node 上还需要安装 nfs 组件（redhat: nfs-utils, debian: nfs-common）。

在阿里云 centos 8 上，先修复仓库：

```
cd /etc/yum.repos.d/
for i in `ls`;do sed -i 's/mirrors.cloud.aliyuncs.com/mirrors.aliyun.com/g' $i;done
for i in `ls`;do sed -i 's/$releasever/$releasever-stream/g' $i;done

rm /etc/yum.repos.d/nodesource-el8.repo
rm /etc/yum.repos.d/CentOS-Linux-epel.repo
rm -f /etc/yum.repos.d/epel*.repo

yum clean all
yum makecache
```

再安装 docker-ce：

```
wget -O /etc/yum.repos.d/docker-ce.repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
yum install docker-ce
```

再安装 k8s，可参考： https://developer.aliyun.com/mirror/kubernetes?spm=a2c6h.13651102.0.0.3e221b11pDsvp1

```
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF

setenforce 0 # 停止 selinux

yum install kubelet kubeadm kubectl
```

node 上安装 nfs： `yum install nfs-utils` 。

master/node 上启动 docker 服务：
```
systemctl enable docker
systemctl start docker
```

启动 kubelet 服务前，确认一下 docker 的 cgroup driver 是 cgroupfs 还是 systemd。kubelet 配置的 cgroup-driver 要与之保持一致：
```
docker info |grep -i cgroup
 Cgroup Driver: cgroupfs

# /etc/sysconfig/kubelet 初始内容为 "KUBELET_EXTRA_ARGS="
echo "KUBELET_EXTRA_ARGS=--cgroup-driver=cgroupfs" > /etc/sysconfig/kubelet
```
否则，后面 master 执行 kubeadm init 会出错，如 “timed out waiting for the condition” 。

master/node 上启动 kubelet 服务：
```
systemctl enable kubelet
systemctl start kubelet
```
kubelet 服务可能启动失败，systemd 日志中可能出现：
```
5月 12 00:38:00 vp-k8s-node-1 kubelet[6567]: Flag --cgroup-driver has been deprecated, This parameter should be set via the config file specified by the Kubelet's --confi
g flag. See https://kubernetes.io/docs/tasks/administer-cluster/kubelet-config-file/ for more information.
5月 12 00:38:00 vp-k8s-node-1 kubelet[6567]: Error: failed to load kubelet config file, error: failed to load Kubelet config file /var/lib/kubelet/config.yaml, error fail
ed to read kubelet config file "/var/lib/kubelet/config.yaml", error: open /var/lib/kubelet/config.yaml: no such file or directory, path: /var/lib/kubelet/config.yaml
```
以及
```
5月 12 01:14:30 vp-k8s-node-1 kubelet[1129]: Error: failed to construct kubelet dependencies: unable to load client CA file /etc/kubernetes/pki/ca.crt: open /etc/kubernet
es/pki/ca.crt: no such file or directory
```

然后发现 /var/lib/kubelet/ 和 /etc/kubernetes/pki/ 目录并不存在。

master 上启动集群:

```
kubeadm init --image-repository registry.aliyuncs.com/google_containers --pod-network-cidr 10.244.0.0/16 # --apiserver-advertise-address=1.1.1.1  --kubernetes-version v1.22.4   --service-cidr=10.1.0.0/16
```

除了 --image-repository registry.aliyuncs.com/google_containers 和 --pod-network-cidr 10.244.0.0/16 以外，其它的参数可以不写，用默认值。

--pod-network-cidr 的值应当与后面 https://raw.githubusercontent.com/flannel-io/flannel/master/Documentation/kube-flannel.yml 中的 "Network" 字段一致，否则会部分失败，参考 https://blog.csdn.net/kjh2007abc/article/details/100041842 。

kubeadm init 会启动一组 docker 容器用于实现 k8s 自身。用 kubeadm reset 可以停止这些容器。

kubeadm init 的输出形如（节选）：
```
[root@ai-center ~]# kubeadm init --image-repository registry.aliyuncs.com/google_containers --pod-network-cidr 10.244.0.0/16

[init] Using Kubernetes version: v1.23.6
[certs] apiserver serving cert is signed for DNS names [ai-center kubernetes kubernetes.default kubernetes.default.svc kubernetes.default.svc.cluster.local] and IPs [10.96.0.1 172.17.178.7]

[certs] Using certificateDir folder "/etc/kubernetes/pki"

[certs] etcd/server serving cert is signed for DNS names [ai-center localhost] and IPs [172.17.178.7 127.0.0.1 ::1]
[certs] Generating "etcd/peer" certificate and key
[certs] etcd/peer serving cert is signed for DNS names [ai-center localhost] and IPs [172.17.178.7 127.0.0.1 ::1]

[kubeconfig] Using kubeconfig folder "/etc/kubernetes"

Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join 172.17.178.7:6443 --token vnpy9t.c98z62wdmfskxvt8 \
        --discovery-token-ca-cert-hash sha256:0e611edefe5faa24ea10044d801d3ae2059315c2a880158778a5fb7f871a4c77
```

master 上创建 flannel 网络：
```
[root@ai-center ~]# export KUBECONFIG=/etc/kubernetes/admin.conf  
[root@ai-center ~]# /usr/bin/kubectl apply -f https://raw.githubusercontent.com/flannel-io/flannel/master/Documentation/kube-flannel.yml
Warning: policy/v1beta1 PodSecurityPolicy is deprecated in v1.21+, unavailable in v1.25+
podsecuritypolicy.policy/psp.flannel.unprivileged created
clusterrole.rbac.authorization.k8s.io/flannel created
clusterrolebinding.rbac.authorization.k8s.io/flannel created
serviceaccount/flannel created
configmap/kube-flannel-cfg created
daemonset.apps/kube-flannel-ds created
```

指定 export KUBECONFIG=/etc/kubernetes/admin.conf 是必要的，因为 kubectl 的默认配置文件为 $HOME/.kube/config ，但它此时并不存在。kubectl 默认会连接 localhost:8080，但实际上应该连接 https://172.17.178.7:6443 。

查看 master 的状态：

```
[root@ai-center ~]# kubectl get nodes
NAME        STATUS   ROLES                  AGE    VERSION
ai-center   Ready    control-plane,master   2m2s   v1.23.6

[root@ai-center ~]# kubectl get pod -n kube-system
NAME                                READY   STATUS    RESTARTS   AGE
coredns-6d8c4cb4d-lmbv6             1/1     Running   0          116s
coredns-6d8c4cb4d-xsmx7             1/1     Running   0          116s
etcd-ai-center                      1/1     Running   2          2m10s
kube-apiserver-ai-center            1/1     Running   2          2m10s
kube-controller-manager-ai-center   1/1     Running   0          2m10s
kube-flannel-ds-7q2bj               1/1     Running   0          40s
kube-proxy-zznxh                    1/1     Running   0          117s
kube-scheduler-ai-center            1/1     Running   2          2m10s
```

如果任何单元的 STATUS 不是 Ready 或 Running ，则存在故障。

接下来，在 node 上加入集群（ kubeadm join 命令参数可在master 的 kubeadm init 的输出中找到。如果不记得，看后面。 ）：

```
[root@recorder-3 ~]# kubeadm join 172.17.178.7:6443 --token qmehsb.3cx7hxqo30scszkt --discovery-token-ca-cert-hash sha256:0e611edefe5faa24ea10044d801d3ae2059315c2a880158778a5fb7f871a4c77

[preflight] Running pre-flight checks
        [WARNING FileExisting-tc]: tc not found in system path
[preflight] Reading configuration from the cluster...
[preflight] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -o yaml'
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
[kubelet-start] Starting the kubelet
[kubelet-start] Waiting for the kubelet to perform the TLS Bootstrap...

This node has joined the cluster:
* Certificate signing request was sent to apiserver and a response was received.
* The Kubelet was informed of the new secure connection details.

Run 'kubectl get nodes' on the control-plane to see this node join the cluster.
```

kubeadm join 的命令参数可以在 master 节点上查看。 --token 参数在 master 节点用 kubeadm token list 查看。注意 token 在 24 小时内会过期，过期后应该用 kubeadm token create 重新创建，再用 kubeadm token list 查看。--discovery-token-ca-cert-hash 的参数用

```
openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | \
>    openssl dgst -sha256 -hex | sed 's/^.* //'
```
命令查看，结果形如 0e611edefe5faa24ea10044d801d3ae2059315c2a880158778a5fb7f871a4c77 ，这个不会过期。

如果不想再让一台机器作为 node ，或者要重新加入别的 master，或者 kubeadm join 失败了，也可以用 kubeadm reset 命令来重置 node 节点的状态。

在 master 上查看状态：
```
[root@ai-center ~]# kubectl get nodes
NAME         STATUS   ROLES                  AGE     VERSION
ai-center    Ready    control-plane,master   7m47s   v1.23.6
recorder-3   Ready    <none>                 18s     v1.23.6
[root@ai-center ~]# kubectl get pod -n kube-system
NAME                                READY   STATUS    RESTARTS   AGE
coredns-6d8c4cb4d-lmbv6             1/1     Running   0          7m35s
coredns-6d8c4cb4d-xsmx7             1/1     Running   0          7m35s
etcd-ai-center                      1/1     Running   2          7m49s
kube-apiserver-ai-center            1/1     Running   2          7m49s
kube-controller-manager-ai-center   1/1     Running   0          7m49s
kube-flannel-ds-7kzjb               1/1     Running   0          24s
kube-flannel-ds-7q2bj               1/1     Running   0          6m19s
kube-proxy-tkprc                    1/1     Running   0          24s
kube-proxy-zznxh                    1/1     Running   0          7m36s
kube-scheduler-ai-center            1/1     Running   2          7m49s
```

如果要从 master 删除一个 node ，则用 kubectl delete node 命令：
```
[root@ai-center ~]# kubectl delete node recorder-3
node "recorder-3" deleted
[root@ai-center ~]# kubectl get nodes
NAME        STATUS   ROLES                  AGE   VERSION
ai-center   Ready    control-plane,master   15d   v1.23.6
```

尝试往机器中放入一个 Nginx：

```
docker pull nginx
kubectl create deployment nginx --image=nginx
kubectl expose deployment nginx --port=80 --type=NodePort
```

查看 nginx 映射的端口（31985）并用 curl 访问：
```
[root@ai-center ~]# kubectl get pods,svc
NAME                         READY   STATUS              RESTARTS   AGE
pod/nginx-85b98978db-sv4cx   0/1     ContainerCreating   0          12s

NAME                 TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
service/kubernetes   ClusterIP   10.96.0.1       <none>        443/TCP        9m38s
service/nginx        NodePort    10.97.113.106   <none>        80:31985/TCP   11s

curl http://172.17.178.7:31985
```

输出 nginx 欢迎页面就成功了。

如果出现错误，可以查看：

```
kubectl get pods --all-namespaces # 查看所有容器状态
kubectl get ev # 查看最近的事件
kubectl describe pod ai-process-v5 # 查看应用的配置
```

如果要使用私有 docker 镜像仓库的镜像来启动 k8s 容器，就需要创建 k8s 的 secret。参考[10.3]：
可以先在 master 上用 docker 登录镜像仓库，再通过 docker 的登录凭证文件（~/.docker/config.json）来创建 k8s 的 secret：

```
docker login --username=成长轨迹教育 registry.cn-beijing.aliyuncs.com

kubectl create secret generic gt-docker --from-file=.dockerconfigjson=/root/.docker/config.json --type=kubernetes.io/dockerconfigjson
```

gt-docker 是 secret 的名字，可以随便起。创建的 secret 存储在 master 上的 k8s 的注册表中，可以这样查看：

```
kubectl get secret gt-docker --output=yaml
```

之后，可以在 k8s 应用的配置文件中加上 `imagePullSecrets` 属性，引用 gt-docker：

``` 
spec:
      imagePullSecrets:
      - name: gt-docker
      containers:
      - name: eureka
        image: registry.cn-beijing.aliyuncs.com/aiparents/ai-v5:8
```

k8s node 不需要做额外的配置。这样 node 就可以访问私有镜像仓库了，node 会先将镜像下载到本地，然后再创建容器。

关于容器的 cpu 和内存配额：一台 node 上运行的容器的 CPU 配额下限之和不能大于或等于宿主机，必须小一些；上限无此限制。内存配额也是一样的。
如果所有的 node 都不能满足容器的cpu 和内存配额，则一部分容器将永远处于 pending 状态。

### 自建 metrics server
参考资料[11.3-11.4]。
metrics server 提供一套监视 k8s 集群运行状态的 API，也用于支持 kubectl top 命令。但它不是随k8s安装的，要单独安装。

从这里下载 metrics server 的 k8s 配置文件：

https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.6.1/components.yaml

改名为 k8s-metrics-server-components.yaml 。

配置文件 k8s-metrics-server-components.yaml 做如下修改：
```
    spec:
      containers:
      - args:
        - --cert-dir=/tmp
        - --secure-port=4443
        - --kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname
        - --kubelet-use-node-status-port
        - --metric-resolution=15s
        - --kubelet-insecure-tls # 不安全，为了绕开证书问题。参考 https://github.com/kubernetes-sigs/metrics-server
        # 因为无法直接访问 k8s.gcr.io，改用此镜像仓库。参考：https://github.com/anjia0532/gcr.io_mirror
        image: anjia0532/google-containers.metrics-server.metrics-server:v0.6.1
        # 原始镜像地址
        # image: k8s.gcr.io/metrics-server/metrics-server:v0.6.1
```

创建 metrics-server 的容器和服务：
```
[root@ai-center ~]# kubectl apply -f /media/k8s-data-1/k8s_config/k8s-metrics-server-components.yaml
serviceaccount/metrics-server unchanged
clusterrole.rbac.authorization.k8s.io/system:aggregated-metrics-reader unchanged
clusterrole.rbac.authorization.k8s.io/system:metrics-server unchanged
rolebinding.rbac.authorization.k8s.io/metrics-server-auth-reader unchanged
clusterrolebinding.rbac.authorization.k8s.io/metrics-server:system:auth-delegator unchanged
clusterrolebinding.rbac.authorization.k8s.io/system:metrics-server unchanged
service/metrics-server unchanged
deployment.apps/metrics-server created
apiservice.apiregistration.k8s.io/v1beta1.metrics.k8s.io unchanged
```

可以看到 metrics-server 的容器：
```
[root@ai-center ~]# kubectl get pods --all-namespaces -o wide
NAMESPACE       NAME                                       READY   STATUS    RESTARTS       AGE    IP               NODE            NOMINATED NODE   READINESS GATES
kube-system     metrics-server-68c877bcc4-92q4v            0/1     Running   0              12m    10.244.7.50      recorder-3      <none>           <none>
```

然后就可以使用 top pods 命令了：

```
[root@ai-center ~]# kubectl top pods
NAME                         CPU(cores)   MEMORY(bytes)   
recognize-negative-event-0   1m           433Mi           
recognize-negative-event-1   1m           440Mi           
recognize-negative-event-2   1m           416Mi
```

如果不使用 --kubelet-insecure-tls ，也没有配置好证书，则会出现以下错误：
```
[root@ai-center ~]# kubectl top pods
Error from server (ServiceUnavailable): the server is currently unable to handle the request (get pods.metrics.k8s.io)

[root@ai-center ~]# kubectl logs metrics-server-68c877bcc4-92q4v -n kube-system
I0815 17:08:13.326376       1 serving.go:342] Generated self-signed cert (/tmp/apiserver.crt, /tmp/apiserver.key)
I0815 17:08:13.927254       1 requestheader_controller.go:169] Starting RequestHeaderAuthRequestController
I0815 17:08:13.927278       1 shared_informer.go:240] Waiting for caches to sync for RequestHeaderAuthRequestController
I0815 17:08:13.927326       1 configmap_cafile_content.go:201] "Starting controller" name="client-ca::kube-system::extension-apiserver-authentication::client-ca-file"
I0815 17:08:13.927352       1 shared_informer.go:240] Waiting for caches to sync for client-ca::kube-system::extension-apiserver-authentication::client-ca-file
I0815 17:08:13.927374       1 configmap_cafile_content.go:201] "Starting controller" name="client-ca::kube-system::extension-apiserver-authentication::requestheader-client-ca-file"
I0815 17:08:13.927381       1 shared_informer.go:240] Waiting for caches to sync for client-ca::kube-system::extension-apiserver-authentication::requestheader-client-ca-file
I0815 17:08:13.927765       1 secure_serving.go:266] Serving securely on [::]:4443
I0815 17:08:13.927827       1 dynamic_serving_content.go:131] "Starting controller" name="serving-cert::/tmp/apiserver.crt::/tmp/apiserver.key"
I0815 17:08:13.928138       1 tlsconfig.go:240] "Starting DynamicServingCertificateController"
W0815 17:08:13.928226       1 shared_informer.go:372] The sharedIndexInformer has started, run more than once is not allowed
E0815 17:08:13.936292       1 scraper.go:140] "Failed to scrape node" err="Get \"https://172.17.127.251:10250/metrics/resource\": x509: cannot validate certificate for 172.17.127.251 because it doesn't contain any IP SANs" node="vp-k8s-node-4"
E0815 17:08:13.937767       1 scraper.go:140] "Failed to scrape node" err="Get \"https://172.17.178.7:10250/metrics/resource\": x509: cannot validate certificate for 172.17.178.7 because it doesn't contain any IP SANs" node="ai-center"
E0815 17:08:13.941860       1 scraper.go:140] "Failed to scrape node" err="Get \"https://172.17.127.249:10250/metrics/resource\": x509: cannot validate certificate for 172.17.127.249 because it doesn't contain any IP SANs" node="vp-k8s-node-3"
```

### kube-state-metrics 和 kube-prometheus
参考[11.5-11.7]。
kube-prometheus 基于 kube-state-metrics，提供与 prometheus 监控系统对接的 k8s 监控功能。
kube-state-metrics 类似 metrics-server，监控 k8s 集群和容器，但模式略有不同。

## 自建本地k8s测试环境-minikube

https://minikube.sigs.k8s.io/docs/start/

minikube 是个单机环境下的 k8s 系统，管理机构和容器都在一台宿主机上，也只能在一台宿主机上。

### 安装
安装本地
```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb
```
设置 proxy，例如：
```
export http_proxy=http://127.0.0.1:23128;export https_proxy=http://127.0.0.1:23128;export NO_PROXY=localhost,127.0.0.1,10.0.0.0/8,172.16.0.0/12,192.168.0.0/16
```
这是因为 minikube 安装期间要访问的部分网站可能存在访问困难的问题。
NO_PROXY 是需要的，避免通过 proxy 访问 minikube 本身（minikube 容器的IP地址是 192.168.49.2）。

安装 docker 镜像和启动：
```
minikube start
```

```
minikube addons enable metrics-server
```

```
minikube dashboard
```

## 自建本地k8s测试环境-kind

https://kind.sigs.k8s.io/

kind or kubernetes in docker is a suite of tooling for local Kubernetes “clusters” where each “node” is a Docker container. kind is targeted at testing Kubernetes.

## 定时任务

k8s 可以用 CronJob 类型的配置文件（附录2）来启动定时任务。

## 附录1：阿里云上 StatefulSet yaml 样例，挂载 nfs

```
# ai-process.app.yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  labels:
    app: ai-process
  name: ai-process
  namespace: default
spec:
  podManagementPolicy: "Parallel"
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app: ai-process
  serviceName: ai-process-svc
  template:
    metadata:
      labels:
        app: ai-process
    spec:
      containers:
        - command:
            - /usr/bin/supervisord
          env:
            - name: ai-version
              value: 1
            - name: memory_size
              valueFrom:
                resourceFieldRef:
                  resource: requests.memory
            - name: cpu_size
              valueFrom:
                resourceFieldRef:
                  resource: requests.cpu
          image: 'registry-vpc.cn-beijing.aliyuncs.com/aiparents/ai:v2'
          imagePullPolicy: IfNotPresent
          name: ai-process
          resources:
            requests:
              cpu: 250m
              memory: 512Mi
            limits:
              cpu: 2000m
              memory: 4Gi
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
          volumeMounts:
            - mountPath: /var
              name: volume-1554280748218
            - mountPath: /media/private
              name: oss-private
            - mountPath: /media/temp
              name: oss-temp
            - mountPath: /media/static
              name: oss-static
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 5
      volumes:
        - emptyDir: {}
          name: volume-1554280748218
        - name: oss-private
          nfs:
            path: /private
            server: 172.17.112.17
        - name: oss-temp
          nfs:
            path: /temp
            server: 172.17.112.17
        - name: oss-static
          nfs:
            path: /static
            server: 172.17.112.17
  updateStrategy:
    type: RollingUpdate
```

## 附录2：定时任务配置样例

```
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: crontab-job
spec:
  schedule: "1 5 * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: crontab-job
            image: 'your-domain.com/your-project-path:version'
            args:
            - /usr/bin/php
            - /www/demo.php
          imagePullSecrets:
            - name: secret
          restartPolicy: OnFailure
```

schedule 的时间语法与 cron 相同（分、小时、天、月、周天），但这里不包括执行的命令。
