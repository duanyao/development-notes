## 资料
https://www.thesitewizard.com/general/set-cron-job.shtml

https://www.rosehosting.com/blog/automate-system-tasks-using-cron-on-centos-7/

https://stackoverflow.com/questions/134906/how-do-i-list-all-cron-jobs-for-all-users

## 编辑

以下编辑本用户的任务：
```
crontab -e
# 导入任务
crontab crontab.txt
# 删除已有任务
crontab -r
# 列出任务
crontab -l
```

## 语法

/var/spool/cron/crontabs/root
```
# do daily/weekly/monthly maintenance
# min   hour    day     month   weekday command
*/15    *       *       *       *       run-parts /etc/periodic/15min
0       *       *       *       *       run-parts /etc/periodic/hourly
0       2       *       *       *       run-parts /etc/periodic/daily
0       3       *       *       6       run-parts /etc/periodic/weekly
0       5       1       *       *       run-parts /etc/periodic/monthly
*       *       *       *       *       reboot
```

全局配置文件的语法

```
#/etc/crontab
SHELL=/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin
MAILTO=root

# For details see man 4 crontabs

# Example of job definition:
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# |  |  |  |  |
# *  *  *  *  * user-name  command to be executed

#minute hour     day     month  weekday   username   command
40      10       *       *       *        root       /usr/bin/touch /tmp/cron_flag
```

## 可能的问题

* 配置文件的主人和权限

	全局配置文件 /etc/crontab /etc/cron.d/* 必须属于 root 用户，否则可能被拒绝执行，crond 的输出类似：

	WRONG FILE OWNER (/etc/cron.d/crontab)

* 配置文件的最后必须有换行符，否则最后一行会被忽略

## 调试方法
cronie 的调试：

crond -n -x ext,sch,proc,pars,load,misc,test,bit

## systemd 服务

```
systemctl status crond
```
