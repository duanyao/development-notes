## 参考资料
[1.1] bcc
https://github.com/iovisor/bcc

[1.2] bcc INSTALL
https://github.com/iovisor/bcc/blob/master/INSTALL.md

[1.3] nfsslower crashes on kernel 5.10.0: cannot attach kprobe, probe entry may not exist #3438 
https://github.com/iovisor/bcc/issues/3438

[1.4] [Proposal] Tracing inline functions #5093 
https://github.com/iovisor/bcc/issues/5093

[2.1] fuse 文件系统监控 fuse-ebpf-exporter
https://github.com/syseleven/fuse-ebpf-exporter
https://gitee.com/duanyao/fuse-ebpf-exporter

[2.2] Prometheus exporter for custom eBPF metrics and OpenTelemetry traces.
https://github.com/cloudflare/ebpf_exporter


## 安装
```
sudo apt install bpfcc-tools linux-headers-$(uname -r)
# 可选的命令行工具
sudo apt libbpf-tools
# 可选的python API
sudo apt install python3-bpfcc
```
### python API
在 debian 系上，注意需要用全局 python3 运行 bcc，因为需要 root 权限。其依赖也应优先安装 apt 版本而不是 pip 版本。
```
sudo apt install python3-bpfcc
```

## 可以被监视的内核函数

### 查看当前内核中可用的函数
例如要查看 nfs_file_read 函数是否存在于当前的内核中，可以：
```
cat /sys/kernel/debug/tracing/available_filter_functions | grep nfs_file_read
kernfs_file_read_iter
nfs_file_read [nfs]


cat /proc/kallsyms | grep nfs_file_read

ffffffff87456040 t kernfs_file_read_iter
ffffffffc44004e1 r __kstrtab_nfs_file_read      [nfs]
ffffffffc44004ef r __kstrtabns_nfs_file_read    [nfs]
ffffffffc43fb390 r __ksymtab_nfs_file_read      [nfs]
ffffffffc43f968b t nfs_file_read.cold   [nfs]
ffffffffc43dba80 t nfs_file_read        [nfs]
```

注意，有的内核函数位于动态加载模块中，如果未加载该模块，则以上方法查询不到。nfs 就是一个例子，如果没有使用 nfs，则 nfs 模块可能没有加载，这时可以手动加载：`modprobe nfs`。

### 内核函数内联问题
一个 linux 内核函数是否被内联是经常发生变化的。如果函数被内联，则一般无法通过 kprobe 探测到该函数，而且不一定会报错，这造成了bpf的相关工具经常会出问题。
例如 vfs_fsync 调用了 vfs_fsync_range ，而 vfs_fsync 被内联了，所以 vfsstat-bcc 跟踪 vfs_fsync 就会返回0结果，而跟踪 vfs_fsync_range 就能正常工作。
bcc 提出了一个方法，通过调试符号来跟踪被内联的函数 [1.4]。

## 文件系统监控

### 概述
监控 FUSE, xfs, ext4, vfs 的例子见 [2.1]。
[1.1] 中也有多个文件系统监控的例子，如 tools/vfsstat.py， tools/xfsslower.py，examples/tracing/vfsreadlat.py 。

注意 vfs 监控会收集到大量数据，许多是非常规文件系统相关的（例如鼠标），如果想专注于常规文件系统，还是需要指定文件系统类型或用其他方式过滤。

### 命令行工具
* nfsdist-bpfcc : 属于 bpfcc-tools 包，用于监控 NFS 文件系统。
  * nfsdist-bpfcc -m 5 # trace nfs operation, 5s summaries, in ms
* fsdist : 属于 libbpf-tools 包，用于监控 nfs, xfs, ext4, btrfs 。
  * fsdist -t nfs -p 1216 # trace nfs operations with PID 1216 only
  * fsdist -t btrfs -m 5       # trace btrfs operation, 5s summaries, in ms
* fileslower-bpfcc: 属于 bpfcc-tools 包，用于监视虚拟文件系统读/写延迟（vfs_read/vfs_write）。这个工具不区分实际文件系统类型，而是做全系统监控。
  * ./fileslower             # trace sync file I/O slower than 10 ms (default)
  * ./fileslower 1           # trace sync file I/O slower than 1 ms
  * ./fileslower -p 185      # trace PID 185 only
* vfscount-bpfcc: 属于 bpfcc-tools 包，用于计数各种VFS函数（vfs_*）的调用次数。

### fuse 文件系统监控 fuse-ebpf-exporter
这个工具可以监视 xfs, ext4, nfs, vfs 等文件系统，可输出事件日志、直方图，同时提供 prometheus metrics 格式的指标[2.1]。例如：
```
prometheus-ebpf-exporter.py --listen-addr localhost --listen-port 9500  --xfs --ext4 --events --minms 10 --printhistograms
```

### ebpf_exporter
```
sudo ./ebpf_exporter --config.dir=examples --config.names=biolatency
```