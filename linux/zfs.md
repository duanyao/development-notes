## 参考资料
https://zfsonlinux.org/

https://openzfs.github.io/openzfs-docs/Getting%20Started/index.html

## 安装（debian）

### 非 root 分区
apt update
apt install dpkg-dev linux-headers-$(uname -r) linux-image-amd64
apt install zfs-dkms zfsutils-linux

### root 分区
