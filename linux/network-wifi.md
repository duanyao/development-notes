## 参考资料

[3.1] Resetting Wireless Networking on Ubuntu without Rebooting
https://blog.ostermiller.org/resetting-wireless-networking-on-ubuntu-without-rebooting/

[9.1] Bug 204151 - iwlwifi: firmware crash on Intel Wireless AC 3168 when sending GEO_TX_POWER_LIMIT  https://bugzilla.kernel.org/show_bug.cgi?id=204151

[10.1] Make Network Manager restart after dropped connection?
https://askubuntu.com/questions/71528/make-network-manager-restart-after-dropped-connection


WiFi6无线路由器大盘点（2021年1月版）
https://zhuanlan.zhihu.com/p/219697530

2021年618热门WiFi6路由器大盘点
https://zhuanlan.zhihu.com/p/374635769

2021年双11 WiFi6路由器选购攻略（TP-LINK篇）
https://zhuanlan.zhihu.com/p/426529877

2021年双11路由器选购攻略（华为篇）
https://zhuanlan.zhihu.com/p/435746356

WiFi6手机盘点，市面所有WiFi6手机一览
https://zhuanlan.zhihu.com/p/343295003

WiFi6无线网卡大盘点，想用WiFi6，你的网卡升级了吗？（2021.11更新）
https://zhuanlan.zhihu.com/p/142167200

## 解决问题

### 重启 wifi 驱动程序（内核模块）
[3.1]
先确定 wifi 驱动的内核模块名：
```
sudo lshw -C network 2>&1 | grep wireless | grep driver

       configuration: broadcast=yes driver=rtw89_8852ce driverversion=6.4.12-060412-generic firmware=N/A ip=192.168.8.119 latency=0 link=yes multicast=yes wireless=IEEE 802.11
```
内核模块名为：rtw89_8852ce

重启 wifi 驱动的内核模块：
```
sudo modprobe -r rtw89_8852ce
sudo modprobe rtw89_8852ce
```

有的模块名时别名，modprobe 无法找到，例如 “wl0”。可以查找类似的模块名：
```
    lsmod | grep wl
    wl 4207846 0
    lib80211 14381 2 wl,lib80211_crypt_tkip
    cfg80211 484040 1 wl 
```
可以确定模块名为 wl。然后再执行： 
```
sudo modprobe -r wl && sudo modprobe wl
```

如果 `modprobe -r` 无效，可以试试 `rmmod`。

## bug

### rtw89_8852ce 断流/延迟高

WiFi hangs periodically
https://github.com/lwfinger/rtw89/issues/226

The RTL8852BE works erratically under 5Ghz WiFi
https://github.com/lwfinger/rtw89/issues/214

Lenovo Thinkbook AMD CPU rtw89_8852be stopped working after a period of time #271 
https://github.com/lwfinger/rtw89/issues/271

I just pushed a new version that may fix this problem. Do a 'git pull' and remake the driver. Report here if it helps or not.
----

This problem is due to your BIOS not handling the power changes on the PCIe bus. Create a file with
sudo touch /usr/lib/modprobe.d/70-rtw8852be.conf
Using your favorite editor, add the following lines to the file above.

```
options rtw89_pci disable_clkreq=y disable_aspm_l1=y disable_aspm_l1ss=y
options rtw89pci disable_clkreq=y disable_aspm_l1=y disable_aspm_l1ss=y
options rtw89_core disable_ps_mode=y
options rtw89core disable_ps_mode=y
```
Those options keep the PCI bus from entering the deepest idle mode, and keeps the AP from entering power-save mode.. That set of options will work for the kernel driver, AND the one from this repo. The downside is that your wireless card and computer will use more power, which is why these options are not standard.

sudo journalctl -k --since 15:30
```
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x20119f98
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x20119f7e
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2011be6e
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2011a028
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2011b514
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x20119f86
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x20119f6e
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2011a03a
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2011a016
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x20119f72
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1001
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: FW status = 0x93008100
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: FW BADADDR = 0x0
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: FW EPC/RA = 0x0
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: FW MISC = 0x20117bd4
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: R_AX_HALT_C2H = 0x1002
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: R_AX_SER_DBG_INFO = 0xf2000012
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2000c1c8
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2003a9c8
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2011ac0e
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2003c930
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2013efa6
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2000c1d2
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2013ae3e
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2003c992
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x201399ec
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2000c1d2
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x20030efe
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2003c992
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2013a0e6
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2002e974
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2013a0ee
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1002
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: c2h class 1 func 3 not support
9月 07 15:50:14 duanyao-lt-d kernel: wlp3s0: deauthenticating from c2:6c:b3:18:26:e1 by local choice (Reason: 3=DEAUTH_LEAVING)
9月 07 15:50:14 duanyao-lt-d kernel: wlp3s0: authenticate with 5c:02:14:79:b6:6e
9月 07 15:50:14 duanyao-lt-d kernel: wlp3s0: send auth to 5c:02:14:79:b6:6e (try 1/3)
9月 07 15:50:14 duanyao-lt-d kernel: wlp3s0: authenticated
9月 07 15:50:14 duanyao-lt-d kernel: wlp3s0: associate with 5c:02:14:79:b6:6e (try 1/3)
9月 07 15:50:14 duanyao-lt-d kernel: wlp3s0: RX AssocResp from 5c:02:14:79:b6:6e (capab=0x1511 status=0 aid=1)
9月 07 15:50:14 duanyao-lt-d kernel: wlp3s0: associated
9月 07 15:50:14 duanyao-lt-d kernel: wlp3s0: Limiting TX power to 27 (30 - 3) dBm as advertised by 5c:02:14:79:b6:6e
9月 07 15:50:14 duanyao-lt-d kernel: IPv6: ADDRCONF(NETDEV_CHANGE): wlp3s0: link becomes ready
...skipping...
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x20119f98
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x20119f7e
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2011be6e
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2011a028
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2011b514
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x20119f86
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x20119f6e
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2011a03a
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2011a016
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x20119f72
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1001
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: FW status = 0x93008100
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: FW BADADDR = 0x0
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: FW EPC/RA = 0x0
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: FW MISC = 0x20117bd4
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: R_AX_HALT_C2H = 0x1002
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: R_AX_SER_DBG_INFO = 0xf2000012
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2000c1c8
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2003a9c8
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2011ac0e
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2003c930
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2013efa6
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2000c1d2
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2013ae3e
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2003c992
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x201399ec
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2000c1d2
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x20030efe
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2003c992
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2013a0e6
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2002e974
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: [ERR]fw PC = 0x2013a0ee
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1002
9月 07 15:31:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: c2h class 1 func 3 not support
```

另一种表现（从 2023-09-22 到 2023-11-29 ，2024-10-24 均有出现）：
```
9月 22 10:59:40 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: firmware failed to ack for entering ps mode
```

检查固件:
确定固件文件（从 2023-09-22 到 2023-11-29 ，2024-10-24 均有如此）： 
```

11月 27 16:24:48 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: loaded firmware rtw89/rtw8852c_fw.bin
```

本地固件：
```
dpkg -S rtw8852c_fw.bin
linux-firmware: /lib/firmware/rtw89/rtw8852c_fw.bin
ls -l /lib/firmware/rtw89/rtw8852c_fw.bin
-rw-r--r-- 1 root root 1532656  8月 15 17:55 /lib/firmware/rtw89/rtw8852c_fw.bin

# 固件可能是zstd压缩的，用 zstd -lv 查看其原始大小：
# 从 2023-09-22 到 2023-11-29
dpkg -S rtw89/rtw8852c_fw.bin
linux-firmware: /lib/firmware/rtw89/rtw8852c_fw.bin.zst
zstd -lv /lib/firmware/rtw89/rtw8852c_fw.bin.zst
Decompressed Size: 1.46 MiB (1532736 B)

# 2024-10-24，不再压缩。大小与一年前一样。
dpkg -S rtw8852c_fw.bin
linux-firmware: /lib/firmware/rtw89/rtw8852c_fw.bin
stat /lib/firmware/rtw89/rtw8852c_fw.bin
  文件：/lib/firmware/rtw89/rtw8852c_fw.bin
  大小：1532736         块：3000       IO 块大小：4096   普通文件
  修改时间：2024-03-15 15:55:10.000000000 +0800
md5sum /lib/firmware/rtw89/rtw8852c_fw.bin
ceed5cd7b2066d7a8b99e73e9056d3c1  /lib/firmware/rtw89/rtw8852c_fw.bin

# 2023-11-29
apt policy linux-firmware
linux-firmware:
  已安装：20230323.gitbcdcfbcf-0ubuntu1.6

linux-firmware:
  已安装：20230919.git3672ccab-0ubuntu2.2

linux-firmware:
  已安装：20230919.git3672ccab-0ubuntu2.4

# 2024-10-24
linux-firmware:
  已安装：1.202.16
  候选： 1.202.16
  版本列表：
 *** 1.202.16 500
        500 https://community-packages.deepin.com/beige beige/main amd64 Packages
```
固件的仓库在 https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/tree/rtw89/rtw8852c_fw.bin
提交历史在 https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/log/rtw89/rtw8852c_fw.bin?showmsg=1

当前安装的应该是 2023-01-17 提交的 v0.27.56.10，大小 1532656 bytes。
linux-firmware 20230919 更新到 2023-04-06 提交的 v0.27.56.13，1532736 bytes。
2024-03-21 提交的 v0.27.56.14，大小 1532656 bytes。

git 中的文件大小可以在提交日志中找到：
https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/commit/rtw89/rtw8852c_fw.bin?id=fc5a25fa022c7e28ecfc2ced4ea309ad61ca142e
https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/commit/rtw89/rtw8852c_fw.bin?id=b9c8e9f7056ba3bd0313208ac4d185b695a84f2d

#### 修复1：安装 https://github.com/lwfinger/rtw89 dkms
```
sudo apt install dh-sequence-dkms debhelper build-essential devscripts

git clone https://github.com/lwfinger/rtw89.git rtw89.git
cd rtw89.git

# 检出 test 分支(main 分支暂时没有 debian 目录)
# If you've already built as above clean up your workspace or check one out specially (otherwise some temp files can end up in your package)
git clean -xfd

git deborig HEAD
dpkg-buildpackage -us -uc
sudo apt install ../rtw89-dkms_1.0.2-3_all.deb 
```
安装日志（节选）：
```
...

rtw_8852ce.ko.zst:
Running module version sanity check.
 - Original module
   - No original module exists within this kernel
 - Installation
   - Installing to /lib/modules/6.5.6-060506-generic/updates/dkms/
```
查看安装位置：
```
cd /lib/modules/6.5.6-060506-generic/updates/dkms/ && ls -l rtw*
-rw-r--r-- 1 root root   2367 11月 30 17:30 rtw_8852ae.ko.zst
-rw-r--r-- 1 root root  74038 11月 30 17:30 rtw_8852a.ko.zst
-rw-r--r-- 1 root root   2364 11月 30 17:30 rtw_8852be.ko.zst
-rw-r--r-- 1 root root  77994 11月 30 17:30 rtw_8852b.ko.zst
-rw-r--r-- 1 root root   2410 11月 30 17:30 rtw_8852ce.ko.zst
-rw-r--r-- 1 root root  91010 11月 30 17:30 rtw_8852c.ko.zst
-rw-r--r-- 1 root root 226376 11月 30 17:30 rtw89core.ko.zst
-rw-r--r-- 1 root root  29321 11月 30 17:30 rtw89pci.ko.zst
```
确定系统使用的内核模块的绝对路径：

```
modinfo rtw89_8852ce
filename:       /lib/modules/6.5.6-060506-generic/kernel/drivers/net/wireless/realtek/rtw89/rtw89_8852ce.ko.zst
```
内核自带的模块
```
ls -l /lib/modules/6.5.6-060506-generic/kernel/drivers/net/wireless/realtek/rtw89/
总计 1176
-rw-r--r-- 1 root root  42230 10月  6 20:35 rtw89_8851be.ko.zst
-rw-r--r-- 1 root root 121518 10月  6 20:35 rtw89_8851b.ko.zst
-rw-r--r-- 1 root root  42275 10月  6 20:35 rtw89_8852ae.ko.zst
-rw-r--r-- 1 root root 136241 10月  6 20:35 rtw89_8852a.ko.zst
-rw-r--r-- 1 root root  42283 10月  6 20:35 rtw89_8852be.ko.zst
-rw-r--r-- 1 root root 144647 10月  6 20:35 rtw89_8852b.ko.zst
-rw-r--r-- 1 root root  42314 10月  6 20:35 rtw89_8852ce.ko.zst
-rw-r--r-- 1 root root 161058 10月  6 20:35 rtw89_8852c.ko.zst
-rw-r--r-- 1 root root 366823 10月  6 20:35 rtw89_core.ko.zst
-rw-r--r-- 1 root root  79822 10月  6 20:35 rtw89_pci.ko.zst
```
注意 dkms 模块与内核自带模块不同名，前者为 `rtw_*`（除了 rtw89core 和 rtw89pci），后者为 `rtw89_*` ，但首次安装 dkms 模块后，应当重启系统以完成新旧模块切换。重启后检查加载的模块的绝对路径：

```
lsmod | grep rtw
rtw_8852ce             12288  0
rtw_8852c             835584  1 rtw_8852ce
rtw89pci               73728  1 rtw_8852ce
rtw89core             589824  2 rtw_8852c,rtw89pci
mac80211             1724416  2 rtw89pci,rtw89core
cfg80211             1314816  3 rtw_8852c,rtw89core,mac80211

modinfo rtw_8852ce
filename:       /lib/modules/6.5.6-060506-generic/updates/dkms/rtw_8852ce.ko.zst

modinfo rtw89core
filename:       /lib/modules/6.5.6-060506-generic/updates/dkms/rtw89core.ko.zst
```

说明已经完成了替换。

检查内核日志：

```
11月 30 17:57:50 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: Direct firmware load for rtw89/rtw8852c_fw.bin failed with error -2
11月 30 17:57:50 duanyao-lt-d kernel: ***** failed to early request firmware: -2
11月 30 17:57:50 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: Firmware version 0.27.56.13, cmd version 0, type 1
11月 30 17:57:50 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: Firmware version 0.27.56.13, cmd version 0, type 3
```
根据 https://github.com/lwfinger/rtw89/issues/220 似乎时因为 rtw8852c_fw.bin 时压缩状态，内核模块不能正确处理，但最终还是能加载固件。事实上 wifi 工作正常，可以忽略此错误。

查看 NetworkManager 日志中的状态转换，`CONNECTED_SITE` 还是会经常出现，一般会迅速恢复，但也有例外，例如 12月 01 01:36:47 连上 `BJCZ_4G_2`, 12月 01 05:27:21 转为 `CONNECTED_SITE`，
12月 01 09:03:59 内核才检测到 lost，重新连接。
```
sudo journalctl -b | grep "NetworkManager state"
11月 30 17:57:56 duanyao-lt-d NetworkManager[1275]: <info>  [1701338276.3008] manager: NetworkManager state is now CONNECTING
11月 30 17:57:57 duanyao-lt-d NetworkManager[1275]: <info>  [1701338277.1996] manager: NetworkManager state is now CONNECTED_SITE
11月 30 17:57:57 duanyao-lt-d NetworkManager[1275]: <info>  [1701338277.7354] manager: NetworkManager state is now CONNECTED_GLOBAL
12月 01 00:43:17 duanyao-lt-d NetworkManager[1275]: <info>  [1701362597.8028] manager: NetworkManager state is now CONNECTED_SITE
12月 01 00:43:18 duanyao-lt-d NetworkManager[1275]: <info>  [1701362598.4703] manager: NetworkManager state is now CONNECTED_GLOBAL
12月 01 01:36:41 duanyao-lt-d NetworkManager[1275]: <info>  [1701365801.8750] manager: NetworkManager state is now CONNECTED_LOCAL
12月 01 01:36:44 duanyao-lt-d NetworkManager[1275]: <info>  [1701365804.9946] manager: NetworkManager state is now CONNECTING
12月 01 01:36:45 duanyao-lt-d NetworkManager[1275]: <info>  [1701365805.8455] manager: NetworkManager state is now CONNECTED_SITE
12月 01 01:36:47 duanyao-lt-d NetworkManager[1275]: <info>  [1701365807.4797] manager: NetworkManager state is now CONNECTED_GLOBAL
12月 01 05:27:21 duanyao-lt-d NetworkManager[1275]: <info>  [1701379641.8616] manager: NetworkManager state is now CONNECTED_SITE
12月 01 09:05:53 duanyao-lt-d NetworkManager[1275]: <info>  [1701392753.6759] manager: NetworkManager state is now CONNECTED_GLOBAL
12月 01 12:15:23 duanyao-lt-d NetworkManager[1275]: <info>  [1701404123.6229] manager: NetworkManager state is now CONNECTED_LOCAL
12月 01 12:15:23 duanyao-lt-d NetworkManager[1275]: <info>  [1701404123.9566] manager: NetworkManager state is now CONNECTING
12月 01 12:15:32 duanyao-lt-d NetworkManager[1275]: <info>  [1701404132.2980] manager: NetworkManager state is now CONNECTED_SITE
12月 01 12:15:33 duanyao-lt-d NetworkManager[1275]: <info>  [1701404133.6090] manager: NetworkManager state is now CONNECTED_GLOBAL

sudo journalctl -k -b | grep -e "lost" -e "associate with"
11月 30 17:57:56 duanyao-lt-d kernel: wlp3s0: associate with 42:bd:ea:25:55:ca (try 1/3)
11月 30 19:06:49 duanyao-lt-d kernel: wlp3s0: Connection to AP 42:bd:ea:25:55:ca lost
11月 30 19:06:53 duanyao-lt-d kernel: wlp3s0: associate with 42:bd:ea:25:55:ca (try 1/3)
11月 30 23:47:01 duanyao-lt-d kernel: wlp3s0: Connection to AP 42:bd:ea:25:55:ca lost
11月 30 23:47:05 duanyao-lt-d kernel: wlp3s0: associate with 42:bd:ea:25:55:ca (try 1/3)
12月 01 01:36:26 duanyao-lt-d kernel: wlp3s0: Connection to AP 42:bd:ea:25:55:ca lost
12月 01 01:36:45 duanyao-lt-d kernel: wlp3s0: associate with 42:75:b4:51:25:dd (try 1/3)
12月 01 09:03:59 duanyao-lt-d kernel: wlp3s0: Connection to AP 42:75:b4:51:25:dd lost
12月 01 09:04:00 duanyao-lt-d kernel: wlp3s0: associate with 42:75:b4:51:25:d9 (try 1/3)
12月 01 09:09:05 duanyao-lt-d kernel: wlp3s0: associate with 42:75:b4:51:25:dd (try 1/3)
12月 01 09:10:53 duanyao-lt-d kernel: wlp3s0: Connection to AP 42:75:b4:51:25:dd lost
12月 01 09:10:54 duanyao-lt-d kernel: wlp3s0: associate with 42:75:b4:51:25:d9 (try 1/3)
12月 01 09:16:00 duanyao-lt-d kernel: wlp3s0: associate with 42:75:b4:51:25:dd (try 1/3)
12月 01 12:15:31 duanyao-lt-d kernel: wlp3s0: associate with 42:bd:ea:25:55:ca (try 1/3)

nmcli d wifi
IN-USE  BSSID              SSID                          MODE   CHAN  RATE        SIGNAL  BARS  SECURITY  
        5C:02:14:79:B6:6E  BJCZ_5                        Infra  48    270 Mbit/s  85      ▂▄▆█  WPA2      
        42:75:B4:51:25:DD  BJCZ_4G_2                     Infra  149   270 Mbit/s  84      ▂▄▆█  WPA2      
        C2:6C:B3:18:26:D9  BJCZ_4G_3                     Infra  7     130 Mbit/s  82      ▂▄▆█  WPA2      
        42:75:B4:51:25:D9  BJCZ_4G_2                     Infra  12    130 Mbit/s  82      ▂▄▆█  WPA2      
        42:75:B4:51:25:E1  BJCZ_4G_2_5G                  Infra  149   270 Mbit/s  82      ▂▄▆█  WPA2      
        42:BD:EA:25:55:C6  BJCZ_4G_1                     Infra  149   270 Mbit/s  82      ▂▄▆█  WPA2      
*       42:BD:EA:25:55:CA  BJCZ_4G_1_5G                  Infra  149   270 Mbit/s  80      ▂▄▆_  WPA2      
        42:BD:EA:25:55:C2  BJCZ_4G_1                     Infra  10    130 Mbit/s  79      ▂▄▆_  WPA2      
        C2:6C:B3:18:26:E1  BJCZ_4G_3_5G                  Infra  149   270 Mbit/s  77      ▂▄▆_  WPA2      
        C2:6C:B3:18:26:DD  BJCZ_4G_3                     Infra  149   270 Mbit/s  77      ▂▄▆_  WPA2 
```

#### 修复2：停用深度节能模式
新建 rtw89 配置文件：
/usr/lib/modprobe.d/rtw89-fix-ps-1--dy.conf
内容是（根据 issue 226）：
```
options rtw89_pci disable_aspm_l1=y disable_aspm_l1ss=y
options rtw89pci disable_aspm_l1=y disable_aspm_l1ss=y
```

经过测试（2023-11-29, linux-firmware: 20230919.git3672ccab-0ubuntu2.4, ），仍然存在错误：
```
11月 29 17:40:57 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1000
11月 29 17:40:57 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: firmware failed to ack for leaving ps mode
11月 29 17:40:57 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1001
11月 29 17:40:57 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1002
11月 29 17:40:57 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: c2h class 1 func 3 not support

11月 29 17:54:58 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: no tx fwcmd resource
11月 29 17:54:58 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: failed to send h2c
```
仍出现断流。

或者（根据 issue 226,进一步 disable_clkreq）
```
options rtw89_pci disable_aspm_l1=y disable_aspm_l1ss=y disable_clkreq=y
options rtw89pci  disable_aspm_l1=y disable_aspm_l1ss=y disable_clkreq=y
```
仍出现断流。

重新加载 rtw89 内核模块：
```
lsmod | grep rtw

rtw89_8852ce           12288  0
rtw89_8852c           839680  1 rtw89_8852ce
rtw89_pci              77824  1 rtw89_8852ce
rtw89_core            614400  2 rtw89_pci,rtw89_8852c
mac80211             1724416  2 rtw89_core,rtw89_pci
cfg80211             1314816  3 rtw89_core,mac80211,rtw89_8852c

sudo modprobe -rv rtw89_8852ce

rmmod rtw89_8852ce
rmmod rtw89_pci
rmmod rtw89_8852c

sudo modprobe -v rtw89_8852ce

insmod /lib/modules/6.5.6-060506-generic/kernel/drivers/net/wireless/realtek/rtw89/rtw89_pci.ko.zst disable_aspm_l1=y disable_aspm_l1ss=y 
insmod /lib/modules/6.5.6-060506-generic/kernel/drivers/net/wireless/realtek/rtw89/rtw89_8852c.ko.zst 
insmod /lib/modules/6.5.6-060506-generic/kernel/drivers/net/wireless/realtek/rtw89/rtw89_8852ce.ko.zst
```

### 修复3：停用pcie深度节能模式+停用 rtw89_core 节能模式
新建 rtw89 配置文件：
/usr/lib/modprobe.d/rtw89-fix-ps-1--dy.conf
内容是（根据 issue 226, 271）：
```
options rtw89_pci disable_aspm_l1=y disable_aspm_l1ss=y disable_clkreq=y
options rtw89pci  disable_aspm_l1=y disable_aspm_l1ss=y disable_clkreq=y
options rtw89_core disable_ps_mode=y
options rtw89core disable_ps_mode=y
```

```
sudo modprobe -rv rtw89_8852ce

rmmod rtw89_8852ce
rmmod rtw89_pci
rmmod rtw89_8852c

sudo modprobe -rv rtw89_core # 如果失败 “rmmod: ERROR: Module xxx is in use by: yyy” 可以试试 sudo modprobe --remove-dependencies -f rtw89_core

rmmod rtw89_core
rmmod mac80211

sudo modprobe -v rtw89_core

insmod /lib/modules/6.5.6-060506-generic/kernel/net/mac80211/mac80211.ko.zst 
insmod /lib/modules/6.5.6-060506-generic/kernel/drivers/net/wireless/realtek/rtw89/rtw89_core.ko.zst disable_ps_mode=y rtw89_8852ce

sudo modprobe -v rtw89_8852ce

insmod /lib/modules/6.5.6-060506-generic/kernel/drivers/net/wireless/realtek/rtw89/rtw89_pci.ko.zst disable_aspm_l1=y disable_aspm_l1ss=y disable_clkreq=y 
insmod /lib/modules/6.5.6-060506-generic/kernel/drivers/net/wireless/realtek/rtw89/rtw89_8852c.ko.zst 
insmod /lib/modules/6.5.6-060506-generic/kernel/drivers/net/wireless/realtek/rtw89/rtw89_8852ce.ko.zst
```

内核日志未出现 rtw89 相关的错误，但仍出现断流，断流时没有内核日志。

不知是否相关的 NetworkManager 日志：
```
11月 30 16:44:00 duanyao-lt-d NetworkManager[1242]: <info>  [1701333840.2154] manager: NetworkManager state is now CONNECTED_SITE
11月 30 16:44:00 duanyao-lt-d dbus-daemon[1086]: [system] Activating via systemd: service name='org.freedesktop.nm_dispatcher' unit='dbus-org.freedesktop.nm-dispatcher.service' requested by ':1.11' (uid=0 pid=1242 comm="/usr/sbin/NetworkManager --no-daemon")
11月 30 16:44:00 duanyao-lt-d systemd[1]: Starting NetworkManager-dispatcher.service - Network Manager Script Dispatcher Service...
11月 30 16:44:00 duanyao-lt-d systemd[1]: Started NetworkManager-dispatcher.service - Network Manager Script Dispatcher Service.
11月 30 16:44:10 duanyao-lt-d systemd[1]: NetworkManager-dispatcher.service: Deactivated successfully.
```
NM_STATE_CONNECTED_SITE 意味着失去互联网连接：

https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/main/src/libnm-core-public/nm-dbus-interface.h#L150
```
 * @NM_STATE_CONNECTED_SITE: There is only site-wide IPv4 and/or IPv6 connectivity.
 *    This means a default route is available, but the Internet connectivity check
 *    (see "Connectivity" property) did not succeed. The graphical shell should
 *    indicate limited network connectivity.
 * @NM_STATE_CONNECTED_GLOBAL: There is global IPv4 and/or IPv6 Internet connectivity
 *    This means the Internet connectivity check succeeded, the graphical shell should
 *    indicate full network connectivity.
```

### 修复：手动更新 firmware
2024-10-24，更新前：
```
uname -a
Linux duanyao-lt-d 6.9.6-amd64-desktop-rolling

apt list --installed | grep 6.9.6-amd64-desktop-rolling
linux-image-6.9.6-amd64-desktop-rolling/未知,now 23.01.01.02 amd64 [已安装]

/lib/firmware/rtw89/rtw8852c_fw.bin

md5sum /lib/firmware/rtw89/rtw8852c_fw.bin
ceed5cd7b2066d7a8b99e73e9056d3c1  /lib/firmware/rtw89/rtw8852c_fw.bin

ls -l /lib/firmware/rtw89/rtw8852c_fw.bin
-rw-r--r-- 1 root root 1532736 2024年 3月15日 /lib/firmware/rtw89/rtw8852c_fw.bin

apt policy linux-firmware
linux-firmware:
  已安装：1.202.16
  候选： 1.202.16
  版本列表：
 *** 1.202.16 500
        500 https://community-packages.deepin.com/beige beige/main amd64 Packages
```

更新/更新后：
```
sudo cp -a /lib/firmware/rtw89 /lib/firmware/rtw89.bk-linux-firmware-1.202.16
sudo rsync -a /home/duanyao/project/linux-firmware.git/rtw89/ /lib/firmware/rtw89

diff -r /lib/firmware/rtw89 /lib/firmware/rtw89.bk-linux-firmware-1.202.16/
二进制文件 /lib/firmware/rtw89/rtw8851b_fw.bin 和 /lib/firmware/rtw89.bk-linux-firmware-1.202.16/rtw8851b_fw.bin 不同
二进制文件 /lib/firmware/rtw89/rtw8852b_fw-1.bin 和 /lib/firmware/rtw89.bk-linux-firmware-1.202.16/rtw8852b_fw-1.bin 不同
只在 /lib/firmware/rtw89 中存在：rtw8852bt_fw.bin
只在 /lib/firmware/rtw89 中存在：rtw8852c_fw-1.bin
二进制文件 /lib/firmware/rtw89/rtw8852c_fw.bin 和 /lib/firmware/rtw89.bk-linux-firmware-1.202.16/rtw8852c_fw.bin 不同
只在 /lib/firmware/rtw89 中存在：rtw8922a_fw-1.bin
只在 /lib/firmware/rtw89 中存在：rtw8922a_fw-2.bin
只在 /lib/firmware/rtw89 中存在：rtw8922a_fw.bin

diff -r /home/duanyao/project/linux-firmware.git/rtw89 /lib/firmware/rtw89
# 空

md5sum /lib/firmware/rtw89/rtw8852c_fw.bin
db00433f1d82eea5ad53f76e7247f6b5  /lib/firmware/rtw89/rtw8852c_fw.bin

ls -l /lib/firmware/rtw89/rtw8852c_fw.bin
-rw-r--r-- 1 duanyao duanyao 1532656 10月24日 13:43 /lib/firmware/rtw89/rtw8852c_fw.bin
```

更新后效果确认（2024-10-24 16:38）：
```
sudo journalctl -k -b | grep -e firmware -e rtw
10月 24 14:10:06 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: loaded firmware rtw89/rtw8852c_fw.bin
10月 24 14:10:06 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: enabling device (0000 -> 0003)
10月 24 14:10:06 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: Firmware version 0.27.56.14 (1942d927), cmd version 0, type 1
10月 24 14:10:06 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: Firmware version 0.27.56.14 (1942d927), cmd version 0, type 3
10月 24 14:10:06 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: chip rfe_type is 1
10月 24 14:10:06 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0 wlp3s0: renamed from wlan0
10月 24 14:27:02 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1000
10月 24 14:27:02 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: firmware failed to ack for leaving ps mode
10月 24 14:27:02 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1001
10月 24 14:27:02 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1002
10月 24 14:27:02 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: c2h class 1 func 3 not support
10月 24 15:52:02 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1000
10月 24 15:52:02 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: firmware failed to ack for leaving ps mode
10月 24 15:52:02 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: rtw89: failed to leave lps state
10月 24 15:52:02 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1001
10月 24 15:52:02 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1002
10月 24 15:52:02 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: c2h class 1 func 3 not support
10月 24 17:54:02 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1000
10月 24 17:54:02 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: firmware failed to ack for leaving ps mode
10月 24 17:54:02 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1001
10月 24 17:54:02 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1002
10月 24 17:54:02 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: c2h class 1 func 3 not support
10月 24 17:54:59 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: read rf busy swsi
10月 24 17:54:59 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: write rf busy swsi
10月 24 17:54:59 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: read rf busy swsi
10月 24 17:54:59 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: write rf busy swsi
10月 24 17:54:59 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: read rf busy swsi
10月 24 17:54:59 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: write rf busy swsi
10月 24 17:54:59 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: read rf busy swsi
10月 24 17:54:59 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: write rf busy swsi
10月 24 17:54:59 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: write rf busy swsi
10月 24 17:54:59 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: write rf busy swsi
10月 24 17:54:59 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: write rf busy swsi
10月 24 17:54:59 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: write rf busy swsi
10月 24 17:54:59 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: write rf busy swsi
10月 24 17:54:59 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: write rf busy swsi
10月 24 17:54:59 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: write rf busy swsi
10月 24 17:54:59 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: write rf busy swsi
10月 24 19:06:27 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1000
10月 24 19:06:27 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: firmware failed to ack for leaving ps mode
10月 24 19:06:27 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1001
10月 24 19:06:27 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1002
10月 24 19:06:27 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: c2h class 1 func 3 not support
10月 24 19:49:12 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1000
10月 24 19:49:12 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: firmware failed to ack for leaving ps mode
10月 24 19:49:12 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1001
10月 24 19:49:12 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1002
10月 24 19:49:12 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: c2h class 1 func 3 not support
10月 25 11:13:05 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1000
10月 25 11:13:05 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: firmware failed to ack for leaving ps mode
10月 25 11:13:05 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1001
10月 25 11:13:05 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: SER catches error: 0x1002
10月 25 11:13:05 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: c2h class 1 func 3 not support

sudo journalctl -k -b | grep -e "lost" -e "associate with"
10月 24 14:10:12 duanyao-lt-d kernel: wlp3s0: associate with c2:6c:b3:18:26:dd (try 1/3)
10月 24 17:51:19 duanyao-lt-d kernel: wlp3s0: Connection to AP c2:6c:b3:18:26:dd lost
10月 24 17:51:24 duanyao-lt-d kernel: wlp3s0: associate with c2:6c:b3:18:26:d9 (try 1/3)
10月 24 17:55:36 duanyao-lt-d kernel: wlp3s0: associate with c2:6c:b3:18:26:dd (try 1/3)
10月 24 19:52:03 duanyao-lt-d kernel: wlp3s0: Connection to AP c2:6c:b3:18:26:dd lost
10月 24 19:52:04 duanyao-lt-d kernel: wlp3s0: associate with c2:6c:b3:18:26:d9 (try 1/3)
10月 24 19:53:54 duanyao-lt-d kernel: wlp3s0: Connection to AP c2:6c:b3:18:26:d9 lost
10月 24 19:53:55 duanyao-lt-d kernel: wlp3s0: associate with c2:6c:b3:18:26:dd (try 1/3)
10月 24 19:58:55 duanyao-lt-d kernel: wlp3s0: Connection to AP c2:6c:b3:18:26:dd lost
10月 24 19:58:59 duanyao-lt-d kernel: wlp3s0: associate with c2:6c:b3:18:26:d9 (try 1/3)
10月 25 11:08:37 duanyao-lt-d kernel: wlp3s0: associate with c2:6c:b3:18:26:dd (try 1/3)

sudo journalctl -b | grep "NetworkManager state"
10月 24 14:10:11 duanyao-lt-d NetworkManager[808]: <info>  [1729750211.9231] manager: NetworkManager state is now CONNECTING
10月 24 14:10:13 duanyao-lt-d NetworkManager[808]: <info>  [1729750213.0366] manager: NetworkManager state is now CONNECTED_SITE
10月 24 14:10:13 duanyao-lt-d NetworkManager[808]: <info>  [1729750213.0370] manager: NetworkManager state is now CONNECTED_GLOBAL
10月 24 20:16:46 duanyao-lt-d NetworkManager[808]: <info>  [1729772206.9837] manager: NetworkManager state is now ASLEEP
10月 25 11:08:33 duanyao-lt-d NetworkManager[808]: <info>  [1729825713.6842] manager: NetworkManager state is now CONNECTED_LOCAL
10月 25 11:08:37 duanyao-lt-d NetworkManager[808]: <info>  [1729825717.6209] manager: NetworkManager state is now CONNECTING
10月 25 11:08:48 duanyao-lt-d NetworkManager[808]: <info>  [1729825728.2189] manager: NetworkManager state is now CONNECTED_SITE
10月 25 11:08:48 duanyao-lt-d NetworkManager[808]: <info>  [1729825728.2199] manager: NetworkManager state is now CONNECTED_GLOBAL
```
可见在20小时以内，网络连接较少发生自动断开。虽然仍有 “firmware failed to ack for leaving ps mode” 消息。

### 修复：安装外部 rtw dkms

2024-10-30
```
cd ~/project/rtw89/rtw89.git

# 检出 main 分支 
git log | head
commit d1fced1b8a741dc9f92b47c69489c24385945f6e
Author: Ping-Ke Shih <pkshih@realtek.com>
Date:   Sat Aug 24 23:04:49 2024 -0500
    Adjust MCS map condition in phy.c

# clean up your workspace
git clean -xfd

git deborig HEAD
dpkg-buildpackage -us -uc
sudo apt install ../rtw89-dkms_1.0.2-3_all.deb
```

创建 `/usr/lib/modprobe.d/rtw89-dkms.conf`，内容是禁用内核内置的 rtw89 驱动，`blacklist rtw89_core` 。

```
echo "blacklist rtw89_core
" | sudo tee /usr/lib/modprobe.d/rtw89-dkms.conf
```

效果确认：

```
uname -a
Linux duanyao-lt-d 6.9.6-amd64-desktop-rolling #23.01.01.02 SMP PREEMPT_DYNAMIC Tue Jul 23 12:20:02 CST 2024 x86_64 GNU/Linux

apt show rtw89-dkms 
Package: rtw89-dkms
Version: 1.0.2-3

lsmod | grep rtw
rtw_8852ce             12288  0
rtw_8852c             942080  1 rtw_8852ce
rtw89pci               90112  1 rtw_8852ce
rtw89core             761856  2 rtw_8852c,rtw89pci
mac80211             1294336  2 rtw89pci,rtw89core
cfg80211             1220608  3 rtw_8852c,rtw89core,mac80211

modinfo rtw89core
filename:       /lib/modules/6.9.6-amd64-desktop-rolling/updates/rtw89core.ko

sudo journalctl -k -b | grep -e firmware -e rtw
10月 29 19:30:03 duanyao-lt-d kernel: [drm] Loading DMUB firmware via PSP: version=0x08003000
10月 29 19:30:03 duanyao-lt-d kernel: [drm] Found VCN firmware Version ENC: 1.12 DEC: 5 VEP: 0 Revision: 0
10月 29 19:30:03 duanyao-lt-d kernel: amdgpu 0000:65:00.0: amdgpu: Will use PSP to load VCN firmware
10月 29 19:30:03 duanyao-lt-d kernel: rtw89_8852ce: disagrees about version of symbol rtw8852c_chip_info
10月 29 19:30:03 duanyao-lt-d kernel: rtw89_8852ce: Unknown symbol rtw8852c_chip_info (err -22)
10月 29 19:30:03 duanyao-lt-d kernel: rtw89_8852ce: disagrees about version of symbol rtw89_pci_fill_txaddr_info_v1
10月 29 19:30:03 duanyao-lt-d kernel: rtw89_8852ce: Unknown symbol rtw89_pci_fill_txaddr_info_v1 (err -22)
10月 29 19:30:03 duanyao-lt-d kernel: rtw89_8852ce: disagrees about version of symbol rtw89_pci_ltr_set_v1
10月 29 19:30:03 duanyao-lt-d kernel: rtw89_8852ce: Unknown symbol rtw89_pci_ltr_set_v1 (err -22)
10月 29 19:30:03 duanyao-lt-d kernel: rtw89_8852ce: disagrees about version of symbol rtw89_pci_gen_ax
10月 29 19:30:03 duanyao-lt-d kernel: rtw89_8852ce: Unknown symbol rtw89_pci_gen_ax (err -22)
10月 29 19:30:03 duanyao-lt-d kernel: rtw89_8852ce: disagrees about version of symbol rtw89_pci_config_intr_mask_v1
10月 29 19:30:03 duanyao-lt-d kernel: rtw89_8852ce: Unknown symbol rtw89_pci_config_intr_mask_v1 (err -22)
10月 29 19:30:03 duanyao-lt-d kernel: rtw89_8852ce: disagrees about version of symbol rtw89_pci_disable_intr_v1
10月 29 19:30:03 duanyao-lt-d kernel: rtw89_8852ce: Unknown symbol rtw89_pci_disable_intr_v1 (err -22)
10月 29 19:30:03 duanyao-lt-d kernel: rtw89_8852ce: disagrees about version of symbol rtw89_pci_enable_intr_v1
10月 29 19:30:03 duanyao-lt-d kernel: rtw89_8852ce: Unknown symbol rtw89_pci_enable_intr_v1 (err -22)
10月 29 19:30:03 duanyao-lt-d kernel: rtw89_8852ce: disagrees about version of symbol rtw89_pci_recognize_intrs_v1
10月 29 19:30:03 duanyao-lt-d kernel: rtw89_8852ce: Unknown symbol rtw89_pci_recognize_intrs_v1 (err -22)
10月 29 19:30:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: loaded firmware rtw89/rtw8852c_fw.bin
10月 29 19:30:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: enabling device (0000 -> 0003)
10月 29 19:30:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: Firmware version 0.27.56.14 (1942d927), cmd version 0, type 1
10月 29 19:30:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: Firmware version 0.27.56.14 (1942d927), cmd version 0, type 3
10月 29 19:30:03 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: chip rfe_type is 1
10月 29 19:30:04 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0 wlp3s0: renamed from wlan0
```
尽管存在 “disagrees about version of symbol” 和 “Unknown symbol” 错误，但网络连接正常。

```
modinfo rtw89_8852ce
filename:       /lib/modules/6.9.6-amd64-desktop-rolling/kernel/drivers/net/wireless/realtek/rtw89/rtw89_8852ce.ko
```
rtw89_8852ce 是内核内置rtw89的一部分，不应该被加载（启动完成后就不见了），因此也添加到 blacklist 中：
```
echo "blacklist rtw89_core
blacklist rtw89_8852ce
" | sudo tee /usr/lib/modprobe.d/rtw89-dkms.conf
```

验证效果：
```
sudo journalctl -k -b | grep -e firmware -e rtw
10月 30 11:52:12 duanyao-lt-d kernel: [drm] Loading DMUB firmware via PSP: version=0x08003000
10月 30 11:52:12 duanyao-lt-d kernel: [drm] Found VCN firmware Version ENC: 1.12 DEC: 5 VEP: 0 Revision: 0
10月 30 11:52:12 duanyao-lt-d kernel: amdgpu 0000:65:00.0: amdgpu: Will use PSP to load VCN firmware
10月 30 11:52:12 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: loaded firmware rtw89/rtw8852c_fw.bin
10月 30 11:52:12 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: enabling device (0000 -> 0003)
10月 30 11:52:12 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: Firmware version 0.27.56.14 (1942d927), cmd version 0, type 1
10月 30 11:52:12 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: Firmware version 0.27.56.14 (1942d927), cmd version 0, type 3
10月 30 11:52:12 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0: chip rfe_type is 1
10月 30 11:52:12 duanyao-lt-d kernel: rtw89_8852ce 0000:03:00.0 wlp3s0: renamed from wlan0

lsmod | grep rtw
rtw_8852ce             12288  0
rtw_8852c             942080  1 rtw_8852ce
rtw89pci               90112  1 rtw_8852ce
rtw89core             761856  2 rtw_8852c,rtw89pci
mac80211             1294336  2 rtw89pci,rtw89core
cfg80211             1220608  3 rtw_8852c,rtw89core,mac80211
```
rtw89_8852ce 的 “disagrees about version of symbol” 和 “Unknown symbol” 错误消失了。

```
sudo journalctl -k -b | grep -e "lost" -e "associate with"
10月 30 11:52:18 duanyao-lt-d kernel: wlp3s0: associate with 5c:02:14:79:b6:6e (try 1/3)
10月 30 12:15:08 duanyao-lt-d kernel: wlp3s0: Connection to AP 5c:02:14:79:b6:6e lost
10月 30 12:15:09 duanyao-lt-d kernel: wlp3s0: associate with 5c:02:14:79:b6:6f (try 1/3)
10月 30 12:16:03 duanyao-lt-d kernel: wlp3s0: Connection to AP 5c:02:14:79:b6:6f lost
10月 30 12:16:03 duanyao-lt-d kernel: wlp3s0: associate with 5c:02:14:79:b6:6e (try 1/3)

sudo journalctl -b | grep "NetworkManager state"
10月 30 11:52:18 duanyao-lt-d NetworkManager[815]: <info>  [1730260338.0516] manager: NetworkManager state is now CONNECTING
10月 30 11:52:19 duanyao-lt-d NetworkManager[815]: <info>  [1730260339.0954] manager: NetworkManager state is now CONNECTED_SITE
10月 30 11:52:19 duanyao-lt-d NetworkManager[815]: <info>  [1730260339.0958] manager: NetworkManager state is now CONNECTED_GLOBAL
```

### intel 3165 无线网卡使用中突然不可用
内核版本：
```
$ uname -a
Linux duanyao-laptop-c 4.15.0-30deepin-generic #31 SMP Fri Nov 30 04:29:02 UTC 2018 x86_64 GNU/Linux
```

现象是：无线网卡设备从操作系统中消失：
```
$ ifconfig

$ iwconfig

$ iw phy
```
以上3命令中都不再显示无线网卡设备。

以下命令还可以看到无线网卡的存在：
```
$ lshw -C network
WARNING: you should run this program as super-user.
  *-network                 
       description: Ethernet interface
       product: RTL8101/2/6E PCI Express Fast/Gigabit Ethernet controller
       vendor: Realtek Semiconductor Co., Ltd.
       physical id: 0
       bus info: pci@0000:03:00.0
       logical name: eno1
       version: 0a
       serial: 98:e7:f4:50:78:d5
       size: 10Mbit/s
       capacity: 100Mbit/s
       width: 64 bits
       clock: 33MHz
       capabilities: bus_master cap_list ethernet physical tp mii 10bt 10bt-fd 100bt 100bt-fd autonegotiation
       configuration: autonegotiation=on broadcast=yes driver=r8169 driverversion=2.3LK-NAPI duplex=half firmware=rtl8107e-2_0.0.2 02/26/15 latency=0 link=no multicast=yes port=MII speed=10Mbit/s
       resources: irq:17 ioport:3000(size=256) memory:a4104000-a4104fff memory:a4100000-a4103fff
  *-network UNCLAIMED
       description: Network controller
       product: Wireless 3165
       vendor: Intel Corporation
       physical id: 0
       bus info: pci@0000:04:00.0
       version: 81
       width: 64 bits
       clock: 33MHz
       capabilities: cap_list
       configuration: latency=0
       resources: memory:a4000000-a4001fff
  *-network
       description: Ethernet interface
       physical id: 1
       logical name: docker0
       serial: 02:42:19:e8:67:07
       capabilities: ethernet physical
       configuration: broadcast=yes driver=bridge driverversion=2.3 firmware=N/A ip=172.17.0.1 link=no multicast=yes
WARNING: output may be incomplete or inaccurate, you should run this program as super-user.

$ lspci
04:00.0 Network controller: Intel Corporation Wireless 3165 (rev 81)
````

dmesg 可以参考错误：
````
$ dmesg

[14171.808381] WARNING: CPU: 2 PID: 8558 at /build/linux-4.15.0/net/mac80211/driver-ops.c:39 drv_stop+0xed/0x100 [mac80211]
[14171.808381] Modules linked in: snd_hrtimer snd_seq snd_seq_device rfcomm xt_conntrack ipt_MASQUERADE nf_nat_masquerade_ipv4 nf_conntrack_netlink nfnetlink xt_addrtype iptable_filter iptable_nat nf_conntrack_ipv4 nf_defrag_ipv4 nf_nat_ipv4 nf_nat nf_conntrack br_netfilter bridge stp llc xfrm_user xfrm4_tunnel tunnel4 ipcomp xfrm_ipcomp esp4 ah4 af_key xfrm_algo ccm lz4 lz4_compress cmac bnep aufs overlay uvcvideo videobuf2_vmalloc btusb videobuf2_memops videobuf2_v4l2 btrtl btbcm videobuf2_core btintel videodev bluetooth zram media ecdh_generic snd_soc_skl snd_soc_skl_ipc intel_rapl snd_hda_ext_core x86_pkg_temp_thermal snd_soc_sst_dsp snd_soc_sst_ipc intel_powerclamp snd_soc_acpi coretemp snd_soc_core snd_compress kvm_intel ac97_bus kvm snd_pcm_dmaengine irqbypass snd_hda_codec_hdmi nls_iso8859_1
[14171.808407]  crct10dif_pclmul snd_hda_codec_realtek snd_hda_codec_generic arc4 snd_hda_intel crc32_pclmul snd_hda_codec wl(POE) iwlmvm ghash_clmulni_intel snd_hda_core mac80211 snd_hwdep intel_cstate iwlwifi snd_pcm intel_rapl_perf hp_wmi joydev input_leds snd_timer sparse_keymap serio_raw wmi_bmof snd cfg80211 rtsx_pci_ms soundcore memstick mei_me processor_thermal_device shpchp mei intel_pch_thermal int340x_thermal_zone intel_soc_dts_iosf hp_accel lis3lv02d int3400_thermal input_polldev hp_wireless mac_hid acpi_pad acpi_thermal_rel nvidia_drm(POE) nvidia_modeset(POE) nvidia(POE) ipmi_devintf ipmi_msghandler parport_pc ppdev lp parport binfmt_misc vfs_monitor(OE) ip_tables x_tables autofs4 btrfs zstd_compress raid10 raid456 async_raid6_recov async_memcpy async_pq async_xor async_tx xor raid6_pq
[14171.808432]  libcrc32c raid1 raid0 multipath linear hid_generic usbhid hid rtsx_pci_sdmmc i915 aesni_intel aes_x86_64 crypto_simd cryptd glue_helper i2c_algo_bit drm_kms_helper psmouse syscopyarea sysfillrect i2c_i801 sysimgblt fb_sys_fops r8169 ahci mii drm rtsx_pci libahci wmi video [last unloaded: mincores]
[14171.808446] CPU: 2 PID: 8558 Comm: kworker/2:2 Tainted: P        W  OE    4.15.0-30deepin-generic #31
[14171.808446] Hardware name: HP HP Pavilion Notebook/820A, BIOS F.52 05/02/2019
[14171.808454] Workqueue: events_freezable ieee80211_restart_work [mac80211]
[14171.808462] RIP: 0010:drv_stop+0xed/0x100 [mac80211]
[14171.808463] RSP: 0018:ffffad3c8506fc10 EFLAGS: 00010246
[14171.808464] RAX: 0000000000000000 RBX: ffff9a573260c760 RCX: 0000000000000000
[14171.808465] RDX: ffff9a572af88000 RSI: 0000000000000286 RDI: ffff9a573260c760
[14171.808466] RBP: ffffad3c8506fc20 R08: 0000000000000000 R09: 0000000000000000
[14171.808467] R10: ffffad3c8506fc30 R11: ffff9a5735c02938 R12: ffff9a573260cff0
[14171.808468] R13: ffff9a573260cee8 R14: ffff9a573260c760 R15: ffff9a57323ed380
[14171.808469] FS:  0000000000000000(0000) GS:ffff9a573ed00000(0000) knlGS:0000000000000000
[14171.808470] CS:  0010 DS: 0000 ES: 0000 CR0: 0000000080050033
[14171.808470] CR2: 000055f8646400b0 CR3: 000000002b40a004 CR4: 00000000003606e0
[14171.808471] Call Trace:
[14171.808481]  ieee80211_stop_device+0x43/0x50 [mac80211]
[14171.808490]  ieee80211_do_stop+0x4c5/0x7f0 [mac80211]
[14171.808492]  ? qdisc_reset+0x2b/0x70
[14171.808501]  ieee80211_stop+0x1a/0x20 [mac80211]
[14171.808503]  __dev_close_many+0xa5/0x110
[14171.808504]  dev_close_many+0x8c/0x140
[14171.808506]  dev_close.part.89+0x4a/0x70
[14171.808507]  dev_close+0x18/0x20
[14171.808515]  cfg80211_shutdown_all_interfaces+0x49/0xc0 [cfg80211]
[14171.808525]  ieee80211_handle_reconfig_failure+0x98/0xb0 [mac80211]
[14171.808535]  ieee80211_reconfig+0x8c/0xf60 [mac80211]
[14171.808543]  ieee80211_restart_work+0x9a/0xd0 [mac80211]
[14171.808545]  process_one_work+0x1e0/0x400
[14171.808546]  worker_thread+0x4b/0x420
[14171.808548]  kthread+0x105/0x140
[14171.808549]  ? process_one_work+0x400/0x400
[14171.808551]  ? kthread_create_worker_on_cpu+0x70/0x70
[14171.808553]  ? do_syscall_64+0x73/0x130
[14171.808554]  ? SyS_exit_group+0x14/0x20
[14171.808555]  ret_from_fork+0x35/0x40
[14171.808556] Code: 63 09 00 4d 85 e4 74 1e 49 8b 04 24 49 8b 7c 24 08 49 83 c4 18 48 89 de e8 a1 77 60 d2 49 8b 04 24 48 85 c0 75 e6 e9 51 ff ff ff <0f> 0b 5b 41 5c 5d c3 66 90 66 2e 0f 1f 84 00 00 00 00 00 0f 1f 
[14171.808582] ---[ end trace 86a858d250587e8b ]---
[14171.922653] iwlwifi 0000:04:00.0: loaded firmware version 29.1044073957.0 op_mode iwlmvm
[14171.922671] iwlwifi 0000:04:00.0: Detected Intel(R) Dual Band Wireless AC 3165, REV=0x210
[14171.941675] iwlwifi 0000:04:00.0: base HW address: 68:07:15:c9:33:a8
[14172.154309] iwlwifi 0000:04:00.0: Microcode SW error detected.  Restarting 0x2000000.
[14172.154444] iwlwifi 0000:04:00.0: Start IWL Error Log Dump:
[14172.154447] iwlwifi 0000:04:00.0: Status: 0x00000100, count: 6
[14172.154448] iwlwifi 0000:04:00.0: Loaded firmware version: 29.1044073957.0
[14172.154450] iwlwifi 0000:04:00.0: 0x00000034 | NMI_INTERRUPT_WDG           
[14172.154452] iwlwifi 0000:04:00.0: 0x000002F0 | trm_hw_status0
[14172.154453] iwlwifi 0000:04:00.0: 0x00000000 | trm_hw_status1
[14172.154455] iwlwifi 0000:04:00.0: 0x00041702 | branchlink2
[14172.154456] iwlwifi 0000:04:00.0: 0x00044696 | interruptlink1
[14172.154458] iwlwifi 0000:04:00.0: 0x000069D6 | interruptlink2
[14172.154459] iwlwifi 0000:04:00.0: 0x00000000 | data1
[14172.154461] iwlwifi 0000:04:00.0: 0x00000002 | data2
[14172.154462] iwlwifi 0000:04:00.0: 0x07030000 | data3
[14172.154463] iwlwifi 0000:04:00.0: 0x00000000 | beacon time
[14172.154465] iwlwifi 0000:04:00.0: 0x000352EB | tsf low
[14172.154466] iwlwifi 0000:04:00.0: 0x00000000 | tsf hi
[14172.154468] iwlwifi 0000:04:00.0: 0x00000000 | time gp1
[14172.154469] iwlwifi 0000:04:00.0: 0x000352EB | time gp2
[14172.154471] iwlwifi 0000:04:00.0: 0x00000009 | uCode revision type
[14172.154472] iwlwifi 0000:04:00.0: 0x0000001D | uCode version major
[14172.154474] iwlwifi 0000:04:00.0: 0x3E3B4DE5 | uCode version minor
[14172.154475] iwlwifi 0000:04:00.0: 0x00000210 | hw version
[14172.154477] iwlwifi 0000:04:00.0: 0x00C89200 | board version
[14172.154478] iwlwifi 0000:04:00.0: 0x0012016A | hcmd
[14172.154480] iwlwifi 0000:04:00.0: 0x02023080 | isr0
[14172.154481] iwlwifi 0000:04:00.0: 0x00000000 | isr1
[14172.154483] iwlwifi 0000:04:00.0: 0x00000002 | isr2
[14172.154484] iwlwifi 0000:04:00.0: 0x004000C0 | isr3
[14172.154486] iwlwifi 0000:04:00.0: 0x00000080 | isr4
[14172.154487] iwlwifi 0000:04:00.0: 0x0012016A | last cmd Id
[14172.154489] iwlwifi 0000:04:00.0: 0x00000000 | wait_event
[14172.154490] iwlwifi 0000:04:00.0: 0x000000D4 | l2p_control
[14172.154492] iwlwifi 0000:04:00.0: 0x00000000 | l2p_duration
[14172.154493] iwlwifi 0000:04:00.0: 0x00000000 | l2p_mhvalid
[14172.154495] iwlwifi 0000:04:00.0: 0x00000000 | l2p_addr_match
[14172.154496] iwlwifi 0000:04:00.0: 0x00000007 | lmpm_pmg_sel
[14172.154498] iwlwifi 0000:04:00.0: 0x14031202 | timestamp
[14172.154499] iwlwifi 0000:04:00.0: 0x00341020 | flow_handler
[14172.154535] iwlwifi 0000:04:00.0: Failed to run INIT calibrations: -5
[14172.166857] iwlwifi 0000:04:00.0: Failed to run INIT ucode: -5
```

modinfo 看不出问题：

```
$ modinfo iwlwifi
filename:       /lib/modules/4.15.0-30deepin-generic/kernel/drivers/net/wireless/intel/iwlwifi/iwlwifi.ko
license:        GPL
author:         Copyright(c) 2003- 2015 Intel Corporation <linuxwifi@intel.com>
description:    Intel(R) Wireless WiFi driver for Linux
firmware:       iwlwifi-100-5.ucode
firmware:       iwlwifi-1000-5.ucode
firmware:       iwlwifi-135-6.ucode
firmware:       iwlwifi-105-6.ucode
...
```

尝试修复（重新加载驱动）：
```
$ sudo rmmod iwlmvm iwlwifi
$ sudo modprobe iwlwifi

$ demsg

[15142.588437] Intel(R) Wireless WiFi driver for Linux
[15142.588439] Copyright(c) 2003- 2015 Intel Corporation
[15142.589798] iwlwifi 0000:04:00.0: loaded firmware version 29.1044073957.0 op_mode iwlmvm
[15142.600078] iwlwifi 0000:04:00.0: Detected Intel(R) Dual Band Wireless AC 3165, REV=0x210
[15142.622963] iwlwifi 0000:04:00.0: base HW address: 68:07:15:c9:33:a8
[15142.835923] iwlwifi 0000:04:00.0: Microcode SW error detected.  Restarting 0x2000000.
[15142.836060] iwlwifi 0000:04:00.0: Start IWL Error Log Dump:
[15142.836062] iwlwifi 0000:04:00.0: Status: 0x00000100, count: 6
[15142.836063] iwlwifi 0000:04:00.0: Loaded firmware version: 29.1044073957.0
[15142.836065] iwlwifi 0000:04:00.0: 0x00000034 | NMI_INTERRUPT_WDG           
[15142.836067] iwlwifi 0000:04:00.0: 0x000002F0 | trm_hw_status0
[15142.836068] iwlwifi 0000:04:00.0: 0x00000000 | trm_hw_status1
[15142.836070] iwlwifi 0000:04:00.0: 0x00041702 | branchlink2
[15142.836071] iwlwifi 0000:04:00.0: 0x00044696 | interruptlink1
[15142.836073] iwlwifi 0000:04:00.0: 0x0000C65A | interruptlink2
[15142.836074] iwlwifi 0000:04:00.0: 0x00000000 | data1
[15142.836076] iwlwifi 0000:04:00.0: 0x00000002 | data2
[15142.836077] iwlwifi 0000:04:00.0: 0x07030000 | data3
[15142.836079] iwlwifi 0000:04:00.0: 0x00000000 | beacon time
[15142.836080] iwlwifi 0000:04:00.0: 0x0003578E | tsf low
[15142.836082] iwlwifi 0000:04:00.0: 0x00000000 | tsf hi
[15142.836083] iwlwifi 0000:04:00.0: 0x00000000 | time gp1
[15142.836085] iwlwifi 0000:04:00.0: 0x0003578E | time gp2
[15142.836086] iwlwifi 0000:04:00.0: 0x00000009 | uCode revision type
[15142.836088] iwlwifi 0000:04:00.0: 0x0000001D | uCode version major
[15142.836089] iwlwifi 0000:04:00.0: 0x3E3B4DE5 | uCode version minor
[15142.836091] iwlwifi 0000:04:00.0: 0x00000210 | hw version
[15142.836092] iwlwifi 0000:04:00.0: 0x00C89200 | board version
[15142.836094] iwlwifi 0000:04:00.0: 0x0012016A | hcmd
[15142.836095] iwlwifi 0000:04:00.0: 0x02023080 | isr0
[15142.836097] iwlwifi 0000:04:00.0: 0x00000000 | isr1
[15142.836098] iwlwifi 0000:04:00.0: 0x00000002 | isr2
[15142.836100] iwlwifi 0000:04:00.0: 0x004000C0 | isr3
[15142.836101] iwlwifi 0000:04:00.0: 0x00000080 | isr4
[15142.836102] iwlwifi 0000:04:00.0: 0x0012016A | last cmd Id
[15142.836104] iwlwifi 0000:04:00.0: 0x00000000 | wait_event
[15142.836105] iwlwifi 0000:04:00.0: 0x000000D4 | l2p_control
[15142.836107] iwlwifi 0000:04:00.0: 0x00000000 | l2p_duration
[15142.836108] iwlwifi 0000:04:00.0: 0x00000000 | l2p_mhvalid
[15142.836110] iwlwifi 0000:04:00.0: 0x00000000 | l2p_addr_match
[15142.836111] iwlwifi 0000:04:00.0: 0x00000007 | lmpm_pmg_sel
[15142.836113] iwlwifi 0000:04:00.0: 0x14031202 | timestamp
[15142.836114] iwlwifi 0000:04:00.0: 0x00341020 | flow_handler
[15142.836127] iwlwifi 0000:04:00.0: Failed to run INIT calibrations: -5
[15142.848394] iwlwifi 0000:04:00.0: Failed to run INIT ucode: -5

```

重点可能是最后两行：

```
Failed to run INIT calibrations: -5
Failed to run INIT ucode: -5
```

NMI_INTERRUPT_WDG 可能也反映了一个bug [9.1]

### NetworkManager 保存的wifi密码

NetworkManager默认在连接文件/etc/NetworkManager/system-connections/中以明文存储密码。要打印存储的密码，使用下列命令：

```
sudo grep -H '^psk=' /etc/NetworkManager/system-connections/*
```

GUI 客户端 nm-applet/nm-connection-editor 和 deepin 的连接设置都可以查看 wifi 密码。

命令行：
```
nmcli -s -g 802-11-wireless-security.psk connection show <connection name>
```

## NetworkManager 启动和关闭 wifi

```
nmcli radio wifi off
nmcli radio wifi on
nmcli radio help
```

## NetworkManager 检查 wifi 状态并自动重启
参考[10.1]。

/usr/local/bin/wifi-arc.sh

```
#!/bin/bash
gateway=192.168.1.1
fping -c 5 -t 100 ${gateway}
received=$?
#echo $received
if [[ $received -ne 0 ]] ; then
    echo "warning: fping ${gateway} timeout or failed, restarting wifi"
    nmcli radio wifi off && nmcli radio wifi on
else
    echo "fping ${gateway} ok"
fi
```

/etc/systemd/system/wifi-arc.timer
```
[Unit]
Description=wifi-auto-reconnect

[Timer]
OnBootSec=2min
OnUnitActiveSec=2min

[Install]
WantedBy=timers.target
```

/etc/systemd/system/wifi-arc.service
```
[Unit]
Description=wifi-auto-reconnect

[Service]
Type=simple
ExecStart=/usr/local/bin/wifi-arc.sh
```

## 频率选择

对于拥有多个频率的 SSID，linux 可以要求只连接 2.4G 或 5G，或者自动选择（一般优先5G）。这可以通过 nm-connection-editor (GUI) 来设置。

## 无线路由器的工作模式

大部分无线路由器有多个工作模式：

* 路由模式。无线路由器的一个有线网口连接上级网络设备，从而连通外网；无线路由器的无线信号和其它有线网口组成一个内网，无线路由器充当路由器、默认网关、DHCP服务器、NAT等职责（除了路由器，其它职责是可选的，但一般是启用的）。
* 桥接模式。与路由模式类似，但是此无线路由器还负责上级网络设备的拨号，即PPPoE。上级网络设备通常是光猫、ADSL猫等。
* AP 模式。无线路由器的一个有线网口连接上级网络设备，但自身不充当路由器等职责（由上级设备负责），仅充当无线 AP。
* 中继模式。此无线路由器连接上级无线网络，自身也产生一个无线网络，且通常与上级的ssid和密码相同，起到扩展上级无线网络的范围以及无线转有线的功能。此无线路由器的有线网口不连接上级网络设备，但可以连接下级网络设备。此无线路由器相当于交换机，没有路由等职责。此无线路由器的无线频段、ssid和密码可以与上级路由器不同，但一般为了方便设置为相同。
  中继模式下，DHCP服务器由上级网络提供，但由于未知的原因，上级的DHCP服务器可能不响应中继路由器下的客户端，造成客户端无法获得IP地址。客户端设置静态IP地址可能有用。
