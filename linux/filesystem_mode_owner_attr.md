## 参考资料

[5.1] How to recursively chmod all directories except files?
  https://superuser.com/questions/91935/how-to-recursively-chmod-all-directories-except-files

[6.2] root-cannot-remove-file-on-ext4
  https://unix.stackexchange.com/questions/287129/root-cannot-remove-file-on-ext4
  
[7.2] Linux 文件系统扩展属性 xattr
  https://www.cnblogs.com/xuyaowen/p/linux-xattrs.html

## 命令行工具

### chmod

* 递归修改[5.1]

  难点是目录一般必须有x权限，而文件一般不需要有，但chmod一般是一视同仁的。解决办法：X 标志：
  ```
  chmod -R u=rw,go=r,a+X path
  ```
  `-R`: 递归；`u=rw,go=r` 设置权限为 `644`，不分目录和文件；`a+X` 对目录对任何用户增加 x 权限，对文件不动。


## 扩展属性

Linux 文件系统扩展属性 xattr [7.2] 。

## ext2/3/4 属性

类似扩展属性，但它是 ext2/3/4 特有的。可以用 lsattr 查看，chattr 修改，这些命令属于 e2fsprogs 包。

ext2 属性中有一些可阻止文件被删除，即使是 root 用户，例如 i 和 a 属性。可以用 `chattr -i path` 和 `chattr -a path` 去除。
