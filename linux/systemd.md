[1.1] systemd.unit — Unit configuration
  https://www.freedesktop.org/software/systemd/man/systemd.unit.html

[1.2] systemd.service — Service unit configuration
  https://www.freedesktop.org/software/systemd/man/systemd.service.html

[1.3] systemd.service - Service unit configuration 
  https://jlk.fjfi.cvut.cz/arch/manpages/man/systemd.service.5.en

[1.4] systemd.timer - Timer unit configuration 
  https://jlk.fjfi.cvut.cz/arch/manpages/man/systemd.timer.5

[1.7] systemd.time - Time and date specifications 
  https://jlk.fjfi.cvut.cz/arch/manpages/man/systemd.time.7.en

[1.8] openSUSE:How to write a systemd service
  https://zh.opensuse.org/openSUSE:How_to_write_a_systemd_service
  
[1.9] Understanding systemd at startup on Linux
  https://opensource.com/article/20/5/systemd-startup
  
[1.10] Learning to love systemd
  https://opensource.com/article/20/4/systemd

[2.1] wiki.archlinux systemd
https://wiki.archlinux.org/index.php/Systemd_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)
https://wiki.archlinux.org/index.php/Systemd

[2.4] systemd/Timers
  https://wiki.archlinux.org/index.php/Systemd/Timers

[2.5] 使用 systemd timer 备份数据库
  https://blog.csdn.net/linuxnews/article/details/51818282

[2.6] Start/stop a systemd.service at specific times
  https://unix.stackexchange.com/questions/265704/start-stop-a-systemd-service-at-specific-times

[2.7] Systemd Timers: Three Use Cases
  https://www.linux.com/blog/intro-to-linux/2018/8/systemd-timers-two-use-cases-0

[3.1] How to set ulimits on service with systemd?
  https://unix.stackexchange.com/questions/345595/how-to-set-ulimits-on-service-with-systemd

[4.0] journalctl/journald
[4.3] How to have line breaks in output of journalctl log
  https://bbs.archlinux.org/viewtopic.php?id=170061
  
[4.4] Beginner's Guide to Analyzing Logs in Linux With journalctl Command
  https://linuxhandbook.com/journalctl-command/

[4.5] How to Log to Only Systemd Log Files in Linux
  https://www.baeldung.com/linux/systemd-journald-capture-logs

[11] How To Deploy Node.js Applications Using Systemd and Nginx
https://www.digitalocean.com/community/tutorials/how-to-deploy-node-js-applications-using-systemd-and-nginx

[12] Deploying Node.js with systemd
  https://rocketeer.be/articles/deploying-node-js-with-systemd/

[13] systemd-arch-units/service/squid.service
  https://github.com/devkral/systemd-arch-units/blob/master/service/squid.service

[14] Can't start Squid with workers via systemd on CentOS 7
  http://squid-web-proxy-cache.1019090.n4.nabble.com/Can-t-start-Squid-with-workers-via-systemd-on-CentOS-7-td4681711.html

[15] Missing logs?!? Learning about linux logging systems
  https://nickcanzoneri.com/centos/logging/journald/rsyslog/2017/08/18/losing-log-messages.html

## 基本工具
[2.1] 
显示 系统状态：

$ systemctl status

输出激活的单元：

$ systemctl
$ systemctl list-units

输出运行失败的单元：

systemctl --failed
systemctl list-units --failed
systemctl list-units --state=failed

输出所有的 enable 的单元：
systemctl --all
systemctl list-units --all

所有可用的单元文件存放在 /usr/lib/systemd/system/ 和 /etc/systemd/system/ 目录（后者优先级更高）。查看所有已安装服务：

$ systemctl list-unit-files


使用单元

一个单元配置文件可以描述如下内容之一：系统服务（.service）、挂载点（.mount）、sockets（.sockets） 、系统设备（.device）、交换分区（.swap）、文件路径（.path）、启动目标（.target）、由 systemd 管理的计时器（.timer）。详情参阅 systemd.unit(5) 。

使用 systemctl 控制单元时，通常需要使用单元文件的全名，包括扩展名（例如 sshd.service ）。但是有些单元可以在 systemctl 中使用简写方式。

    如果无扩展名，systemctl 默认把扩展名当作 .service 。例如 netcfg 和 netcfg.service 是等价的。
    挂载点会自动转化为相应的 .mount 单元。例如 /home 等价于 home.mount 。
    设备会自动转化为相应的 .device 单元，所以 /dev/sda2 等价于 dev-sda2.device 。

```
立即激活单元：

# systemctl start <单元>

立即停止单元：

# systemctl stop <单元>

重启单元：

# systemctl restart <单元>

重新加载配置：

# systemctl reload <单元>

输出单元运行状态：

$ systemctl status <单元>

检查单元是否配置为自动启动：

$ systemctl is-enabled <单元>

开机自动激活单元：

# systemctl enable <单元>

设置单元为自动启动并立即启动这个单元:

# systemctl enable --now unit
```

修改单元文件后，重新加载：

```
# systemctl daemon-reload
```

## 服务单元

```
[Unit]
Description=foo.js

[Service]
Type=simple
ExecStart=/usr/bin/node /usr/local/lib/foo/foo.js
Restart=always

#User=foo
#Group=foo
#Environment=NODE_ENV=production

[Install]
WantedBy=multi-user.target
```

注意， `ExecStart=` 中的可执行文件需要用绝对路径，这是 systemd 要求的。

## 环境变量
systemd 服务文件不会自动使用任何环境变量设置，例如 `/etc/environment` ，因此必须在服务文件中明确定义，或明确导入环境变量设置文件，例如：

```
[Service]
EnvironmentFile=-/etc/environment
```

文件路径前面的 `-` 表示忽略文件不存在的情形。不过严格来说，/etc/environment 的语法与 systemd EnvironmentFile 不完全相同。

一个可能比较重要的的环境变量是 LANG，例如

```
Environment=LANG=zh-CN.UTF-8
```

`ExecStart` 可以使用 Environment 或 EnvironmentFile 定义的环境变量，例如[1.2]：

```
Environment=ONE='one' "TWO='two two' too" THREE=
ExecStart=/bin/echo ${ONE} ${TWO} ${THREE}
ExecStart=/bin/echo $ONE $TWO $THREE
```

`${}` 可以用于展开有空格的环境变量。

但是，环境变量展开只能用于 ExecStart 的命令参数部分，不能用于命令的路径部分（?!）。
PIDFile 指令也中也不可以使用环境变量展开，否则报错。
Environment 指令中不可以使用环境变量展开，'$'会被认为是普通字符。

## 定时任务
定时任务的定义由成对的 x.timer 和 x.service 文件构成，其中 x.timer 是入口。

运行期间，会在 /var/lib/systemd/timers (或者 ~/.local/share/systemd/ 下）生成 stamp-* 文件来标记正在运行的任务。

列出定时任务：

```
systemctl list-timers
```

例子：周期执行的任务，参照点为启动时间：

```
#A timer which will start 15 minutes after boot and again every week while the system is running.

/etc/systemd/system/foo.timer

[Unit]
Description=Run foo weekly and on boot

[Timer]
OnBootSec=15min
OnUnitActiveSec=1w 

[Install]
WantedBy=timers.target
```

例子：周期执行的任务，参照点为日历时间：

```
#/etc/systemd/system/foo.timer

[Unit]
Description=Run foo weekly

[Timer]
OnCalendar=Mon *-*-* 00:00:00
OnCalendar=Sat,Sun 20:00
Persistent=true

[Install]
WantedBy=timers.target
```

注意到 OnCalendar 允许出现多次，可以一次指定多个执行的时间点。

对应的 service 文件：

```
#/etc/systemd/system/foo.service

[Unit]
Description=Backup database

[Service]
Type=simple
ExecStart=/usr/local/bin/db_backup
```

与持续运行的服务不同，没有 Restart=always 之类的设置。

`OnCalendar` 指示执行的时间，具体格式可参考“时间格式”一节。

新建 timer 后，需要用 systemctl enable 激活它（但不需要 systemctl start）。

用 systemctl 操作定时任务时，一般应针对 .timer 文件而不是 .service 文件（不带后缀时默认是后者）。例如

```
systemctl enable xxx.timer
systemctl status xxx.timer
```

查看日志：

```
journalctl -u xxx.timer # 关于 timer 的日志，只包括何时启动了 service。
journalctl -u xxx.service # 关于 timer 关联的 service 的日志。
```

但是，systemd timers 只能用于定时启动服务，不能定时关闭服务，所以只适合执行低占空比的任务。
要实现定时关闭服务，可以：（1）用 cron 或另一组 systemd timer 来执行 systemctl start|stop xxx 命令。
（2）使用互斥的一对 systemd timer+service ，即 Conflicts 指令，让服务因为冲突而关闭[2.6,2.7]。

## 时间格式
单个日期的表示[1.7]：

```
Fri 2012-11-23 23:02:15.74 CET
```

第一个字段是周天（可选），用三个英文字母的缩写（不论 locale），第二字段是年-月-日，第三字段是时:分:秒.亚秒（亚秒可选），第四字段是时区（可选，不写时，意思是操作系统的时区）。

表达周期性任务的时间时，将其中一部分替换为通配符 `*` 即可。不能用通配符表达的情况下，可以用逗号分割的列表来枚举。例如[1.7]：

```
    minutely → *-*-* *:*:00
      hourly → *-*-* *:00:00
       daily → *-*-* 00:00:00
     monthly → *-*-01 00:00:00
      weekly → Mon *-*-* 00:00:00
      yearly → *-01-01 00:00:00
   quarterly → *-01,04,07,10-01 00:00:00
semiannually → *-01,07-01 00:00:00
```

也可以用 `a..b` 来表示范围[1.7]：

```
Mon..Thu,Sat,Sun *-*-* 00:00:00
```

## 自动重启和高频启动防止机制

对于持续运行的服务，如果它意外退出了，systemd可以自动重启它。但如果重启频率过高，systemd也会停止自动重启。
相关设置是：
[1.1, 1.2]

以下都在 unit 文件的 [Service] 段中。
```
# Restart= 设置自动重启的行为。
# 要求 systemd 自动重启服务，只要服务进程因为 systemd 以外的原因退出。包括 Restart=on-failure 的情形，加上返回正常退出码、正常信号杀死。
Restart=always
# 要求 systemd 自动重启服务，当服务进程退出时返回非正常退出码，被异常信号（ SIGSEGV 等）杀死，或者遇到超时，看门狗信号等。
# Restart=on-failure
# 不自动重启。默认行为。
# Restart=no

# RestartSec= 自动/手动重启前的等待时间。
# 默认值只有 0.1 秒，这在很多情况下太短了，容易触发 systemd 的”高频启动防止机制“，导致服务重启失败。
# 值不带单位时单位是秒，也可以写上单位，如 5min10s 。
RestartSec=3.0

# StartLimitIntervalSec= 和 StartLimitBurst= 控制 systemd 的”高频启动防止机制“的行为，包括自动和手动重启。
# 默认值是 10 和 5，即最多允许每10秒内有5次启动。
# 超过限度后，服务不再允许被自动重启（但仍可手动启动）。
# StartLimitIntervalSec=10
# StartLimitBurst=5
```

触发”高频启动防止机制“后，此 unit 的日志中会输出错误 `service start request repeated too quickly, refusing to start`，systemd 也不再试图重启服务。
如果仍想恢复服务，可以：
```
# 直接启动服务
systemctl start <service name>
# 重置”高频启动防止机制“状态，并且让服务启动
systemctl reset-failed <service name>
```

## node.js + systemd
Create a service file for the application, in /etc/systemd/system/node-sample.service

```
useradd -mrU srv-node-sample
```

```
[Service]
ExecStart=[node binary] /home/srv-node-sample/[main file]
Restart=always
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=node-sample
User=srv-node-sample
Group=srv-node-sample
Environment=NODE_ENV=production

[Install]
WantedBy=multi-user.target
```

Now start the service:
```
# systemctl enable node-sample
# systemctl start node-sample
```
Status
```
# systemctl status node-sample
node-sample.service
   Loaded: loaded (/etc/systemd/system/node-sample.service; enabled)
   Active: active (running) since Fri 2013-11-22 01:12:15 UTC; 35s ago
 Main PID: 7213 (node)
   CGroup: name=systemd:/system/node-sample.service
           └─7213 /home/srv-node-sample/.nvm/v0.10.22/bin/node /home/srv-nod...

Nov 22 01:12:15 d02 systemd[1]: Started node-sample.service.
```
Logs
```
# journalctl -u node-sample
```

## squid + systemd 的例子

例一[13]：
```
[Unit]
Description=Web Proxy Cache Server

[Service]
EnvironmentFile=/etc/conf.d/squid
ExecStartPre=/usr/sbin/squid -z
ExecStart=/usr/sbin/squid -N $SQUID_ARGS
ExecStop=/usr/sbin/squid -k shutdown
ExecReload=/usr/sbin/squid -k reconfigure

[Install]
WantedBy=multi-user.target
```

注意 ExecStart 采用了 squid -N，意思是让 squid 在前台运行，不进行 double fork。supervisor 大多要求这样做，如 s6, openrc supervise-daemon。

例二[14]：

```
[Unit]
Description=Squid Web Proxy Server
Documentation=man:squid(8)
After=network.target nss-lookup.target

[Service]
Type=forking
PIDFile=/var/run/squid.pid
ExecStartPre=/usr/sbin/squid --foreground -z
ExecStart=/usr/sbin/squid -sYC
ExecReload=/bin/kill -HUP $MAINPID
KillMode=mixed

[Install]
WantedBy=multi-user.target
```
注意此处采用了 Type=forking 以及 squid 默认的 double fork。

## 服务的停止
默认情况下，一个服务单元被停止后，它所创建的子进程也都会被 systemd 所终止。这是 KillMode 指令控制的：

KillMode=
  control-group #默认值，用SIGTERM终止所有进程
  process       #只终止主进程
  mixed         #对主进程用 SIGTERM，其它进程用 SIGKILL
  none          #不终止任何进程，除了执行 ExecStop 指令。

## 资源限制（limits）

一个服务可以使用的资源，包括可以同时打开的文件数量，都会被 systemd 限制。
注意，systemd 及其管理的进程不受 ulimit 和 /etc/security/limits.conf 影响，它们只影响登录用户的进程。

用 systemctl show foo.service 可以显示 systemd 施加的限制，但为了准确，请查看 /proc/<pid>/limits 。

如果不指定，LimitNOFILE 可能很小，例如 centos 7 是 4096 。要修改限制，可以修改 service 文件的 [Service] 段[3.1]：

```
[Service]
...
# 打开的文件数。centos 7 默认仅有 4096
LimitNOFILE=1000000
```

修改后 systemctl daemon-reload 即可看到 systemctl show 数值的改变，但仍需要 systemctl restart 重启服务让设置生效。

systemd 的全局默认限制在 /etc/systemd/system.conf 文件中，例如

```
#DefaultLimitNOFILE=
#DefaultLimitAS=
#DefaultLimitNPROC=
```

## target/ run level 运行级别 

systemctl isolate rescue.target  # 进入 rescue 模式
systemctl isolate emergency.target  # 进入 emergency 模式

systemctl suspend # 待机/挂起到内存

## 日志系统
实时日志：

  journalctl -f

按服务单元过滤：

  journalctl -u foo.service

只显示本次（0）、上次（-1）启动的日志：

  journalctl -b
  journalctl -b -1

列出各次启动的起止时间：
  journalctl --list-boots

显示内核日志：
  journalctl -k
内核日志就是 dmesg 显示的内容，但 journalctl 显示的时间是绝对时间，更容易阅读。
-k 默认只显示本次启动的内核日志，要显示上次启动的内核日志：
 journalctl -k -b -1

按时间过滤：

  journalctl --since "2018-11-24 10:23"
  journalctl --since "2018-11-24" --until "2018-11-25"
  journalctl --since today
  
  --since 等于 -S，--until 等于 -U 

按优先级（日志等级）过滤：
  journalctl -b -1  -p "emerg".."crit"
  journalctl -b -1  -p warning
优先级为：
●      0 or “emerg”

●      1 or “alert”

●      2 or “crit”

●      3 or “err”

●      4 or “warning”

●      5 or “notice”

●      6 or “info”

●      7 or “debug”

倒序
  journalctl -r | --reverse

不自动分页：
  journalctl --no-pager

  journalctl 默认自动分页，需要用方向键/翻页键来查看，超长的行不会折行，需要用左右方向键查看。--no-pager 则不自动分页，在终端里超长的行会折行。

journalctl 不支持关键字或正则表达式过滤，需要用 grep 之类的工具过滤：
  
  journalctl --since today -u foo | grep bar

### 清除日志
journalctl --vacuum-time=10d
journalctl --vacuum-size=2G

### 日志限流

journald 默认配置下会自动限流，写入速率过高的进程可能会丢失一些日志条目。这种丢失会反映在 journald 自身的日志中[15]：
```
$ journalctl -u systemd-journald

systemd-journal[4431]: Suppressed 316 messages from /system.slice/postfix.service
```

如果不希望有这种行为，要修改 journald 配置文件并重启[15]：

```
# in /etc/systemd/journald.conf

RateLimitInterval=0
RateLimitBurst=0

$ systemctl restart systemd-journald
```

### 转发到 syslog
[4.5]
systemd-journald 可以被配置为转发日志到 syslog。不同发行版的默认配置是不一样的，例如 ubuntu 22.04 转发，而 deepin V23 不转发。
要修改此配置，修改 `/etc/systemd/journald.conf` ，变更 `ForwardToSyslog=no|yes`，然后重启 `systemctl restart systemd-journald`。
如果关闭了转发到 syslog ，可以同时停止 syslog 守护进程，并禁用它：`sudo systemctl disable --now syslog.socket rsyslog.service`。

## 用户级服务
systemd 还支持只在一个用户的会话中运行的服务。

户级服务的命令行要带上 --user 参数，例如
```
systemctl --user list-unit-files
```

这种用户级服务的配置文件位于：

```
~/.config/systemd/user/ # 用户自建的服务放在这里。
/etc/systemd/user/ # 软件包安装的服务放在这里。
~/.local/share/systemd/user/  # 软件包安装的服务放在这里。
/usr/lib/systemd/user/  # 软件包安装的服务放在这里。
```



例子：
```
/usr/lib/systemd/user/pulseaudio.service
/etc/systemd/user/timidity.service
```

由于 systemd 并不自动导入环境变量，通常需要使用一些 systemd 的变量来代替，例如 `%h`（用户的主目录，等效于 `$HOME`）。
例子：

```
~/.config/systemd/user/foldingathome-user.service

[Unit]
Description=Folding@home distributed computing client
After=network.target

[Service]
Type=simple
WorkingDirectory=%h/.config/fah
ExecStart=/usr/bin/FAHClient
CPUSchedulingPolicy=idle
IOSchedulingClass=3

[Install]
WantedBy=default.target
```
