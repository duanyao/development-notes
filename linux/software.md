## 其它桌面环境

KDE：install task-kde-desktop

## 二进制编辑器
bless
wxhexeditor

## 开发
manpages-dev  git git-gui git-svn subversion build-essential automake libtool cmake gdb golang-go meld traceroute strace moreutils time dh-make

moreutils 里有 ts 命令，可以在标准输出中加上时间戳。
time 是进程计时工具，`\time -v cmdline`

nvidia-driver nvidia-smi nvidia-settings nvidia-xconfig # nv 显卡驱动。如果是 nv 单显卡，或者有核显但想用nv 单显卡。

nvidia-cuda-dev nvidia-cuda-toolkit # cuda 开发工具。如果使用核显但想用 cuda 做计算，可以不装nv 显卡驱动，只装这个。

ffmpeg vlc totem

vscode # or com.visualstudio.code

tortoisehg

kate

monodevelop code::blocks

maven ant

virtualenv python3-virtualenv

meson valac gtk+-3.0-dev # gtk3、gnome 程序编译

deepin.com.wechat.devtools wechat-web-devtools  # 微信开发者工具。deepin 特有，二选一即可。

### .net 

在 Linux 上安装 .NET
https://docs.microsoft.com/zh-cn/dotnet/core/install/linux?WT.mc_id=dotnet-35129-website

```
wget https://packages.microsoft.com/config/debian/10/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
rm packages-microsoft-prod.deb
sudo apt-get update
sudo apt install dotnet-runtime-5.0
```

## 阅读
comix

## 网络
autossh mtr

amule
transmission
iptux

tcpdump wireshark curl tmux avahi-daemon

youtube_dl # sudo pip3 install --upgrade youtube_dl; youtube-dl

FClash # 科学上网

## 视频编辑/图像处理
kdenlive Flowblade Cinelerra Lightworks openshot Shotcut

## 科技数据处理/绘图

scidavis
https://scidavis.sourceforge.net/
https://github.com/SciDAVis/scidavis

QtiPlot
https://qtiplot.com/

## 管理
atop lsof psensor dconf-editor cgroup-tools 

earlyoom

swap-control-gui
https://github.com/MeowSprite/swap-control-gui
https://bbs.deepin.org/forum.php?mod=viewthread&tid=133470
https://bbs.deepin.org/forum.php?mod=viewthread&tid=148661

network-manager-gnome # 网络连接管理，包括 nm-applet

nautilus=3.26.3.1-1 nautilus-data=3.26.3.1-1 libnautilus-extension1a=3.26.3.1-1
sudo apt-mark hold nautilus nautilus-data libnautilus-extension1a

nautilus-open-terminal # 文管“打开终端”插件

nemo # 文管

gnome-power-manager  # 电池状态
powertop             # 进程和系统功耗

cpu-g cpu-x # CPU 型号、状态，GUI
cpufrequtils # CPU 状态如频率，cpufreq-info

apt-file # 根据文件搜索包

gnome-disk-utility # 用于挂载 iso 等文件。

smartmontools # 磁盘 SMART 信息查看，sudo smartctl /dev/sda -x -d sat

gnome-tweaks # gnome 设置。ubuntu 23 的系统设置功能太少。

timidity

software-properties-common #add-apt-repository

utools 自由组合插件应用 https://u.tools/

## 剪贴板管理
CopyQ https://github.com/hluk/CopyQ/releases

Qlipper

Clipit

deepin 剪贴板管理（包名不详，dde-clipboard？），ctrl+alt+v唤醒。

## cpupower-gui

控制 intel CPU 的调速策略和最大频率。

echo 'deb https://download.opensuse.org/repositories/home:/erigas:/cpupower-gui/xUbuntu_20.04/ /' | sudo tee /etc/apt/sources.list.d/home=erigas=cpupower-gui.list
curl -fsSL https://download.opensuse.org/repositories/home:erigas:cpupower-gui/xUbuntu_20.04/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/home=erigas=cpupower-gui.gpg > /dev/null
sudo apt update
sudo apt install cpupower-gui

## nvm
https://github.com/creationix/nvm

curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
nvm install node # or nvm install v15
nvm use node
nvm ls
nvm ls-remote

## npm
typescript

## snapd
sudo apt install snapd
sudo snap install kubectl --classic
sudo ln -s /snap/bin/kubectl /usr/local/bin/kubectl

## 其它
unetbootin

Rescatux 系统救援盘，也可用来救 windows
 http://www.supergrubdisk.org/rescatux/

TestDisk and PhotoRec 数据恢复
  http://www.cgsecurity.org/

fbterm

## 系统级虚拟机、模拟器

EXAGEAR 在 ARM（linux 或 android）上模拟运行 x86 （windows）程序。

box64 box86 在非 x86 系统（linux 或 android）上模拟运行 x86 （windows）程序。

## 应用商店/仓库

spark-store aptss # 星火商店

## 云存储

### google-drive-ocamlfuse

在 https://launchpad.net/~alessandro-strada/+archive/ubuntu/ppa/+packages 找合适的 deb 包。
```
wget -4 https://launchpad.net/~alessandro-strada/+archive/ubuntu/ppa/+files/google-drive-ocamlfuse_0.7.27-0ubuntu1~ubuntu18.04.1_amd64.deb
sudo apt install ./google-drive-ocamlfuse_0.7.27-0ubuntu1~ubuntu18.04.1_amd64.deb
```

## 软件配置项目

### 禁用 pc speeker

/etc/modprobe.d/nobeep.conf
```
blacklist pcspkr
blacklist snd_pcsp
```
注意，单独禁用 pcspkr 不足以禁用 pc speeker，因为 snd_pcsp 会顶上来。

### ufw/gufw 防火墙
```
apt install gufw
```
设置规则：
```
sudo ufw enable

# 允许内网访问 ssh
sudo ufw allow from 192.168.0.0/16 to any port 22 proto tcp
sudo ufw allow from 10.0.0.0/8 to any port 22 proto tcp
sudo ufw allow from 172.16.0.0/12 to any port 22 proto tcp

# 允许特定端口段 25000:26000 内网访问。
sudo ufw allow from 192.168.0.0/16 to any port 25000:26000 proto tcp
sudo ufw allow from 192.168.0.0/16 to any port 25000:26000 proto udp
sudo ufw allow from 10.0.0.0/8 to any port 25000:26000 proto tcp
sudo ufw allow from 10.0.0.0/8 to any port 25000:26000 proto udp
sudo ufw allow from 172.16.0.0/12 to any port 25000:26000 proto tcp
sudo ufw allow from 172.16.0.0/12 to any port 25000:26000 proto udp

# IPv6 规则。允许特定端口段 25000:26000 内网（link-local）访问。
sudo ufw allow from fe80::/10 to any port 22 proto tcp
sudo ufw allow from fe80::/10 to any port 25000:26000 proto tcp
sudo ufw allow from fe80::/10 to any port 25000:26000 proto udp

# IPv6 规则。允许特定端口段 25000:26000 内网（unique-local）访问。IPv6 如果是 DHCP 分配的，则通常在这一段。
sudo ufw allow from fc00::/7 to any port 22 proto tcp
sudo ufw allow from fc00::/7 to any port 25000:26000 proto tcp
sudo ufw allow from fc00::/7 to any port 25000:26000 proto udp
```

### 避免 ssh 客户端超时

/etc/ssh/ssh_config
增加
```
ServerAliveInterval 15
```

### 打开、监视文件数限制

/etc/sysctl.d/zz-custom.conf

fs.inotify.max_user_watches = 530000
fs.inotify.max_user_instances = 8000
#fs.inotify.max_queued_events = 16000
#fs.file-max = 990000

### 内存和交换设置
/etc/sysctl.d/zz-custom.conf
/usr/lib/sysctl.d/deepin.conf

vm.swappiness = 100

### 网络设置
/etc/sysctl.d/zz-custom.conf
/usr/lib/sysctl.d/deepin.conf

net.core.somaxconn = 10000

### nautilus 新建文件模板
查找模板目录：
```
$ xdg-user-dir TEMPLATES
/home/duanyao/.Templates
```
新建空文本文件：
```
touch ~/.Templates/new_file.txt
```
### 加入不稳定 apt 源
详见 apt.md 。

修改 /etc/apt/sources.list ，增加“panda”一行:

```
deb [by-hash=force] http://packages.deepin.com/deepin lion main contrib non-free   # 稳定版
deb [by-hash=force] http://packages.deepin.com/deepin panda main contrib non-free  # 不稳定版
```

修改 /etc/apt/preferences ，增加
```
# based on debian unstable
Package: *
Pin: release panda
Pin-Priority: 100

# based on debian stable
Package: *
Pin: release lion
Pin-Priority: 500
```
### .profile

```
export SSH_ASKPASS=/usr/bin/ssh-askpass # 询问 ssh 密码的 GUI 对话框
```

## 配置文件
~/.pip/pip.conf

~/.config/deepin/deepin-terminal/  # 旧位置：~/.config/deepin-terminal/
