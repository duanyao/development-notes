## 资料
[1] GNU Parted
https://wiki.archlinux.org/index.php/GNU_Parted

[2] Unrecognised Disk Label When Creating Partition
https://techjourney.net/unrecognised-disk-label-when-creating-partition/

[3] Create UEFI boot USB
https://wiki.alpinelinux.org/wiki/Create_UEFI_boot_USB

[4] 
https://serverfault.com/questions/749258/how-to-reset-a-harddisk-delete-mbr-delete-partitions-from-the-command-line-w

[5] GParted forum → Live Media → Partition resize is very slow
http://gparted-forum.surf4.info/viewtopic.php?id=14462

[6] Move partition to the left without copying
https://gitlab.gnome.org/GNOME/gparted/issues/67#note_545837

## 擦除已有的分区表
显示现有的分区表：
wipefs /dev/sdb
擦除：
wipefs -a /dev/sdb

## parted

 sudo parted --script /dev/sdb mklabel gpt

 sudo parted --script --align=none /dev/sdb mkpart ESP fat16 1MiB 20MiB

 sudo parted --script /dev/sdb set 1 boot on 

 sudo parted --script /dev/sdb print

 sudo parted --script /dev/sdb rm 1

主要命令
mklabel <type> 创建分区表。
  例如 mklabel gpt 或 mklabel msdos

mkpart <partition type> <fs type> <start> <end>  创建分区。

  例如 mkpart primary ext4 20MiB 220MiB

  --align=optimal|none|minimal|cylinder

rm <partition>
  删除分区。分区号从1开始。

set <partition> flag state
                     Change  the  state of the flag on partition to state.  Supported flags are: "boot", "root",
                     "swap", "hidden", "raid", "lvm", "lba", "legacy_boot", "irst",  "esp"  and  "palo".   state
                     should be either "on" or "off".

parted 创建的 ESP 分区的类型在 gdisk 中显示为 0700(microsoft basic data)，但正确的似乎应该是 EF00。
可以用 gdisk 的 t 命令修改。

### 问题

执行 sudo parted --script /dev/sdb mklabel gpt 时可能遇到错误：

Error: Partition(s) 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36,
37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74,
75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109,
110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128 on /dev/sdb have been written, but we have been unable to
inform the kernel of the change, probably because it/they are in use.  As a result, the old partition(s) will remain in use.  You should reboot now
before making further changes.

原因不明。解决方法可能是进入 parted 交互模式，执行 mklabel gpt 和 mkpart ESP fat16 1MiB 20MiB 之类的命令，并回答忽略错误。然后在非交互模式就可能能够正常操作。

## gdisk
gdisk 的一项功能是将 MBR 分区的硬盘无损转换为 GPT 分区的。实际上，启动 gdisk 后，如果它发现硬盘是 MBR 分区的，就会提示进行转换，执行 w 命令写入即可。
gdisk 可以转换正在运行系统的硬盘，无需先卸载。不过要完全生效，还是需要一次重启。

gdisk 的 t 命令可以修改分区类型，它支持的分区类型是很全面的。输入 EF00 可转化为 ESP 分区。

gdisk s 命令可以重新对分区排序。

## 创建文件系统和卷标

mkfs.vfat -F 16 -n ESP /dev/sdb1 # FAT16 分区，因为 FAT32最小32MiB。
mkfs.ext4 -F -L root0 /dev/sdb2
mkfs.ext4 -F -L root1 /dev/sdb3
mkfs.ext4 -F -L update /dev/sdb4
mkfs.ext4 -F -L var /dev/sdb5
mkfs.ext4 -F -L squid /dev/sdb6

用 blkid 可以列出分区的卷标、UUID等信息，请确认卷标是否正确。
传给 mkfs.vfat 的卷标必须是大写，否则可能无法正确生成卷标。

事后修改卷标可用 mlabel 来实现：
  * 安装 mtools: apk add mtools
  * 执行 echo mtools_skip_check=1 > ~/.mtoolsrc （为了避免mtools的警告）
  * 执行 mlabel -i /dev/sda1 修改卷标（假定esp是 sda1）。注意 mlabel 会把卷标设置为大写的。
  
或者也可以试试 dosfslabel <device> <label>

## 列出分区信息
blkid

/dev/sda6: UUID="f788fe1c-c3c3-4cf0-9108-2a040114fca8" TYPE="ext4"
/dev/sda5: UUID="d2be5895-b4c9-4efd-9c06-c9f94633a12f" TYPE="ext4"
/dev/sda4: UUID="01de9959-d2fe-4a6f-816d-98c578fe010a" TYPE="ext4"
/dev/sda3: UUID="3aeb14b5-7599-4da1-9744-6f8aea04759f" TYPE="ext4"
/dev/sda2: UUID="f23ca29a-53c4-4a51-ad13-bc40da1d76c3" TYPE="ext4"
/dev/sda1: TYPE="vfat"
/dev/loop/0: TYPE="squashfs"
/dev/sr0: LABEL="alpine-virt 3.5.2 x86_64" TYPE="iso9660"
/dev/loop0: TYPE="squashfs"

在 sudo 系统上的 blkid 命令可能需要用 sudo 运行才能得到结果，否则输出空。
或者，将用户加入 disk 组也可以。

## USB 盘的自动挂载
### 桌面系统
启动 dconf-editor （如果没装先安装：debian 系列：apt-get install dconf-editor）；
查找 automount，将 org.gnome.desktop.media-handling 下的 automount 置为 false 即可停止自动挂载，反之允许自动挂载。

## 查询分区/文件系统信息
### df 命令
输出格式的灵活性是有限的，比如不能去掉表头。如果在脚本或程序中调用，你需要自行解析其输出。
只能用于已挂载的分区。

### C 函数 statfs() 和 statvfs() 
前者是 linux / BSD 系统调用，后者是 POSIX 标准 API。
只能用于已挂载的分区。

       int statvfs(const char *path, struct statvfs *buf);
       int fstatvfs(int fd, struct statvfs *buf);

           struct statvfs {
               unsigned long  f_bsize;    /* Filesystem block size */
               unsigned long  f_frsize;   /* Fragment size */
               fsblkcnt_t     f_blocks;   /* Size of fs in f_frsize units */
               fsblkcnt_t     f_bfree;    /* Number of free blocks */
               fsblkcnt_t     f_bavail;   /* Number of free blocks for
                                             unprivileged users */
               fsfilcnt_t     f_files;    /* Number of inodes */
               fsfilcnt_t     f_ffree;    /* Number of free inodes */
               fsfilcnt_t     f_favail;   /* Number of free inodes for
                                             unprivileged users */
               unsigned long  f_fsid;     /* Filesystem ID */
               unsigned long  f_flag;     /* Mount flags */
               unsigned long  f_namemax;  /* Maximum filename length */
           };

### Linux 特殊文件系统

## 无损调整分区大小
### gparted 向左（低地址）增大ext4分区非常慢的问题

向左（低地址）增大ext4分区时，gparted 执行以下步骤：

1. 检查分区和文件系统（很快）
2. 向左移动分区的起点，从而增大分区到目标值（瞬间）
3. 向左移动文件系统到分区头部（含数据，很慢）

  gparted 使用 e2image 命令来移动 ext4 文件系统的数据。如：
  ```
  e2image -ra -p -o 22311323 /dev/sda3
  ```

4. 向左移动分区的终点，从而缩小分区到原始值（瞬间）
5. 检查文件系统（很快）
6. 向右移动分区的终点，从而增大分区到目标值（瞬间）
7. 扩大文件系统到整个分区（瞬间）
  ```
  resize2fs -p /dev/sda3
  ```
  
因为3整体移动整个文件系统的数据，所以非常慢。帖子[5]确认了这一点。

没有看到资料表明 resize2fs 要求文件系统位于分区的头部，所以3,4,5,6可能是多余的。
但是[6]指出所有的文件系统都得从分区的头部开始。这可能是事实。

## 保留的磁盘空间
```
tune2fs -m 1 /dev/sdXY
```