## 参考资料

[1.1] Difference between sh and bash
https://stackoverflow.com/questions/5725296/difference-between-sh-and-bash

[1.2] What’s the Difference Between Bash, Zsh, and Other Linux Shells?
https://www.howtogeek.com/68563/HTG-EXPLAINS-WHAT-ARE-THE-DIFFERENCES-BETWEEN-LINUX-SHELLS/

[2.1] Why ~/.bash_profile is not getting sourced when opening a terminal?
https://askubuntu.com/questions/121073/why-bash-profile-is-not-getting-sourced-when-opening-a-terminal

[3.1] How to echo shell commands as they are executed
https://stackoverflow.com/questions/2853803/how-to-echo-shell-commands-as-they-are-executed

## bash 和标准 sh 的差异
[1.1]

## Interactive login shell/ Interactive non login shell

命令[2.1]：

shopt login_shell

如果显示
login_shell    	off

则当前是 non login shell 。

如果从文本模式登录、ssh 登录，以及从 GUI 中 Ctrl+Alt+F2 切换文本模式，则应该是 login shell；从 GUI 登录，则可能是 non login shell（取决于桌面环境的实现）。

对于 bash ，如果是 non login shell ，则执行 ~/.bashrc 。如果是 login shell ，则先执行 /etc/profile 以及 *profile (按 ~/.bash_profile, ~/.bash_login, ~/.profile 的顺序，执行找到的第一个)。

执行 su 或者 bash 命令时，加上 -l 参数则启动 login shell，否则启动 non login shell [2.1]。

### GUI 环境

GUI 登录后，shell 被认为是 non login shell，但是 display manager 可能类似 shell 一样执行若干 profile 文件。之所以说可能，是因为不同的发行版和 DM 有不同的行为。

例如 ubuntu 的 lightdm 会执行：

/etc/profile, ~/.profile, /etc/xprofile, ~/.xprofile

但 debian 10 和 deepin V20 就不会。如果要改为 ubuntu 的行为，可以将 https://github.com/canonical/lightdm/blob/master/debian/lightdm-session 下载，放到 /usr/sbin 目录，加上可执行权限。

https://bbs.deepin.org/forum.php?mod=viewthread&tid=192373&highlight=.profile

#### lightdm + deepin v20

使用 /usr/sbin/lightdm-session:

/etc/profile
~/.profile, PATH=/home/duanyao/t-project/emsdk.git:/home/duanyao/t-project/emsdk.git/upstream/emscripten:/home/duanyao/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/sbin:/usr/sbin , BASH_VERSION=
/etc/X11/Xsession.d/01deepin-profile:start, PATH=/home/duanyao/t-project/emsdk.git:/home/duanyao/t-project/emsdk.git/upstream/emscripten:/home/duanyao/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/sbin:/usr/sbin
/etc/profile
/etc/X11/Xsession.d/01deepin-profile:end, PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/sbin:/usr/sbin

不使用 /usr/sbin/lightdm-session:

/etc/X11/Xsession.d/01deepin-profile:start, PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games
/etc/profile
/etc/X11/Xsession.d/01deepin-profile:end, PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/sbin:/usr/sbin
~/.bashrc(s), PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/sbin:/usr/sbin
~/.bashrc(e), PATH=/home/duanyao/.nvm/versions/node/v12.3.1/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/sbin:/usr/sbin

使用 /usr/sbin/lightdm-session 是 ubuntu 的特点，不使用 /usr/sbin/lightdm-session 是 debian 的特点。
前者会自动执行 /etc/profile, ~/.profile, /etc/xprofile, ~/.xprofile，后者不会。但两者都会执行 /etc/X11/Xsession.d/* ，在 profile 之后。

注意 ~/.bashrc 没有被 ~/.profile 调用，这是因为 lightdm-session 在调用 ~/.profile 前清空了 BASH_VERSION：

```
    BASH_VERSION= . "$CONFIG_FILE" 2>"$ERR"
```
而 ~/.profile 在这种情况下不调用 ~/.bashrc ：

```
# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi
```

这个行为与 sddm 不一致，sddm 不清空 BASH_VERSION ，所以 bashrc 会执行。

## 回显执行的命令

bash：
在命令行中 `bash -x script_file` 。在 script 中[3.1]：
```
#!/bin/bash
set -x #echo on

ls $PWD
set -v #echo off
```
