## 参考资料
[1] Advanced Linux Sound Architecture (简体中文)
  https://wiki.archlinux.org/index.php/Advanced_Linux_Sound_Architecture_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)#ALSA_%E5%92%8C_Systemd


## 检查工具

### alsamixer

alsamixergui
gnome-alsamixer
qasmixer
pavucontrol
alsa-tools-gui

### speaker-test
这个工具播放测试声音。对于一台计算机，2 声道和 7.1 声道都可以测试。[1]
```
$ speaker-test -c 2  # 2 声道
$ speaker-test -c 8  # 7.1 声道
```

### aplay
列出通道：

```
$ aplay -L | grep :CARD
sysdefault:CARD=PCH
front:CARD=PCH,DEV=0
surround21:CARD=PCH,DEV=0
surround40:CARD=PCH,DEV=0
surround41:CARD=PCH,DEV=0
surround50:CARD=PCH,DEV=0
surround51:CARD=PCH,DEV=0
surround71:CARD=PCH,DEV=0
hdmi:CARD=PCH,DEV=0
hdmi:CARD=PCH,DEV=1
hdmi:CARD=PCH,DEV=2
hdmi:CARD=PCH,DEV=3
hdmi:CARD=PCH,DEV=4
dmix:CARD=PCH,DEV=0
dmix:CARD=PCH,DEV=3
dmix:CARD=PCH,DEV=7
dmix:CARD=PCH,DEV=8
dmix:CARD=PCH,DEV=9
dmix:CARD=PCH,DEV=10
dsnoop:CARD=PCH,DEV=0
dsnoop:CARD=PCH,DEV=3
dsnoop:CARD=PCH,DEV=7
dsnoop:CARD=PCH,DEV=8
dsnoop:CARD=PCH,DEV=9
dsnoop:CARD=PCH,DEV=10
hw:CARD=PCH,DEV=0
hw:CARD=PCH,DEV=3
hw:CARD=PCH,DEV=7
hw:CARD=PCH,DEV=8
hw:CARD=PCH,DEV=9
hw:CARD=PCH,DEV=10
plughw:CARD=PCH,DEV=0
plughw:CARD=PCH,DEV=3
plughw:CARD=PCH,DEV=7
plughw:CARD=PCH,DEV=8
plughw:CARD=PCH,DEV=9
plughw:CARD=PCH,DEV=10
usbstream:CARD=PCH
```

## 系统服务
```
$ systemctl --user | grep pulse
  pulseaudio.service                                                                       loaded active     running   Sound Service                                                             
  pulseaudio.socket                                                                        loaded active     running   Sound System  

$ systemctl | grep alsa
  alsa-restore.service                                                                     loaded    active     exited       Save/Restore Sound Card State                                                
  alsa-state.service                                                                       loaded    active     running      Manage Sound Card State (restore and store)                                  
```
  
部分声音故障可以通过重启服务来实现，例如：

```
systemctl --user restart pulseaudio
```

查看日志：
```
$ journalctl --user --since yesterday -u pulseaudio
-- Logs begin at Tue 2020-09-22 09:30:14 CST, end at Fri 2020-09-25 02:06:56 CST. --
9月 24 14:27:30 duanyao-laptop-c pulseaudio[15558]: W: [alsa-sink-ALC295 Analog] ratelimit.c: 2088 events suppressed
9月 24 14:27:30 duanyao-laptop-c pulseaudio[15558]: W: [alsa-sink-ALC295 Analog] protocol-native.c: Failed to push data into queue
9月 24 14:54:48 duanyao-laptop-c pulseaudio[15558]: E: [pulseaudio] module.c: Module "module-switch-on-connect" should be loaded once at most. Refusing to load.
9月 24 16:19:13 duanyao-laptop-c pulseaudio[15558]: E: [pulseaudio] module.c: Module "module-switch-on-connect" should be loaded once at most. Refusing to load.
9月 24 20:30:11 duanyao-laptop-c pulseaudio[15558]: E: [pulseaudio] module.c: Module "module-switch-on-connect" should be loaded once at most. Refusing to load.
9月 24 20:59:04 duanyao-laptop-c pulseaudio[15558]: XIO:  fatal IO error 11 (资源暂时不可用) on X server ":0"
9月 24 20:59:04 duanyao-laptop-c pulseaudio[15558]:       after 17 requests (17 known processed) with 0 events remaining.
9月 24 20:59:05 duanyao-laptop-c systemd[9009]: pulseaudio.service: Main process exited, code=exited, status=1/FAILURE
9月 24 20:59:05 duanyao-laptop-c systemd[9009]: pulseaudio.service: Failed with result 'exit-code'.
9月 24 20:59:05 duanyao-laptop-c systemd[9009]: pulseaudio.service: Service RestartSec=100ms expired, scheduling restart.
9月 24 20:59:05 duanyao-laptop-c systemd[9009]: pulseaudio.service: Scheduled restart job, restart counter is at 1.
9月 24 20:59:05 duanyao-laptop-c systemd[9009]: Stopped Sound Service.
9月 24 20:59:05 duanyao-laptop-c systemd[9009]: Starting Sound Service...
9月 24 20:59:05 duanyao-laptop-c pulseaudio[12150]: W: [pulseaudio] pid.c: Stale PID file, overwriting.
9月 24 20:59:06 duanyao-laptop-c pulseaudio[12150]: E: [pulseaudio] module-alsa-card.c: Failed to open mixer for jack detection
9月 24 20:59:06 duanyao-laptop-c pulseaudio[12150]: W: [pulseaudio] server-lookup.c: Unable to contact D-Bus: org.freedesktop.DBus.Error.Spawn.ExecFailed: /usr/bin/dbus-launch terminated abnormally with the following error: Invalid MIT-MAGIC-COOKIE-1 keyAutolaunch error: X11 initialization failed.
9月 24 20:59:06 duanyao-laptop-c pulseaudio[12150]: W: [pulseaudio] main.c: Unable to contact D-Bus: org.freedesktop.DBus.Error.Spawn.ExecFailed: /usr/bin/dbus-launch terminated abnormally with the following error: Invalid MIT-MAGIC-COOKIE-1 keyAutolaunch error: X11 initialization failed.
9月 24 20:59:06 duanyao-laptop-c systemd[9009]: Started Sound Service.
9月 24 20:59:06 duanyao-laptop-c pulseaudio[12150]: E: [pulseaudio] backend-ofono.c: Failed to register as a handsfree audio agent with ofono: org.freedesktop.DBus.Error.ServiceUnknown: The name org.ofono was not provided by any .service files

9月 25 02:03:48 duanyao-laptop-c systemd[9009]: Stopping Sound Service...
9月 25 02:03:48 duanyao-laptop-c systemd[9009]: pulseaudio.service: Succeeded.
9月 25 02:03:48 duanyao-laptop-c systemd[9009]: Stopped Sound Service.
9月 25 02:03:48 duanyao-laptop-c systemd[9009]: Starting Sound Service...
9月 25 02:03:48 duanyao-laptop-c systemd[9009]: Started Sound Service.
9月 25 02:03:48 duanyao-laptop-c pulseaudio[14187]: E: [pulseaudio] backend-ofono.c: Failed to register as a handsfree audio agent with ofono: org.freedesktop.DBus.Error.ServiceUnknown: The name org.ofono was not provided by any .service files
```

## 命令行播放声音

aplay

mpv

## ffmpeg 音频采集（linux）

Linux 上可以采用两种输入方式：ALSA 和 PulseAudio ，分别用 `-f alsa` 和 `-f pulse` 指定。
两者既可以采集话筒声音，也可以采集声卡声音。

例子：
`ffmpeg -f pulse -i default` 或者 `ffmpeg -f alsa -i default`。
输入设备名不知道时，可以用 `default`。不指定输出文件时，将显示输入设备的属性后退出，大概是这样：

```
Guessed Channel Layout for Input Stream #0.0 : stereo
Input #0, alsa, from 'default':
  Duration: N/A, start: 1525760340.502230, bitrate: 1536 kb/s
    Stream #0:0: Audio: pcm_s16le, 48000 Hz, stereo, s16, 1536 kb/s
```
录制声音：
`ffmpeg -y -f alsa -i default system.wav`

linux 上采集设备可以用 `ffmpeg -sources` 列出，结果大概是这样（包含音视频采集设备，此处节选alsa和pulse部分）：

```
Auto-detected sources for alsa:
* default [Playback/recording through the PulseAudio sound server]
  null [Discard all samples (playback) or generate zero samples (capture)]
  sysdefault:CARD=PCH [Default Audio Device]
  front:CARD=PCH,DEV=0 [Front speakers]
  dmix:CARD=PCH,DEV=0 [Direct sample mixing device]
  dsnoop:CARD=PCH,DEV=0 [Direct sample snooping device]
  hw:CARD=PCH,DEV=0 [Direct hardware device without any conversions]
  plughw:CARD=PCH,DEV=0 [Hardware device with all software conversions]
Auto-detected sources for pulse:
  bluez_sink.00_11_C2_08_F6_79.headset_head_unit.monitor [Monitor of G18]
  bluez_source.00_11_C2_08_F6_79.headset_head_unit [G18]
  alsa_output.pci-0000_00_1b.0.analog-stereo.monitor [Monitor of 内置音频 模拟立体声]
* alsa_input.pci-0000_00_1b.0.analog-stereo [内置音频 模拟立体声]

```

方括号里是注释，设备名就是 `default`，`front:CARD=PCH,DEV=0` 这样的了，冒号及其之后的部分有时可以省略，例如 `front`。alsa 设备还可以用 `aplay -L` 命令列出[5.1]。
对于 alsa，`default` 是默认的声音设备，这可能是默认的话筒，也可能是默认的声卡。在联想 V470 笔记本上测试时，`default` 和 `sysdefault` 都是话筒。
带有 speakers 字样的是扬声器，可以用来记录系统的声音。注意，这必须在扬声器工作的情况下才能记录，如果插上耳机就可能记录不到；并且，系统音量也影响记录到的音量。

对于 pulse 设备，带有 sink、output 字样的是扬声器，带有 source、input 字样的是话筒。记录扬声器（alsa_output）时，系统音量不影响记录的音量。

alsa 不能记录蓝牙耳机，pulse 则可以（bluez 开头的设备）；alsa 记录系统声音也有较大的杂音，pulse 则没有这个问题。所以最好使用 pulse 设备。

前面带星号的是当前在工作的设备（不一定准确）。

除了采用采集设备默认的采样率和声道数，还可以用以下位置相关选项指定: 
  -sample_rate(-ar)   默认 48000，单位 Hz，也可以写作 48K
  -channels(-ac)      默认 2

例如：

`ffmpeg -y -channels 1 -ar 44.1k -f pulse -i "alsa_output.pci-0000_00_1b.0.analog-stereo.monitor" pulse.wav`

注意，ffmpeg 网站上提供的 linux 静态编译版在某些系统（如 deepin 15.11）上可能有兼容性问题，无法录制声卡声音。
