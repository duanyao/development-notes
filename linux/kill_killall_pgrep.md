## 参考资料
https://www.linode.com/docs/guides/use-killall-and-kill-to-stop-processes-on-linux

## 按名字杀进程

### killall
killall 匹配进程名的方法不明，似乎与 ps / top 等工具显示的名字不一定相同，也与 `/proc/<pid>/comm` 或 `/proc/<pid>/cmdline` 不一定相同。
因此，如果 killall 不奏效，可以尝试 pgrep / pkill。

### pgrep, pkill
pgrep 和 pkill 匹配的进程名与 ps / top 等工具显示的名字相同，且可以匹配命令行参数部分（-f 参数）
```
pgrep -f part_of_cmdline
pkill -f part_of_cmdline
```
