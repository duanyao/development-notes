## 参考资料

[1.3] PCIe扫盲——PCIe总线基本概念
http://blog.chinaaet.com/justlxy/p/5100053225
https://blog.csdn.net/kunkliu/article/details/93993436

[1.4] PCIe扫盲——ReTimer和ReDriver简介 
http://blog.chinaaet.com/justlxy/p/5100066965

[1.5] PCIE 的 GT/s 与 Gbps 的关系
https://www.mr-wu.cn/what-does-pcie-gt-s-mean/

[1.6] PCIe的通道是怎么分分合合的？详解PCIe bifurcation
https://zhuanlan.zhihu.com/p/70125574

[1.7] 芯片中的数学——均衡器EQ和它在高速外部总线中的应用
https://zhuanlan.zhihu.com/p/48343011

[2.1] Linux查看PCIe版本及速率
https://www.cnblogs.com/lsgxeva/p/9542975.html

[2.2] 开发者分享 | 使用 lspci 和 setpci 调试 PCIe 问题
http://xilinx.eetrend.com/content/2020/100059215.html

[2.3] PCIe Set Speed
https://alexforencich.com/wiki/en/pcie/set-speed

[2.4] 基于Linux系统的PCIe常见问题定位总结
https://zhuanlan.zhihu.com/p/222449803

[3.1] How to change PCIE from 4.0 to 3.0 in ASUS TUF X570 plus (WIFI)
https://linustechtips.com/topic/1118919-how-to-change-pcie-from-40-to-30-in-asus-tuf-x570-plus-wifi/

[4.1] https://forums.evga.com/How-to-force-PCIE-Gen-3-in-Windows-10-m2372915.aspx
How to force PCIE Gen 3 in Windows 10?

[5.1] 8. The PCI Express Advanced Error Reporting Driver Guide HOWTO
https://www.kernel.org/doc/html/latest/PCI/pcieaer-howto.html

[5.2] Troubleshooting PCIe Bus Error severity Corrected on Ubuntu and Linux Mint
https://itsfoss.com/pcie-bus-error-severity-corrected/

[6.1] PCI-Express 接口供电能力详解
https://www.nomox.cn/post/pcie-power-supply-ability/

[6.3] 980 PRO NVMe™ M.2 固态硬盘
https://www.samsung.com/cn/memory-storage/nvme-ssd/980-pro-nvme-m-2-1tb-mz-v8p1t0bw/

[6.4] 980 NVMe™ M.2 固态硬盘
https://www.samsung.com/cn/memory-storage/nvme-ssd/980-1tb-nvme-pcie-gen-3-mz-v8v1t0bw/

[6.5] Solid state drive/NVMe
https://wiki.archlinux.org/title/Solid_state_drive/NVMe#Troubleshooting

[6.6] 3.7. Active-State Power Management
https://docs.fedoraproject.org/en-US/Fedora/19/html/Power_Management_Guide/ASPM.html

[7.1] adt link M.2 NVMe 延长线
http://www.adtlink.cn/cn/m2m.html
https://item.taobao.com/item.htm?id=572172787785

## lspci

```
lspci # 简略的输出
lspci -v -s xxx.xxx  # 较详细的输出
lspci -vvv -s xxx.xxx  # 很详细的输出
```

在 -vvv 模式，可以看到理论链路速度（LnkCap Speed xxGT/s, width xN）、当前链路速度（LnkSta: Speed xxGT/s, width xN）。

“链路功能寄存器 (Link Capability Register, LnkCap)”和“链路状态寄存器 (Link Status Register, LnkSta)”均显示 Gen3x8。有时由于链路问题，可能发生链路向下训练。向下训练的链路状态会反映在链路状态寄存器中[2.2]。

注意，当前链路速度是随时可调的，通常是出于节能的原因。

nvme ssd 硬盘在 lspci 中的名称是："Non-Volatile memory controller"。

## setpci

setpci 命令可用于读取和写入配置寄存器[2.2]。

setpci 包含标准配置报头中的所有寄存器的名称。“setpci --dumpregs”命令可显示包含所有 PCI 寄存器和功能的列表，如下所示：
以下是识别 setpci 命令中所使用的寄存器的各种方法。
使用十六进制地址
提供寄存器名称
对于属于 PCI 功能的一部分的寄存器，可通过功能名称来找到第一个寄存器。在 --dumpregs 输出中。查找以 `CAP_' 或 `ECAP_' 开头的名称。在此名称后可接 +offset 以向该地址添加偏移（十六进制值）。这样即可便于找到包含在已设置的相应功能寄存器内的寄存器。
宽度说明符（b、.w 或 .l）用于选择要读取或写入的字节数（1、2 或 4）。如果按名称来引用寄存器且该寄存器宽度已知，则可删除该说明符。
寄存器的所有名称和宽度说明符都区分大小写。


示例：

setpci -s 00:01.0 d0.b=42

以上命令用于写入链路控制 2 寄存器，以将速度设置为 Gen2。
此处值“2”表示 Gen2，另一个位为“插槽时钟”，该位已启用，因此不发生更改。发出以上命令后，如果运行 lspci，那么寄存器中显示的速度值将更改为 Gen2。但这只是它再次执行链路训练时的训练目标速度。
要将链路速度更改为 Gen2，必须对链路进行重新训练。可通过执行以下所示命令来进行重新训练：

setpci -s 00:01.0 b0.b=62

修改 pci-e link speed 的一个脚本见[2.3]。核心部分是：

```
# 修改链路速度，$speed 由用户指定，1～n的整数，分别代表 PCI-E 的代数，即 1.0, 2.0, 3.0 等。 CAP_EXP+30.L 是包括了链路速度的PCI-E寄存器地址，低4位是代数。
lc2=$(setpci -s $dev CAP_EXP+30.L)
lc2n=$(printf "%08x" $((("0x$lc2" & 0xFFFFFFF0) | $speed)))
setpci -s $dev CAP_EXP+30.L=$lc2n

# 触发 PCI-E 重新训练，让修改生效。CAP_EXP+10.L 是触发重新训练的寄存器地址。
lc=$(setpci -s $dev CAP_EXP+10.L) 
lcn=$(printf "%08x" $(("0x$lc" | 0x20)))
setpci -s $dev CAP_EXP+10.L=$lcn
```

## nvme-cli

```
sudo apt install nvme-cli

sudo nvme list # 列出设备，有容量、序列号、固件版本等信息
sudo nvme smart-log /dev/nvmeXnY # 显示 SMART 信息
sudo nvme error-log /dev/nvmeXnY
```

nvme-cli 看不到 nvme 设备的速度模式，请用 lspci -vvv 查看。

## pci-e / nvme error and error reporting
[5.1-5.2]

sudo journalctl --since 2021-01-04 | grep -i PCIe

1月 04 21:37:22 gtrd kernel: pcieport 0000:00:01.2: AER: enabled with IRQ 27
1月 04 21:37:22 gtrd kernel: pcieport 0000:00:03.1: AER: enabled with IRQ 28
1月 04 21:37:22 gtrd kernel: pcieport 0000:00:07.1: AER: enabled with IRQ 30
1月 04 21:37:22 gtrd kernel: pcieport 0000:00:08.1: AER: enabled with IRQ 31

## windows pci-e 设置

[4.1]

#2 Type regedit and hit enter.

#3 Goto  HKEY_LOCAL_MACHINE/SYSTEM/CurrentControlSet/Control/Video 

#4 Identify the correct registry folders for each of graphics cards you have installed. There will be one associated folder for each card installed. To identify the correct folder for each card, you will need to review the names of each folder within the “HKEY_LOCAL_MACHINE/SYSTEM/CurrentControlSet/Control/Video” registry directory. The folder associated with a graphics card will have three or more subfolders (depending on how many PCI-E slots available on the motherboard). The values listed for each subfolder will be 0000, 0001, 0002, 0003, 0004, and Video. Review only the subfolders labeled as "0000". You will know you have selected the correct "0000" subfolder when you see a registry labeled “DriverDesc” with a value that matches the graphics card you have installed. 

#5 Right click on the folder labeled “0000”. Select “New”, then select “DWORD (32-bit) Value“, then enter “RMPcieLinkSpeed” for the name of the registry. 

#6 Right click the “RMPcieLinkSpeed” registry you just created, then select “Modify”, then enter “4” as the data value and verify that the “Hexadecimal” option is checked under “Base”, and then select “OK”. 

#7 Repeat steps 5 and 6 for each graphics card associated folder (named “0000”) 

#8 Once you have completed creating the RMPcieLinkSpeed registry for each card, close the Registry Editor window and restart your computer. 

## 主板 bios pci-e 设置

### 华硕
[3.1] 
以 x570-p 为例：高级->内置设备->M.2_1 link mode | M.2_2 link mode ，有 gen1~gen4 以及 自动 模式。


3.6
 高級菜單(Advanced) -> 3.6.5
 內置設備設置(OnBoard Devices Configuration)
 本項目可讓您切換 PCIe 通道並進行內置設備設置。

## 功耗
PCIe 的供电分为 3.3V 和 12V 两组，3.3V 最多支持 9.9W，12V 最多支持 66W，合计75.9W。

M.2 NVMe 硬盘供电只采用 3.3 V。如果也采用 PCIe 的供电方式，则最多可支持 9.9W 功耗。
三星 980 PRO NVMe M.2 PCIe 4.0 固态硬盘峰值功耗 8.9W [6.3]，而 980 NVMe M.2 PCIe 3.0 峰值功耗 5.3 W 。

功耗等级：

```
sudo smartctl -a /dev/nvme0

Supported Power States
St Op     Max   Active     Idle   RL RT WL WT  Ent_Lat  Ex_Lat
 0 +     8.37W       -        -    0  0  0  0        0       0
 1 +     8.37W       -        -    1  1  1  1        0     200
 2 +     8.37W       -        -    2  2  2  2        0     200
 3 -   0.0500W       -        -    3  3  3  3     2000    1200
 4 -   0.0050W       -        -    4  4  4  4      500    9500
```

关闭自动功耗管理：

adding the option nvme_core.default_ps_max_latency_us=0 to the Grub Linux launch command

Add pcie_aspm=off to GRUB_CMDLINE_LINUX_DEFAULT in /etc/default/grub file and run sudo update-grub

nvme get-feature -f 0x0c -H /dev/nvme0 测试APST是否关闭。

[6.6]
PCIe ASPM policies are set in /sys/module/pcie_aspm/parameters/policy, but can also be specified at boot time with the pcie_aspm kernel parameter, where pcie_aspm=off disables ASPM and pcie_aspm=force enables ASPM, even on devices that do not support ASPM. 
