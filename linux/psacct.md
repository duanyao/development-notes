参考资料：
User status and activity monitoring in Linux with GNU acct
https://www.redhat.com/sysadmin/linux-system-monitoring-acct

Detailed CPU Usage Overview With psacct
https://gryzli.info/2019/04/27/detailed-cpu-usage-overview-with-psacct/

psacct，可以定时记录各个进程的信息，包括 CPU、内存占用。
 
