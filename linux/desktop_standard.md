## 参考资料
[1.0] Association between MIME types and applications
https://specifications.freedesktop.org/mime-apps-spec/mime-apps-spec-1.0.1.html

[1.1] Default applications
https://wiki.archlinux.org/index.php/Default_applications

[1.2] ubuntu修改文件的默认打开方式命令大全
https://www.2cto.com/kf/201710/689598.html

[1.3] MIME
https://wiki.debian.org/MIME

[1.4] update-desktop-database (1) - Linux Man Pages
https://www.systutorials.com/docs/linux/man/1-update-desktop-database/

[1.5]
https://askubuntu.com/questions/154906/which-default-list-should-i-modify-for-default-applications-and-what-are-the-dif

[1.6] Override the default registered application for individual users
https://help.gnome.org/admin/system-admin-guide/stable/mime-types-application-user.html.en

[1.7] xdg-open and xdg-mime disagree on the default handler app
https://gitlab.freedesktop.org/xdg/xdg-utils/-/issues/147

[1.8] /usr/share/applications/mimeinfo.cache is used for default applications
https://bugzilla.mozilla.org/show_bug.cgi?id=727422

[1.9] Mozilla should follow shared mime freedesktop specs (determining MIME type of a file and the default app for it)
https://bugzilla.mozilla.org/show_bug.cgi?id=296443

[2] How to manage bookmarks in Nautilus
http://www.techrepublic.com/blog/linux-and-open-source/how-to-manage-bookmarks-in-nautilus/

[3.1] Deepin 开发指南
http://deepin.lolimay.cn/intro/start.html

[4.1] https://askubuntu.com/questions/534135/how-to-fix-unity-locked-disabled-settings

[4.2] https://bbs.archlinux.org/viewtopic.php?id=165485

[5.3] Running a .desktop file in the terminal
https://askubuntu.com/questions/5172/running-a-desktop-file-in-the-terminal

[6.1] Desktop Notifications Specification
https://www.galago-project.org/specs/notification/0.9/index.html

[6.2] HTML5 Web Notifications Test
https://www.bennish.net/web-notifications.html

[6.3] Bug 484945 - D-Bus activation needs a way to prefer one service over the other depending on the running desktop environment 
https://bugzilla.redhat.com/show_bug.cgi?id=484945

[6.5] DDE 与 KDE 对于dbus接口 “org.freedesktop.Notifications” 的服务互相冲突
https://github.com/linuxdeepin/dde-session-ui/issues/183

[7] XDG Base Directory Specification
https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html

[11] Desktop Bookmark Spec
https://www.freedesktop.org/wiki/Specifications/desktop-bookmark-spec/

[12] The FreeDesktop.org Trash specification
https://specifications.freedesktop.org/trash-spec/trashspec-latest.html

[13.1] Desktop Entry Specification
https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html

[14.1] Desktop Application Autostart Specification
https://specifications.freedesktop.org/autostart-spec/autostart-spec-latest.html

[15.1] What does the StartupWMClass field of a .desktop file represent?
https://askubuntu.com/questions/367396/what-does-the-startupwmclass-field-of-a-desktop-file-represent

[15.2] Application Based GNOME 3
https://wiki.gnome.org/Projects/GnomeShell/ApplicationBased

[15.3] Firefox profiles with different icons in Ubuntu dock
https://askubuntu.com/questions/1000818/firefox-profiles-with-different-icons-in-ubuntu-dock

[16.1] Using files in /etc/environment.d
https://askubuntu.com/questions/1458973/using-files-in-etc-environment-d

[16.2] environment.d — Definition of user service environment
https://www.freedesktop.org/software/systemd/man/latest/environment.d.html#

## 特殊目录

注意，XDG_* 不是环境变量。
在 ~/.config/user-dirs.dirs 中定义：

XDG_DESKTOP_DIR="$HOME/桌面"
XDG_DOWNLOAD_DIR="$HOME/下载"
XDG_TEMPLATES_DIR="$HOME/.Templates"
XDG_PUBLICSHARE_DIR="$HOME/.Public"
XDG_DOCUMENTS_DIR="$HOME/文档"
XDG_MUSIC_DIR="$HOME/音乐"
XDG_PICTURES_DIR="$HOME/图片"
XDG_VIDEOS_DIR="$HOME/视频"

此处没有定义的默认值在[7]中定义：

XDG_DATA_HOME: $HOME/.local/share 
XDG_CONFIG_HOME: $HOME/.config
XDG_DATA_DIRS: /usr/local/share/:/usr/share/
XDG_CACHE_HOME: $HOME/.cache

自启动配置目录：

$XDG_CONFIG_HOME/autostart (~/.config/autostart by default)
全局配置在：
$XDG_CONFIG_DIRS/autostart (/etc/xdg/autostart by default)

## 选择桌面环境
环境变量：
```
XDG_SESSION_DESKTOP=deepin
```

## 默认应用、打开方式、文件关联、协议关联
参考[1.0-1.2]。

/etc/mime.types
  全局的扩展名与mime的映射关系。

/usr/share/applications/mimeapps.list
  全局默认关联，标准。
  deepin 中属于 deepin-default-settings 包。
  内容形如 image/png=/usr/share/applications/deepin-image-viewer.desktop

/usr/share/applications/mimeinfo.cache 
  全局可选关联（仅仅是备选，不指定优先级）。由 update-desktop-database 工具扫描 .desktop 文件而形成[1.3,1.4]。
  内容形如 image/png=comix.desktop;pinta.desktop;firefox.desktop;mcomix.desktop;deepin-image-viewer.desktop;display-im6.q16.desktop;
  虽然多个备选的排名不分先后，但有些程序还是会受到排名的影响，且优先级还高于 /usr/share/applications/mimeapps.list，例如 xdg-open 。

/etc/gnome/defaults.list
  全局默认关联，非标准，gnome 专用[1.5]，deepin 不存在。

/usr/share/applications/defaults.list
  全局默认关联，非标准，gnome 专用[1.5,1.6]，deepin 存在，但不属于任何包。

~/.config/mimeapps.list
  用户设置关联，标准。
  deepin 控制中心修改默认应用时、dde-file-mananger 修改打开方式时修改此文件。

~/.local/share/applications/mimeapps.list
  用户设置关联，标准，已废弃。
  deepin 上默认为空。
  仍然影响 nautilus，但似乎不影响 dde-file-manager、thunar。

~/.local/share/applications/mimeinfo.cache
  deepin 上可能存在，但作用不明。

.desktop 文件中，用 MimeType 属性指定自己可以处理的 mime 类型。

MimeType=image/jpeg;image/png;

### ~/.config/mimeapps.list 的语法

```
[Default Applications]
image/gif=deepin-image-viewer.desktop

[Added Associations]
image/gif=pinta.desktop;deepin-image-viewer.desktop;comix.desktop;
```

Default Applications 小节下的优先级高于 Added Associations，所以上面的例子 gif 是用 deepin-image-viewer 打开的。

### 用命令行查询或修改打开方式

查询文件的 mime 类型：
```
$ xdg-mime query filetype ~/Downloads/
inode/directory
```
查询 mime 类型的默认打开方式：
```
$ xdg-mime query default inode/directory
/usr/share/applications/dde-file-manager.desktop
```
显示查询步骤：

```
$ XDG_UTILS_DEBUG_LEVEL=2 xdg-mime query default inode/directory
Checking /home/duanyao/.config/mimeapps.list
Checking /usr/share//applications/mimeapps.list
/usr/share/applications/dde-file-manager.desktop
```

修改 mime 类型的默认打开方式：
```
xdg-mime default org.gnome.Nautilus.desktop  inode/directory 
```
这修改的是 ~/.config/mimeapps.list 文件。

用默认打开方式打开文件：

```
xdg-open ~/Downloads/
```

注意，xdg-open 选择的程序可能与 xdg-mime query default 输出的程序不同。
在用户级没有明确设置打开方式时，前者可能受到 /usr/share/applications/mimeinfo.cache 的排序的影响，优先级高于 /usr/share/applications/mimeapps.list，但后者不会。
在 deepin 中测试了 debian 的 xdg-utils 从 1.1.1-1+deb9u1 到 1.1.3-4.1 ，都有这个毛病。
这种情况下，最好用 xdg-mime default 明确设置用户级打开方式。

值得注意的是，gio.app_info_get_all_for_type 也有 xdg-open 类似的毛病：

```
python -c 'import gio; print gio.app_info_get_all_for_type("inode/directory")'
python -c 'import gio; print gio.app_info_get_default_for_type("inode/directory", False)'
```

### 默认浏览器的配置
在 ~/.config/mimeapps.list 的 `[Default Applications]` 下面，以 firefox-esr 为例：

```
text/html=firefox-esr.desktop
x-scheme-handler/http=firefox-esr.desktop
x-scheme-handler/https=firefox-esr.desktop
x-scheme-handler/ftp=firefox-esr.desktop
text/xml=firefox-esr.desktop
text/xhtml_xml=firefox-esr.desktop
text/xhtml+xml=firefox-esr.desktop
```
可见这涉及不止一项配置。

用命令查询、设置默认浏览器：

```
xdg-settings get default-web-browser
xdg-settings set default-web-browser iceweasel.desktop
```
或者：

```
xdg-mime default google-chrome.desktop x-scheme-handler/https
xdg-mime default google-chrome.desktop x-scheme-handler/http
```

### 一些特殊的mime类型

inode/directory : 目录，用于文件管理器。有些 IDE 也会注册这个类型，例如：

```
/usr/share/applications/mimeapps.list:
inode/directory=/usr/share/applications/dde-file-manager.desktop

/usr/share/applications/mimeinfo.cache:
inode/directory=code.desktop;nemo.desktop;dde-file-manager.desktop;org.gnome.Nautilus.desktop;
```
### 应用分类
在 .desktop 文件的 Categories 属性中，如
Categories=GNOME;GTK;Utility;Core;FileManager;

一些重要的分类：
  FileManager: 文件管理器。
  WebBrowser: 浏览器。

### 错误的协议关联引起的问题（案例）

因为未知的原因，~/.local/share/applications/mimeapps.list 中将 file: 协议关联到了 exo-file-manager (属于包 exo-utils, Xfce 的一部分)上。
效果是：nautilus 中双击任何文件时，都调用 exo-file-manager.desktop 打开；而 exo-file-manager.desktop 最终又调用 nautilus 打开此文件，结果相当于最终什么也没做。

/usr/share/applications/exo-file-manager.desktop 的主要内容是：
Exec=exo-open --launch FileManager %u

$ grep -Irn exo ~/.local

~/.local/share/applications/mimeapps.list:73:x-scheme-handler/file=exo-file-manager.desktop

去掉 x-scheme-handler/file=exo-file-manager.desktop 这一行就正常了。实际上，也可以完全清空 ~/.local/share/applications/mimeapps.list 文件。

## 应用程序的 .desktop 文件
[13.1]
个人的：
~/.local/share/applications
全局的：
/usr/share/applications

同名的个人 desktop 会覆盖全局的。

应用商店(deepin)的：
`/opt/apps/<app_name>/entries/applications/*.desktop` ，例如：

`/opt/apps/org.gnome.gnome-boxes/entries/applications/org.gnome.Boxes.desktop`

`NoDisplay=true` 属性会导致开始菜单不显示这个 .desktop 文件。

有些程序只想在特定桌面环境中的开始菜单中显示，则可以设置
`NotShowIn=KDE;GNOME;Deepin;`
或者：
`OnlyShowIn=LxQt;`
NotShowIn 和 OnlyShowIn 最多只能出现一个。

## 自启动程序
[14.1]
用户配置：
$XDG_CONFIG_HOME/autostart (~/.config/autostart by default)
全局配置：
$XDG_CONFIG_DIRS/autostart (/etc/xdg/autostart by default)

这些目录下的 .desktop 文件会自启动。

有些程序只想在特定桌面环境中自启动，则可以设置
`NotShowIn=KDE;GNOME;Deepin;`
或者：
`OnlyShowIn=LxQt;`

NotShowIn 和 OnlyShowIn 最多只能出现一个。

设置 Hidden=true 则完全禁止自启动。NoDisplay 属性应该没影响。

如果用户配置和全局配置中有同名文件，则只有前者有效。因此，可以通过在用户配置中创建同名的 .desktop 文件，来覆盖全局设置，包括禁止某个程序自启动。

## 从命令行启动 .desktop 文件
[5.3]
目前并没有标准化。xdg-open 不是启动 .desktop 文件，而是根据其 mime 类型调用编辑器去打开 .desktop 文件。

启动.desktop 文件的方法：
```
    exo-open program_name.desktop  # exo-utils, xfce 的工具
    gtk-launch program_name.desktop # libgtk-3-bin
    kioclient exec program_name.desktop # kde-runtime，kde 的工具
    dex program_name.desktop # dex，独立的工具
```

## 默认终端
有些应用（如文件管理器）有“在终端中打开”的功能，这时启动的程序即为默认终端。
默认终端的配置没有标准化，它没有关联的mime或协议类型。
deepin 的配置在 gconf 数据库中，路径是：com.deepin.desktop.default-applications.terminal.exec
，值是终端程序的命令名，如 deepin-terminal 或者 gnome-terminal ，默认值是 deepin-terminal 。

可以用命令行查找，修改：

```
gsettings get com.deepin.desktop.default-applications.terminal exec
gsettings set com.deepin.desktop.default-applications.terminal exec gnome-terminal
```

gnome 的设置可能是 org.gnome.desktop.default-applications.terminal.exec ，deepin 上的默认值是
'x-terminal-emulator' 。

```
gsettings get org.gnome.desktop.default-applications.terminal exec
'x-terminal-emulator'
```

cinnamon 的设置可能是 org.cinnamon.desktop.default-applications.terminal ，deepin 上的默认值是 gnome-terminal 。
```
gsettings get org.cinnamon.desktop.default-applications.terminal exec
'gnome-terminal'
gsettings set org.cinnamon.desktop.default-applications.terminal exec 'x-terminal-emulator'
```
nemo 文件管理器使用 org.cinnamon.desktop.default-applications.terminal 的设置。

在 deepin 中，x-terminal-emulator 实际上是指向 /usr/lib/deepin-daemon/default-terminal ，后者应该是查找 com.deepin.desktop.default-applications.terminal.exec 来选择启动的终端程序。所以，在 deepin 中没必要修改 org.gnome.desktop.default-applications.terminal exec 。

不过，nautilus 3.22.3-1 和 nautilus-open-terminal 0.20-51 似乎有 bug，并不尊重 org.gnome.desktop.default-applications.terminal 的值，而是总是去找 /usr/bin/gnome-terminal ，如果不存在，则无法打开终端。如果 sudo ln -s /usr/bin/deepin-terminal /usr/bin/gnome-terminal ，则可以打开 deepin-terminal 。

## 书签
### gtk 3 适用于 nautilus、xfce thunar、mint 等[2]：
~/.config/gtk-3.0/bookmarks

nautilus 也支持 ~/.gtk-bookmarks 

### 适用于 deepin file-manager
~/.config/deepin/dde-file-manager/bookmark.json

### freedesktop 标准
[11]。
用户手工添加的书签
  $XDG_USER_DATA/shortcuts.xbel
似乎
  
最近使用的文件
  $XDG_USER_DATA/recently-used.xbel

$XDG_USER_DATA 是 ~/.local/share/

## 最近打开的文件
~/.local/share/recently-used.xbel

## 回收站
[12]
Linux 桌面每个用户有一处回收站，即 ~/.local/share/Trash ，这是必备的。每个分区还可以有一个回收站，即 分区根目录/.Trash ，这是可选的。然而，大部分 Linux 桌面（包括）没有实现后者，所以删除不在 /home 分区的文件时，要么直接删除，要么复制到用户回收站。

## dconf 设置
存储于 ~/.config/dconf/user 文件，是个数据库格式。

可以用 dconf-editor 或者 gsettings（命令行）来编辑。

dconf 文件有可能会损坏，造成设置无法被编辑，这时可以重建它[4.1]：
mv ~/.config/dconf/ ~/.config/dconf.bak

## startx

startx /usr/bin/startfluxbox
startx /usr/bin/startkde
startx /usr/bin/startdde

## 选择 display manager
debain:

# sudo dpkg-reconfigure lightdm
# sudo dpkg-reconfigure gdm3
# sudo dpkg-reconfigure sddm
# sudo dpkg-reconfigure kdm

修改后要重启生效，注销无法变更 display manager 。

## 启动过程

使用 startx：

startx
  xinit
    ~/.xinitrc
    /etc/X11/xinit/xinitrc # 一般仅仅执行 /etc/X11/Xsession


使用 display manager:

display-manager
  display-manager scripts # 在这里执行 /etc/profile, $HOME/.profile 等。名字不固定，如 /etc/kde4/kdm/Xsession，/etc/sddm/Xsession

共同步骤：

/etc/X11/Xsession
  /etc/X11/Xsession.d/*
  ~/.xsession, ~/.xsessionrc, ~/.Xsession

有的发行版在这里执行 /etc/profile ，如 deepin (X11/Xsession.d/01deepin-profile) 。
  
## profile

/etc/kde4/kdm/Xsession:11:    [ -f /etc/profile ] && . /etc/profile
/etc/kde4/kdm/Xsession:41:    [ -f /etc/profile ] && . /etc/profile

/etc/sddm/Xsession:11:    [ -f /etc/profile ] && . /etc/profile
/etc/sddm/Xsession:41:    [ -f /etc/profile ] && . /etc/profile
/etc/sddm/Xsession:48:    [ -f /etc/profile ] && . /etc/profile

/etc/X11/Xsession.d/01deepin-profile:1:[ -f /etc/profile ] && . /etc/profile

## xprofile
理论上所有的 DM 都应该支持，gdm、lightdm 等是支持的，但 sddm 可能支持，但 debian 版本似乎不支持：

https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=931485

## 环境变量的初始化
标准不统一，可能在多个地方初始化、修改环境变量：
* /etc/environment 
  systemd --user 从以下位置加载配置文件并设置环境变量：
  ```
  ~/.config/environment.d/*.conf
  /etc/environment.d/*.conf
  /run/environment.d/*.conf
  /usr/local/lib/environment.d/*.conf
  /usr/lib/environment.d/*.conf
  /etc/environment
  ```
  注意，以上文件并非专属于 systemd ，shell 启动时一般也会读取并设置环境变量（但可以被要求不读取）。
  格式：
  ```
  # /etc/environment.d/60-foo.conf:
  FOO_DEBUG=force-software-gl,log-verbose
  PATH=/opt/foo/bin:$PATH
  LD_LIBRARY_PATH=/opt/foo/lib${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}
  XDG_DATA_DIRS=/opt/foo/share:${XDG_DATA_DIRS:-/usr/local/share/:/usr/share/}
  ```
  这里可以用变量展开（`$PATH`）或者条件展开（ `${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}` ）和（ `${XDG_DATA_DIRS:-/usr/local/share/:/usr/share/}` ）。
  `${FOO:+ALTERNATE_VALUE}` 当 `${FOO}` 非空时返回 `ALTERNATE_VALUE`，否则返回空。`${FOO:-DEFAULT_VALUE}` 当 `${FOO}` 非空时返回 `${FOO}`，否则返回 `DEFAULT_VALUE`。

* display manager：可能从以下位置执行脚本，脚本设置环境变量：
  ```
  /etc/X11/Xsession
  /etc/X11/Xsession.d/*
  ~/.xsession
  ~/.xsessionrc
  ~/.Xsession
  /etc/kde4/kdm/Xsession
  /etc/sddm/Xsession
  /etc/xprofile
  ~/.xprofile
  ```
  但 display manager 可以被配置为不执行它们。

* profile
  ```
  /etc/profile.d/*
  /etc/profile
  ~/.profile
  ```
  桌面系统中，以上文件可能由 display manager 或者 /etc/X11/Xsession.d/ 中的脚本调用，但 display manager 可以被配置为不执行它们。
  以上文件并非专属于 systemd ，shell 启动时一般也会读取并执行（但可以被要求不读取）。

* systemd --user 的 dbus 接口 org.freedesktop.systemd1 的方法 SetEnvironment，可以设置/获得环境变量，systemd --user 随后创建的进程会获得这些环境变量。
  例如在 `/etc/X11/Xsession.d/00deepin-dde-env` 中：
  ```
  EXCLUDED_ENV="XDG_VTNR\|XDG_SESSION_ID\|XDG_SEAT\|XDG_SESSION_TYPE\|XMODIFIERS\|GTK_IM_MODULE\|QT_IM_MODULE\|CLUTTER_IM_MODULE\|SDL_IM_MODULE"
  eval "$(
	busctl --json=short --user get-property org.freedesktop.systemd1 /org/freedesktop/systemd1 org.freedesktop.systemd1.Manager Environment |
		jq .data\[\] --raw-output |
		grep -v "$EXCLUDED_ENV" | # handle special character in environment
		sed -r 's/\\/\\\\/' |
		sed -r 's/'\''/\\'\''/' |
		sed -r 's/"/\\"/' |
		sed -r 's/\$/\\$/' |
		sed -r 's/^([^=]+)=(.*)$/export \1="\2"/'
  )"
  ```

* PAM 系统
  PAM 系统设置的环境变量会同时影响GUI和文本控制台。
  PAM 会读以下配置文件：
  ```
  ~/.pam_environment
  ```
  语法与 /etc/environment 类似但似乎不全相同，参考 `/etc/security/pam_env.conf` 的注释。
  `~/.pam_environment` 默认不存在，但 PAM 系统还是会设置一些环境变量的默认值。这个过程在 `~/.profile` 执行之前。

* `~/.bashrc`
  以下文件：
  ```
  ~/.bashrc
  ~/.bash_profile
  ~/.bash_login
  ```
  在一个 bash 进程初始化时执行。注意它不会影响 GUI 会话，也不一定会被非 bash 的 shell 执行。
  
## 桌面开发
[3.1]

## 桌面通知
[6.1]
桌面通知通过一组dbus接口发送。通常，桌面环境的某个进程（以下称为通知服务）负责实现这组接口，而应用程序则调用这组接口。
接口名为：org.freedesktop.Notifications ，服务定义文件位于 /usr/share/dbus-1/services/ ，例如 

com.deepin.dde.freedesktop.Notification.service
org.kde.plasma.Notifications.service

一个可以用来测试系统通知的命令行工具是：
```
notify-send "title" "body"
```

或者用

```
dbus-send --session --print-reply --dest=org.freedesktop.Notifications /org/freedesktop/Notifications org.freedesktop.Notifications.GetCapabilities
gdbus call --session --dest org.freedesktop.Notifications --object-path /org/freedesktop/Notifications --method org.freedesktop.DBus.Introspectable.Introspect
```

如果通知服务没有运行，dbus接口不存在，dbus 会尝试激活对应的服务；如果无法激活，则 notify-send 不会报错，而是等待一段时间后退出。

deepin 系统中，实现通知服务的程序为 dde-osd (dde-session-ui: /usr/lib/deepin-daemon/dde-osd)。

如果安装了多个桌面环境，每个桌面环境自带其dbus服务程序，则可能相互冲突，例如DDE和KDE和XFCE[6.3]。

浏览器的通知功能[6.2]可以利用桌面通知实现，有的浏览器（如firefox）也有后备的内部实现，当桌面通知不可用时启用。

## 任务栏

### 驻留任务栏
没有标准。dde 的实现是：

* d-conf 中配置：
```
com.deepin.dde.dock docked-apps ['/S@dde-control-center', '/S@deepin-system-monitor', '/D@w:4fef4ae0e4fe762d3741f8e7dcaba709', '/S@org.rnd2.cpupower_gui', '/S@org.kde.kate', '/S@gnome-system-monitor', '/S@org.gnome.Nautilus', '/S@deepin-terminal', '/D@d:e0cc8ad6f81f5a16709dec3e97c99b3b', '/D@w:8d9db31e27f1d9225da48a14ca8e705d']
```

"/S@" 开头的应该是”系统“的 .desktop 文件的名字，这些文件可以在 /usr/share/applications 和 ~/.local/share/applications 目录下。

"/D@"开头的应该是“自定义”的程序，它们没有预置的 .desktop 文件，用于点击“驻留”时，dde-dock自动创建对应名字的 .desktop 和 *.sh 文件，在以下目录：`~/.config/dock/scratch/`，例如：

```
w:8d9db31e27f1d9225da48a14ca8e705d.sh
w:8d9db31e27f1d9225da48a14ca8e705d.desktop
```
前者的内容是：

```
#!/bin/sh
cd "/home/duanyao/project/object-detection-metrics-validation.git"
exec "/usr/bin/python3.9" "/home/duanyao/project/review_object_detection_metrics.git/run.py" $@
```

### 任务栏窗口分组
[15.1-15.3]
一般按照窗口的 application id / WM_CLASS 属性来分组。详见 “application id”。

## application id

wayland 的每个窗口有 application id 的概念，但 X 没有（ 接近的概念是 WM_CLASS ）。

在没有 application id 的系统/应用中，使用 X window 的 WM_CLASS 属性来识别一个窗口属于哪个应用。
可以用 xprop WM_CLASS 命令来查看一个窗口的 WM_CLASS 属性。

在 .desktop 文件中， StartupWMClass 字段表明了一个应用的特征的 WM_CLASS 值。

一般来说，WM_CLASS 使用简短的应用程序名称（与其 .desktop 文件同名），并且不要用本地化名，例如：qtcreator 。

