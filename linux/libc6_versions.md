|  OS version                       |  libc6 version |
|---|---|
|  debian 10 (Buster)               |  2.28-10 |
|  Debian 11 (Bullseye)             |  2.31-13 |
|  Debian 12 (Bookworm)             |  2.36-9  |
|  Debian Sid (2024.2.18)           |  2.37-15 |
|  ubuntu 18.04 bionic              |  2.27-3  |
|  Ubuntu 20.04 LTS (Focal Fossa)   |  2.31-0  |
|  Ubuntu 22.04 (Jammy Jellyfish)   |  2.35-0  |
|  Ubuntu 23.10 (Mantic Minotaur)   |  2.38-1  |
|  deepin 23b3 (beige)              |  2.37-12 |
|  deepin 23 (beige)                |  2.38-6  |
