## 参考资料

VNC ( Virtual Network Computing ) 
https://wiki.centos.org/HowTos/VNC-Server

https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-vnc-on-ubuntu-22-04

https://www.cyberciti.biz/faq/install-and-configure-tigervnc-server-on-ubuntu-18-04/

https://wiki.archlinux.org/title/TigerVNC

Help with a systemd user service that works under v1.11.0+
https://github.com/TigerVNC/tigervnc/issues/1096

Configuring the VNC Service
https://docs.oracle.com/en/operating-systems/oracle-linux/7/network/network-ConfiguringtheVNCService.html#ol7-vnc-connect

Server version 1.11.0 not executing '~/.vnc/xstartup'
https://github.com/TigerVNC/tigervnc/issues/1109

## 安装
以 ubuntu/deepin 为例。
服务器端安装

```
sudo apt update


sudo apt install tigervnc-standalone-server 
sudo apt install tigervnc-xorg-extension 
sudo apt install tigervnc-viewer

sudo apt install xfce4 xfce4-goodies
sudo apt install ubuntu-gnome-desktop
sudo systemctl enable gdm
sudo systemctl start gdm
```

客户端安装

```
sudo apt install tigervnc-viewer
```

## 设置和使用

### 设置 vnc 密码
vnc 密码是每个 linux 用户一个，不是全局的，存储在 `~/.vnc/passwd`。
```
vncpasswd
Password:
Verify:
Would you like to enter a view-only password (y/n)? n

ls -l ~/.vnc/
总用量 4
-rw------- 1 rd rd 8 8月   3 16:15 passwd
```

### vnc xstartup 的配置

可执行shell 脚本 `~/.vnc/xstartup` 决定如何启动桌面。

```
touch ~/.vnc/xstartup
chmod +x ~/.vnc/xstartup
```

启动默认桌面的样例：
```
#!/bin/sh
unset SESSION_MANAGER
exec /etc/X11/xinit/xinitrc
```

启动gnome的样例

```
#!/bin/sh
# Start Gnome 3 Desktop 
[ -x /etc/vnc/xstartup ] && exec /etc/vnc/xstartup
[ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources
vncconfig -iconic &
dbus-launch --exit-with-session gnome-session &
```

启动 xfce4 的样例：
```
#!/bin/sh
unset SESSION_MANAGER
unset DBUS_SESSION_BUS_ADDRESS
exec startxfce4
```

另一个 xfce4 样例：
```
#!/bin/bash
xrdb $HOME/.Xresources
startxfce4 &
```

### 启动和管理 vncserver

一个 linux 用户可以启动一个或多个 vncserver 实例。命令是

```
# vncserver -depth {8|16|24|32} -geometry {width}x{height} -localhost yes|no

vncserver -localhost no
vncserver -depth 32 -geometry 1680x1050
```
vncserver -list

```
vncserver -list

TigerVNC server sessions:

X DISPLAY #     RFB PORT #      PROCESS ID
:1              5901            622134
```

关闭 vncserver 会话
```
vncserver -kill :1 # 显示器1
vncserver -kill :* # 所有显示器
```

### 启动 vnc 客户端

使用端口号或者显示器号是等效的：
```
vncviewer gtrd.local:5901

vncviewer gtrd.local:1
```

## 多头显示问题
有的程序可以同时在本地屏幕上和vnc屏幕上显示，或同时在多个vnc屏幕上显示，并使用多个 X server实例，这称作多头（multi head）显示。大部分比较简单的桌面程序是支持多头显示的，如终端、文本编辑器。
但也有些程序不支持多头显示，如 firefox 和 chrome（也包括基于 chromium 的内核的程序）。firefox 和 chrome 在为不同的屏幕指定不同的用户数据目录时，则可以支持多头显示。

参考：
Issue 15781: Support multiple X displays
https://bugs.chromium.org/p/chromium/issues/detail?id=15781

## systemd --user 用户服务重复启动问题
同一用户同时登录本地屏幕和vnc屏幕时，systemd 用户服务往往会重复启动（测试过本地 gnome + vnc xfce4）。

## ~/autostart 重复启动问题

## 输入法问题
