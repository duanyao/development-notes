## 参考资料
[1] How to switch GCC version using update-alternatives
  https://codeyarns.com/2015/02/26/how-to-switch-gcc-version-using-update-alternatives/

[2] How to Install Linux Kernel 4.17 in Ubuntu 18.04
  http://ubuntuhandbook.org/index.php/2018/06/install-linux-kernel-4-17-ubuntu-18-04/

## 问题

### dkms/nvidia 对 gcc 版本的要求比系统默认的高

```
/var/lib/dkms/nvidia-current/390.67/build/make.log:

DKMS make.log for nvidia-current-390.67 for kernel 4.20.10-042010-generic (x86_64)
2019年 02月 17日 星期日 17:52:53 CST

Compiler version check failed:

The major and minor number of the compiler used to
compile the kernel:

gcc version 8.2.0 (Ubuntu 8.2.0-20ubuntu1)

does not match the compiler used here:

gcc (Debian 7.3.0-19) 7.3.0
```

```
ls -la /usr/bin/ | grep gcc

lrwxrwxrwx  1 root    root             5 4月   4  2018 gcc -> gcc-7
lrwxrwxrwx  1 root    root            22 5月  12  2018 gcc-7 -> x86_64-linux-gnu-gcc-7
lrwxrwxrwx  1 root    root            22 5月  12  2018 gcc-8 -> x86_64-linux-gnu-gcc-8
lrwxrwxrwx  1 root    root             8 4月   4  2018 gcc-ar -> gcc-ar-7
lrwxrwxrwx  1 root    root             8 4月   4  2018 gcc-ar -> gcc-ar-7
lrwxrwxrwx  1 root    root            25 5月  12  2018 gcc-ar-7 -> x86_64-linux-gnu-gcc-ar-7
lrwxrwxrwx  1 root    root            25 5月  12  2018 gcc-ar-8 -> x86_64-linux-gnu-gcc-ar-8
lrwxrwxrwx  1 root    root             8 4月   4  2018 gcc-nm -> gcc-nm-7
lrwxrwxrwx  1 root    root            25 5月  12  2018 gcc-nm-7 -> x86_64-linux-gnu-gcc-nm-7
lrwxrwxrwx  1 root    root            25 5月  12  2018 gcc-nm-8 -> x86_64-linux-gnu-gcc-nm-8
lrwxrwxrwx  1 root    root            12 4月   4  2018 gcc-ranlib -> gcc-ranlib-7
lrwxrwxrwx  1 root    root            29 5月  12  2018 gcc-ranlib-7 -> x86_64-linux-gnu-gcc-ranlib-7
lrwxrwxrwx  1 root    root            29 5月  12  2018 gcc-ranlib-8 -> x86_64-linux-gnu-gcc-ranlib-8
lrwxrwxrwx  1 root    root             5 4月   4  2018 x86_64-linux-gnu-gcc -> gcc-7
-rwxr-xr-x  1 root    root       1006432 5月  12  2018 x86_64-linux-gnu-gcc-7
-rwxr-xr-x  1 root    root       1055608 5月  12  2018 x86_64-linux-gnu-gcc-8
lrwxrwxrwx  1 root    root             8 4月   4  2018 x86_64-linux-gnu-gcc-ar -> gcc-ar-7
-rwxr-xr-x  1 root    root         27104 5月  12  2018 x86_64-linux-gnu-gcc-ar-7
-rwxr-xr-x  1 root    root         27104 5月  12  2018 x86_64-linux-gnu-gcc-ar-8
lrwxrwxrwx  1 root    root             8 4月   4  2018 x86_64-linux-gnu-gcc-nm -> gcc-nm-7
-rwxr-xr-x  1 root    root         27104 5月  12  2018 x86_64-linux-gnu-gcc-nm-7
-rwxr-xr-x  1 root    root         27104 5月  12  2018 x86_64-linux-gnu-gcc-nm-8
lrwxrwxrwx  1 root    root            12 4月   4  2018 x86_64-linux-gnu-gcc-ranlib -> gcc-ranlib-7
-rwxr-xr-x  1 root    root         27104 5月  12  2018 x86_64-linux-gnu-gcc-ranlib-7
-rwxr-xr-x  1 root    root         27104 5月  12  2018 x86_64-linux-gnu-gcc-ranlib-8

```

```
ls -la /usr/bin/ | grep g++

lrwxrwxrwx  1 root    root             5 4月   4  2018 g++ -> g++-7
lrwxrwxrwx  1 root    root            22 4月  24  2018 g++-6 -> x86_64-linux-gnu-g++-6
lrwxrwxrwx  1 root    root            22 5月  12  2018 g++-7 -> x86_64-linux-gnu-g++-7
lrwxrwxrwx  1 root    root             5 4月   4  2018 x86_64-linux-gnu-g++ -> g++-7
-rwxr-xr-x  1 root    root        949016 4月  24  2018 x86_64-linux-gnu-g++-6
-rwxr-xr-x  1 root    root       1006432 5月  12  2018 x86_64-linux-gnu-g++-7
```

```
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8 100
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 50
```

```
ls -la /usr/bin/ | grep gcc
lrwxrwxrwx  1 root    root            21 2月  17 18:46 gcc -> /etc/alternatives/gcc

ls -la /etc/alternatives/gcc
lrwxrwxrwx 1 root root 14 2月  17 18:46 /etc/alternatives/gcc -> /usr/bin/gcc-8
```

### dkms nvidia-current 编译错误

```
/var/lib/dkms/nvidia-current/390.67/build/make.log

DKMS make.log for nvidia-current-390.67 for kernel 4.20.10-042010-generic (x86_64)

/var/lib/dkms/nvidia-current/390.67/build/nvidia/os-interface.c:1700:5: error: unknown type name ‘ipmi_user_t’
     ipmi_user_t         p_user;     // ptr to ipmi_msghandler user structure
     ^~~~~~~~~~~
/var/lib/dkms/nvidia-current/390.67/build/nvidia/os-interface.c:1709:5: error: unknown type name ‘ipmi_user_t’; did you mean ‘pci_power_t’?
     ipmi_user_t     user,
     ^~~~~~~~~~~
     pci_power_t
/var/lib/dkms/nvidia-current/390.67/build/nvidia/os-interface.c: In function ‘os_ipmi_connect’:
/var/lib/dkms/nvidia-current/390.67/build/nvidia/os-interface.c:1781:66: error: passing argument 4 of ‘ipmi_create_user’ from incompatible pointer type [-Werror=incompatible-pointer-types]
     err_no = ipmi_create_user(devIndex, &nv_ipmi_hndlrs, p_priv, &p_priv->p_user);
                                                                  ^~~~~~~~~~~~~~~
In file included from /var/lib/dkms/nvidia-current/390.67/build/common/inc/nv-linux.h:339,
                 from /var/lib/dkms/nvidia-current/390.67/build/nvidia/os-interface.c:15:
./include/linux/ipmi.h:114:32: note: expected ‘struct ipmi_user **’ but argument is of type ‘int *’
        struct ipmi_user      **user);
        ~~~~~~~~~~~~~~~~~~~~~~~~^~~~
/var/lib/dkms/nvidia-current/390.67/build/nvidia/os-interface.c:1793:41: warning: passing argument 1 of ‘ipmi_set_gets_events’ makes pointer from integer without a cast [-Wint-conversion]
     err_no = ipmi_set_gets_events(p_priv->p_user, 0);
                                   ~~~~~~^~~~~~~~
```

解决：删除包 nvidia-kernel-dkms 等 nvidia 二进制驱动程序。

###

```
DKMS make.log for mincores-0.2.0 for kernel 4.20.10-042010-generic (x86_64)
2019年 02月 17日 星期日 19:07:03 CST
make: 进入目录“/usr/src/linux-headers-4.20.10-042010-generic”
  CC [M]  /var/lib/dkms/mincores/0.2.0/build/mincores.o
/var/lib/dkms/mincores/0.2.0/build/mincores.c: In function ‘dump_mapping’:
/var/lib/dkms/mincores/0.2.0/build/mincores.c:122:5: error: implicit declaration of function ‘radix_tree_for_each_contig’; did you mean ‘radix_tree_for_each_slot’? [-Werror=implicit-function-declaration]
     radix_tree_for_each_contig(slot, &addr->page_tree, &iter, next_start) {
     ^~~~~~~~~~~~~~~~~~~~~~~~~~
     radix_tree_for_each_slot
/var/lib/dkms/mincores/0.2.0/build/mincores.c:122:43: error: ‘struct address_space’ has no member named ‘page_tree’
     radix_tree_for_each_contig(slot, &addr->page_tree, &iter, next_start) {
                                           ^~
/var/lib/dkms/mincores/0.2.0/build/mincores.c:122:74: error: expected ‘;’ before ‘{’ token
     radix_tree_for_each_contig(slot, &addr->page_tree, &iter, next_start) {
                                                                          ^~
                                                                          ;
/var/lib/dkms/mincores/0.2.0/build/mincores.c:113:26: warning: unused variable ‘end’ [-Wunused-variable]
   unsigned long start=0, end = 0, next_start = 0;
                          ^~~
/var/lib/dkms/mincores/0.2.0/build/mincores.c:113:17: warning: unused variable ‘start’ [-Wunused-variable]
   unsigned long start=0, end = 0, next_start = 0;
                 ^~~~~
cc1: some warnings being treated as errors
make[1]: *** [scripts/Makefile.build:298：/var/lib/dkms/mincores/0.2.0/build/mincores.o] 错误 1
make: *** [Makefile:1562：_module_/var/lib/dkms/mincores/0.2.0/build] 错误 2
make: 离开目录“/usr/src/linux-headers-4.20.10-042010-generic”
```

解决：删除 mincores-dkms 和 warm-sched （ https://github.com/snyh/warm-sched ）

###

```
DKMS make.log for deepin-anything-0.0 for kernel 4.20.10-042010-generic (x86_64)
2019年 02月 17日 星期日 19:06:57 CST
make: 进入目录“/usr/src/linux-headers-4.20.10-042010-generic”
  CC [M]  /var/lib/dkms/deepin-anything/0.0/build/vfs_utils.o
  CC [M]  /var/lib/dkms/deepin-anything/0.0/build/arg_extractor.o
  CC [M]  /var/lib/dkms/deepin-anything/0.0/build/vfs_change.o
  CC [M]  /var/lib/dkms/deepin-anything/0.0/build/vfs_kretprobes.o
/var/lib/dkms/deepin-anything/0.0/build/vfs_change.c: In function ‘copy_vfs_changes’:
/var/lib/dkms/deepin-anything/0.0/build/vfs_change.c:118:3: error: implicit declaration of function ‘time_to_tm’; did you mean ‘time64_to_tm’? [-Werror=implicit-function-declaration]
   time_to_tm(shifted_secs, 0, &ts);
   ^~~~~~~~~~
   time64_to_tm
cc1: some warnings being treated as errors
make[1]: *** [scripts/Makefile.build:291：/var/lib/dkms/deepin-anything/0.0/build/vfs_change.o] 错误 1
make: *** [Makefile:1562：_module_/var/lib/dkms/deepin-anything/0.0/build] 错误 2
make: 离开目录“/usr/src/linux-headers-4.20.10-042010-generic”
```

解决：
dde 将被删除
dde-file-manager 将被删除
deepin-anything-dkms 将被删除
deepin-anything-server 将被删除


### 依赖错误
```
sudo ukuu --install v4.20.10
Distribution: Deepin 15.9.1
System architecture: amd64
Running kernel: 4.15.0-29deepin-generic
Kernel version: 4.15.0.31
Using cache directory: /home/duanyao/.cache/ukuu
Found installed: 4.15.0-29deepin.31
Found installed: 4.15.0-21deepin.21
Preparing to install 'v4.20.10'

正在选中未选择的软件包 linux-image-unsigned-4.20.10-042010-generic。
(正在读取数据库 ... 系统当前共安装有 540111 个文件和目录。)
正准备解包 linux-image-unsigned-4.20.10-042010-generic_4.20.10-042010.201902150516_amd64.deb  ...
正在解包 linux-image-unsigned-4.20.10-042010-generic (4.20.10-042010.201902150516) ...
正在选中未选择的软件包 linux-headers-4.20.10-042010。
正准备解包 linux-headers-4.20.10-042010_4.20.10-042010.201902150516_all.deb  ...
正在解包 linux-headers-4.20.10-042010 (4.20.10-042010.201902150516) ...
正在选中未选择的软件包 linux-headers-4.20.10-042010-generic。
正准备解包 linux-headers-4.20.10-042010-generic_4.20.10-042010.201902150516_amd64.deb  ...
正在解包 linux-headers-4.20.10-042010-generic (4.20.10-042010.201902150516) ...
dpkg: 依赖关系问题使得 linux-image-unsigned-4.20.10-042010-generic 的配置工作不能继续：
 linux-image-unsigned-4.20.10-042010-generic 依赖于 linux-modules-4.20.10-042010-generic；然而：
  未安装软件包 linux-modules-4.20.10-042010-generic。

dpkg: 处理软件包 linux-image-unsigned-4.20.10-042010-generic (--install)时出错：
 依赖关系问题 - 仍未被配置
正在设置 linux-headers-4.20.10-042010 (4.20.10-042010.201902150516) ...
正在设置 linux-headers-4.20.10-042010-generic (4.20.10-042010.201902150516) ...
在处理时有错误发生：
 linux-image-unsigned-4.20.10-042010-generic
```

```
ls /home/duanyao/.cache/ukuu/v4.20.10/amd64/
linux-headers-4.20.10-042010_4.20.10-042010.201902150516_all.deb
linux-headers-4.20.10-042010-generic_4.20.10-042010.201902150516_amd64.deb
linux-image-unsigned-4.20.10-042010-generic_4.20.10-042010.201902150516_amd64.deb
```

原因是 ukuu 少装了 linux-modules-4.20.10-042010-generic 包。可以从这里下载[2]:
```
wget https://kernel.ubuntu.com/~kernel-ppa/mainline/v4.20.10/linux-modules-4.20.10-042010-generic_4.20.10-042010.201902150516_amd64.deb
```
安装

```
sudo ukuu --install v4.20.10
sudo dpkg -i /home/duanyao/.cache/ukuu/v4.20.10/amd64/*.deb ./linux-modules-4.20.10-042010-generic_4.20.10-042010.201902150516_amd64.deb
```
