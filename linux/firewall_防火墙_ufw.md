## 参考资料
[1.1] Linux 防火墙 ufw 简介 https://linux.cn/article-12079-1.html

[1.2] Security - Firewall
https://help.ubuntu.com/community/UFW

[1.3] 如何在 Ubuntu 20.04 上使用 UFW 来设置防火墙
https://zhuanlan.zhihu.com/p/139381645

[1.4] 简明教程｜Linux中UFW的使用
https://zhuanlan.zhihu.com/p/98880088

[5.1] 解决 UFW 和 Docker 的问题 https://github.com/chaifeng/ufw-docker

## 使用命令行
```
sudo ufw enable

sudo ufw disable

To see the firewall status, enter:

sudo ufw status

And for more verbose status information use:

sudo ufw status verbose

To view the numbered format:

sudo ufw status numbered


To open a port (SSH in this example):

sudo ufw allow 22

Rules can also be added using a numbered format:

sudo ufw insert 1 allow 80

Similarly, to close an opened port:

sudo ufw deny 22

To remove a rule, use delete followed by the rule:

# 这需要写出完整的规则，否则无法删除
sudo ufw delete deny 22

# 按 sudo ufw status numbered 给出的编号删除规则。注意，删除一条规则后会重新编号，要重新用 sudo ufw status numbered 查看后再删除下一条。
sudo ufw delete 1

sudo ufw allow proto tcp from 192.168.0.2 to any port 22 # any 是目标IP地址，即本机IP地址。

# 7000:64000 是端口范围，tcp或udp是传输层协议，192.168.0.0/16 是子网地址，这表示仅允许来自内网 192.168.X.X 的访问。
# to ... 可以改为 to any，应该不会有明显区别。
# 此处 port 7000:64000 proto tcp 不能简写为 port 7000:64000/tcp 或 7000:64000/tcp，可以省略 proto tcp 
sudo ufw allow from 192.168.0.0/16 to 192.168.0.0/16 port 5000:65000 proto tcp
sudo ufw allow from 192.168.0.0/16 to 192.168.0.0/16 port 5000:65000 proto udp


# fd42:bdea:2555:c000::/64 是 IPv6 地址段，同样表示来自同一内网
sudo ufw allow from fd42:bdea:2555:c000::/64 to fd42:bdea:2555:c000::/64 port 5000:64000 proto tcp
sudo ufw allow from fd42:bdea:2555:c000::/64 to fd42:bdea:2555:c000::/64 port 5000:64000 proto tcp
```

问题：
1. 省略 proto tcp 或 udp 时，效果是针对 tcp 还是 tcp 和 udp 都有效？
2. 针对 udp 的入站规则是否有意义？因为 udp 似乎无法区分发起方和接受方。

## 图形前端 gufw
```
apt install gufw
```

gufw 只能编辑它自己创建的规则，不能编辑 ufw 命令行创建的规则（但可以查看和删除）。

## 最佳实践

### 个人电脑的配置

```
sudo ufw enable

# 允许内网访问 ssh
sudo ufw allow from 192.168.0.0/16 to any port 22 proto tcp
sudo ufw allow from 10.0.0.0/8 to any port 22 proto tcp
sudo ufw allow from 172.16.0.0/12 to any port 22 proto tcp

# 允许特定端口段 25000:26000 内网入站访问。
sudo ufw allow from 192.168.0.0/16 to any port 25000:26000 proto tcp
sudo ufw allow from 192.168.0.0/16 to any port 25000:26000 proto udp
sudo ufw allow from 10.0.0.0/8 to any port 25000:26000 proto tcp
sudo ufw allow from 10.0.0.0/8 to any port 25000:26000 proto udp
sudo ufw allow from 172.16.0.0/12 to any port 25000:26000 proto tcp
sudo ufw allow from 172.16.0.0/12 to any port 25000:26000 proto udp

# 允许来自 docker 容器的大部分入站访问（docker 常用 172.{17-19}.0.0）
sudo ufw allow from 172.16.0.0/12 to any port 1000:65000 proto tcp
sudo ufw allow from 172.16.0.0/12 to any port 1000:65000 proto udp

# IPv6 规则。允许特定端口段 25000:26000 内网（link-local）访问。
sudo ufw allow from fe80::/10 to any port 22 proto tcp
sudo ufw allow from fe80::/10 to any port 25000:26000 proto tcp
sudo ufw allow from fe80::/10 to any port 25000:26000 proto udp

# IPv6 规则。允许特定端口段 25000:26000 内网（unique-local）访问。IPv6 如果是家用路由器 DHCP 分配的，则通常在这一段。
sudo ufw allow from fc00::/7 to any port 22 proto tcp
sudo ufw allow from fc00::/7 to any port 25000:26000 proto tcp
sudo ufw allow from fc00::/7 to any port 25000:26000 proto udp
```

ufw 默认是允许出站连接以及ICMP（用于ping）的，不用特意设置。

## 代码

代码仓库： https://code.launchpad.net/ufw
git clone https://git.launchpad.net/ufw ufw.git
