## 初始状态
硬盘分区：
```
/dev/nvme0n1p7: LABEL="sys1" UUID="e7889cac-3f57-4e3c-89c4-3b68ee1706de" UUID_SUB="9ebc4c41-b8b9-4603-b580-6df1bcac59dd" BLOCK_SIZE="4096" TYPE="btrfs" PARTLABEL="sys1" PARTUUID="fea18868-9a70-4121-b3cf-cb8c3d35d794"
/dev/nvme0n1p5: UUID="d908dc57-e95e-4df6-9b42-7eccca642c7e" TYPE="swap" PARTUUID="ddb36532-f577-4d9c-a5d6-16fb675919b9"
/dev/nvme0n1p3: LABEL="Windows" BLOCK_SIZE="512" UUID="96306C94306C7D5D" TYPE="ntfs" PARTLABEL="Basic data partition" PARTUUID="1fa2c6a2-a727-47e1-9c26-785235ad00e8"
/dev/nvme0n1p1: LABEL_FATBOOT="SYSTEM" LABEL="SYSTEM" UUID="08E0-C710" BLOCK_SIZE="512" TYPE="vfat" PARTLABEL="EFI system partition" PARTUUID="2e4346d5-d26a-4d49-863b-0ab9c9c2a562"
/dev/nvme0n1p6: LABEL="sys0" UUID="f79119c6-a687-4ae2-9980-11499b349c8d" BLOCK_SIZE="4096" TYPE="ext4" PARTUUID="dec48396-64e5-44a0-8fe5-0c32e7b55e6d"
/dev/nvme0n1p4: LABEL="Windows RE Tools" BLOCK_SIZE="512" UUID="3648137E48133BD5" TYPE="ntfs" PARTLABEL="Basic data partition" PARTUUID="804e9232-3675-4695-8dd2-11d1af3265ec"
```

ubuntu2304安装在 nvme0n1p7，预先格式化为 btrfs，没有划分子卷。nvme0n1p1 为 ESP 分区。

ESP 分区上的文件：
```
/boot/efi/SYSTEM (空目录)

/boot/efi/EFI
/boot/efi/EFI/Boot
/boot/efi/EFI/Boot/fbx64.efi
/boot/efi/EFI/Boot/mmx64.efi
/boot/efi/EFI/Boot/bootx64.efi
...
/boot/efi/EFI/HP
/boot/efi/EFI/HP/dip.zip
/boot/efi/EFI/HP/BiosUpdate
/boot/efi/EFI/HP/BiosUpdate/BiosMgmt.efi
/boot/efi/EFI/HP/BiosUpdate/BiosMgmt.s12
/boot/efi/EFI/HP/BiosUpdate/BiosMgmt.s14
...
/boot/efi/EFI/HP/DEVFW
/boot/efi/EFI/HP/DEVFW/firmware.bin
/boot/efi/EFI/HP/DEVFW/Realtek.bin
/boot/efi/EFI/HP/DEVFW/ClickPad.bin
...
/boot/efi/EFI/Microsoft
/boot/efi/EFI/Microsoft/Boot
/boot/efi/EFI/Microsoft/Boot/BCD
/boot/efi/EFI/Microsoft/Boot/bootmgfw.efi
/boot/efi/EFI/Microsoft/Boot/bootmgr.efi
/boot/efi/EFI/Microsoft/Recovery
/boot/efi/EFI/Microsoft/Recovery/BCD
...
/boot/efi/EFI/ubuntu
/boot/efi/EFI/ubuntu/grubx64.efi
/boot/efi/EFI/ubuntu/shimx64.efi
/boot/efi/EFI/ubuntu/mmx64.efi
/boot/efi/EFI/ubuntu/BOOTX64.CSV
/boot/efi/EFI/ubuntu/grub.cfg

/boot/efi/System Volume Information (空目录)
```
通过 diff 比较，/boot/efi/EFI/Boot/bootx64.efi 与 /boot/efi/EFI/ubuntu/shimx64.efi 相同，看来是安装 ubuntu 时生成的。

通过 strings 查看，/boot/efi/EFI/ubuntu/shimx64.ef 中有 `\\grubx64.efi`，推测是转而加载 `/boot/efi/EFI/ubuntu/grubx64.efi`。

通过 strings 查看， /boot/efi/EFI/ubuntu/grubx64.efi 中有 `/EFI/ubuntu` 和 `%s/grub.cfg`，推测是写死了加载 `EFI/ubuntu/grub.cfg` 配置文件。

/boot/efi/EFI/ubuntu/grub.cfg 的全文如下：
 
```
search.fs_uuid e7889cac-3f57-4e3c-89c4-3b68ee1706de root 
set prefix=($root)'/boot/grub'
configfile $prefix/grub.cfg
```
EFI 启动项：
```
efibootmgr -v

BootCurrent: 0000
Timeout: 0 seconds
BootOrder: 0003,0000,0001,0002,0004
Boot0000* ubuntu	HD(1,GPT,2e4346d5-d26a-4d49-863b-0ab9c9c2a562,0x800,0x82000)/File(\EFI\ubuntu\shimx64.efi)
Boot0001* IPV4 Network - Realtek PCIe GBE Family Controller	PciRoot(0x0)/Pci(0x1,0x3)/Pci(0x0,0x0)/MAC(bc0ff367d246,0)/IPv4(0.0.0.00.0.0.0,0,0)N.....YM....R,Y.....ISPH
Boot0002* IPV6 Network - Realtek PCIe GBE Family Controller	PciRoot(0x0)/Pci(0x1,0x3)/Pci(0x0,0x0)/MAC(bc0ff367d246,0)/IPv6([::]:<->[::]:,0,0)N.....YM....R,Y.....ISPH
Boot0003 USB: 	PciRoot(0x0)/Pci(0x8,0x1)/Pci(0x0,0x3)N.....YM....R,Y.....ISPH
Boot0004* SAMSUNG MZVL41T0HBLB-00BH1-S6B7NJ0W261060	PciRoot(0x0)/Pci(0x2,0x4)/Pci(0x0,0x0)/NVMe(0x1,00-25-38-E2-31-42-75-0C)N.....YM....R,Y.....ISPH
```

## 定制 grub/efi

### 添加 windows efi 启动项
这是为了在 grub 损坏时，仍可以方便地启动 windows。不做这一步也可以，只要能在 BIOS 中选择 /EFI/Microsoft/Boot/bootmgfw.efi 来启动系统。
```
sudo efibootmgr --create --gpt --disk /dev/nvme0n1 --part 1 --label "Windows (custom efi entry)" --loader "\\EFI\\Microsoft\\Boot\\bootmgfw.efi"
```
```
sudo efibootmgr --bootorder 0003,0000,0005,0001,0002,0004
```
当前 EFI 启动项：
```
sudo efibootmgr -v
BootCurrent: 0000
Timeout: 0 seconds
BootOrder: 0003,0000,0005,0001,0002,0004
Boot0000* ubuntu	HD(1,GPT,2e4346d5-d26a-4d49-863b-0ab9c9c2a562,0x800,0x82000)/File(\EFI\ubuntu\shimx64.efi)
Boot0001* IPV4 Network - Realtek PCIe GBE Family Controller	PciRoot(0x0)/Pci(0x1,0x3)/Pci(0x0,0x0)/MAC(bc0ff367d246,0)/IPv4(0.0.0.00.0.0.0,0,0)N.....YM....R,Y.....ISPH
Boot0002* IPV6 Network - Realtek PCIe GBE Family Controller	PciRoot(0x0)/Pci(0x1,0x3)/Pci(0x0,0x0)/MAC(bc0ff367d246,0)/IPv6([::]:<->[::]:,0,0)N.....YM....R,Y.....ISPH
Boot0003 USB: 	PciRoot(0x0)/Pci(0x8,0x1)/Pci(0x0,0x3)N.....YM....R,Y.....ISPH
Boot0004* SAMSUNG MZVL41T0HBLB-00BH1-S6B7NJ0W261060	PciRoot(0x0)/Pci(0x2,0x4)/Pci(0x0,0x0)/NVMe(0x1,00-25-38-E2-31-42-75-0C)N.....YM....R,Y.....ISPH
Boot0005* Windows (custom efi entry)	HD(1,GPT,2e4346d5-d26a-4d49-863b-0ab9c9c2a562,0x800,0x82000)/File(\EFI\Microsoft\Boot\bootmgfw.efi)
```

### 定制 grub 镜像

```
cd ~
mkdir -p custom-generic-grub/

grub-mkimage -d /usr/lib/grub/x86_64-efi/ -O x86_64-efi --prefix /EFI/custom-generic-grub/ -o custom-generic-grub/custom-generic-grub.efi loadenv part_gpt part_msdos loopback btrfs fat ext2 ntfs ntfscomp iso9660 scsi normal configfile chain multiboot search search_label search_fs_uuid search_fs_file boot efi_gop efi_uga linux linuxefi echo cpio cat cpio hexdump ls date reboot minicmd video_bochs video_cirrus gfxterm font lzopio zstd

touch custom-generic-grub/grub.cfg
touch custom-generic-grub/root-ubuntu-1.grub.cfg
```

linuxefi 可能是新版 grub 引导 linux 必须的。

修改 custom-generic-grub/grub.cfg 如下：
```
set timeout=10
set default=0
insmod part_gpt
insmod ext2
insmod btrfs
insmod loopback
insmod efi_gop
insmod efi_uga
insmod font
insmod video_bochs
insmod video_cirrus

menuentry "jump ./root-ubuntu-1.grub.cfg" {
  configfile root-ubuntu-1.grub.cfg
}

menuentry "jump (0,7)/subvol/root-ubuntu-1/boot/grub/grub.cfg" {
  search.fs_uuid e7889cac-3f57-4e3c-89c4-3b68ee1706de root 
  set prefix=($root)'/subvol/root-ubuntu-1/boot/grub'
  configfile $prefix/grub.cfg
}

menuentry "jump (0,7)/boot/grub/grub.cfg" {
  search.fs_uuid e7889cac-3f57-4e3c-89c4-3b68ee1706de root 
  set prefix=($root)'/boot/grub'
  configfile $prefix/grub.cfg
}
```

修改 custom-generic-grub/root-ubuntu-1.grub.cfg 如下:
```
search --no-floppy --fs-uuid --set=root e7889cac-3f57-4e3c-89c4-3b68ee1706de
linux	/subvol/root-ubuntu-1/boot/vmlinuz-6.2.0-27-generic root=UUID=e7889cac-3f57-4e3c-89c4-3b68ee1706de rootflags=subvol=subvol/root-ubuntu-1 ro  quiet splash
initrd	/subvol/root-ubuntu-1/boot/initrd.img-6.2.0-27-generic
```

### 部署 custom-generic-grub 及其配置文件
```
cd ~
sudo rsync -a custom-generic-grub /boot/efi/EFI/
```

### 添加 custom-generic-grub efi 启动项
这是为了在 grub 损坏时，仍可以方便地启动 windows。不做这一步也可以，只要能在 BIOS 中选择 /EFI/Microsoft/Boot/bootmgfw.efi 来启动系统。
```
sudo efibootmgr --create --gpt --disk /dev/nvme0n1 --part 1 --label "custom-generic-grub" --loader "\\EFI\\custom-generic-grub\\custom-generic-grub.efi"
```
```
sudo efibootmgr --bootorder 0003,0006,0000,0005,0001,0002,0004
```
当前 EFI 启动项：
```
sudo efibootmgr -v
BootCurrent: 0000
Timeout: 0 seconds
BootOrder: 0003,0000,0005,0001,0002,0004
Boot0000* ubuntu	HD(1,GPT,2e4346d5-d26a-4d49-863b-0ab9c9c2a562,0x800,0x82000)/File(\EFI\ubuntu\shimx64.efi)
Boot0001* IPV4 Network - Realtek PCIe GBE Family Controller	PciRoot(0x0)/Pci(0x1,0x3)/Pci(0x0,0x0)/MAC(bc0ff367d246,0)/IPv4(0.0.0.00.0.0.0,0,0)N.....YM....R,Y.....ISPH
Boot0002* IPV6 Network - Realtek PCIe GBE Family Controller	PciRoot(0x0)/Pci(0x1,0x3)/Pci(0x0,0x0)/MAC(bc0ff367d246,0)/IPv6([::]:<->[::]:,0,0)N.....YM....R,Y.....ISPH
Boot0003 USB: 	PciRoot(0x0)/Pci(0x8,0x1)/Pci(0x0,0x3)N.....YM....R,Y.....ISPH
Boot0004* SAMSUNG MZVL41T0HBLB-00BH1-S6B7NJ0W261060	PciRoot(0x0)/Pci(0x2,0x4)/Pci(0x0,0x0)/NVMe(0x1,00-25-38-E2-31-42-75-0C)N.....YM....R,Y.....ISPH
Boot0005* Windows (custom efi entry)	HD(1,GPT,2e4346d5-d26a-4d49-863b-0ab9c9c2a562,0x800,0x82000)/File(\EFI\Microsoft\Boot\bootmgfw.efi)
```

## btrfs 重构
### 快照根目录

```
sudo btrfs subvolume snapshot -r / /@=2023-08-12=1
```

### 子卷重构
```
cd /

sudo mkdir subvol snapshot
sudo btrfs subvolume create subvol/root-ubuntu-1
sudo btrfs subvolume create subvol/home
sudo cp -a --reflink=always ~ subvol/home/

ls
sudo cp -a --reflink=alway -x boot etc lib lib64 media opt root sbin var bin lib32 libx32 mnt snap srv  usr /subvol/root-ubuntu-1/

cd /subvol/root-ubuntu-1/
sudo mkdir -p dev proc run sys home
```

```
sudo btrfs quota enable /

sudo btrfs qgroup show /

Qgroupid    Referenced    Exclusive   Path 
--------    ----------    ---------   ---- 
0/5           16.99GiB     70.33MiB   <toplevel>
0/256         16.98GiB     71.92MiB   @=2023-08-12=1
0/257         11.21GiB    166.87MiB   subvol/root-ubuntu-1
0/258          5.71GiB     48.36MiB   subvol/home
```

### 子卷备份

```
sudo btrfs subvolume snapshot -r /media/top/subvol/root-ubuntu-1 /media/top/snapshot/root-ubuntu-1=2023-10-08=1
sudo btrfs subvolume snapshot -r /media/top/subvol/home /media/top/snapshot/home=2023-10-08=1
```

## fstab

```
cp /etc/fstab ~/custom-generic-grub/fstab.root-ubuntu-1
```

custom-generic-grub/fstab.root-ubuntu-1 内容如下：

```
LABEL=sys1 /     btrfs rw,relatime,compress=zlib,ssd,commit=120,subvol=subvol/root-ubuntu-1 0 1
LABEL=sys1 /home btrfs rw,relatime,compress=zlib,ssd,commit=120,subvol=subvol/home          0 1

# /media/windows was on /dev/nvme0n1p3 during curtin installation
/dev/disk/by-uuid/96306C94306C7D5D /media/windows ntfs defaults 0 1
# /boot/efi was on /dev/nvme0n1p1 during curtin installation
/dev/disk/by-uuid/08E0-C710 /boot/efi vfat defaults 0 1
# /media/sys0 was on /dev/nvme0n1p6 during curtin installation
/dev/disk/by-uuid/f79119c6-a687-4ae2-9980-11499b349c8d /media/sys0 ext4 defaults 0 1
```
注意：去掉了 `space_cache` 选项，因为 btrfs 已经默认采用 `space_cache=v2` 选项。`space_cache` 等效于 `space_cache=v1`，无法挂载 `space_cache=v2` 的分区。
部署 fstab:
```
sudo cp custom-generic-grub/fstab.root-ubuntu-1 /subvol/root-ubuntu-1/etc/fstab
```

## 后处理
### 重启进入新的文件系统布局

确认文件系统挂载：
```
/dev/nvme0n1p7 on / type btrfs (rw,relatime,compress=zlib:3,ssd,discard=async,space_cache=v2,commit=120,subvolid=257,subvol=/subvol/root-ubuntu-1)
/dev/nvme0n1p7 on /var/snap/firefox/common/host-hunspell type btrfs (ro,noexec,noatime,compress=zlib:3,ssd,discard=async,space_cache=v2,commit=120,subvolid=257,subvol=/subvol/root-ubuntu-1)
/dev/nvme0n1p7 on /home type btrfs (rw,relatime,compress=zlib:3,ssd,discard=async,space_cache=v2,commit=120,subvolid=258,subvol=/subvol/home)
```

### 修复 grub
修复 grub 包括 grub-install 和  update-grub 两步。

```
sudo LC_ALL=C grub-install --debug

grub-install: info: grub-mkimage --directory '/usr/lib/grub/x86_64-efi' --prefix '/subvol/root-ubuntu-1/boot/grub' --output '/boot/grub/x86_64-efi/core.efi'  --dtb '' --sbat '' --format 'x86_64-efi' --compression 'auto'   --config '/boot/grub/x86_64-efi/load.cfg' 'btrfs' 'part_gpt' 'search_fs_uuid'

grub-install: info: reading /usr/lib/grub/x86_64-efi/zstd.mod.
grub-install: info: reading /usr/lib/grub/x86_64-efi/diskfilter.mod.
grub-install: info: reading /usr/lib/grub/x86_64-efi/raid6rec.mod.
grub-install: info: reading /usr/lib/grub/x86_64-efi/crypto.mod.
grub-install: info: reading /usr/lib/grub/x86_64-efi/lzopio.mod.
grub-install: info: reading /usr/lib/grub/x86_64-efi/gcry_crc.mod.
grub-install: info: reading /usr/lib/grub/x86_64-efi/gzio.mod.
grub-install: info: reading /usr/lib/grub/x86_64-efi/btrfs.mod.
grub-install: info: reading /usr/lib/grub/x86_64-efi/part_gpt.mod.
grub-install: info: reading /usr/lib/grub/x86_64-efi/search_fs_uuid.mod.
grub-install: info: reading /boot/grub/x86_64-efi/load.cfg.

grub-install: info: grub-mkimage --directory '/usr/lib/grub/x86_64-efi' --prefix '' --output '/boot/grub/x86_64-efi/grub.efi'  --dtb '' --sbat '' --format 'x86_64-efi' --compression 'auto'   --config '/boot/grub/x86_64-efi/load.cfg' 'btrfs' 'part_gpt' 'search_fs_uuid'

grub-install: info: copying `/usr/lib/grub/x86_64-efi-signed/grubx64.efi.signed' -> `/boot/efi/EFI/ubuntu/grubx64.efi'.
grub-install: info: copying `/usr/lib/shim/shimx64.efi.signed' -> `/boot/efi/EFI/ubuntu/shimx64.efi'.
grub-install: info: copying `/usr/lib/shim/mmx64.efi' -> `/boot/efi/EFI/ubuntu/mmx64.efi'.
grub-install: info: copying `/usr/lib/shim/BOOTX64.CSV' -> `/boot/efi/EFI/ubuntu/BOOTX64.CSV'.
grub-install: info: copying `/boot/grub/x86_64-efi/load.cfg' -> `/boot/efi/EFI/ubuntu/grub.cfg'.
grub-install: info: copying `/usr/lib/shim/shimx64.efi.signed' -> `/boot/efi/EFI/Boot/bootx64.efi'.
grub-install: info: copying `/usr/lib/shim/fbx64.efi' -> `/boot/efi/EFI/Boot/fbx64.efi'.
grub-install: info: copying `/usr/lib/shim/mmx64.efi' -> `/boot/efi/EFI/Boot/mmx64.efi'.
grub-install: info: Registering with EFI: distributor = `ubuntu', path = `\EFI\ubuntu\shimx64.efi', ESP at hostdisk//dev/nvme0n1,gpt1.
grub-install: info: executing modprobe efivars 2>/dev/null.
grub-install: info: setting EFI variable Boot0000.
grub-install: info: setting EFI variable BootOrder.
Installation finished. No error reported.
```

这修改了 /boot/efi/EFI/ubuntu/grub.cfg ：
```
search.fs_uuid e7889cac-3f57-4e3c-89c4-3b68ee1706de root 
set prefix=($root)'/subvol/root-ubuntu-1/boot/grub'
configfile $prefix/grub.cfg
```
再执行 update-grub：
```
sudo update-grub

Sourcing file `/etc/default/grub'
Sourcing file `/etc/default/grub.d/init-select.cfg'
Generating grub configuration file ...
Found linux image: /boot/vmlinuz-6.2.0-27-generic
Found initrd image: /boot/initrd.img-6.2.0-27-generic
Found linux image: /boot/vmlinuz-6.2.0-26-generic
Found initrd image: /boot/initrd.img-6.2.0-26-generic
Found memtest86+ 64bit EFI image: /subvol/root-ubuntu-1/boot/memtest86+x64.efi
Warning: os-prober will be executed to detect other bootable partitions.
Its output will be used to detect bootable binaries on them and create new boot entries.
Found Windows Boot Manager on /dev/nvme0n1p1@/EFI/Microsoft/Boot/bootmgfw.efi
Adding boot menu entry for UEFI Firmware Settings ..
```

这修改了 /boot/grub/grub.cfg：
```
	linux	/subvol/root-ubuntu-1/boot/vmlinuz-6.2.0-27-generic root=UUID=e7889cac-3f57-4e3c-89c4-3b68ee1706de ro rootflags=subvol=subvol/root-ubuntu-1  quiet splash $vt_handoff
	initrd	/subvol/root-ubuntu-1/boot/initrd.img-6.2.0-27-generic
```

当前 EFI 启动项：
```
efibootmgr -v
BootCurrent: 0006
Timeout: 0 seconds
BootOrder: 0000,0003,0006,0001,0002
Boot0000* ubuntu	HD(1,GPT,2e4346d5-d26a-4d49-863b-0ab9c9c2a562,0x800,0x82000)/File(\EFI\ubuntu\shimx64.efi)
Boot0001* IPV4 Network - Realtek PCIe GBE Family Controller	PciRoot(0x0)/Pci(0x1,0x3)/Pci(0x0,0x0)/MAC(bc0ff367d246,0)/IPv4(0.0.0.00.0.0.0,0,0)N.....YM....R,Y.....ISPH
Boot0002* IPV6 Network - Realtek PCIe GBE Family Controller	PciRoot(0x0)/Pci(0x1,0x3)/Pci(0x0,0x0)/MAC(bc0ff367d246,0)/IPv6([::]:<->[::]:,0,0)N.....YM....R,Y.....ISPH
Boot0003  USB:  	PciRoot(0x0)/Pci(0x8,0x1)/Pci(0x0,0x3)N.....YM....R,Y.....ISPH
Boot0006* custom-generic-grub	HD(1,GPT,2e4346d5-d26a-4d49-863b-0ab9c9c2a562,0x800,0x82000)/File(\EFI\custom-generic-grub\custom-generic-grub.efi)....ISPH
```

## docker 子卷配置
建立子卷 docker 用于 /var/lib/docker，docker-vol 用于 /var/lib/docker/volumes
```
cd /media/top/subvol
sudo btrfs subvolume create ./docker
sudo btrfs subvolume create ./docker-vol

sudo mkdir -p ./docker/volumes
sudo mkdir -p /var/lib/docker
```

fstab：

```
LABEL=sys1 /                         btrfs rw,relatime,compress=zlib,ssd,commit=120,subvol=subvol/root-ubuntu-1         0 1
LABEL=sys1 /home                     btrfs rw,relatime,compress=zlib,ssd,commit=120,subvol=subvol/home                  0 1
LABEL=sys1 /var/lib/docker           btrfs rw,relatime,compress=zlib,ssd,commit=120,subvol=subvol/docker                0 1
LABEL=sys1 /var/lib/docker/volumes   btrfs rw,relatime,compress=zlib,ssd,commit=120,subvol=subvol/docker-vol            0 1
```

```
cp ~/fstab /etc/fstab
systemctl daemon-reload
sudo mount /var/lib/docker
sudo mount /var/lib/docker/volumes
```
## 双机复制

```
cp -a --reflink=always ~/backup_c/doc.personal ~/

rm -Rf ~/.mozilla
cp -a --reflink=always ~/backup_c/.mozilla ~/

rm -Rf ~/.vscode && cp -a --reflink=always ~/backup_c/.vscode ~/

rsync -avhx --progress --delete-after --exclude={'project/','ai-dataset/','opt/','software/','annotation/','.cache/','Documents/','.fiftyone/','ai-dataset-e/','.deepinwine/'} duanyao@duanyao-laptop-c.local:/home/duanyao/ ~/backup_c/

rsync -avhx --progress --delete-after --files-from=$HOME/rsync_list.txt duanyao@duanyao-laptop-c.local:/home/duanyao/project/ ~/project/

rsync -avhx --progress --delete-after --exclude={'annotation/','openvino.git/','openvino_model_zoo.git/','data/','ai-parents-samples/','Paddle2ONNX.git/','paddle2openvino.git/'} duanyao@duanyao-laptop-c.local:/home/duanyao/project/ ~/project/

for src in .sdkman 
#.vineyard .vnc .vscode-server passwd-ossfs .oss-browser .nvm .npm .minikube .m2 .kube .bito .aws work-gt work-fuxin Videos Pictures Music Downloads media fiftyone etc bin ai-model ai-demo ai-article 负面动作视频  
# .ssh .pip .docker .vscode .mozilla
do
cp -a --reflink=always ~/backup_c/$src ~/
done

cp -a --reflink=always ~/backup_c/.git* ~/

cp -a --reflink=always ~/backup_c/.oss* ~/

for src in .config
do
cp -a --reflink=always ~/$src ~/$src.bk
cp -a --reflink=always ~/backup_c/$src ~/
done
```

## 修改 .profile 和 .bashrc
.profile 增加：
```
#SSH_ASKPASS=/usr/bin/ssh-askpass
#keychain id_dsa id_ed25519
#source $HOME/.keychain/$HOSTNAME-sh

# 去除deepin（15.11）系统中Firefox上部的白色标题栏 https://bbs.deepin.org/forum.php?mod=viewthread&tid=181716&extra=
#MOZ_GTK_TITLEBAR_DECORATION=client; export MOZ_GTK_TITLEBAR_DECORATION

#echo "~/.profile, PATH=$PATH , SHELL=$SHELL, BASH_VERSION=$BASH_VERSION" >> $HOME/bash_init.log

# 10.96.0.0/12,192.168.59.0/24,192.168.49.0/24,192.168.39.0/24 是 minikube 使用的网段，必须加入 NO_PROXY，不通过代理。但此处我们让它更广泛一些。
#export NO_PROXY=localhost,127.0.0.1,10.96.0.0/12,192.168.59.0/24,192.168.49.0/24,192.168.39.0/24
export NO_PROXY=localhost,127.0.0.1,10.0.0.0/8,172.16.0.0/12,192.168.0.0/16
```

.bashrc 增加：
```
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

echo "~/.bashrc(e), PATH=$PATH" >> $HOME/bash_init.log

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/home/duanyao/.sdkman"
[[ -s "/home/duanyao/.sdkman/bin/sdkman-init.sh" ]] && source "/home/duanyao/.sdkman/bin/sdkman-init.sh"
```

## 安装软件

sudo apt install manpages-dev  git git-gui build-essential automake libtool cmake gdb golang-go meld traceroute strace moreutils time dh-make

sudo apt install kate ffmpeg vlc totem autossh iptux tcpdump wireshark curl tmux avahi-daemon atop lsof psensor dconf-editor cgroup-tools earlyoom  cpu-x cpufrequtils timidity  powertop cpufrequtils

cpu-g CopyQ comix

## ufw/gufw 防火墙
详见 software.md 。

