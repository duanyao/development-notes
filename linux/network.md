## 参考资料
[1.0] ip命令
[1.1] 试试Linux下的ip命令，ifconfig已经过时了 
https://linux.cn/article-3144-1.html

[1.1] How to Use IP Command in Linux with Examples
https://phoenixnap.com/kb/linux-ip-command-examples

[2.0] 物理网卡
[2.1] HowTo: Linux Show List Of Network Cards
https://www.cyberciti.biz/faq/linux-list-network-cards-command/

[3.0] hostname
[3.1] How to Change the Hostname of a Linux System
http://www.ducea.com/2006/08/07/how-to-change-the-hostname-of-a-linux-system/

[3.2] linux的hostname修改详解
http://soft.chinabyte.com/os/281/11563281.shtml

[4.1] UDP and TCP ports: A list of the most important ports
https://www.ionos.com/digitalguide/server/know-how/tcp-ports-and-udp-ports/

[4] /etc/network/interfaces
http://manpages.ubuntu.com/manpages/precise/man5/interfaces.5.html

[5] What is the difference between 'ifconfig up eth0' and 'ifup eth0'? 
https://access.redhat.com/solutions/27166

[6] ip link set not assigning IP Address but ifup does
https://serverfault.com/questions/603906/ip-link-set-not-assigning-ip-address-but-ifup-does

[7] Android Traceroute 功能实现
http://www.cnblogs.com/punkisnotdead/p/4233978.html

[8] Linux Ethernet Bonding Driver HOWTO
https://www.kernel.org/doc/Documentation/networking/bonding.txt

[9] 11.2.4.2. Create a Channel Bonding Interface
https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Deployment_Guide/sec-Create_a_Channel_Bonding_Interface.html

[10] how to "bond" two Ethernet connections together to create an auto failover interface. 
https://www.tecmint.com/network-nic-bonding-teaming-in-debian-linux/

[11] Controlling Hardware with ioctls
http://www.linuxjournal.com/node/6908/print

[12] Can't ifdown eth0 (main interface)
https://unix.stackexchange.com/questions/50602/cant-ifdown-eth0-main-interface

[13] dhcpcd.conf — dhcpcd configuration file
http://manpages.ubuntu.com/manpages/wily/man5/dhcpcd.conf.5.html

[14] https://linux.die.net/man/8/avahi-autoipd

[15] https://github.com/lathiat/avahi/tree/master/avahi-autoipd

[16] https://bugs.alpinelinux.org/issues/7988

[17] https://superuser.com/questions/904903/cant-add-default-route-rtnetlink-answers-network-is-unreachable

[18] https://wiki.debian.org/NetworkConfiguration#Multiple_IP_addresses_on_one_Interface

[19] linux查看已经连接的wifi的密码 https://blog.csdn.net/zhang__liuchen/article/details/81260491

[20] How to get the hostname from a DHCP server https://askubuntu.com/questions/104918/how-to-get-the-hostname-from-a-dhcp-server

[21] hosts file seems to be ignored
https://serverfault.com/questions/121890/hosts-file-seems-to-be-ignored

## ip 命令
### ip addr
  显示接口的地址。例子：

1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP qlen 1000
    link/ether 08:00:27:63:a5:f1 brd ff:ff:ff:ff:ff:ff
    inet 172.16.9.39/24 brd 172.16.9.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe63:a5f1/64 scope link 
       valid_lft forever preferred_lft forever

ip addr show <nic>

3: wlan0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 74:e5:0b:e5:57:f6 brd ff:ff:ff:ff:ff:ff
    inet 192.168.100.143/24 brd 192.168.100.255 scope global wlan0
       valid_lft forever preferred_lft forever
    inet6 fe80::76e5:bff:fee5:57f6/64 scope link 
       valid_lft forever preferred_lft forever

### ip addr add/del
sudo ip addr add 192.168.0.193/24 dev wlan0
sudo ip addr del 192.168.0.193/24 dev wlan0

ip addr add dev eth0 scope link fe80::xxxx:xxff:fexx:xxxx

### ip route
  显示路由，包括默认网关。例子：

default via 172.16.9.1 dev eth0  src 172.16.9.39  metric 202 
172.16.9.0/24 dev eth0  src 172.16.9.39  metric 202

假设现在你有一个IP地址，你需要知道路由包从哪里来。可以使用下面的路由选项（译注：列出了路由所使用的接口等）：

ip route get 10.42.0.47
10.42.0.47 via 192.168.100.1 dev wlan0  src 192.168.100.143

如果没有网口可达，则返回错误
ip route get 10.42.0.47
RTNETLINK answers: Network is unreachable

### 删除路由
ip route del 172.16.9.0/24
ip route del default

注意如果 del 的参数匹配多项，则需要执行多次才能全部删除。

### 添加路由
ip route add 192.0.2.0/24 via 10.0.0.1
ip route add default via 10.0.0.1
ip route add default via 10.0.0.1 dev eth0

### 修改路由
```
ip route
default via 192.168.8.1 dev enp5s0 proto static metric 100
...

sudo ip route replace default via 192.168.8.3 dev enp5s0 proto static metric 100

ip route
default via 192.168.8.3 dev enp5s0 proto static metric 100
```

注意 ip route replace 后面应该照抄 ip route 显示的字段，除了修改的部分，不要省略任何字段，否则就不是修改原有的路由条目，而是创建一条新的路由条目了，等效于 add。

## 端口（port）

端口的总量是 64K 。服务器用客户端的 IP+port 来区分不同的连接，所以端口的总量会限制来自同一客户端 IP 的连接数。一般来说这是足够的，但如果有很多客户机位于同一个 NAT 后面（IP都相同），端口可能会不够用。

ICANN/IANA 分配的，以及其它广为人知的端口见 [4.1]。
49152 和以上为动态端口(dynamic port)，这个范围不会被 ICANN/IANA 分配，可以由应用程序随意使用，一般是作为 TCP/UDP 的本地端口。所以这个范围不应被用作服务的监听端口 [4.1]。
但操作系统可以自行设定动态端口的范围。

## 将 linux 配置为路由器（router）
详见 iptables。

## ping 命令
-t 参数指定 ttl。如果从小（1）到大逐渐增加ttl，则可以看到路由上的节点，等效于 traceroute。例如：
 ping -c 1 -t 1 61.135.169.121
不过这个方法似乎不如 traceroute 可靠。

## 激活和停止网络接口
ip link set eth0 down
ip link set eth0 up

ifconfig up eth0
ifconfig down eth0

ifup eth0
ifdown eth0

注意，"ip link set" 和 ifconfig 是低层命令，不会根据配置文件去设置IP地址、默认网关等。
要利用配置文件（/etc/network/interfaces，或者类似的网络配置文件），应使用 ifup/ifdown 命令[5,6]。

## 查看网络接口的属性 /sys
* /sys/class/net/eth0/speed 单位 M
* /sys/class/net/eth0/address  mac 地址
* /sys/class/net/eth0/operstate
  up|down
* /sys/class/net/eth0/statistics 统计信息

## 查看网络接口的属性 ethtool
ethtool eth0

ethtool -s enp4s0 speed 1000 duplex half autoneg off

## 查看网络接口的属性-C函数
[11]
edata.cmd = ETHTOOL_GLINK;
strncpy(ifr.ifr_name, ifname, sizeof(ifr.ifr_name)-1);
ifr.ifr_data = (char *) &edata;
if (ioctl(skfd, SIOCETHTOOL, &ifr) == -1) {
    printf("ETHTOOL_GLINK failed: %s\n", strerror(errno));
    return 2;
}
return (edata.data ? 0 : 1);

## 将网卡名字改为 eth0

较新的 linux 系统采用了 "Predictable Network Interface Names"， 这样网卡名就变成了形如 enp0s3 的样子。
但是对于虚拟机来说，这反而不可预测了，所以需要改为传统的 ethN 的形式。参考：

https://www.freedesktop.org/wiki/Software/systemd/PredictableNetworkInterfaceNames/
https://unix.stackexchange.com/questions/81834/how-can-i-change-the-default-ens33-network-device-to-old-eth0-on-fedora-19
Linux:How to change a network interface name on CentOS 7
http://www.tuicool.com/articles/yMn2ii6

修改 `/etc/default/grub`，增加 `net.ifnames=0 biosdevname=0` 参数：

```
GRUB_CMDLINE_LINUX=”rd.lvm.lv=vg0/swap vconsole.keymap=us crashkernel=auto vconsole.font=latarcyrheb-sun16 net.ifnames=0 biosdevname=0 rd.lvm.lv=vg0/usr rhgb quiet”
```

重新生成启动菜单：

```
grub2-mkconfig -o /boot/grub2/grub.cfg
```

对 `ifcfg-*` 文件改名：

```
mv /etc/sysconfig/network-scripts/ifcfg-enp0s3 /etc/sysconfig/network-scripts/ifcfg-eth0
```
修改 `ifcfg-eth0`，将里面的 enp0s3 改为 eth0

## DHCP
### busybox udhcpc
主要问题：
  * 无法响应网线热插拔。可以通过安装 ifplugd 来弥补。
  * ifdown/ifup 之后，ipv6 地址丢失（如果DHCP服务器没有分配ipv6地址的话）。
  
### dhcpcd
没有 udhcpc 的问题。

dhcpcd 在不能通过 dhcp 获得IPv4地址的情况下，会产生一个IPv4 link local 地址（169.254.*）。
要停止这种行为，可以用 noipv4ll 选项[13]。

## 自动产生 IPv4 link local 地址
这对零配置网络（zeroconf）是有用的，因为 linux 尚不完全支持 mDNS + IPv6 link local 地址。
产生 ipv4ll 地址的算法，一般是随机产生IP地址，然后用 ARP 查询地址是否被占用，如果占用则继续随机，选定后则通过 ARP 广播出去。

ipv4ll 除了可以用 dhcpcd 之类的 DHCP 客户端来设置，还可以用专门的程序来做，如 avahi-autoipd [14]。
在 debian 等发行版中，avahi-autoipd 包需要单独安装，不是 avahi 包的一部分；alpine 没有这个包，可能需要自行编译 [15]。

如果在 /etc/netmask/interfaces 中使用 ipv4ll 方法，则 ifup 程序会使用 avahi-autoipd 程序来产生 ipv4ll 地址。

可以给一个网口同时分配普通 IPv4 地址和 IPv4LL 地址。
但应该注意 mDNS 查询时，avahi 只会返回一个 IPv4 地址，选择该地址的算法比较复杂，通常是网口上最后一个设置的地址，也就是 ip addr 显示的最后一个地址，但有时也会暂时返回前面的地址。
NetworkManager 中，如果给一个网口同时分配普通 IPv4 地址和 IPv4LL 地址，则普通地址总是位于最后。

## hostname
hostname 对一台机器是唯一的，它是操作系统内核的一个运行时属性，并不依赖于网络。

从 linux/posix 内核的角度来说，它自己无法得知本机的 hostname，而是靠系统调用
  int sethostname(const char *name, size_t len);
让用户态程序来设置 hostname。
设置之后，可以用
  int gethostname(char *name, size_t len);
以及
  int uname(struct utsname *buf);
来查询 hostname。

此外，hostname 也通过 /proc/sys/kernel/hostname 文件暴露出来（这是linux特有的），可以用 sysctl kernel.hostname 来查询和设置[2]。

命令行工具 hostname 可以用来查询设置 hostname。

根据不同的linux发行版，采用不同的配置文件来设定持久的 hostname，例如[2]：
  /etc/hostname Debian, alpine linux 等
  /etc/sysconfig/network Redhat 等

/etc/hosts 中也可以加入主机名，以便从本机访问：
  127.0.0.1 foo localhost localhost.localdomain

有的发行版可能根据 /etc/hosts 反推和设置 hostname，如redhat[3]。

操作系统也可以根据 DHCP 信息来更新自己的 hostname 。不过，并非所有的 DHCP 客户端都支持这个特性[20]。
一些云服务器服务商可能运用这个技术来支持从外部修改云服务器的主机名。

## 域名/主机名解析
[21]
相关配置文件：
/etc/nsswitch.conf ：影响域名解析的方法及其顺序，方法包括 /etc/hosts, DNS, mDNS 等。
/etc/hosts ：静态域名解析表。
/etc/resolv.conf ：DNS 服务器地址。

相关工具：
dig, nslookup, host：仅考虑标准 DNS 服务器，不考虑 /etc/hosts 和 mDNS 。
`getent hosts <hostname>` 或 `getent hosts <hostname>` ：根据 nsswitch.conf 指定的方法。
libc `gethostbyname*()` ：根据 nsswitch.conf 指定的方法。
ping ：根据 nsswitch.conf 指定的方法。

## ifdown/ifup 和 /etc/network/interfaces
ifdown/ifup 命令根据 /etc/network/interfaces 配置网络。
它们是 debian 系统的组件（包：ifupdown），但也被一些其它发行版采用，如 alpine linux。

注意他们与 ifconfig up|down 等可能修改网口的命令不应该同时使用，否则会有冲突。

ifdown/ifup 使用 /run/ifstate 或者 /run/network/ifstate 文件来记录网口的状态。
如果使用别的工具修改网口设置，则 /run/ifstate 可能处于不一致的状态，运行 ifdown eth0 可能出错：
  ifdown: interface eth0 not configured
这时，可以用 ifdown -f eth0 和 ifup -f eth0 来强制执行（ifup -f 修复/run/ifstate）。


/etc/network/interfaces 可参考 [4] 或者 man interfaces。


```
auto lo
iface lo inet loopback

auto eth0

#动态
iface eth0 inet dhcp

#动态 dhcp6。但实际上可能不需要设置，如果设置了 ipv4的dhcp，则 dhcpcd 会自动处理ipv6。
iface eth0 inet6 dhcp

#自动 ipv6。auto 指令在 Alpine Linux 上不被支持。
iface eth0 inet6 auto

# 静态
iface eth0 inet static
        address 192.0.2.7
        netmask 255.255.255.0
        gateway 192.0.2.254

# 通过多个 iface 段落，可以为一个网口添加多个 IP 地址。不过，只允许其中一个有 gateway 指令[18]。
# 多个 ip 地址的添加顺序就是其出现顺序。
iface eth0 inet static
    address 192.168.1.43
    netmask 255.255.255.0

# 静态 ipv6
iface eth0 inet6 static
    address 2001:db8::c0ca:1eaf
    netmask 64
    gateway 2001:db8::1ead:ed:beef
```

如果配置了 iface xxx inet dhcp，则 ifup 运行时会启动 dhcp 客户端程序（如果还没有启动），而 ifdown 会关闭 dhcp 客户端。
它可使用的客户端有多个，会按以下顺序尝试：dhclient, pump, udhcpc, dhcpcd（但 alpine 上，dhcpcd 的优先级似乎高于 udhcpc?）。也可以用 client 选项直接指定一种客户端。

要修改 /etc/network/interfaces，正确的顺序是 ifdown 相关网口、修改 /etc/network/interfaces、ifup 相关网口。
如果按修改 /etc/network/interfaces、ifdown 相关网口、ifup 相关网口的顺序操作，则可能引起问题。例如：
* 将一个静态 IP 地址变更为另一个，则可能导致旧的 IP 或路由设置残存在网口上。
* 将 dhcp 变更为静态，则不会自动关闭 dhcp 客户端程序，可能导致其与静态 IP 配置冲突。
  例如，如果 interfaces 中配置了静态 ip，但 dhcpcd 还在运行，则可能与 ifup 产生冲突，导致静态 IP 被 dhcpcd 产生的 link local 地址覆盖掉。
  这种冲突发生多次后还可导致 dhcpcd 进程崩溃（在 alpine linux 中观察到）。


如果配置的默认网关或者路由的下一跳地址与网口的IP不在一个子网，则 ifup 可能报错[17]
  RTNETLINK answers: Network is unreachable
退出码是1。不过，设置仍然会生效。

不需要在 /etc/network/interfaces 中设置 IPv6 link local 地址，内核和ifup会自动设置一个。

## 无线网络

### 打开关闭无线网卡

使用 NetworkManager ：

```
nmcli radio wifi off
nmcli radio wifi on
```

使用 rfkill:

```
rfkill block <device number>
```

显示无线网络的打开、关闭状态：

```
rfkill list
```

### 显示搜索到的AP信号及其强度

NetworkManager
```
nmcli d wifi
```

图形界面程序有 LinSSID 。

### nm 存储的连接设置（包括密码）

在 /etc/NetworkManager/system-connections 下，配置文件名字是连接名。

配置文件中，wifi的密码是psk：

```
[wifi-security]
key-mgmt=wpa-psk
psk=...
```

## NAT
### 查看本机的外网 IP
使用外部网站反馈：
```
curl ident.me
curl ifconfig.me
```
使用 miniupnpc ：
```
# debian/ubuntu setup: 
# sudo apt-get install miniupnpc

# get WAN IP address from UPNP router:
upnpc -s | grep ^ExternalIPAddress | cut -c21-
```
前提是路由器打开了 UPNP 功能。华为移动路由Pro 似乎不支持 UPNP 。

## ARP
arp -a 或 arp -e 显示条目
arp -d <IP or host> 删除条目
tcpdump \( arp or icmp \) 捕获 arp 包

Linux 上可以用下面的方法打开/关闭 arp 回答（默认是打开的）：

ip link set dev eth0 arp off
ip link set dev eth0 arp on
ifconfig eth0 -arp
ifconfig eth0 arp

Linux 上 arp 询问、回答一般是由内核负责的。
Linux 收到位于同一子网内的 IP 地址的 arp 询问一般会回答，不在同一子网的 arp 则不一定回答（似乎有时关闭再打开网口对此也有影响），ping、TCP 也有类似的情况。
所以，即使两台机器在同一以太网内，还应该确保其有同一子网的IP地址（不仅仅是路由！），才能保证互相访问。

## 链路聚合
[8-10]
Linux 可以将多个网口聚合（bind）为一个虚拟网口，可以实现增加带宽、负载均衡或热备份等功能。
目前电缆型以太网的最大速度是1G，要突破这个限制，链路聚合是个可行的办法。虽然有10G的以太网，但是需要光纤，成本较高。

## NetworkManager

### 图形前端

network-manager-gnome : 网络连接管理，包括 nm-applet （托盘区小程序）、nm-connection-editor （网络连接编辑器）
nm-tray: LXQt 的nm前端。

## connman
类似 NetworkManager？
