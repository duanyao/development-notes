Init System

## 参考资料
[1] why systemd
http://blog.jorgenschaefer.de/2014/07/why-systemd.html

[2] systemd 为什么会有那么大的争议？
https://www.zhihu.com/question/25873473

[3] Comparison of init systems
https://wiki.gentoo.org/wiki/Comparison_of_init_systems

[4] OpenRC
https://wiki.gentoo.org/wiki/OpenRC

[5] Handbook:AMD64/Working/Initscripts (OpenRC)
https://wiki.gentoo.org/wiki/Handbook:AMD64/Working/Initscripts

[6] openrc guide
https://github.com/throwawaygh/arch-openrc-guide

[7] Writing Init Scripts
https://wiki.alpinelinux.org/wiki/Writing_Init_Scripts

[8] start-stop-daemon
http://man7.org/linux/man-pages/man8/start-stop-daemon.8.html

[9] env(1) - Linux man page
https://linux.die.net/man/1/env

[10] Re: start-stop-daemon: how to pass environment variables to the daemon
http://linux.derkeiler.com/Mailing-Lists/Debian/2007-07/msg01555.html

[11] How can I log the stdout of a process started by start-stop-daemon?
https://stackoverflow.com/questions/8251933/how-can-i-log-the-stdout-of-a-process-started-by-start-stop-daemon

[12] --stdout and --stderr to support redirection to syslog
https://github.com/OpenRC/openrc/issues/118

[13] Build a network of processes and connecting pipes - and have them act like a single process.
https://github.com/flonatel/pipexec

[14] start-stop-daemon: add the ability to pipe stdout and/or stderr to a process rather than a file
https://github.com/OpenRC/openrc/issues/127

[15] openrc man/supervise-daemon
https://github.com/OpenRC/openrc/blob/master/man/supervise-daemon.8

[16] Supervise-daemon does not give up restarting a daemon if the daemon fails to start
https://github.com/OpenRC/openrc/issues/126

[17] Using supervise-daemon
https://github.com/OpenRC/openrc/blob/master/supervise-daemon-guide.md

[18] supervision-inspired or supervision-alike frameworks compared
https://bitbucket.org/avery_payne/supervision-scripts/wiki/Comparison

[19] Supervisor: A Process Control System
http://supervisord.org/

[20] Process-Supervision
https://wiki.gentoo.org/wiki/Process-Supervision

[21] S-6 Why another supervision suite ? 
http://www.skarnet.org/software/s6/why.html


## Init System 概述
When a computer starts, some kind of built-in firmware (in PCs, this is the BIOS or UEFI) takes over control, initializes the system, and loads a boot loader from a storage device (usually a hard drive). This boot loader is a bit more elaborate and loads a kernel from a storage device. The kernel then loads drivers, initializes hardware access, and starts one very specific program: The init process. As it is the first process to be run by the kernel, it gets the process ID (PID) 1 [1].

At least, it involves starting, stopping and restarting services. But on modern systems, this should be done in parallel, not just sequentially. Then, these services might need to be run in specialized environments, for example as a different user than root, using certain user limits (ulimit), with a specified CPU affinity, in a chroot or as a cgroup. [1]

Then, the init system should allow the status of a service to be looked at, to see if it is running. Ideally, it would not just allow an admin to manually look at the service, but also monitor it by itself and possibly restart the service if and when it fails. [1]

Finally, and often forgotten, the kernel can send certain events to the user land that can be responded to. For example, on PCs, Linux intercepts the ctrl-alt-del key combo and sends an appropriate signal to which init can respond by e.g. shutting down the system. [1]

## supervision
supervision 是某些 init system 的一种功能，或者可以接入另外的 supervisor 来实现的功能，即监视服务的运行情况，在服务意外退出时重启服务。
systemd 自带 supervision 功能，其它的 supervisor 有 s6, runit,  daemontools, Supervisord[19] 等。[18]做了一个比较表格。
[20] 描述了 supervisor 的设计原则。

不少 supervisor 还与日志系统结合在一起，通常是捕获服务进程的标准输出，写入日志文件。

## OpenRC
显示所有服务的状态
rc-status [run level]

[run level] 可以是 default sysinit boot shutdown 等

启动/停止/重启
rc-service <name> start|stop|restart

设定为开机启动
rc-update add <name> 
rc-update add -k sshd default

取消开机启动
rc-update del sshd

rc-update add/del 之后可以运行 rc-update -u 来更新缓存，否则 rc-status 显示的还是旧状态。

### 日志
openrc 本身的日志通过 /etc/rc.conf 中的这些来配置

# rc_logger launches a logging daemon to log the entire rc process to
# /var/log/rc.log
# NOTE: Linux systems require the devfs service to be started before
# logging can take place and as such cannot log the sysinit runlevel.
rc_logger="YES"

# Through rc_log_path you can specify a custom log file.
# The default value is: /var/log/rc.log
rc_log_path="/var/log/rc.log"

但是，似乎没有办法让 openrc 的日志进入 syslog。

### 并行启动[6]：
/etc/rc.conf
rc_parallel="YES"

### Init Scripts
[5,6,7]

位置：
/etc/init.d/<service name>  # 服务脚本，如 /etc/init.d/httpd
/etc/runlevels/<run level name>/<service name>  # 特定运行级要执行的脚本，如 /etc/runlevels/sysinit/devfs
/etc/conf.d/<service name>  # 服务脚本的配置文件，如 /etc/init.d/squid

httpd 等。

基本结构：

```
#!/sbin/openrc-run

# 声明依赖关系
depend() {
  need net # 依赖 net 服务
  need localmount
  use xxx  # 类似 need，但不是强制的，没有 xxx 本程序仍可工作
  before yyy # 需要在 yyy 服务之前启动
  after zzz # 同 need zzz
  provide mta # 
}
  
start() {
  ebegin "Starting myApp"
  VAR1=xxx VAR2=xxx VAR3=xxx \ # 设定环境变量[10]。
  start-stop-daemon \
    --wait 5000 \  # 等待 5 秒后再将状态置为”运行中“
    --background --start --exec \
    env MYENV1=env1 MYENV2=env2 \  # 设定环境变量。不需要则省略这一行[5]。但怀疑这是否能工作
    /home/user/myApp \
    --user user \ # 以其它账户运行。不指定则是 root
    --make-pidfile --pidfile /home/user/myApp.pid  \ # PID 文件
    --stdout /var/log/x.log --stderr /var/log/x.log \ # 将标准输出和错误写入日志文件。这是 openrc 的 start-stop-daemon 支持的选项 [11]。
    -- --flag1 option1 --flag2 option2 # "--" 之后是传给 myApp 的参数
  eend $?
}
  
stop() {
  ebegin "Stopping myApp"
  start-stop-daemon --stop --exec /home/user/myApp \
    --user user \ # 匹配此用户的进程
    --remove-pidfile --pidfile /home/user/myApp.pid
  eend $? #将上个命令的返回值作为整个函数的返回值。0表示成功，非0表示失败。
}
```

声明式(/etc/init.d/syslog)

```
#!/sbin/openrc-run

description="Message logging system"

name="busybox syslog"
command="/sbin/syslogd"
command_args="${SYSLOGD_OPTS}"
pidfile="/var/run/syslogd.pid"
start_stop_daemon_args="-g wheel -k 027"

depend() {
	need clock hostname klogd localmount
	provide logger
}
```

注意：
* openrc-run 旧名 runscript，有些教程中还可以看到，目前的 openrc 中两者都支持。

### 日志
似乎没有专门支持，得服务程序自己实现[12]。或许可以用 pipexec[13]，或者 named pipe + start-stop-daemon --stdout 来实现。

### supervise-daemon
代替 start-stop-daemon，可以用来在服务崩溃时自动重启它[17]。

在服务脚本中，声明式语法[16]：
```
#!/sbin/openrc-run
supervisor=supervise-daemon

user=transcoder
supervise_daemon_args="-u $user"
command=/home/transcoder/test.sh
command_args="arg1 arg2"
pidfile=/run/test.pid

depend() {
    need net
}

start_pre() {
# start_pre() 在脚本加载的时候执行一次，但不会在 supervise-daemon 重启服务时执行。
}
```

命令式语法[16]：
```
supervise-daemon --start --pidfile /run/transcoder.pid /home/transcoder/fetch.sh -- arg1 arg2
```

选项[15]：
Options: [ d:e:g:I:Kk:N:p:r:Su:1:2:ChqVv ]
  -d, --chdir <arg>                 Change the PWD
  -e, --env <arg>                   Set an environment string
  -g, --group <arg>                 Change the process group
  -I, --ionice <arg>                Set an ionice class:data when starting
  -K, --stop                        Stop daemon
  -k, --umask <arg>                 Set the umask for the daemon
  -N, --nicelevel <arg>             Set a nicelevel when starting
  -p, --pidfile <arg>               Match pid found in this file
  -u, --user <arg>                  Change the process user
  -r, --chroot <arg>                Chroot to this directory
  -S, --start                       Start daemon
  -1, --stdout <arg>                Redirect stdout to file
  -2, --stderr <arg>                Redirect stderr to file
  -h, --help                        Display this help output
  -C, --nocolor                     Disable color output
  -V, --version                     Display software version
  -v, --verbose                     Run verbosely
  -q, --quiet                       Run quietly (repeat to suppress errors)

要点[15,17]：

* 服务进程本身不能 fork。
* 服务进程不要自己创建 pidfile，让 supervise-daemon --pidfile 来管理。
  如果服务进程不能被配置为不创建 pidfile，则让 supervise-daemon --pidfile 指向不同的文件（？）。

### service supervision
openrc 目前可以使用3种 supervisor：runit，s6, supervise-daemon。最后一个是 openrc 内置的。
runit-guide.md, s6-guide.md and supervise-daemon-guide.md

## systemd
参考 systemd.md



