## 参考资料

[1] 人脸识别图像的模糊度判别算法的优化  https://www.jianshu.com/p/60ac53013be4

[2] OpenCV 图像清晰度评价（相机自动对焦） https://blog.csdn.net/dcrmg/article/details/53543341

[3] 图像清晰度评价 https://blog.csdn.net/liuuze5/article/details/50773160

[4] 无参考图像的清晰度评价方法 https://blog.csdn.net/charlene_bo/article/details/72673490

[5] 基于sift算子的模糊图像识别算法 http://www.docin.com/p-982241498.html

[6] Laplace Operator https://docs.opencv.org/2.4/doc/tutorials/imgproc/imgtrans/laplace_operator/laplace_operator.html

## 综述

文章[1]指出百度人脸识别API给出的模糊（blur）指标不准确，改进的方法是：先裁剪脸部，再用OpenCV拉普拉斯算子（cv2.Laplacian）来计算图片中的模糊量，得到了比较准确的结果。

文章[4]综述了多种无参考图像的清晰度评价方法。

论文[5]详细论述了引起图像模糊的各种原因。
