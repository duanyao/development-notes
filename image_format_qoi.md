QOI - The “Quite OK Image Format” for fast, lossless image compression
https://github.com/phoboslab/qoi

Python wrapper around qoi
https://github.com/kodonnell/qoi

https://phoboslab.org/log/2021/11/qoi-fast-lossless-image-compression

https://qoiformat.org/



## 简介

the Quite OK Image Format. It losslessly compresses RGB and RGBA images to a similar size of PNG, while offering a 20x-50x speedup in compression and 3x-4x speedup in decompression. All single-threaded, no SIMD.

## python api

### 性能测试
ARM64 
```
| Test image                | Method              | Format   | Input (kb) | Encode (ms) | Encode (kb) | Decode (ms) | SSIM |
| ------------------------- | ------------------- | -------- | ---------- | ----------- | ----------- | ----------- | ---- |
| all black ('best' case)   | PIL                 | jpg @ 90 | 6075.0     | 14.43       | 32.5        | 16.36       | 1.00 |
| all black ('best' case)   | PIL                 | png      | 6075.0     | 64.23       | 6.0         | 40.03       | 1.00 |
| all black ('best' case)   | opencv              | jpg @ 90 | 6075.0     | 43.73       | 32.5        | 28.45       | 1.00 |
| all black ('best' case)   | opencv              | png      | 6075.0     | 28.46       | 7.7         | 38.04       | 1.00 |
| all black ('best' case)   | qoi                 | qoi      | 6075.0     | 8.81        | 32.7        | 7.77        | 1.00 |
| all black ('best' case)   | qoi-lossy-0.50x0.50 | qoi      | 6075.0     | 4.47        | 8.2         | 8.20        | 1.00 |
| koi photo                 | PIL                 | jpg @ 90 | 6075.0     | 20.54       | 427.2       | 27.14       | 0.96 |
| koi photo                 | PIL                 | png      | 6075.0     | 1902.92     | 2821.5      | 134.83      | 1.00 |
| koi photo                 | opencv              | jpg @ 90 | 6075.0     | 51.00       | 427.9       | 44.17       | 0.96 |
| koi photo                 | opencv              | png      | 6075.0     | 198.68      | 3121.5      | 73.15       | 1.00 |
| koi photo                 | qoi                 | qoi      | 6075.0     | 46.90       | 3489.0      | 38.78       | 1.00 |
| koi photo                 | qoi-lossy-0.50x0.50 | qoi      | 6075.0     | 14.57       | 980.1       | 15.65       | 0.95 |
| random noise (worst case) | PIL                 | jpg @ 90 | 6075.0     | 36.04       | 1815.4      | 53.88       | 0.56 |
| random noise (worst case) | PIL                 | png      | 6075.0     | 634.56      | 6084.5      | 85.20       | 1.00 |
| random noise (worst case) | opencv              | jpg @ 90 | 6075.0     | 63.72       | 1815.4      | 74.20       | 0.56 |
| random noise (worst case) | opencv              | png      | 6075.0     | 203.22      | 6086.9      | 19.62       | 1.00 |
| random noise (worst case) | qoi                 | qoi      | 6075.0     | 40.36       | 8095.9      | 21.82       | 1.00 |
| random noise (worst case) | qoi-lossy-0.50x0.50 | qoi      | 6075.0     | 12.53       | 2018.9      | 11.93       | 0.24 |
```

X86
```
| Test image                | Method              | Format   | Input (kb) | Encode (ms) | Encode (kb) | Decode (ms) | SSIM |
| ------------------------- | ------------------- | -------- | ---------- | ----------- | ----------- | ----------- | ---- |
| all black ('best' case)   | PIL                 | jpg @ 90 | 6075.0     | 3.23        | 32.5        | 4.82        | 1.00 |
| all black ('best' case)   | PIL                 | png      | 6075.0     | 17.34       | 6.0         | 13.90       | 1.00 |
| all black ('best' case)   | opencv              | jpg @ 90 | 6075.0     | 29.10       | 32.5        | 12.12       | 1.00 |
| all black ('best' case)   | opencv              | png      | 6075.0     | 9.08        | 7.7         | 15.61       | 1.00 |
| all black ('best' case)   | qoi                 | qoi      | 6075.0     | 3.34        | 32.7        | 1.86        | 1.00 |
| all black ('best' case)   | qoi-lossy-0.50x0.50 | qoi      | 6075.0     | 1.14        | 8.2         | 1.27        | 1.00 |
| koi photo                 | PIL                 | jpg @ 90 | 6075.0     | 4.82        | 427.2       | 8.25        | 0.96 |
| koi photo                 | PIL                 | png      | 6075.0     | 654.66      | 2821.5      | 44.27       | 1.00 |
| koi photo                 | opencv              | jpg @ 90 | 6075.0     | 27.06       | 427.9       | 13.81       | 0.96 |
| koi photo                 | opencv              | png      | 6075.0     | 54.19       | 3121.5      | 24.87       | 1.00 |
| koi photo                 | qoi                 | qoi      | 6075.0     | 15.35       | 3489.0      | 9.96        | 1.00 |
| koi photo                 | qoi-lossy-0.50x0.50 | qoi      | 6075.0     | 4.37        | 980.1       | 3.06        | 0.95 |
| random noise (worst case) | PIL                 | jpg @ 90 | 6075.0     | 9.30        | 1816.1      | 16.28       | 0.56 |
| random noise (worst case) | PIL                 | png      | 6075.0     | 179.40      | 6084.4      | 27.13       | 1.00 |
| random noise (worst case) | opencv              | jpg @ 90 | 6075.0     | 31.26       | 1815.8      | 23.36       | 0.56 |
| random noise (worst case) | opencv              | png      | 6075.0     | 37.03       | 6086.9      | 8.35        | 1.00 |
| random noise (worst case) | qoi                 | qoi      | 6075.0     | 10.37       | 8096.0      | 5.04        | 1.00 |
| random noise (worst case) | qoi-lossy-0.50x0.50 | qoi      | 6075.0     | 3.26        | 2019.0      | 2.00        | 0.24 |
```