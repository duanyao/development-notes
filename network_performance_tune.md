## 参考资料

[11] Increasing the maximum number of tcp/ip connections in linux
http://stackoverflow.com/questions/410616/increasing-the-maximum-number-of-tcp-ip-connections-in-linux

[12] Linux Increasing The Transmit Queue Length (txqueuelen)
https://www.cyberciti.biz/faq/gentoo-centos-rhel-debian-fedora-increasing-txqueuelen/

[13] Linux Increase The Maximum Number Of Open Files / File Descriptors (FD)
https://www.cyberciti.biz/faq/linux-increase-the-maximum-number-of-open-files/

[14] How To: Network / TCP / UDP Tuning
https://wwwx.cs.unc.edu/~sparkst/howto/network_tuning.php

[15] 10.2. Changing Network Kernel Settings
https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/5/html/Tuning_and_Optimizing_Red_Hat_Enterprise_Linux_for_Oracle_9i_and_10g_Databases/sect-Oracle_9i_and_10g_Tuning_Guide-Adjusting_Network_Settings-Changing_Network_Kernel_Settings.html

[16] TCP Tuning parameters for different OS:es
http://proj.sunet.se/E2E/tcptune.html

[17] Administer Linux on the fly
https://www.ibm.com/developerworks/linux/library/l-adfly/index.html

[18] Linux Programmer's Manual TCP(7)
http://man7.org/linux/man-pages/man7/tcp.7.html

[19] How to achieve Gigabit speeds with Linux 
http://datatag.web.cern.ch/datatag/howto/tcp.html

[1.10] linux kernel IP Sysctl attrs
https://www.kernel.org/doc/html/latest/networking/ip-sysctl.html

[20] Hardening your TCP/IP Stack Against SYN Floods
https://www.ndchost.com/wiki/server-administration/hardening-tcpip-syn-flood

[21] tcp_syn_retries等参数详解 
http://blog.csdn.net/zhangxinrun/article/details/7621028

[22] Getting current TCP connection count on a system
https://unix.stackexchange.com/questions/67150/getting-current-tcp-connection-count-on-a-system

[23] How to count the number of open network connections on Linux
http://xmodulo.com/how-to-count-the-number-of-open-network-connections-on-linux.html

[26] Linux : Force network interface speed and duplex with EthTool
http://www.itechlounge.net/2014/02/linux-force-network-interface-speed-and-duplex-with-ethtool/

[27] Cannot get current device settings: Operation not supported ethtool CentOS 7
https://serverfault.com/questions/709670/cannot-get-current-device-settings-operation-not-supported-ethtool-centos-7

[28] wondershaper
https://github.com/magnific0/wondershaper

[31] ulimit
https://ss64.com/bash/ulimit.html

[41] A Measurement Study of the Linux TCP/IP Stack Performance and Scalability on SMP systems
https://www.cse.iitb.ac.in/~varsha/allpapers/mypapers/comswarepaper.pdf

[42] TCP IP Network Stack Performance in Linux Kernel 2.4 and 2.5
https://www.kernel.org/doc/ols/2002/ols2002-pages-8-30.pdf

[43] Re: Squid Throughput/Performance Testing
https://www.mail-archive.com/squid-dev@squid-cache.org/msg07787.html

[44] ab - Apache HTTP server benchmarking tool
https://httpd.apache.org/docs/current/programs/ab.html

[45] httperf
https://github.com/httperf/httperf

[46] Flood - a profile-driven HTTP load tester
https://httpd.apache.org/test/flood/

[4.7] 记一次Python程序的性能优化，最后竟是 TCP 的锅(timewait 的数量)
https://mp.weixin.qq.com/s/6u6GeR3Nex09pPUzMlsQaQ

[4.8] Dropping of connections with tcp_tw_recycle/ tcp_tw_reuse
https://stackoverflow.com/questions/8893888/dropping-of-connections-with-tcp-tw-recycle

[4.9] tcp: remove tcp_tw_recycle
https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=4396e46187ca5070219b81773c4e65088dac50cc

[5.1] Will increasing net.core.somaxconn make a difference?
https://serverfault.com/questions/518862/will-increasing-net-core-somaxconn-make-a-difference

[5.2] How TCP backlog works in Linux
http://veithen.io/2014/01/01/how-tcp-backlog-works-in-linux.html

[6.1] ss command: Display Linux TCP / UDP Network/Socket Information
https://www.cyberciti.biz/tips/linux-investigate-sockets-network-connections.html

[71] The default dynamic port range for TCP/IP has changed since Windows Vista and in Windows Server 2008
https://support.microsoft.com/en-us/help/929851/the-default-dynamic-port-range-for-tcp-ip-has-changed-in-windows-vista


## TCP 调优 - linux
用以下命令来查看当前的 tcp 相关设置：
sudo sysctl -a | grep "tcp\|ip\|net"

* net.core.somaxconn
limits the maximum number of requests queued to a listen socket.
这个值针对的是一个 server socket，即用来接受入站连接的 socket ，表示已经完成TCP握手、但还没有被用户代码处理的TCP连接（放在“backlog”队列里）的最大数量。
根据不同的 linux 内核版本，默认值从 128 到 4096 (5.4+)不等。
这个值也是系统调用 `int listen(int sockfd, int backlog)` 的 backlog 参数的上限，用户代码可以设置比它更小的值。
如果 backlog 队列已满，当客户端继续发起连接，则此 server socket 可以选择（1）什么也不回复，让客户端超时、重试。（2）回复 RST，让客户端立即知道连接失败。
linux 默认采用（1），设置 tcp_abort_on_overflow = 1 (true) 则采用（2）。
linux 中一个 server socket 还有另一个队列存放正在进行TCP握手的连接，其最大数量由 sys/net/ipv4/tcp_max_syn_backlog 控制，用户代码无法设置其他值。

* txqueuelen
[12, 13]
txqueuelen 网卡发送数据队列的长度。
默认值是 1000。

Small value for slower devices with a high latency like modem links and ISDN.
High value is recommend for server connected over the high-speed Internet connections that perform large data transfers.

ifconfig eth0 txqueuelen 5000
echo "/sbin/ifconfig eth0 txqueuelen 5000" >> /etc/rc.local

/etc/rc.local ： executed at the end of each multiuser runlevel

不过，较大的 txqueuelen 会造成 tcp 拥塞控制机制工作不良，因此也有人建议用较小的值（如200）[12]。
transmit queue 的主要作用是让网卡在来不及发送数据时暂时缓存数据，因此看起来较大的 txqueuelen 适用于CPU性能/网速的比值较高的系统。

* net.core.netdev_max_backlog 
[19]
网卡的接收缓冲区大小。默认值是 1000。
OS来不及处理时，包会留在这个缓冲区里，如果缓冲区满了会造成丢包。

sysctl net.core.netdev_max_backlog=2000

* net.ipv4.tcp_syncookies
开启 syn 洪水攻击。0|1表示关闭/开启。开启时，在 tcp_max_syn_backlog 进一步配置。

* net.ipv4.tcp_max_syn_backlog
半开连接的数量。值的大小与 syn flood 攻击的对抗能力有关[20]。默认值是 1024，deepin 是 256。
sysctl net.ipv4.tcp_max_syn_backlog=5000
只有当 tcp_syncookies==1 时有意义。
tcp_max_syn_backlog 不能太高，应有 TCP_SYNQ_HSIZE*16<=tcp_max_syn_backlog，TCP_SYNQ_HSIZE 是内核源码中的一个常数

* net.ipv4.tcp_synack_retries
影响半开连接的寿命。
对于远端的连接请求SYN，内核会发送SYN ＋ ACK数据报，以确认收到上一个 SYN连接请求包。这是所谓的三次握手( threeway handshake)机制的第二个步骤。这里决定内核在放弃连接之前所送出的 SYN+ACK 数目。不应该大于255，默认值是5，对应于180秒左右时间[21]。
值 3 对应大约 45s 的寿命[20]。

* File descriptor limit [13,31]
全局最大文件描述符数量在 /proc/sys/fs/file-max
在 deepin 上是 806220
可以修改 /etc/sysctl.conf 来持久修改。
fs.file-max = 1000000

每个用户的打开文件数限制可以用 ulimit
ulimit -Hn # 硬限制
ulimit -Sn # 软限制

例如 ulimit -n 10000

其中 H/S 指硬/软限制，n指文件描述符。如果没有 H/S，则两个同时被设定。
其它选项见 [31]。

要持久修改
  * 修改 /etc/security/limits.conf (适用于 debian 等系统)。
  * 修改 /etc/rc.conf 文件的 rc_ulimit="-u 1000 -n 100000"。
  * 对于服务，可以在其启动脚本中设定，这是针对特定服务的。
  * 服务自己的配置选项，例如 squid 的 max_filedescriptors。

```
*               soft    nofile           20000
*               hard    nofile           20000
# Set httpd user soft and hard limits as follows:
httpd soft nofile 4096
httpd hard nofile 10240
```
* 端口范围
net.ipv4.ip_local_port_range
deepin 上默认值 32768	61000

设置较大的范围可以允许更多的连接数。
sysctl -w net.ipv4.ip_local_port_range="10000 65000"

* 缓冲区/TCP窗口
[14-18]
net.core.rmem_max # 所有连接类型的接收缓冲区最大尺寸，单位字节
net.core.wmem_max # 所有连接类型的接收缓冲区最大尺寸，单位字节

deepin 上都是 212992
带宽延迟乘积越高的网络上，这个值应该越大。

net.ipv4.tcp_rmem # tcp 自动调节接收缓冲区参数，三个值分别是最小、默认、最大，单位字节。
 最大值根据 net.ipv4.tcp_mem[1] 来计算。deepin 上是 4096 87380 6291456。
 一般不用修改。

net.ipv4.tcp_wmem # tcp 自动调节发送缓冲区参数，三个值分别是最小、默认、最大，单位字节。

net.ipv4.tcp_mem # 全局 tcp 内存占用设定，三元组 [low, pressure, high]，单位是 system page size （x86一般是 4KiB）。
  当 tcp 内存占用达到 pressure 时，OS 设法降低内存占用，直到 low；内存占用不允许超过 high。
  默认值是内核根据可用内存计算出来的，deepin 上（8GB）默认值是 4096	87380	6291456，按字节算是 16，341，24576M。
  一般不用修改。
  
  
### 端口耗尽问题和 timewait (TIME_WAIT) 调优

TCP/UDP 端口的总量是 64K 。服务器用客户端的 IP+port 来区分不同的连接，所以端口的总量会限制来自同一客户端 IP 的连接数。一般来说这是足够的，但如果有很多客户机位于同一个 NAT 后面（IP都相同），端口可能会不够用。

timewait 调优对于使用大量短连接的服务器是有价值的，否则本地端口可能很快耗尽[4.7-4.8]。

TCP 链接在经过四次握手结束连接后并不会立即释放，而是处于 timewait 状态，会等待一段时间，以防止客户端后续的数据未被接收[4.7]。

```
#timewait 的数量，默认值在不同的系统上不同。
net.ipv4.tcp_max_tw_buckets = 6000

net.ipv4.ip_local_port_range = 1024 65000

#启用 timewait 快速回收。
net.ipv4.tcp_tw_recycle = 1#开启重用。允许将 TIME-WAIT sockets 重新用于新的 TCP 连接。4.12 后不要使用。
net.ipv4.tcp_tw_reuse = 1
```

服务器开启 tcp_tw_recycle 可能导致 NAT 丢弃部分数据，造成 NAT 后的客户端随机地无法连接，这时可以只开启 tcp_tw_reuse 并降低 tcp_fin_timeout [4.8]。
linux 4.12 （2017年）删除了 tcp_tw_recycle。[4.9]

## TCP 监控
### 连接总量统计
参考：

/proc/net/sockstat

  TCP: inuse 39 orphan 11 tw 13 alloc 46 mem 12
  UDP: inuse 22 mem 12
  UDPLITE: inuse 0
  RAW: inuse 1
  FRAG: inuse 0 memory 0

/proc/net/sockstat6

  TCP6: inuse 6
  UDP6: inuse 8
  UDPLITE6: inuse 0
  RAW6: inuse 0
  FRAG6: inuse 0 memory 0

用 ss (包名 iproute2) 命令：
ss -s

```
ss -s
Total: 2361
TCP:   163 (estab 59, closed 36, orphaned 0, timewait 28)

Transport Total     IP        IPv6
RAW       2         1         1        
UDP       20        18        2        
TCP       127       110       17       
INET      149       129       20       
FRAG      0         0         0
```

### 查看每个连接
netstat (包名 net-tools)：
```
netstat -npt

Active Internet connections (w/o servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 127.0.0.1:6379          127.0.0.1:55828         ESTABLISHED 3432/./redis-server 
tcp        0      1 192.168.0.24:22         142.93.184.4:51140      LAST_ACK    -                   
tcp        0      0 192.168.0.24:22         124.64.23.139:52002     ESTABLISHED 1278106/sshd: root@ 
tcp        0      0 192.168.0.24:59282      192.168.0.26:22         ESTABLISHED 1284462/ssh
tcp        0      0 172.17.0.2:9100         192.168.0.32:52060      TIME_WAIT   -                   
tcp        0      0 172.17.0.2:59866        192.168.0.32:80         TIME_WAIT   -
```
state（状态）：
* ESTABLISHED: 打开的连接。
* TIME_WAIT (timewait)：正在关闭的连接。具体来说，本机已经发送了关闭的包，等待对端回复确认关闭的包。

-a （全部），也包括在监听的 server socket。
-l （listen），在监听的 server socket。

/proc/net/tcp
```
cat /proc/net/tcp
  sl  local_address rem_address   st tx_queue rx_queue tr tm->when retrnsmt   uid  timeout inode                                                     
   0: 3500007F:0035 00000000:0000 0A 00000000:00000000 00:00000000 00000000   102        0 60293142 1 ffff96ec58caf1c0 100 0 0 10 5                  
   1: 00000000:0016 00000000:0000 0A 00000000:00000000 00:00000000 00000000     0        0 60290733 4 ffff96ec4c9b2bc0 100 0 0 10 0                  
   2: 00000000:006F 00000000:0000 0A 00000000:00000000 00:00000000 00000000     0        0 18130 1 ffff96ec58cacec0 100 0 0 10 0
```
ss (包名 iproute2)：
```
ss -neopt

State                     Recv-Q                     Send-Q                                                     Local Address:Port                                                          Peer Address:Port                      Process                                                                                                                                                                                                                            
ESTAB                     0                          0                                                              127.0.0.1:6379                                                             127.0.0.1:55828                      users:(("redis-server",pid=3432,fd=9)) timer:(keepalive,4min37sec,0) ino:89544 sk:2c cgroup:/user.slice/user-0.slice/session-5.scope <->                                                                                          
ESTAB                     0                          0                                                           192.168.0.24:22                                                           124.64.23.243:46776                      users:(("sshd",pid=1276648,fd=4)) timer:(keepalive,108min,0) ino:169969291 sk:896f0 cgroup:/system.slice/ssh.service <->                                                                                                          
ESTAB                     0                          0                                                              127.0.0.1:3306                                                             127.0.0.1:50440

ss -neopt state established
Recv-Q  Send-Q               Local Address:Port                  Peer Address:Port   Process                                                                                                                                       
0       0                        127.0.0.1:6379                     127.0.0.1:55828   users:(("redis-server",pid=3432,fd=9)) timer:(keepalive,18sec,0) ino:89544 sk:2c cgroup:/user.slice/user-0.slice/session-5.scope <->         
0       0                     192.168.0.24:22                   124.64.23.243:46776   users:(("sshd",pid=1276648,fd=4)) timer:(keepalive,109min,0) ino:169969291 sk:896f0 cgroup:/system.slice/ssh.service <->
```

注意，netstat 和 ss 显示的连接信息不包括容器的（docker），即使容器映射端口到宿主机，且连接来自宿主机之外。
要显示容器相关的连接，要在容器内执行 netstat 和 ss，例如：`docker exec e5db9a01d4a8 netstat |grep ESTABLISHED`。

### atop
tcpao 是活跃连接数

### conntrack
[23]
sudo conntrack -C #连接数
sudo conntrack -L #所有的连接

## 限制网络速度/限速
### 修改网卡速度
查看网口速度及其他信息：
  ethtool <设备名>

更改网口速度[26]：
  ethtool -s <设备名> speed <10|100|1000|10000> duplex <half|full> autoneg <on|off>
  
  例如 ethtool -s eth0 speed 10 autoneg off
  
但是，在虚拟机中，似乎无法更改网口速度[27]。

在 Windows 上，可以在“网络连接->属性->配置->高级->链接速度和双工”中修改。

### 软件限制 - wondershaper
适用于 linux。问题：经过测试，局域网内还是比较有效的，但广域网上有问题，会让网速过度降低。

版本 1.2
./wondershaper -a eth0 -d 10000 -u 2000
./wondershaper -s -a eth0
./wondershaper -c -a eth0

版本 1.1
sudo apt install wondershaper
wondershaper enp4s0 10000 2000
wondershaper clear enp4s0

### 限速的效应
假定最简单的情形，两端在一个局域网（以太网）里，考虑单项发送数据，根据两端的相对网口速度，有3种类型：
* 漏斗型，即发送端比接收端网口快。
  这时会出现较高的丢包率，容易导致 TCP 连接因为连续重传失败而异常断开。
* 喇叭型，即发送端比接收端网口慢。
  这时丢包率很低，TCP 连接稳定。

以上是传输层以下没有流控制（flow control）机制的情形。实际上以太网有流控制机制，Windows “网络连接->属性->配置->高级->流控制”中可以设置。
使用以太网流控制时，如果接收端饱和了，它会向接收端发送一个特殊帧，使其暂停发送。不过，只有网络路径上的所有节点都支持流控制才行，所以实践上用得并不多。

## 问题
### linux node.js server 如果发送 'Connection': 'keep-alive', 而 ab -c 100 -k 会收到服务器的 RST。
原因是虽然有keep-alive，但是没有 content-length 也没有 'Transfer-Encoding': 'chunked'，node.js 只好强行关闭连接。

### linux node.js server 如果 ab -n 40000 -c 100，则会有 16000 多个 TIME_WAIT 连接，要1分钟以上才下降。
数据来自 conntrack -C 。

如果两台机器同时，能达到 32000；如果1分钟内连续操作，可达 65000 多，但似乎不会进一步提高了。-c 200 没有明显变化。

从 linux 到 linux 测试，连接数上限达到 55007，但连续执行 ab 的性能似乎并未下降。

### 并发数达到双机 2x300 或更高时，rps 明显下降（到1/6），但如果是单机 1000 并发则 rps 不会下降。
抓包看不出明显问题。双机时有少数 RST 和 TCP Port numbers reused。

## 性能测试工具
### ab - Apache HTTP server benchmarking tool
主要问题：
1. 不支持 http1.1，特别是 chunked encoding。
2. 对 node.js 服务器，最小延迟很高，为 40ms。对 squid 则只有 0.1ms。

### JMeter

### linux上安装
apt 的系统：apache2-utils
centos6: httpd-tools

## TCP 连接数（Windows）
最大连接数：
[HKEY_LOCAL_MACHINE \System \CurrentControlSet \Services \Tcpip \Parameters]
TcpNumConnections = 0x00fffffe (Default = 16,777,214)

动态TCP端口范围（影响最大出站连接数）,上限由这个控制：
[HKEY_LOCAL_MACHINE \System \CurrentControlSet \Services \Tcpip \Parameters]
MaxUserPort = 5000 (Default = 5000, Max = 65534)

较老的 windows 是 1024-5000, vista 之后是 49152-65535。vista 之后这个注册表项默认不存在，但可以用以下命令查看和更改：
    netsh int ipv4 show dynamicport tcp
    netsh int ipv4 show dynamicport udp
    netsh int ipv6 show dynamicport tcp
    netsh int ipv6 show dynamicport udp 
    netsh int ipv4 set dynamicport tcp start=10000 num=1000
    netsh int ipv4 set dynamicport udp start=10000 num=1000
    netsh int ipv6 set dynamicport tcp start=10000 num=1000
    netsh int ipv6 set dynamicport udp start=10000 num=1000


最大活动连接数：
[HKEY_LOCAL_MACHINE \System \CurrentControlSet \Services \Tcpip \Parameters]
MaxFreeTcbs = 2000 (Default = RAM dependent, but usual Pro = 1000, Srv=2000)

[HKEY_LOCAL_MACHINE \System \CurrentControlSet \services \Tcpip \Parameters]
MaxHashTableSize = 512 (Default = 512, Range = 64-65536)

backlog：
5 (200 when Server)

SYN-Attack-Protection 
how many connection attempts (half-open) one can make simultaneously (XP SP2 & Vista = 10; Vista SP2 = no limit)

the half-open TCP connections limit
HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\EnableConnectionRateLimiting

Windows 7 以上，似乎上述注册表项都默认不存在。

