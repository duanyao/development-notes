QuickJS 是一个轻量且可嵌入的 JavaScript 引擎，它支持 ES2019 规范，包括 ES module、异步生成器以及 proxies。
  https://bellard.org/quickjs/
  https://www.oschina.net/p/quickjs
  https://github.com/quickjs-zh/QuickJS

txiki.js — The tiny JavaScript runtime
  https://github.com/saghul/txiki.js

moddable xs JavaScript engine
  https://github.com/Moddable-OpenSource/moddable
  https://github.com/Moddable-OpenSource/moddable-xst

graaljs - A high performance implementation of JavaScript. Built on the GraalVM by Oracle Labs.
  https://github.com/oracle/graaljs

Hermes JavaScript engine optimized for fast start-up of React Native apps on Android. It features ahead-of-time static optimization and compact bytecode.
  https://github.com/facebook/hermes

An implementation of ECMA-262 in JavaScript, new JS features can be quickly prototyped
  https://github.com/engine262/engine262

JavaScript (engine) Version Updater. install various JavaScript engines without compile source.
  https://github.com/GoogleChromeLabs/jsvu
