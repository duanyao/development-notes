## 参考资料
[4.6] tomcat Class Loader HOW-TO
  https://tomcat.apache.org/tomcat-7.0-doc/class-loader-howto.html
[4.7] 理解Tomcat的Classpath-常见问题以及如何解决
  https://www.linuxidc.com/Linux/2011-08/41684.htm
  
[5.1]
  https://stackoverflow.com/questions/20622746/exception-loading-sessions-from-persistent-storage
  
[5.2] 
  https://serverfault.com/questions/300222/invalidate-all-sessions-at-application-reload

## tomcat
### 类路径/类加载器
#### 共享的类路径/类加载器
各个类路径/类加载器的关系如下图[4.6]：

```
  Bootstrap
      |
    System
      |
    Common
     /  \
Server  Shared
         /  \
   Webapp1  Webapp2 ...
```

注意，此图并不意味着 Webapp 类路径的优先级是最低的；事实上，它高于 Shared，Common 和 System，但低于 Bootstrap；此外，Servlet API 类不能被 Webapp 类路径覆盖[4.6]。

其中，Common、Server、Shared 类路径可以在 tomcat安装目录下的 conf/catalina.properties 文件中配置，分别是属性 common.loader, server.loader, shared.loader [4.6,4.7]。

Common 的默认路径包括tomcat安装目录下的 lib/ 和 lib/*.jar，而 Server、Shared 类路径默认是空的。

优先级：Webapp > Common 。

### session 持久化

tomcat 关闭时，session 持久化文件默认保存在[5.1]：

CATALINA_HOME/work/Catalina/localhost/<appName>

tomcat 启动时，从上述目录读取文件，然后删除文件。

关闭 session 持久化：

在 META-INF/context.xml 文件（如果没有则新建）中配置：

<Context>
  <Manager pathname="" />
</Context>

### session 超时
在conf目录下的web.xml 设置所有web的session

 

 

    <session-config>  
      
        <session-timeout>15</session-timeout>//单位为分钟   
      
    </session-config>  
    
3.为单个WEB设置SESSION 在WEB.XML中添加

 

    <session-config>  
      
        <session-timeout>15</session-timeout>//单位为分钟   
      
    </session-config> 

 

 

4.为单个Servlet指定会话超时时间

 

    <servlet>  
     <servlet-name>Servlet名称</servlet-name>  
     <servlet-class>Servlet类路径</servlet-class>  
     <init-param>  
      <param-name>timeout</param-name>  
      <param-value>600</param-value>  
     </init-param>  
    </servlet> 



