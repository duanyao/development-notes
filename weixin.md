## 参考资料
[1] 微信网页授权
https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140842

[2] 发送模板消息
https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1433751277

[3] 微信订阅号和服务号的区别是什么
https://jingyan.baidu.com/article/6fb756ec74193b241958fb49.html

[4] 微信的订阅号和服务号如何区分？
https://www.zhihu.com/question/21289814

[5] 微信服务号怎样做到每天推送一条消息?
https://www.zhihu.com/question/21692559

[6] 微信公众号开发之模板消息
https://www.jianshu.com/p/eb0e9c4dcdfe

[7] 微信公众号开发之授权获取用户信息
https://www.jianshu.com/p/01b04bdf9645

[8] 极速开发微信公众号
https://www.jianshu.com/p/a172a1b69fdd

## 微信开发工具

## 微信公众号简介
什么是订阅号？

订阅号：为媒体和个人提供一种新的信息传播方式，主要功能是在微信侧给用户传达资讯；（功能类似报纸杂志，提供新闻信息或娱乐趣事）

适用人群：个人、媒体、企业、政府或其他组织。

群发次数：订阅号（认证用户、非认证用户）1天内可群发1条消息。

1）如果想用公众平台简单发发消息，做宣传推广服务，建议可选择订阅号；

2）如果想用公众平台进行商品销售，建议可选择服务号，后续可认证再申请微信支付商户；

什么是服务号？

服务号：为企业和组织提供更强大的业务服务与用户管理能力，主要偏向服务类交互（功能类似12315，114，银行，提供绑定信息，服务交互的）；

适用人群：媒体、企业、政府或其他组织。

群发次数：服务号1个月（按自然月）内可发送4条群发消息。

1）如果想用公众平台简单发发消息，做宣传推广服务，建议可选择订阅号；

2）如果想用公众号获得更多的功能，例如开通微信支付，建议可以选择服务号。

## 微信网页授权
[1]



## 发送模板消息
[2]

模板消息仅用于公众号向用户发送重要的服务通知，只能用于符合其要求的服务场景中，如信用卡刷卡通知，商品购买成功通知等。

接口调用请求说明

http请求方式: POST
https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN

POST数据说明

POST数据示例如下：

      {
           "touser":"OPENID",
           "template_id":"ngqIpbwh8bUfcSsECmogfXcV14J0tQlEpBO27izEYtY",
           "url":"http://weixin.qq.com/download",  
           "miniprogram":{
             "appid":"xiaochengxuappid12345",
             "pagepath":"index?foo=bar"
           },          
           "data":{
                   "first": {
                       "value":"恭喜你购买成功！",
                       "color":"#173177"
                   },
                   "keyword1":{
                       "value":"巧克力",
                       "color":"#173177"
                   },
                   "keyword2": {
                       "value":"39.8元",
                       "color":"#173177"
                   },
                   "keyword3": {
                       "value":"2014年9月22日",
                       "color":"#173177"
                   },
                   "remark":{
                       "value":"欢迎再次购买！",
                       "color":"#173177"
                   }
           }
       }

## 微信浏览器清除缓存

安卓手机：

访问 http://debugx5.qq.com/ ，勾选cookie、文件缓存，点击“清理”。
如果显示“非x5内核”，则先访问 http://debugmm.qq.com/?forcex5=true 再重新访问上述链接。

iPhone 手机：

将微信退出登录，杀掉微信，重新登录，可清除微信 cookie。
按下面的链接操作，只清理“缓存”即可清除微信缓存。
https://www.yubaibai.com.cn/article/5617246.html

参考：
https://www.bilibili.com/read/cv6839449/
