# AMD GPU linux
## 参考资料
[1.1] rocm 安装（2024.10）
  https://rocm.docs.amd.com/projects/install-on-linux/en/docs-6.2.0/install/quick-start.html
  https://rocm.docs.amd.com/projects/install-on-linux/en/docs-6.2.0/install/detailed-install.html
  https://rocm.docs.amd.com/projects/install-on-linux/en/latest/install/post-install.html

[1.2] Compatibility Matrix
  https://rocm.docs.amd.com/en/latest/compatibility/compatibility-matrix.html#operating-systems-and-kernel-versions

[1.3] Install MIGraphX for Radeon GPUs
  https://rocm.docs.amd.com/projects/radeon/en/docs-6.0.2/docs/install/install-migraphx.html

[2.1] Install ONNX Runtime for Radeon GPUs
  https://rocm.docs.amd.com/projects/radeon/en/latest/docs/install/native_linux/install-onnx.html

## ROCM 安装
参考 [1.1]。最新版本请在 https://repo.radeon.com/amdgpu-install/ 中寻找。注意 latest 不一定是最新的，较新的内核一般需要较新的 amdgpu-install 。
目前（2024.10）rocm 支持的内核小版本是不连续的，例如 6.x 中仅支持 6.2, 6.5, 6.8，而最新的稳定内核是 6.10。
rocm 仅提供 ubuntu、rhel、suse 的版本，debian 也可以安装近似发行日期的 ubuntu 的版本。
```
sudo apt update
sudo apt install "linux-headers-$(uname -r)" "linux-modules-extra-$(uname -r)"
sudo usermod -a -G render,video $LOGNAME # Add the current user to the render and video groups
wget https://repo.radeon.com/amdgpu-install/6.2.3/ubuntu/noble/amdgpu-install_6.2.60203-1_all.deb
sudo apt install ./amdgpu-install_6.2.60203-1_all.deb
sudo apt update
sudo apt install amdgpu-dkms
sudo apt install rocm
```

sudo amdgpu-install --usecase=rocm

rocm-smi ROCm System Management Interface (ROCm SMI) command-line interface
rocm-core Radeon Open Compute (ROCm) Runtime software stack
rocm
rocm-dev
rocm-developer-tools
rocm-device-libs/stable 5.2.3-2 i386
  AMD specific device-side language runtime libraries
rocm-libs/noble 6.2.0.60200-66~24.04 amd64
  Radeon Open Compute (ROCm) Runtime software stack
rocm-ml-libraries/noble 6.2.0.60200-66~24.04 amd64
  Radeon Open Compute (ROCm) Runtime software stack
rocm-ml-sdk/noble 6.2.0.60200-66~24.04 amd64
  Radeon Open Compute (ROCm) Runtime software stack
rocm-opencl/noble 2.0.0.60200-66~24.04 amd64
  clr built using CMak
rocm-utils/noble 6.2.0.60200-66~24.04 amd64
  Radeon Open Compute (ROCm) Runtime software stack

rocm-core:
/opt/rocm-6.2.0/lib/librocm-core.so.1.0.60200

```
sudo tee --append /etc/ld.so.conf.d/rocm.conf <<EOF
/opt/rocm/lib
/opt/rocm/lib64
EOF
sudo ldconfig
```

### amdgpu-install Unsupported OS: /etc/os-release ID 'deepin'

/usr/bin/amdgpu-install: 
```
os_release() {
	if [[ -r  /etc/os-release ]]; then
		. /etc/os-release
		PKGUPDATE=

		case "$ID" in
		ubuntu|linuxmint|debian|deepin) # 改之前： ubuntu|linuxmint|debian
			PKGUPDATE="apt-get update"
			PKGMAN=apt-get
			OS_CLASS=debian
```

## 安装 pip onnxruntime_rocm
参考 [2.1]
安装依赖（migraphx, half）
```
sudo apt install migraphx
```
MIGraphX is a graph optimizer [1.3].

最新版本 onnxruntime_rocm 可以在这里找到：
https://repo.radeon.com/rocm/manylinux/

不是每个 python 版本都会支持，目前（2024.10）onnxruntime_rocm-1.18.0(rocm-rel-6.2.3) 仅支持 python 3.10。可以这样安装：

```
pip install onnxruntime-rocm -f https://repo.radeon.com/rocm/manylinux/rocm-rel-6.2.3/
或者
pip install https://repo.radeon.com/rocm/manylinux/rocm-rel-6.2.3/onnxruntime_rocm-1.18.0-cp310-cp310-linux_x86_64.whl
```
测试：
```
>>> import onnxruntime as ort
>>> ort.get_available_providers()
['MIGraphXExecutionProvider', 'ROCMExecutionProvider', 'CPUExecutionProvider']
```
MIGraphXExecutionProvider 和 ROCMExecutionProvider 是 onnxruntime_rocm 提供的 onnxruntime 后端。
