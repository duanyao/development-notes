## 参考资料

[1] List of Intel codenames
 https://en.wikipedia.org/wiki/List_of_Intel_codenames
 
[2] Skylake (client) - Microarchitectures - Intel 
 https://en.wikichip.org/wiki/intel/microarchitectures/skylake_(client)

## 微架构代号与型号对应
### Core 系列

2006 	Intel Core 	第一代 Core 2, Core 2 Duo E 7xxx, Pentium E5xxx, Celeron E3xxx
2008 	Nehalem 	Core i5 750, Core i7 860, Core i7 960, Celeron P1053
2011 	Sandy Bridge
  桌面版：Core i3 21xx, Core i5 23xx|24xx|25xx, Core i7 26xx|27xx|38xx|39xx, Pentium G6xx|G8xx, Celeron G4xx|G5xx
  移动版：Core i3 23xxM, , Core i5 24xxM|25xxM, Core i7 26xxM|27xxM|29xxM , Pentium B9xx|9xx, Celeron B8xx|7xx|8xx
  
2012  Ivy Bridge: Sandy Bridge 的 22 nm改进
  桌面版：Core i3 32xx, Core i5 33xx|34xx|35xx, Core i7 37xx|38xx|49xx, Pentium G20xx|G21xx, Celeron G16xx
  移动版：Core i3 31xxM|31xxU|32xxY, Core i5 32xxM|33xxU|34xxU|34xxY, Core i7 35xxM|35xxU|36xxY, Pentium 20xxM|21xxU, Celeron 10xxM|10xxU
  
2013 	Haswell
 Broadwell:14 nm改进
  
2015 	Skylake 	14 (16 with fetch/retire) 	4200 MHz
2016 	Goldmont 	20 unified with branch prediction 	3500 MHz
2016 	Kabylake 	14 (16 with fetch/retire) 	4500 MHz
2017 	Cannonlake 	14 	 ?

"Skylake-SP" (14 nm) Scalable Performance

    Xeon Platinum supports up to 8 sockets. Xeon Gold supports up to 4 sockets. Xeon Silver and Bronze support up to 2 sockets.
        −M: 1536 GB RAM per socket instead of 768 GB RAM for non−M SKUs
        −F: integrated OmniPath fabric
        −T: High thermal-case and extended reliability
    Support for up to 12 DIMMs of DDR4 memory per CPU socket.
    Xeon Platinum, Gold 61XX, and Gold 5122 have two AVX-512 FMA units per core. Xeon Gold 51XX (except 5122), Silver, and Bronze have a single AVX-512 FMA unit per core.

Suffixes to denote:

    M – Mobile processor
    Q – Quad-core
    U – Ultra-low power
    X – "Extreme"
    Y – Extreme ultra-low power


### Atom 系列

2008 	Bonnell
  Diamondville: 2008: Atom Z5xx, Atom N2xx
  Pineview 2009-2010: N450, D510, D410, N470
  Cedarview 2011: D25xx, D27xx
2013 	Silvermont
  Bay Trail-T(平板): Z36xx, Z37xx
  Bay Trail-I(微型主机): E38xx
2015  Airmont
  Cherry Trail-T(平板): x5-Z8xxx
  Braswell(微型主机):2016: x5-E8000
