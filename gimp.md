## 参考资料
[9.1] issues for "entry point could not be located" in project GNOME / GIMP
https://gitlab.gnome.org/search?utf8=%E2%9C%93&snippets=&scope=issues&repository_ref=&search=entry+point+could+not+be+located&project_id=1848

[9.2] The procedure entry point inflateValidate could not be located in the dynamic link library C:\Program Files\GIMP2\bin\libpng16-16.dll.
  https://gitlab.gnome.org/GNOME/gimp/issues/1383
  
[9.3] libpng16 throwing entry point inflateReset2 could not be located
  https://github.com/msys2/MINGW-packages/issues/813
  
[9.4] Dynamic-Link Library Search Order
  https://docs.microsoft.com/en-us/windows/win32/dlls/dynamic-link-library-search-order#search-order-for-desktop-applications
  
[9.5] GIMP Portable
  https://portableapps.com/apps/graphics_pictures/gimp_portable
  
## 显示日志/控制台输出
在 windows 上，使用命令 gimp-2.10.exe --verbose --console-messages ，可以显示详细日志/控制台输出。

例如，插件加载相关的是：

Parsing 'C:\Users\duanyao\AppData\Roaming\GIMP\2.10\pluginrc'
Querying plug-in: 'C:\Program Files\GIMP 2\lib\gimp\2.0\plug-ins\file-rawtherapee\file-rawtherapee.exe'
Querying plug-in: 'C:\Program Files\GIMP 2\lib\gimp\2.0\plug-ins\file-darktable\file-darktable.exe'
Initializing plug-in: 'C:\Program Files\GIMP 2\lib\gimp\2.0\plug-ins\file-rawtherapee\file-rawtherapee.exe'
Initializing plug-in: 'C:\Program Files\GIMP 2\lib\gimp\2.0\plug-ins\file-darktable\file-darktable.exe'

## 插件/plugin
配置文件（自动生成）：
C:\Users\duanyao\AppData\Roaming\GIMP\2.10\pluginrc

插件目录：
C:\Program Files\GIMP 2\lib\gimp\2.0\plug-ins
  
## bugs

### entry point could not be located in the dynamic link library
在 32 位 win 10 上安装 gimp-2.10.12-setup-3.exe ，会遇到 entry point could not be located in the dynamic link library 错误，涉及 web-page.exe，libpng16-16.dll 等文件。
64 位 win 10 上安装 gimp-2.10.12-setup-3.exe 没有发现问题。
参考gimp的bug库：[9.1, 9.2] 。

entry point 有 inflateReset2、inflateValidate 等。inflateReset2、inflateValidate 应该属于 zlib1.dll，这也是 libpng16-16.dll 的依赖。这两个文件都在 C:\Program Files\GIMP 2\bin 下，
可以用 depends 程序查看其导出函数表。因此需要考察 zlib1.dll 丢失、版本不对或者系统里有其它版本的 zlib1.dll 的问题。

根据[9.3]，有些程序（如intel wifi（C:\Program Files\Intel\WiFi\bin\））会把自己的 zlib1.dll 所在目录加到 PATH 里面，这样其它程序所在的目录（exe 所在目录）下放置 zlib1.dll，就会加载另一个版本的 zlib1.dll。

根据[9.4]，程序所在的目录高于PATH。

在我的 64 位 win 10 上，C:\Program Files\Intel\WiFi\bin\ 也在PATH里，并且有 zlib1.dll（是x64）。64 位 win 10 上，C:\Program Files\Intel\WiFi\bin\zlib1.dll 版本是 1.2.3 ，而且没有 inflateReset2、inflateValidate。
但我的 64 位 win 10 上，gimp 并没有报告 entry point 错误。

## 解决办法
* 寻找 C:\Program Files\GIMP 2\bin\zlib1.dll ，如果不存在，复制一个（从好的gimp安装中复制）。
* 去掉 PATH 中的 C:\Program Files\Intel\WiFi\bin\
* 采用精简安装。
* 运行命令 gimp-2.10.exe --verbose --console-messages
* 安装 portable 版本
