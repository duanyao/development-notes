## 参考资料
[1.1] xxHash is an extremely fast non-cryptographic hash algorithm
https://xxhash.com/

[1.2] xxHash - Extremely fast hash algorithm
https://github.com/Cyan4973/xxHash

[1.3] CityHash, a family of hash functions for strings
https://github.com/google/cityhash

[1.4] CRC Cyclic Redundancy Check
https://www.techopedia.com/definition/1793/cyclic-redundancy-check-crc

[1.5] CRC-3 CRC RevEng
https://reveng.sourceforge.io/crc-catalogue/all.htm

[1.6] BLAKE2 — fast secure hashing
https://www.blake2.net/

[1.7] BLAKE3 a cryptographic hash function
https://github.com/BLAKE3-team/BLAKE3

[2.1] xxhash is a Python binding for the xxHash library by Yann Collet.
https://pypi.org/project/xxhash/

[2.2] Python crc lib Cyclic Redundancy Check
https://github.com/Nicoretti/crc

[2.3] Python的hashlib提供了常见的摘要算法，如MD5，SHA1等等
https://www.liaoxuefeng.com/wiki/1016959663602400/1017686752491744

[2.4] hashlib — Secure hashes and message digests
https://docs.python.org/3/library/hashlib.html

[2.5] Python bindings for the official Rust implementation of BLAKE3
https://github.com/oconnor663/blake3-py

[3.1] 文件校验工具 gchecksum 与 shasum/xxhsum 性能对比
https://zhuanlan.zhihu.com/p/636876667

[3.1] gchecksum is an easy-to-use and high-performance file hashing tool.
https://github.com/Glavo/gchecksum

## xxHash
xxHash 是一系列非加密散列算法，输出长度 32、64、128bit。它适用于数据校验，但不适合加密应用。
xxHash 与 MD5、SHA-1 相比速度要快得多，与 CRC32、Adler32 等相比，提供了更大的输出长度，碰撞的可能性更低。

### xxHash python API

```
>>> import xxhash
>>> x = xxhash.xxh32()
>>> x.update(b'Nobody inspects')
>>> x.update(b' the spammish repetition')
>>> x.digest()
b'\xe2);/'
>>> x.digest_size
4
>>> x.block_size
16

>>> xxhash.xxh32(b'Nobody inspects the spammish repetition').hexdigest()
'e2293b2f'
>>> xxhash.xxh32(b'Nobody inspects the spammish repetition').digest() == x.digest()
True
```

## BLAKE2/3
BLAKE2/3 是一系列加密散列算法，输出长度1～64 bytes。BLAKE2 曾经是 sha-3 的候选算法之一。
BLAKE2 的特点之一是变长输出，这意味着它也可以用于低强度（非加密）散列算法。

BLAKE3 显著改进了速度，达到约 6.8GiB/s/线程[1.7]。

### python API
blake2 - hashlib
```
hashlib.blake2b(data=b'', *, digest_size=64, key=b'', salt=b'', person=b'', fanout=1, depth=1, leaf_size=0, node_offset=0, node_depth=0, inner_size=0, last_node=False, usedforsecurity=True)
```
blake3
```
pip install blake3
# Hash some input all at once. The input can be bytes, a bytearray, or a memoryview.
hash1 = blake3(b"foobarbaz").digest()
```

## CRC (Cyclic Redundancy Check)

### python crc

```
from crc import Calculator, Crc8

expected = 0xBC
data = bytes([0, 1, 2, 3, 4, 5])
calculator = Calculator(Crc8.CCITT, optimized=True)

assert expected == calculator.checksum(data)
```

## python hashlib
python hashlib 设计用于摘要算法。目前内置了 SHA2, SHA3, shake, whirlpool, blake2b, blake2s等安全摘要算法，但也保留了一些旧的不再安全的算法，如 MD5、SHA-1等。

```
import hashlib

hashlib.algorithms_available
{'shake_256', 'shake_128', 'sha3_384', 'blake2s', 'md5', 'sha3_256', 'sm3', 'sha512_224', 'sha512', 'sha3_224', 'sha3_512', 'md5-sha1', 'sha224', 'blake2b', 'whirlpool', 'sha1', 'sha256', 'sha384', 'sha512_256', 'md4', 'ripemd160'}

hashlib.algorithms_guaranteed
{'sha3_224', 'sha3_512', 'sha3_384', 'shake_256', 'blake2s', 'sha1', 'md5', 'sha256', 'sha224', 'sha384', 'sha3_256', 'shake_128', 'sha512', 'blake2b'}

m = hashlib.sha256() # 等效于 hashlib.new('sha256')

m.update(b"Nobody inspects")

m.update(b" the spammish repetition")

m.digest()
b'\x03\x1e\xdd}Ae\x15\x93\xc5\xfe\\\x00o\xa5u+7\xfd\xdf\xf7\xbcN\x84:\xa6\xaf\x0c\x95\x0fK\x94\x06'

m.hexdigest()
'031edd7d41651593c5fe5c006fa5752b37fddff7bc4e843aa6af0c950f4b9406'

import io, hashlib, hmac

with open(hashlib.__file__, "rb") as f:
    digest = hashlib.file_digest(f, "sha256")
digest.hexdigest()
'...'
```