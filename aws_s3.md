## nodejs sdk V2
### httpOptions
```
import S3 from 'aws-sdk/clients/s3'
import * as http from 'http'
import * as https from 'https'

    // S3 SDK 的默认 http(s) Agent 以及 node.js 的默认 http(s) Agent 可能是没有启用 keepAlive 的，此处启用。
    // 最大并发连接数仍然不限制。参考 https://nodejs.org/api/http.html#new-agentoptions
    const agentOpts = {
      keepAlive: true,
    }
    const s3opt: any = {
      ...options,
      secretAccessKey: options.accessKeySecret,
      httpOptions: {
        timeout: options.timeout,
        agent: options.sslEnabled? new https.Agent(agentOpts) : new http.Agent(agentOpts)
      },
    }
```
