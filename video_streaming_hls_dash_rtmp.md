## 参考资料
[1.1] HLS Spec https://tools.ietf.org/html/draft-pantos-hls-rfc8216bis-04
[1.2] https://developer.apple.com/documentation/http_live_streaming
[1.3] About the EXT-X-VERSION tag https://developer.apple.com/documentation/http_live_streaming/about_the_ext-x-version_tag

[3.1] Segmenting Video with ffmpeg – Part 1 http://hlsbook.net/segmenting-video-with-ffmpeg/
[3.2] Segmenting Video with ffmpeg – Part 2 http://hlsbook.net/segmenting-video-with-ffmpeg-part-2/

[4] https://www.ffmpeg.org/ffmpeg-formats.html#hls-1

[5] Apple introduces fragmented MP4 in HLS https://bitmovin.com/hls-news-wwdc-2016/

[6] https://github.com/Dash-Industry-Forum/dash.js/

[7.1] Android HLS video mime type
  https://stackoverflow.com/questions/9178321/android-hls-video-mime-type/11800566#11800566

## 实现和应用

浏览器方面，移动浏览器的 video/audio 标签都支持 HLS。但 PC 浏览器中，只有 Edge 和 WebKit 核心的浏览器（如 Safari、epiphany-browser）支持 HLS。
浏览器中，只有 Edge 支持 mpeg-dash，但支持 MSE 的浏览器可以通过 dash.js 来支持。主要浏览器里只有 iOS Safari 不支持 MSE。

## HLS
### 用 ffmpeg 生成内容

hls muxser[3.2, 4]:

```
ffmpeg -i all.ts -c copy -bsf:a aac_adtstoasc -f hls -hls_time 30 -hls_playlist_type vod -hls_segment_type fmp4 all-mp4.m3u8
ffmpeg -i all.ts -c copy -f hls -hls_time 20 -hls_playlist_type vod -hls_segment_type mpegts all.m3u8
```

用 `-hls_segment_type` 指定分段的格式。mpegts 是较为简单的。fmp4 会生成一个 init.mp4 文件和多个 .m4s 文件。后者是一个 mp4 分段（segment），不能独立播放。

### 播放列表文件 m3u8
[1.1-1.3]
例子：

```
EXTM3U
#EXT-X-VERSION:3
#EXT-X-TARGETDURATION:20
#EXT-X-MEDIA-SEQUENCE:0
#EXT-X-PLAYLIST-TYPE:VOD
#EXTINF:20.007978,
all0.ts
#EXTINF:19.984767,
all1.ts
#EXTINF:19.984767,
all2.ts
#EXT-X-ENDLIST
```
EXT-X-VERSION:3 是版本[1.3]。mpegts 分段用版本3即可，fmp4 分段用版本7。版本是向后兼容的。

EXT-X-PLAYLIST-TYPE:VOD 表示这是点播类型（内容固定，时长确定）。

EXT-X-TARGETDURATION 表示最长的分段的时长。EXTINF 表示下面的一个分段的时长。

播放列表文件的 mime 类型可以是 application/x-mpegURL 或者 application/vnd.apple.mpegurl。

注意 android 或者 chrome 有个 bug：HLS的URL里面要有 m3u8 字样才可以播放[7.1]。这个 bug 至少影响 android 4.4.4 到 6.0，以及微信浏览器（2019.4）。

### 分段不连续性问题

一般的HLS直播流里面，后续的 ts 片段的开始时间是跟着前面的片段结束时间的，但我们的音频总是从0开始，这样会导致android设备不兼容，vlc也不兼容。
在时间戳等不连续的地方，应该加上 #EXT-X-DISCONTINUITY 标签。例子：

```
#EXTM3U
#EXT-X-VERSION:3
#EXT-X-TARGETDURATION:111
#EXT-X-MEDIA-SEQUENCE:0
#EXT-X-PLAYLIST-TYPE:VOD
#EXTINF:19.984767,
all1.ts
#EXT-X-DISCONTINUITY
#EXTINF:20.007978,
all0.ts
#EXT-X-ENDLIST
```

用 `ffprobe -show_packets xxx.ts` 或者 `ffmpeg -i xxx.ts` 可以查看片段的时间戳。

## 分段的格式

分段的包装格式目前有 mpegts 和 fragmented mp4, 或者 fmp4。

### 非独立的 fmp4
fmp4 要求一个分段一个文件，并不是随意的，扩展名 `.m4s` ，不能独立播放，因此还需要一个“初始化元数据文件”，通常叫 `init.mp4` 。 

使用 fmp4 的 m3u8 的例子：

```
#EXTM3U
#EXT-X-VERSION:7
#EXT-X-TARGETDURATION:30
#EXT-X-MEDIA-SEQUENCE:0
#EXT-X-PLAYLIST-TYPE:VOD
#EXT-X-MAP:URI="init.mp4"
#EXTINF:30.000181,
all-mp40.m4s
#EXTINF:30.000181,
all-mp41.m4s
#EXT-X-ENDLIST
```

EXT-X-VERSION 只要应该是6。

fmp4 的意义在于可以与 mpeg-dash 协议共用同样的分段文件格式，从而节约存储成本并覆盖尽可能多的浏览器和客户端[5]。

### 独立的 fragmented mp4, ismv
HLS 中可以使用 mp4 分段，但要求是 fragmented mp4，即元数据附加于一个一个的分段，而不是集中于 mp4 文件的尾部。
ismv 就是 Smooth Streaming，分段的 mp4，用这个命令可以生成：

```
ffmpeg -i input -c copy -f ismv -frag_duration 2000000 output.mp4
```
`-frag_duration` 给定分段的时间长度，注意单位是微秒。
用 `MP4Box -info output.mp4` 可以确认是不是分段 mp4，ffmpeg 还不行。

不过，直接用整个的分段 mp4 文件作为的 HLS 的分段，android、chrome android 并不兼容，但 webkit 核心的浏览器是可以播放的（如 epiphany-browser）。

例子：

```
#EXTM3U
#EXT-X-TARGETDURATION:111
#EXT-X-VERSION:7
#EXT-X-MEDIA-SEQUENCE:0
#EXT-X-PLAYLIST-TYPE:VOD
#EXTINF:71.0,
1.m4a

#EXT-X-DISCONTINUITY
#EXTINF:111.0,
2.m4a
#EXT-X-ENDLIST
```

并不需要 init.mp4，各个分段可以独立播放。
