## spwan
### 标准输入输入输出的编码
默认编码都是二进制，即 'buffer' 以及 Buffer 类型。
通过 ChildProcess.std{in,out,error}.setEncoding() 可以改变编码。

### 标准输出的阻塞
如果子进程属于向标准输出写入数据然后退出的类型，而父进程没有读取标准输出，有两种可能：
（1）输出的数据量比较小，则子进程写完数据后即退出。
（2）输出的数据量比较大，则子进程被阻塞，无法退出。
所以为了保证子进程可以退出，父进程一定要读取标准输出。

### 进程创建成功/失败
#### Windows
创建成功，则返回 ChildProcess 对象，pid 是正常的;
正常退出时，ChildProcess 对象依次产生 exit、close 事件。

如果命令不存在：
spwan() 仍然会返回 ChildProcess 对象，但 pid 属性是 undefined；
产生 error 事件，内容是“spawn xxxx ENOENT”；
产生 close 事件，code:-4058, signal:null；
没有 exit 事件。

如果不是可执行文件：
spawn() 抛出异常（Error: spawn UNKNOWN）。
感觉这是个bug（7.4.0）。

如果发生链接错误（即依赖的dll不存在）：
创建成功，则返回 ChildProcess 对象，pid 是正常的;
ChildProcess 对象依次产生 exit、close 事件，code:3221225781, signal:null;
没有 error 事件。

因此要区分进程创建成功或失败：
* 捕获 spawn 的异常
* 看 pid 是否存在，存在就选创建成功。

### 父进程退出
如果 node.js 进程退出，则 spawn 创建的进程不一定会退出。
在控制台中，如果用 ctrl+C 退出 node.js 进程，则子进程（似乎）也会收到 ctrl+C，是否退出就要看其怎样处理 SIGINT 了。不通过控制台，直接给 node.js 进程发送 SIGINT ，就不会影响到子进程。

如果子进程不断地向标准输出写数据，而 node.js 读标准输出，那么如果父进程在子进程之前退出，则一般来说子进程会因为 SIGPIPE 信号退出。

systemd 默认的 KillMode 指令为 control-group，会杀死服务启动的所有子进程。
