## 参考资料
[5] Dual_EC_DRBG
https://en.wikipedia.org/wiki/Dual_EC_DRBG

[5.1] Dual_EC_DRBG: NSA 在 RSA 加密算法中安置后门是怎么一回事，有何影响？
https://www.zhihu.com/question/22343037

[6] Why do people advise against using RSA-4096?
https://www.gnupg.org/faq/gnupg-faq.html#please_use_ecc

[7] key exchange methode curve25519-sha256@libssh.org for SSH version 2 protocol.
https://git.libssh.org/projects/libssh.git/tree/doc/curve25519-sha256@libssh.org.txt#n4

[8] EdDSA + Ed25519
https://en.wikipedia.org/wiki/EdDSA

[9] Curve25519
https://en.wikipedia.org/wiki/Curve25519

[10] Nothing up my sleeve number
https://en.wikipedia.org/wiki/Nothing_up_my_sleeve_number

[11] Entropy Attacks!
http://blog.cr.yp.to/20140205-entropy.html

[13] Hash-based message authentication code
https://en.wikipedia.org/wiki/Hash-based_message_authentication_code

[14] UMAC
https://en.wikipedia.org/wiki/UMAC

[15] Universal_hashing
https://en.wikipedia.org/wiki/Universal_hashing

[16] UMAC: Fast and Provably Secure Message Authentication
http://fastcrypto.org/umac/

[20] Key exchange
https://en.wikipedia.org/wiki/Key_exchange

[21] Diffie–Hellman key exchange
https://en.wikipedia.org/wiki/Diffie%E2%80%93Hellman_key_exchange

[22] Elliptic curve Diffie–Hellman
https://en.wikipedia.org/wiki/Elliptic_curve_Diffie%E2%80%93Hellman

[23] Supersingular isogeny key exchange
https://en.wikipedia.org/wiki/Supersingular_isogeny_key_exchange

[24] Weak Diffie-Hellman and the Logjam Attack
https://weakdh.org/

[25] Imperfect Forward Secrecy: How Diffie-Hellman Fails in Practice
https://weakdh.org/imperfect-forward-secrecy-ccs15.pdf

[26] Guide to Deploying Diffie-Hellman for TLS
https://weakdh.org/sysadmin.html

[27] Another New AES Attack
https://www.schneier.com/blog/archives/2009/07/another_new_aes.html

[28] What's the purpose of DH Parameters?
https://security.stackexchange.com/questions/94390/whats-the-purpose-of-dh-parameters

[29] SSL/TLS & Perfect Forward Secrecy
https://vincent.bernat.im/en/blog/2011-ssl-perfect-forward-secrecy

[30] Deploying Forward Secrecy
https://blog.ivanristic.com/2013/06/ssl-labs-deploying-forward-secrecy.html

[40] ssl labs
https://www.ssllabs.com/index.html

[41] User Agent TLS Capabilities
https://www.ssllabs.com/ssltest/clients.html

[42] SSL and TLS Deployment Best Practices
https://github.com/ssllabs/research/wiki/SSL-and-TLS-Deployment-Best-Practices

[43] Which elliptic curve should I use?
https://security.stackexchange.com/questions/78621/which-elliptic-curve-should-i-use

[51] Node.Js TLS(SSL) HTTPS双向验证
http://blog.csdn.net/marujunyy/article/details/8477854

[59] config - OpenSSL CONF library configuration files
https://www.openssl.org/docs/man1.1.0/apps/config.html

[60] OpenSSL Certificate Authority
https://jamielinux.com/docs/openssl-certificate-authority/index.html

[61] What is a Pem file and how does it differ from other OpenSSL Generated Key File Formats?
https://serverfault.com/questions/9708/what-is-a-pem-file-and-how-does-it-differ-from-other-openssl-generated-key-file

[62] Extensions for SSL server certificate
https://security.stackexchange.com/questions/26647/extensions-for-ssl-server-certificate/26650#26650

[63] x509v3_config - X509 V3 certificate extension configuration format
https://www.openssl.org/docs/man1.1.0/apps/x509v3_config.html

[64] Which key usages are required by each key exchange method?
https://security.stackexchange.com/questions/24106/which-key-usages-are-required-by-each-key-exchange-method/24107#24107

[65] Which properties of a X.509 certificate should be critical and which not?
https://security.stackexchange.com/questions/30974/which-properties-of-a-x-509-certificate-should-be-critical-and-which-not

[71] Online Certificate Status Protocol
https://en.wikipedia.org/wiki/Online_Certificate_Status_Protocol

[72] Online Certificate Status Protocol
https://jamielinux.com/docs/openssl-certificate-authority/online-certificate-status-protocol.html

[73] OCSP stapling
https://en.wikipedia.org/wiki/OCSP_stapling

[80] Crypto AG 沈逸：你永远不知道，你是否在美国面前“裸奔”
https://www.guancha.cn/ShenYi/2020_02_19_537095_1.shtml

## 加密算法
### AES
#### 弱点和攻击
根据 [27]，AES-256 实际上比 AES-128 更不安全，所以如果使用 AES，应使用 AES-128。目前没有已知的AES-128的弱点。

### 椭圆曲线
实际应用的椭圆曲线有多种，如 TLS 1.2 标准中采用的 secp256r1, secp384r1，以及 TLS 1.3 标准中增加的 Curve25519 和 Curve448 [3]。

#### 弱点和攻击
NSA可能在它建立的椭圆曲线常数中植入后门[10]，包括 secp256r1, secp384r1等；Curve25519，Curve448没有这个问题，因为不使用不能说明来源的常数[7]。

### RSA
很多 RSA 应用程序默认采用 2048 位 RSA 密钥。随着密钥位数的进一步增长，RSA 的安全性只是缓慢地增长，因此有些人认为，如果需要显著优于 2048 位 RSA的安全性，
应该改用椭圆曲线算法[6]。

## 随机数算法
[11] 认为，从多个来源采集随机性不一定无害，特别是其中一个被攻击者控制时。

## 数字签名
### 数字签名算法的选择
有2类选择：
* DSA 和 RSA。都是基于分解质因数的算法。
* ECDSA 和 Ed25519[8]。都是基于椭圆曲线离散对数的算法。

其中 DSA 已经被发现明显的弱点，因此不应该再使用。Ed25519 可能是最安全的，但是因为比较新，兼容性较差。
ECDSA 被怀疑有弱点：NSA可能在它建立的椭圆曲线常数中植入后门[10]；Ed25519没有这个问题，因为不使用硬编码的常数[7]。

注意，有些地方（如TLS标准）将 Ed25519 称为为 ECDSA 的一种，这就需要具体了解其使用的曲线。

很多 RSA 应用程序默认采用 2048 位 RSA 密钥。随着密钥位数的进一步增长，RSA 的安全性只是缓慢地增长，因此有些人认为，如果需要显著优于 2048 位 RSA的安全性，
应该改用椭圆曲线算法[6]。

## 消息鉴证码
### HMAC
注意，hmac 比其使用的散列算法本身安全性要高，因此 HMAC-MD5 和 HMAC-SHA1 仍然可以使用[13]。

### UMAC
[14-16]
UMAC is a message authentication code : an algorithm for generating authentication tags, which are used to ensure the authenticity and integrity of transmitted messages[16]. 

## 密钥交换算法

Diffie–Hellman 使用一对参数，称为 Diffie-Hellman parameters。这对参数可以公开，也不必经常更换，但为了对抗 Logjam 攻击，参数长度要长，且不要所有的服务器都用同样的参数[28]。
常用的 1024 位质数参数是可以被国家级组织破解的；2048位质数应该还是安全的[24,25]。
对于 TLS，DH参数只要放在服务器上即可。

基于椭圆曲线的密钥交换算法 Elliptic-Curve Diffie-Hellman (ECDHE) 暂时没有发现弱点（前提是不使用NIST推荐的曲线，而是使用类似Curve25519的曲线）。

RSA 作为密钥交换算法不支持完全前向保密（forward security）[42]（DHE和ECDHE支持），因此不应该被继续使用。


## 密码术常用的文件格式
参考 [61]：

  .csr This is a Certificate Signing Request. Some applications can generate these for submission to certificate-authorities. The actual format is PKCS10 which is defined in RFC 2986. It includes some/all of the key details of the requested certificate such as subject, organization, state, whatnot, as well as the public key of the certificate to get signed. These get signed by the CA and a certificate is returned. The returned certificate is the public certificate (which includes the public key but not the private key), which itself can be in a couple of formats.

  .pem Defined in RFC's 1421 through 1424, this is a container format that may include just the public certificate (such as with Apache installs, and CA certificate files /etc/ssl/certs), or may include an entire certificate chain including public key, private key, and root certificates. Confusingly, it may also encode a CSR (e.g. as used here) as the PKCS10 format can be translated into PEM. The name is from Privacy Enhanced Mail (PEM), a failed method for secure email but the container format it used lives on, and is a base64 translation of the x509 ASN.1 keys.

  .key This is a PEM formatted file containing just the private-key of a specific certificate and is merely a conventional name and not a standardized one. In Apache installs, this frequently resides in /etc/ssl/private. The rights on these files are very important, and some programs will refuse to load these certificates if they are set wrong.

  .pkcs12 .pfx .p12 Originally defined by RSA in the Public-Key Cryptography Standards, the "12" variant was enhanced by Microsoft. This is a passworded container format that contains both public and private certificate pairs. Unlike .pem files, this container is fully encrypted. Openssl can turn this into a .pem file with both public and private keys: openssl pkcs12 -in file-to-convert.p12 -out converted-file.pem -nodes

A few other formats that show up from time to time:

  .der A way to encode ASN.1 syntax in binary, a .pem file is just a Base64 encoded .der file. OpenSSL can convert these to .pem (openssl x509 -inform der -in to-convert.der -out converted.pem). Windows sees these as Certificate files. By default, Windows will export certificates as .DER formatted files with a different extension. Like...

  .cert .cer .crt A .pem (or rarely .der) formatted file with a different extension, one that is recognized by Windows Explorer as a certificate, which .pem is not.

  .p7b Defined in RFC 2315, this is a format used by windows for certificate interchange. Java understands these natively. Unlike .pem style certificates, this format has a defined way to include certification-path certificates.

  .crl A certificate revocation list. Certificate Authorities produce these as a way to de-authorize certificates before expiration. You can sometimes download them from CA websites.

In summary, there are four different ways to present certificates and their components:

  PEM Governed by RFCs, it's used preferentially by open-source software. It can have a variety of extensions (.pem, .key, .cer, .cert, .crt, more)

  PKCS7 An open standard used by Java and supported by Windows. Does not contain private key material.

  PKCS12 A private standard that provides enhanced security versus the plain-text PEM format. This can contain private key material. It's used preferentially by Windows systems, and can be freely converted to PEM format through use of openssl.

  DER The parent format of PEM. It's useful to think of it as a binary version of the base64-encoded PEM file. Not routinely used by much outside of Windows.

## TLS
### 密码算法
TLS 使用的密码算法通常用这样的字符串来表示：
  `TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256`
意思是：
```
  密钥交换：ECDHE
  认证（证书）：RSA
  内容加密：AES-128-GCM
  消息鉴证：HMAC-SHA256
```
### 部署要点
参考[42]。
* 使用 2048 位 RSA 证书或者 ECDSA 证书。大于 2048 位的证书计算量太大，安全性却增加不多。
  注：使用 ECDSA 的话，还是等 TLS 1.3 加入更安全的椭圆曲线比较好？
* 证书有效期不应太长，因为吊销证书并不总是可靠的。1年比较合适。更新证书时，最好也同时更新私钥。
* 证书应覆盖足够多的主机名/域名。
* 证书的签名算法应可靠。SHA-1 正在退役。
* 将全部中间证书（从你的服务器证书到CA）也部署在服务器上。
* 不使用 TLS 压缩，因为会严重降低安全性。HTTP压缩是允许的。

### 版本和特性
TLS 1.2

TLS 1.3
增加了椭圆曲线 Curve25519 和 Curve448 [3]。

### 前向安全
[29,30,25]

前向安全的意思是，如果攻击者记录了过去的加密通信，然后获得了私钥，他也无法解密过去的通信。
简单地用 RSA 交换一个对称秘钥是无法做到前向安全的。前向安全要求采用另一种秘钥交换机制，例如DHE，此时，RSA仅用于身份验证（authentication）。

### 软件对 TLS 的支持情况
参考[41,43]。

#### Web 服务器
待补充

#### 浏览器
* Firefox 52
```
    协议：TLS 1.2
    签名：ECDSA(x25519, secp256r1, secp384r1, secp521r1), RSA
    密钥交换：ECDHE(包括Curve25519?), RSA
    对称加密：chacha20-poly1305, AES
    消息鉴证：sha2
```

* Chrome 51
```
    协议：TLS 1.2
    签名：ECDSA(x25519, secp256r1, secp384r1, secp521r1), RSA
    密钥交换：ECDHE(包括Curve25519?), RSA
    对称加密：chacha20-poly1305, AES
    消息鉴证：sha2
```

* IE11, EDGE13
```
    协议：TLS 1.2
    签名：ECDSA(secp256r1, secp384r1), RSA, DSA
    密钥交换：ECDHE, DHE, RSA
    对称加密：AES
    消息鉴证：sha2
```

* OpenSSL
```
    1.0.1h
    协议：TLS 1.2
    签名：ECDSA(secp256k1, secp256r1..., 不支持Curve25519), RSA, DSA
    密钥交换：ECDHE, DHE, RSA
    对称加密：AES
    消息鉴证：sha2
```

* SSH
```
  签名：Ed25519, ECDSA, RSA
  密钥交换：DHE, ECDHE(包括Curve25519), RSA
  对称加密：chacha20-poly1305, AES
  消息鉴证：hmac-sha2-(etm),umac-(etm)
```

## 用 openssl 生成 TLS 密钥和证书

### 基础知识
数字证书一般来说包括被数字签名的公钥以及其它信息。

openssl 生成的证书可以用于 https 协议，以及 openvpn 。openssh 则用它自己的证书格式（用 ssh-keygen 命令）。

### 命令和手册

可以用 man openssl 来查看手册。openssl 有很多子命令，可以用 `man <sub command>` 来查看手册。子命令包括：

```
asn1parse(1)
  解析证书和秘钥文件。

ca(1)
config(5), crl(1), crl2pkcs7(1), dgst(1), dhparam(1), dsa(1), dsaparam(1), enc(1), engine(1), gendsa(1), genpkey(1),
genrsa(1), nseq(1), openssl(1), passwd(1), pkcs12(1), pkcs7(1), pkcs8(1), rand(1), 
req(1), rsa(1), rsautl(1), s_client(1), s_server(1),
s_time(1), smime(1), spkac(1), verify(1), version(1), x509(1), crypto(7), ssl(7), x509v3_config(5)
```

### 生成私钥
```
openssl genpkey -algorithm RSA -out key.pem -aes-128-cbc -pass pass:hello -pkeyopt rsa_keygen_bits:2048
```

  生成 2048 位 RSA 私钥，输出到 key.pem 文件，用 -aes-128-cbc 和口令 "hello" 加密。去掉 -aes-128-cbc -pass pass:hello 则不加密。

关于 genrsa/genpkey 子命令可以查看 `man genrsa` 和 `man genpkey`。

语法：

  ```
  openssl genpkey [-out filename] [-outform PEM|DER] [-pass arg] [-cipher] [-engine id] [-paramfile file]
       [-algorithm alg] [-pkeyopt opt:value] [-genparam] [-text]
  ```

参数：

  ```
   -cipher      This option encrypts the private key with the supplied cipher. des,des3,idea 等
   -algorithm alg
           public key algorithm to use such as RSA, DSA or DH. If used this option must precede any -pkeyopt
           options.
   -pkeyopt opt:value  算法参数。可能的参数：rsa_keygen_bits:numbits  dsa_paramgen_bits:numbits dh_paramgen_prime_len:numbits
          dh_paramgen_generator:value  ec_paramgen_curve:curve
          
    -pass pass:password 直接指定口令为 password
    -pass stdin 从标准输入读取口令。直接运行会让口令显示在控制台上，不是很安全，但可以用管道来让其它程序输入口令。
    -pass file:pathname 从文件读取口令。如果有多行，一般是第一行有效。
    -cipher 是 -aes-128-cbc|-des|-des3|-idea 。注意指定 -pass 的同时必须指定加密算法，否则不会加密。
    仅指定加密算法 -cipher，不指定 -pass 是可以的，这时采用控制台交互输入口令。
  ```

### 关于 openssl 的椭圆曲线

目前 openssl 支持 Elliptic Curve Diffie Hellman (ECDH) 用于密钥交换，Elliptic Curve Digital Signature Algorithm (ECDSA)
用于签名。

列出支持的曲线：
```
openssl ecparam -list_curves
```

用 secp256k1 曲线生成私钥，输出到 secp256k1-key.pem：

```
openssl ecparam -name secp256k1 -genkey -noout -out secp256k1-key.pem
```

### 生成自签名CA证书
参考[51,60,71,72]。

  ```
  openssl genpkey -algorithm RSA -out ca_key.pem -pkeyopt rsa_keygen_bits:2048
  openssl req -batch -new -key ca_key.pem -config openssl_ca.cnf -out ca_csr.pem  
  openssl x509 -req -days 1000 -in ca_csr.pem -signkey ca_key.pem -out ca_cert.pem -extensions v3_req -extfile openssl_ca.cnf 
  ```

 执行上述3个命令之后，得到3个文件，其中“ca_key.pem”为CA的私钥，“ca_cert.pem”为CA的自签名证书。有了CA证书和私钥之后就可以使用它来签发服务器及客户端的证书了。openssl_ca.cnf 如下（配置文件格式见[59.63]）：

```
[req]
prompt = no
distinguished_name = req_distinguished_name
req_extensions = v3_req

[req_distinguished_name]

# Country Name (2 letter code)
C = CN
# State or Province Name (full name)
ST = BeiJing
# Locality Name (eg, city)
L = BeiJing
# Organizational Name (eg, company)
O = Mainbo
# Organizational Unit Name (eg, section)
OU = uclass-cache CA
# Common Name
CN = uclass-cache CA
emailAddress=yunwei.list@mainbo.com

[v3_req]
# see https://www.openssl.org/docs/man1.1.0/apps/x509v3_config.html
basicConstraints = critical, CA:TRUE
keyUsage = critical, keyCertSign, cRLSign

```

critical 的意思是证书的接收者如果不认识后面的值，则应该把证书视为无效[65]。
CA:TRUE 是 CA 的证书必需的。
keyUsage 表示此证书的私钥可以用来做什么。keyCertSign, cRLSign 是 CA 证书必需的。

### 生成CA签名的网站证书

  ```
  openssl genpkey -algorithm RSA -out https_server_key.pem -pkeyopt rsa_keygen_bits:2048
  openssl req -new -key https_server_key.pem -config openssl_https_server.cnf -out https_server_csr.pem  
  openssl x509 -req -days 365 -CA ca_cert.pem -CAkey ca_key.pem -CAcreateserial -in https_server_csr.pem -out https_server_cert.pem -extensions v3_req -extfile openssl_https_server.cnf
  ```

其中 openssl.cnf 是网站证书的配置文件（配置文件格式见[59.63]），内容如下

```
[req]
prompt = no
distinguished_name = req_distinguished_name
req_extensions = v3_req

[req_distinguished_name]

# Country Name (2 letter code)
C = CN
# State or Province Name (full name)
ST = BeiJing
# Locality Name (eg, city)
L = BeiJing
# Organizational Name (eg, company)
O = Mainbo
# Organizational Unit Name (eg, section)
OU = uclass-cache
# Common Name
CN = uclass-cache node
emailAddress=yunwei.list@mainbo.com

[v3_req]
# see https://www.openssl.org/docs/man1.1.0/apps/x509v3_config.html
basicConstraints = critical, CA:FALSE
keyUsage = critical, nonRepudiation, digitalSignature, keyEncipherment, dataEncipherment, keyAgreement
extendedKeyUsage = serverAuth
subjectAltName = @alt_names

[alt_names]
DNS.1 = uclass-cache
DNS.2 = uclass-cache.local
IP.1 = 127.0.0.1
IP.2 = ::1

```

“req_distinguished_name”部分的内容就是证书的主体内容，包括证书持有者的相关信息。重点是“alt_name”部分的内容，其中的“DNS.1”所指定的是使用该证书的主机所使用的DNS，还有“IP.1”所指定的是使用该证书的主机的IP地址，这里可以指定多个DNS和IP，方便有多DNS和多IP的主机能够使用同一个该证书来完成验证。

对于 TLS 服务器证书， `extendedKeyUsage = serverAuth` 是必需的。
keyEncipherment 和 digitalSignature 通常也是要有的，但根据 TLS 密码套件的配置，并不都用得上。keyAgreement, nonRepudiation 和 dataEncipherment 几乎用不上[64]。
不要加入 CA 才需要的 keyUsage 值（keyCertSign, cRLSign）。

### 生成CA签名的客户端证书
  ```
  openssl genpkey -algorithm RSA -out https_client_key.pem -pkeyopt rsa_keygen_bits:2048  
  openssl req -batch -new -key https_client_key.pem -config openssl_https_client.cnf -out https_client_csr.pem  
  openssl x509 -req -days 3 -CA ca_cert.pem -CAkey ca_key.pem -CAcreateserial -in https_client_csr.pem -out https_client_cert.pem  -extensions v3_req -extfile openssl_https_client.cnf
  ```
生成 pkcs12 文件（包括了私钥）：

  ```
  openssl pkcs12 -export -password pass: -in https_client_cert.pem -inkey https_client_key.pem -certfile ca_cert.pem -out https_client.p12
  ```
openssl_https_client.cnf 如下（配置文件格式见[59.63]）：

```
[req]
prompt = no
distinguished_name = req_distinguished_name
req_extensions = v3_req

[req_distinguished_name]

# Country Name (2 letter code)
C = CN
# State or Province Name (full name)
ST = BeiJing
# Locality Name (eg, city)
L = BeiJing
# Organizational Name (eg, company)
O = Mainbo
# Organizational Unit Name (eg, section)
OU = developement
# Common Name
CN = uclass-cache administrator (temporal)
emailAddress=yunwei.list@mainbo.com

[v3_req]
# see https://www.openssl.org/docs/man1.1.0/apps/x509v3_config.html
basicConstraints = critical, CA:FALSE
keyUsage = critical, nonRepudiation, digitalSignature, keyEncipherment, dataEncipherment, keyAgreement
extendedKeyUsage = clientAuth
```

### 生成 pkcs12 文件
  ```
  openssl pkcs12 -export -password pass:foo -in https_server_cert.pem -inkey https_server_key.pem -certfile ca_cert.pem -out https_server.p12
  openssl pkcs12 -export -password pass:foo -in https_client_cert.pem -inkey https_client_key.pem -certfile ca_cert.pem -out https_client.p12 
  ```

-password pass:foo 指定口令为"foo"，口令可以为空（写作 -password pass:）。
p12 文件可以导入浏览器（具体方法见 Web Security_Crypto_CSP.txt）。
  
注意，-certfile 只能用一次，这样一个 p12 中的证书数量被限制到了 2 个。要添加更多的证书，就必须现将多个证书文件（pem 格式）合并（简单的文件合并即可），然后用 -in 添加，例如：

  ```
  cat client.cert ca1.cert root_ca.cert > chain.cert

  openssl pkcs12 -export -password pass: -in chain.cert -inkey c.key.pem -out client.p12
  ```

这个例子合并了整个证书链的3个证书到 chain.cert 中，其顺序最好按证书链的顺序从低到高来。

### 查看证书信息

  ```
  openssl x509 -noout -text -in cert/ca_cert.pem
  openssl pkcs12 -in client.p12
  ```

### CRL 和 OCSP
参考[71,72,73]。

通常每个证书应附有 CRL 或 OCSP 的 URL，让浏览器能够查询证书的吊销状态。
在证书配置文件中的 [v3_req] 下加上：
```
crlDistributionPoints = URI:http://example.com/intermediate.crl
```
或者
```
authorityInfoAccess = OCSP;URI:http://ocsp.example.com
```
一般来说，OCSP 信息放在 CA 所属的网站上，并由 CA 或 CA 的下级证书签名。

OCSP 存在一些问题：
（1）将用户的浏览行为暴露给了提供 OCSP 的网站。
（2）如果 OCSP 网站暂时不可访问（有可能是被攻击者阻断，也有可能是普通故障），则客户端处于两难境地：要么忽略错误，认为证书继续有效，要么认为证书无效。
  一般浏览器会忽略错误。
（3）提供 OCSP 的网站可能遇到很高的访问量。

OCSP stapling 可以解决上述问题[13]，它利用证书持有者的服务器作为中介来传输 OCSP 信息，并且 OCSP 信息有时间戳。

## let's encrypt

安装 certbot

```
sudo apt install certbot
```
或者通过 snap
```
sudo dnf install epel-release
sudo dnf upgrade

sudo yum install snapd
sudo systemctl enable --now snapd.socket
sudo ln -s /var/lib/snapd/snap /snap
 
sudo snap install core
sudo snap refresh core
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot
```

初次获得证书

```
[root@label-1 cvat-1]# /var/lib/snapd/snap/bin/certbot --nginx

Saving debug log to /var/log/letsencrypt/letsencrypt.log
Enter email address (used for urgent renewal and security notices)
 (Enter 'c' to cancel): duanyao@chengzhangguiji.cn
Please enter the domain name(s) you would like on your certificate (comma and/or
space separated) (Enter 'c' to cancel): label-1.ai-parents.cn

Successfully received certificate.
Certificate is saved at: /etc/letsencrypt/live/label-1.ai-parents.cn/fullchain.pem
Key is saved at:         /etc/letsencrypt/live/label-1.ai-parents.cn/privkey.pem
This certificate expires on 2021-11-18.
These files will be updated when the certificate renews.
Certbot has set up a scheduled task to automatically renew this certificate in the background.

Deploying certificate
Successfully deployed certificate for label-1.ai-parents.cn to /etc/nginx/nginx.conf
Congratulations! You have successfully enabled HTTPS on https://label-1.ai-parents.cn
```

证书保存在本机的 /etc/letsencrypt/ 目录下。

注意要输入本机的域名（ label-1.ai-parents.cn ），通配符域名无法直接被验证。
完成后可访问 https://label-1.ai-parents.cn 测试。之后可以杀死 nginx 进程。

手动更新证书：

```
certbot renew --dry-run
certbot renew
```
手动更新证书时 certbot 会启动它自带的 nginx，需要监听 80 端口，所以得把其它占用80端口的程序停下。

一般来说无需手动更新证书，使用证书的http服务程序一般会自动更新证书，大约90天更新一次。如果需要手动更新，请查询你使用的http服务程序的手册。

例如，对于 traefik 服务器，应当删除或改名包含证书的 /letsencrypt/acme.json 文件（具体路径要看配置），然后重启 traefik 服务器，这样就强制它更新了证书。
