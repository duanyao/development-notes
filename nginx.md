## 参考资料

[2.1] How to configure Multiple Domains with Nginx on Ubuntu
  https://www.serverlab.ca/tutorials/linux/web-servers-linux/how-to-configure-multiple-domains-with-nginx-on-ubuntu/

[3.1] A Regular Expression Tester for NGINX and NGINX Plus
  https://www.nginx.com/blog/regular-expression-tester-nginx/

[4.1] How To Create Temporary and Permanent Redirects with Nginx
  https://www.digitalocean.com/community/tutorials/how-to-create-temporary-and-permanent-redirects-with-nginx
  
[4.2] Can nginx location blocks match a URL query string?
  https://serverfault.com/questions/811912/can-nginx-location-blocks-match-a-url-query-string

[4.3] If is Evil... when used in location context
  https://www.nginx.com/resources/wiki/start/topics/depth/ifisevil/

[4.4] Module ngx_http_rewrite_module
  http://nginx.org/en/docs/http/ngx_http_rewrite_module.html
  
[5.1] ngx_stream_proxy_module
  https://nginx.org/en/docs/stream/ngx_stream_proxy_module.html

[5.2] TCP and UDP Load Balancing
  https://docs.nginx.com/nginx/admin-guide/load-balancer/tcp-udp-load-balancer/
  
[9.1] A debugging log
  http://nginx.org/en/docs/debugging_log.html?_ga=2.118696010.2123305638.1503935126-1169033168.1503935126

[10.1] Nginx Optimization – The Definitive Guide
  https://www.scalescale.com/tips/nginx/nginx-optimization-the-definitive-guide/#

## 配置样例
```
#user  nobody;
worker_processes  1;

error_log  logs/error.log  debug;

#pid        logs/nginx.pid;

events {
    worker_connections  1024;
}


http {
    include       mime.types;
    default_type  application/octet-stream;

    #log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
    #                  '$status $body_bytes_sent "$http_referer" '
    #                  '"$http_user_agent" "$http_x_forwarded_for"';

    #access_log  logs/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    #keepalive_timeout  0;
    keepalive_timeout  65;
    
    send_timeout 30;

    #gzip  on;

    server {
        listen       4080;
        server_name  localhost;

        #charset koi8-r;

        #access_log  logs/host.access.log  main;

        location / {
            root   html;
            index  index.html index.htm;
        }

        #error_page  404              /404.html;

        # redirect server error pages to the static page /50x.html
        #
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }

        # deny access to .htaccess files, if Apache's document root
        # concurs with nginx's one
        #
        #location ~ /\.ht {
        #    deny  all;
        #}
    }
}
```

## 多域名配置
[2.1]

```
http {
  server {
    listen       80;
    listen       [::]:80;
    server_name  ai-parents.cn;

    location / {
      root         /var/www/aiparents-static;
    }
  }
  server {
    listen       80;
    listen       [::]:80;
    server_name  test1.ai-parents.cn test2.ai-parents.cn;
    
    location / {
      root         /var/www/aiparents-static-test;
    }
  }
  server {
    listen       80 default_server;
    listen       [::]:80 default_server;
    server_name  _;
    return 403; # 403 forbidden
  }
}
```
多个 server 块可以监听同样的端口。请求由哪个 server 块处理，由 server_name 与请求中的 host 头是否匹配来决定。
如果没有匹配 server_name 的 server 块，则用标记了 `listen <port> default_server` 的块来处理。如果没有一个 server 块有 default_server 标记，则将第一个 server 块视为有 default_server 标记。

一般来说，有多个 server 块时，应该留出一个专门的作为 default_server ，这个块的 server_name 则不重要。

## location 的匹配
[3.1]
```
# 路径的前缀匹配
location /a/b { } 

# 路径的正则表达式匹配，区分大小写
location ~ ^/a/b$ { }

# 路径的正则表达式匹配，不区分大小写
location ~* ^/a/b$ { }  
```

在 nginx 匹配 location 时，仅匹配 url 的路径部分，参数部分已经被去掉了。所以，不可能用 location 指令去匹配参数部分。
nginx 的正则表达式规则与 perl 类似。不过，'/' 不需要转义为 '\/'，但转义也不算错。

location 的匹配是自上而下的，第一个匹配的生效。

## 重写/重定向
[4.1, 4.4]

location 块中有多种“重写”指令[4.1-4.4]。

```
# 向客户端发送 http 302 重定向。$1 表示正则表达式匹配到的第一个圆括号里的内容。
rewrite ^/(.*)$ http://www.domain2.com/$1 redirect;

# 向客户端发送 http 301 重定向。
rewrite ^/(.*)$ http://domain2.com/$1 permanent;

# 向客户端发送 http 403 响应
return 403;

# 向客户端发送 http 302 重定向。$request_uri 变量表示请求 url 的路径以及参数
return 302 http://test2.ai-parents.cn$request_uri;
```

## HTTP 反向代理
```
server {
    listen 80;
    server_name example.com;

    location /api {
        proxy_pass http://backend-server:8080/api/;  # 将请求转发到后端服务器，并保持路径不变
    }

    location /new-path {
        proxy_pass http://backend-server:8080/api/;  # 将请求转发到后端服务器，并修改路径

        # 以下都是可选的，看 backend-server 是否需要。
        proxy_set_header X-Original-URI $request_uri;  # 可选：将原始请求路径发送给后端服务器。$request_uri == /new-path... 。
        proxy_set_header Host $host;  # 发送正确的 Host 头
        proxy_set_header X-Real-IP $remote_addr;  # 发送真实的客户端 IP
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;  # 发送 X-Forwarded-For 头
        proxy_set_header X-Forwarded-Proto $scheme;  # 发送 X-Forwarded-Proto 头
    }
}
```

## TCP/UDP 反向代理
[5.1-5.2]
```
stream {
  server {
      listen 127.0.0.1:12345;
      proxy_pass 127.0.0.1:8080;
  }

  server {
      listen 12345;
      proxy_connect_timeout 1s;
      proxy_timeout 1m;
      proxy_pass example.com:12345;
  }

  server {
      listen 53 udp reuseport;
      proxy_timeout 20s;
      proxy_pass dns.example.com:53;
  }

  server {
      listen [::1]:12345;
      proxy_pass unix:/tmp/stream.socket;
  }
}
```
TCP/UDP 反向代理不解析流量，因此可以转发任意协议的流量，包括 TLS、https 等。但为了实现多个虚拟主机，就需要多个 IP 地址。

## 检测 url 参数，if
[4.2, 4.3]
```
location @tomcat_test {
  proxy_pass   http://127.0.0.1:9080;
}

location /aiparents_ {
  error_page 449 = @tomcat_test;
  if ($query_string ~ test_mode) {
    return 449;
  }
  proxy_pass   http://127.0.0.1:8080;
}
```

```
location /aiparents_ {
  if ($query_string ~ test_mode) {
    return 302 http://test2.ai-parents.cn$request_uri;
  }
  proxy_pass   http://127.0.0.1:8080;
}
```

```
location /aiparents_ {
  if ($query_string ~ test_mode) {
    proxy_pass   http://127.0.0.1:9080;
  }
  proxy_pass   http://127.0.0.1:8080;
}
```

nginx 只有 if 指令，没有 else 指令。if 指令的块内，只能使用重写类指令（return, rewrite等）[4.4]，否则可能有奇怪的结果[4.3]。
if 指令的条件块中，可以用 `value ~ regex` 的方式用正则表达式测试。`$query_string` 表示 url 参数部分。

## 超时 timeout

client_body_timeout 12;
client_header_timeout 12;
keepalive_timeout 15;
send_timeout 10;

client_body_timeout Directive sets the read timeout for the request body from client. The timeout is set only if a body is not get in one readstep.

client_header_timeout Specifies how long to wait for the client to send a request header (e.g.: GET / HTTP/1.1).

keepalive_timeout The first parameter assigns the timeout for keep-alive connections with the client. The server will close connections after this time.

send_timeout Specifies the response timeout to the client. This timeout does not apply to the entire transfer but, rather, only between two subsequent client-read operations

注意 send_timeout + sendfile 的一个行为：超时达到时，error_log 中会记录立即终结了请求：

  client timed out (110: Connection timed out)
  http close request
但实际上，TCP 连接没有立即被关闭，如果稍后客户端恢复接收，会继续接收一些数据，然后才关闭连接。这使得客户端比较难得知服务器端的超时设置。
apache web 服务器也有类似的特性，这可能与 linux sendfile 的实现有关。

以下是 nginx 作为反向代理时，访问上游服务器的超时设置：
proxy_connect_timeout 600;
proxy_read_timeout 600;
proxy_send_timeout 600;

## 用 supervisor 管理 nginx

在 supervisor 的配置文件中添加一段：
```
[program:nginx_server]
command=/usr/bin/nginx -g "daemon off;" -c /path/to/your/nginx.conf
stderr_logfile=/path/to/your/error.log
stdout_logfile=/path/to/your/access.log
```

`daemon off;` 让 nginx 在前台运行，这是 supervisor 需要的。这个配置也可以写在配置文件 `nginx.conf` 中。
`-c /path/to/your/nginx.conf` 可以指定非标准的配置文件路径。
