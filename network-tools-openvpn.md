## 参考资料
[1] openvpn
  https://openvpn.net/
  
[2] Manuals
  https://community.openvpn.net/openvpn/wiki/Openvpn24ManPage

[3] How To Assign Static IP Addresses to OpenVPN Clients In pfSense
https://www.iceflatline.com/2014/01/how-to-assign-static-ip-addresses-to-openvpn-clients-in-pfsense/

[4] OpenVPN: Set a static IP Address for a client
http://michlstechblog.info/blog/openvpn-set-a-static-ip-address-for-a-client/

[5] OpenVPN简易文档
http://blog.csdn.net/dog250/article/details/5626255

[6] Openvpn的搭建
https://my.oschina.net/guol/blog/39924

[7] How To Set Up an OpenVPN Server on Ubuntu 16.04
https://www.digitalocean.com/community/tutorials/how-to-set-up-an-openvpn-server-on-ubuntu-16-04
  
[11] How to install OpenVPN Server and Client on CentOS 7
https://www.unixmen.com/install-openvpn-centos-7/
http://www.tuicool.com/articles/ymQbMja

[15] https://serverfault.com/questions/21399/how-do-you-avoid-network-conflict-with-vpn-internal-networks

[16] https://books.google.com.hk/books?id=_1MoDwAAQBAJ&pg=PA58&lpg=PA58

[21] OpenVPN的route命令实现选择性路由
http://ntcn.net/blog/?p=6360

[22] how-to-refuse-routes-that-are-pushed-by-openvpn-server
https://unix.stackexchange.com/questions/16500/how-to-refuse-routes-that-are-pushed-by-openvpn-server

## 安装
### centos 7

```
yum install epel-release
yum install openvpn -y
```

## 一般配置
[5]。

### 加密系统
不论客户端还是服务器端，都需要在配置文件中指定 TLS 的证书和私钥，具体包括
* ca

  CA 证书文件例如 ca openvpn_ca.crt 如果 CA 链条多于一节，则应当把多个 CA 证书合并。

* cert

  服务器或客户端自身的证书，例如 cert openvpn_client.crt

* key

  服务器或客户端自身的证书对应的私钥，如 key openvpn_client.key
  
服务器端可能还需要 Diffie hellman 参数文件，例如 
```
dh dh2048.pem
```
可以这样生成：`openssl dhparam -out dh2048.pem 2048`


## 客户端

### 直接命令运行
以 root 运行[2]：

```
openvpn —cd /etc/openvpn —config client1.ovpn
```

### systemd + linux
适用于 ubuntu， centos 7 等。

在 /etc/openvpn 中建立配置文件，命名为 `xxx.conf` （xxx 替换为任意名字），执行命令：

```
sudo systemd enable openvpn@xxx
sudo systemd start openvpn@xxx
```

### 配置文件
配置文件中的选项与命令行参数是可互换的，详见[2]。

```
client # client 表示客户端配置，不写表示服务器端
remote openvpn.mainbo.com 1194
dev tun #说明连接方式是点对点的连接，如要以以太网的方式则可以将tun修改为tap
proto tcp
resolv-retry infinite
nobind
persist-key
persist-tun
ca openvpn_ca.crt
cert openvpn_client.crt
key openvpn_client.key
ns-cert-type server
comp-lzo
route-delay 2
route-method exe
#route-nopull
#route 10.8.0.0 255.255.255.0 vpn_gateway
#route 10.10.3.0 255.255.255.0 vpn_gateway
verb 3
```

### 路由设置
[21-22]

其实，如果使用OpenVPN类型的VPN的话，那么有个很简单的方法，可以仅让指定的IP范围内的网络请求通过VPN发送，而正常的访问则不会占用VPN一丝流量。

首先，OpenVPN的配置文件，支持一个参数route-nopull，可以使VPN连接后，并不修改默认路由，也就不会有任何网络请求走VPN。

其次，我们可以在配置文件的末尾，添加一些route命令，指定特定的IP范围请求通过VPN发送。比如：
  ```
  # NTT Twitter
  route 168.143.0.0 255.255.0.0 vpn_gateway
  route 128.121.0.0 255.255.0.0 vpn_gateway
  ```
上面的一行命令可以分成三部分来理解：
```
route        168.143.0.0 255.255.0.0        vpn_gateway
命令名称       网络范围（IP段）                 指定通过的路由
```

此外，也可以在 openvpn 服务器上配置路由，这样就不用再每台客户机上设置了。

或者过滤掉部分服务器 route 指令[22]：
  ```
  pull-filter accept "route 192.168."

  pull-filter ignore "route 172."

  pull-filter accept "route 1"

  pull-filter ignore "route "
  ```

## 服务器端

样例文件：https://github.com/OpenVPN/openvpn/blob/master/sample/sample-config-files/server.conf


```
port 1194

# TCP or UDP server?
proto tcp
;proto udp

# "dev tun" will create a routed IP tunnel,
# "dev tap" will create an ethernet tunnel.
;dev tap
dev tun

# SSL/TLS root certificate (ca), certificate
# (cert), and private key (key).
ca ca.crt
cert server.crt
key server.key  # This file should be kept secret

# Diffie hellman parameters. 
# create: openssl dhparam -out dh2048.pem 2048
dh dh2048.pem

# Network topology
# Should be subnet (addressing via IP)
;topology subnet

# Configure server mode and supply a VPN subnet
# The server will take 10.8.0.1 for itself,
# the rest will be made available to clients.
server 10.8.0.0 255.255.255.0

# Maintain a record of client <-> virtual IP address
# associations in this file.
ifconfig-pool-persist /run/openvpn_ipp.txt

# Push routes to the client to allow it
# to reach other private subnets behind
# the server.
;push "route 192.168.10.0 255.255.255.0"
;push "route 192.168.20.0 255.255.255.0"

# If enabled, this directive will configure
# all clients to redirect their default
# network gateway through the VPN, causing
# all IP traffic such as web browsing and
# and DNS lookups to go through the VPN
# (The OpenVPN server machine may need to NAT
# or bridge the TUN/TAP interface to the internet
# in order for this to work properly).
;push "redirect-gateway def1 bypass-dhcp"

# Certain Windows-specific network settings
# can be pushed to clients, such as DNS
# or WINS server addresses.  CAVEAT:
# http://openvpn.net/faq.html#dhcpcaveats
# The addresses below refer to the public
# DNS servers provided by opendns.com.
;push "dhcp-option DNS 208.67.222.222"
;push "dhcp-option DNS 208.67.220.220"

# Uncomment this directive to allow different
# clients to be able to "see" each other.
# By default, clients will only see the server.
# To force clients to only see the server, you
# will also need to appropriately firewall the
# server's TUN/TAP interface.
;client-to-client

# Uncomment this directive if multiple clients
# might connect with the same certificate/key
# files or common names.  This is recommended
# only for testing purposes.  For production use,
# each client should have its own certificate/key
# pair.
duplicate-cn

# The keepalive directive causes ping-like
# messages to be sent back and forth over
# the link so that each side knows when
# the other side has gone down.
# Ping every 10 seconds, assume that remote
# peer is down if no ping received during
# a 120 second time period.
keepalive 10 120

# Select a cryptographic cipher.
# This config item must be copied to
# the client config file as well.
# Note that v2.4 client/server will automatically
# negotiate AES-256-GCM in TLS mode.
# See also the ncp-cipher option in the manpage
cipher AES-256-CBC

# The maximum number of concurrently connected
# clients we want to allow.
;max-clients 100

# It's a good idea to reduce the OpenVPN
# daemon's privileges after initialization.
# 不过不同的发行版上 nobody 的 group 
user nobody
;group nobody

# The persist options will try to avoid
# accessing certain resources on restart
# that may no longer be accessible because
# of the privilege downgrade.
persist-key
persist-tun

# Output a short status file showing
# current connections, truncated
# and rewritten every minute.
status /run/openvpn-status

# Set the appropriate level of log
# file verbosity.
#
# 0 is silent, except for fatal errors
# 4 is reasonable for general usage
# 5 and 6 can help to debug connection problems
# 9 is extremely verbose
verb 3

# Notify the client that when the server restarts so it
# can automatically reconnect. 
# 只适用于 UDP 模式
;explicit-exit-notify 1

```

## IP 地址的分配
### 地址段的选择和冲突问题

openvpn 分配的地址段如果与客户端所在局域网的地址段交叠，则会发生冲突，造成无法访问网络。
由于局域网IPv4地址段基本上管理员随意选择的，所以冲突无法完全避免，只能设法降低其概率，即 openvpn 选择不那么常用的地址段[15]。

192.168.0.0/16 是小型网络最常用的，应该避免。不过 192.168.255.0/8 这样的可能用得较少。
10.0.0.0/8 是大型机构常用的。
172.16.0.0/12 当中，172.16-31.x 不那么常用。

比较彻底的解决方法可能是在 openvpn 上采用 IPv6。但不幸的是，截止到 2.4，openvpn 不支持纯 IPv6 地址段，必须同时指定 IPv4 地址段[16]。

### IPv6
用 server-ipv6 指定地址和前缀长度即可。

```
server 10.109.244.0 255.255.252.0
server-ipv6 fdf2:2b5d:6ac0:85bf::/112
```

目前的一些实现特点/限制：
* 前缀长度仅支持 64-112 之间。
* 服务器地址是第一个可用地址，如 fdf2:2b5d:6ac0:85bf::1 ，但客户端的地址不是从第2个可用地址开始，而是从第 1000 开始，如 fdf2:2b5d:6ac0:85bf::1000 。
* 不能单纯用 IPv6，也要指定 IPv4，即 "server ..." 指令。

有的服务器禁用了 IPv6，例如阿里云ECS（2018.9）。这时如果使用 server-ipv6 设置会导致 openvpn 服务器启动失败：
`RTNETLINK answers: Permission denied` 。检查 IPv6 是否被禁用，可检查以下 sysctl 属性是否为 1（true）：

```
net.ipv6.conf.all.disable_ipv6 = 0 
net.ipv6.conf.default.disable_ipv6 = 0 
net.ipv6.conf.lo.disable_ipv6 = 0
```

### 静态分配
openvpn 一般对客户端动态分配IP。如果要静态分配，参考[3,4]。

## 组播和广播
dev tap 模式支持组播和广播，但 dev tun 模式则不支持。
openvpn 会将组播变成广播。对此有补丁正在开发，也有临时解决办法[14]。

## 服务器端掉线、重启的效应
服务器端如果掉线、重启，客户端的 openvpn 进程会尝试重新连接。
但是，如果客户端设置了 user nobody; group nobody ，则会没有足够的权限重新设置网络接口，导致客户端报错退出。
这种情况下，需要让服务管理程序（supervisor）自动重启客户端进程。 
