## 参考资料

[1.5]
  https://www.pyimagesearch.com/opencv-tutorials-resources-guides/
  
[2.5] Set Opencv path for a c++ project using Cmake
  https://stackoverflow.com/questions/37336667/set-opencv-path-for-a-c-project-using-cmake

[3.1] Basic Image processing using PIL and Opencv
  https://hasangoni.github.io/2021/08/03/Basic-image-processing-with-opencv-and-PIL.html

[3.2] Basic Image Manipulating using PIL and opencv
  https://hasangoni.github.io/2021/08/13/Image-Manipulating-Using-PIL-and-opencv.html

[3.5] Drawing Functions in OpenCV
  https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_gui/py_drawing_functions/py_drawing_functions.html

[4.1] Getting Started with Videos
  https://www.geeksforgeeks.org/python-play-a-video-using-opencv/
  
[5.0] DNN

[5.3] Building a face detector with OpenCV in C++
https://bewagner.net/programming/2020/04/12/building-a-face-detector-with-opencv-in-cpp/

[5.4] Image Difference with OpenCV and Python
https://www.pyimagesearch.com/2017/06/19/image-difference-with-opencv-and-python/

[5.5] Deep Learning based image colorization with OpenCV
  https://cv-tricks.com/opencv/deep-learning-image-colorization/

[8.1] How to draw 3D Coordinate Axes with OpenCV for face pose estimation?
  https://stackoverflow.com/questions/30207467/how-to-draw-3d-coordinate-axes-with-opencv-for-face-pose-estimation
  
[8.2] Is it easy to manipulate 3D images in OpenCV for a beginner in image processing?
  https://www.quora.com/Is-it-easy-to-manipulate-3D-images-in-OpenCV-for-a-beginner-in-image-processing

[9.0] 相机
[9.1] 【OpenCV】摄像机标定+畸变校正
 https://blog.csdn.net/Loser__Wang/article/details/51811347

手机广角相机标定和畸变校正
 https://blog.csdn.net/cnbloger/article/details/73845258

## C++ API

/usr/local/opencv4/include/opencv4/opencv2/opencv.hpp
/usr/local/opencv4/include/opencv4/opencv2/highgui.hpp
/usr/local/opencv4/lib/libopencv_core.so
/usr/local/opencv4/lib/cmake/opencv4/OpenCVConfig.cmake

/usr/local/opencv4/include/opencv4

/opt/intel/openvino_2020.2.120/opencv/include/opencv2/opencv.hpp
/opt/intel/openvino_2020.2.120/opencv/include/opencv2/highgui.hpp
/opt/intel/openvino_2020.2.120/opencv/lib/libopencv_core.so
/opt/intel/openvino_2020.2.120/opencv/cmake/OpenCVConfig.cmake

LD_LIBRARY_PATH=/opt/intel/openvino_2020.2.120/opencv/lib

/usr/lib/x86_64-linux-gnu/libopencv_core.so.2.4

### 在 C++ 工程中引用
[2.5]

find_package(OpenCV REQUIRED PATHS "/usr/opencv-2.4.10")
set(OpenCV_DIR /path/to/opencv_install_dir/lib/cmake/opencv4)

### image

```
# img 是 cv::Mat 类型
img.cols // 图宽
img.rows // 图高
// 截取子图
auto subImg = img(cv::Rect(x, y, w, h))
```
## python API

### 安装
pip 包有两个：opencv-python 和 opencv-python-headless ，前者带 GUI 功能（ cv2.imshow 等），后者不带。前者无法在没有启用 GUI 的机器上使用，会抛出异常。
* 两个包可以同时安装，但 cv2 模块只会指向其中一个。哪个是不确定的，可能是最后安装的那个，所以不要同时安装。
* 如果两个包都安装了，然后卸载其中一个，有可能造成另一个也不可用，因为卸载了 cv2 模块。解决办法是两个都卸载，然后安装想要的那个。

### 图像读写
[3.1-3.2]
```
val_image = cv2.imread(path, cv2.IMREAD_COLOR)
cv2.imwrite(outImagePath, outImage, [cv2.IMWRITE_JPEG_QUALITY, 90])
```

### 视频读写、显示、摄像头采集
[4.1]
在 linux 上，opencv 的视频处理后端为 g-streamer 。早期版本似乎用过 ffmpeg 后端。

```
import numpy as np
import cv2

cap = cv2.VideoCapture(0)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Display the resulting frame
    cv2.imshow('frame',gray)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
```

VideoCapture() 的参数可以是：

* 数字，解释为本机摄像头的编号。
* 字符串，解释为本地文件路径。
* 字符串，解释为url。

frame 的类型是 numpy.ndarray<uint8>，3个轴分别是 (height,width,color_channel)。
颜色顺序是 BGR，要转换顺序，可以 `cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)` 。

```
import numpy as np
import cv2

cap = cv2.VideoCapture('vtest.avi')

while(cap.isOpened()):
    ret, frame = cap.read()

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    cv2.imshow('frame',gray)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
```

从摄像头读，写入视频文件：
```
import numpy as np
import cv2

cap = cv2.VideoCapture(0)

# Define the codec and create VideoWriter object
fourcc = cv2.VideoWriter_fourcc(*'mp4v')
out = cv2.VideoWriter('output.avi',fourcc, 20.0, (640,480))　　#　宽高必须与帧的宽高相同，否则会产生空的输出。

while(cap.isOpened()):
    ret, frame = cap.read()
    if ret==True:
        frame = cv2.flip(frame,0)

        # write the flipped frame
        out.write(frame)

        cv2.imshow('frame',frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break

# Release everything if job is finished
cap.release()
out.release()
cv2.destroyAllWindows()
```

其它可能的 VideoWriter 的视频格式： DIVX, XVID, MJPG, X264, WMV1, WMV2。

注意，VideoWriter 构造器传入的宽高必须与帧的宽高相同，否则会产生空的输出。

### 视频属性
VideoCapture::get() 方法可以获取视频属性，包括宽、高、当前播放时间、当前帧数等。例如：
```
cap.get(cv.CAP_PROP_FRAME_WIDTH)
cap.get(cv.CAP_PROP_FRAME_HEIGHT)
```

有的属性是可写的，如通过摄像头采集时，CAP_PROP_FRAME_WIDTH 和 CAP_PROP_FRAME_HEIGHT 可以设置。

```
ret = cap.set(cv.CAP_PROP_FRAME_WIDTH,320)
ret = cap.set(cv.CAP_PROP_FRAME_WIDTH,240)
```

其它有用的属性：
```
cv.CAP_PROP_POS_MSEC: Current position of the video file in milliseconds. 
cv.CAP_PROP_POS_FRAMES: 0-based index of the frame to be decoded/captured next.
cv.CAP_PROP_FPS Frame: rate.
cv.CAP_PROP_FRAME_COUNT: Number of frames in the video file. 
```

设置 CAP_PROP_POS_MSEC 和 CAP_PROP_POS_FRAMES 可以跳转位置，即 seeking 。

```
cap.set(cv.CAP_PROP_POS_MSEC, 30.0)
cap.set(cv.CAP_PROP_POS_FRAMES, 120)
```

### 窗口缩放

```
        window_name = 'result'
        cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(window_name, 1280, 960)
        cv2.imshow(window_name, image)
```

### 图像处理
[3.1-3.2]
缩放

```
cv2.resize(frame: numpy.ndarray<uint8,h,w,c>, new_size: tuple<w,h>): numpy.ndarray<uint8,h,w,c>
```

### 2D 图形绘制
[3.2, 3.5]
画线

```
cv2.line(frame: numpy.ndarray<uint8,h,w,c>, p1:tuple<x:int,y:int>, p2:tuple<x:int,y:int>, color:tuple<r:int,g:int,b:int>, line_width: int)

cv2.rectangle(img: numpy.ndarray<uint8,h,w,c>, pt1:tuple<x:int,y:int>, pt2:tuple<x:int,y:int>, color:tuple<r:int,g:int,b:int> [, thickness: int [, lineType[, shift]]]) -> img
```
绘制字符
```
cv2.putText(img: numpy.ndarray<uint8,h,w,c>, text: Text, org: tuple<x:int,y:int>, fontFace:int, fontScale: float, color:tuple<r:int,g:int,b:int> [, thickness: int [, lineType[, bottomLeftOrigin]]]) -> img
```
org 是文本基点，即左下角点。

fontFace 是 `cv2.FONT_HERSHEY_SIMPLEX, cv2.FONT_HERSHEY_PLAIN` 等。

fontScale 是放大率，默认1。

颜色是 BGR 顺序，如 (255, 0, 0) 是蓝色。

opencv 很难使用 unicode 字体，要解决此问题可改用 PIL 或 matplotlib 或 pyQt 。

### 3D 图形绘制
[8.1-]

### 多线程
opencv 的很多 API 默认使用多线程，例如 resize() 。控制线程数的方法是：
```
cv2.setNumThreads(n: int) # 0 表示恢复默认值。
cv2.getNumThreads() -> int 
```

## 边缘检测
https://docs.opencv.org/3.4/d2/d2c/tutorial_sobel_derivatives.html
利用OpenCV进行边缘检测 https://blog.csdn.net/zhuoqingjoking97298/article/details/122762445
【学习OpenCV4】OpenCV边缘检测算法总结 https://zhuanlan.zhihu.com/p/469634621

```
def get_edged_image(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = cv2.GaussianBlur(img, (3, 3),  sigmaX=0, sigmaY=0)
    # ddepth: The depth of the output image. We set it to CV_16S to avoid overflow.
    img = cv2.Sobel(img, ddepth=cv2.CV_16S, dx=1, dy=1, ksize=5) # cv2.CV_64F CV_16S
    #img = cv2.Canny(image=img, threshold1=100, threshold2=200)
    img = cv2.convertScaleAbs(img) # converting back to CV_8U
    img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    return img
```

