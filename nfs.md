## 参考资料
[1.1] nfs(5) - Linux man page
https://linux.die.net/man/5/nfs

[1.3] Linux NFS: Network File System Client and Server Complete Guide 2013
https://www.slashroot.in/linux-nfs-network-file-system-client-and-server-complete-guide

[1.5] 5. Optimizing NFS Performance
https://nfs.sourceforge.net/nfs-howto/ar01s05.html


[2.1] 10 practical examples to export NFS shares in Linux
https://www.golinuxcloud.com/nfs-exports-options-examples/

[2.2] How do filesystem commit interval options interact with vm.dirty_expire_centisecs?
https://serverfault.com/questions/945936/how-do-filesystem-commit-interval-options-interact-with-vm-dirty-expire-centisec

[3.1] NAS读取延时问题及NFS缓存机制
https://blog.csdn.net/qq_31896123/article/details/111161845

[3.2] Understanding NFS Caching
https://avidandrew.com/understanding-nfs-caching.html
https://www.cnblogs.com/longchang/p/10695549.html

[3.3] https://unix.stackexchange.com/questions/23408/is-there-a-command-which-will-force-linux-to-flush-cache-of-one-file-on-an-nfs-s

[3.5] NFS, SUMMARY Shivaram Venkataraman CS 537
https://pages.cs.wisc.edu/~shivaram/cs537-sp23-notes/review-summary/cs537-review-summary-notes.pdf

[3.6] NFS Troubleshooting
https://wiki.archlinux.org/title/NFS/Troubleshooting

[3.7] 7.4. NFS Caching
https://docstore.mik.ua/orelly/networking_2ndEd/nfs/ch07_04.htm

[4.1] [Debian-Sarge] Tunneling NFS over SSH
  https://www.howtoforge.com/nfs_ssh_tunneling

[4.2] Why isn't NFS over SSH transparent to NFS?
https://serverfault.com/questions/941570/why-isnt-nfs-over-ssh-transparent-to-nfs

[4.3] How to provide NFS access over an ssh tunnel?
https://superuser.com/questions/1540212/how-to-provide-nfs-access-over-an-ssh-tunnel

[5.1] Encrypting NFSv4 with Stunnel TLS
https://www.linuxjournal.com/content/encrypting-nfsv4-stunnel-tls


## 安装

### 服务器端
```
sudo apt install nfs-kernel-server
sudo systemctl start nfs-kernel-server.service
```

### 客户端
```
sudo apt install nfs-common
```

## 服务器端配置
### 协议版本
```
cat /proc/fs/nfsd/versions
-2 +3 +4 +4.1 +4.2
```
减号表示不支持该协议，加号表示支持该协议。
可以在 `/etc/nfs.conf` 中修改。

### `/etc/exports`
配置导出的目录。
修改 `/etc/exports`:
```
/     *(rw,sync,subtree_check,insecure)
/media/data2    *(rw,sync,subtree_check,insecure)

/share *(rw,sync,no_root_squash,no_subtree_check,insecure)
```
每一行的格式为 `/PATH/TO/DIR HOST([OPTIONS])` [2.2]。

`subtree_check` 的意思是检查被访问的文件是否在导出目录下，如果不在则报错。如果导出目录不是所在文件系统的根目录时，此选项可确保远程用户不会逃出导出目录。如果导出目录是所在文件系统的根目录，无需此检查，`no_subtree_check` 可减少一些开销。
`no_root_squash` 选项允许 root 用户在 NFS 服务器上拥有 root 权限。如果使用此选项，则必须将 /share 目录的所有者设置为 root。
`sync|async` ：sync 表示如果客户端要求（fsync()），服务器端将确保数据写盘后才报告客户端已经写盘。`async` 表示即使客户端要求，服务器端将在数据写入服务器内存后就报告客户端已经写盘（这是违反NFS协议的）。`async`可以提高性能，但会降低安全性。详见后面的讨论。

注意，对于导出的某个目录（`/`）下的挂载点（`/media/data2`），需要再次导出挂载点。否则客户端如果只挂载上级目录，则其下的挂载点会显示为空。

刷新导出目录（每次修改 `/etc/exports` 后要刷新，才能生效）：
```
sudo exportfs -r
```
然后可以用 sudo exportfs -v 查看导出的目录及其选项。

### `/etc/nfs.conf`
配置 NFS 服务器端的行为。这个文件采用 shell ini格式，可配置的参数有：
```
[nfsd]
# debug=0
# nfsd 进程的数量，默认为8。如果要充分利用 CPU，应当设置为核数的4～8倍。
# threads=8
# host=
# port=0
# grace-time=90
# lease-time=90
# udp=n
# tcp=y

# 要启用哪些版本的协议
# vers2=n
# vers3=y
# vers4=y
# vers4.0=y
# vers4.1=y
# vers4.2=y
```
改后通过 `systemctl restart nfs-kernel-server` 生效。

### `/etc/default/nfs-kernel-server`
配置 NFS 服务器端的行为，与 /etc/nfs.conf 功能有重叠，在有的系统（ubuntu 22.04）上虽然存在但不生效。
这个文件采用 shell 脚本格式，可配置的参数有：

```
# nfsd 进程的数量，默认为8。如果要充分利用 CPU，应当设置为核数的4～8倍。
# Number of servers to start up
RPCNFSDCOUNT=8
# Runtime priority of server (see nice(1))
RPCNFSDPRIORITY=0
```

改后通过 `systemctl restart nfs-kernel-server` 生效。

### 内核参数
```
# rmem_default 和 rmem_max 设置得较大（262144, 256k）有利于提高 nfs 性能。
net.core.rmem_default = 212992
net.core.rmem_max = 212992
net.core.wmem_default = 212992
net.core.wmem_max = 212992

net.core.somaxconn = 4096
fs.file-max = 9223372036854775807
vm.swappiness = 0 # 0-100之间，越大越积极使用 swap。vm.swappiness 并不是个硬约束，即使等于 0，系统仍然会使用交换。

# 脏页在被写回到磁盘之前可以保持脏状态的最长时间，单位为 1/100 秒。[2.2]
vm.dirty_expire_centisecs = 3000
# 内核检查并开始写回脏页的时间周期，单位为 1/100 秒。即使脏页寿命没达到dirty_expire_centisecs ，仍可能被写盘。设置为0可以关闭此周期性检查。
vm.dirty_writeback_centisecs = 500

# 脏数据百分比，当达到此值时，开始写入硬盘。较大的值会推迟写入，提高性能并容易丢数据。
vm.dirty_ratio = 20 # 99
# 当脏页比例低于vm.dirty_ratio但高于vm.dirty_background_ratio时，系统会以较低的优先级在后台写回脏页。
vm.dirty_background_ratio = 10 # 50

# 每次写交换（swap out）应当空出多少物理内存，单位是1/10000，最大允许设为 1000，也就是 10% 的物理内存。
# 较大的值可能增加 swap 延迟，并提高写交换的吞吐量。实测对 tmpfs 缓存外性能影响不明显。
vm.watermark_scale_factor = 10

# 从内存中丢弃文件系统元数据对丢弃文件内容数据的倾向性的比值。较小的值造成缓存更多的元数据，缓存更少的文件内容；100表示两者平衡。
vm.vfs_cache_pressure = 100
```

* 当前脏数据的量可以在 /proc/meminfo 的 Dirty 字段中找到：`watch -n 1 grep -e Dirty /proc/meminfo` 。
* 可以用 sync 命令强制将脏数据写盘。
* nfs 客户端调用 `close()` 会造成脏数据写盘，写盘之后才会返回。本地文件系统一般不会这样。
* 本地 ext4 文件系统上，用 dd 做同文件覆写会触发写盘，新建文件写入则不会。strace 表明，是 dd 中 close()触发写盘。dd没有调用过强制写盘的系统调用（fsync, fdatasync , sync等），只有 write() 和 close() 。推测是 ext4 data=ordered 挂载选项所致。

### 本地文件系统挂载参数
* noatime, relatime 可减少写入操作。
* `commit=600` 最大提交间隔 600 秒，可减少写入操作，适用于 ext4和btrfs。ext4默认5秒，btrfs默认30秒。
* nobarrier 可减少写入操作，但容易丢失数据。

`vm.dirty_expire_centisecs` 与 `commit` 参数的关系是：数据最终写入硬盘的间隔最多可能是两者之和，其中可能包含了从文件系统日志写入实际文件的耗时[2.2]。

## 客户端配置

修改 /etc/fstab
```
127.0.0.1:/ /media/aiws-2 nfs port=22049,vers=4,nolock,proto=tcp,rsize=1048576,wsize=1048576,soft,timeo=600,retrans=2,noresvport,noauto,user
```

## NFS 的缓存和持久化机制
[1.1, 3.1~3.6]
NFS 的缓存机制可在两个层面影响使用者：
1. 可见性。一个用户程序对文件系统做了修改且确认成功，另一个用户程序能否立即看到这个修改？对本地文件系统，答案为“是”，但对于NFS，如果两个用户程序在同一客户机，答案为“是”，否则要看客户机的挂载选项。
2. 持久性。一个用户程序对文件系统做了修改且确认成功，能否确保这个修改已经持久化，即即使之后系统崩溃，数据也不会丢失？对本地文件系统，答案为“是”，但对于NFS，要看服务器的 NFS 导出设置。

对于新建/改名/删除文件的可见性，客户机可以用 lookupcache=positive 和 lookupcache=none 来影响。

ac / noac 挂载选项影响文件和目录的属性（例如修改时间）。noac 即“不缓存属性”，客户机总是从 NFS 获取文件系统的元数据，确保可以即时看到最新的修改，并且写入文件内容也变成同步的[3.1]。如果客户端看到目录的修改时间（mtime）属性改变了，ac 是默认的，客户机会定时或不定时（量级在数秒到数十秒）的刷新元数据。

如果使用 ac 挂载选项，有没有什么机制能强迫刷新NFS客户机的元数据缓存呢？根据[3.3]，opendir/closedir系统调用似乎可以让此目录和其下的文件的元数据立即刷新。

对于持久性，nfs 服务器可通过 sync / async 导出选项来控制[3.2,3.6]。
* 服务器端 sync 模式：客户端的 write() 类操作会在服务器端写盘后才返回。这是默认值。
* 服务器端 async 模式：客户端的 write() 类和 fsync() 类操作会在服务器端写入内存（如果容纳得下）后就返回。这实际上是违反 NFS 协议的，但提高了性能。
* 服务器崩溃时，async 模式可能丢失客户端以为已经写入的数据，sync 则不会。
* 不论服务器端 sync / async ，客户端在写文件后的 close() 操作一般仍要在服务器端写盘后才返回，可进一步参考客户端 cto/nocto 挂载选项。

客户机的 sync/async [1.1]:
* async 模式：这是默认值。客户机的 write() 类操作返回时，数据可能仍缓存在客户机上，尚未发送给服务器。直到客户端调用 fsync() 类操作或 close() 时，数据才会发送给服务器，服务器确认后返回。
* sync 模式：客户机的 write() 类操作将立即发送数据给服务器，并在服务器确认后返回。
* 如果客户机打开文件时用了 O_SYNC 选项，那么对此文件的写入等同于 sync 模式，即使挂载选项是 async。
* 服务器确认是否意味着服务器已写盘，仍受到服务器端的 sync/async 选项的影响。目前，不论服务器端的配置，客户机确保写盘的方式可以是调用 close()。

客户机的 cto/nocto 挂载选项可能影响写多客户机读写同一文件时的数据一致性，以及读写性能[1.1,3.5,3.6]。cto 是 close-to-open，意思是以下的操作模式：

  客户机A：打开（open()）文件x，写文件 x，然后关闭文件x（ close()）。
  客户机B：打开（open()）文件x，读文件 x。
  客户机B的open()在客户机A的close()返回之后才开始，但时间间隔可以很短。

* cto 挂载选项会确保客户机B读到的x的内容就是客户机A写入的最新内容。cto 为linux的默认选项。
* nocto 挂载选项则不做 cto 那样的保证，以换取比 cto 更高的读写性能。nocto 原则上只可以在不同客户机访问的文件不存在交集的情况下使用。
* 要实现 cto 选项，理论上并不需要服务器将 A 对文件 x 修改的数据写盘。只要 A 的 close() 返回前，这些数据进入了服务器的文件系统缓存，就可以确保 B 读到最新内容。
但是，目前（2024.8）linux的nfs实现并不是这样，不论 cto 还是 nocto，A 的 close() 一定会触发服务器写盘，写盘完成后 close() 才会返回。所以 linux 的 nocto 实际上也没有性能优势，没理由使用它。
* linux 的 nfs 对 cto 的实现在一些场景下限制了读写性能，例如如果服务器内存能完全容下x，客户机B在读完x后立即删除它，则理论上可以完全省略写盘，但linux上无法省略。
  
总结 NFS 与本地文件系统的数据持久性异同：
* close()：NFS 确保写盘，本地文件系统不一定。
* fsync()类：本地文件系统确保写盘，NFS 不一定，如果服务器端设置了 async，则忽略它。
* open() 类的 O_SYNC 选项：本地文件系统确保 write() 写盘；NFS 不一定，如果服务器端设置了 async，则忽略 O_SYNC。

## hard/soft
hard 对于网络故障会一直等待网络恢复，soft则只等待有限的时间。

## NFS over ssh
[4.1-4.2]
NFS 没有内置加密机制（NFS v4 有 Kerberos 加密），因此不适合在广域网上使用；通过 ssh 隧道来访问 NFS，可以弥补这个缺点。

配置时要注意的点：
1. 选择 NFS v4 协议，而不是 v3。客户端mount时加上vers=4参数即可。
2. 选择TCP为NFS的传输层协议。因为ssh隧道只能承载TCP。客户端mount时加上proto=tcp。
3. 只通过映射NFS的主要端口（默认2049）即可，忽略其它端口。
4. 用root帐号登录ssh做ssh隧道。否则客户端会收到 access denied 错误。
5. 服务器导出目录采用 insecure 选项。
5. 使用 soft 模式。

服务器端的例子：
修改 `/etc/exports`:
```
/     *(rw,sync,subtree_check,insecure)
/media/data2    *(rw,sync,subtree_check,insecure)

/share *(rw,sync,no_root_squash,no_subtree_check,insecure)
```
subtree_check 的意思是检查被访问的文件是否在导出目录下，如果不在则报错。如果导出目录不是所在文件系统的根目录时，此选项可确保远程用户不会逃出导出目录。如果导出目录是所在文件系统的根目录，无需此检查，no_subtree_check 可减少一些开销。
no_root_squash 选项允许 root 用户在 NFS 服务器上拥有 root 权限。如果使用此选项，则必须将 /share 目录的所有者设置为 root。

客户端的例子：
修改 /etc/fstab
```
127.0.0.1:/ /media/aiws-2 nfs port=22049,vers=4,nolock,proto=tcp,rsize=1048576,wsize=1048576,soft,timeo=600,retrans=2,noresvport,noauto,user
```
`port=22049` 端口号，与后面的 ssh 隧道端口号对应。
`vers=4` 使用 nfs v4 协议，另一个可能的版本是 vers=3。据说如果要与ssh隧道搭配使用，则最好使用nfs v4。
`proto=tcp` 使用 tcp 协议来承载。nfs默认也使用udp协议，但udp无法与ssh隧道搭配，故应该强制使用tcp。
`soft` 对于网络故障，只等待有限的时间，如果未恢复就报错。另一个对应的选项是 hard，对于网络故障会一直等待恢复。soft适用于不可靠或非持久的网络环境，如个人电脑；hard适用于可靠而持久的网络环境，如数据中心。soft 模式可能造成数据丢失，如果访问nfs挂载点的应用程序没有对不可靠的文件系统做好准备。
`noauto` 不要开机自动挂载。适用于不可靠或非持久的网络环境。否则，如果开机时网络不通，则可能导致启动卡住。
`user` 允许普通用户挂载。
`noresvport` 客户端使用非特权端口（non-privileged source port），这允许客户端挂载大量的nfs目录。
`timeo` 请求超时，单位是0.1秒。仅在 `soft` 模式下生效。超时发生时，nfs 客户端可能会重试，如果重试次数超过 `retrans` 指定的值，则放弃，并导致报客户代码报错，例如 `read()` 系统调用返回 EIO 错误。
`_netdev` 等待网络在准备好后才挂载。

建立ssh隧道（在一个终端中执行）：
```
autossh -CNg -L 22049:127.0.0.1:2049 root@36.138.101.83
```

2049 是 nfs server 的默认端口，对应客户端挂载选项的 `port=22049` 。

挂载文件系统：
```
sudo mkdir -p /media/aiws-2
sudo mount /media/aiws-2
```

这样设置后，客户机可以实现可靠的 nfs 访问，待机或切换wifi后都可以恢复访问，无需重新挂载。

不过，NFS over ssh 的可靠也是相对的。有时候，网络问题或者 ssh 隧道不通的问题，会导致整个文件系统（不只是nfs挂载点）的响应速度变得很缓慢，表现为在很多目录下 ls 之类的命令要几分钟才能出结果。重启ssh隧道并重新挂载nfs应该可以解决。

## NFSv4 with Stunnel TLS
[5.1]

## NFS 性能监控/剖析
### nfsstat
包名：nfs-common

https://www.redhat.com/sysadmin/using-nfsstat-nfsiostat

nfsstat Command
https://www.ibm.com/docs/en/aix/7.2?topic=n-nfsstat-command

On the NFS client, you run the following:

nfsstat -c

Output should be similar to below:

Client rpc stats:
calls      retrans    authrefrsh
30557550   27686      11075

Client nfs v4:
null         read         write        commit       open         open_conf
0         0% 1601136   5% 568239    1% 23683     0% 1652804   5% 1466260   4%
open_noat    open_dgrd    close        setattr      fsinfo       renew
0         0% 0         0% 1648000   5% 27154     0% 8         0% 28320     0%

nfsstat -z re-initialize the counters 

### nfsiostat
包名：nfs-common

Running nfsiostat without any argument should have an output similar to the following:

10.10.1.10:/data/share mounted on /samba/students:
   op/s         rpc bklog
  16.96            0.00
read:             ops/s            kB/s           kB/op         retrans         avg RTT (ms)    avg exe (ms)
                  0.900           5.392           5.990        0 (0.0%)           0.550           0.660
write:            ops/s            kB/s           kB/op         retrans         avg RTT (ms)    avg exe (ms)
                  0.031          21.818         708.149        0 (0.0%)         122.745         35874.872

3秒间隔统计
nfsiostat 3

### mountstats


## 阿里云

sudo mount -t nfs -o vers=3,nolock,proto=tcp,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport 1de504b5fa-oux67.cn-beijing.nas.aliyuncs.com:/ /mnt
