## psutil - CPU、mem

psutil.cpu_percent(interval=None, percpu=False)

Return a float representing the current system-wide CPU
utilization as a percentage.

When *interval* is > 0.0 compares system CPU times elapsed before
and after the interval (blocking).

When *interval* is 0.0 or None compares system CPU times elapsed
since last call or module import, returning immediately (non
blocking). That means the first time this is called it will
return a meaningless 0.0 value which you should ignore.
In this case is recommended for accuracy that this function be
called with at least 0.1 seconds between calls.

When *percpu* is True returns a list of floats representing the
utilization as a percentage for each CPU.
First element of the list refers to first CPU, second element
to second CPU and so on.
The order of the list is consistent across calls.

psutil.virtual_memory()

Return statistics about system memory usage as a namedtuple
including the following fields, expressed in bytes:

- total:
total physical memory available.

- available:
the memory that can be given instantly to processes without the
system going into swap.
This is calculated by summing different memory values depending
on the platform and it is supposed to be used to monitor actual
memory usage in a cross platform fashion.

- percent:
the percentage usage calculated as (total - available) / total * 100

- used:
memory used, calculated differently depending on the platform and
designed for informational purposes only:
macOS: active + wired
BSD: active + wired + cached
Linux: total - free

以上字段在 linux 上与内核的 /proc/meminfo 一致，也与 free 命令一致。

## nvitop - GPU - GPU mem
```
from nvitop import Device

devices = Device.all()  # or Device.cuda.all()
for device in devices:
processes = device.processes()  # type: Dict[int, GpuProcess]
sorted_pids = sorted(processes)

print(device)
print(f'  - Fan speed:       {device.fan_speed()}%')
print(f'  - Temperature:     {device.temperature()}C')
print(f'  - GPU utilization: {device.gpu_utilization()}%')
print(f'  - Total memory:    {device.memory_total_human()}')
print(f'  - Used memory:     {device.memory_used_human()}')
print(f'  - Free memory:     {device.memory_free_human()}')
print(f'  - Processes ({len(processes)}): {sorted_pids}')
for pid in sorted_pids:
    print(f'    - {processes[pid]}')
print('-' * 120)
```

```
collector = ResourceMetricCollector(interval=2.0)
```

```
resources/host/cpu_percent (%)/mean             4.3
resources/host/cpu_percent (%)/min              4.3
resources/host/cpu_percent (%)/max              4.3
resources/host/cpu_percent (%)/last             4.3
resources/host/memory_percent (%)/mean          13.6
resources/host/memory_percent (%)/min           13.6
resources/host/memory_percent (%)/max           13.6
resources/host/memory_percent (%)/last          13.6
resources/host/swap_percent (%)/mean            0.0
resources/host/swap_percent (%)/min             0.0
resources/host/swap_percent (%)/max             0.0
resources/host/swap_percent (%)/last            0.0
resources/host/memory_used (GiB)/mean           7.782115936279297
resources/host/memory_used (GiB)/min            7.782115936279297
resources/host/memory_used (GiB)/max            7.782115936279297
resources/host/memory_used (GiB)/last           7.782115936279297
resources/host/load_average (%) (1 min)/mean            0.019999999999999997
resources/host/load_average (%) (1 min)/min             0.02
resources/host/load_average (%) (1 min)/max             0.02
resources/host/load_average (%) (1 min)/last            0.02
resources/host/load_average (%) (5 min)/mean            0.03
resources/host/load_average (%) (5 min)/min             0.03
resources/host/load_average (%) (5 min)/max             0.03
resources/host/load_average (%) (5 min)/last            0.03
resources/host/load_average (%) (15 min)/mean           0.0
resources/host/load_average (%) (15 min)/min            0.0
resources/host/load_average (%) (15 min)/max            0.0
resources/host/load_average (%) (15 min)/last           0.0
resources/gpu:0/memory_used (MiB)/mean          8.5
resources/gpu:0/memory_used (MiB)/min           8.5
resources/gpu:0/memory_used (MiB)/max           8.5
resources/gpu:0/memory_used (MiB)/last          8.5
resources/gpu:0/memory_free (MiB)/mean          24259.8125
resources/gpu:0/memory_free (MiB)/min           24259.8125
resources/gpu:0/memory_free (MiB)/max           24259.8125
resources/gpu:0/memory_free (MiB)/last          24259.8125
resources/gpu:0/memory_total (MiB)/mean         24268.3125
resources/gpu:0/memory_total (MiB)/min          24268.3125
resources/gpu:0/memory_total (MiB)/max          24268.3125
resources/gpu:0/memory_total (MiB)/last         24268.3125
resources/gpu:0/memory_percent (%)/mean         0.0
resources/gpu:0/memory_percent (%)/min          0.0
resources/gpu:0/memory_percent (%)/max          0.0
resources/gpu:0/memory_percent (%)/last         0.0
resources/gpu:0/gpu_utilization (%)/mean                0.0
resources/gpu:0/gpu_utilization (%)/min         0.0
resources/gpu:0/gpu_utilization (%)/max         0.0
resources/gpu:0/gpu_utilization (%)/last                0.0
resources/gpu:0/memory_utilization (%)/mean             0.0
resources/gpu:0/memory_utilization (%)/min              0.0
resources/gpu:0/memory_utilization (%)/max              0.0
resources/gpu:0/memory_utilization (%)/last             0.0
resources/gpu:0/fan_speed (%)/mean              0.0
resources/gpu:0/fan_speed (%)/min               0.0
resources/gpu:0/fan_speed (%)/max               0.0
resources/gpu:0/fan_speed (%)/last              0.0
resources/gpu:0/temperature (C)/mean            43.0
resources/gpu:0/temperature (C)/min             43.0
resources/gpu:0/temperature (C)/max             43.0
resources/gpu:0/temperature (C)/last            43.0
resources/gpu:0/power_usage (W)/mean            34.97
resources/gpu:0/power_usage (W)/min             34.97
resources/gpu:0/power_usage (W)/max             34.97
resources/gpu:0/power_usage (W)/last            34.97
resources/gpu:1/memory_used (MiB)/mean          259.875
resources/gpu:1/memory_used (MiB)/min           259.875
resources/gpu:1/memory_used (MiB)/max           259.875
resources/gpu:1/memory_used (MiB)/last          259.875
resources/gpu:1/memory_free (MiB)/mean          24000.375
resources/gpu:1/memory_free (MiB)/min           24000.375
resources/gpu:1/memory_free (MiB)/max           24000.375
resources/gpu:1/memory_free (MiB)/last          24000.375
resources/gpu:1/memory_total (MiB)/mean         24260.25
resources/gpu:1/memory_total (MiB)/min          24260.25
resources/gpu:1/memory_total (MiB)/max          24260.25
resources/gpu:1/memory_total (MiB)/last         24260.25
resources/gpu:1/memory_percent (%)/mean         1.1
resources/gpu:1/memory_percent (%)/min          1.1
resources/gpu:1/memory_percent (%)/max          1.1
resources/gpu:1/memory_percent (%)/last         1.1
resources/gpu:1/gpu_utilization (%)/mean                0.0
resources/gpu:1/gpu_utilization (%)/min         0.0
resources/gpu:1/gpu_utilization (%)/max         0.0
resources/gpu:1/gpu_utilization (%)/last                0.0
resources/gpu:1/memory_utilization (%)/mean             2.0
resources/gpu:1/memory_utilization (%)/min              2.0
resources/gpu:1/memory_utilization (%)/max              2.0
resources/gpu:1/memory_utilization (%)/last             2.0
resources/gpu:1/fan_speed (%)/mean              0.0
resources/gpu:1/fan_speed (%)/min               0.0
resources/gpu:1/fan_speed (%)/max               0.0
resources/gpu:1/fan_speed (%)/last              0.0
resources/gpu:1/temperature (C)/mean            50.0
resources/gpu:1/temperature (C)/min             50.0
resources/gpu:1/temperature (C)/max             50.0
resources/gpu:1/temperature (C)/last            50.0
resources/gpu:1/power_usage (W)/mean            30.418999999999997
resources/gpu:1/power_usage (W)/min             30.419
resources/gpu:1/power_usage (W)/max             30.419
resources/gpu:1/power_usage (W)/last            30.419
resources/duration (s)          0.04889535717666149
resources/timestamp             1712912044.440592
resources/last_timestamp                1712912044.4014556
```

数据定义：
* `host/cpu_percent` 来自 psutil.cpu_percent 。
* `host/memory_percent` ：来自 psutil.virtual_memory().percent ，定义为 (total - available) / total * 100 。Linux 上这个是比较准确的。
* `host/memory_used (GiB)` ：来自 psutil.virtual_memory().used ，Linux 上定义为: total - free 。Linux 上这个不够准确。
