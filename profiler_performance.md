## 参考资料
[1.1]
https://www.valgrind.org/docs/manual/cl-manual.html

[2.0] KCachegrind
https://kcachegrind.github.io/
[2.1] 
https://kcachegrind.github.io/html/Documentation.html

[3.0] Sysprof
https://www.sysprof.org/

[3.1] [Sysprof 的一个应用案例] Thumbnails are shuffling around / resorting while loading large folders in grid view
https://gitlab.gnome.org/GNOME/nautilus/-/issues/2826


## Callgrind
### 安装
Callgrind 是 valgrind 的一部分，可视化分析工具为 KCachegrind 。包名分别是 valgrind kcachegrind qcachegrind 。

callgrind 会显著拖慢被剖析程序。

### 基本使用
[1.1]

valgrind --tool=callgrind  <program_to_profile> [program_args]

被剖析程序退出后，会在当前目录生成 callgrind.out.XXX 文件。接着在当前目录运行 kcachegrind [2.0] ，不用写文件名，它会自动加载 callgrind.out.XXX 文件。

其它选项：
--separate-threads=<yes|no> [default: yes]  是否将每个线程单独写入一个数据文件。
--instr-atstart=<yes|no> [default: yes]  是否启动被剖析程序后就立即开始搜集数据。如果是no，有两个方法可以在后期启停剖析：（1） callgrind_control -i on|off （2）在被剖析程序中调用 CALLGRIND_START_INSTRUMENTATION()/CALLGRIND_STOP_INSTRUMENTATION() 宏。
--callgrind-out-file=<file>

### 运行中的控制

启动和停止 callgrind 搜集数据：

callgrind_control -i on|off

让 callgrind 立即输出剖析数据文件。默认是等到被剖析程序退出后才输出。

callgrind_control --dump

### 剖析不能直接启动的程序
callgrind 并不能剖析已经在运行的程序。
如果有的程序不方便直接从命令行启动，则可以采取以下方法。例如要剖析的是 /usr/bin/foo ，想要把数据文件放到 /foo-inst 目录：

（0）安装 foo 的调试符号（如果有）
这是为了让 callgrind 输出的函数名可读。

```
dpkg -S /usr/bin/foo  # 确定 /usr/bin/foo 所属的包名，例如是 foo
apt search foo  # 确定是否有调试符号包，例如是 foo-dbgsym
sudo apt install foo-dbgsym
```
如果想剖析的一些函数位于 foo 的一些依赖库中，可以继续安装它们的调试符号。

（1）创建一个shell脚本 /foo-inst/foo.sh ，内容是

```
#!/bin/sh
#cd /foo-inst
valgrind --log-file=/foo-inst/foo.valgrind.%p.log --tool=callgrind --callgrind-out-file=/foo-inst/foo.callgrind.out.%p --instr-atstart=no /usr/bin/foo.bk $*
```

（2）用 /foo-inst/foo.sh 取代 /usr/bin/foo ：

```
sudo mv /usr/bin/foo /usr/bin/foo.bk
sudo ln -s /foo-inst/foo.sh /usr/bin/foo
```

（3）重新启动 foo
（4）执行剖析
```
callgrind_control -i on
callgrind_control -i off
callgrind_control --dump
```
（5）恢复原状
```
sudo mv /usr/bin/foo.bk /usr/bin/foo
```

## kcachegrind
用法： `kcachegrind [callgrind_out_file]` 。如果不带参数，则在当前目录自动寻找 `callgrind.out.XXX` 文件。

## Sysprof
Sysprof is a statistical, system-wide profiler for Linux.
Sysprof 是 gnome 开发的，有一些针对GUI系统的剖析功能 [3.0]。
Sysprof 不会显著影响被剖析程序。

### 安装（deb）
```
sudo apt install sysprof
```
sysprof 的版本与 gnome 版本一致，当前为 46.0 或 3.46.0 。

### 安装（编译安装）

less /boot/config-6.9.6-amd64-desktop-rolling

```
CONFIG_CC_VERSION_TEXT="gcc (Deepin 13.2.0-3deepin3) 13.2.0"
```

sudo update-alternatives --config gcc

```
./configure --prefix=$HOME/opt
make
make install

~/opt/bin/sysprof-cli -h
```

### 使用
```
sudo sysprof-cli xxx.syscap # 开始剖析，直到按 ctrl+C 结束。
sudo chown <group>:<user> xxx.syscap # 更改文件所有者，以便普通用户可以查看剖析结果。
sysprof xxx.syscap # 查看剖析结果。sysprof 是 GUI 查看器。
```

剖析特定程序
```
# 被程序用 <command_line> 命令启动。
sysprof-cli --speedtrack --scheduler xxx.syscap -- <command_line>

# 剖析 PID 为 nnn 的程序。
sysprof-cli --speedtrack --scheduler -p nnn xxx.syscap
```

## 固定 CPU 的频率

### linux cpufrequtils 
查看当前频率设置：
```
cpufreq-info

  driver: acpi-cpufreq
  CPUs which run at the same hardware frequency: 23
  CPUs which need to have their frequency coordinated by software: 23
  maximum transition latency: 4294.55 ms.
  hardware limits: 2.20 GHz - 5.62 GHz
  available frequency steps: 4.20 GHz, 2.80 GHz, 2.20 GHz
  available cpufreq governors: conservative, ondemand, userspace, powersave, performance, schedutil
  current policy: frequency should be within 2.20 GHz and 4.20 GHz.
                  The governor "ondemand" may decide which speed to use
                  within this range.
  current CPU frequency is 2.20 GHz.
  cpufreq stats: 4.20 GHz:0.41%, 2.80 GHz:0.02%, 2.20 GHz:99.57%  (38125)
```

设置 CPU 调度策略为 userspace
将 CPU 的调度策略设置为 userspace，以便手动设置频率。
然后，将 CPU 核心的频率设置为固定值，例如，将 CPU 核心 0 的频率设置为 3GHz。

```
sudo cpufreq-set -c 0 -g userspace
sudo cpufreq-set -c 0 -f 4.2GHz
```
-c 参数指定设置 CPU 核心 0。

如果需要对所有核心进行设置，可以编写一个脚本：

```
for i in $(seq 0 $(nproc --all)); do
sudo cpufreq-set -c $i -g userspace
sudo cpufreq-set -c $i -f 4.2GHz
done
```
nproc --all 列出 CPU 数量。

恢复默认设置：
```
for i in $(seq 0 $(nproc --all)); do
sudo cpufreq-set -c $i -g ondemand
done
```