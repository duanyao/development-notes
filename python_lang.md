## 参考资料

[1.1] Python 3 文档
https://docs.python.org/zh-cn/3/

[1.2] sys
https://docs.python.org/3/library/sys.html

[1.6] How to Unpack a List in Python
https://www.pythontutorial.net/python-basics/python-unpack-list/

[1.7] function definitions
https://docs.python.org/3/reference/compound_stmts.html#function-definitions

[1.8] Python args, kwargs, and All Other Ways to Pass Arguments to Your Function
https://towardsdatascience.com/python-args-kwargs-and-all-other-ways-to-pass-arguments-to-your-function-bd2acdce72b5

[1.9] Lambda Expressions
https://joshdata.me/lambda-expressions.html

[1.12] Python 获取类对象的父类
https://www.cnblogs.com/qq952693358/p/10333282.html

[1.13] Custom Python Dictionaries: Inheriting From dict vs UserDict
https://realpython.com/inherit-python-dict/

[1.14] Custom Python Lists: Inheriting From list vs UserList
https://realpython.com/inherit-python-list/

[2.1] New interesting data structures in Python 3
https://github.com/topper-123/Articles/blob/master/New-interesting-data-types-in-Python3.rst

[2.2] StringIO in Python
https://medium.com/techtofreedom/stringio-in-python-eebf31d18f43

[2.3] Python io – BytesIO, StringIO
https://www.journaldev.com/19178/python-io-bytesio-stringio

[3.3] Relative Imports in Python - Without Tearing Your Hair Out
https://gideonbrimleaf.github.io/2021/01/26/relative-imports-python.html

[3.4] How to do relative imports in Python?
https://stackoverflow.com/questions/72852/how-to-do-relative-imports-in-python

[3.5] absolute-and-relative-imports-in-python
https://www.geeksforgeeks.org/absolute-and-relative-imports-in-python/

[3.6] ImportError: Attempted Relative Import With No Known Parent Package (Python)
https://techwithtech.com/importerror-attempted-relative-import-with-no-known-parent-package/

[3.7] Python Relative Imports in VSCode (Fix ModuleNotFoundError and Auto-completion)
https://k0nze.dev/posts/python-relative-imports-vscode/

[4.5] glob

[5.1] Python Type Checking (Guide)
https://realpython.com/python-type-checking/

[6.2] ThreadPoolExecutor
 https://docs.python.org/3/library/concurrent.futures.html#concurrent.futures.ThreadPoolExecutor

[6.3] 深入理解Python中的GIL（全局解释器锁）
https://zhuanlan.zhihu.com/p/75780308

[6.4] Python Multithreading without GIL
https://github.com/colesbury/nogil

[6.5] PEP 703 – Making the Global Interpreter Lock Optional in CPython
https://peps.python.org/pep-0703/

[6.6] Free Threading in Python
https://docs.python.org/3/howto/free-threading-python.html

[6.7] free-threading Compatibility Status Tracking
https://py-free-threading.github.io/tracking/

[6.8] Free-Threaded Wheels
https://hugovk.github.io/free-threaded-wheels/

[7.1] Python Async/Await入门指南
https://zhuanlan.zhihu.com/p/27258289

[7.2] 协程与任务
https://docs.python.org/zh-cn/3/library/asyncio-task.html

[7.3] python教程：使用 async 和 await 协程进行并发编程
https://www.cnblogs.com/heniu/p/12740400.html

[7.4] python异步编程之asyncio（百万并发）
https://www.cnblogs.com/shenh/p/9090586.html

[7.5] concurrent.futures --- 启动并行任务
https://docs.python.org/zh-cn/3/library/concurrent.futures.html

[7.6] 从0到1，Python异步编程的演进之路
https://zhuanlan.zhihu.com/p/25228075

[8.1] python-multiprocessing
https://python.land/python-concurrency/python-multiprocessing

[8.2] The subprocess Module: Wrapping Programs With Python
https://realpython.com/python-subprocess/

[8.3] subprocess — Subprocess management
https://docs.python.org/3/library/subprocess.html

[8.4] Python Subprocess Examples
https://www.datacamp.com/tutorial/python-subprocess

[8.5] Switching default multiprocessing context to “spawn” on POSIX as well
https://discuss.python.org/t/switching-default-multiprocessing-context-to-spawn-on-posix-as-well/21868

[8.6] Why your multiprocessing Pool is stuck (it’s full of sharks!)
https://pythonspeed.com/articles/python-multiprocessing/

[9.1] python内置缓存lru_cache
https://zhuanlan.zhihu.com/p/378439607

[10.1] Requests: HTTP for Humans
https://docs.python-requests.org/en/latest/index.html

[10.2] requests
https://www.liaoxuefeng.com/wiki/1016959663602400/1183249464292448

[10.3] Python concurrent HTTP requests
https://zetcode.com/python/concurrent-http-requests/

[11.1] Flatten a List in Python
https://www.delftstack.com/howto/python/how-to-flatten-a-list-in-python/

[11.2] Python – Itertools.chain.from_iterable()
https://www.geeksforgeeks.org/python-itertools-chain-from_iterable/

[12.1] How to decode URL and Form parameters in Python
https://bobbyhadz.com/blog/python-decode-url-parameters

[13.1] Running Python Parallel Applications with Sub Interpreters
https://tonybaloney.github.io/posts/sub-interpreter-web-workers.html

[13.2] extrainterpreters
https://github.com/jsbueno/extrainterpreters

[13.3] PEP 734 – Subinterpreters
https://peps.python.org/pep-0734

## 数字

分整数和浮点数.
整数之间的运算结果一般都是整数, 如果至少一个操作数是浮点数, 结果是浮点数.
但是除法例外 '/' 得到的总是浮点数.（python2.x 的整数除整数得到整数）
'//' 是取整除法, 当两边都是整数时, 结果也是整数, 否则结果是浮点数(经过floor操作).
'%' 是求余数, 当两边都是整数时, 结果也是整数, 否则结果是浮点数.

NaN 和无穷大可以用 float() 转换字符串得到：

```
pos_inf = float('inf')     # positive infinity
neg_inf = float('-inf')    # negative infinity
not_a_num = float('nan')   # NaN ("not a number")
```

浮点数除以零（如 1.2/0.0）也会抛出 ZeroDivisionError 异常，这与其它主流语言、浮点硬件的实现以及 IEEE754 标准不同。
这个特性无法关闭。如果想获得标准的浮点运算特性，可以使用 numpy 库。

python3 的整数只有一种（int），是有符号的、无限位数的，永远不会溢出的。
不过，python3 的基础数据结构（list, tuple, str 等）的长度和下标仍任是有上限的，可以从 sys.maxsize 获得，在64位或32位系统上，其值为 int64 的最大值（`9223372036854775807 == 2**63-1`）
或者 int32 的最大值（`2147483647 == 2**31 - 1`）[1.2]。

## 字符串

`\` 在字符串里的含义取决于其后面的字符:如果可以解释为转义序列, 则解释为转义序列, 否则解释为反斜杠字符.
例如 '\n' 是换行, 而 '\s' 和 '\\s' 都是一个反斜杠接's'. 这与其它类C语言都不同.

不接受转义的字符串： `r'C:\some\name'`。
保留原样的字符串：
```
"""\
Usage: thingy [OPTIONS]
     -h                        Display this usage message
     -H hostname               Hostname to connect to
"""
```
字符串的类是str。利用 str() 构造器可以将其它类型转换为字符串，例如 `str(1.4)` 。

字符串的常用操作：

```
i: int = t.find('*') # 搜索字符的（第一个）位置
parts: list[str] = t.split('/') # 分割字符串为数组

```

python 中还有一种“字节型字符串”，类名为 bytes 。bytes 与 str 类似，也是不可变的。

```
b'bytes' # bytes 字面量，仅限于 ASCII
b'\xe5\xad\x97\xe7\xac\xa6 # bytes 字面量，16进制转义，可用于任意数据
bytes('字符串', encoding='UTF-8') # str 转为 bytes ，可指定编码，默认 utf-8，下同
'字符串'.encode('utf-8') # str 转为 bytes
b'bytes'.decode('utf-8') # bytes 转为 str
```

### 格式化字符串

百分号格式化字符串：
```
"78%s,%d" % ('a',1)    # '78a,1'
"78%s" % 'a'           # '78a'
```
单个参数时，可以不用组元来包装。

格式化占位符说明：
`%s` 字符串。但不仅适用于字符串，还适用于可转化为字符串的类型，包括 None，因此几乎适用于所有的类型。当不确定具体参数类型时，可以用`%s`占位符。
`%d %3d %03d` 整数。但也适用于浮点数（截断为整数）。如果有数字前缀，则在左边填空格或填0。
`%f %.3f %0.3f` 浮点数，保留指定的小数位。但也适用于整数。
`%.4g` 浮点数或整数，保留指定的有效数字。如果太大会使用科学计数法。

花括号格式化字符串（也称 f-string）：

```
"78{},{},{}".format('a', 1/3, {'c': 5}) # "78a,0.3333333333333333,{'c': 5}"
```
内嵌的 f-string，可以直接引用作用域内的变量。
```
name = 'hello'
value = 0.023451
f'{name}\t{value:.4g}' # 'hello\t0.02345'
```
花括号中，冒号后面是格式化占位符。

## 正则表达式
python 没有正则表达式字面量，可以通过 re 包使用正则表达式。
re.compile() 将字符串转换为正则表达式对象，re.split分割字符串，re.sub|subn 替换字符串。
```
import re
p = re.compile(r'\s') # r 表示 raw string，其中的 `\` 无需转义。
t = ' a  b \nc'
p.split(t)
re.split(r'\s', t)
```
正则表达式的方法：
 match(string) 测试是否匹配字符串的开头。返回 Match 对象或 None。
 fullmatch(string) 测试是否匹配字符串的全部。返回 Match 对象或 None。
 search(string) 查找一个匹配位置。返回 Match 对象或 None。
 findall(string) 查找全部匹配位置。
 sub(repl, string, count=0, flags=0) 替换所有匹配位置。
 subn(string) 替换所有匹配位置。返回替换后的字符串。
 split(string, maxsplit=0)

## 变量

变量的定义和赋值的语法相同, 即首次赋值就是定义变量.
使用未定义变量抛出异常(NameError: name 'n' is not defined).

## 布尔类型和表达式

除了 bool 类型，可以当做 bool 类型的 False 的表达式是：
空串，0，None，空的容器（仅限内置容器，如list, dict, tuple, set）。
注意，浮点数 NaN 转化为 True 而不是 False，这与js不同。

## 显式类型转换

函数表达式 `xxx(foo)` 可以显式类型转换，其中 xxx 可以是 `int, float, complex, bool, str, list, tuple, set, dict` 等。
但有些情形是无法合理转换的，就会抛出异常。无法转为一般数字的字符串用float转换时抛出异常而不是转为nan。

`int(1.6) -> 1` 因此是截断取整。要其它的取整方式，可使用 math.floor 等方法。

## 反射/RTTI

`type(foo)` 或者 `foo.__class__` 返回对象的类型，是个类（class）。`foo.__class__` 不适用于简单类型，如 int，但 `type(foo)` 可以。

```
[].__class__ # <class 'list'>

type('str') is str # True
```

`isinstance(obj, type)` 测试实例关系。

`c.__bases__` 返回一个类的所有基类（超类），类型是 tuple[class]。

```
[].__class__.__bases__ # (<class 'object'>,)
```

## 列表 list

```
a = [1,2,'3',[5, 'k']] #字面量
a = [i for i in range(0, 10)] # 初始化为0-9的序列
a = [0 for i in range(0, 10)] # 初始化为单一值（0）的长度为10的序列

#截取复制
b = a[1:3]  # 含1号，不含3号。
a[:]
a[1:]
a[1:-1] # -k (k>0) 表示 len(a) - k 号元素，即 -1 表示最后一个元素

追加
[0].append(9) # 追加一个元素
[0].extend([1, 2]) # 追加多个元素，放在列表或集合等容器里

len([1, 2, 3]) # 长度
[1, 2, 3] + [4, 5, 6]  # 连接
3 in [1, 2, 3]   # 测试存在性
for x in [1, 2, 3]: print x # 遍历

list.sort(cmp=None, key=None, reverse=False) # 排序
list.index(obj) # 查找索引

# 连接操作
>>> [1,3]+[4]
[1, 3, 4]

# 重复/填充
[2] * 5 # [2, 2, 2, 2, 2]

# map 操作
list(map(lambda x: x ** 2, [1, 2, 3, 4, 5]))
list(map(lambda x, y: x + y, [1, 3, 5, 7, 9], [2, 4, 6, 8, 10])) #对相同位置的列表数据进行相加

max(list)
min(list)

# 将列表连接为字符串（如果都是字符串，可直接join，否则要先转换）：
','.join(str(e) for e in [1, 3, 'bb'])
','.join(['1', '4'])
```

对 list 按索引访问时（ list[n] ），无论读写，只要索引超范围就会抛出异常（IndexError），不像js数组那样动态增长。
要增长 list 长度到至少 len2，可用：
```
a.extend([None for i in range(max(len2 - len(a), 0))])
```

## 解包（unpack）
将 list 或 list like 对象解开，赋值给多个变量[1.6]：
```
colors = ['red', 'blue', 'green']
red, blue, green = colors # 'red', 'blue', 'green'

red, blue = colors # ValueError: too many values to unpack (expected 2)

# 对于不定长的 list，解包时可以将剩余的元素放入另一个 list，用 * 操作符：
red, blue, *other = colors # red, blue, ['green']
```
解包操作符（`*`）可以将 list 或 list like 对象解开，常用于将 list 作为函数的实际参数：

```
def add(a, b):
  return a + b

add(1, 2) # 3
add(*[1, 2]) # 3，等效于 add(1, 2)
add(*[1,2,3]) # 错误，参数太多
```
或者将一个list拆包并加入另一个list：
```
[0, *[1, 2], 3] # [0, 1, 2, 3]
```
## 元组

元组是不可变列表。除了字面量用圆括号外，其它语法都类似。

```
tup1 = ('physics', 'chemistry', 1997, 2000)
tup1 = (50,) # 只包含一个元素时，需要在元素后面添加逗号
```

## 元组和列表的类型转换
```
tuple(seq)
list(seq)
```

## range
```
range(3) # 0..3 ，不含3
range(1,3) # 1..3，不含3
range(2, 10, 2) # 2,4,6,8
range(3, 0, -1) # 3, 2, 1 。
```
range 是一个类，并且是个迭代器。
构造器为 range(start, stop, step) 。输出不含 stop。step 如果不是 1 则必须指明，否则会有意外的结果。
属性和方法有：'count', 'index', 'start', 'step', 'stop'。

list 的构造器接受 range 类型的参数。也可以用解包操作符（`*`）将 range 产生的元素放入 list 或 set ：

```
[5, *range(3)] # [5, 0, 1, 2]
{*range(3)} # {0, 1, 2}
```

在 python2 中，range 返回一个 list，而 xrange 类则与 python3 的 range 类相同。

## 字典/映射 dict

dict 的key可以用不可变类型，如数字、字符串、元组，但不能用列表等可变类型。

```
dict = {'Name': 'Zara', 'Age': 7, 'Class': 'First'} # dict 的字面量

dict['Age'] = 8

del dict['Name']; # remove entry with key 'Name'
dict.clear();     # remove all entries in dict
del dict ;        # delete entire dictionary

dict[key] # 根据key查找值，如果存在直接返回值；如果不存在key，抛出 KeyError。

dict.get(key, default=None) # 根据key查找值，如果存在直接返回值；如果不存在key，返回None；也可以指定键不存在时返回的值。

dict.setdefault(key, default=None) #根据key查找值，如果存在直接返回值；如果不存在key，则先设置值为第二参数，再返回值。

len(dict) # 总量

dict.has_key(key) # 是否含有 key 这个键（无此方法？）

key in dict # 是否含有 key 这个键

dict.keys() # 获得所有的key，是个类列表对象（dict_keys），可以用 list(dict.keys()) 转化为列表。

dict.items() # 获得所有的(key,value)对，是个类列表对象（dict_items）。

dict.update(dict2) #Adds dictionary dict2's key-values pairs to dict

dict.copy() # Returns a shallow copy of dictionary dict

for k, v in dict.items(): # 迭代。k 是键，v 是值
  ...

dict = vars(obj) # 将一般对象转化为 dict
```

用元组做键时，可以在方括号中直接写元组的元素（get 方法不行）：

```
v = dict()
v['a','b'] = 1
v[('a','b')] == v['a','b'] # True
v['a','b'] == v.get(('a','b')) # True
```

defaultdict 是 dict 的子类，可以指定用方括号访问不存在键时创建/返回的值：

```
from collections import defaultdict
y = defaultdict(lambda : 'foo')
y.get('a') # None
y['a'] # 'foo'
y.get('a') # 'foo'
```
可注意到用 get 方法访问则不会创建一个值。

计数器 Counter 是 dict 的一个子类，可用于统计列表中重复条目的数量：
```
from collections import Counter

c = Counter('abcdeabcdabcaba')
c.most_common(3)                # three most common elements
    [('a', 5), ('b', 4), ('c', 3)]
sorted(c)                       # list all unique elements
    ['a', 'b', 'c', 'd', 'e']
```
Counter 的键是条目，值是其出现次数。
most_common() 输出的列表的条目是(条目, 出现次数)，按出现次数倒排。不带参数则输出所有条目。

dict 是有序的，其顺序为键值对插入的顺序（之后更新值不改变顺序）。如果要根据键/值重新排序，可以对 items 排序，然后创建新的 dict：
```
d0 = { 1: 'b', 2: 'a'}
dict(sorted(d0.items(), key=lambda it: it[0], reverse=True))
{2: 'a', 1: 'b'}
dict(sorted(d0.items(), key=lambda it: it[1]))
{2: 'a', 1: 'b'}
```

## 集合 set

集合的实现是 hashset，只有 hashable 元素可添加，否则会抛出异常。内置类型中，基本上只有不可变类型满足。

```
x = {'q', 'w', 'e', 'r', 't', 'y'} # 字面量语法
set1 = set(['a', 'b', 'a']) # 从列表创建集合，会自动去重
list(set1) # 集合转化为列表

# 添加多个元素。每个参数必须可迭代，会解包后添加。
x.update('2', '3') # 错误，参数不可迭代
x.update([4,5],[6,7]) # 添加了 4,5,6,7

#注意字符串是可迭代的，直接做参数会被拆成字符，但作为二级则不会拆包。
x.update('TU') # 添加了 'T', 'U'
x.update(['ab', 'cd']) # 添加了 'ab', 'cd'

# 添加单个元素。不会拆包。
x.add('hg')

# 删除单个元素。不会拆包。
x.remove(4)
```
其它方法：'copy', 'difference', 'difference_update', 'discard', 'intersection', 'intersection_update', 'isdisjoint', 'issubset', 'issuperset', 'pop', 'remove', 'symmetric_difference', 'symmetric_difference_update', 'union'。

set 也是个迭代器。其迭代顺序是确定的，按实际添加顺序排列。

## 简单对象，命名空间（Namespace）,vars

命名空间就是简单的类的对象。标准库中有 types.SimpleNamespace 可以直接使用[2.1]：

```
>>> from types import SimpleNamespace

>>> data = SimpleNamespace(a=1, b=2)
>>> data
namespace(a=1, b=2)
>>> data.c = 3
>>> data
namespace(a=1, b=2, c=3)
```
命名空间的属性只能用 '.' 访问，不能用下标 `[]` 访问。
命名空间与字典可以互相转化：
```
ns = SimpleNamespace(**aDict)
aDict = vars(ns)
```
用 `vars(ns)` 产生的 dict 与 ns 是共享数据的，修改其中一个也会修改另一个。如果不希望这样，可以 `vars(ns).copy()` 

argparse.ArgumentParser.parse_args() 返回的是个命名空间。

自定义的空类（以及大部分标准库的类）的对象也可以动态添加属性：
```
class C:
  pass

o = C()
o.b=1
o # <__main__.C object at 0x7f264d3de010>
o.b # 1
vars(o) #{'b': 1}
```
不过，比起 SimpleNamespace，它并不自动具有构造器和转换为字符串的函数(`__str__()`)。

要注意的是，尽管所有自定义类的祖先是 object，但 object 的对象不允许动态添加属性：
```
o = object()
o.b = 1 # AttributeError: 'object' object has no attribute 'b'
```

## 排序
常用的排序方法是 
```
sorted(iterable, key, reverse)
```
iterable 是被排序的对象，可以是list, tuple, set, dict（只排序key）等可迭代对象。
key 是个函数，需要返回用来排序的属性，可选。
reverse 是否降序排序，可选。
sorted 不修改被排序的对象，总是返回一个新建的排序后的 list 。
```
L = ["cccc", "b", "dd", "aaa"]

print("Normal sort :", sorted(L))
 
print("Sort with len :", sorted(L, key=len))
```

## 逆序
```
[*reversed(range(3))] # [2, 1, 0]
```
reversed() 接受一个迭代器（如list、range），返回其逆序的迭代器。
注意有的迭代器是不可逆序的，如 set 。

## 最大/最小

内置函数 max() min() 可以接受变长参数表、list、dict、set 等作为参数，dict 则只考虑键。如果容器为空则抛出异常。

## 平坦化、列表/迭代器的串接
[11.1]
### itertools.chain
```
import itertools

original_list = [[1, 2], [3, 4]]

flattened_iterator = itertools.chain(*original_list)

for item in flattened_iterator:
    print(item)
```
itertools.chain 是浅的。接受多个参数，每个参数是可迭代对象。返回一个迭代器。

### itertools.chain.from_iterable
```
import itertools

original_list = [[1, 2], [3, 4]]

flattened_iterator = itertools.chain.from_iterable(original_list)

for item in flattened_iterator:
    print(item)
```
itertools.chain.from_iterable 是浅的。接受一个参数，每个参数是可迭代对象。返回一个按顺序连接的迭代器。

### pandas flatten
```
from pandas.core.common import flatten

original_list = [[1, 2], [3, 4], [5, 6, 7]]

flattened_list = flatten(original_list) # flattened_list 并非 list 类型

flat_list = list(flattened_list)
```
pandas.core.common.flatten 是浅的。

### iteration-utilities.deepflatten

```
pip install iteration-utilities
```

```
from iteration_utilities import deepflatten

deeply_nested_list = [[[1, 2], 3], [4, 5, 6]]

flat_list = list(deepflatten(deeply_nested_list))

print("Flattened List:", flat_list)
```
deepflatten 是深的。

## 包的导入和搜索， import

导入同目录的或上级目录的包：
```
from .src.drink import Drink
from ..src.drink import Drink
```

尽管语法上没问题，但这通常会导致错误[3.3-3.5]。

## 类型标注（Type Annotations）、类型检查

[5.1]

python 3 语法中有可选的类型标注，例如

```
pi: float = 3.142本地实现的（native）
def headline(text: str, align: bool = True) -> str:
```

但 python 运行时不会检查类型标注。一些源码检查器和IDE会利用类型标注来检查错误或者辅助代码提示和重构等。
常用的源码检查器有 mypy 。安装： `pip install mypy` 或 `sudo apt install python3-mypy` 。

用法如下：`mypy xxx.py` 。

python 也支持复合类型的标注（以泛型的形式）：

```
from typing import Dict, List, Tuple

names: List[str] = ["Guido", "Jukka", "Ivan"]
version: Tuple[int, int, int] = (3, 7, 1)
options: Dict[str, bool] = {"centered": False, "capitalize": True}
```

直接使用类名似乎也可以：
```
a: list[int]=[1]
b: dict[str,int]={'a':3}
```

自定义类型的变量类型，可以用其类名来表示，例如 numpy.ndarray ：

```
import numpy as np
c: np.ndarray = np.zeros((2,3), 'float32')
```

## 循环

```
for i in range(1, 100):
  pass

for i in range(len(['a','b'])):
  pass
  
count = 0
while (count < 9):
   count = count + 1
```

## 容器的循环/迭代

用 for ... in 语法迭代数组中的元素。如果需要同时得知下标，可以 for ... in enumerate ：

```
for item in ['a', 'b', 'c']:
  print(item) # 'a\nb\nc\n'

for i,item in enumerate(['a', 'b', 'c']):
  print(i, item) # '0 a\n1 b\n2 c\n'
```

numpy 等实现了多维数组，也可以用 for ... in 迭代。

用内嵌 for ... in 表达式可以实现数组的映射（ 相当于 JS 数组的 map 方法 ），结合 if 表达式可实现过滤（ 相当于 JS 数组的 filter 方法 ）：

```
[a + 2 for a in [1, 2, 3]]               # [3, 4, 5]
[a + 2 for a in [1, 2, 3] if a % 2 == 0] # [4]
```

其中 a + 2 for a in [1, 2, 3] 产生一个 generator 。

set 可以像数组一样用 for ... in 迭代。

标量作为键的 dict 的 for ... in 迭代，参数是键：

```
d = dict()
d['a'] = 1
for i1 in d:
  print('i1=', i1) # i1= a
```

如果键是2元的元组，则会把键自动拆包：

```
d = dict()
d[('a','b')] = 1

for i1, i2 in d:
  print('i1=', i1, 'i2=', i2) # i1= a i2= b
```

多于2元的键，不能用此方法迭代，可以在 keys() 上迭代，同样会自动拆包：

```
d = dict()
d[('a','b','c')] = 1

for i1, i2, i3 in d.keys():
  print('i1=', i1, 'i2=', i2, 'i3=', i3) # i1= a i2= b i3= c
```

如果不希望拆包，可以在 items() 上迭代，参数是键和值：

```
d = dict()
d[('a','b','c')] = 1

for i1, i2 in d.items():
  print('i1=', i1, 'i2=', i2) # i1= ('a', 'b', 'c') i2= 1
```

## 枚举
```
from enum import Enum

class CoordinatesType(Enum):
    RELATIVE = 1
    ABSOLUTE = 2

# 访问实例：
type(CoordinatesType.RELATIVE) ==  CoordinatesType # True

# 名和值
CoordinatesType.RELATIVE.name # 'RELATIVE'
CoordinatesType.RELATIVE.value # 1

# 字符串转枚举/根据名检索：__members__ 是类字典对象，键是名，值是枚举
CoordinatesType.__members__['RELATIVE'] is CoordinatesType.RELATIVE # True

# 整数转枚举/根据值检索
CoordinatesType(1) is CoordinatesType.RELATIVE # True
```
Enum 还有几个子类：
IntEnum: 也是 int 的子类，可以作为 int 参与运算和比较。
StrEnum：也是 str 的子类，可以作为 str 参与运算和比较。
IntFlag
Flag

## 函数，function

### 参数表
[1.7] 
形式：
`def f1(a, b, c=3, /, d=4, *args, e=1, f=None, **kwargs):`
参数表可分为4个部分：
* 命名的位置参数表(positional parameters)。例如`a, b, c=3, /, d=4`。最后几个参数可以用`=`指定默认值；有默认值的参数之后不能有无默认值的参数。`/` 之前是纯位置参数，`/`之后（或不写`/`）到 * 之前是位置或关键字两用参数。`/`只能出现一次。`/` 是 3.8 引入的。
* 变长的位置参数表。例如`*args`。最多出现一次。其特征在于开头的`*`，args 并非固定的名字。args 中包含除了命名的位置参数以外的所有位置参数。
  * 一种特殊形式是只写 `*`，这意味着不接受多余的位置参数，且 `*` 后至少要求有一个关键字参数。这种写法的主要用应用场景是“仅有关键字参数的函数”，例如，`f(*, a=1, b=2)`。如果函数定义为 `f(a, b, *, c=1)`，则调用时写 `f(1, 2, 3)` 是不允许的；函数定义 `f(a, b, *)` 也是不允许的。
* 命名的关键词参数表(keyword parameters)。例如 `c=1, d=None`。关键词参数都要指定默认值。
* 变长的关键词参数表，例如 `**kwargs`。最多出现一次。其特征在于开头的`**`，kwargs 并非固定的名字；也可以省略 kwargs 只保留 `*`。kwargs 中包含除了命名的关键词参数以外的关键词参数。

这4个部分都是可选的，但其顺序是固定的。例如，不能在关键词参数后，又出现位置参数。

一般的位置参数（除了在`/`之前的纯位置参数）是位置或关键字两用参数。纯位置参数则不能在调用时指定参数名。

调用函数时，位置参数的数量不匹配是错误，除非有默认值的参数或变长位置参数表可容纳不匹配的参数。

调用函数时，关键词的名字不匹配是错误，除非变长的关键词参数可容纳不匹配的参数。

变长的位置参数表类型是turple,变长的关键词参数表类型是dict。

python 术语中，形式参数（函数定义中的参数）叫做 parameters，实际参数（函数定义中的参数）叫做 arguments。

### 获取整个参数表
可以用以下代码获得整个函数的参数表：
```
args = dict(locals())
args.pop('self') # 是实例方法
args.pop('__class__') # 是构造器
args.pop('kwargs') # 有 **kwargs 参数
args.update(kwargs)
```
需要注意的地方：
* `locals()` 要在函数体的最前面，因为它返回的是所有局部变量的字典，只有在函数体的最前面调用，才能确保只返回实际参数的字典。
* `locals()` 返回的字典能够被修改，但会有难以预测的效果，所以不要修改，而是复制成另一个 dict 对象再修改。`locals()` 返回类似 dict 的对象，但不是 dict，不能用 ** 解包。
* `locals()` 不会自动解包 kwargs 参数（如果有），所以需要先弹出它，再与 args 合并。
* 如果是实例方法，则 `locals()` 多出 `self` 参数（可能改为其它名字）。
* 如果是构造器，`locals()` 多出 `self` 和 `__class__` 参数。

### 变量的作用域
一个函数体或者顶层脚本被视为一个作用域。函数可以嵌套，因此作用域也可以嵌套。
如果一个作用域内对一个变量赋值了，则此变量视为在此作用域内被定义了。单纯读取变量的操作不会定义变量。
注意，不是赋值语句后变量才被视为定义了，而是在整个作用域内都被定义了。
变量被定义后、首次赋值前的读取操作会导致抛出 UnboundLocalError 。
内层作用域定义的变量会屏蔽外层作用域的同名变量。
内层作用域如果只有读取变量的操作，则可以读取外层作用域定义的变量。
如果想在内层作用域重新赋值外层作用域定义的变量，则应当在内层作用域用 `nonlocal var_name` 语句声明此变量是在外层定义的。如果外层就是顶层，需要用 `global`。
如果想在内层作用域重新赋值顶层作用域定义的变量，则应当在内层作用域用 `global var_name` 语句声明此变量是在顶层定义的。
例子：
```
def f0():
    x = 1
    def f1():
        print(f'f1():x={x}') # x=1, 读取了外层函数定义的 x。
    f1()
    
    def f2():
        x = 2 # 内层函数再次定义了 x，屏蔽了外层函数定义的 x，外层的 x 不受影响
        print(f'f2():x={x}') # x=2
    f2()
    print(f'f0()/2:x={x}') # x=1
    
    def f3():
        # 内层函数再次定义了 x，但又试图读取外层函数定义的 x，抛出异常：UnboundLocalError: cannot access local variable 'x' where it is not associated with a value
        x = x + 2
        print(f'f3():x={x}')
    try:
        f3()
    except:
        import traceback
        traceback.print_exc()
    print(f'f0()/3:x={x}') # x=1
    
    def f4():
        print(f'f4()/1:x={x}') # 先读取，再赋值，x仍被视为内层定义的变量，读取操作抛出异常：UnboundLocalError
        x = 2
        print(f'f4()/2:x={x}')
    try:
        f4()
    except:
        import traceback
        traceback.print_exc()
    print(f'f0()/4:x={x}') # x=1
    
    def f5():
        nonlocal x # 声明此变量是在外层定义的
        x = x + 2 # 成功修改外层变量
        print(f'f5():x={x}') # x=3
    f5()
    print(f'f0()/5:x={x}') # x=3

f0()
```

### lambda
python lambda 捕获外层作用域的变量时，总是采用“根据引用捕获”的方式，因此在捕获循环中不断变化的变量时，总是会获得循环结束时的值。
要在循环中捕获变量的当前值，可以在定义 lambda 时使用默认参数值，让默认参数值绑定当前变量的值[1.9]：
```
def test_loop_lambda():
    fn_list_1 = []
    for i in range(3):
        fn_list_1.append(lambda : print(f'lambda/1: i={i}')) # 捕获 i 在循环结束时的值。
    
    fn_list_2 = []
    for j in range(3):
        fn_list_1.append(lambda k=j : print(f'lambda/2: j={k}')) # 捕获 j 的当前值。
    
    for fn in fn_list_1:
        fn()
    
    for fn in fn_list_2:
        fn()

test_loop_lambda()
```

输出：
```
lambda/1: i=2
lambda/1: i=2
lambda/1: i=2
lambda/2: j=0
lambda/2: j=1
lambda/2: j=2
```
注意这个问题不限于 lambda 函数，嵌套的函数也有类似的特性。

## 类，class

### super

引用父类构造器
```
class A(object):
    def __init__(self, ):
        super(A, self).__init__() # 等效于 super().__init__()
```
定义自身的 `__init__()` 需要 self 参数，但调用父类的构造器时，不需要传递 self 参数。
如果不需要修改父类的构造器的行为，则可以不在子类实现构造器。

python 不要求在子类构造器在第一句调用父类构造器，而是可以在后面调用。

### 继承本地实现的（native）类
本地实现的（native）类，也是允许继承的，但需要注意其各个方法的依赖关系，覆盖必要的方法。

例如继承 dict，如果要修改其数据结构，需要覆盖3个方法[1.13]：
```
__setitem__(self, key, value)
__init__(self, mapping=None, /, **kwargs)
update(self, mapping)
```

而继承 collections.UserDict （非本地实现），要修改其数据结，只需要覆盖 `__setitem__(self, key, value)` 方法[1.13]。

如果要继承 list 并修改其数据结构，则覆盖以下方法[1.14]：
```
__init__(self, iterable)
__setitem__(self, index, item)
insert(self, index, item)
append(self, item)
extend(self, other)
```
此外，如果要`+` 操作符，还需要覆盖`.__add__(), .__radd__(), .__iadd__()` 。

类似的，也可以继承 collections.UserList，实现 list-like 的数据结构。

但如果不寻求彻底改变 native 类的行为，而是附加一些行为，则不需要覆盖全部方法。

一些本地实现的（native）类不允许随意添加属性，如 list 和 dict 等。但如果继承它们，其它什么也不需要做，则可以随意添加属性[1.14]：
```
class A(list):
    pass

a = A([1, 2, 3])
a.foo='bar'
```

### 方法的绑定
类的方法可分为3类：
（1）intance method，即普通的实例方法，与实例绑定，可访问实例成员变量/方法。第一个参数是 self，实例对象。
（2）class method，类方法，与类对象绑定，可访问类成员变量/方法。第一个参数是 cls，类对象。使用 @classmethod 装饰来定义。
（3）static method，静态方法，与类方法相似，但没有第一个特殊参数。使用 @staticmethod 装饰来定义。

## 随机数

```
import random
```

方法有 'betavariate', 'choice', 'choices', 'expovariate', 'gammavariate', 'gauss', 'getrandbits', 'getstate', 'lognormvariate', 'normalvariate', 'paretovariate', 'randint', 'random', 'randrange', 'sample', 'seed', 'setstate', 'shuffle', 'triangular', 'uniform', 'vonmisesvariate', 'weibullvariate' 等。

## 时间

```
import time
time.time()
```
time.time() 返回 float 类型，单位是秒，有小数部分，起点是 1970-01-01 GMT+0。

```
import datetime

# 参数 1634732721.224 是从 1970-01-01 GMT+0 开始的秒数，即 2021-10-20T12:25:21.224Z 。
# 返回对象 datetime.datetime(2021, 10, 20, 12, 25, 21, 224000) ，2021-10-20 12:25:21.224000 ，UTC 时间。但 dd 并没有显式时区信息。
dd = datetime.datetime.utcfromtimestamp(1634732721.224)

# 另有方法 fromtimestamp() ，参数是本地时间的秒数，起点也是本地时间的 1970-01-01。
dd = datetime.datetime.fromtimestamp(1634732721.224)

dd.isoformat()
# '2021-10-20T12:25:21.224000' UTC 时间。
```

datetime.datetime 实际上有两种类型：带时区信息的，不带时区信息的。不带时区信息的datetime属于过时的API，容易引起混乱应避免使用。
不过，有些 API 没有带时区的版本，如 datetime.utcfromtimestamp()/fromtimestamp() ，需要一些技巧来解决。
参考： https://vinta.ws/code/timezone-in-python-offset-naive-and-offset-aware-datetimes.html?msclkid=dfb98882ce8811eca10173aa9f0eea8d
datetime.date 仅有不带时区的版本。

```
from datetime import datetime, timezone, timedelta

# 创建 unix 时间戳零点时间：
d0 = datetime(1970, 1, 1, tzinfo=timezone.utc) # 得到 datetime.datetime(1970, 1, 1, 0, 0, tzinfo=datetime.timezone.utc)
d0.timestamp() # 返回 unix 时间戳，单位秒，即 0.0

# 解析 unix 时间戳（1651888252924， 即 2022-05-07T01:50:52.924Z）。datetime 可以与 timedelta 加减，得到另一个 datetime。

d5 = d0 + timedelta(seconds=1651888252.924) # 得到 datetime.datetime(2022, 5, 7, 1, 50, 52, 924000, tzinfo=datetime.timezone.utc)

d5.timestamp() # 返回 1651888252.924 ，证明结果是对的。

d5.isoformat() # 返回 '2022-05-07T01:50:52.924000+00:00'

# datetime 相减得到 timedelta 对象：
dt50 = d5 - d0 # timedelta: datetime.timedelta(days=19119, seconds=6652, microseconds=924000)
# timedelta 再转化为秒数：
dt50.total_seconds() # 1651888252.924

# 创建东八区时区
tz8 = timezone(timedelta(hours=8)) # 得到 datetime.timedelta(seconds=28800)

# 转换 datetime 对象的时区
d6 = d5.astimezone(tz8) # 得到 datetime.datetime(2022, 5, 7, 9, 50, 52, 924000, tzinfo=datetime.timezone(datetime.timedelta(seconds=28800)))

d6.timestamp() # 返回 1651888252.924 ，证明结果是对的，astimezone() 不会改变实际时间。

d5 == d6 # 得到 True， 比较的是实际时间，不考虑时区的不同。

d6.isoformat() # 返回 '2022-05-07T09:50:52.924000+08:00' ，带有时区信息。

d6.utcoffset() # 返回 datetime.timedelta(seconds=28800) ，时区的偏移量。

# 当前获得时间的 datetime
dnu = datetime.now(timezone.utc)
dnl = datetime.now(tz8)

# 格式化字符串。指令都是区分大小写的。%z 为时区，%f 为秒的小数部分（尚不清楚如何指定保留的位数）。
dnl.strftime('%Y-%m-%d %H %M %S.%f %z') # 得到 '2024-04-10 18 34 06.826884 +0800' 。

# 获得 date 部分。注意丢失了时区信息。
dd6 = d6.date()
dd6.isoformat() # '2022-05-07'

# 获得 date 部分，保留时区信息。
d6d = datetime(d6.year, d6.month, d6.day, tzinfo = d6.tzinfo)
```

不带时区信息的 datetime 与带时区信息的 datetime 不可以比较，否则抛出异常。

datetime 类、date 类的构造器并不能接受字符串。要将字符串转化为 datetime/date，可以使用 `datetime.datetime.strptime(str, format_spec: str)`。

从字符串解析时间：

https://www.iditect.com/guide/python/python_howto_read_datetime.html?msclkid=5bef811dce9311eca61d0a3dbad8b29a
```
datetime.strptime('2024-03-05 12:18:14,137', '%Y-%m-%d %H:%M:%S,%f')
# datetime.datetime(2024, 3, 5, 12, 18, 14, 137000)
```
## 文件系统

```
import os

os.makedirs(os.path.join('..', 'data'), exist_ok=True)
data_file = os.path.join('..', 'data', 'house_tiny.csv')
with open(data_file, 'w') as f:
    f.write('NumRooms,Alley,Price\n')  # 列名
    f.write('NA,Pave,127500\n')  # 每行表示一个数据样本
    f.write('2,NA,106000\n')
    f.write('4,NA,178100\n')
```

```
f = open('C:\myimg.png', 'rb') # opening a binary file
content = f.read() # reading all lines, 返回类型为 bytes
content # b'\x8

f=open("binfile.bin","wb")
num=[5, 10, 15, 20, 25]
arr=bytearray(num)
f.write(arr)
f.close()
```

open() 函数所返回的 file object 类型取决于所用模式。 当使用 open() 以文本模式 ('w', 'r', 'wt', 'rt' 等) 打开文件时，它将返回 io.TextIOBase (特别是 io.TextIOWrapper) 的一个子类。 当使用缓冲以二进制模式打开文件时，返回的类是 io.BufferedIOBase 的一个子类。 具体的类会有多种：在只读的二进制模式下，它将返回 io.BufferedReader；在写入二进制和追加二进制模式下，它将返回 io.BufferedWriter，而在读/写模式下，它将返回 io.BufferedRandom。 当禁用缓冲时，则会返回原始流，即 io.RawIOBase 的一个子类 io.FileIO。按照文本模式打开的文件，无法写入bytes，按二进制模式打开的则可以。

处理非ASCII文本文件：
```
import codecs
with codecs.open(data_file, 'w', encoding='utf-8') as f:
    f.write('中文\n')
```

按行读文件：
```
# 读取一行数据，返回字符串
f = open("my_file.txt")
line = f.readline()
print(byt)

# 读取所有的行，返回字符串的列表
f = open("my_file.txt")
lines = f.readlines() 

# 读取所有的行，返回二进制串的列表
f = open("my_file.txt",'rb')
lines = f.readlines() 
```

## 当前目录

```
os.getcwd()
os.chdir(path)
```

## 特殊文件/目录


`__file__` ：全局变量，当前模块文件的路径。


## 列出/搜索/搜集文件

```
import os
os.listdir(path) # 返回条目列表，list[str] 类型。

import glob
fnames: list[str] = glob.glob('data/train/*/det/det.txt')

# glob.escape 转义特殊字符；recursive 表示递归搜索。
fnames: list[str] = glob.glob(glob.escape(base_dir) + '/**', recursive = True)
```

## 路径操作（文件系统）

```
import os.path
file_name = os.path.basename('/foo/abc.txt') # abc.txt
name, ext = os.path.splitext('/foo/abc.txt') # ('/foo/abc', '.txt')
os.path.dirname('/foo/abc.txt') # /foo
os.path.join('/foo/abc.txt', 'cf.mp4') # '/foo/abc.txt/cf.mp4'
```

## 符号链接

```
```

## shutil --- 高阶文件操作

```
shutil.copyfile(src, dst, *, follow_symlinks=True) # src dst 是文件。
shutil.copy(src, dst, *, follow_symlinks=True) # src 是文件，dst 可以是文件或目录。

shutil.rmtree(src, ignore_errors=False, onerror=None) # 删除目录树
```

## json
```
import json

jsonfile = open(path,'r',encoding='utf-8')
result = json.load(jsonfile)
jsonfile.close()

f = open(jsonpath, 'w',encoding='utf-8')
f.write(json.dumps(strjson,ensure_ascii=False))
f.close()

def write_json(path, obj):
    annot_str = json.dumps(obj, indent=2, ensure_ascii=False)
    with codecs.open(path, 'w', 'utf-8') as out_file:
        out_file.write(annot_str)
        print('write output: ', path)
```

将对象转换为可json序列化的对象：

```
from enum import Enum
import numpy as np

def copy_for_json(obj):
    t = type(obj)
    if obj is None or t == int or t == float or t == str:
        return obj
    elif isinstance(obj, list) or isinstance(obj, tuple):
        return [ copy_for_json(item) for item in obj ]
    elif isinstance(obj, dict):
        new_o = {}
        for key in obj:
            new_key = key if type(key) == str else f'{key}'
            new_o[new_key] = copy_for_json(obj[key])
        return new_o
    elif isinstance(obj, Enum):
        return obj.name
    elif hasattr(obj, '__dict__'):
        return copy_for_json(obj.__dict__)
    elif isinstance(obj, np.ndarray) or isinstance(obj, np.number):
        return obj.tolist()
    else:
        s = f'{obj}'
        return s
```

## zip 文件操作

```
import zipfile
with zipfile.ZipFile(archive_path, 'w') as archive:
    archive.writestr("annotations.json", cvat_annot_str)
    archive.writestr("task.json", task_str)
    archive.write(video_path, 'data/' + video_file_name, zipfile.ZIP_STORED)
```

## 标准输入输出、控制台

```
#read multiple strings from user
firstName = input('Enter your first name: ')
lastName = input('Enter your last name: ')

print('Hello',firstName, lastName)
```

```
line = sys.stdin.readline().strip()
```

```
sys.stderr.write('please input image file path\n')
sys.stdout.write(sResult)
sys.stdout.flush()
```

## 命令行参数解析

```
import argparse

parser = argparse.ArgumentParser(description=__doc__)

parser.add_argument(
        "--model_dir",
        type=str,
        default=None,
        help=("Directory include:'model.pdiparams', 'model.pdmodel', "
              "'infer_cfg.yml', created by tools/export_model.py."),
        required=True)
parser.add_argument(
        "--trt_opt_shape",
        type=int,
        default=640,
        help="opt_shape for TensorRT.")
parser.add_argument(
        '--use_dark',
        type=bool,
        default=True,
        help='whether to use darkpose to get better keypoint position predict ')

parser.add_argument("--label_list", type=str, nargs='+',
                        help='模型输出的类别列表。如果模型没有自带类别列表，或者属于开放词汇表模型（如 OpenAI CLIP），则应当提供此参数。')
        
FLAGS = parser.parse_args() # FLAGS 是类 dict 对象
```

```
>>> parser = argparse.ArgumentParser(prog='game.py')
>>> parser.add_argument('move', choices=['rock', 'paper', 'scissors'])
>>> parser.parse_args(['rock'])
Namespace(move='rock')
>>> parser.parse_args(['fire'])
```

## 内存文件： StringIO， BytesIO
[2.1, 2.2]
StringIO, BytesIO 具有类似 file 的接口，但读写都是在内存中，在一些 API 中可以代替 file 。

## 日志

### 基本用法
logging 的默认输出目标是 stderror 。

```
import logging
logging.getLogger().setLevel(logging.INFO)
logging.info('ParaInferExecutor:starting task: %s (%d); pending: %d' % (task.inputImagePath, task.sn, self.pendingTask.qsize()))
logging.exception('ParaInferExecutor: uncaught task error: %s (%d);' % (task.inputImagePath, task.sn))
```

### 常见问题：丢失日志

太长不看版：建议在所有使用 logging 的源文件开头总是调用一次 `logging.basicConfig()`，以避免日志丢失问题。

在下面的两个例子中，只有 'warn message' 和 'error message' 会被输出到 stderr，而 'debug message' 和 'info message' 会丢失。
例1：
```
import logging

logger = logging.getLogger('simple_example')
logger.setLevel(logging.DEBUG)

logger.debug('debug message')
logger.info('info message')
logger.warning('warn message')
logger.error('error message')
```
例2：
```
import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

logger.debug('debug message')
logger.info('info message')
logger.warning('warn message')
logger.error('error message')
```
原因如下：
首次调用`logging.getLogger(...)` 时，会自动创建 Logger 对象，但此时它是没有完全初始化的，缺少 handler（`logger.handlers` 为空列表），这就造成日志无法被处理而丢弃。
但此时 warning 以上的日志有特殊处理，仍会被输出到 stderr 。

下面3个例子不会丢失日志：

例3：
```
import logging

logging.basicConfig()

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

logger.debug('debug message')
logger.info('info message')
logger.warning('warn message')
logger.error('error message')
```

例4：
```
import logging

logging.getLogger().setLevel(logging.DEBUG)

logging.debug('debug message')
logging.info('info message')
logging.warning('warn message')
logging.error('error message')
```

例5：
```
import logging

logging.basicConfig()

logger = logging.getLogger('simple_example')
logger.setLevel(logging.DEBUG)

logger.debug('debug message')
logger.info('info message')
logger.warning('warn message')
logger.error('error message')
```

例3和例2的区别在于，首次输出日志前调用了 `logging.basicConfig()`，这会给 root logger ( 即 `logging.getLogger()` 返回的对象) 添加一个默认的 handler，其效果正是向 stderr 输出日志，因此例3不会丢失日志。例4中，`logging.debug|info()` 会在 root logger 没有 handler 时自动调用 `logging.basicConfig()`，因此也不会丢失日志。例5中，当非 root 的 logger `logging.getLogger('simple_example')` 没有自己的 handler 时，会委托自己的上级 logger 输出日志，这最终会委托到 root logger，因此 `logging.basicConfig()` 也有效地解决了日志丢失的问题。

总结：建议在所有使用 logging 的源文件开头总是调用一次 `logging.basicConfig()`，以避免日志丢失问题。重复调用 `logging.basicConfig()` 不会有副作用，它对已经有 handler 的 logger 什么也不做。

### 日志级别
日志级别为整数，从 DEBUG(10) 到 FATAL(50) 。级别越大，输出的日志越少。
```
logging.getLevelNamesMapping()
{'CRITICAL': 50, 'FATAL': 50, 'ERROR': 40, 'WARN': 30, 'WARNING': 30, 'INFO': 20, 'DEBUG': 10, 'NOTSET': 0}
```

### 让日志带有时间戳。

例1:
```
logging.basicConfig(level=logging.INFO, format='[%(asctime)s] [%(process)d] [%(name)s] [%(levelname)s]: %(message)s')
```
输出形如：
```
[2024-04-18 17:24:06,647] [1479472] [vision_predict_service__conf] [INFO]: on_starting: loading config_path=./vision_predict_service.conf.json
```

例2：
参考：https://pythonhint.com/post/1365664850001243/how-to-display-date-format-using-python-logging-module

```
class CustomFormatter(logging.Formatter):
    t0 = time.time()
    relative_timestamp = True
    def formatTime(self, record, datefmt=None):
        if self.relative_timestamp:
            return f'T+{record.created - self.t0:.4f}s'
        dt = datetime.datetime.fromtimestamp(record.created)
        if datefmt:
            return dt.strftime(datefmt)
        else:
            return dt.strftime('%Y-%m-%d %H:%M:%S.%f')

logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = CustomFormatter('%(asctime)s %(levelname)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S.%f')
handler.setFormatter(formatter)
logger.addHandler(handler)

logging.getLogger().setLevel(logging.INFO)
```

### 条件输出日志
如果创建日志消息本身的开销较大，可以使用条件输出日志：
```
logger = logging.getLogger('foo')
#...
if logger.getEffectiveLevel() <= logging.DEBUG:
    logger.debug(f'predict:done:time_pred={t1 - t0:.4f},time_draw={t2 - t1:.4f}, image_path_list={image_path_list}')
```
注意，logger.level 不一定是实际使用的日志级别，因为可能使用上级 logger 的级别。应使用 `logger.getEffectiveLevel()` 获得有效日志级别。

## 延迟/sleep

```
import time
time.sleep(3) # 等待 3 秒
```
time.sleep() 可以在任何线程中调用，一个线程 sleep 时其它线程仍可执行；信号处理不影响等待时间。
time.sleep() 无法提前被唤醒；如果需要提前唤醒，可以用 Event.wait() 。

在 async 代码中，可以用 asyncio.sleep() （返回 coroutine）来代替 time.sleep() ：

```
import asyncio
import datetime

async def foo():
  await asyncio.sleep(1)

asyncio.run(foo())
```

## 异常处理

### stack trace
python 异常的 stack trace 不是 Exception 对象的属性，而是需要用 `sys.exc_info()` 获取，用 `traceback.format_tb()` 格式化。
获取 stack trace 应当及时，否则下一个异常发生后，或者当前线程发生切换后，就无法获得正确的 stack trace 。

```
import sys
import traceback

# 结果形如 '  File "/usr/lib/python3.9/concurrent/futures/thread.py", line 52, in run\n    result = self.fn(*self.args, **self.kwargs)\n  File "/home/rd/project/mark-tool2.git/paddle_infer_predictor.py", line 174, in do_predict\n    input_tensor.copy_from_cpu(input_data)\n  File "/home/rd/opt/venv/v39-1/lib/python3.9/site-packages/paddle/fluid/inference/wrapper.py", line 40, in tensor_copy_from_cpu\n    raise TypeError(\n'

''.join(traceback.format_tb(sys.exc_info()[2]))
```

### 异常嵌套/级联 (exception chaining)
https://www.tutorialspoint.com/python/python_exception_chaining.htm

Exception 的 `__context__` 和 `__cause__` 属性可以表示上级的异常，但似d乎无法直接设置这两个属性。

## concurrent.futures, Future, Executor

### Executor
Executor 的具体实现有 concurrent.futures.ThreadPoolExecutor 和 concurrent.futures.ProcessPoolExecutor 。

```
submit(fn, /, *args, **kwargs) -> Future

map(func, *iterables, timeout=None, chunksize=1) -> iterables

shutdown(wait=True, *, cancel_futures=False)
```

Executor 没有查询内部状态的API，如队列长度（运行中和等待中的任务总数）。具体实现通常有一些内部状态可以查询，例如：

```
len(ProcessPoolExecutor._pending_work_items)
# qsize() 是等待队列的长度，不包括正在执行的任务的数量。
ThreadPoolExecutor._work_queue.qsize()
```
就是队列长度。

### concurrent.futures.Future

将可调用对象封装为异步执行。Future 实例由 Executor.submit() 创建。

```
cancel()

    尝试取消调用。 如果调用正在执行或已结束运行不能被取消则该方法将返回 False，否则调用会被取消并且该方法将返回 True。

cancelled()
    如果调用成功取消返回 True。

running()

    如果调用正在执行而且不能被取消那么返回 True 。

done()

    如果调用已被取消或正常结束（包括抛出异常）那么返回 True。

result(timeout=None)

    返回调用返回的值。如果调用还没完成那么这个方法将等待 timeout 秒。如果在 timeout 秒内没有执行完成，concurrent.futures.TimeoutError 将会被触发。timeout 可以是整数或浮点数。如果 timeout 没有指定或为 None，那么等待时间就没有限制。

    如果 futrue 在完成前被取消则 CancelledError 将被触发。

    如果调用引发了一个异常，这个方法也会引发同样的异常。

exception(timeout=None)
   返回抛出的异常。如果以没有异常的形式结束，返回 None。

add_done_callback(on_finish: function)
   当此实例以任何方式（正常、异常、撤销）结束时调用 on_finish 回调。on_finish 可能不是在调用 add_done_callback() 的线程中被调用的，而是在调用 set_result()/set_exception()/cancel() 的线程中被调用的。
```

用于实现的方法：

```
set_running_or_notify_cancel()
    Future 实例创建后调用一次，让它进入 running 状态。

set_result(result)
    设置正常结束的结果。

set_exception(exception)
```

基于 Future 的 API 一般是在多线程环境下使用的。
用户代码要实现返回 Future 的API时，可直接创建 Future 对象，然后在适当时机调用以上方法。
如果要串接多个 Future ，可以添加回调（ add_done_callback() ），注意回调可能在不同的线程被调用。

注意，对已经结束的 Future 对象调用以上`set_xxx`方法都会引发 RuntimeError 。如果确实无法避免重复调用以上方法，可以事先检查是否结束：
```
if not future.done():
    try:
        future.set_result(result) # 或者 future.set_exception(e)
    except:
        pass
```
在多线程环境下，future 可能在 `future.done()` 和 `future.set_result()` 之间结束，因此后者仍然可能发生异常，需要捕获。

## 线程

### Thread 类
```
import threading

threading.Thread(target=outputLoop, args=(exe,)).start()

threading.Thread(target=self.inferThreadRun, args=(tfPoseEstimator,)).start()
```
target 可以是普通函数，类静态函数，也可以是类实例方法。args 将作为参数传给 target ，类实例方法会正确获得 self 参数。

```
th = threading.current_thread() # 当前 Thread 对象
th.native_id # 操作系统级线程id，正整数
th.name # 线程名
th.ident # 线程id，正整数，只存在于 python 内部。
```

### cpython 和线程实现，GIL 和自由线程
[6.3-6.5]
cpython 中的 python 线程（Thread 类）一般是对应操作系统级线程，但任何时刻只有一个 python 线程在 CPU 上执行，这是通过 GIL 实现的。
cpython 的线程调度属于“粗粒度的抢占式”，因此并不意味着完全不用担心线程安全问题。
一个 python 线程在等待 IO 或锁或事件时，cpython 会试图调度其它线程执行；一个线程在执行CPU计算一段时间后，可能会被其它线程抢占。
因此，cpython 中的多线程虽然不能利用多核CPU（仅指纯python代码），但还是可以实现多路 IO。
有的库如 numpy、opencv 的操作的底层是本地代码，执行期间可能主动释放 GIL（执行完再获取），因此使用这些库较多的 python 代码是可能通过多线程来利用多核CPU的。
未来 cpython 将取消 GIL，从而能利用多核CPU，被称为“自由线程（free threading）”。这会发生在 python 3.13。

python 3.13 的二进制版本分别是常规版本和“自由线程（free threading）“版，后者的版本号带有”t“后缀。两者的二进制接口是不兼容的，因此本地代码模块也是互不兼容的。
目前“自由线程“版的主要障碍是大量有本地代码模块的python包需要移植。目前（2025.2）自由线程的兼容情况有[6.7-6.8]：
兼容的：
numpy
pillow
onnxruntime (1.20+)

不兼容的[6.7-6.8]：
opencv （ https://github.com/opencv/opencv-python/issues/1029 ）
pyO3 (可忽略警告)
orjson  (可忽略警告，但可能运行错误)

python 3.13t 解释器可以工作在 GIL 或者 free threading 模式，这可以通过两种方式来切换[6.6]：
1. `PYTHON_GIL=0|1` 环境变量。1 表示启用 GIL，0 或不设置表示启用 free threading。
2. `python -X gil=0|1 ...` 命令行参数。1 表示启用 GIL，0 或不设置表示启用 free threading。优先级高于 PYTHON_GIL 。

python 代码可以感知其运行的线程模式：
```
import sys
sys._is_gil_enabled()
```

或者感知其解释器是否有 free threading 能力（不论是否启用）：
```
import sysconfig
sysconfig.get_config_var("Py_GIL_DISABLED")
```

解释器的线程模式在运行时也是可能改变的，例如：
```
# 启动 python 3.13t 交互式解释器，默认为 free threading 模式：
import sys
sys._is_gil_enabled() # False

# 导入 orjson 包，因为其尚未支持 free threading [6.7]，这会导致解释器发出警告，并回到 GIL 模式：

import orjson
<frozen importlib._bootstrap>:488: RuntimeWarning: The global interpreter lock (GIL) has been enabled to load module 'orjson.orjson', which has not declared that it can run safely without the GIL. To override this behavior and keep the GIL disabled (at your own risk), run with PYTHON_GIL=0 or -Xgil=0.

sys._is_gil_enabled() # True
```

不过，尚未有公共 API 来切换线程模式。要避免不支持 free threading 的包改变线程模式，可以设置 PYTHON_GIL=0 或 -Xgil=0。

### 队列

queue.Queue 可用于在线程间传递数据，并自动阻塞和唤醒线程。Queue 是线程安全的。

```
q = queue.Queue(0) # 参数是最大长度。0 表示不限制。
q.put(o) # 超出最大长度时，阻塞直到不超出。
q.qsize()
q.get_nowait() # 如果非空则返回第一个，否则 Empty exception
q.get() # 如果非空则返回第一个，如果空则阻塞到非空。等效于 q.get(block=Ture, timeout=None)
q.get(block=Ture, timeout=1.0) # 如果空则阻塞最多 1.0 秒，如果仍然空则抛出异常。
```

queue.PriorityQueue
```
q = queue.PriorityQueue(0)
q.put((2,o2))
q.put((1,o1))
sn, o = q.get()
```
从 PriorityQueue 中取出元素时，优先级小的元素先出。

### 锁
* class threading.RLock 可重入锁，同一个线程可获取无限次。
* class threading.Lock 不可重入锁，同一个线程可获取最多一次，尝试重复获取会死锁。

```
some_lock = RLock()

with some_lock:
    # do something...
```
等效于
```
some_lock.acquire()
try:
    # do something...
finally:
    some_lock.release()
```

### 条件变量
class threading.Condition(lock=None)

Condition 内部使用一个锁对象（Lock/RLock），可以创建时传入，或让它自动创建。
一般可以把 Condition 放到 with 语句中，这样可以自动加锁、解锁。调用 wait/notify 时均要求已经获得了 Condition 的锁。

```
cond = Condition()
#...
with cond:
    cond.notify()
    cond.notify_all()

#...
with cond:
    cond.wait(timeout=5.0) # 等待期间，本线程会暂时释放 cond 的锁。
```

### ThreadPoolExecutor

```
# class concurrent.futures.ThreadPoolExecutor(max_workers=None, thread_name_prefix='', initializer=None, initargs=())

import sys
from concurrent.futures import ThreadPoolExecutor

print(f"nogil={getattr(sys.flags, 'nogil', False)}")

def fib(n):
    if n < 2: return 1
    return fib(n-1) + fib(n-2)

threads = 8
if len(sys.argv) > 1:
    threads = int(sys.argv[1])

with ThreadPoolExecutor(max_workers=threads) as executor:
    for _ in range(threads):
        future = executor.submit(lambda: print(fib(34)))
```
ThreadPoolExecutor 继承自抽象类 concurrent.futures.Executor 。
Executor.submit(function, args) 用来提交一个任务，返回一个 Future 对象 [7.5]。

## 进程

### 本进程
```
import os
os.getpid()
```

### 设置进程/线程名
```
pip install setproctitle
```

```
import setproctitle
setproctitle.setproctitle(title)
setproctitle.getproctitle()
```

### 异构子进程
[8.1-8.4]
```
import subprocess

# 返回 subprocess.CompletedProcess
# capture_output=True ，则 res.stdout 是二进制串/字符串，否则是 stream 对象（默认）。
# encoding='xxx' 将 run() 置于文本模式，即自动解码输出， res.stdout 是普通字符串；encoding 默认是 None，res.stdout 是二进制串。
# errors=True 和 universal_newlines=True 也会将 run() 置于文本模式。
res = subprocess.run(["ls", "-l", "/dev/null"], capture_output=True, encoding='utf-8')

res.returncode
res.stdout # 如果选择文本模式，类型为 str，否则为 bytes sequence
res.stderr

# 也可以将命令和参数写到一个字符串里。
# shell=True 表示开一个 shell 来执行。
# check=True 表示检查返回值，不为0则抛出异常 subprocess.CalledProcessError 。
subprocess.run("exit 1", shell=True, check=True) 

# check_output(...)等效于 run(..., check=True, stdout=subprocess.PIPE).stdout
# stdout=subprocess.PIPE, stderr=subprocess.PIPE 等效于 capture_output=True
output = subprocess.check_output("ls non_existent_file; exit 0", stderr=subprocess.STDOUT, shell=True)

# 返回状态码。
retcode = call("mycmd" + " myarg", shell=True)
```

部分参数[8.4]：
```
    args: The command to run and its arguments, passed as a list of strings.
    capture_output: When set to True, will capture the standard output and standard error.
    text: When set to True, will return the stdout and stderr as string, otherwise as bytes.
    check: a boolean value that indicates whether to check the return code of the subprocess, if check is true and the return code is non-zero, then subprocess `CalledProcessError` is raised.
    timeout: A value in seconds that specifies how long to wait for the subprocess to complete before timing out.
    shell: A boolean value that indicates whether to run the command in a shell. This means that the command is passed as a string, and shell-specific features, such as wildcard expansion and variable substitution, can be used.
```

### 同构子进程 multiprocessing.Process
简称 mp.Process 。mp.Process 创建的是 python 进程，用户可以指定一个此进程要运行的 python 函数及其参数，当这个函数执行完毕时，此进程就退出；但如果此函数抛出异常，此进程默认不退出，只是打印异常信息，如果希望抛出异常时退出，应自行捕获异常，然后调用 `sys.exit()` 。

mp.Process 进程的创建方式有 fork，spawn, fork server 等，在 posix（ 除了 mac ）上默认为 fork，在 windows 和 mac 上默认为 spawn 。fork 的语义就对应 posix 的 fork 系统调用，子进程完全复制了父进程（除了线程）。spawn 的语义有些含糊，按文档描述是与父进程共享最低限度的资源。fork 的开销要低于 spawn。

如果父进程是单线程的，则 mp.Process 的 fork 方式是没问题的；但如果父进程是多线程的（包括在本地代码中创建的线程），则有概率发生死锁，且较难防范[8.5, 8.6]。因此有建议将 posix 上的 mp.Process 的默认进程的创建方式也改为 spawn ，python 3.14 可能做出这个改动 [8.5]。作为一般用户，建议默认使用 spawn ，仅在对进程启动速度有高要求、且能确保父进程是单线程时使用 fork。

spawn 方式下，target, args, kwargs 参数均需要可被 pickle 序列化，而 fork 方式无此要求。基本数据类型、仅含有可序列化成员的内置数据结构（list、dict等）、一般的函数、类、mp.Queue 等都是可序列化的。而闭包（嵌套函数）、Future 则不可以。类实例则要看其属性是否都可以序列化。

```
 class multiprocessing.Process(group=None, target=None, name=None, args=(), kwargs={}, *, daemon=None)

    进程对象表示在单独进程中运行的活动。 Process 类拥有和 threading.Thread 等价的大部分方法。

    应始终使用关键字参数调用构造函数。 group 应该始终是 None ；它仅用于兼容 threading.Thread 。 target 是由 run() 方法调用的可调用对象。它默认为 None ，意味着什么都没有被调用。 name 是进程名称（有关详细信息，请参阅 name ）。 args 是目标调用的参数元组。 kwargs 是目标调用的关键字参数字典。如果提供，则键参数 daemon 将进程 daemon 标志设置为 True 或 False 。如果是 None （默认值），则该标志将从创建的进程继承。

run()

    表示进程活动的方法。

    你可以在子类中重载此方法。标准 run() 方法调用传递给对象构造函数的可调用对象作为目标参数（如果有），分别从 args 和 kwargs 参数中获取顺序和关键字参数。

start()

    启动进程活动。

    这个方法每个进程对象最多只能调用一次。它会将对象的 run() 方法安排在一个单独的进程中调用。

join([timeout])

    如果可选参数 timeout 是 None （默认值），则该方法将阻塞，直到调用 join() 方法的进程终止。

pid

    返回进程ID。在生成该进程之前，这将是 None 。

exitcode

    The child's exit code. 


terminate()

    终止进程。 在Unix上，这是使用 SIGTERM 信号完成的；在Windows上使用 TerminateProcess() 。 请注意，不会执行退出处理程序和finally子句等。

kill()
```

要指定进程的创建方式为 spawn ，可以：
```
import multiprocessing as mp

def workerMain(proc_args: dict):
    print('workerMain')

if __name__ == '__main__':
    # 创建 mp_context 对象
    ctx = mp.get_context('spawn') # or fork, forkserver
    # 用 ctx.* 代替 mp.* ，例如 Queue 和 Process
    downlink = ctx.Queue()
    proc_args = dict(downlink=downlink)
    proc = ctx.Process(workerMain, name="foo", args=(proc_args,))
    proc.start()
```

注意，spawn 方式不允许在顶层脚本中使用，而是要放到 `if __name__ == '__main__'` 块中。

### 同构子进程池 multiprocessing.Pool

功能上很类似 ProcessPoolExecutor ，但使用的是较老的 API，即用 multiprocessing.pool.AsyncResult 来表示结果，而非 Future 。

```
class multiprocessing.pool.Pool([processes[, initializer[, initargs[, maxtasksperchild[, context]]]]])
```

### ProcessPoolExecutor

继承自抽象类 concurrent.futures.Executor

```
class concurrent.futures.ProcessPoolExecutor(max_workers=None, mp_context=None, initializer=None, initargs=())
```

ProcessPoolExecutor 中的任何进程崩溃后，整个 ProcessPoolExecutor 都会不可用，引发 BrokenProcessPool 异常。

传入 submit() 和 map() 的函数也可以是实例方法，例如以 submit(self.fn, arg1...) 的形式调用。

ProcessPoolExecutor 的 submit() 和 map() 除了批量的差异，似乎在传递 `self` 参数的实现上也有差异，使用后者时可能出现错误：
`TypeError: cannot pickle '_thread.lock' object`。

#### mp_context

```
import multiprocessing
multiprocessing.get_all_start_methods()
['fork', 'spawn', 'forkserver']

multiprocessing.get_context() 
<multiprocessing.context.ForkContext object at 0x7f264d478d10>

executor = ProcessPoolExecutor()
print(executor._mp_context)
<multiprocessing.context.ForkContext object at 0x7f264d478d10>

# create a start process context
context = multiprocessing.get_context('spawn')
print(context)
<multiprocessing.context.SpawnContext object at 0x7f264d478d50>
executor = ProcessPoolExecutor(mp_context=context)
print(executor._mp_context)
<multiprocessing.context.SpawnContext object at 0x7f264d478d50>
```

### 进程退出事件处理器 atexit

```
import atexit
atexit.register(func, *args, **kwargs)
atexit.unregister(func)
```
atexit 有效的情况：程序正常退出，包括 main 模块执行完毕或者调用 sys.exit()；因为可处理信号而退出。
atexit 无效的情况：调用 os._exit() 退出（python 标准库一般不会这样做）；python 严重内部错误；因为不可处理的信号而退出（如SIGKILL）；ProcessPoolExecutor 的 worker 进程的退出，不论因为什么。

要在 ProcessPoolExecutor 的 worker 进程中做清理，似乎没有100%可靠的办法。相对可行的办法是在关闭 ProcessPoolExecutor 前，提交数量等于 worker 数的任务来让各  worker 进程做清理。例如：

```
# worker 退出前要执行的任务
def on_worker_end():
    print('on_worker_end')

pool = ProcessPoolExecutor(3)
# 等待现有任务执行完毕
while len(pool._pending_work_items) > 0:
    sleep(0.1)

# 提交数量等于 worker 数的 on_worker_end 任务
res_list: list[Future] = [ pool.submit(on_worker_end) for _ in range(len(pool._processes)) ]
for res in res_list:
    ret = res.result()
self.pool.shutdown()
```

### 进程间通信

multiprocessing （简称 mp）模块下的多种数据结构可以用于进程间通信。如果使用自定义启动方式，则改为从 mp_context 访问。

* Queue：跨进程队列。基于 PIPE 实现，要求传递的对象可 pickle 序列化。一般的函数、类静态函数都可以传递，但传递类实例函数时会将实例一起传递，开销较大，需要慎重使用。
* Condition、Lock：跨进程同步对象。

如果子进程是通过 fork 方式创建的，则以上进程间通信对象可以直接被子进程访问；如果是通过 spawn 方式创建的，则进程间通信对象应在初始化时作为参数传递给子进程。

## 多解释器（子解释器， Multiple Interpreters, subinterpreters）

### 概述
长期以来 python 允许在同一进程中运行多个独立的 python 解释器实例，只允许利用 C API 启动；3.12起，同一进程的多解释器的GIL互相独立，通过实验性的 _xxsubinterpreters 包(3.13 改为 _interpreters）提供了 python API，但尚不完整。
多解释器可以获得比多线程更好的多核并发性能，同时避免多进程开销，但一般不会共享python代码和数据（一般靠序列化），因此是介于多线程和多进程之间的并发计算机制。
有相当一部分本地扩展与多解释器不兼容，主要问题是使用了进程级别的共享数据。

多解释器的 API 见 pep-0734 [13.3] 。
第三方实现（2025.2 不完整）见 [13.2] 。

python 3.14 提供了 concurrent.futures.InterpreterPoolExecutor ，利用多个解释器并发执行任务。

## pickle
pickle 依赖严格的类型定义，这意味着保存和加载时，类的定义是完全相同的。否则，pickle.load 会报告 ModuleNotFoundError 。

## 异步操作、协程

* asyncio.Future
  * 可 await
  * 不能作为 async 函数的返回值
  * 非线程安全，设计上被用于单线程的异步 IO 模式。
  * 主要方法
    * .result() 获得结果。如果没结束，会阻塞。
    * .done() 判断是否结束
    * .set_result(res) 设置结果使其结束
    * .set_exception(res)

* concurrent.futures.Future
  * 与 asyncio.Future 的接口大致相同。
  * 不可 await
  * 可用 asyncio.wrap_future() 转换为 asyncio.Future
    
```
Awaitable
    @abstractmethod
    def __await__(self):
      yield
    
class Coroutine(Awaitable):
    __slots__ = ()

    @abstractmethod
    def send(self, value):
        ...

    @abstractmethod
    def throw(self, typ, val=None, tb=None):
        ...

    def close(self):
```

## LRU 缓存

Python 的 3.2 版本中，引入了一个非常优雅的缓存机制，即 functool 模块中的 lru_cache 装饰器，可以直接将函数或类方法的结果缓存住，后续调用则直接返回缓存的结果。
原型：

```
@functools.lru_cache(maxsize=None, typed=False)
```
例子
```
from functools import lru_cache

@lru_cache(None)
def add(x, y):
    print("calculating: %s + %s" % (x, y))
    return x + y

print(add(1, 2))
print(add(1, 2))
print(add(2, 3))
```

缓存是按照参数作为键，所有参数必须可哈希。

## URL
[12.1]

```
from urllib.parse import urlparse, unquote, unquote_plus, urlencode

o = urlparse("http://docs.python.org:80/3/library/urllib.parse.html?"

             "highlight=params#url-parsing")

o
ParseResult(scheme='http', netloc='docs.python.org:80',
            path='/3/library/urllib.parse.html', params='',
            query='highlight=params', fragment='url-parsing')

o.scheme
'http'

o.netloc
'docs.python.org:80'

o.hostname
'docs.python.org'

o.port

unquote(o.path) # 解码 %xx 

unquote_plus(o.path) # 解码 %xx 和 '+'（空格）

urlencode(unquote(o.path)) # 编码为 %xx 
```

## 网络请求，http

### requests
[10.1]
安装：
```
pip install requests
```
例子：
```
def detect_head(image_path_list: list[str]):
    req_body = {
        "unikey": "12331",
        "image_path_list": image_path_list,
        "ext":{
            "id":"xxxx",
            "trace":"trace"
        }
    }
    res = requests.post(detect_person_head_url, json=req_body) # 参数 json 接收可以序列化为 json 的对象。
    
    if not res.ok:
        res_body = res.text
        raise make_exception('INTERNAL_ERROR', f'detect person head failed, url={detect_person_head_url}, ' +
                       f'image_path_list={",".join(image_path_list)}, status={res.status_code}, res_body={res.text}')
    
    res_body = res.json()
    return res_body['data']['result_list']
```
requests 的操作是同步的，IO操作会阻塞当前线程。为了并行执行，可以用线程池[10.3]：

```
import requests
import concurrent.futures
import time

def get_status(url):
    resp = requests.get(url=url)
    return resp.status_code

urls = ['http://webcode.me', 'https://httpbin.org/get',
    'https://google.com', 'https://stackoverflow.com',
    'https://github.com', 'https://clojure.org',
    'https://fsharp.org']

tm1 = time.perf_counter()

with concurrent.futures.ThreadPoolExecutor() as executor:

    futures = []

    for url in urls:
        futures.append(executor.submit(get_status, url=url))

    for future in concurrent.futures.as_completed(futures):
        print(future.result())

tm2 = time.perf_counter()
print(f'Total time elapsed: {tm2-tm1:0.2f} seconds')
```

也有一些在 requests 基础上实现的异步 http 客户端：requests-threads, grequests, requests-futures, and httpx。

超时控制：
```
r = requests.get('https://github.com', timeout=5) # 连接超时和读超时都设置为5秒。
r = requests.get('https://github.com', timeout=(3.05, 27)) # 连接超时和读超时分别设置为3.05秒和27秒。
```

会话：
```
s = requests.Session()

s.get('https://httpbin.org/cookies/set/sessioncookie/123456789')
r = s.get('https://httpbin.org/cookies')

print(r.text)
# '{"cookies": {"sessioncookie": "123456789"}}'
```
通过 Session 可以实现 cookies 和 http 持久连接。

Session 对象并非线程安全的。不过，如果不依赖其可变状态（如 cookies），可以当作线程安全的对象来用。

要配置 Session 的 http 行为，可以加入自定义的 HTTPAdapter ，例如：

```
from urllib3.util import Retry
from requests import Session
from requests.adapters import HTTPAdapter

s = Session()
retries = Retry(
    total=3, # 总重试次数。也可用 connect, read, status 等参数分别指定各种错误的重试次数。
    backoff_factor=0.1, # 最初的重试间隔，秒。后续间隔以 2 的幂递增。
    status_forcelist=[502, 503, 504], # 将这些状态码视为错误并重试
    allowed_methods={'POST'},
)
adpt = HTTPAdapter(max_retries=retries, pool_connections=50, pool_maxsize=50)
# 覆盖默认的 HTTPAdapter
s.mount('http://', adpt)
s.mount('https://', adpt)
```
注意，mount() 会替换掉已有的同样前缀的 HTTPAdapter，如果不存在已有的则是增加。Session 根据 mount() 的前缀来匹配请求 url，最长的匹配生效。
由于 Session 对象创建时已经有 'http://' 和 'https://' 前缀的默认的 HTTPAdapter，因此要改变默认行为，至少要替换这两个前缀，如上所示。

Session 的默认 HTTPAdapter 的配置是：
```
HTTPAdapter(pool_connections=10, pool_maxsize=10, max_retries=0, pool_block=False)
```
