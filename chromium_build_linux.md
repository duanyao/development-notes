## 参考资料

https://github.com/chromium/chromium/blob/master/docs/linux/build_instructions.md

## 设置 proxy

```
export http_proxy=http://127.0.0.1:3128;export https_proxy=http://127.0.0.1:3128
```

以下可选：
```
kate ~/.config/boto
```

```
[Boto]
debug = 0
num_retries = 10

proxy = 127.0.0.1
proxy_port = 3128
```

export NO_AUTH_BOTO_CONFIG=$HOME/.config/boto

NOTICE: You have PROXY values set in your environment, but gsutilin depot_tools does not (yet) obey them.
Also, --no_auth prevents the normal BOTO_CONFIG environmentvariable from being used.
To use a proxy in this situation, please supply those settingsin a .boto file pointed to by the NO_AUTH_BOTO_CONFIG environmentvariable.

## 获取源码
安装 depot_tools ：
```
cd ~/t-project
git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git depot_tools.git
```

下载源码
```
cd ~/t-project
mkdir chromium && cd chromium

~/t-project/depot_tools.git/fetch --nohooks --no-history chromium
```
如果这一步中间出错，可以尝试用

```
~/t-project/depot_tools.git/gclient sync --nohooks --no-history
```
来继续。如果仍然不行，就只有删除已经下载的文件，重新运行 fetch 。

~/t-project/depot_tools.git/gclient runhooks

## 安装其它依赖
cd ~/t-project/chromium/src
export LC_ALL=C
./build/install-build-deps.sh --help
./build/install-build-deps.sh --quick-check --no-syms --no-arm --no-chromeos-fonts --no-nacl --no-backwards-compatible
./build/install-build-deps.sh --no-syms --no-arm --no-chromeos-fonts --no-nacl --no-backwards-compatible

The following packages have unmet dependencies:
 binutils-aarch64-linux-gnu : Depends: binutils (= 2.28-5) but 2.30-20 is to be installed
 binutils-arm-linux-gnueabihf : Depends: binutils (= 2.28-5) but 2.30-20 is to be installed
 binutils-mips64el-linux-gnuabi64 : Depends: binutils (= 2.28-5) but 2.30-20 is to be installed
 binutils-mipsel-linux-gnu : Depends: binutils (= 2.28-5) but 2.30-20 is to be installed
 libc6-i386 : Depends: libc6 (= 2.24-11+deb9u3) but 2.27-3 is to be installed
 libkrb5-dev : Depends: krb5-multidev (= 1.15-1+deb9u1) but it is not going to be installed

sudo aptitude install -t panda libc6-i386 libkrb5-dev

sudo aptitude install gperf # 一个未列出的依赖

## 更新源码

$ git rebase-update
$ gclient sync

## 编译
```
export PATH=~/t-project/depot_tools.git/:$PATH
cd ~/t-project/chromium/src
gn help  # ~/t-project/depot_tools.git/
gn args ../out/default
gn args ../out/default --list
```

```
# ~/t-project/chromium/out/default/args.gn
enable_nacl=false
symbol_level=0
blink_symbol_level=0
is_debug = false
dcheck_always_on = true
```

gn gen ../out/default
autoninja -C ../out/default chrome  # 这一步要访问网络，最好设置 http_proxy 和 https_proxy

45GB

## 启用 hevc

测试文件： https://private-ai-parents.oss-cn-beijing.aliyuncs.com/record-video/3451226911400671022/2020-07-09GMT%2B8/1594254969764.mp4

src/third_party/ffmpeg/ffmpeg_generated.gni

append to your condition:

```
if ((is_mac && current_cpu == "x64") || (is_win && current_cpu == "x64") || (is_win && current_cpu == "x86") || (use_linux_config && current_cpu == "x64") || (use_linux_config && current_cpu == "x86")) {
  ffmpeg_c_sources += [
    "libavcodec/bswapdsp.c",
    "libavcodec/autorename_libavcodec_hevcdec.c",
    "libavcodec/hevc_cabac.c",
    "libavcodec/hevc_data.c",
    "libavcodec/hevc_filter.c",
    "libavcodec/hevc_mvs.c",
    "libavcodec/hevc_parse.c",
    "libavcodec/hevc_parser.c",
    "libavcodec/hevc_ps.c",
    "libavcodec/hevc_refs.c",
    "libavcodec/hevc_sei.c",
    "libavcodec/hevcdsp.c",
    "libavcodec/hevcpred.c",
    "libavcodec/x86/bswapdsp_init.c",
    "libavcodec/x86/hevcdsp_init.c",
    "libavformat/autorename_libavformat_hevc.c",
    "libavformat/hevcdec.c",
  ]
  ffmpeg_asm_sources += [
    "libavcodec/x86/bswapdsp.asm",
    "libavcodec/x86/hevc_deblock.asm",
    "libavcodec/x86/hevc_idct.asm",
    "libavcodec/x86/hevc_mc.asm",
    "libavcodec/x86/hevc_add_res.asm",
    "libavcodec/x86/hevc_sao.asm",
    "libavcodec/x86/hevc_sao_10bit.asm",
  ]
}
```

```
cd ~/t-project/chromium/src/third_party/ffmpeg
cp libavcodec/hevcdec.c libavcodec/autorename_libavcodec_hevcdec.c
cp libavformat/hevc.c libavformat/autorename_libavformat_hevc.c
```

~/t-project/chromium/src/third_party/ffmpeg/chromium/config/Chromium/linux/x64/config.asm:
~/t-project/chromium/src/third_party/ffmpeg/chromium/config/Chrome/linux/x64/config.asm:
```
#define CONFIG_HEVC_DECODER 1
#define CONFIG_HEVC_DEMUXER 1
#define CONFIG_HEVC_PARSER 1
```

~/t-project/chromium/src/third_party/ffmpeg/chromium/config/Chromium/linux/x64/config.h:
~/t-project/chromium/src/third_party/ffmpeg/chromium/config/Chrome/linux/x64/config.h:
```
#define CONFIG_HEVC_DECODER 1
#define CONFIG_HEVC_DEMUXER 1
#define CONFIG_HEVC_PARSER 1
```

~/t-project/chromium/src/third_party/ffmpeg/chromium/config/Chromium/linux/x64/libavcodec/codec_list.c
~/t-project/chromium/src/third_party/ffmpeg/chromium/config/Chrome/linux/x64/libavcodec/codec_list.c
    &ff_hevc_decoder,
~/t-project/chromium/src/third_party/ffmpeg/chromium/config/Chromium/linux/x64/libavcodec/parser_list.c
~/t-project/chromium/src/third_party/ffmpeg/chromium/config/Chrome/linux/x64/libavcodec/parser_list.c
    &ff_hevc_parser,
~/t-project/chromium/src/third_party/ffmpeg/chromium/config/Chromium/linux/x64/libavformat/demuxer_list.c
~/t-project/chromium/src/third_party/ffmpeg/chromium/config/Chrome/linux/x64/libavformat/demuxer_list.c
    &ff_hevc_demuxer,

~/t-project/chromium/src/media/base/mime_util_internal.cc

```
//============for hevc===========
 if (video_codec == kCodecHEVC) {
 #if BUILDFLAG(ENABLE_HEVC_DEMUXING)
   DVLOG(0) << __func__ << " >>>> HEVC IsSupported BUILDFLAG(ENABLE_HEVC_DEMUXING) ";
   return IsSupported;
 #else
   DVLOG(0) << __func__ << " >>>> HEVC IsNotSupported BUILDFLAG(ENABLE_HEVC_DEMUXING) ";
   return IsNotSupported;
 #endif
 } else {
   DVLOG(0) << __func__ << " >>>> not HEVC) ";
 }
//============end for hevc======
  if (video_codec != kUnknownVideoCodec) {
    if (!IsSupportedVideoType(
            {video_codec, video_profile, video_level, color_space})) {
      return IsNotSupported;
    }
  }
```
修改 ~/t-project/chromium/src/media/BUILD.gn

```
    "ENABLE_PLATFORM_HEVC=$enable_platform_hevc",
    "ENABLE_HEVC_DEMUXING=$enable_hevc_demuxing",  // 增加这一行
```

修改 ~/t-project/chromium/src/media/media_options.gni

```
  enable_platform_hevc = proprietary_codecs && is_chromecast
  enable_hevc_demuxing = false  // 增加这一行
```

```
cd ~/t-project/chromium/src/
gn args ../out/release-hevc
```

~/t-project/chromium/out/release-hevc/args.gn

```
enable_nacl=false
symbol_level=0
blink_symbol_level=0
is_debug = false
is_official_build = true # enable the official build level of optimization
dcheck_always_on = true

ffmpeg_branding = "Chrome"
proprietary_codecs = true
enable_hevc_demuxing = true
```

``
gn gen ../out/release-hevc
autoninja -C ../out/release-hevc chrome
```

播放hevc时，无法播放，命令行中显示：

```
[7562:13:0724/180054.663997:ERROR:batching_media_log.cc(38)] MediaEvent: {"error":"FFmpegDemuxer: no supported streams"}
[7562:1:0724/180054.664287:ERROR:batching_media_log.cc(35)] MediaEvent: {"pipeline_error":14}
```
~/t-project/chromium/out/release-hevc/args.gn

```
enable_nacl=false
symbol_level=0
blink_symbol_level=0
is_debug = false
is_official_build = true # enable the official build level of optimization
dcheck_always_on = true

ffmpeg_branding = "Chrome"
proprietary_codecs = true
enable_platform_hevc = true
```

启用 enable_platform_hevc 后，并没有改变。

'"error":"FFmpegDemuxer: no supported streams"' 的来源是： media/filters/ffmpeg_demuxer.cc:1462

```
media/filters/ffmpeg_demuxer.cc:205
std::unique_ptr<FFmpegDemuxerStream> FFmpegDemuxerStream::Create() 

media/base/supported_types.cc:63:bool IsSupportedVideoType(const VideoType& type) {

bool IsDefaultSupportedVideoType(const VideoType& type) {
```

经过试验，media/base/supported_types.cc 需要做出如下修改：


```
@@ -312,16 +312,17 @@ bool IsDefaultSupportedVideoType(const VideoType& type) {
       return IsColorSpaceSupported(type.color_space) &&
              IsVp9ProfileSupported(type.profile);
     case kCodecH264:
     case kCodecVP8:
     case kCodecTheora:
+    case kCodecHEVC: //duanyao
       return true;
 
     case kUnknownVideoCodec:
     case kCodecVC1:
     case kCodecMPEG2:
-    case kCodecHEVC:
+    //case kCodecHEVC: //duanyao
     case kCodecDolbyVision:
       return false;
 
     case kCodecMPEG4:
 #if defined(OS_CHROMEOS)
```

修改以后，h265 视频可以播放了。不过，js 中的检测仍然通不过：

```
v.canPlayType('video/mp4;codecs="hevc"') // 返回 ''
```

## 从 build_ffmpeg.py 修改

修改 ~/t-project/chromium/src/third_party/ffmpeg/chromium/scripts/build_ffmpeg.py

```
@@ -909,13 +909,13 @@ def ConfigureAndBuild(target_arch, target_os, host_os, host_arch, parallel_jobs,
           '--ar=cygwin-wrapper lib',
       ])
 
   # Google Chrome & ChromeOS specific configuration.
   configure_flags['Chrome'].extend([
-      '--enable-decoder=aac,h264',
+      '--enable-decoder=aac,h264,hevc',
       '--enable-demuxer=aac,hevc',
-      '--enable-parser=aac,h264',
+      '--enable-parser=aac,h264,hevc',
   ])
```

更新 ffmpeg 构建文件：
```
export PATH=~/t-project/chromium/src/third_party/llvm-build/Release+Asserts/bin/:$PATH  # clang++
export PATH=~/t-project/chromium/out/release-hevc/:$PATH # nasm, 从源码编译

cd ~/t-project/chromium/src/third_party/ffmpeg/

chromium/scripts/build_ffmpeg.py linux x64 # 不要使用 --config-only，否则不完整。默认输出目录是 ~/t-project/chromium/src/build.x64.linux/ 。
chromium/scripts/copy_config.sh  # 更新 chromium/config/Chromium/linux/x64/config.h , chromium/config/Chrome/linux/x64/libavcodec/codec_list.c 等文件
chromium/scripts/generate_gn.py  # 更新 ffmpeg_generated.gni 文件。必须从 ~/t-project/chromium/src/third_party/ffmpeg/ 目录运行，并且在 build_ffmpeg.py linux x64 和 copy_config.sh 之后运行。
```

结果：

```
[3201:12:0731/010131.819323:ERROR:batching_media_log.cc(38)] MediaEvent: {"error":"Warning, FFmpegDemuxer failed to create a valid/supported video decoder configuration from muxed stream, config:codec: unknown, profile: unknown, level: not available, alpha_mode: is_opaque, coded size: [2560,1440], visible rect: [0,0,2560,1440], natural size: [2560,1440], has extra data: true, encryption scheme: Unencrypted, rotation: 0°, flipped: 0, color space: {primaries:BT709, transfer:BT709, matrix:BT709, range:LIMITED}"}
[3201:12:0731/010131.819489:ERROR:batching_media_log.cc(38)] MediaEvent: {"error":"FFmpegDemuxer:Create:check decoder: 1,0,0,"}
[3201:12:0731/010131.819734:ERROR:batching_media_log.cc(38)] MediaEvent: {"error":"FFmpegDemuxer: skipping invalid or unsupported video track"}
[3201:12:0731/010131.819821:ERROR:batching_media_log.cc(38)] MediaEvent: {"error":"FFmpegDemuxer: no supported streams"}
```
这是因为 args.gn 文件中还需要设置 enable_platform_hevc = true ，设置后重新编译即可。

## 打包

```
autoninja -C ../out/release-hevc "chrome/installer/linux:unstable_deb"
```

这样会在 ../out/release-hevc 中生成一个 deb 安装包。将 deb 中的文件解压到任意目录，也可以运行。

其它的打包方式可以如此查看：

```
autoninja -C ../out/release-hevc -t targets all | grep installer
```

除了 deb 似乎只有 rpm 格式。

## IceCC

sudo apt install icecc icecc-monitor ccache
