## 参考资料
[4.1] Debug containerized apps
https://code.visualstudio.com/docs/containers/debug-common

[4.2] Debug Python within a container
https://code.visualstudio.com/docs/containers/debug-python

[4.3] Docker in Visual Studio Code
https://code.visualstudio.com/docs/containers/overview

[4.4] Customize the Docker extensio
https://code.visualstudio.com/docs/containers/reference

## 目录
用户数据目录：
/home/duanyao/.config/Code

扩展安装目录：
/home/duanyao/.vscode/extensions

## 降级/升级扩展
有时候有些扩展的版本会有兼容性问题，例如 python 扩展 2024.6 在 vscode 1.87.0 (2024.2.27) 通过 ssh 访问时无法加载，将 python 扩展降级到 2024.4.1 即可。

## 在 docker 中调试 python

### docker + flask + vscode docker 调试模板 
.vscode/launch.json
```
{
  "configurations": [
    {
      "name": "Docker: Python - Flask",
      "type": "docker",
      "request": "launch",
      "preLaunchTask": "docker-run: debug",
      "python": {
        "pathMappings": [
          {
            "localRoot": "${workspaceFolder}",
            "remoteRoot": "/app"
          }
        ],
        "projectType": "flask"
      }
    }
  ]
}
```

.vscode/tasks.json:

```
{
	"version": "2.0.0",
	"tasks": [
		{
			"type": "docker-build",
			"label": "docker-build",
			"platform": "python",
			"dockerBuild": {
				"tag": "testdebugdockerpython:latest",
				"dockerfile": "${workspaceFolder}/Dockerfile",
				"context": "${workspaceFolder}",
				"pull": true
			}
		},
		{
			"type": "docker-run",
			"label": "docker-run: debug",
			"dependsOn": [
				"docker-build"
			],
			"dockerRun": {
				"env": { // 环境变量
					"FLASK_APP": "main.py"
				}
			},
			"python": { // 翻译为 python3 -m flask run --no-debugger --no-reload --host 0.0.0.0 --port 9100
				"args": [ // python3 -m <module> 之后的参数
					"run",
					"--no-debugger",
					"--no-reload",
					"--host",
					"0.0.0.0",
					"--port",
					"9100"
				],
				"module": "flask" // 翻译为 python3 -m flask ...
			}
		}
	]
}
```

Dockerfile

```
# For more information, please refer to https://aka.ms/vscode-docker-python
FROM python:3.10-slim

EXPOSE 9100

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

# Install pip requirements
COPY requirements.txt .
RUN python -m pip install -r requirements.txt

WORKDIR /app
COPY . /app

# Creates a non-root user with an explicit UID and adds permission to access the /app folder
# For more info, please refer to https://aka.ms/vscode-docker-python-configure-containers
RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /app
USER appuser

# During debugging, this entry point will be overridden. For more information, please refer to https://aka.ms/vscode-docker-python-debug
CMD ["gunicorn", "--bind", "0.0.0.0:9100", "main:app"]
```

requirements.txt: 
```
flask==2.2.2
gunicorn==20.1.0
```

docker inspect testdebugdockerpython-dev
```
        "Path": "python3",
        "Args": [],

"Mounts": [
            {
                "Type": "bind",
                "Source": "/home/duanyao/.vscode/extensions/ms-python.python-2023.8.0/pythonFiles/lib/python/debugpy", // 将 python 调试器代码目录挂载到容器内
                "Destination": "/debugpy",
                "Mode": "",
                "RW": false,
                "Propagation": "rprivate"
            }
        ],
        
            "Env": [
                "FLASK_APP=main.py",
                "PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                "LANG=C.UTF-8",
                "GPG_KEY=A035C8C19219BA821ECEA86B64E628F8D684696D",
                "PYTHON_VERSION=3.10.11",
                "PYTHON_PIP_VERSION=23.0.1",
                "PYTHON_SETUPTOOLS_VERSION=65.5.1",
                "PYTHON_GET_PIP_URL=https://github.com/pypa/get-pip/raw/0d8570dc44796f4369b652222cf176b3db6ac70e/public/get-pip.py",
                "PYTHON_GET_PIP_SHA256=96461deced5c2a487ddc65207ec5a9cffeca0d34e7af7ea1afc470ff0d746207",
                "PYTHONDONTWRITEBYTECODE=1",
                "PYTHONUNBUFFERED=1"
            ],
            
            "NetworkSettings": {
            "Bridge": "",
            "SandboxID": "9ce0edba1c6619cdd356d02185cfa01f4802f63a1086c175a984ac76f02f1ffe",
            "HairpinMode": false,
            "LinkLocalIPv6Address": "",
            "LinkLocalIPv6PrefixLen": 0,
            "Ports": {
                "9100/tcp": [
                    {
                        "HostIp": "0.0.0.0",
                        "HostPort": "32768"
                    },
                    {
                        "HostIp": "::",
                        "HostPort": "32768"
                    }
                ]
            },

```

调试命令（可以在vs终端里看到。docker container run 只有在出错时显示）：
```
docker container run --detach --tty --name 'testdebugdockerpython-dev' --publish-all --mount 'type=bind,source=/home,destination=/home' --mount 'type=bind,source=/home/duanyao/.vscode/extensions/ms-python.python-2023.8.0/pythonFiles/lib/python/debugpy,destination=/debugpy,readonly' --label 'com.microsoft.created-by=visual-studio-code' --env 'FLASK_APP=main.py' --env 'PATH=/home/duanyao/opt/venv/py39-centos8/bin:/usr/bin:/bin' --entrypoint 'python3' testdebugdockerpython:latest

docker exec -d testdebugdockerpython-dev python3 /debugpy/launcher 172.17.0.1:34011 -- -m flask run --no-debugger --no-reload --host 0.0.0.0 --port 9100
```

172.17.0.1:34011 的端口每次会变化。

调试网址： `http://localhost:32768`，端口每次会变化。

### docker + venv + 自定义镜像 + flask

修改 Dockerfile 的基础镜像为自定义的镜像：
```
FROM registry.cn-beijing.aliyuncs.com/aiparents/centos8-aiparents3-node12-py39-py36-arcface3-openvino214:2023-03-16
```

修改 Dockerfile 的 PATH 环境变量，让 venv 排在前面（可选，也可在 tasks.json 中修改）
```
ENV PATH=/home/duanyao/opt/venv/py39-centos8/bin:/usr/local/bin:/usr/bin:/bin
```

修改 tasks.json 的 dockerRun.volumes 对象，增加对 venv 所在目录的挂载（可选），增加对工程目录的挂载（可选。如果 Dockerfile 中有复制代码的指令，则不需要）：
```
		{
			"type": "docker-run",
			"label": "docker-run: debug",
			"dependsOn": [
				"docker-build"
			],
			"dockerRun": {
				"volumes": [
					{"localPath": "/home", "containerPath": "/home", "permissions": "rw"	},
					{"localPath": "${workspaceFolder}", "containerPath": "/app", "permissions": "rw"	}
				],
```

修改 tasks.json 的 dockerRun.env 对象，让 venv 排在前面（可选，也可在 Dockerfile 中修改）
```
		{
			"type": "docker-run",
			"label": "docker-run: debug",
			"dependsOn": [
				"docker-build"
			],
			"dockerRun": {
				...
				"env": {
					"FLASK_APP": "main.py",
					"PATH": "/home/duanyao/opt/venv/py39-centos8/bin:/usr/local/bin:/usr/bin:/bin"
				}
			},
```

### docker + venv + 自定义镜像 + 调试通用 python 程序

Dockerfile 可以不需要。

修改 tasks.json 的 "type": "docker-run" 任务
* 修改 label 属性，值可以随便，在 launch.json 中要引用。
* 去掉 dependsOn["docker-build"]。
* 增加 dockerRun.image 属性，设置为要运行的 docker 镜像。
* 修改 dockerRun.volumes 属性，将工程代码目录 `${workspaceFolder}` 挂载到容器内 /app 目录（可以改成别的，但后面要相应修改），以能在容器内运行。
* 根据实际情况设置 env 和 portsPublishAll 。
* 增加属性 dockerRun.customOptions: "-w /app"。这是把默认当前目录设置到 /app 。customOptions 可以包含任意的 "docker run" 的命令行参数。
* 增加属性 python.file: "main_cli.py"。这是被调试的程序文件。如果设置了 "-w /app"，。python.args 是要传给被调试的程序的参数，可相应调整。
* 删除 python.module 属性。它与 python.file 需要二选一。有 python.module 时，运行的方式是 `python -m <module>`，而有 python.file，运行方式是 `python <file>`。

```
		{
			"type": "docker-run",
			"label": "docker-run: main_cli",
			// "dependsOn": [
			// 	"docker-build"
			// ],
			"dockerRun": {
				"image": "registry.cn-beijing.aliyuncs.com/aiparents/centos8-aiparents3-node12-py39-py36-arcface3-openvino214:2023-03-16",
				"volumes": [
					{"localPath": "/home", "containerPath": "/home", "permissions": "rw"	},
					{"localPath": "${workspaceFolder}", "containerPath": "/app", "permissions": "rw"	}
				],
				"env": {
					//"FLASK_APP": "main.py",
					"PATH": "/home/duanyao/opt/venv/py39-centos8/bin:/usr/local/bin:/usr/bin:/bin",
					"HOME": "/tmp"
				},
				// "command": "sh",
				"portsPublishAll": true,
				"customOptions": "--user 18233:18233 -w /app"
			},
			"python": {
				"file": "main_cli.py",
				"args": [
					"-h"
				],
				//"module": "flask"
			}
		}
```
* dockerRun 增加属性 "command": "sh" 。这个可以有多种选择，只要不会立即退出的命令都可以，如 "python" 。

### 使用 debugpy 和网络调试
https://medium.com/@ogretenahmetcan/debugging-production-python-code-with-vscode-django-as-an-example-fbb9c76ae68c

### 通用容器调试 dev containers: 在容器中打开文件夹+Dockerfile

.devcontainer/devcontainer.json:
```
{
	"name": "Existing Dockerfile",
	"build": {
		// Sets the run context to one level up instead of the .devcontainer folder.
		"context": "..",
		// Update the 'dockerFile' property if you aren't using the standard 'Dockerfile' filename.
		"dockerfile": "../Dockerfile.custom"
	}

	// Features to add to the dev container. More info: https://containers.dev/features.
	// "features": {},

	// Use 'forwardPorts' to make a list of ports inside the container available locally.
	// "forwardPorts": [],

	// Uncomment the next line to run commands after the container is created.
	// "postCreateCommand": "cat /etc/os-release",

	// Configure tool-specific properties.
	// "customizations": {},

	// Uncomment to connect as an existing user other than the container default. More info: https://aka.ms/dev-containers-non-root.
	// "remoteUser": "devcontainer"
}
```

```
docker images
REPOSITORY                                                                                            TAG                 IMAGE ID            CREATED             SIZE
vsc-test_debug_docker_python-22e2bc1dc0bc9fdabf5a46e77cbb106fc8183e0f36643f4c49699f56700bca64         latest              4a2a7691d98c        4 hours ago         2.83GB
```

```
docker ps
CONTAINER ID        IMAGE                                                                                           COMMAND                  CREATED             STATUS              PORTS               NAMES
545134412dec        vsc-test_debug_docker_python-22e2bc1dc0bc9fdabf5a46e77cbb106fc8183e0f36643f4c49699f56700bca64   "/bin/sh -c 'echo Co…"   13 minutes ago      Up 13 minutes       9100/tcp            interesting_chebyshev
```

```
docker inspect  interesting_chebyshev

        "Mounts": [
            {
                "Type": "bind",
                "Source": "/home/duanyao/project/MyPython/test_debug_docker_python",
                "Destination": "/workspaces/test_debug_docker_python", // 此处为工程代码
                "Mode": "",
                "RW": true,
                "Propagation": "rprivate"
            },
            
            // 此处安装了 vscode 在容器内的 server、扩展
            {
                "Type": "volume",
                "Name": "vscode",
                "Source": "/var/lib/docker/volumes/vscode/_data",
                "Destination": "/vscode",
                "Driver": "local",
                "Mode": "z",
                "RW": true,
                "Propagation": ""
            }

```

另有 vscode 的扩展代码安装到了容器内的 /root/.vscode 目录下。

### 附加到已有容器来调试

vscode 中要安装2个扩展：docker 和 dev containers 。

先启动开发用的容器：
```
docker run --rm --name centos8-py39-dev-duanyao-1 -d --user $(id -u):$(id -g) -e USER=$(whoami) -e HOME=$HOME -v $HOME:$HOME -v /media:/media -v /mnt:/mnt -w $HOME registry.cn-beijing.aliyuncs.com/aiparents/centos8-aiparents3-node12-py39-py36-arcface3-openvino214:2023-03-16 tail -f /dev/null
```
或者
```
docker run --rm --name centos8-py39-dev-duanyao-1 -d --user $(id -u):$(id -g) -v /etc/passwd:/etc/passwd -v $HOME:$HOME -v /media:/media -v /mnt:/mnt -w $HOME registry.cn-beijing.aliyuncs.com/aiparents/centos8-aiparents3-node12-py39-py36-arcface3-openvino214:2023-03-16 tail -f /dev/null
```

或者：
```
c_name=centos8-py39-dev-duanyao-1
c_home=$HOME/docker-home/$c_name
mkdir -p $c_home
docker run --gpus all --name c_name -d --user $(id -u):$(id -g) -e USER=$(id -un) -e HOME=$c_home -v $HOME:$HOME -v $HOME/.pip:$c_home/.pip -v /media:/media -v /mnt:/mnt -w $HOME registry.cn-beijing.aliyuncs.com/aiparents/centos8-aiparents3-node12-py39-py36-arcface3-openvino214:2023-03-16 tail -f /dev/null
```

说明：
* tail -f /dev/null 确保容器持续运行，不立即退出。
* --user $(id -u):$(id -g) 使用与宿主机当前用户相同的用户id和组id。
* -e USER=$(whoami) 设定容器的用户名与宿主机相同。可选，只有少数情况下需要用户名。不要写 -e USER=$USER，因为不是所有的宿主机系统都设置此变量。
* -e HOME=$HOME 设定容器的主目录与宿主机相同。
* -e HOME=$HOME/docker-home/centos8-py39-dev-duanyao-1 使用不同于宿主机的主目录。这是因为 vscode 要在主目录安装扩展，使用一个容器专用的主目录可以避免它弄乱宿主机的主目录。
* -v $HOME:$HOME -v $HOME/.pip:$HOME/docker-home/centos8-py39-dev-duanyao-1/.pip 使用宿主机的 pip 设置。
* -v /etc/passwd:/etc/passwd 挂载宿主机的 /etc/passwd 到容器。可以代替 -e USER=$(whoami) -e HOME=$HOME 的设置。
* -v $HOME:$HOME -w $HOME 挂载主目录到容器，将默认当前目录设定为主目录。
* -v /media:/media -v /mnt:/mnt 挂载其它可能用到的目录。

要关闭容器：

```
docker stop -t 0 centos8-py39-dev-duanyao-1
```
再次启动容器：

```
docker start centos8-py39-dev-duanyao-1
```

在 vscode 中，点击左下角的“打开远程窗口”，点击“附加到运行中的容器”，选择 centos8-py39-dev-duanyao-1，这样就连接了在此容器中的环境。之后，可以打开文件夹，选择项目文件夹。之后的操作与本地相同。


### 远程容器调试

vscode 中要安装3个扩展：docker 和 dev containers 和 remote-ssh。

如果容器位于远程机器，先用 SSH 连接到远程服务器，启动开发用的容器，方法与“附加到已有容器来调试”相同。

在 vscode 中，先用 SSH 连接到远程服务器，然后再点击左下角的“正在SSH：xxx”上编辑，再点击“附加到已有容器来调试”，选择远程服务器上的容器。

### 其它容器调试扩展

dev container

dockerRun

vscode-containers-pack

## 调试环境变量

https://reuvenharrison.medium.com/using-visual-studio-code-to-debug-a-go-program-with-environment-variables-523fea268271
"configurations": [
    {
      "name": "Launch",
      "type": "go",
      "request": "launch", 
      "mode": "debug",
      "remotePath": "",
      "port": 2345,
      "host": "127.0.0.1",
      "${workspaceFolder}/subdir/http_test.go",
      "args": [
          "-test.run",
          "TestWithHTTP"
      ],
      "env": {
        "GCLOUD_PROJECT_ID":"my-project"
      },
      "showLog": true
    }
  ]
}

{
  "version": "0.2.0",
  "configurations": [
    {
      "name": "Launch",
      "type": "go",
      "request": "launch", 
      "mode": "debug",
      "remotePath": "",
      "port": 2345,
      "host": "127.0.0.1",
      "${workspaceFolder}/subdir/http_test.go",
      "args": [
          "-test.run",
          "TestWithHTTP"
      ],
      "envFile": "${workspaceFolder}/.env",
      "showLog": true
    }
  ]
}

KEY1="TEXT_VAL1"
KEY2='{"key1":val1","key2":"val2"}'

## 问题 issues

### mpegts 文件会导致 ts 语言服务内存暴涨（1.5~2GB）。

### 扩展安装失败/永久显示“正在安装”
* 可能是网络问题或需要代理，可以在“首选项-Http: Proxy“中设置代理。扩展的安装需要网络，有的扩展的安装还需要目标位置（容器、远程主机）有网络。注意此选项总是全局的，无法为 docker 容器、远程主机单独设置代理。如果代理服务器地址为宿主机（例如 http://127.0.0.1:23128 ），为了向 docker 容器中安装扩展，可以设置代理服务器为 http://172.17.0.1:23128 （docker0 网卡的地址）。

* 临时问题，需要重启 vscode 后重试。注意将所有 vscode 进程都退出或杀死，在任务管理器中搜索“code”相关进程，有些是 node.js 进程，也要杀掉。
