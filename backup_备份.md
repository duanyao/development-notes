## 参考资料

13 个开源备份解决方案
https://zhuanlan.zhihu.com/p/60404181

如何使用 rsync 的高级用法进行大型备份
https://zhuanlan.zhihu.com/p/66206489

Welcome to Dirvish
https://dirvish.org/

BorgBackup (short: Borg) is a deduplicating backup program.
https://borgbackup.readthedocs.io/en/stable/
