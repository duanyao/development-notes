## 参考资料
[1.0] Apache ZooKeeper
  http://zookeeper.apache.org/

[2.1] ZooKeeper - A Reliable, Scalable Distributed Coordination System 
  http://highscalability.com/blog/2008/7/15/zookeeper-a-reliable-scalable-distributed-coordination-syste.html
  
[2.2] The good, the bad, and the ugly of Apache ZooKeeper
  https://cwiki.apache.org/confluence/display/ZOOKEEPER/ZooKeeperPresentations?preview=/24193445/61328445/unified-log-zk-nov15.pdf

[3.1] 采用zookeeper的EPHEMERAL节点机制实现服务集群的陷阱
  https://yq.aliyun.com/articles/227260
  
[3.2] node-zookeeper-client A pure Javascript ZooKeeper client module for Node.js.
  https://www.npmjs.com/package/node-zookeeper-client
  
