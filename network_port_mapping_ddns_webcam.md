# 端口映射、内网服务、DDNS、网络摄像头的网络配置
## 参考资料

[2] 怎么知道自己家宽带是不是公网ip?
  https://zhidao.baidu.com/question/1497787602236877219.html

[3] 花生壳动态域名服务
  https://www.oray.com/

[4] 怎样用动态域名实现路由器的远程配置
  https://jingyan.baidu.com/article/3a2f7c2e56856a26afd6112f.html

[5] 网络摄像机安装拾音器教程（图文）
  http://www.gzngn.com/Article/wlsxjazsyq.html

[6] mpv 播放器 https://mpv.io/

[7] 现在电信内网 ip 还能改回公网的吗？
  https://www.v2ex.com/t/215620

[8] 花生壳:什么是子帐号？什么是父帐号？
  http://service.oray.com/question/693.html

[9] H3C DDNS配置简介
  https://blog.csdn.net/xiyanxiyan10/article/details/12998997

[10] 桥接模式与路由器模式的区别
  http://network.chinabyte.com/220/12438220.shtml

[11] 获得本局域网的外网IP
  http://members.3322.org/dyndns/getip

[12] dyndns
  https://dyn.com/dns/

[13] 花生壳 
  https://www.oray.com/

[14] 可用于内网穿透的高性能的反向代理应用
  https://github.com/fatedier/frp

[15] 域名方式外网无法访问排查步骤
  https://www.hikvision.com/cn/support_det_55_i118.html
  
[16] Hikvision Tools（含SADP、录像容量计算等工具）
  http://www1.hikvision.com/cn/download_more_393.html
  
[17] 通过阿里云域名动态解析 IP 地址
  https://www.v2ex.com/t/249694
  https://github.com/Mickyxing/aliyundns/blob/master/aimijia.py
  
[18] ddwrt路由/linux动态解析ip(ddns)到dnspod配置
  https://www.xdty.org/1841
  https://github.com/xdtianyu/scripts/tree/master/ddns

[19] 开放 DNSPod 几乎所有接口
  https://support.dnspod.cn/Support/api

[20] 通过阿里云域名动态解析IP地址
  https://github.com/Mickyxing/aliyundns
  
[21] NO-IP Dynamic IP address
  https://www.noip.com/
  
[22] noip动态域名配置案例
  http://www.utt.com.cn/reference.php?id=1892
  
[23] f3322.net/pubyun 动态域名
  http://www.pubyun.com/

[24] Dynamic DNS
  https://dyn.com/dns/
  
[25] 适用于OpenWRT/LEDE自带DDNS功能的阿里云脚本，完美嵌入
  https://www.right.com.cn/forum/thread-267501-1-1.html

[26] 哪些路由器对OpenWrt支持较好？
  https://www.zhihu.com/question/30771491
  
[27] ddclient Introduction about the supported protocols
  https://sourceforge.net/p/ddclient/wiki/protocols/
  
[28] Dyndns2 protocol specification?
  https://stackoverflow.com/questions/54039095/dyndns2-protocol-specification

[29] nsupdate.info ddns server with code
  https://github.com/nsupdate-info/nsupdate.info

[30] DNS Server
  https://github.com/rahimkhoja/DDNS_Server
  
[31] minidyndns
  https://github.com/arkanis/minidyndns

[6.1] 联通家宽 IPv6 无法接受入站 TCP 请求？
https://www.v2ex.com/t/709257

[6.2] 北京联通华为光猫HG8346R破解改桥接 
https://post.smzdm.com/p/441624/

[6.3] 光猫更换指南+无线路由器选择参考
https://zhuanlan.zhihu.com/p/50946685

[6.4] IPV6网络下建立安全的家庭服务器[NAS]
https://zhuanlan.zhihu.com/p/75247811

[7.1] 教你如何投诉电信运营商（Plus版） 
https://mp.weixin.qq.com/s/KklRA63sA4F16tpYrmi2eQ

[7.2] 教你如何有效的投诉电信运营商
https://zhuanlan.zhihu.com/p/107941236

[7.3] 我为什么建议你投诉中国移动，中国联通，中国电信？
https://zhuanlan.zhihu.com/p/67866830

[7.4] 中国联通投诉、历史投诉查询
https://club.10010.com/newIndex/#/trouble?channelcode=robot
https://c.10010.com/pcserv/trouble
http://iservice.10010.com/e4/query/others/service_complain-iframe.html?menuCode=000500020004

[7.5] 运营商瑟瑟发抖！除换套餐这些也可投诉到工信部
https://pcedu.pconline.com.cn/1025/10252924.html

[7.6] 电信用户申诉
https://yhssglxt.miit.gov.cn/web/

## 动态域名 DDNS 的原理
1. 获得本局域网的外网IP

  有两种可能：（1）从连接外网的设备（如光猫）上获取。（2）借助外网的服务器API，例如[2.1] http://members.3322.org/dyndns/getip 。

2. 用本局域网的外网IP更新指定的域名

  这需要调用域名解析服务商提供API。
  目前阿里云[17,20,25]和DNSpod[18,19]都提供这样的API。

## 自制路由器固件的动态域名服务

商品路由器的固件一般仅支持少数几种甚至只有一种动态域名服务，导致供应商锁定。但自制路由器固件，如 openwrt、ddwrt 都可以加入自定义脚本来支持任意类型的动态域名服务，包括自建服务[18,25,26]。

## 申请和使用动态域名服务
[3,4,8,9]。

几乎所有的光猫/路由器都支持动态域名（也写作动态DNS，缩写 DDNS）功能。从其 web 管理页面找到相关设置，填入动态域名的账号和密码即可。光猫拨号成功后，动态域名服务会将相关动态域名与宽带的公网 IP 绑定在一起，外界可以通过动态域名访问该公网 IP。

目前，DDNS更新过程没有统一的标准，向不同的DDNS服务器请求更新的过程各不相同[9]。

### 花生壳
以花生壳动态域名服务[3]为例： 在 www.oray.com 上注册一个账号。注册时需要提供一个有效手机号接收验证码，账号类型选“个人“即可（选”公司“亦可注册，暂时看不出区别），都是免费的。注册后，可以进行实名认证，有个人和企业两种认证类型。一个手机号可以注册若干个账号（不清楚是否存在上限）。每个账号会得到一个固定的”免费壳域名“，也就是动态域名，例如 2235f2042p.iask.in 。每个公网 IP 需要一个动态域名账号，一个动态域名，所以可以说我们需要为每个园区提供一个账号。

为了适应大量终端设备的情形，可以为一个账号再申请若干（20-999个）子账号，每个子账号也对应一个动态域名，与普通账号一样填入路由器设置即可[8]。子账号不需要对应手机号。

花生壳的动态域名付费服务314-418元/年/每个动态域名。

### 3322.net/pubyun 动态域名
pubyun 不采用子账号形式，而是可以创建多个动态域名。服务费是300元/年/15或30个动态域名[23]。

更多报价：

每个账户前100个二级域名，单个二级域名购买15元/个/年
每个第100个至500个二级域名，单个二级域名购买12元/个/年
每个账户第500个至1000个二级域名，单个二级域名购买10元/个/年
每个账户第1000个至5000个二级域名，单个二级域名购买8元/个/年
每个账户第5000个以上二级域名，单个二级域名购买6元/个/年

### noip 动态域名
[21,22]
国外服务。

### dyndns 动态域名
[24]
国外服务。

## 海康威视摄像头的初始安装

摄像头的初始 IP 地址为 192.168.1.64，web管理地址为 http://192.168.1.64，rtsp 端口为 554。
将用来管理的 PC 与摄像头接入同一交换机，用浏览器打开web管理地址开始设置。
如果 PC的 IP 地址如果不在 192.168.1.x 范围，应暂时手动修改到此范围，但不要与摄像头的地址重复。

如果摄像头 IP 地址已经被设置过，但是忘记了，可以在 PC 上用海康威视的 SADP 工具来搜索[1]。

初次登录web管理系统时，会提示设置管理员（admin）的密码。

修改摄像头 IP 后，原web管理页面无法再访问，请修改 PC 的 IP 以及web管理地址后重试。

播放视频（假定密码是12345678）的地址是： rtsp://admin:12345678@192.168.1.64:554 。

web管理系统使用了 ActiveX 控件，一些操作，如升级固件，需要通过 IE 浏览器才能做到（或者使用海康威视的客户端软件）。

## 海康威视摄像头的DDNS设置
目前仅支持 noip 和 dyndns 两种，可以指定服务器地址。后者可以自建服务器。

## DDNS 更新协议，自建 DDNS 服务器。
协议参考[27-28]。

DynDNS2 协议是 www.dyndns.com 定义的，形式是

```
https://username:password@domains.google.com/nic/update?hostname=subdomain.yourdomain.com&myip=1.2.3.4
```
或者
```
POST /nic/update?hostname=subdomain.yourdomain.com&myip=1.2.3.4 HTTP/1.1
Host: domains.google.com
Authorization: Basic base64-encoded-auth-string User-Agent: Chrome/41.0 your_email@yourdomain.com
```

实现了 DynDNS2 协议的服务器软件是[29-31]。

## 光猫/家用宽带接入设备
[6.1-]

### 
