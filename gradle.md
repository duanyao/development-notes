## 参考资料

[1.3] https://sdkman.io/install
[2.1] https://stackoverflow.com/questions/18487406/how-do-i-tell-gradle-to-use-specific-jdk-version
[2.2] Settings https://docs.gradle.org/current/dsl/org.gradle.api.initialization.Settings.html#org.gradle.api.initialization.Settings:includeFlat%28java.lang.String[]%29

[3.1] lombok gradle:not find method annotationProcessor() https://blog.csdn.net/qq_30336433/article/details/103494709

## 安装

* 压缩包

  从 https://gradle.org/install/ 下载 gradle-6.7.1-bin.zip ，解压到任意目录
  
  gradle 当前版本是 6.7.1 (2020.11)
  
  
* debian
  ```
  sudo apt instal gradle
  ```

  gradle 当前版本是deepin 当前版本是 4.4.1 (2020.11)。这个版本可能太低，会造成错误[3.1]。
  
* sdkman
```
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
sdk install gradle 6.7.1
```
安装后，位于 /home/duanyao/.sdkman/candidates/gradle/current/bin/gradle

下载服务器位于 https://www.digitalocean.com/ ，速度较慢，建议使用代理。

## 选择 JDK

```
gradle -Dorg.gradle.java.home=$JAVA_HOME
```

## 搜索包
可以使用 maven 的包。搜索 https://search.maven.org/ 。

## 包缓存

~/.gradle/caches/modules-2/files-2.1/

## 问题

### javafx

## 仓库
gradle 可以使用 maven 的仓库。本地缓存在以下目录：
```
 ~/
  .m2/
    settings.xml  # 配置文件，初始不存在
    repository/   # 本地仓库缓存，下载的库文件放在这里
```

## 工程结构和配置文件

java 项目的标准目录结构为

```
根项目目录/
  build.gradle
  settings.gradle
  gradle.properties
  子项目1目录/
    build.gradle
    src/
      main/
        java/
          package1/
            Foo.java
      test/
        java/
          package1/
            TestFoo.java
    build/                   # gradle 编译时创建。
      classes/
        java/
          main/
            package1/
              Foo.class
  子项目2目录/
    build.gradle
```

注意“java”这个目录，如果不这样命名，gradle无法自动找到java源文件。


### 项目的类型

子项目的 build.gradle 中应当指明使用的插件

```
apply plugin: 'java'  // 无特点的 java 项目，生成一个 jar 文件。

apply plugin: 'application'  // java se 应用，包含一个或多个 main 类。生成一个 zip/tgz 压缩包，包括自身的 jar 文件、依赖库的 jar 文件、启动的shell 和 bat 脚本等。

apply plugin:  'war' // java web 应用。生成一个 war 文件。

```

### 依赖项

```
dependencies {
    implementation project(':base')
    implementation project(':wxpay-sdk')
    implementation project(':aiparents_common')
    implementation project(':aiparents_model')
    implementation project(":aiparents_db")
    implementation project(":aiparents_service")

    implementation "javax.annotation:javax.annotation-api:$javaxAnnotationVersion"

    compile "org.springframework:spring-core:$springVersion"
    compile "org.springframework:spring-context:$springVersion"
```

## 任务
```
gradle tasks                # 列出所有的任务
gradle classes testClassed  # 编译出 java 类和测试类
gradle build                # 执行整个编译和构建，包括运行测试
gradle run                  # 运行项目，对java来说，是 build.gradle 的 mainClass 指定的类。
gradle build -x test        # 执行整个编译和构建，但不运行测试。-x 是排除一个任务。
```
