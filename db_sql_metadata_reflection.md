# SQL 数据库的元数据反射

## 参考资料
[1.1] https://en.wikipedia.org/wiki/Information_schema

[2.1] https://dev.mysql.com/doc/refman/5.7/en/information-schema-introduction.html

[2.2] https://dev.mysql.com/doc/refman/5.7/en/columns-table.html

[2.5] http://www.mysqltutorial.org/mysql-show-columns/

[2.6] https://dev.mysql.com/doc/refman/5.5/en/table-constraints-table.html

[2.7] https://dev.mysql.com/doc/refman/5.5/en/referential-constraints-table.html

[2.8] https://dev.mysql.com/doc/refman/5.7/en/views-table.html

[2.9] https://dev.mysql.com/doc/refman/5.5/en/statistics-table.html

[3.1] Manage Database in MySQL
  http://www.mysqltutorial.org/mysql-create-drop-database.aspx
  http://www.mysqltutorial.org/mysql-create-database/

[3.2]
  https://dev.mysql.com/doc/refman/8.0/en/charset-database.html

[3.3]
  https://dev.mysql.com/doc/refman/8.0/en/create-table.html
[3.4]
  https://dev.mysql.com/doc/refman/8.0/en/alter-table.html
  
[3.5]
  https://dev.mysql.com/doc/refman/8.0/en/use.html
[3.6]
  https://dev.mysql.com/doc/refman/8.0/en/rename-table.html
[3.7]
  https://stackoverflow.com/questions/67093/how-do-i-quickly-rename-a-mysql-database-change-schema-name
[3.8]
  https://dev.mysql.com/doc/refman/8.0/en/create-index.html
[3.9]
  https://dev.mysql.com/doc/refman/8.0/en/create-table-foreign-keys.html

[3.10]
  https://stackoverflow.com/questions/9018584/error-code-1005-cant-create-table-errno-150

## ANSI SQL information_schema
ANSI SQL 标准中，数据库的元数据位于 "information_schema" 数据库中。
其中 TABLES 表列出表的元数据，COLUMNS 表列出表列的元数据。

### 实现情况
[1.1]

RDBMSs which support `information_schema` include:

    Microsoft SQL Server
    MySQL
    PostgreSQL
    SQLite 【存疑】
    InterSystems Caché
    H2 Database
    HSQLDB
    MariaDB

RDBMSs which do not support `information_schema` include:

    Oracle Database
    IBM DB2
    Sybase ASE
    sqlite

### 数据库的元数据
例子[2.1]：
```
SELECT * FROM information_schema.schemata
```
结果
```
CATALOG_NAME |SCHEMA_NAME        |DEFAULT_CHARACTER_SET_NAME |DEFAULT_COLLATION_NAME |SQL_PATH |
-------------|-------------------|---------------------------|-----------------------|---------|
def          |information_schema |utf8                       |utf8_general_ci        |         |
def          |aiparents          |utf8mb4                    |utf8mb4_unicode_ci     |         |
def          |mysql              |latin1                     |latin1_swedish_ci      |         |
def          |performance_schema |utf8                       |utf8_general_ci        |         |
def          |test               |latin1                     |latin1_swedish_ci      |         |
```
schemata （schema的复数）表列出数据库的元数据。

字段
* `schema_name` 数据库名。
* `default_character_set_name` 字符集。
* `default_collation_name` 字符集和排序。

### 表的元数据
例子[2.1]：

```
SELECT table_name, table_type, table_schema, table_collation, table_comment, table_rows, engine
       FROM information_schema.tables
       WHERE table_schema = 'aiparents'
       ORDER BY table_name;
```
输出：

```
table_name                     |table_type |table_schema |table_collation    |table_comment |table_rows |engine |
-------------------------------|-----------|-------------|-------------------|--------------|-----------|-------|
ai_daily_statistic             |BASE TABLE |aiparents    |utf8mb4_unicode_ci |              |199        |InnoDB |
character                      |BASE TABLE |aiparents    |utf8mb4_unicode_ci |性格类型        |2          |InnoDB |
child                          |BASE TABLE |aiparents    |utf8mb4_unicode_ci |              |197        |InnoDB |
child_parent                   |BASE TABLE |aiparents    |utf8mb4_unicode_ci |              |119        |InnoDB |
child_parent_class_teacher     |VIEW       |aiparents    |                   |VIEW          |           |       |
class                          |BASE TABLE |aiparents    |utf8mb4_unicode_ci |              |23         |InnoDB |
class_critical_period          |BASE TABLE |aiparents    |utf8mb4_unicode_ci |              |12         |InnoDB |
```
tables 表列出表的元数据，其中一些字段的解释：

* `table_schema` 所属的数据库的名字。
* `table_type` 表的类型。
    * BASE TABLE 普通表。
    * VIEW 视图。
* `table_collation` 表的字符串格式和排序方式。MySQL 应该采用 `utf8mb4_unicode_ci` ，但默认是 `utf8_general_ci` 。
* `table_comment` 表的注释。
* `table_rows` 表的行数。
* `engine` 后端引擎（实现相关）。

### 视图的元数据
其一，参照表的元数据（information_schema.tables），table_type 类型为 VIEW，但是这里无法看到 view 的定义。
其二，information_schema.views ，有 view 的定义，但缺少 table_comment 部分。

例子[2.8]：

```
select table_schema, table_name, view_definition, character_set_client, collation_connection from information_schema.views  where table_schema = 'aiparents';
```
输出：

```
table_schema |table_name                 |view_definition                                                                    |character_set_client |collation_connection |
aiparents    |child_parent_class_teacher |select `aiparents`.`child`.`id` AS ` `parent_id`,`aiparents`.`parent`.`real_name`  |utf8                 |utf8_general_ci      |
aiparents    |class_key_report           |select `aiparents`.`class_critical_period`.`class_id` AS `class_id`,`aiparents`.   |utf8                 |utf8_general_ci      |
```

* `table_name` 视图名。
* `table_schema` 所属的数据库的名字。
* `view_definition` 视图的定义，select 部分。

### 表列的元数据
例子[2.2]：
```
SELECT column_name, data_type, column_type, character_set_name, collation_name, column_key,  is_nullable, column_default, extra, column_comment
  FROM information_schema.columns
  WHERE table_name = 'tbl_name'
  AND table_schema = 'db_name'
```

`table_schema` 是数据库名，`table_name` 是表名。

输出：

```
column_name     |data_type |column_type  |character_set_name |collation_name     |column_key |is_nullable |column_default |extra |column_comment |
----------------|----------|-------------|-------------------|-------------------|-----------|------------|---------------|------|---------------|
id              |bigint    |bigint(20)   |                   |                   |PRI        |NO          |               |      |               |
real_name       |varchar   |varchar(20)  |utf8mb4            |utf8mb4_unicode_ci |           |NO          |               |      |               |
level           |int       |int(3)       |                   |                   |           |YES         |               |      |               |
points          |int       |int(3)       |                   |                   |           |YES         |               |      |               |
telphone_bound  |varchar   |varchar(11)  |utf8               |utf8_general_ci    |UNI        |YES         |               |      |               |
privilege       |int       |int(1)       |                   |                   |           |YES         |               |      |               |
class_id        |bigint    |bigint(20)   |                   |                   |           |YES         |               |      |               |
kindergarten_id |bigint    |bigint(20)   |                   |                   |           |YES         |               |      |               |
weixin_openid   |varchar   |varchar(100) |utf8               |utf8_general_ci    |           |YES         |               |      |               |
weixin_nickname |varchar   |varchar(100) |utf8mb4            |utf8mb4_unicode_ci |           |YES         |               |      |               |
```

* `data_type` 数据类型，不带长度、精度等参数。
* `column_type` 数据类型，带长度、精度等参数。

* `column_default` 默认值，注意其类型是 longtext，不论 data_type 是什么。

* `column_key`
  * PRI 主键或联合主键的一部分，PRIMARY KEY。注意，mysql 中一个表如果没有主键，information_schema.columns 会把第一个 unique + not null 的列标记为 PRI，但这是不真实的。
    一个表是否指定了主键，可以从 information_schema.table_constraints.CONSTRAINT_TYPE 中查询。

  * UNI 非重复，UNIQUE。如果是多列 unique，则只有第一列标记为 MUL。
  
  * MUL nonunique index 或多列 unique 的第一列。
  
* character_set_name 和 collation_name：编码和排序。对于文本类型的列，这两个属性应该被理解为附着于列，并且不会为 null；对于非文本类型的列，一定为 null。

* extra
  `auto_increment` 自增。AUTO_INCREMENT。

* `column_comment` 备注。没有指定备注时，column_comment 为空串，而非 null 。

### 表约束的元数据

约束的总体情况[2.6]：

```
select constraint_schema, constraint_name, table_schema, table_name, constraint_type from information_schema.table_constraints;
```
结果：

```
constraint_catalog |constraint_schema |constraint_name |table_schema |table_name                     |constraint_type
-------------------|------------------|----------------|-------------|-------------------------------|--------------
def                |aiparents         |PRIMARY         |aiparents    |ai_daily_statistic             |PRIMARY KEY      
def                |aiparents         |close_personnel |aiparents    |child                          |FOREIGN KEY    
def                |aiparents         |sack_time       |aiparents    |child                          |FOREIGN KEY   
def                |aiparents         |telphone_bound  |aiparents    |teacher                        |UNIQUE        
def                |aiparents         |PRIMARY         |aiparents    |teacher_answer_survey_question |PRIMARY KEY   
def                |database_t1       |a_2             |database_t1  |tasks                          |UNIQUE        
def                |database_t1       |task_id_2       |database_t1  |tasks                          |UNIQUE        
def                |database_t1       |task_id_3       |database_t1  |tasks                          |UNIQUE        
```

`TABLE_SCHEMA` 和 `TABLE_NAME` 分别是数据库的名字。

`CONSTRAINT_TYPE` 的可能的值为：UNIQUE, PRIMARY KEY, FOREIGN KEY, or CHECK。
一般的 index 不属于 constraint，也不会列出。但 UNIQUE, PRIMARY KEY 都自动具有 index。

虽然索引不是约束，但两者的名字不能重复。

外键约束的细节[2.7]：
```
select * from information_schema.referential_constraints;
```

```
CONSTRAINT_CATALOG |CONSTRAINT_SCHEMA |CONSTRAINT_NAME |UNIQUE_CONSTRAINT_CATALOG |UNIQUE_CONSTRAINT_SCHEMA |UNIQUE_CONSTRAINT_NAME |MATCH_OPTION |UPDATE_RULE |DELETE_RULE |TABLE_NAME |REFERENCED_TABLE_NAME |
-------------------|------------------|----------------|--------------------------|-------------------------|-----------------------|-------------|------------|------------|-----------|----------------------|
def                |aiparents         |character       |def                       |aiparents                |PRIMARY                |NONE         |NO ACTION   |NO ACTION   |child      |character             |
def                |aiparents         |close_personnel |def                       |aiparents                |PRIMARY                |NONE         |NO ACTION   |NO ACTION   |child      |close_personnel       |
```

约束涉及的列的细节：

```
select constraint_name,table_schema,table_name,column_name,ordinal_position,position_in_unique_constraint,referenced_table_schema,referenced_table_name,referenced_column_name
  from information_schema.key_column_usage where table_schema='aiparents';
```

输出：
```
constraint_name |table_schema |table_name                     |column_name                |ordinal_position |position_in_unique_constraint |referenced_table_schema |referenced_table_name |referenced_column_name |
----------------|-------------|-------------------------------|---------------------------|-----------------|------------------------------|------------------------|----------------------|-----------------------|
PRIMARY         |aiparents    |ai_daily_statistic             |id                         |1                |                              |                        |                      |                       |
PRIMARY         |aiparents    |character                      |id                         |1                |                              |                        |                      |                       |
PRIMARY         |aiparents    |child                          |id                         |1                |                              |                        |                      |                       |
character       |aiparents    |child                          |character_id               |1                |1                             |aiparents               |character             |id                     |
close_personnel |aiparents    |child                          |close_personnel_id         |1                |1                             |aiparents               |close_personnel       |id                     |
get_up_time     |aiparents    |child                          |get_up_time_id             |1                |1                             |aiparents               |get_up_time           |id                     |
sack_time       |aiparents    |child                          |sack_time_id               |1                |1                             |aiparents               |sack_time             |id                     |
PRIMARY         |aiparents    |child_parent                   |child_id                   |1                |                              |                        |                      |                       |
PRIMARY         |aiparents    |child_parent                   |parent_id                  |2                |                              |                        |                      |                       |
```

* ordinal_position 该列在约束的列表中的位置，从1开始。

the column's position within the constraint, not the column's position within the table. column positions are numbered beginning with 1.

* position_in_unique_constraint 外键的被引用列表中的位置（被引用列的还是本列的？），从1开始。对unique 或者 primary key 约束，值是 NULL。

### 索引 index 的元数据
information_schema.statistics 存放关于索引的元数据。

```
select table_schema,table_name,non_unique,index_schema,index_name,seq_in_index,column_name,collation,cardinality,sub_part,packed,nullable,index_type,comment,index_comment from information_schema.statistics
  where table_name = 'tasks' and table_schema = 'database_t1';
```

```
table_schema |table_name |non_unique |index_schema |index_name |seq_in_index |column_name |collation |cardinality |sub_part |packed |nullable |index_type |comment |index_comment |
-------------|-----------|-----------|-------------|-----------|-------------|------------|----------|------------|---------|-------|---------|-----------|--------|--------------|
database_t1  |tasks      |0          |database_t1  |PRIMARY    |1            |task_id     |A         |0           |         |       |         |BTREE      |        |              |
database_t1  |tasks      |0          |database_t1  |u11        |1            |b           |A         |0           |         |       |YES      |BTREE      |        |              |
database_t1  |tasks      |0          |database_t1  |u11        |2            |a           |A         |0           |         |       |         |BTREE      |        |              |
database_t1  |tasks      |1          |database_t1  |index_aa   |1            |b           |A         |0           |         |       |YES      |BTREE      |        |comment 1     |
```

* collation 排序，A 升序，D 降序，NULL 未排序。
* index_comment 注释。

多列索引会占据多行，每一列对应一行。

unique 约束（包括多列）的 non_unique 是 0，否则为 1。

主键、unique 都会在 information_schema.statistics 中有记录，但外键不会。
mysql 中，unique 的约束名与对应的索引名原则上可以不同（TODO 实际上待确认）；foreign key 的约束名与对应的索引名原则上可以不同，但 information_schema.statistics 中无记录（TODO?）；主键的约束名与对应的索引名总是相同。

## ANSI SQL 变更元数据
### 变更数据库元数据
#### 新建库
```
CREATE DATABASE [IF NOT EXISTS] database_name
[CHARACTER SET charset_name]
[COLLATE collation_name]
```

没有 IF NOT EXISTS 时，如果数据库已经存在则报错。
成功时，无论是否已经存在，更新行数为1。

character set 字符集，最好使用 utf8mb4 。
collate 字符排序，最好使用 utf8mb4_unicode_ci 。

#### 删除库
```
DROP DATABASE IF EXISTS database_name;
```
没有 IF EXISTS 时，如果数据库不存在则什么都不做；此时更新行数为0。

#### 修改库
[3.2]
```
ALTER DATABASE db_name
    [[DEFAULT] CHARACTER SET charset_name]
    [[DEFAULT] COLLATE collation_name]
```
DEFAULT 在这里没有作用，可以省略。

#### 重命名库
mysql 没有直接支持重命名库，但可以通过（1）新建一个空库；（2）跨库重命名表的方式快速转移数据到新库；（3）删除旧库[3.7]。
关于跨库重命名表，见重命名表章节。

### 变更表的元数据
#### 指定数据库
没有标准。MySQL 的：
[3.5]
```
USE db_name;
```
`use` 的作用范围是整个会话。

也可以在表名前直接指定数据库名：

```
DROP TABLE IF EXISTS db_name.tbl_name;
```

不过，这并非标准的 ANSI SQL 语法。

#### 新建表
```
CREATE TABLE [IF NOT EXISTS] tbl_name (column_list)
    [[DEFAULT] CHARACTER SET charset_name]
    [COLLATE collation_name]]
    [ENGINE [=] storage_engine]
    [COMMENT [=] 'string']
```

没有 IF NOT EXISTS 时，如果表已经存在则报错。
成功时，无论是否已经存在，更新行数为1。

并不清楚 DEFAULT 在这里有何作用；对于 mysql 来说，两者效果似乎相同。

storage_engine 可以是 InnoDB and MyISAM

`column_list` 是逗号分割的条目，每条指定一个字段。至少要有一个字段，否则是语法错误。

格式为 `column_name data_type(length) [NOT NULL] [UNIQUE [KEY]] [[PRIMARY] KEY] [default value] [AUTO_INCREMENT]` ；

如果有 character set xxx collate xxx 的修饰符，应位于 data_type 之后， NOT NULL 之前，否则 MariaDB 会报告语法错误。

default value 的格式是字面量：
* 字符串类，用单引号 `'text value'`
* 数字类，直接写数值 `-123.3`。 bigint 似乎可以用字符串写法？
* date、datetime, 用字符串 `'0000-00-00 00:00:00'`，后者还可以用 `CURRENT_TIMESTAMP` 关键字，表示插入时的时间。

例子：

```
CREATE TABLE IF NOT EXISTS tasks (
    task_id INT AUTO_INCREMENT,
    title VARCHAR(255) NOT NULL,
    start_date DATE,
    due_date DATE,
    status TINYINT NOT NULL,
    priority TINYINT NOT NULL,
    description TEXT,
    product_category INT NOT NULL,
    product_id INT NOT NULL,
    PRIMARY KEY (task_id),
    INDEX index_name (title,start_date),
    CONSTRAINT cons_name FOREIGN KEY (product_category, product_id) REFERENCES product(category, id)
)  ENGINE=INNODB;
```

对于 mysql INNODB，应该在 CREATE TABLE 的时候就指定主键，因为它根据主键决定物理存储布局[3.8]。

如果要对主键命名，则 `PRIMARY KEY (task_id)` 改为 `CONSTRAINT name PRIMARY KEY (task_id)` 。

#### 删除表
```
DROP TABLE IF EXISTS tbl_name;
```
没有 IF EXISTS 时，如果数据库不存在则什么都不做；此时更新行数为0。
可以用数据库名来限定表名，例如 db_name.tbl_name 。

#### 重命名表
[3.6]
重命名表没有标准。

MySQL:
```
ALTER TABLE old_table RENAME new_table;
RENAME TABLE old_table TO new_table;
```

如果在 old_table 或者 new_table 上加上数据库名来限定，可以跨库移动+重命名表。例如[3.7]：

```
RENAME TABLE old_db.table TO new_db.table;
```

#### 修改表
[3.2, 3.4]
```
ALTER TABLE tbl_name
    [[DEFAULT] CHARACTER SET charset_name]
    [COLLATE collation_name]
    [COMMENT [=] 'string']
    [ENGINE [=] engine_name]
```
并不清楚 DEFAULT 在这里有何作用；对于 mysql 来说，两者效果似乎相同。

### 变更表列的元数据
#### 新建列
```
ALTER TABLE table_name ADD column_name column_definition;
```

column_definition 中可以包括 PRIMARY KEY 属性，但如果表中已经有主键了，则会报错。

#### 删除列
```
ALTER TABLE table_name DROP [COLUMN] column_name;
```
表中仅有一列的时候不可删除。

#### 重命名、修改列
```
ALTER [COLUMN] col_name {SET DEFAULT literal | DROP DEFAULT};

ALTER TABLE CHANGE [COLUMN] old_col_name new_col_name column_definition;

ALTER TABLE t1 CHANGE a b BIGINT NOT NULL; 

ALTER TABLE t1 CHANGE b b INT NOT NULL;

ALTER TABLE t1 MODIFY col1 BIGINT UNSIGNED DEFAULT 1 COMMENT 'my column' CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

ALTER TABLE t1 RENAME COLUMN b TO a;
```
RENAME 只改名，MODIFY 只改属性，CHANGE 则都可以改。CHANGE 给出的前后名字可以相同。

CHANGE MODIFY RENAME 子句都可以在一个 ALTER TABLE 语句中重复出现，用逗号连接。

CHANGE 和 MODIFY 必须给出完全的列属性表（除了 PRIMARY 和 UNIQUE 属性，它们是约束；也除了 CHARACTER SET 和 COLLATE），没有提及的属性将被置为不存在。
CHANGE 和 MODIFY 不可以修改 PRIMARY 属性，但可以增加 UNIQUE 属性。

CHANGE 和 MODIFY 可以指定 CHARACTER SET 和 COLLATE 属性；如果不指定，则这两个属性保持旧值不变。

RENAME 和 MODIFY 是 oracle 和 mysql 8.0+ 支持，CHANGE 是 mysql 支持。

如果要新增自增（auto_increment）列，因为自增列必须同具有 index，所以需要在 column_definition 中同时指定 primary key 或者 unique，不能事后指定，否则会出错。

### 变更约束：主键和 UNIQUE 和 FOREIGN KEY 等
```
ALTER TABLE table_name ADD [CONSTRAINT [symbol]] PRIMARY KEY
        [index_type] (key_part,...)
        [index_option] ...

ALTER TABLE table_name ADD [CONSTRAINT [symbol]] UNIQUE [INDEX|KEY]
        [index_name] [index_type] (key_part,...)
        [index_option] ...

ALTER TABLE table_name ADD [CONSTRAINT [symbol]] FOREIGN KEY
        [index_name] (col_name,...)
        REFERENCES ref_table_name(ref_col_name,...)
        ON DELETE {CASCADE|RESTRICT}
        ON UPDATE CASCADE

index_option:
    KEY_BLOCK_SIZE [=] value
  | index_type
  | WITH PARSER parser_name
  | COMMENT 'string'
  | {VISIBLE | INVISIBLE}



ADD check_constraint_definition

CREATE TABLE table_name
(
  column1 datatype [ NULL | NOT NULL ],
  column2 datatype [ NULL | NOT NULL ],
  ...

  CONSTRAINT constraint_name UNIQUE (uc_col1, uc_col2, ... uc_col_n)
);

ALTER TABLE table_name DROP PRIMARY KEY;
ALTER TABLE table_name DROP INDEX { index_name | constraint_name };
ALTER TABLE table_name DROP FOREIGN KEY fk_symbol;
```

mysql 中，所有的约束都有名字；如果创建时没有指定名字，mysql 会自动生成一个名字，规则是：

* 主键约束命名为 "PRIMARY"。因为是关键字，所以要用 ``PRIMARY`` 的形式来指代，例如：

	```
	ALTER TABLE tasks drop index `PRIMARY`;
	```
* 其它列约束按 `列名` 或 `列名_n` 命名，n 是个大于1的整数。多列约束的名字按第一列命名。

mysql 中，DROP INDEX 实际上包括了 DROP CONSTRAINT（Oracle语法）的功能，可以删除任何约束；但 mysql 并不允许 DROP CONSTRAINT 的写法。

用 `DROP PRIMARY KEY` 删除主键约束并不需要指定名字、列名，因为只能有一个主键。

必须先删除已有主键，才能重新指定主键；不能修改现有主键，例如增加联合主键；不能将已经是主键的列重复添加为主键。

mysql 中可以对一列重复添加 UNIQUE 约束，会生成多个约束。

mysql 中 not null 并不被视为一种约束，而仅仅是列属性。

ALTER TABLE 后面允许跟着多个多个子句，用逗号连接。

虽然索引不是约束，但两者的名字不能重复。例如一般不能用 `PRIMARY` 做索引的名字。


注意，UNIQUE 和 FOREIGN KEY 除了约束名（symbol），还有个索引名（index_name）。指定了约束名但省略索引名时，mysql 认为两者同名[3.9]。
使用 alter table 而不是 create table 时，mysql 忽略 FOREIGN KEY 的索引名。

### 外键的要求
mysql 中，对创建外键有如下要求[3.10]：

* 引用列和被引用列的类型要完全相同（包括精度、符号、字符集、排序）。

* 被引用列（列组）必须有索引（主键总是有索引）；作为其它索引列组的不算；如果外键是多列的，则被引用列组的顺序要与索引中列组的顺序相同。

* 引用列不能有默认值。

如果违反，create table 时，会出现 1005 Can’t create table 错误。

修改表时，有如下要求：

* 外键的被引用列上的索引不能被删除，除非先删除相应的外键。

### 变更索引 index

```
CREATE [UNIQUE | FULLTEXT | SPATIAL] INDEX index_name
    [index_type]
    ON tbl_name (key_part,...)
    [index_option]
    [algorithm_option | lock_option] ...

key_part: {col_name [(length)] | (expr)} [ASC | DESC]

index_option:
    KEY_BLOCK_SIZE [=] value
  | index_type
  | WITH PARSER parser_name
  | COMMENT 'string'
  | {VISIBLE | INVISIBLE}

index_type:
    USING {BTREE | HASH}

algorithm_option:
    ALGORITHM [=] {DEFAULT | INPLACE | COPY}

lock_option:
    LOCK [=] {DEFAULT | NONE | SHARED | EXCLUSIVE}
```

等效于

```
ALTER TABLE tbl_name ADD {INDEX|KEY} [index_name] [index_type] (key_part,...) [index_option] ...

ALTER TABLE tbl_name ADD {FULLTEXT|SPATIAL} [INDEX|KEY] [index_name]  (key_part,...) [index_option] ...
```

删除 index

```
ALTER TABLE tbl_name DROP {INDEX|KEY} index_name
```

虽然索引不是约束，但两者的名字不能重复。


### 变更视图

```
CREATE
    [OR REPLACE]
    [ALGORITHM = {UNDEFINED | MERGE | TEMPTABLE}]
    [DEFINER = { user | CURRENT_USER }]
    [SQL SECURITY { DEFINER | INVOKER }]
    VIEW view_name [(column_list)]
    AS select_statement
    [WITH [CASCADED | LOCAL] CHECK OPTION]


ALTER
    [ALGORITHM = {UNDEFINED | MERGE | TEMPTABLE}]
    [DEFINER = { user | CURRENT_USER }]
    [SQL SECURITY { DEFINER | INVOKER }]
    VIEW view_name [(column_list)]
    AS select_statement
    [WITH [CASCADED | LOCAL] CHECK OPTION]
```

`view_name` 和 `select_statement` 中的表名都可以用数据库名加以限定；一个数据库中的视图也可以引用其它数据库中的表。

## sqlite

sqlite 尚不支持 `information_schema` ，但有一些非标准的元数据。

查询表、视图：

```
SELECT * FROM sqlite_master WHERE type ='table' AND name NOT LIKE 'sqlite_%';
SELECT * FROM sqlite_master WHERE type ='view' AND name NOT LIKE 'sqlite_%';
```