## 参考资料
[1.1] conda的安装与使用（2019-6-28更新）
  https://www.jianshu.com/p/edaa744ea47d
  
[1.2] deepin 15.11 安装 tensorflow or pytorch (基于conda)
  https://jansora.com/post/deepin-deeplearning-gpu-dependency
  
[1.3] Conda工具使用
  https://www.jianshu.com/p/17288627b994

[2.1] Managing environments
  https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html
  
[3.1] cudaGetDevice() failed. Status: CUDA driver version is insufficient for CUDA runtime version解决 https://www.machunjie.com/trouble/110.html

## conda/minicoda 安装（linux）

* 下载 minconda 安装器，从 https://docs.conda.io/en/latest/miniconda.html 下载 Miniconda3-latest-Linux-x86_64.sh 。

* 执行 bash Miniconda3-latest-Linux-x86_64.sh

  这会询问一些问题。例如 miniconda 的安装位置，默认是 ~/miniconda3 ，可以更改，但一开始最好不要装到全局位置。我们假设装到了 ~/opt/miniconda3 。
  另一个问题是“Do you wish the installer to initialize Miniconda3 by running conda init? ”最好回答no ，这样可避免与 miniconda 外的环境互相干扰。
  回答完问题就开始安装，~/opt/miniconda3 下将会包含 conda 本身和 python ，python 一般都是很新的版本，如 3.7 （2019.1）。

* 激活 miniconda 环境。在任何目录下执行 `. ~/opt/miniconda3/bin/activate` 即可，这样会把 `~/opt/miniconda3/bin` 添加到 PATH 中，这个目录里包含了 conda 和 python 。
  其实不做激活也能用，只要用全路径执行 `~/opt/miniconda3/bin` 里的命令即可。

* 添加镜像源。编辑 ~/.condarc

```
  channels:
    - https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/conda-forge/
    - https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/
    - https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/
    - defaults
```

  或者用命令添加 `conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/`。

* (可选) 降级 python 版本。tensorflow 对 python 的最高版本可能有限制，例如 tensorflow 1.12 最高允许 python3.6 。如果 miniconda 自带的 python 更高，可以降级：
  `conda install python=3.6` 。


## conda/minicoda 安装（windows）

下载 conda 安装包（exe）。安装时选择“仅本用户”，因为“所有用户”有一些问题。

## 在 conda 中安装和管理软件包
* 搜索包 `conda search foo` 或者 `conda search -i foo` 输出详细信息。

* 安装包 `conda search foo bar=1.2 'tea<4.1'` 。可以指定版本号，也可以指定大于或小于某个版本号。

* 安装 tensorflow 。先列出可安装的版本 `conda search tensorflow-gpu` ，然后安装一个：`conda install tensorflow-gpu=1.15.0` 。
  这也会同时安装匹配 tensorflow 的依赖，包括 cudatoolkit, cudnn, cupti 等。安装的位置也是 `~/opt/miniconda3/` 。

* 使用 tensorflow 。启动  `~/opt/miniconda3/bin/python` ，执行 `import tensorflow` 如果无错误就表明安装成功了。以后用 `~/opt/miniconda3/bin/python` 即可使用 tensorflow 。

* 在 miniconda 环境中安装其它 python 包。这可以用 `~/opt/miniconda3/bin/pip` 来实现，用法与全局 pip 相同，只不过会装到 `~/opt/miniconda3/` 下。
  对于 python 包，pip 和 conda 包似乎是等效的，但 conda 还管理非 python 包。

### 用其它下载工具下载 conda 软件包

`conda install foo` 不具备断点续传能力，一旦网络出错，就只能从头开始下载。因此对于大型软件包，建议使用其他下载工具下载 conda 软件包，再安装。可以用 uget ，linux和windows版本都有。

用 `conda search -i foo` 显示软件包详细信息，其中就包含了下载url，用其他下载工具下载这些包，然后：

`conda install /path1/foo...tar.bz2 /path1/bar...tar.bz2`

或者：
```
conda config --set show_channel_urls yes
```
## 运行 conda 环境中的程序
第一种方法是激活一个 conda 环境后，再使用里面的程序，如 python.
```
. ~/opt/miniconda3/bin/activate  # 激活 base
conda activate e2                # 激活 e2
conda activate C:\Users\rd\c39   # 激活指定目录下的环境。
python ...
```

主要是修改了环境变量：
```
CONDA_SHLVL=2
CONDA_EXE=/home/duanyao/opt/miniconda3/bin/conda
CONDA_PREFIX=/home/duanyao/opt/miniconda3/envs/e2
CONDA_PREFIX_1=/home/duanyao/opt/miniconda3
CONDA_PYTHON_EXE=/home/duanyao/opt/miniconda3/bin/python
_CE_CONDA=
CONDA_PROMPT_MODIFIER=(e2) 
PATH=/home/duanyao/opt/miniconda3/envs/e2/bin:/home/duanyao/opt/miniconda3/condabin:/home/duanyao/.nvm/versions/node/v12.3.1/bin:/home/duanyao/t-project/emsdk.git:/home/duanyao/t-project/emsdk.git/upstream/emscripten:/home/duanyao/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/sbin:/usr/sbin
CONDA_DEFAULT_ENV=e2
```

第二种方法是直接调用 conda 中的 python：

```
~/opt/miniconda3/bin/python ...
~/opt/miniconda3/envs/e2/bin/python ...
```

反激活：
```
conda deactivate
```
启用/不启用登录shell时激活：
```
conda config --set auto_activate_base False # True
```

## conda 与 cuda
[3.1]
从 conda 里安装 tensorflow 或 pytorch 时，也会同时安装匹配 tensorflow 或 pytorch 的 cuda 包，包括 cudatoolkit, cudnn, cupti 等。

conda 里的 cuda 的版本是与 tensorflow 或 pytorch 的版本相关的，与主机环境里的 cuda 版本无关。

conda 里的 cudatoolkit 并不包括 nvcc 编译器。如果某个程序需要编译 cu 源文件，则会使用主机环境的 nvcc。
这就可能造成编译出来的 cuda 模块依赖的是主机环境的 cuda 版本，而其它部分依赖 conda 的 cuda 版本。
这可能会造成运行失败，一般是 `ImportError: xxx.so: undefined symbol: _YYY` ，其中 xxx.so 是编译好的 cuda 模块。
主机的 nvcc 和 conda 的 cudatoolkit 版本不同，不管哪个大哪个小都可能造成问题（比较到小版本，如 10.0）。

## 创建/管理环境
[2.1]
```
conda create -n e2 python=3.7 -y
```
会创建在 ~/opt/miniconda3/envs/e2

删除
```
conda env remove -n ENV_NAME
```

列出
```
conda env list
```

conda 的环境在配置文件 `~/.conda/environments.txt` 中列出。

## 清除缓存

缓存的目录（~/opt/miniconda3 是 conda 的安装目录）：
```
~/opt/miniconda3/pkgs
~/.conda/pkgs            # 不一定使用
```

缓存的目录是所有 env 共享的。有一些缓存是各个环境需要使用的，不能简单删除。

清除无用的缓存：
```
conda clean -t -d
conda clean -p -d
```

参数含义：
```
-d, --dry-run
-t, --tarballs
-p, --packages
```

-t 和 -p 的区分不太清楚。但如果要彻底清除无用的缓存，应该两个都执行。

## 原理

conda 创建时会安装以下包：

```
  _libgcc_mutex      anaconda/cloud/conda-forge/linux-64::_libgcc_mutex-0.1-conda_forge
  _openmp_mutex      anaconda/cloud/conda-forge/linux-64::_openmp_mutex-4.5-0_gnu
  ca-certificates    anaconda/cloud/conda-forge/linux-64::ca-certificates-2019.11.28-hecc5488_0
  certifi            anaconda/cloud/conda-forge/linux-64::certifi-2019.11.28-py37_0
  ld_impl_linux-64   anaconda/cloud/conda-forge/linux-64::ld_impl_linux-64-2.33.1-h53a641e_8
  libffi             anaconda/cloud/conda-forge/linux-64::libffi-3.2.1-he1b5a44_1006
  libgcc-ng          anaconda/cloud/conda-forge/linux-64::libgcc-ng-9.2.0-h24d8f2e_2
  libgomp            anaconda/cloud/conda-forge/linux-64::libgomp-9.2.0-h24d8f2e_2
  libstdcxx-ng       anaconda/cloud/conda-forge/linux-64::libstdcxx-ng-9.2.0-hdf63c60_2
  ncurses            anaconda/cloud/conda-forge/linux-64::ncurses-6.1-hf484d3e_1002
  openssl            anaconda/cloud/conda-forge/linux-64::openssl-1.1.1d-h516909a_0
  pip                anaconda/cloud/conda-forge/noarch::pip-20.0.2-py_2
  python             anaconda/cloud/conda-forge/linux-64::python-3.7.6-h357f687_2
  readline           anaconda/cloud/conda-forge/linux-64::readline-8.0-hf8c457e_0
  setuptools         anaconda/cloud/conda-forge/linux-64::setuptools-45.2.0-py37_0
  sqlite             anaconda/cloud/conda-forge/linux-64::sqlite-3.30.1-hcee41ef_0
  tk                 anaconda/cloud/conda-forge/linux-64::tk-8.6.10-hed695b0_0
  wheel              anaconda/cloud/conda-forge/noarch::wheel-0.34.2-py_1
  xz                 anaconda/cloud/conda-forge/linux-64::xz-5.2.4-h14c3975_1001
  zlib               anaconda/cloud/conda-forge/linux-64::zlib-1.2.11-h516909a_1006
```
