## 参考资料
[1.1] linpack http://www.netlib.org/linpack/

[2.2] lapack http://www.netlib.org/lapack/

[3.1] hpl-ai https://bitbucket.org/icl/hpl-ai/src/main/

[3.2] NVIDIA HPC-Benchmarks 21.4 HPL/HPL-AI https://catalog.ngc.nvidia.com/orgs/nvidia/containers/hpc-benchmarks

[3.3] AMD HPL https://developer.amd.com/spack/hpl-benchmark/

[4.1] 面向国产异构处理器的 HPLAI 基准实现及优化 https://wu-kan.github.io/sysu-thesis/main.pdf

[4.2] 面向国产异构处理器的 HPL-AI 基准实现优化 https://wu-kan.github.io/sysu-thesis/presentation/pre.pdf

[4.3] https://wu-kan.cn/2021/03/14/HPL-AI/

[4.4] https://github.com/wu-kan/HPL-AI

## linpack

## lapack

### 安装
参考 readme.md
依赖： cmake
```
mkdir build
cd build
cmake -DCMAKE_INSTALL_LIBDIR=$HOME/opt/lapack ..
cmake --build . -j --target install
```

安装了以下文件：
```
-- Installing: /home/duanyao/opt/lapack/cmake/lapack-3.10.0/lapack-targets.cmake
-- Installing: /home/duanyao/opt/lapack/cmake/lapack-3.10.0/lapack-targets-release.cmake
-- Installing: /home/duanyao/opt/lapack/pkgconfig/lapack.pc
-- Installing: /home/duanyao/opt/lapack/cmake/lapack-3.10.0/lapack-config.cmake
-- Installing: /home/duanyao/opt/lapack/cmake/lapack-3.10.0/lapack-config-version.cmake
-- Installing: /home/duanyao/opt/lapack/pkgconfig/blas.pc
-- Installing: /home/duanyao/opt/lapack/libblas.a
-- Installing: /home/duanyao/opt/lapack/liblapack.a
```

## HPL

编译：

将源码解压到 ~/hpl 目录，

```
cd ~/hpl
cp setup/Make.Linux_Intel64 .
make arch=Linux_Intel64
```
产物在 bin/Linux_Intel64 目录。

注意源码目录必须是 ~/hpl，否则会有编译错误：
```
make[2]: 进入目录“/home/duanyao/project/hpl/src/auxil/Linux_Intel64”
Makefile:47: Make.inc: 没有那个文件或目录
make[2]: *** 没有规则可制作目标“Make.inc”。 停止。
make[2]: 离开目录“/home/duanyao/project/hpl/src/auxil/Linux_Intel64”
make[1]: *** [Make.top:54：build_src] 错误 2
make[1]: 离开目录“/home/duanyao/project/hpl”
make: *** [Makefile:72：build] 错误 2
```

继续编译仍有错误：

```
make[2]：mpiicc：命令未找到
```
mpiicc 类似 mpicc ，但是使用 intel 编译器。

## HPL-AI
[]
Linpack1是国际上最受认可的高性能计算机基准
是 TOP500 超算排名的唯一参照，仅能衡量计算系统双精度求解能力
2019 年，J. Dongarra 对 Linpack 基准进行扩展
新基准被称作 HPL-AI2
在矩阵求解过程中放弃双精度计算的要求
使用数值迭代手段，恢复在低精度运算中损失的精度
可利用已有的或即将推出的 AI 设备来加速求解过程
目前的开源实现仅有 J. Dongarra 提供的单线程参考版本

### HPL-AI 参考实现
[3.1]

```
autoreconf -ivf
./configure --prefix=$HOME/opt/hpl-ai

$ ./hpl-ai 4000 4
================================================================================
                        HPL-AI Mixed-Precision Benchmark                        
       Written by Yaohung Mike Tsai, Innovative Computing Laboratory, UTK       
================================================================================

This is a reference implementation with the matrix generator, an example
mixed-precision solver with LU factorization in single and GMRES in double,
as well as the scaled residual check.
Please visit http://www.icl.utk.edu/research/hpl-ai for more details.

Time spent in conversion to single:        0.314 second
Time spent in factorization       :       18.809 second
Time spent in solve               :        0.022 second
Time spent in conversion to double:        0.357 second
Residual norm at the beginning of GMRES: 1.654646e-09
Estimated residual norm at the 1-th iteration of GMRES: 1.658896e-15
Estimated residual norm at the 2-th iteration of GMRES: 2.561155e-21
Time spent in GMRES               :        0.222 second
Total time                        :       19.410 second
Effective operation per sec       :     2.199404 GFLOPs
```

默认状态下编译，只会使用 x86 的标量运算指令。

### nvidia HPL-AI 实现
