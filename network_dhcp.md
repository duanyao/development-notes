


[2.1] 对169.254.0.0/16网段的一点解释
https://zhuanlan.zhihu.com/p/100732856

[2.2] 关于 169.254.0.0/16 地址的一点笔记
https://nova.moe/note-on-169-254-ip-addresses/

[3.1] 1.3. Configuring the DHCP Client Behavior
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/configuring_the_dhcp_client_behavior

[3.2] Using Network Manager (nmcli) to configure DHCP, with a fallback static IP address in case DHCP fails
https://stackoverflow.com/questions/58715702/using-network-manager-nmcli-to-configure-dhcp-with-a-fallback-static-ip-addre

[3.3] NetworkManager and IPv4LL (link-local)
https://www.linuxquestions.org/questions/linux-embedded-and-single-board-computer-78/networkmanager-and-ipv4ll-link-local-4175672529/

[3.4] Linux Force DHCP Client (dhclient) to Renew IP Address
https://www.cyberciti.biz/faq/howto-linux-renew-dhcp-client-ip-address/

## linux Network Manager

与 windows 不同，如果一个连接被设置为 dhcp 模式，那么如果 dhcp 过程失败，则连接失败并不可用，不会分配 link local 地址。
不过，Network Manager 可以用以下方法解决这个问题：
（1）另外建立一个静态IP模式的连接，使用与 dhcp 模式连接相同的网卡，然后对两者设置适当的自动连接优先级 [3.2]。
（2）对同一个连接同时设置静态IP和dhcp [3.2-3.3]。

## linux force DHCP client to renew IP address
重新自动获得 IP：
```
sudo dhclient -d -r  # 释放当前 IP
sudo dhclient -d     # 获得 IP
```
指定网口：

```
sudo dhclient -d -r eth0
sudo dhclient -d eth0
```

其它参数：

-s server-addr
-4 DHCPv4 protocol
-6 DHCPv6 protocol
-d Force dhclient to run as a foreground process. imply -v 。
-v Enable verbose log messages.
-n Do  not  configure  any interfaces.

其它方法：
sudo systemctl restart network.service

nmcli con down id 'enp6s0'
nmcli con up id 'enp6s0'

