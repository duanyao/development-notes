## 参考资料

[1] https://www.landiannews.com/archives/57259.html
[2] https://www.microsoft.com/zh-cn/p/hevc-video-extensions-from-device-manufacturer/9n4wgh0z6vhq

## windows 10 Edge 与 h.265

Windows 10系统安装后会自动下载安装免费版版HEVC 扩展，所以用户日常使用并不会发现还要付费购买。
而且经蓝点网查询微软商店里有两个完全相同的HEVC 扩展，而系统预装的是免费版这里购买的是收费版本。
至于系统预装的免费扩展为何会被自动删除可能也得问微软，至少蓝点网测试时没有主动删除这个扩展程序[1]。

这个扩展安装后会出现在windows的应用列表里，叫做“HEVC 视频扩展”。只有安装了这个扩展，edge浏览器才可以播放h.265视频。

通过链接[2]可以通过microsoft store 免费安装这个扩展。

Microsoft Edge 44.18362.449.0
Microsoft EdgeHTML 18.18363

## 支持 HEVC 的 chromium
https://github.com/henrypp/chromium 二进制版只有windows。现在的版本有问题，无法播放h.264。

https://chromium.woolyss.com/ 二进制版有各种操作系统的版本。all-codecs 标签的就是支持 h.265 的版本。

https://github-production-release-asset-2e65be.s3.amazonaws.com/235662161/0e284300-2f79-11eb-83f6-d2d964aee437?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20201205%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20201205T055040Z&X-Amz-Expires=300&X-Amz-Signature=5d9cca085e71f79ce91a8cd23a13ee6a7582c0508f2621e5598424c8ac46ec91&X-Amz-SignedHeaders=host&actor_id=0&key_id=0&repo_id=235662161&response-content-disposition=attachment%3B%20filename%3Dungoogled-chromium_87.0.4280.66_1.vaapi_linux.tar.xz&response-content-type=application%2Foctet-stream

https://chromium.woolyss.com/f/chrlauncher-win64-stable-ungoogled.zip

https://github-production-release-asset-2e65be.s3.amazonaws.com/95730276/9618e780-3620-11eb-9662-1577054f23b2?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20201205%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20201205T060417Z&X-Amz-Expires=300&X-Amz-Signature=63abad9d8320b5a0fa41a840ba251410b565bc646bd93454c3eed65e0a98456f&X-Amz-SignedHeaders=host&actor_id=0&key_id=0&repo_id=95730276&response-content-disposition=attachment%3B%20filename%3Dungoogled-chromium-87.0.4280.88-1_Win64.7z&response-content-type=application%2Foctet-stream


